
//=====================================
//datepicker
//=====================================

$(function() {
	$( ".ma-datepicker" ).datepicker();
});


//=====================================
//checkbox
//=====================================

$(document).on('click', '.ma-checkbox-wrap', function(e) {
	$(this).find('input[type=checkbox]').prop('checked', function(){
		return !this.checked;
	});
});

//=====================================
//modals
//=====================================

function modalIn($Id) {
	$('body').addClass('modal-in');
	$($Id).addClass('ma-in');
	if( $($Id).has('.ma-modal-search-input') ) {
		$($Id).find('.ma-modal-search-input').focus();
	} else {}
	$('#OverlayMask').addClass('ma-in');
}

function modalOut() {
	$('body').removeClass('modal-in');
	$('#OverlayMask').removeClass('ma-in');
	$('.ma-modal').removeClass('ma-in');
}

$(document).on('click', '.ma-modal-close', function(e) {
	modalOut();
});

// example of calling a specific modal below

//$(document).on('click', '#triggerModal1', function(e){
//	modalIn('#modal1');
//});


//=====================================
//tabs
//=====================================

	$(document).on('click', '.ma-tab-link', function(){
		var tab_id = $(this).attr('data-tab');
		$(this).siblings('.ma-tab-link').removeClass('active');
		$("#"+tab_id).siblings('.ma-tab-content').removeClass('active');
		$(this).addClass('active');
		$("#"+tab_id).addClass('active');
	});
