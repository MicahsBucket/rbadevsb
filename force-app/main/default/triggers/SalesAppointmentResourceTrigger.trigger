/**
* @File Name          : SalesAppointmentResourceTrigger.trigger
* @Description        :
* @Author             : James Loghry (Demand Chain)
* @Group              :
* @Last Modified By   : James Loghry (Demand Chain)
* @Last Modified On   : 5/21/2019, 8:56:09 PM
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    5/21/2019, 8:56:04 PM   James Loghry (Demand Chain)     Initial Version
**/
trigger SalesAppointmentResourceTrigger on Sales_Appointment_Resource__c (before insert, before update,after update,after delete) {
    
    if (!UtilityMethods.isTriggerActive('SalesAppointmentResource')) return;
    
    Set<String> assignedStatuses = new Set<String>{'Manually Reassigned','Assigned','Manually Assigned','Accepted','Confirmed'};
        
        if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
            Set<Id> appointmentIds = new Set<Id>();
            Set<Id> capacityIds = new Set<Id>();
            for(Sales_Appointment_Resource__c sar : (List<Sales_Appointment_Resource__c>)Trigger.new){
                appointmentIds.add(sar.Sales_Appointment__c);
                if(assignedStatuses.contains(sar.Status__c)){
                    capacityIds.add(sar.Sales_Capacity__c);
                }
            }
            
            Map<Id,Sales_Capacity__c> capacities = new Map<Id,Sales_Capacity__c>([Select Id,User__c From Sales_Capacity__c Where Id in :capacityIds]);
            
            Map<Id,Sales_Appointment__c> appointments = new Map<Id,Sales_Appointment__c>(
                [Select
                 Id,
                 Slot__c,
                 Appointment_Date_Time__c,
                 Sales_Order__r.Opportunity__r.Id,
                 Sales_Order__r.Opportunity__r.OwnerId
                 From
                 Sales_Appointment__c
                 Where
                 Id in :appointmentIds]);
            
            Map<Id,Opportunity> opportunitiesToUpdate = new Map<Id,Opportunity>();
            for(Sales_Appointment_Resource__c sar : (List<Sales_Appointment_Resource__c>)Trigger.new){
                
                Sales_Appointment__c sa = appointments.get(sar.Sales_Appointment__c);
                sar.Slot__c = sa.Slot__c;
                sar.Date__c = (sa.Appointment_Date_Time__c == null ? null : sa.Appointment_Date_Time__c.date());
                
                if(sar.Primary__c && sar.Sales_Capacity__c != null && (Trigger.isInsert || Trigger.oldMap.get(sar.Id).Sales_Capacity__c != sar.Sales_Capacity__c)){
                    Sales_Capacity__c capacity = capacities.get(sar.Sales_Capacity__c);
                    Opportunity opportunityOwnerUpdate = sa.Sales_Order__r.Opportunity__r;
                    opportunityOwnerUpdate.OwnerId = capacity.User__c;
                    opportunitiesToUpdate.put(opportunityOwnerUpdate.Id,opportunityOwnerUpdate);
                }
            }
            update opportunitiesToUpdate.values();
            
            //Look for existing appointment resources, to see if any appointments need to be unassigned from the current rep(s).
            /*List<Sales_Appointment_Resource__c> resourcesToCheck = [Select Id,Status__c,Sales_Capacity__c From Sales_Appointment_Resource__c Where Sales_Capacity__c in :capacityIds And Status__c in :assignedStatuses];
if(!resourcesToCheck.isEmpty()){
for(Sales_Appointment_Resource__c sar : (List<Sales_Appointment_Resource__c>)Trigger.new){
if(assignedStatuses.contains(sar.Status__c)){
for(Sales_Appointment_Resource__c otherAppointment : resourcesToCheck){
if(otherAppointment.Sales_Capacity__c == sar.Sales_Capacity__c && otherAppointment.Id != sar.Id){
sar.addError('Sales rep is already assigned to an appointment during this time slot.  Please either unassign the rep from their other appointments or select a different rep.');
}
}
}
}
}*/
        }
    
    if(Trigger.isUpdate){
        Set<Id> idsToCheckForResulting = new Set<Id>();
        for(Sales_Appointment_Resource__c sar : Trigger.new){
            if(sar.Primary__c && Trigger.oldMap.get(sar.Id).Status__c != sar.Status__c && sar.Status__c != 'Resulted'){
                idsToCheckForResulting.add(sar.Id);
            }
        }
        
        if(!idsToCheckForResulting.isEmpty()){
            for(Sales_Appointment_Resource__c sar : [Select Id From Sales_Appointment_Resource__c Where Sales_Appointment__r.Sales_Order__r.Resulted__c = true And Id in :idsToCheckForresulting]){
                Trigger.newMap.get(sar.Id).Status__c = 'Resulted';
            }
        }
    }
    
    //SS-259 Start
    if(Trigger.isUpdate && Trigger.iSAfter){
        SalesAppointmentResourceTriggerHandler.AfterUpdate(trigger.new,trigger.OldMap);
    }
    
    if(Trigger.isDelete && Trigger.iSAfter){
        SalesAppointmentResourceTriggerHandler.AfterDelete(trigger.OldMap);
    }
    //SS-259 End
}