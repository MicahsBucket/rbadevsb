/*******************************************************//**

@trigger UserTrigger

@brief  trigger framework to secure order of operation

@author  Creston Kuenzi (Slalom.CDK)

@version    2016-5/25  Slalom.CDK
    Created.

@see        UserTriggerTest

@copyright  (c)2016 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/

trigger UserTrigger on User (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Begin logic to filter skip logic by profile    
	System.debug('entering User Trigger');
    if (!UtilityMethods.isTriggerActive('User')) return;
    
    //HANDLERS AND MANAGERS
	System.debug('user trigger calling user trigger handler');
    UserTriggerHandler handler = new UserTriggerHandler(Trigger.isExecuting,Trigger.size);       
        
    // Before Insert
    
    if(Trigger.isInsert && Trigger.isBefore){
	    System.debug('user trigger on before insert');
        handler.OnBeforeInsert(Trigger.new);
    }
    
    //  Before Update
    
    if(Trigger.isUpdate && Trigger.isBefore){
	    System.debug('User Trigger onbeforeUpdate');
        handler.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
    }
            
    // After Insert 
    
    else if(Trigger.isInsert && Trigger.isAfter && !UserTriggerHandler.skipTrigger){
            System.debug('user trigger on after insert');
            handler.OnAfterInsert(Trigger.new);
			handler.assignPermissions(null, Trigger.newMap);
    } 
    
    // After Update
    
    else if(Trigger.isUpdate && Trigger.isAfter){
	    System.debug('user trigger on after update');
        handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
		handler.assignPermissions(Trigger.oldMap, Trigger.newMap);
    }
    
}