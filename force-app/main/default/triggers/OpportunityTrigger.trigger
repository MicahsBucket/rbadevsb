/*
Trigger framework Anthony Strafaccia 2015

This trigger is a framework for Opportunity object. It is designed to be the only trigger needed for the Opportunity object and allows for the customization of the order of execution.

All logic should be handled on the OpportunityTriggerHandler class.
*/

trigger OpportunityTrigger on Opportunity (before insert, before update, before delete, after insert, after undelete, after update, after delete) {
    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Opportunity')) return;

    RMS_salesOrderManager salesOrderManager = new RMS_salesOrderManager();
    OpportunityTriggerHandler handler = new OpportunityTriggerHandler();

    // Before Insert
    if(Trigger.isInsert && Trigger.isBefore) {
        system.debug('updating opportunity');
        handler.onBeforeInsert(Trigger.new);
        handler.DetermineIfJsonUploaded(Trigger.newMap, Trigger.oldMap);
    }
    //  Before Update
    if(Trigger.isUpdate && Trigger.isBefore){
        salesOrderManager.createSalesOrderOnOpportunityClosedWon(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        handler.DetermineIfJsonUploaded(Trigger.newMap, Trigger.oldMap);
    }

    // After Insert
    else if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterInsert(Trigger.new, Trigger.newMap);
    }

    // After Update
    else if(Trigger.isUpdate && Trigger.isAfter){
        CommissionsManagement.checkForOwnerUpdate(Trigger.new, Trigger.oldMap);
        handler.updateSalesRepContact(Trigger.newMap, Trigger.oldMap);  
    }

    UtilityMethods.setOpportunityTriggerRan();
}