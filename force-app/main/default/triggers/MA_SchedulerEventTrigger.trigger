trigger MA_SchedulerEventTrigger on MA_Scheduler_Event__c (after insert, before delete) {

	/******************************************* */
	/*											 */
	/*		On INSERT - oldMap is NULL 			 */
	/*		On DELETE - newMap is NULL 			 */
	/*		On UPDATE - neither is NULL 		 */
	/*											 */
	/******************************************* */

	if ((Trigger.isAfter && Trigger.isInsert) || (Trigger.isBefore && Trigger.isDelete)) {
		if ((Trigger.newMap != null && !Trigger.newMap.isEmpty()) || (Trigger.oldMap != null && !Trigger.oldMap.isEmpty())) {
			Set<Id> storeIds = new Set<Id>();
			Set<Id> resourceIds = new Set<Id>();
			Set<Id> workOrderIds = new Set<Id>();
			Map<Id, Id> workOrderToStoreMap = new Map<Id, Id>();
			Map<Id, MA_Scheduler_Event__c> maEventsMap = Trigger.newMap;
			Map<String, Assigned_Resource_History__c> resourceHistoryMap = new Map<String, Assigned_Resource_History__c>();
			Map<String, Assigned_Resource_History__c> resourceHistoryMapToUpsert = new Map<String, Assigned_Resource_History__c>();

			if (maEventsMap != null && Trigger.oldMap != null) {
				maEventsMap.putAll(Trigger.oldMap);
			}
			else if (Trigger.oldMap != null) {
				maEventsMap = Trigger.oldMap;
			}
			else if (maEventsMap == null) {
				maEventsMap = new Map<Id, MA_Scheduler_Event__c>();
			}

			for (MA_Scheduler_Event__c maEvent : maEventsMap.values()) {
				resourceIds.add(maEvent.Resource__c);
				workOrderIds.add(maEvent.RbA_Work_Order__c);
			}

			for (RbA_Work_Order__c workOrder : [SELECT Id, Account__r.Store_Location__r.Active_Store_Configuration__c FROM RbA_Work_Order__c WHERE Id IN :workOrderIds]) {
				storeIds.add(workOrder.Account__r.Store_Location__r.Active_Store_Configuration__c);
				workOrderToStoreMap.put(workOrder.Id, workOrder.Account__r.Store_Location__r.Active_Store_Configuration__c);
			}

			Map<Id, String> storeTimezonesMap = MA_SchedulerUtilities.retrieveStoreTimezones(storeIds);

			List<Assigned_Resource_History__c> resourceHistories = [
				SELECT Id, Scheduled_Times__c, Resource__c, RbA_Work_Order__c FROM Assigned_Resource_History__c
					WHERE Resource__c IN :resourceIds AND RbA_Work_Order__c IN :workOrderIds
			];

			for (Assigned_Resource_History__c resourceHistory : resourceHistories) {
				resourceHistoryMap.put(resourceHistory.Resource__c + ':' + resourceHistory.RbA_Work_Order__c, resourceHistory);
			}

			String usersTimezone = UserInfo.getTimeZone().toString();

			for (MA_Scheduler_Event__c maEvent : maEventsMap.values()) {
				Assigned_Resource_History__c resourceHistory = resourceHistoryMap.get(maEvent.Resource__c + ':' + maEvent.RbA_Work_Order__c);
				DateTime startDateTime = maEvent.StartDateTime__c;

				if (startDateTime != null) {
					String storeId = workOrderToStoreMap.get(maEvent.RbA_Work_Order__c);
					String timezone = usersTimezone;

					if (String.isNotBlank(storeId)) {
						timezone = storeTimezonesMap.get(storeId);
					}

					startDateTime = MA_SchedulerUtilities.convertTimezoneToTimezone(startDateTime, timezone, usersTimezone);
				}

				if (resourceHistory == null) {
					Set<String> scheduledTimesSet = new Set<String> { startDateTime.format('MM/dd/yyyy hh:mm a') };
					String scheduledTimes = JSON.serialize(scheduledTimesSet);

					resourceHistory = new Assigned_Resource_History__c(
						Resource__c = maEvent.Resource__c,
						RbA_Work_Order__c = maEvent.RbA_Work_Order__c,
						Scheduled_Times__c = scheduledTimes
					);
				}
				else {
					Set<String> scheduledTimesSet = new Set<String>();
					String scheduledTimes = resourceHistory.Scheduled_Times__c;

					if (String.isNotBlank(scheduledTimes)) {
						scheduledTimesSet = (Set<String>)JSON.deserialize(scheduledTimes, Set<String>.class);
					}

					scheduledTimesSet.add(startDateTime.format('MM/dd/yyyy hh:mm a'));
					scheduledTimes = JSON.serialize(scheduledTimesSet);
					resourceHistory.Scheduled_Times__c = scheduledTimes;
				}

				resourceHistoryMap.put(resourceHistory.Resource__c + ':' + resourceHistory.RbA_Work_Order__c, resourceHistory);

				if (resourceHistory.Resource__c != null) {
					resourceHistoryMapToUpsert.put(resourceHistory.Resource__c + ':' + resourceHistory.RbA_Work_Order__c, resourceHistory);
				}
			}

			upsert resourceHistoryMapToUpsert.values();
		}
	}

}