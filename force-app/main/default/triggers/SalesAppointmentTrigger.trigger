/**
 * @description       : 
 * @author            : Connor.Davis@andersencorp.com
 * @group             : 
 * @last modified on  : 02-16-2021
 * @last modified by  : Connor.Davis@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                          Modification
 * 1.0   01-25-2021   Connor.Davis@andersencorp.com   Initial Version
**/
trigger SalesAppointmentTrigger on Sales_Appointment__c (before insert, before update) {
    
    if (!UtilityMethods.isTriggerActive('SalesAppointment')) return;

    if(Trigger.isBefore){
        Set<Id> orderIds = new Set<Id>();
    	List<String> days = new List<String>();
    
        //First, generating a set of sales orders to query for the account configuration / slot group, which
        //is neede to determine the specific slot that we'll default for the given appointments.
        for(Sales_Appointment__c sa : (List<Sales_Appointment__c>)Trigger.new){
            if(Trigger.oldMap == null || Trigger.oldMap.get(sa.Id).Appointment_Date_Time__c != sa.Appointment_Date_Time__c || Trigger.oldMap.get(sa.Id).Appointment_Time__c != sa.Appointment_Time__c){

                orderIds.add(sa.Sales_Order__c);

                days.add(sa.Appointment_Date_Time__c.format('EEEE'));
            }
            
            /*
            //Runs on an update trigger to determine if this appointment is assigned or not.
            if(Trigger.oldMap != null && Trigger.oldMap.get(sa.Id).Sales_Appointment_Resources__c != sa.Sales_Appointment_Resources__c){
            	sa.Assigned__c = (sa.Sales_Appointment_Resources__c != 0);
            }*/
        }
        
        System.debug('JWL: orderIds: ' + orderIds);
           
        //If there are Sales Orders, in other words if this is a new appointment, or if the appointment date time has changed,
        //then run the logic below.
        if(!orderIds.isEmpty()){
            
            //Determine the slot group (e.g. three slots or five slots), that are needed to query to determine the specific slot(s) we'll need to set.
            Set<String> slotGroups = new Set<String>();
            Map<Id,Sales_Order__c> orders = new Map<Id,Sales_Order__c>([
                Select 
                Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Slot_Group__c ,
                Opportunity__r.Store_Location__r.Email_For_SalesSched__c
                From Sales_Order__c 
                Where Id in :orderIds]);
            for(Sales_Order__c so : orders.values()){
                if(so.Opportunity__r != null && so.Opportunity__r.Store_Location__r != null && so.Opportunity__r.Store_Location__r.Active_Store_Configuration__r != null){
                    slotGroups.add(so.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Slot_Group__c);
                }
            }
            
            List<Slot__c> slots = [Select Id,Day__c,Start__c,End__c,Name,Slot_Group__c From Slot__c Where Slot_Group__c in :slotGroups And Day__c in :days];
            System.debug('JWL: slots: ' + slots);
            
            //This map is used for updating related appointment resources (If this is an update to an existing date time, for instance.)
            Set<Id> appointmentSlotUpdates = new Set<Id>();
            
            //Iterate through the current trigger context to set the appropriate slots on the appointment records.
            for(Sales_Appointment__c sa : (List<Sales_Appointment__c>)Trigger.new){
                String day = sa.Appointment_Date_Time__c.format('EEEE');
                
                Sales_Order__c so = orders.get(sa.Sales_Order__c);
                String slotGroup = null;
                if(so.Opportunity__r != null && so.Opportunity__r.Store_Location__r != null && so.Opportunity__r.Store_Location__r.Active_Store_Configuration__r != null){
                    slotGroup = so.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Slot_Group__c;
                }
                Time appointmentStartTime = sa.Appointment_Time__c;
                for(Slot__c slot : slots){
                    if(slot.Slot_Group__c == slotGroup && 
                       	slot.Day__c == day && 
                      	slot.Start__c <= appointmentStartTime && 
                       	slot.End__c >= appointmentStartTime){
                       	//Hey, we found the slot!
                       	sa.Slot__c = slot.Id;
                            
                        //Populate this text field, so that the proper capacity records are displayed in the lookup.
                        //sa.Slot_and_Date_Reference__c = sa.Appointment_Date_Time__c.format('M/d/yyyy') + ' ' + slot.Name;
                        //System.debug('JWL: slot and date reference: ' + sa.Slot_and_Date_Reference__c);
                        
                        //If this trigger is an update, then we may also need to update related resource records.  This map will help with that.
                        if(sa.Id != null){
                            appointmentSlotUpdates.add(sa.Id);            
                        }
                    }   
                }

                // If the appointment is today, send an email to the managers so they know to assign it
                Datetime appointmentDateTime = sa.Appointment_Date_Time__c;
                if(appointmentDateTime.date() == System.today()){
                    
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    String emailString = so.Opportunity__r.Store_Location__r.Email_For_SalesSched__c;
                    if(String.isNotBlank(emailString)){
                        
                        List<String> emailList = emailString.split(',');
                        email.setToAddresses(emailList);

                        String body = 'A same-day appointment has just been confirmed. Check the Sales Manager page to assign and finalize.';
                        email.setPlainTextBody(body);

                        email.SetSubject('Appointment Scheduled for Today - Do Not Reply');

                        Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});
                    }
                }
            }
            
            //If this trigger is an update to an appointment date/time, then update the related sales appointment resource records.
            if(!appointmentSlotUpdates.isEmpty()){
                List<Sales_Appointment_Resource__c> sarl = [Select Id,Sales_Appointment__r.Slot__c,Sales_Appointment__r.Appointment_Date_Time__c From Sales_Appointment_Resource__c Where Sales_Appointment__c in :appointmentSlotUpdates];
                for(Sales_Appointment_Resource__c sar : sarl){
                    sar.Slot__c = sar.Sales_Appointment__r.Slot__c;
                    sar.Date__c = sar.Sales_Appointment__r.Appointment_Date_Time__c == null ? null : sar.Sales_Appointment__r.Appointment_Date_Time__c.dateGmt();
                }
                update sarl;
            }
        }
    }
    
    
    // Current day appointment cancellation email
    if(trigger.isUpdate) {
        List<Sales_Appointment__c> lCanceledAppts = new List<Sales_Appointment__c>();
        for(Sales_Appointment__c apptNew : trigger.new){
            if(apptNew.Cancelled__c && apptNew.Cancelled__c != trigger.oldMap.get(apptNew.Id).Cancelled__c){
                //appt has been newly updated to cancelled. Add to list.
                System.Debug('CD: Canceled appointement added to list');
                lCanceledAppts.add(apptNew);
            }
        }
        if(lCanceledAppts.size() > 0){
        	//SalesApptCancellation clsApptCancellation = new SalesApptCancellation();     
        	SalesSchedApptCancellationUtil.sendApptCancellationEmails(lCanceledAppts);
        }
    }
    
}