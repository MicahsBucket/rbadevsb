/**
 * @File Name          : SalesOrderTrigger.trigger
 * @Description        : 
 * @Author             : James Loghry (Demand Chain)
 * @Group              : 
 * @Last Modified By   : James Loghry (Demand Chain)
 * @Last Modified On   : 5/23/2019, 12:04:58 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    5/1/2019, 9:53:30 AM   James Loghry (Demand Chain)     Initial Version
**/
trigger SalesOrderTrigger on Sales_Order__c (before update){

    Set<String> secondaryResultingFields = new Set<String>{'Ride_Along_Learnings__c'};//'Time_Arrived__c','Time_Left__c',

    Set<String> allResultingFields = new Set<String>{'Time_Arrived__c','Time_Left__c','Key_Contact_Person__c','Result__c'};

    /*Map<String,Set<String>> resultMap = new Map<String,Set<String>>{
        'Sale' => new Set<String>{'Payment_Type__c',Deposit_Type__c','Deposit__c','Construction_Department_Notes__c','Why_Customer_Purchased__c','Concerns__c','Address_Concerns__c','Partial_Sale__c','Historical_Area__c','Mail_and_Canvass_Neighborhood__c','Were_All_Owners_Present__c'}
        ,'Demo No Sale' =>  new Set<String>{'Project_Description__c','Key_Reason_Customer_Didnt_Proceed__c','Mail_and_Canvass_Neighborhood__c','Future_Purchase__c','Were_All_Owners_Present__c','Rescheduled__c'}
        ,'No Demo' => new Set<String>{'Why_No_Demo__c','Mail_and_Canvass_Neighborhood__c','Were_All_Owners_Present__c','Rescheduled__c'}
        ,'Not Home' => new Set<String>{'Comments__c','Project_Description__c'}
        ,'Not Covered' => new Set<String>{'Why_Not_Covered__c'}
    };*/
        
    Map<String,Set<String>> resultMap = new Map<String,Set<String>>{
        'Sale' => new Set<String>{'Series_One_Windows__c','Patio_Doors__c','Amount_If_Sold__c',
                                  'Series_One_Windows_Quoted__c','Patio_Doors_Quoted__c',
                                  'Contract_Amount_Quoted__c','BAY_Windows_Quoted__c','BOW_Windows_Quoted__c','Construction_Department_Notes__c',
                                  'Why_Customer_Purchased__c','Concerns__c','Address_Concerns__c','Partial_Sale__c','Historical_Area__c',
                                  'Mail_and_Canvass_Neighborhood__c','Payment_Type__c','Deposit__c','Deposit_Type__c',
                                  'Key_Contact_Person__c','Were_All_Owners_Present__c'}
        ,'Demo No Sale' =>  new Set<String>{'Series_One_Windows__c','Patio_Doors__c','Amount_If_Sold__c','Were_All_Owners_Present__c',
                                          'Series_One_Windows_Quoted__c','Patio_Doors_Quoted__c',
                                          'Contract_Amount_Quoted__c', 'Key_Contact_Person__c','Mail_and_Canvass_Neighborhood__c',
                                          'Project_Description__c','Key_Reason_Customer_Didnt_Proceed__c','Rescheduled__c','Future_Purchase__c'}
        ,'No Demo' => new Set<String>{'Why_No_Demo__c','Mail_and_Canvass_Neighborhood__c','Were_All_Owners_Present__c','Rescheduled__c','Key_Contact_Person__c'}
        ,'Not Home' => new Set<String>{'Comments__c','Project_Description__c','Key_Contact_Person__c'}
        ,'Not Covered' => new Set<String>{'Why_Not_Covered__c','Key_Contact_Person__c'}
    };
    
    //below code(Map) is written by ratna for clearing the previous result fields
    Map<String,Set<String>> resultOptinalFieldMap = new Map<String,Set<String>>{
        'Sale' => new Set<String>{'Series_One_Windows__c','Series_Two_Windows__c','Patio_Doors__c','Entry_Doors__c','Amount_If_Sold__c',
                                  'Series_One_Windows_Quoted__c','Series_Two_Windows_Quoted__c','Patio_Doors_Quoted__c','Entry_Doors_Quoted__c',
                                  'Contract_Amount_Quoted__c','BAY_Windows_Quoted__c','BOW_Windows_Quoted__c','Construction_Department_Notes__c',
                                  'Why_Customer_Purchased__c','Concerns__c','Address_Concerns__c','Partial_Sale__c','Historical_Area__c',
                                  'Mail_and_Canvass_Neighborhood__c','Payment_Type__c','Deposit__c','Deposit_Type__c','Cash__c',
                                  'Store_Finance_Program__c','Approved__c','Account_Number__c', 'Key_Contact_Person__c','Were_All_Owners_Present__c',
                                  'Turned_Down_By__c','Next_For_Renewal__c','Finance_Issues__c',
                                  'Financeable_Details__c','Includes_Series_A_Window__c'}, 
        'Demo No Sale' => new Set<String>{'Series_One_Windows__c','Series_Two_Windows__c','Patio_Doors__c','Entry_Doors__c','Amount_If_Sold__c','Were_All_Owners_Present__c',
                                          'Series_One_Windows_Quoted__c','Series_Two_Windows_Quoted__c','Patio_Doors_Quoted__c','Entry_Doors_Quoted__c','Includes_Series_A_Window__c',
                                          'Contract_Amount_Quoted__c','BAY_Windows_Quoted__c','BOW_Windows_Quoted__c', 'Key_Contact_Person__c','Mail_and_Canvass_Neighborhood__c',
                                          'Project_Description__c','Key_Reason_Customer_Didnt_Proceed__c','Rescheduled__c','Future_Purchase__c'},
        'No Demo' => new Set<String>{'Why_No_Demo__c','Mail_and_Canvass_Neighborhood__c','Were_All_Owners_Present__c','Rescheduled__c','Key_Contact_Person__c', 'Includes_Series_A_Window__c'}
        ,'Not Home' => new Set<String>{'Comments__c','Project_Description__c','Key_Contact_Person__c'}
        ,'Not Covered' => new Set<String>{'Why_Not_Covered__c','Key_Contact_Person__c'}
    };
    Map<String,Set<String>> paymentTypeMap = new Map<String,Set<String>>{
        'Finance' => new Set<String>{
            'Store_Finance_Program__c','Account_Number__c','Approved__c'
        },
        'Cash & Finance' => new Set<String>{
            'Cash__c','Finance__c','Store_Finance_Program__c','Account_Number__c','Approved__c'
        },
        'Finance Turn-Down' => new Set<String>{
             'Turned_Down_By__c','Next_For_Renewal__c','Finance_Issues__c'
        },
        'Cash' => new Set<String>()
    };

    Map<String,String> resultToOpportunityStageMap = new Map<String,String>{
        'Sale' => 'Sold - Pending',
        'Demo No Sale' => 'Demo No Sale',
        'No Demo' => 'No Demo',
        'Not Home' => 'Not Home',
        'Not Covered' => 'Not Covered'
    };
        
    //Below set is created by Ratna and the purpose is to clear/blank the fields other than fields in set when Result is changed
    Set<String> skipFieldsForResultChange=new Set<String>{'Account_Number__c','Check_out_Time__c','Result__c',
                                                          'Ride_Along_Advice__c','Ride_Along_Learnings__c','Time_Arrived__c',
                                                          'Time_Left__c','Name','Opportunity__c','Enabled_Id__c','Store_ID__c'};
   
    if(Trigger.isBefore || Trigger.isUpdate){
        Set<Id> opportunityIds = new Set<Id>();
        for(Sales_Order__c so : Trigger.new)
        {
            opportunityIds.add(so.Opportunity__c);
            //below code is written by Ratna for finding & clearing the fields for previous result
            Sales_Order__c oldOrder=(Sales_Order__c)Trigger.oldMap.get(so.id);
            if(so.Result__c!=null&&oldOrder.Result__c!=null&&oldOrder.Result__c!=so.Result__c)
            {
                String oldResult=oldOrder.Result__c;
                Set<String> oldResults=resultOptinalFieldMap.get(oldOrder.Result__c);
                Set<String> newResults=resultOptinalFieldMap.get(so.Result__c);
                for(String result:oldResults)
                {   
                    if(!newResults.contains(result)&&so.get(result)!=null)
                    {
                        so.put(result,null);
                    }
                }
            }
			//below code is written by Ratna for clearing the fields when the Sales-Manager click the button "Clear Resultant Details"            
            if(so.Clear_Resultant_Details__c)
            {
                so.Clear_Resultant_Details__c=false;
                Set<String> newResults=resultOptinalFieldMap.get(so.Result__c);
                for(String result:newResults)
                {
                	so.put(result,null);
                }
            }
        }

        //Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>([Select Store_Location__r.Active_Store_Configuration__r.Reps_can_schedule_follow_ups__c From Opportunity Where Id in :opportunityIds]);

        Map<Id,Boolean> resultedMap = new Map<Id,Boolean>();

        Set<Id> rideAlongRequirementsMet = new Set<Id>();

        Set<Id> ordersWithRideAlongs = new Set<Id>();
        for(Sales_Appointment_Resource__c sar :
            [Select
                Sales_Appointment__r.Sales_Order__c
             From
                Sales_Appointment_Resource__c
             Where
                Sales_Appointment__r.Sales_Order__c in :Trigger.newMap.keySet()
                And Primary__c = false]){
            ordersWithRideAlongs.add(sar.Sales_Appointment__r.Sales_Order__c);
        }

        Map<Id,Opportunity>  opportunitiesToUpdate = new Map<Id,Opportunity>();
        for(Sales_Order__c so : Trigger.new){
            Set<String> seriesAndEntryChecks=new Set<String>();
            if (so.Is_Series_2_Enabled__c && !('No Demo' == so.Result__c)) {
            	seriesAndEntryChecks.add('Series_Two_Windows__c');	    
                seriesAndEntryChecks.add('Series_Two_Windows_Quoted__c');
            }
            if (so.Is_Entry_Door_Enabled__c && !('No Demo' == so.Result__c)) {
            	seriesAndEntryChecks.add('Entry_Doors__c');	    
                seriesAndEntryChecks.add('Entry_Doors_Quoted__c');
            }
            if (so.Is_Series_A_Enabled__c) {
            	seriesAndEntryChecks.add('Includes_Series_A_Window__c');
            }
            System.debug('JWL: resulting fields: ' + secondaryResultingFields);
            if(validateSet(so, secondaryResultingFields)){
                rideAlongRequirementsMet.add(so.Id);
            }
            System.debug('JWL: rideAlongRequirementsMet: ' + rideAlongRequirementsMet);

            boolean isResulted = validateSet(so, allResultingFields);
            System.debug('JWL: isResulted1: ' + isResulted);

            if(isResulted){
                isResulted &= validateSet(so, resultMap.get(so.Result__c));
                System.debug('JWL: isResulted2: ' + isResulted);

                if('Sale' == so.Result__c){
                    System.debug('JWL: its a sale!');
                    isResulted &= validateSet(so, paymentTypeMap.get(so.Payment_Type__c));
                    if (!seriesAndEntryChecks.isEmpty()) {
                        isResulted &= validateSet(so, seriesAndEntryChecks);
                    }
                    System.debug('JWL: isResulted 3: ' + isResulted);

                    if('Finance Turn-Down' == so.Payment_Type__c && 'No' == so.Finance_Issues__c && String.isEmpty(so.Financeable_Details__c)){
                        isResulted = false;
                    }
                    System.debug('JWL: isResulted4: ' + isResulted);

                }

                
                if(('Demo No Sale' == so.Result__c || 'No Demo' == so.Result__c) && 'Yes' == so.Rescheduled__c) {
                    isResulted &= (so.Follow_Up_Appt_Date_Time__c != null);
                }

                if ((so.Result__c == 'Demo No Sale' || so.Result__c == 'No Demo') && !seriesAndEntryChecks.isEmpty()) {
                    isResulted &= validateSet(so, seriesAndEntryChecks);
                } 
                System.debug('JWL: isResulted5: ' + isResulted);

            }

            isResulted &= (!String.isEmpty(so.Ride_Along_Advice__c) || !ordersWithRideAlongs.contains(so.Id));
            System.debug('JWL: isResulted6: ' + isResulted);

            so.Resulted__c = isResulted;
            
            if(so.Resulted__c && (Trigger.oldMap == null || !Trigger.oldMap.get(so.Id).Resulted__c)){
                resultedMap.put(so.Id,isResulted);
                
                String stage = resultToOpportunityStageMap.get(so.Result__c);
                if(!String.isEmpty(stage)){
                    opportunitiesToUpdate.put(so.Opportunity__c, new Opportunity(Id = so.Opportunity__c,StageName = stage));
                }
            }
        }

        if(!opportunitiesToUpdate.values().isEmpty()){
            update opportunitiesToUpdate.values();
        }
        
        Set<Id> allSOIds = new Set<Id>();
        if(!rideAlongRequirementsMet.isEmpty())
        {
            allSOIds.addAll(rideAlongRequirementsMet);    
        }
        if(!resultedMap.isEmpty())
        {
            allSOIds.addAll(resultedMap.keySet());    
        }
        
        if(!allSOIds.isEmpty()){
            List<Sales_Appointment_Resource__c> resourcesToUpdate =
                [Select
                    Id,
                    Sales_Appointment__r.Sales_Order__c,
                    Primary__c,Status__c
                 From
                    Sales_Appointment_Resource__c
                 Where
                    (Sales_Appointment__r.Sales_Order__c in :allSOIds)
                    And Status__c != 'Resulted'];
            for(Sales_Appointment_Resource__c sar : resourcesToUpdate)
            {
                System.debug('JWL in resourcesToUpdate loop');
                if(sar.Primary__c&&resultedMap.containsKey(sar.Sales_Appointment__r.Sales_Order__c))
                {
                    sar.Status__c='Resulted';
                }
                if(!sar.Primary__c && rideAlongRequirementsMet.contains(sar.Sales_Appointment__r.Sales_Order__c))
                {
                    sar.Status__c = 'Resulted';
                }
            }
            update resourcesToUpdate;
        }
    }

    private static boolean validateSet(Sales_Order__c so, Set<String> lst){
        System.debug('JWL: validateSet: ' + lst);
        System.debug('JWL: so: ' + so);

        boolean ans = true;
        if(lst != null){
            for(String s : lst){
                Object o = so.get(s);
                ans &= (o != null);
                if(o instanceof String){
                    ans &= !String.isEmpty((String)o);
                }
            }
        }
        return ans;
    }
}