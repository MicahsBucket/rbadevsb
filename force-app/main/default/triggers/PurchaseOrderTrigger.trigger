trigger PurchaseOrderTrigger on Purchase_Order__c ( after insert, after update, before update, after delete, after undelete, before insert) {   
    
      List<SObject> orders = new List<SObject>();

    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Purchase_Order__c')) return;

    Map<Id,Order> updateOrderMap =new Map<Id,Order>(); 
    Set<Id> orderIds = new Set<Id>(); 
    private static Id costPoRecordTypeId = UtilityMethods.retrieveRecordTypeId('Cost_Purchase_Order', 'Purchase_Order__c');                                                                                                 
    
    PurchaseOrderTriggerHandler handler = new PurchaseOrderTriggerHandler(Trigger.isExecuting, Trigger.size);
    // List<SObject> orders = new List<SObject>();
    if(Trigger.isUpdate && Trigger.isBefore){
        handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
    }
    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    if(Trigger.isDelete && Trigger.isAfter){
        handler.OnAfterDelete(Trigger.oldMap);
        for(Purchase_Order__c pod : Trigger.old) {
            orderIds.add(pod.Order__c);
        }  
    }
    if(Trigger.isUpdate && Trigger.isAfter){
        handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        Purchase_Order__c oldpod;
        for(Purchase_Order__c po : Trigger.new) {
            oldpod = Trigger.oldMap.get(po.Id);
            if(po.RecordTypeId != costPoRecordTypeId){
                orderIds.add(po.Order__c);
            }
        }
        // orders = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
        System.enqueueJob(new DynamicRollUpQue(trigger.new));
        // orders = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
    }
    if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterInsert(Trigger.new);
        for(Purchase_Order__c pod : Trigger.new) {
            if(pod.RecordTypeId != costPoRecordTypeId){
                orderIds.add(pod.Order__c);
            }
        }
    }
    if(Trigger.isUnDelete && Trigger.isAfter){
        for(Purchase_Order__c pod : Trigger.new) {
            if(pod.RecordTypeId != costPoRecordTypeId){
                orderIds.add(pod.Order__c);
            }
        }
    }
    if(orderIds!=null && orderIds.size()>0) {
        try{
        PurchaseOrderTriggerHandler.updateEstimatedShipDateonOrder(orderIds,updateOrderMap);
        }
        catch(Exception e){
        // :: TO DO
        }
    }
    // Try - Catch to catch any dml errors doing the order rollup and displaying
  // errors on the payment records
  try { update orders;} 
  catch(System.DmlException e) {
    if (Trigger.isDelete) for (sObject obj : trigger.old) { obj.addError(e.getDmlMessage(0)); }
    else for (sObject obj : trigger.new) { obj.addError(e.getDmlMessage(0)); }
  }
}