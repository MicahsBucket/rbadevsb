/*******************************************************//**

@trigger OrderTrigger

@brief  trigger framework to secure order of operation

@author  Anthony Strafaccia (Slalom.ADS)

@version    2015-10/15  Slalom.ADS
    Created.
    Edited by Ian Fitzpatrick ianf@slalom.com 11/8/2017
    Edited by Mitchell Woloschek mwoloschek@penrodsoftware.com 3/16/2018

@see        OrderTriggerTest

@copyright  (c)2015 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/
///// DC MRJ 8/12/2020 ////
/// TURNED OFF A LOT OF UNUSED METHOD CALLS IN THE "RMS_WorkOrderCreationManager" CLASS AND////////
/////"RMS_backOfficeChecklistManager" CLASS AND "customerPickup" CLASS//////
//
//10/11/2020
//RBA mark.r commented out class. all trigger operations should be handled in OrderTriggerTwo. per Pavan Gunnas updates from 2019. 
//verified trigger is disabled in production.

trigger OrderTrigger on Order (before insert, before update, before delete, after insert, after undelete, after update, after delete) {
/*
    // Set the order trigger to ran
    // this gets checked in the RbAWorkOrderTrigger to prevent recursion when updating records
    UtilityMethods.setOrderTriggerRan();

    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Order')) return;
        RMS_financialTransactionManager financialTransactionManager = new RMS_financialTransactionManager();
        RMS_ServiceProductPickup customerPickup = new RMS_ServiceProductPickup();
        List<SObject> accounts = new List<SObject>();

        // Before Insert
        if(Trigger.isInsert && Trigger.isBefore){
            RMS_OrderTriggerHandler.OnBeforeInsert(Trigger.new);
        }
          if(Trigger.isUpdate && Trigger.isBefore ){
            UtilityMethods.checkLockedByStatus(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap, 'Order');
            RMS_OrderTriggerHandler.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        }


        // After Insert
    if(Trigger.isInsert && Trigger.isAfter){
        accounts = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
        System.debug(accounts);
        RMS_OrderTriggerHandler.OnAfterInsert(Trigger.new);
    }
      if(Trigger.isUpdate && Trigger.isAfter ){
        financialTransactionManager.onAfterUpdateOrder(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        accounts = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
        System.debug(accounts);
        RMS_OrderTriggerHandler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);

    }
    if(Trigger.isUpdate && Trigger.isAfter){
      
        // FSLRollupRolldownController.triggerFromOrder(Trigger.oldMap,Trigger.newMap,FSLRollupRolldownController.TriggerOperationEnum.IS_UPDATE);
    }

    if(Trigger.isAfter && Trigger.isDelete){
        accounts = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.old, false);
    }


        // After Undelete

    if(Trigger.isAfter && Trigger.isUnDelete){
            accounts = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
    }

        // Try - Catch to catch any dml errors doing the account rollup and displaying
        // errors on the order records
    try {
        update accounts;
    } catch(System.DmlException e) {
        if (Trigger.isDelete) for (sObject obj : trigger.old) { obj.addError(e.getDmlMessage(0)); }
            else for (sObject obj : trigger.new) { obj.addError(e.getDmlMessage(0)); }
    }
*/
}