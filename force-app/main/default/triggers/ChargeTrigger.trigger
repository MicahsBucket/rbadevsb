/*******************************************************//**

@trigger ChargeTrigger

@brief  trigger framework to secure order of operation

@author  Brianne Wilson (Slalom.BLW)

@version    2016-06-22  Slalom.BLW
Created.


@copyright  (c)2016 Slalom.  All Rights Reserved.
        Unauthorized use is prohibited.

***********************************************************/

trigger ChargeTrigger on Charge__c (before insert, before update, before delete, after insert, after undelete, after update, after delete) {
    
    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Charge__c')) return;    

    //HANDLERS AND MANAGERS        
    List<SObject> sproducts = new List<SObject>();
       
    // Before Insert
    /*
    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    /
    //  Before Update
    
    if(Trigger.isUpdate && Trigger.isBefore){
        
    }
      

    // Before Delete
    /*
    else if(Trigger.isDelete && Trigger.isBefore){
        
    }
    */
    
    // After Insert 
    //else 
    if(Trigger.isInsert && Trigger.isAfter){
    //  sproducts = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
     sproducts = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
        
    } 
    
    // After Update
    else if(Trigger.isUpdate && Trigger.isAfter){
        // sproducts = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
        sproducts = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
    }
                
    //After Delete
    else if(Trigger.isDelete && Trigger.isAfter){    
        // sproducts = (List<SObject>) dlrs.RollupService.rollup(trigger.old);
        sproducts = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.old, false);
    }
    
    // After Undelete 
    
    else if(Trigger.isUnDelete){
        
        // sproducts = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
        sproducts = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
    }
    
	// Try - Catch to catch any dml errors doing the sproduct rollup and displaying
	// errors on the charge records
	try { update sproducts;} 
	catch(System.DmlException e) {
		if (Trigger.isDelete) for (sObject obj : trigger.old) { obj.addError(e.getDmlMessage(0)); }
		else for (sObject obj : trigger.new) { obj.addError(e.getDmlMessage(0)); }
	}
}