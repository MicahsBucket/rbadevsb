trigger changeHistoryTrigger on Change_History__c (before insert, before update, before delete, after insert, after undelete, after update, after delete) {

    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Change_History__c')) return;

    Boolean isAsync = System.isBatch() || System.isFuture();

    //HANDLERS AND MANAGERS        
    List<SObject> orders = new List<SObject>();
       
    // Before Insert
    
    if(Trigger.isInsert && Trigger.isBefore){
        //handler.OnBeforeInsert(Trigger.new);
    }
    //  Before Update
    if(Trigger.isUpdate && Trigger.isBefore){
        
    }
      
    // Before Delete
    else if(Trigger.isDelete && Trigger.isBefore){

    }
    
    // After Insert 
    //else 
    if(Trigger.isInsert && Trigger.isAfter && !isAsync){
        // RMS_FutureRollups.rollupOrderItemsToOrders(trigger.newMap.keySet());
        List<OrderItem> orderItemRecords = new List<OrderItem>();
        orderItemRecords = [SELECT Id, OrderId, Status__c, Product_Family__c, Product_Type__c, Quantity, Total_Retail_Price__c, Total_Wholesale_Cost__c, Purchase_Order__c from OrderItem where Id IN: trigger.newMap.keySet()];
        // orderRecords =  dlrs.RollupService.rollup(orderItemRecords);
        System.enqueueJob(new DynamicRollUpQue(orderItemRecords));
    } 
    
    // After Update
    else if(Trigger.isUpdate && Trigger.isAfter && !isAsync){
    //    RMS_FutureRollups.rollupOrderItemsToOrders(trigger.newMap.keySet());
       List<OrderItem> orderItemRecords = new List<OrderItem>();
        orderItemRecords = [SELECT Id, OrderId, Status__c, Product_Family__c, Product_Type__c, Quantity, Total_Retail_Price__c, Total_Wholesale_Cost__c, Purchase_Order__c from OrderItem where Id IN: trigger.newMap.keySet()];
        // orderRecords =  dlrs.RollupService.rollup(orderItemRecords);
        System.enqueueJob(new DynamicRollUpQue(orderItemRecords));
    }
                
    //After Delete
    else if(Trigger.isDelete && Trigger.isAfter && !isAsync){    
    //    RMS_FutureRollups.rollupOrderItemsToOrders(trigger.oldMap.keySet());
       List<OrderItem> orderItemRecords = new List<OrderItem>();
        orderItemRecords = [SELECT Id, OrderId, Status__c, Product_Family__c, Product_Type__c, Quantity, Total_Retail_Price__c, Total_Wholesale_Cost__c, Purchase_Order__c from OrderItem where Id IN: trigger.oldMap.keySet()];
        // orderRecords =  dlrs.RollupService.rollup(orderItemRecords);
        System.enqueueJob(new DynamicRollUpQue(orderItemRecords));
    }
    
    // After Undelete 
    
    else if(Trigger.isUnDelete && !isAsync){
        
    //    RMS_FutureRollups.rollupOrderItemsToOrders(trigger.newMap.keySet());
       List<OrderItem> orderItemRecords = new List<OrderItem>();
        orderItemRecords = [SELECT Id, OrderId, Status__c, Product_Family__c, Product_Type__c, Quantity, Total_Retail_Price__c, Total_Wholesale_Cost__c, Purchase_Order__c from OrderItem where Id IN: trigger.newMap.keySet()];
        // orderRecords =  dlrs.RollupService.rollup(orderItemRecords);
        System.enqueueJob(new DynamicRollUpQue(orderItemRecords));
    }
    
    // Try - Catch to catch any dml errors doing the sproduct rollup and displaying
    // errors on the charge records
    try { update orders;} 
    catch(System.DmlException e) {
        if (Trigger.isDelete) for (sObject obj : trigger.old) { obj.addError(e.getDmlMessage(0)); }
        else for (sObject obj : trigger.new) { obj.addError(e.getDmlMessage(0)); }
    }
}