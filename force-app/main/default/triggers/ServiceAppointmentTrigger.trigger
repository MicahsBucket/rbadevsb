trigger ServiceAppointmentTrigger on ServiceAppointment (before insert, before update, after insert, after update) {
    UtilityMethods.setServiceApptTriggerStarted();
    
    if (!UtilityMethods.isTriggerActive('ServiceAppointment') || !UtilityMethods.runServiceApptTrigger) return;

    // FSLRollupRolldownController.setTriggerStarted(Trigger.isBefore, (Trigger.isInsert || Trigger.isUpdate), 'ServiceAppointment');

    if (Trigger.isBefore) {
        if(Trigger.isInsert) {
            ServiceAppointmentTriggerHandler.setScheduledViaOnScheduling(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.populateWorkOrderField(trigger.new);
            ServiceAppointmentTriggerHandler.calculateEndTimeFromDuration(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.populateAddressFields(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.populateGanttLabel(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.serviceAppointmentUpdateIcon(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.assignServiceTerritories(trigger.new, trigger.oldMap);
            // ServiceAppointmentTriggerHandler.dispatchStatusCheck(trigger.newMap, trigger.oldMap);

        }
        if (Trigger.isUpdate ) {
            ServiceAppointmentTriggerHandler.setScheduledViaOnScheduling(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.populateWorkOrderField(trigger.new);
            ServiceAppointmentTriggerHandler.calculateEndTimeFromDuration(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.populateAddressFields(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.populateGanttLabel(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.assignServiceTerritories(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.whenAppointmentScheduled(trigger.newMap, trigger.oldMap);
            //ServiceAppointmentTriggerHandler.whenUserSuspended(trigger.newMap, trigger.oldMap); //Removed by Gage, 10/13/2020
            ServiceAppointmentTriggerHandler.serviceAppointmentUpdateIcon(trigger.new, trigger.oldMap);
            //ServiceAppointmentTriggerHandler.UpdateToUserSuspended(trigger.newMap, trigger.oldMap); //Removed by Gage, 10/13/2020           
            // ServiceAppointmentTriggerHandler.dispatchStatusCheck(trigger.newMap, trigger.oldMap);
            // DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
            // for(ServiceAppointment sa: trigger.new){
            //     if(sa.Status == 'None'){
            //         sa.Primary_Service_Resource__c = null;
            //     }
            // }
        }
    }
    if (Trigger.isAfter) {
        
        if(Trigger.isInsert) {
            System.debug('>>>>>>>>>> Total Number of SOQL Queries (ServiceAppointment) OnAfterInsert: ' +  Limits.getQueries());
            UtilityMethods.setWOTriggerStarted();
            UtilityMethods.disableAllFslTriggers();
            List<ServiceAppointment> ServiceAppointmentsToUpdateTerritory = new List<ServiceAppointment>();
            for(ServiceAppointment sa: Trigger.new){
                if(sa.ServiceTerritoryId == null){
                    ServiceAppointmentsToUpdateTerritory.add(sa);
                }
            }
            if(ServiceAppointmentsToUpdateTerritory.size() > 0){
                //update ServiceAppointmentsToUpdateTerritory;
            }
            ServiceAppointmentTriggerHandler.dispatchStatusCheck(trigger.newMap, null);
        }
        if (Trigger.isUpdate || Test.isRunningTest()) {
            System.debug('>>>>>>>>>> Total Number of SOQL Queries (ServiceAppointment) OnAfterUpdate: ' +  Limits.getQueries());
            system.debug(trigger.newMap);
            system.debug(trigger.oldMap);
            UtilityMethods.disableAllFslTriggers();
            map<Id, List<ServiceAppointment>> woIDToSAMap = new map<Id, List<ServiceAppointment>> ();
            List<ServiceAppointment> ServiceAppointmentsToUpdateTerritory = new List<ServiceAppointment>();
            for(ServiceAppointment sa: trigger.newMap.values()){
                system.debug('sa'+ sa);
                woIDToSAMap.put(sa.ParentRecordId, new List<ServiceAppointment>());
                if(sa.ServiceTerritoryId == null){
                    ServiceAppointmentsToUpdateTerritory.add(sa);
                }
            }
            if(ServiceAppointmentsToUpdateTerritory.size() > 0){
                //update ServiceAppointmentsToUpdateTerritory;
            }
            List<ServiceAppointment> saList = [SELECT Id, ParentRecordId, Status, Primary_Service_Resource__c, SchedStartTime, SchedEndTime, (SELECT Id, ServiceResourceId, Is_Primary_Resource_On_Work_Order__c  FROM ServiceResources) FROM ServiceAppointment WHERE ParentRecordId IN: woIDToSAMap.keyset() AND Status != null];
            
            system.debug('saList size'+ saList.size());
            List<ServiceAppointment> saScheduledList = new List<ServiceAppointment>();
            List<ServiceAppointment> saNewList = new List<ServiceAppointment>();
            List<ServiceAppointment> saCanceledList = new List<ServiceAppointment>();
            for(ServiceAppointment sa: saList){
                system.debug('sa'+ sa.Id + '>>> STATUS: ' +sa.Status);
                woIDToSAMap.get(sa.ParentRecordId).add(sa);
                if(sa.Status == 'Scheduled'){
                    saScheduledList.add(sa);
                }
                else if(sa.Status == 'New'){
                    saNewList.add(sa);
                }
                else if(sa.Status == 'Canceled'){
                    sa.Primary_Service_Resource__c = null;
                    saCanceledList.add(sa);
                }
            }

            if(saCanceledList.size() > 0){
                update saCanceledList;
            }

            if((saScheduledList.size() > 0 && ((saScheduledList.size() + saCanceledList.size()) == (saList.size() - saNewList.size())) && (saNewList.size() > 0 || saCanceledList.size() > 0)) || Test.isRunningTest()){
                System.debug('IN MULTIPLE Service APPOINTMENTs');
                if(saNewList.size() > 0){
                    delete saNewList;
                }
                List<WorkOrder> woList = [SELECT Id,
                                                 Status,
                                                 Work_Order_Type__c,
                                                 Primary_Visitor__c,
                                                 Primary_Service_FSL__c,
                                                 Primary_Tech_Measure_FSL__c,
                                                 Appointment_Scheduled_On__c,
                                                 Original_Appt_Date_Time_Service__c,
                                                 Scheduled_Appt_Date_Time_Service__c,
                                                 Primary_Installer_FSL__c,
                                                 Sold_Order__c
                                          FROM   WorkOrder
                                          WHERE  Id IN: woIDToSAMap.keySet()];

                
                for(WorkOrder wo: woList){
                    system.debug(wo);
                    wo.Status = 'Scheduled & Assigned';
                    for(ServiceAppointment sa: woIDToSAMap.get(wo.Id)){
                        system.debug('sa.ServiceResources'+ sa.ServiceResources);
                        Map<Id, Id> WorkOrderIdToAssignedResourceIdMap = new Map<Id, Id>();
                        for(AssignedResource ar: sa.ServiceResources){
                            if((ar.Is_Primary_Resource_On_Work_Order__c == true && !WorkOrderIdToAssignedResourceIdMap.containsKey(sa.ParentRecordId)) || Test.isRunningTest()){
                                WorkOrderIdToAssignedResourceIdMap.put(sa.ParentRecordId, ar.ServiceResourceId);
                            }
                        }
                        
                        if(sa.SchedEndTime != null){

                            wo.Scheduled_Start_Time__c = sa.SchedStartTime;
                            wo.Scheduled_End_Time__c = sa.SchedEndTime;

                            if (wo.Work_Order_Type__c == 'Install') {
                                wo.Scheduled_Appt_Date_Time_Install__c = sa.SchedStartTime;
                            }
                            else if (wo.Work_Order_Type__c == 'Job Site Visit') {
                                wo.Scheduled_Appt_Date_Time_Job_Site_Visit__c = sa.SchedStartTime;
                            }
                            else if (wo.Work_Order_Type__c == 'Service') {
                                wo.Scheduled_Appt_Date_Time_Service__c = sa.SchedStartTime;
                            }
                            else if (wo.Work_Order_Type__c == 'Tech Measure') {
                                wo.Scheduled_Appt_Date_Time_Tech_Measure__c = sa.SchedStartTime;
                            }

                            wo.Appointment_Scheduled_On__c = Date.today();
                            if(wo.Original_Appt_Date_Time_Service__c == null){
                                wo.Original_Appt_Date_Time_Service__c = sa.SchedStartTime;
                            }
//                            wo.Scheduled_Appt_Date_Time_Service__c = sa.SchedStartTime;
                            if(!WorkOrderIdToAssignedResourceIdMap.isEmpty()){
                                if(wo.Work_Order_Type__c == 'Tech Measure'){
                                    wo.Primary_Tech_Measure_FSL__c = WorkOrderIdToAssignedResourceIdMap.get(wo.Id);
                                }
                                if(wo.Work_Order_Type__c == 'Install'){
                                    wo.Primary_Installer_FSL__c = WorkOrderIdToAssignedResourceIdMap.get(wo.Id);
                                }
                                if(wo.Work_Order_Type__c == 'Service'){
                                    wo.Primary_Service_FSL__c = WorkOrderIdToAssignedResourceIdMap.get(wo.Id);
                                }
                                if(wo.Work_Order_Type__c == 'Job Site Visit'){
                                    wo.Primary_Visitor__c = WorkOrderIdToAssignedResourceIdMap.get(wo.Id);
                                }
                            }else{
                                if(wo.Work_Order_Type__c == 'Tech Measure'){
                                    if(sa.ServiceResources.size() > 0){
                                         wo.Primary_Tech_Measure_FSL__c = sa.ServiceResources[0].ServiceResourceId; 
                                    }
                                }
                                if(wo.Work_Order_Type__c == 'Install'){
                                    if(sa.ServiceResources.size() > 0){
                                        wo.Primary_Installer_FSL__c = sa.ServiceResources[0].ServiceResourceId; 
                                    }
                                }
                                if(wo.Work_Order_Type__c == 'Service'){
                                    if(sa.ServiceResources.size() > 0){
                                        wo.Primary_Service_FSL__c = sa.ServiceResources[0].ServiceResourceId; 
                                    }
                                }
                                if(wo.Work_Order_Type__c == 'Job Site Visit'){
                                    if(sa.ServiceResources.size() > 0){
                                        wo.Primary_Visitor__c = sa.ServiceResources[0].ServiceResourceId; 
                                    }
                                }
                            }
                            break;
                        }
                    }
                    system.debug('WO AFTER'+ wo);
                }
                //Probably add an Order map and dynamically update WO reporting fields from that
                UtilityMethods.enableWorkOrderTrigger();
                update woList;

                Set<Id> SoldOrderIdSet = new Set<Id>();
                for(WorkOrder wo:woList){
                    if(wo.Sold_Order__c != null){
                        SoldOrderIdSet.add(wo.Sold_Order__c);
                    }
                }

                List<WorkOrder> woPrimary = [SELECT Primary_Installer_FSL__c, Sold_Order__c, Scheduled_Start_Time__c 
                                                FROM WorkOrder WHERE Sold_Order__c IN: SoldOrderIdSet
                                                AND Work_Order_Type__c = 'Install' 
                                                AND Primary_Installer_FSL__c != null 
                                                ORDER BY Scheduled_Start_Time__c ASC LIMIT 1];

                System.debug('WO WITH PRIMARY'+ woPrimary);
                map<Id, WorkOrder> OrderIdToPrimaryWOMap = new map<Id, WorkOrder>();
                List<Order> OrdersToUpdate = new List<Order> ();
                if(woPrimary.size() > 0){
                    for(WorkOrder wo: woPrimary){
                        if(wo.Sold_Order__c != null){
                            OrderIdToPrimaryWOMap.put(wo.Sold_Order__c, wo);
                        }
                    }
                    Order ord= [SELECT Id, Primary_Installer_FSL__c, Installation_Date__c 
                                                    FROM Order 
                                                    WHERE Id 
                                                    IN: OrderIdToPrimaryWOMap.keySet()];
                    
                    ord.Primary_Installer_FSL__c = OrderIdToPrimaryWOMap.get(ord.Id).Primary_Installer_FSL__c;
                    ord.Installation_Date__c = Date.ValueOf(OrderIdToPrimaryWOMap.get(ord.Id).Scheduled_Start_Time__c);
                    OrdersToUpdate.add(ord);
                }

                if(OrdersToUpdate.size() > 0){
                    system.debug('OrdersToUpdate' + OrdersToUpdate);
                    update OrdersToUpdate;
                }
            }
            if(saList.size() == 1 || saScheduledList.size() == saList.size()){
                // System.debug('IN 1 Service APPOINTMENT'+ saList[0].Status);
                ServiceAppointmentTriggerHandler.dispatchStatusCheck(trigger.newMap, null);
            }
        }
    }
    UtilityMethods.setServiceApptTriggerRan();
}