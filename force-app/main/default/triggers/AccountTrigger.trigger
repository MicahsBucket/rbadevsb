/*******************************************************//**

@trigger AccountTrigger

@brief  trigger framework to secure order of operation

@author  Anthony Strafaccia (Slalom.ADS)

@version    2015-10/15  Slalom.ADS
    Created.

@see        AccountTriggerTest
 
@copyright  (c)2015 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/

trigger AccountTrigger on Account (before insert, before update, before delete, 
                                            after insert, after undelete, after update, after delete) {
    
    // Run for all profiles
    // Before Insert
    if(Trigger.isInsert && Trigger.isBefore){
        RMS_MunicipalityMatchManager municipalityMatchManager = new RMS_MunicipalityMatchManager(); 
        municipalityMatchManager.matchBuildingPermitMunicipality(Trigger.New);
    }
 
    if (!UtilityMethods.isTriggerActive('Account')) return;
    // FSLRollupRolldownController.setTriggerStarted(Trigger.isBefore, (Trigger.isUpdate), 'Account');

    //HANDLERS AND MANAGERS
    RMS_reactivateResource reactivateResource = new RMS_reactivateResource();
    RMS_AccountTriggerHandler handler = new RMS_AccountTriggerHandler();
    
    // Before 
    if (Trigger.isBefore){
        if (Trigger.isInsert) {
            //handler.OnBeforeInsert(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap); 
        }
        if (Trigger.isUpdate) {
            // handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap); 
        }
        if (Trigger.isDelete) {
            // handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
        }
    }
    
    // After 
    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            handler.OnAfterInsert(Trigger.new);
        }
        if (Trigger.isUpdate) {
            reactivateResource.reactivateResource(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);              
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap, Trigger.new);
            System.enqueueJob(new DynamicRollUpQue(Trigger.new));
            // FSLRollupRolldownController.triggerFromAccount(Trigger.oldMap,Trigger.newMap,FSLRollupRolldownController.TriggerOperationEnum.IS_UPDATE);
        }
        if (Trigger.isDelete) {
            // handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
        }
        if (Trigger.isUnDelete) {
            // handler.OnUndelete(Trigger.new);
        }
    }
}