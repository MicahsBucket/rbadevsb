trigger LeadTimeTrigger on Lead_Time__c (before insert) {

    
     if(Trigger.isBefore && Trigger.isInsert)
    {  
        
       for(Lead_Time__c leadtime :Trigger.New)
       {    
          Datetime myDT = Datetime.now();
          leadtime.name=String.valueOf(myDT);

       }
        
     LeadTimeTriggerHandler.makeLeadTimeInactive(); 
        
    }
    
     if(Trigger.isAfter && Trigger.isUpdate)
    {
       // LeadTimeTriggerHandler.cloneLeadTimeRecord(Trigger.New);
    }
}