/*******************************************************//**

@trigger OrderTrigger

@brief  trigger framework to secure order of operation

@author  Anthony Strafaccia (Slalom.ADS)

@version    2015-10/15  Slalom.ADS
    Created.
    Edited by Ian Fitzpatrick ianf@slalom.com 11/8/2017


@see        OrderTriggerTest

@copyright  (c)2015 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/

trigger RbAWorkOrderTrigger on RbA_Work_Order__c (after delete, after insert, after undelete, 
                                                    after update, before delete, before insert, before update) {
/*    RbA_Work_Order__c obsolete with FSL.

    //GET ALL RMS SETTINGS CUSTOM SETTINGS
    map<String, RMS_Settings__c> RMS_Settings_map = RMS_Settings__c.getAll(); 
    
    //CHECK IF DATA LOADING PROFILE 
    if(RMS_Settings_map.get('Data Loading Profile ID') == null ){
        if(Trigger.isDelete){
            Trigger.old[0].addError(RMS_ErrorMessages.DATA_LOADING_CUSTOM_SETTING_REQUIRED);
        }else{
            Trigger.new[0].addError(RMS_ErrorMessages.DATA_LOADING_CUSTOM_SETTING_REQUIRED);
        }
        return;
    }
    //IF NOT DATA LOADING PROFILE RUN LOGIC
    if((UserInfo.getProfileId() == RMS_Settings_map.get('Data Loading Profile ID').Value__c ) ){
        return;
    }
        
    //HANDLERS AND MANAGERS
    RMS_addApplicationNotes applicationNotes = new RMS_addApplicationNotes();
    //commenting out to comply with FSL changes
    //RMS_addPaintStainOrderItemNotestoWO addPaintStainNotestoWO = new RMS_addPaintStainOrderItemNotestoWO(); 
    RMS_WorkOrderTriggerHandler workOrderHandler = new RMS_WorkOrderTriggerHandler();
    List<SObject> orders = new List<SObject>();
   
    // Before Insert
    
    if(Trigger.isInsert && Trigger.isBefore){
        system.debug('****Work Order Is Insert Is Before');
        applicationNotes.populateApplicationNotesCreate(Trigger.new, Trigger.newMap); 
        //commenting out to comply with FSL changes
        //addPaintStainNotestoWO.addPaintStainNotesWO(Trigger.new, Trigger.newMap);  
    }
    
    
    //  Before Update
    if(Trigger.isUpdate && Trigger.isBefore){
        system.debug('****Work Order Is Update Is Before');
        RMS_WorkOrderCreationManager.createInstallWorkOrderOnTechMeasureComplete(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        applicationNotes.populateApplicationNotesUpdate(Trigger.old,Trigger.new,Trigger.oldMap,Trigger.newMap);
    }
         
        // After Insert 
    if(Trigger.isInsert && Trigger.isAfter){
        system.debug('****Work Order Is Insert Is After');
            // run the order rollup real-time if the order trigger
            // hasn't been run yet, otherwise run it @future         
        RMS_addSkilltoWO.addSkilltoWO(Trigger.new, Trigger.newMap);  
        //IF I don't like this but I'm not sure how to replace yet.       
        if (UtilityMethods.hasOrderTriggerRan()){
            RMS_FutureRollups.rollupWorkOrdersToOrders(trigger.newMap.keySet());
        }
        else {
                orders = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
        } 
     
    }

    // After Update
    if(Trigger.isUpdate && Trigger.isAfter){
        system.debug('****Work Order Is Update Is After');
        // run the order rollup real-time if the order trigger
        // hasn't been run yet, otherwise run it @future            
        RMS_cancelWOManager.closeEvent(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        RMS_addSkilltoWO.addSkillonUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        workOrderHandler.updateOrderAfterWorkOrderComplete(Trigger.new, Trigger.newMap);
        
        //IF I don't like this but I'm not sure how to replace yet.
        if (UtilityMethods.hasOrderTriggerRan()){
            RMS_FutureRollups.rollupWorkOrdersToOrders(trigger.newMap.keySet());
        }
        else {
            orders = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
        }
    }
    
    // return before updating if the order trigger has alreay been run.           
    if (UtilityMethods.hasOrderTriggerRan()) return;

    // Try - Catch to catch any dml errors doing the order  rollup and displaying
    // errors on the rba work orders
    try { 
        system.debug('****Work Order Update Orders');
        update orders;
    } catch(System.DmlException e) {
        if (Trigger.isDelete){
            for (sObject obj : trigger.old) {
                obj.addError(e.getDmlMessage(0)); 
            }
        } else {
            for (sObject obj : trigger.new){
                obj.addError(e.getDmlMessage(0)); 
            }
        }
    }
    */
}