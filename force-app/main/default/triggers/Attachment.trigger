trigger Attachment on Attachment (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	
	// Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Attachment')) return;

	if (Trigger.isBefore && Trigger.isInsert) {
		//call your handler.before method
		RMS_AttachmentHandler.onBeforeInsert(Trigger.new);
	
	} else if (Trigger.isAfter) {
		//call handler.after method
	
	}
}