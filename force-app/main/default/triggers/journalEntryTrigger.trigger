/*******************************************************//**

@trigger journalEntryTrigger


@copyright  (c)2016 Slalom.  All Rights Reserved.
Unauthorized use is prohibited.

***********************************************************/

trigger journalEntryTrigger on Journal_Entry__c (after delete, after insert, after undelete,after update, before delete, before insert, before update) {
    
    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Journal_Entry__c')) return;

    //HANDLERS AND MANAGERS		
    //List<SObject> assets = new List<SObject>();
    
    if(Trigger.isInsert && Trigger.isAfter){
        
    //    assets = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
    } 
    
    // After Update
    else if(Trigger.isUpdate && Trigger.isAfter){
        
    //    assets = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
    }
    
    
    //After Delete
    else if(Trigger.isDelete && Trigger.isAfter){
        
    //    assets = (List<SObject>) dlrs.RollupService.rollup(trigger.old);
    }
    // After Undelete 
    else if(Trigger.isUnDelete){
        
    //    assets = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
    }
    
    /*try { 
    } 
    catch(System.DmlException e) {
        if (Trigger.isDelete) for (sObject obj : trigger.old) { obj.addError(e.getDmlMessage(0)); }
        else for (sObject obj : trigger.new) { obj.addError(e.getDmlMessage(0)); }
    }*/
}