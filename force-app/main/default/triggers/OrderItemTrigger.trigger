/*******************************************************/
/**
    
    @trigger OrderItemTrigger
    
    @brief  trigger framework to secure order of operation
    
    @author  Anthony Strafaccia (Slalom.ADS)
    
    @version    2016-3/3  Slalom.ADS
    Created.
    
    @see        OrderItemTriggerTest
    
    @copyright  (c)2016 Slalom.  All Rights Reserved.
    Unauthorized use is prohibited.
    
    ***********************************************************/
trigger OrderItemTrigger on OrderItem(after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('OrderItem')) return;

    //HANDLERS AND MANAGERS
    RMS_OrderItemManager orderItemManager = new RMS_OrderItemManager();
    RMS_createCharges createCharges = new RMS_createCharges();
    List <SObject> orderItems = new List <SObject> ();

    // Before Insert
    if(Trigger.isInsert && Trigger.isBefore){
//          orderItemManager.linkNewChildProducts(Trigger.new);
    } 
    //  Before Update
    else  if (Trigger.isUpdate && Trigger.isBefore) {

    }
    // Before Delete
    /*
    else if(Trigger.isDelete && Trigger.isBefore){
        UtilityMethods.checkLocked(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap, 'Delete');
        financialTransactionManager.onBeforeDeletePayment(Trigger.old, Trigger.oldMap);
    }
    */
    // After Insert
    else if (Trigger.isInsert && Trigger.isAfter) {
        orderItemManager.setUpChangeHistoryOnCreate(Trigger.new, Trigger.newMap);
        createCharges.createCharge(Trigger.new, Trigger.newMap);
        // RMS_FutureRollups.rollupOrderItemsToOrders(trigger.newMap.keySet());
        List<OrderItem> orderItemRecords = new List<OrderItem>();
        orderItemRecords = [SELECT Id, OrderId, Status__c, Product_Family__c, Product_Type__c, Quantity, Total_Retail_Price__c, Total_Wholesale_Cost__c, Purchase_Order__c from OrderItem where Id IN: trigger.newMap.keySet()];
        // orderRecords =  dlrs.RollupService.rollup(orderItemRecords);
        System.enqueueJob(new DynamicRollUpQue(orderItemRecords));
    }
        
    // After Update
    else if(Trigger.isUpdate && Trigger.isAfter){
        Set<Id> oldAndNewTriggerIds = new Set<Id>();
        oldAndNewTriggerIds.addAll(trigger.newMap.keySet());
        oldAndNewTriggerIds.addAll(trigger.oldMap.keySet());    
        // RMS_FutureRollups.rollupOrderItemsToOrders(oldAndNewTriggerIds);
        List<OrderItem> orderItemRecords = new List<OrderItem>();
        orderItemRecords = [SELECT Id, OrderId, Status__c, Product_Family__c, Product_Type__c, Quantity, Total_Retail_Price__c, Total_Wholesale_Cost__c, Purchase_Order__c from OrderItem where Id IN: oldAndNewTriggerIds];
        // orderRecords =  dlrs.RollupService.rollup(orderItemRecords);
        System.enqueueJob(new DynamicRollUpQue(orderItemRecords));
        orderItemManager.updateChangeHistoryOnUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
         set<Id> OrderIdSet = new set<Id>();
         map<Id, Purchase_Order__c> poMap = new map<Id, Purchase_Order__c>();
         for(OrderItem oi: Trigger.New){
            if(oi.Status__c == 'Cancelled'){
                OrderIdSet.add(oi.OrderId);
            }
         }
         List<Purchase_Order__c> poList = [SELECT Id, Order__c, Status__c FROM Purchase_Order__c WHERE Order__c =: OrderIdSet];
         if(poList.size() > 0 ){
            for(Purchase_Order__c po: poList){
                if(!poMap.containsKey(po.Id) ){
                    poMap.put(po.Id, po);
                }
            }
         }
         
        if(!poMap.isEmpty()){
            StatusCheckUtility.PurchaseOrderToOrderStatusCheck(poMap, null);
        }
    }
    //After Delete
    else if(Trigger.isDelete && Trigger.isAfter){
        // RMS_FutureRollups.rollupOrderItemsToOrders(trigger.oldMap.keySet());
        List<OrderItem> orderItemRecords = new List<OrderItem>();
        orderItemRecords = [SELECT Id, OrderId, Status__c, Product_Family__c, Product_Type__c, Quantity, Total_Retail_Price__c, Total_Wholesale_Cost__c, Purchase_Order__c from OrderItem where Id IN: trigger.oldMap.keySet()];
        // orderRecords =  dlrs.RollupService.rollup(orderItemRecords);
        System.enqueueJob(new DynamicRollUpQue(orderItemRecords));
    }
    // After Undelete 
    else if(Trigger.isUnDelete){
        // RMS_FutureRollups.rollupOrderItemsToOrders(trigger.newMap.keySet());
        List<OrderItem> orderItemRecords = new List<OrderItem>();
        orderItemRecords = [SELECT Id, OrderId, Status__c, Product_Family__c, Product_Type__c, Quantity, Total_Retail_Price__c, Total_Wholesale_Cost__c, Purchase_Order__c from OrderItem where Id IN: trigger.newMap.keySet()];
        // orderRecords =  dlrs.RollupService.rollup(orderItemRecords);
        System.enqueueJob(new DynamicRollUpQue(orderItemRecords));
    }    
    // Try - Catch to catch any dml errors doing the order rollup and displaying
    // errors on the order item records
    try { update orderItems;} 
    catch(System.DmlException e) {
        if (Trigger.isDelete) for (sObject obj : trigger.old) { obj.addError(e.getDmlMessage(0)); }
        else for (sObject obj : trigger.new) { obj.addError(e.getDmlMessage(0)); }
    }
}