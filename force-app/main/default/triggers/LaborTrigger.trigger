/*******************************************************//**

@trigger LaborTrigger

@brief  trigger framework to secure order of operation

@author  Anthony Strafaccia (Slalom.ADS)

@version    2016-1/7  Slalom.ADS
    Created.

@see        LaborTriggerTest

@copyright  (c)2016 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/

trigger LaborTrigger on Labor__c (after delete, after insert, after undelete,
                                    after update, before delete, before insert, before update) {

    if (!UtilityMethods.isTriggerActive('Labor__c')) return;
    //HANDLERS AND MANAGERS
    LaborTriggerHandler handler = new LaborTriggerHandler(Trigger.isExecuting,Trigger.size);
    RMS_financialTransactionManager financialTransactionManager = new RMS_financialTransactionManager();
    List<SObject> wkorders = new List<SObject>();

    // Before Insert

    if(Trigger.isInsert && Trigger.isBefore){
        handler.onBeforeInsert(Trigger.new);
    }

    //  Before Update
    /*
    if(Trigger.isUpdate && Trigger.isBefore){
        financialTransactionManager.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }
        */
    if(Trigger.isUpdate && Trigger.isBefore){
        Boolean lockPrev = false;
        For (Labor__c labor : Trigger.old){
            if(lockPrev == false){
                lockPrev = labor.Locked__c;
            }
        }
        if(lockPrev){
            UtilityMethods.checkLocked(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap, 'Update');
        }
    }

    // Before Delete
    //else
    if(Trigger.isDelete && Trigger.isBefore){
        UtilityMethods.checkLocked(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap, 'Delete');

        financialTransactionManager.onBeforeDeleteLabor(Trigger.old, Trigger.oldMap);
    }

    // After Insert
    else if(Trigger.isInsert && Trigger.isAfter){
        financialTransactionManager.onAfterInsertLabor(Trigger.new, Trigger.newMap);
        // wkorders = (List<SObject>) dlrs.RollupService.rollup(Trigger.new);
        wkorders = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
        System.debug('Trigger.isInsert && Trigger.isAfter LABOR TRIGGER: '+ wkorders);
        // System.enqueueJob(new DynamicRollUpQue(wkorders));
    }

    // After Update
    else if(Trigger.isUpdate && Trigger.isAfter){
        financialTransactionManager.onAfterUpdateLabor(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        // wkorders = (List<SObject>) dlrs.RollupService.rollup(Trigger.new);
        wkorders = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
        // System.enqueueJob(new DynamicRollUpQue(wkorders));
    }


    //After Delete

    else if(Trigger.isDelete && Trigger.isAfter){
        // wkorders = (List<SObject>) dlrs.RollupService.rollup(Trigger.old);
        // wkorders = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.old, false);
        Map<Id, List<WorkOrder>> relatedWOMap = new Map<Id, List<WorkOrder>>();
        for(Labor__c l: trigger.old){
            if(l.Related_FSL_Work_Order__c != null){
                relatedWOMap.put(l.Related_FSL_Work_Order__c, new List<WorkOrder>());
            }
        }

        if(relatedWOMap.size() > 0){
            List<WorkOrder> WOsToUpdate = new List<WorkOrder>();
            List<Order> ordersToUpdate = new List<Order>();
            List<Labor__c> relatedLaborList = [SELECT Id, Related_FSL_Work_Order__c FROM Labor__c WHERE Related_FSL_Work_Order__c IN: relatedWOMap.keySet()];
            if(relatedLaborList.size() > 0 ){
                wkorders = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(relatedLaborList, false);
            }else{
                List<WorkOrder> woList = [SELECT Id, Total_Labor_Cost__c, Sold_Order__c FROM WorkOrder WHERE Id IN: relatedWOMap.keySet()];
                Set<Id> orderIdSet = new Set<Id>();
                for(WorkOrder wo: [SELECT Id, Total_Labor_Cost__c, Sold_Order__c FROM WorkOrder WHERE Id IN: relatedWOMap.keySet()]){
                    wo.Total_Labor_Cost__c = null;
                    if(wo.Sold_Order__c != null){
                        orderIdSet.add(wo.Sold_Order__c);
                    }
                    WOsToUpdate.add(wo);
                }
                for(Order o: [SELECT Id, Labor__c FROM Order WHERE Id IN: orderIdSet]){
                    o.Labor__c = null;
                    ordersToUpdate.add(o);
                }
            }
            if(WOsToUpdate.size() > 0){
                update WOsToUpdate;
            }
            if(ordersToUpdate.size() > 0){
                update ordersToUpdate;
            }
        }
        
    }


    // After Undelete

    else if(Trigger.isUnDelete){
        // wkorders = (List<SObject>) dlrs.RollupService.rollup(Trigger.new);
        wkorders = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
    }

    // Try - Catch to catch any dml errors doing the work order rollup and displaying
    // errors on the labor records
    try {

        update wkorders;
        System.enqueueJob(new DynamicRollUpQue(wkorders));
    } catch(System.DmlException e) {
        if (Trigger.isDelete) for (sObject obj : Trigger.old) {
            obj.addError(e.getDmlMessage(0));
        }
        else for (sObject obj : Trigger.new) {
            obj.addError(e.getDmlMessage(0));
        }
    }
}