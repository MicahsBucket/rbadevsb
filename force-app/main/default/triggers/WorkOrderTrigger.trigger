trigger WorkOrderTrigger on WorkOrder (before delete, before insert, before update, after delete, after insert, after undelete, after update) {

    UtilityMethods.setWOTriggerStarted();

    if (!UtilityMethods.isTriggerActive('WorkOrder')) return;


    List<SObject> orders = new List<SObject>();

    if (Trigger.isBefore) {
        if (Trigger.isInsert){
            System.debug('>>>>>>>>>> Total Number of SOQL Queries (Workorder) OnBeforeInsert: ' +  Limits.getQueries());
            RMS_addPaintStainOrderItemNotestoWO addPaintStainNotestoWO = new RMS_addPaintStainOrderItemNotestoWO();
            addPaintStainNotestoWO.addPaintStainNotesWO(Trigger.new);
            WorkOrderTriggerHandler.setduration(Trigger.new);
            WorkOrderTriggerHandler.updateMunicipalityFields(Trigger.new);
            WorkOrderTriggerHandler.updateWorkOrderFromOrderOnInsert(Trigger.new);
            WorkOrderTriggerHandler.setWorkTypeOnWorkOrder(Trigger.new);
            WorkOrderTriggerHandler.assignServiceTerritories(trigger.new, null);
            WorkOrderTriggerHandler.setOperatingHours(Trigger.new);
            WorkOrderTriggerHandler.updateWorkOrderFromAccount(Trigger.new);
            
        }
        if (Trigger.isUpdate) {
            System.debug('>>>>>>>>>> Total Number of SOQL Queries (Workorder) OnBeforeUpdate: ' +  Limits.getQueries());
            
            WorkOrderTriggerHandler.clearFieldsOnCancelation(Trigger.new, Trigger.oldMap);
            WorkOrderTriggerHandler.clearFieldsOnUnscheduling(Trigger.new, Trigger.oldMap);  
            WorkOrderTriggerHandler.workOrderRecordTypeChange(Trigger.new, Trigger.oldMap);
            WorkOrderTriggerHandler.updateWorkOrderTypes(Trigger.newMap);
            WorkOrderTriggerHandler.updateMunicipalityFields(Trigger.new);
            //RMS_WorkOrderCreationManager.createInstallWorkOrderOnTechMeasureComplete(Trigger.oldMap, Trigger.newMap);
            WorkOrderTriggerHandler.updateWorkOrderFromOrderOnUpdate(Trigger.new, Trigger.oldMap);
            WorkOrderTriggerHandler.assignServiceTerritories(trigger.new, trigger.oldMap); 
            WorkOrderTriggerHandler.setOperatingHours(Trigger.new);
            WorkOrderTriggerHandler.updateWorkOrderRescheduleDate(Trigger.new, Trigger.oldMap);  
            WorkOrderTriggerHandler.updateWorkOrderFromAccount(Trigger.new);
            WorkOrderTriggerHandler.setWOCompleteDate(Trigger.new, Trigger.oldMap);
            WorkOrderTriggerHandler.blockOverwritingWithOldRecordVersionInClassic(Trigger.new, Trigger.oldMap);
            
            map<Id, List<WorkOrder>> WorkOrderIdsToDuration = new map<Id, List<WorkOrder>>();
            for(WorkOrder wo: Trigger.new){
                if(wo.Status == 'To be Scheduled' || wo.Status == 'Canceled'){
                    if(wo.Primary_Installer_FSL__c != null){
                        wo.Primary_Installer_FSL__c = null;
                    }
                    if(wo.Primary_Tech_Measure_FSL__c != null){
                        wo.Primary_Tech_Measure_FSL__c = null;
                    }
                    if(wo.Primary_Service_FSL__c != null){
                        wo.Primary_Service_FSL__c = null; 
                    }
                    if(wo.Primary_Visitor__c != null){
                        wo.Primary_Visitor__c = null;
                    }
                }
                if(wo.Duration != Trigger.oldMap.get(wo.Id).Duration){
                    System.debug('move this duration to SA'+ wo.duration);
                    if(!WorkOrderIdsToDuration.containsKey(wo.Id)){
                        WorkOrderIdsToDuration.put(wo.Id, new List<WorkOrder>());
                        WorkOrderIdsToDuration.get(wo.Id).add(wo);
                    }else{
                        WorkOrderIdsToDuration.get(wo.Id).add(wo);
                    }
                }
            }
            List<ServiceAppointment> saList = [SELECT Id, Duration, DurationInMinutes, ParentRecordId, FSL__IsMultiDay__c, SchedStartTime, SchedEndTime FROM ServiceAppointment WHERE ParentRecordId IN: WorkOrderIdsToDuration.keySet()];
            List<ServiceAppointment> saListToUpdate = new List<ServiceAppointment>();
            List<WorkOrder> woListToUpdate = new List<WorkOrder>();
            for(ServiceAppointment sa: saList){
                for(WorkOrder wo: WorkOrderIdsToDuration.get(sa.ParentRecordId)){
                    sa.duration = wo.duration;
                    if(sa.SchedEndTime != null){
                        double dur = wo.DurationInMinutes;
                        sa.SchedEndTime = Datetime.valueOf(sa.SchedStartTime).addMinutes(Integer.valueOf(dur));
                        wo.Scheduled_End_Time__c = sa.SchedEndTime;
                    }
                    saListToUpdate.add(sa);
                }
            }
            if(saListToUpdate.size() > 0){
                UtilityMethods.disableAllFslTriggers();
                List<ServiceAppointment> saMultiDaySAsList = new List<ServiceAppointment>();
                for(ServiceAppointment sa: saListToUpdate){
                    if(sa.FSL__IsMultiDay__c == true){
                        
                        saMultiDaySAsList.add(sa);
                    }
                }
                if(saMultiDaySAsList.size() > 1){
                    for(ServiceAppointment sa: saMultiDaySAsList){
                        WorkOrderIdsToDuration.get(sa.ParentRecordId)[0].Duration.addError('you cannot change DURATION on Work Orders that already have more than 1 MultiDay Appointment. Please Reschedule using Chatter');
                    }
                }
                else{
                    update saListToUpdate;
                }
            }
        }
        if(Trigger.isDelete){
            System.debug('In isDelete');
            List<WorkOrder> woList = [SELECT Id, Sold_Order__c, Status, WorkTypeId, Work_Order_Type__c FROM WorkOrder WHERE Sold_Order__c =: Trigger.old[0].Sold_Order__c AND Work_Order_Type__c =: Trigger.old[0].Work_Order_Type__c AND Id !=: Trigger.old[0].Id];
            map<Id, WorkOrder> woMap = new map<Id, WorkOrder>();
            if(woList.size() > 0){
                for(WorkOrder wo: woList){
                    if(wo.Id != Trigger.old[0].Id){
                        woMap.put(wo.Id, wo);
                        break;
                    }
                }
                WorkOrderTriggerHandler.dispatchWorkOrderToOrder(woMap, null);
            }
        }
    }

    if (Trigger.isAfter) {
        if(Trigger.isInsert) {
            System.debug('In isInsert');
            WorkOrderTriggerHandler.generateWOLIonInsert(Trigger.newMap);
            WorkOrderTriggerHandler.assignSkills(Trigger.new);
            WorkOrderTriggerHandler.dispatchWorkOrderToOrder(Trigger.newMap, null);
            //List<WorkOrder> WorkOrdersToUpdateTerritory = new List<WorkOrder>();
            //for(WorkOrder wo: Trigger.new){
                //if(wo.ServiceTerritoryId == null){
                   // WorkOrdersToUpdateTerritory.add(wo);
                //}
            //}
            //if(WorkOrdersToUpdateTerritory.size() > 0){
                //update WorkOrdersToUpdateTerritory;
            //}
        }
        
        if (Trigger.isUpdate) {
            System.debug('In isUpdate');
            List<WorkOrder> WorkOrdersToUpdate = new List<WorkOrder>();
            set<Id> WorkOrderIdSet = new set<Id>();
            List<WorkOrder> WorkOrdersToUpdateTerritory = new List<WorkOrder>();
            for(WorkOrder wo: Trigger.new){
                WorkOrderIdSet.add(wo.Id);
                if(wo.ServiceTerritoryId == null){
                    WorkOrdersToUpdateTerritory.add(wo);
                }
            }
            if(WorkOrdersToUpdateTerritory.size() > 0){
                //update WorkOrdersToUpdateTerritory;
            }
            List<WorkOrder> woList = [SELECT Id, Sold_Order__c, Status, WorkTypeId, Work_Order_Type__c, (SELECT Id, Status FROM Service_Appointments__r) FROM WorkOrder WHERE Id IN: WorkOrderIdSet];
            List<WorkOrder> woToUpdate = new List<WorkOrder>();
            //List<ServiceAppointment> keep1ServiceAppointment = new List<ServiceAppointment>();
            //List<ServiceAppointment> deleteOtherServiceAppointments = new List<ServiceAppointment>();
            for(WorkOrder wo: woList){
                // System.debug('wo in update trigger'+ wo);
                // System.debug('wo in update trigger'+ wo.Service_Appointments__r.size());
                // if(wo.status == 'To be Scheduled' && wo.Service_Appointments__r.size() > 1){
                //     for(ServiceAppointment sa: wo.Service_Appointments__r){
                //         System.debug('sa: '+ sa);
                //         if(keep1ServiceAppointment.size() == 0){
                //             keep1ServiceAppointment.add(sa);
                //         }
                //         else{
                //             deleteOtherServiceAppointments.add(sa);
                //         }
                //     }
                // }
                
                if(wo.Sold_Order__c == null){
                    woToUpdate.add(wo);
                }
            }

            // if(deleteOtherServiceAppointments.size() > 0){
            //     delete deleteOtherServiceAppointments;
            // }
            if(woToUpdate.size() > 0){
                StatusCheckUtility.updateServiceAppointmentfromWorkOrder(woToUpdate);
            }
            WorkOrderTriggerHandler.generateWOLIonInsert(Trigger.newMap);
            RMS_cancelWOManager.closeEvent(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
            WorkOrderTriggerHandler.updateSkills(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
            WorkOrderTriggerHandler.dispatchWorkOrderToOrder(Trigger.newMap, Trigger.oldMap);
        }
    }

    UtilityMethods.setWOTriggerRan();
}