trigger SalesCapacityTrigger on Sales_Capacity__c (before insert, before update) {
    
    if (!UtilityMethods.isTriggerActive('SalesCapacity')) return;

    if(Trigger.isBefore){
        Set<Id> userIds = new Set<Id>();
        Set<Id> slotIds = new Set<Id>();
        for(Sales_Capacity__c sc : (List<Sales_Capacity__c>)Trigger.new){
            userIds.add(sc.User__c);
            slotIds.add(sc.Slot__c);
        }
        
        Map<Id,User> users = new Map<Id,User>([Select Id,Name From User Where Id in :userIds]);
        Map<Id,Slot__c> slots = new Map<Id,Slot__c>([Select Id,Name,Start__c From Slot__c Where Id in :slotIds]);
    
        for(Sales_Capacity__c sc : (List<Sales_Capacity__c>)Trigger.new){
            //String reference = (sc.Date__c == null ? '' : ((Datetime)sc.Date__c).format('M/d/yyyy') + ' ' + (slots.get(sc.Slot__c) == null ? '' : slots.get(sc.Slot__c).Name)).trim();
            sc.Name = (users.get(sc.User__c).Name + ' - ' + (sc.Date__c == null ? '' : ((Datetime)sc.Date__c).formatGmt('M/d')) + ' ' + (slots.get(sc.Slot__c) == null ? '' : slots.get(sc.Slot__c).Name)).trim();
            //sc.Slot_and_Date_Reference__c = reference;
        }
	}
}