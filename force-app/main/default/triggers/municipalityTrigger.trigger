trigger municipalityTrigger on Municipality__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
            
    //HANDLERS AND MANAGERS
    RMS_MunicipalityManager municipalityManager = new RMS_MunicipalityManager();
    
    // Before Insert
    /*
    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    
    
        Before Update
    
    if(Trigger.isUpdate && Trigger.isBefore){
        
    }
        

    // Before Delete
    
    else if(Trigger.isDelete && Trigger.isBefore){
        
    }
    
    
    // After Insert 
    
    else */
    
    if(Trigger.isInsert && Trigger.isAfter){
        municipalityManager.createMunicipalityMatch(Trigger.new, Trigger.newMap);         
    } 
    
    /*
    // After Update
    
    else if(Trigger.isUpdate && Trigger.isAfter){
        
    }
                
    //After Delete
    
    else if(Trigger.isDelete && Trigger.isAfter){
        
    }
    
    
    // After Undelete 
    
    else if(Trigger.isUnDelete){
        
    }
    */
}