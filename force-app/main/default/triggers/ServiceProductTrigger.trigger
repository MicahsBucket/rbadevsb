/*******************************************************//**

@trigger ProductTrigger

@brief  trigger framework to secure order of operation

@author  Brianne Wilson (Slalom.BLW)

@version    2/13/2017  Slalom.BLW
Created.
Edited by Mitchell Woloschek mwoloschek@penrodsoftware.com 4/10/2018


@copyright  (c)2017 Slalom.  All Rights Reserved.
Unauthorized use is prohibited.

***********************************************************/

trigger ServiceProductTrigger on Service_Product__c (before insert, before update, before delete, after insert, after undelete, after update, after delete) {

    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Service_Product__c')) return;


    //HANDLERS AND MANAGERS

    List<SObject> products = new List<SObject>();

    if(Trigger.isInsert && Trigger.isAfter){

        // products = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
        products = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);

    }

    // After Update
    else if(Trigger.isUpdate && Trigger.isAfter){

        // products = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
        products = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
    }

    //After Delete

    else if(Trigger.isDelete && Trigger.isAfter){

        // products = (List<SObject>) dlrs.RollupService.rollup(trigger.old);
        products = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.old, false);
    }


    //After Undelete

    else if(Trigger.isUnDelete){
        // products = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
        products = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
    }

    // Try - Catch to catch any dml errors doing the account rollup and displaying
    // errors on the order records
    try { update products;}
    catch(System.DmlException e) {
        if (Trigger.isDelete) for (sObject obj : trigger.old) { obj.addError(e.getDmlMessage(0)); }
        else for (sObject obj : trigger.new) { obj.addError(e.getDmlMessage(0)); }
    }
}