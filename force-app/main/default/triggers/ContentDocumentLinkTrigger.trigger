trigger ContentDocumentLinkTrigger on ContentDocumentLink (before insert, before update, after insert)  {
    
    if (Trigger.isBefore) {
        if (Trigger.isInsert || Trigger.isUpdate) {
            ContentDocumentLinkTriggerHelper.setVisibilityOnFSLFiles(Trigger.new);
        }
    }

    if(Trigger.isAfter && Trigger.isInsert) {
        System.debug(Trigger.new);
        ContentDocumentLinkTriggerHelper.isAfterInsert(Trigger.new);
        
        for( ContentDocumentLink contentdoc: trigger.new ){
            //To Capture the Json validations on uploading file 
            if(String.isNotEmpty(ContentDocumentLinkTriggerHelper.fileUploadError))
            {
                contentdoc.adderror(ContentDocumentLinkTriggerHelper.fileUploadError);
            }
        }
        
    }
}