trigger MA_SchedulerAssignedResourcesTrigger on Assigned_Resources__c (after insert, before delete) {

	/******************************************* */
	/*											 */
	/*		On INSERT - oldMap is NULL 			 */
	/*		On DELETE - newMap is NULL 			 */
	/*		On UPDATE - neither is NULL 		 */
	/*											 */
	/******************************************* */

	if ((Trigger.isAfter && Trigger.isInsert) || (Trigger.isBefore && Trigger.isDelete)) {
		if ((Trigger.newMap != null && !Trigger.newMap.isEmpty()) || (Trigger.oldMap != null && !Trigger.oldMap.isEmpty())) {
			Set<Id> resourceIds = new Set<Id>();
			Set<Id> workOrderIds = new Set<Id>();
			Map<Id, Assigned_Resources__c> assignedResourcesMap = Trigger.newMap;
			List<Assigned_Resource_History__c> resourceHistoryToInsert = new List<Assigned_Resource_History__c>();
			Map<String, Assigned_Resource_History__c> resourceHistoryMap = new Map<String, Assigned_Resource_History__c>();

			if (assignedResourcesMap != null && Trigger.oldMap != null) {
				assignedResourcesMap.putAll(Trigger.oldMap);
			}
			else if (Trigger.oldMap != null) {
				assignedResourcesMap = Trigger.oldMap;
			}
			else if (assignedResourcesMap == null) {
				assignedResourcesMap = new Map<Id, Assigned_Resources__c>();
			}

			for (Assigned_Resources__c resource : assignedResourcesMap.values()) {
				resourceIds.add(resource.Scheduled_Resource__c);
				workOrderIds.add(resource.Work_Order__c);
			}

			List<Assigned_Resource_History__c> resourceHistories = [
				SELECT Id, Scheduled_Times__c, Resource__c, RbA_Work_Order__c FROM Assigned_Resource_History__c
					WHERE Resource__c IN :resourceIds AND RbA_Work_Order__c IN :workOrderIds
			];

			for (Assigned_Resource_History__c resourceHistory : resourceHistories) {
				resourceHistoryMap.put(resourceHistory.Resource__c + ':' + resourceHistory.RbA_Work_Order__c, resourceHistory);
			}

			for (Assigned_Resources__c resource : assignedResourcesMap.values()) {
				Assigned_Resource_History__c resourceHistory = resourceHistoryMap.get(resource.Scheduled_Resource__c + ':' + resource.Work_Order__c);

				if (resourceHistory == null) {
					resourceHistory = new Assigned_Resource_History__c(
						Resource__c = resource.Scheduled_Resource__c,
						RbA_Work_Order__c = resource.Work_Order__c
					);

					resourceHistoryMap.put(resource.Scheduled_Resource__c + ':' + resource.Work_Order__c, resourceHistory);
					resourceHistoryToInsert.add(resourceHistory);
				}
			}

			insert resourceHistoryToInsert;
		}
	}

}