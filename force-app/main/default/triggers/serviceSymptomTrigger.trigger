/*******************************************************//**

@trigger serviceSymptomTrigger

@brief  trigger framework to secure order of operation

@author  Brianne Wilson (Slalom.BLW)

@version    2016-6-06  Slalom.BLW
    Created.

@see        

@copyright  (c)2016 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/

trigger serviceSymptomTrigger on Service_Symptom__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Service_Symptom__c')) return;
    
    //HANDLERS AND MANAGERS    
    List<SObject> orderitems = new List<SObject>();
    
    // Before Insert
    /*
    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    */
    
    //  Before Update
    /*
    if(Trigger.isUpdate && Trigger.isBefore){
        
    }
        

    // Before Delete
    /*
    else if(Trigger.isDelete && Trigger.isBefore){
        handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
    }
    */
    
    // After Insert 
    // else       
        if(Trigger.isInsert && Trigger.isAfter){
        // run the order rollup real-time if the order trigger
        // hasn't been run yet, otherwise run it @future
        
        
            
            // orderitems = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
            
            orderitems = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
    } 
    
    
    // After Update
    
    else if(Trigger.isUpdate && Trigger.isAfter){
        // run the order rollup real-time if the order trigger
        // hasn't been run yet, otherwise run it @future            
        
            // orderitems = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
            orderitems = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
    }

    
                
    //After Delete

    else if(Trigger.isDelete && Trigger.isAfter){
        //handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
        // orderitems = (List<SObject>) dlrs.RollupService.rollup(trigger.old);
        orderitems = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.old, false);
    }
    
    
    // After Undelete 

    else if(Trigger.isUnDelete){
        //handler.OnUndelete(Trigger.new);
        // orderitems = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
        orderitems = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
    }
    
    
    // run the order rollup real-time if the order trigger
    // hasn't been run yet, otherwise run it @future            
    if (UtilityMethods.hasOrderTriggerRan()) return;

    // Try - Catch to catch any dml errors doing the order item rollup and displaying
    // errors on the service symptoms records
    try { update orderitems;} 
    catch(System.DmlException e) {
        if (Trigger.isDelete) for (sObject obj : trigger.old) { obj.addError(e.getDmlMessage(0)); }
        else for (sObject obj : trigger.new) { obj.addError(e.getDmlMessage(0)); }
    }
}