trigger OrderTrigger2 on Order( after insert, after update, before insert, before update)
{
 
    if (!UtilityMethods.isTriggerActive('Order')) return;
    
    OrderTriggerHandler handler = new OrderTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    // FSLRollupRolldownController.setTriggerStarted(Trigger.isBefore, (Trigger.isUpdate || Trigger.isInsert), 'Order'); 
    
    if( Trigger.isInsert )
    {
        if(Trigger.isBefore)
        {
            handler.OnBeforeInsert(trigger.New);
        }
        else
        {
            handler.OnAfterInsert(trigger.NewMap);
        }
    }
    else if ( Trigger.isUpdate )
    {
        if(Trigger.isBefore)
        {
            handler.OnBeforeUpdate(Trigger.NewMap,Trigger.OldMap);
        }
        else
        {
            handler.OnAfterUpdate(Trigger.NewMap,Trigger.OldMap);
        }
    }
    else if(Trigger.isDelete)
    {
        if(Trigger.isAfter)
        {
            handler.OnAfterDelete(Trigger.OldMap);
        }   
    }
    else if(Trigger.isUnDelete)
    {
        if(Trigger.isAfter)
        {
            handler.OnAfterUnDelete(Trigger.newMap);
        }
           
    }
}