trigger SkillRequirementTrigger on SkillRequirement (before insert, before update) {
    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('SkillRequirement')) return;
    
    if (Trigger.isBefore) {
        if (Trigger.isInsert){
            SkillRequirementTriggerHandler.validateScheduledWorkOrder(Trigger.new);
        }
        if (Trigger.isUpdate) {
            SkillRequirementTriggerHandler.validateScheduledWorkOrder(Trigger.new);
        }
    }
}