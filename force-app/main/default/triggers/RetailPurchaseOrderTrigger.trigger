/**
 * @File Name          : RetailPurchaseOrderTrigger.trigger
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    7/29/2019, 1:58:14 PM   mark.rothermal@andersencorp.com     Initial Version
**/
trigger RetailPurchaseOrderTrigger on Retail_Purchase_Order__c (after insert, after update) {
    
    
    if (Trigger.isAfter && Trigger.isInsert){
        RetailPurchaseOrderTriggerHandler.updateRelatedOrderItems(Trigger.new);
    // RetailPurchaseOrderTriggerHandler.purchaseOrderName(Trigger.new);
    }
    
    if (Trigger.isAfter && Trigger.isUpdate){
        RetailPurchaseOrderTriggerHandler.updateOrderStatus(Trigger.new,Trigger.oldMap, Trigger.newMap);  
 }

}