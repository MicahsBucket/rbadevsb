/*
 * This trigger exists because there is no relationship between the SMA__MAWaypoint__c and CNVSS_Canvass_Unit__c. 
 * This trigger updates the Notes field in the waypoint to reflect DO NOT CONTACT field in Canvass Unit.
 */

trigger CanvassWaypointTrigger on sma__MAWaypoint__c (after insert) {
   
    if(trigger.isInsert) {
        CanvassWaypointTriggerHandler.UpdateWaypointNotesDoNotKnock(trigger.new);
    }
}