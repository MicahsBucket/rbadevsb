/*******************************************************//**

@trigger RefundTrigger

@brief	trigger framework to secure order of operation

@author  Anthony Strafaccia (Slalom.ADS)

@version	2016-02/17  Slalom.ADS Created

@see		RefundTriggerTest 
 
@copyright  (c)2016 Slalom.  All Rights Reserved.
			Unauthorized use is prohibited.

***********************************************************/

trigger RefundTrigger on Refund__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	
	// Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Refund__c')) return;
	
	//HANDLERS AND MANAGERS
	RMS_financialTransactionManager financialTransactionManager = new RMS_financialTransactionManager();
		
	
	// Before Insert
	if(Trigger.isInsert && Trigger.isBefore){
		financialTransactionManager.onBeforeInsertRefund(Trigger.new);
	}

	// Before Delete
	if(Trigger.isDelete && Trigger.isBefore){
		financialTransactionManager.onBeforeDeleteOrder(Trigger.old, Trigger.oldMap);
	}
	
	// After Insert
	if(Trigger.isInsert && Trigger.isAfter){
		financialTransactionManager.onAfterInsertAsset(Trigger.new, Trigger.newMap);
	}
	
		
	// After Update
	if(Trigger.isUpdate && Trigger.isAfter){
		financialTransactionManager.onAfterUpdateOrder(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	}
	
}