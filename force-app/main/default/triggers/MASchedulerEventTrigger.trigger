/*******************************************************//**

@trigger MASchedulerEventTrigger

@brief  trigger framework to secure order of operation

@author  Brianne Wilson

@version    2016-12/1  
    Created.


@copyright  (c)2016 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/

trigger MASchedulerEventTrigger on MA_Scheduler_Event__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    
    //HANDLERS AND MANAGERS            
    List<SObject> workorders = new List<SObject>();
    
    // Before Insert
    /***
    if(Trigger.isInsert && Trigger.isBefore){
            
    }
    
    
    //  Before Update
    else
    if(Trigger.isUpdate && Trigger.isBefore){
        
    }
        

    // Before Delete
    /*
    else if(Trigger.isDelete && Trigger.isBefore){
        
    }
    */
    
    // After Insert 
    
    //else 
    if(Trigger.isInsert && Trigger.isAfter){
        
        if (UtilityMethods.hasOrderTriggerRan()){
            // RMS_FutureRollups.rollupWorkOrdersToOrders(trigger.newMap.keySet());
            List<sObject> orderRecords = new List<sObject>();
            List<WorkOrder> workOrderRecords = new List<WorkOrder>();
            workOrderRecords = [SELECT Id, Work_Order_Type__c, Sold_Order__c from WorkOrder where Id IN: trigger.newMap.keySet()];
            //    orderRecords =  dlrs.RollupService.rollup(workOrderRecords);
            System.enqueueJob(new DynamicRollUpQue(workOrderRecords));
        }
        else{
            // workorders = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
            workorders = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
            
        }
    } 
    
    
    // After Update
    
    else if(Trigger.isUpdate && Trigger.isAfter){
        if (UtilityMethods.hasOrderTriggerRan()){
            system.debug('UtilityMethods.hasOrderTriggerRan() YES ');
            // RMS_FutureRollups.rollupWorkOrdersToOrders(trigger.newMap.keySet());
            List<sObject> orderRecords = new List<sObject>();
            List<WorkOrder> workOrderRecords = new List<WorkOrder>();
            workOrderRecords = [SELECT Id, Work_Order_Type__c, Sold_Order__c from WorkOrder where Id IN: trigger.newMap.keySet()];
            //    orderRecords =  dlrs.RollupService.rollup(workOrderRecords);
            System.enqueueJob(new DynamicRollUpQue(workOrderRecords));
        }
        else{
            // workorders = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
            workorders = (List<SObject>)DynamicRollUpUtility.DynamicRollUp(trigger.new, false);
            system.debug('UtilityMethods.hasOrderTriggerRan() NOPE ');
        }
    }

    
                
    //After Delete
    
    /*  else if(Trigger.isDelete && Trigger.isAfter){
            if (UtilityMethods.hasOrderTriggerRan())
            RMS_FutureRollups.rollupWorkOrdersToOrders(trigger.newMap.keySet());
        else
        workorders = (List<SObject>) dlrs.RollupService.rollup(trigger.old);
    }
    
    
    // After Undelete 
    /*
    else if(Trigger.isUnDelete){
        
    }
    */
    if (UtilityMethods.hasOrderTriggerRan()) return;
        // Try - Catch to catch any dml errors doing the work order rollup and displaying
    // errors on the labor records
    try { 
        update workorders;
        } 
    catch(System.DmlException e) {
        if (Trigger.isDelete) for (sObject obj : trigger.old) { 
            obj.addError(e.getDmlMessage(0)); 
        }
        else for (sObject obj : trigger.new) { 
            obj.addError(e.getDmlMessage(0)); 
        } 
    }
}