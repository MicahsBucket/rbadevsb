/*******************************************************//**

@trigger ContactHistoryTrigger

@brief  trigger framework to secure order of operation

@author  Slalom (Slalom)

@version    2016-04-04  Slalom
Created.
@version	2017-04-07	Slalom.CDK
Revised

@see        ContactHistorydTriggerTest

@copyright  (c)2016 Slalom.  All Rights Reserved.
Unauthorized use is prohibited.

***********************************************************/
trigger ContactHistoryTrigger on Contact_History__c (before insert, before update, before delete, after insert, after undelete, after update, after delete) {

    if (!UtilityMethods.isTriggerActive('Contact_History__c')) return;
    
    //HANDLERS AND MANAGERS
    RMS_createContactHistoryManager contactHistoryCreationManager = new RMS_createContactHistoryManager(); 
    
    // Before Insert
    /*        
    if(Trigger.isInsert && Trigger.isBefore){
    }
    */
    
    // Before Update
    /*
    else if(Trigger.isUpdate && Trigger.isBefore){
    }
    */


    // Before Delete
    /*
    else if(Trigger.isDelete && Trigger.isBefore){
    }
    */
    
    // After Insert 
    if(Trigger.isInsert && Trigger.isAfter){
        contactHistoryCreationManager.afterContactHistoryInsertManager(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);                   
    } 
    
    // After Update
    else if(Trigger.isUpdate && Trigger.isAfter){
        contactHistoryCreationManager.afterContactHistoryUpdateManager(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);                   
    }
    
    //After Delete
    /*
    else if(Trigger.isDelete && Trigger.isAfter){
    }
    */
    
    // After Undelete 
    /*
    else if(Trigger.isUnDelete){
    }
    */
    
}