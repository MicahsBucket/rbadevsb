trigger CommPaymentPlanTrigger on Comm_Payment_Plan__c (after insert) {

	if(Trigger.isInsert && Trigger.isAfter){
		CommissionsManagement.checkForPayPlanClone(Trigger.New);
	}
}