trigger StoreConfigurationTrigger on Store_Configuration__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Store_Configuration__c')) return;

    RMS_StoreConfigurationTriggerHandler handler = new RMS_StoreConfigurationTriggerHandler();
    if(Trigger.isAfter && Trigger.isDelete) {

    }

    if(Trigger.isAfter && Trigger.isInsert) {

    }

    if(Trigger.isAfter && Trigger.isUndelete) {

    }

    if(Trigger.isAfter && Trigger.isUpdate) {

    }

    if(Trigger.isBefore && Trigger.isDelete) {

    }

    if(Trigger.isBefore && Trigger.isInsert) {
        System.debug('StoreConfigurationTrigger/beforeInsert');
        handler.isBeforeInsert(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }    

    if(Trigger.isBefore && Trigger.isUpdate) {
        System.debug('is before update');
        handler.isBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }
    
}