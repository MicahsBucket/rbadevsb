public without sharing class OrderHelper {

    /***********************************************
    get Account Record's with Id
    ************************************************/
    public static map<id, Account> getAccountIdToAccount(list<id> relatedAccountIds){        
        map<id, Account> accountIdToAccountMap = new map<id, Account>();
        
        for(Account AccRec: [SELECT BillingStreet, BillingCity, BillingState,BillingStateCode,BillingPostalCode,BillingCountry,
                                    BillingCountryCode,Store_Location__c, Store_Location__r.Active_Store_Configuration__c
                             FROM Account 
                             WHERE Id IN :relatedAccountIds]){
                                 
            accountIdToAccountMap.put(AccRec.id, AccRec);
        }
        
        return accountIdToAccountMap;
    }
    
    /***********************************************
    get Store Id To Store Config ID
    ************************************************/
    public static map<id, id> getStoreIdToStoreConfigId(list<id> relatedAccountIds){ 
        map<id,Id> storeIdToStoreConfigIdMap = new map<id,Id>();
        
        for (Account AccRec: [SELECT BillingStreet, BillingCity, BillingState,BillingStateCode,BillingPostalCode,BillingCountry,
                                    BillingCountryCode,Store_Location__c, Store_Location__r.Active_Store_Configuration__c
                             FROM Account 
                             WHERE Id IN :relatedAccountIds]){
                  if (AccRec.Store_Location__r.Active_Store_Configuration__c != null)
                storeIdToStoreConfigIdMap.put(AccRec.Store_Location__c, AccRec.Store_Location__r.Active_Store_Configuration__c);
            
        }
        return storeIdToStoreConfigIdMap;
    }
    /***********************************************
    get Dynamic SOQL Query
    ************************************************/
    public static String getStoreIdToStoreConfigId( string objectName){ 
        
        String bocQuery = 'SELECT ';
        SObjectType xBOC = Schema.getGlobalDescribe().get(objectName);
        map<String,Schema.SObjectField> bocFieldsMap = xBOC.getDescribe().fields.getMap();      
        
        
        for(String bocField : bocFieldsMap.KeySet()){
            bocQuery = bocQuery + bocField + ', ';
        }       
        bocQuery = bocQuery.substring(0,bocQuery.length()-2);
        bocQuery = bocQuery + ' FROM ' + objectName +' WHERE Store_Configuration__c IN :storeConfigIdList';
          
        return bocQuery;
    }
}