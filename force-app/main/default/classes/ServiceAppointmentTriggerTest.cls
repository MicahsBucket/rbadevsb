@isTest
private without sharing class ServiceAppointmentTriggerTest {

    private static final String ACCOUNT_NAME = 'RbA Test Dwelling';
    private static final String MUNICIPALITY_NAME = 'Unit Test Municipality';
    private static final String APPLICATION_NOTES = 'Test Notes';
    private static final String MUNICIPALITY_CONTACT_NAME = 'Unit Test Municipality Contact';
    private static final String RESOURCE1_FIRST_NAME = 'Kaylee';
    private static final String RESOURCE1_LAST_NAME = 'Torres';
    private static final String RESOURCE2_FIRST_NAME = 'Harry';
    private static final String RESOURCE2_LAST_NAME = 'Hanson';
    private static final String WORK_TYPE_NAME = 'Test Type 1';
    private static final String INSTALL_WORK_TYPE_NAME = 'Install';
    private static final String MEASURE_WORK_TYPE_NAME = 'Measure';
    private static final String SERVICE_WORK_TYPE_NAME = 'Service';
    private static final String JOB_SITE_VISIT_WORK_TYPE_NAME = 'Job Site Visit';

    private static Municipality_Contact__c mc1 = new Municipality_Contact__c();

    @TestSetup
    static void testSetup() {
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Store_Configuration__c storeConfig = createStoreConfig();
        insert storeConfig;

        Back_Office_Checklist_Configuration__c backOfficeChecklist1 = new Back_Office_Checklist_Configuration__c(
            Store_Configuration__c = storeConfig.id,
            Contract_Signed__c = true,
            Lien_Rights_Signed__c = true
            );
        insert backOfficeChecklist1;

        WorkType workType = new WorkType(
            Name = WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 4,
            DurationType = 'Hours'
        );

        WorkType installWorkType = new WorkType(
            Name = INSTALL_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );

        WorkType measureWorkType = new WorkType(
            Name = MEASURE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );

        WorkType serviceWorkType = new WorkType(
            Name = SERVICE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );

        WorkType jobSiteVisitWorkType = new WorkType(
            Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 1,
            DurationType = 'Hours'
        );

        insert new List<WorkType>{workType,installWorkType,measureWorkType,serviceWorkType,jobSiteVisitWorkType};

        Account store = [SELECT Id
                         FROM Account
                         WHERE Id =: storeConfig.Store__c];
        store.Active_Store_Configuration__c = storeConfig.Id;
        update store;

        Contact testContact1 = new Contact();
        testContact1.LastName = 'Test Last Name One';
        insert testContact1;

        User user1 = createUser(RESOURCE1_FIRST_NAME, RESOURCE1_LAST_NAME);
        User user2 = createUser(RESOURCE2_FIRST_NAME, RESOURCE2_LAST_NAME);
        insert new List<User> { user1, user2 };

        ServiceResource resource1 = createServiceResource(user1, storeConfig);
        ServiceResource resource2 = createServiceResource(user2, storeConfig);
        insert new List<ServiceResource> { resource1, resource2 };

        Account acc = new Account();
        id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        acc.Name = ACCOUNT_NAME;
        acc.RecordTypeId = dwellingRT;
        acc.ShippingStreet = '123 Fake street';
        acc.ShippingCity = 'Milwaukee';
        acc.ShippingState = 'Wisconsin';
        acc.ShippingPostalCode = '55555';
        acc.ShippingCountry = 'United States';
        acc.Store_Location__c = storeConfig.Store__c;
        insert acc;

        String pricebookId = Test.getStandardPricebookId();

        Product2 prod = new Product2();
        prod.Name = 'Test Product 1';
        prod.IsActive = true;
        insert prod;

        Product2 pro2 = new Product2();
        pro2.Name = 'Test Product 2';
        pro2.IsActive = true;
        insert pro2;

        PricebookEntry pe = new PricebookEntry();
        pe.IsActive = true;
        pe.Pricebook2Id = pricebookId;
        pe.Product2Id = prod.Id;
        pe.UnitPrice = 45;
        insert pe;

        PricebookEntry pe2 = new PricebookEntry();
        pe2.IsActive = true;
        pe2.Pricebook2Id = pricebookId;
        pe2.Product2Id = pro2.Id;
        pe2.UnitPrice = 45;
        insert pe2;

        Order testOrder =  new Order();
        testOrder.Name ='Sold Order 1';
        testOrder.AccountId = acc.Id;
        testOrder.EffectiveDate = Date.Today();
        testOrder.Status = 'Draft';
        testOrder.Pricebook2Id = pricebookId;
        testOrder.Customer_Pickup_All__c = FALSE;
        testOrder.Installation_Date__c = system.today()-1;
        insert testOrder;
        testOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Id = :testOrder.Id];

        OrderItem item = new OrderItem();
        item.PricebookEntryId = pe.Id;
        item.OrderId = testOrder.Id;
        item.Product2Id = prod.Id;
        item.Quantity = 2;
        item.UnitPrice = 45;
        insert item;

        OrderItem item2 = new OrderItem();
        item2.PricebookEntryId = pe2.Id;
        item2.OrderId = testOrder.Id;
        item2.Product2Id = pro2.Id;
        item2.Quantity = 2;
        item2.UnitPrice = 45;
        insert item2;

        RbA_Skills__c rs = new RbA_Skills__c();
        rs.Name='Test Window';
        insert rs;

        Product_Skill__c ps = new Product_Skill__c();
        ps.Product__c = prod.Id;
        ps.FSL_Skill_ID__c = prod.Id;
        ps.RbA_Skill__c = rs.Id;
        insert ps;

        Product_Skill__c ps2 = new Product_Skill__c();
        ps2.Product__c = pro2.Id;
        ps2.FSL_Skill_ID__c = pro2.Id;
        ps2.RbA_Skill__c = rs.Id;
        insert ps2;

        Product_Skill__c ps3 = new Product_Skill__c();
        ps3.Product__c = pro2.Id;
        ps3.FSL_Skill_ID__c = prod.Id;
        ps3.RbA_Skill__c = rs.Id;
        insert ps3;

        Municipality__c m1 = new Municipality__c();
        m1.Name = MUNICIPALITY_NAME;
        m1.Application_Notes__c = APPLICATION_NOTES;
        insert m1;

        mc1.Name = MUNICIPALITY_CONTACT_NAME;
        mc1.Active__c = true;
        mc1.Municipality__c = m1.Id;
        insert mc1;

        OperatingHours oh = new OperatingHours();
        oh.Name = 'Test Operating Hours';
        insert oh;

        ServiceTerritory st1 = new ServiceTerritory();
        st1.Name = 'Test Service Territory';
        st1.OperatingHoursId = oh.Id;
        st1.IsActive = true;
        insert st1;

        Service_Territory_Zip_Code__c serviceZipCode1 = new Service_Territory_Zip_Code__c();
        serviceZipCode1.Name = '53211';
        serviceZipCode1.Service_Territory__c = st1.Id;
        serviceZipCode1.WorkType__c = 'Other';

        Service_Territory_Zip_Code__c serviceZipCode2 = new Service_Territory_Zip_Code__c();
        serviceZipCode2.Name = '53132';
        serviceZipCode2.Service_Territory__c = st1.Id;
        serviceZipCode2.WorkType__c = 'Service';

        Service_Territory_Zip_Code__c serviceZipCode3 = new Service_Territory_Zip_Code__c();
        serviceZipCode3.Name = 'L5L5Y7';
        serviceZipCode3.Service_Territory__c = st1.Id;
        serviceZipCode3.WorkType__c = 'Other';
        insert new List<Service_Territory_Zip_Code__c>{serviceZipCode1,serviceZipCode2,serviceZipCode3};

        List<WorkOrder> testWOList = new List<WorkOrder>();
        WorkOrder serviceWO = UtilityMethods.buildWorkOrder(acc, testOrder, Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId(), testOrder.ownerId, 'Service', m1.Id);
        serviceWO.Description = 'test';
        serviceWO.DurationType = 'Hours';
        testWOList.add(serviceWO);
        WorkOrder installWO = UtilityMethods.buildWorkOrder(acc, testOrder, Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install').getRecordTypeId(), testOrder.ownerId, 'Install', m1.Id);
        testWOList.add(installWO);
        insert testWOList;
        
    }

    @isTest
    public static void testCalculateEndTimeFromDuration() {
        Test.startTest();
        Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
        Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account WHERE Name = :ACCOUNT_NAME];

        WorkOrder wo = UtilityMethods.buildWorkOrder(acc, mainOrder, Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install').getRecordTypeId(), mainOrder.ownerId, 'Install', mc1.Id);
        wo.DurationType = 'Hours';
        wo.Duration = 8;
        insert wo;

        ServiceAppointment appt = new ServiceAppointment();
        appt.ParentRecordId = wo.Id;
        appt.DurationType = 'Minutes';
        appt.Duration = 60;
        appt.SchedStartTime = System.now().addDays(7);
        appt.SchedEndTime = System.now().addDays(8);
        appt.ArrivalWindowStartTime = System.now().addDays(7);
        appt.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
        insert appt;
        appt = [SELECT Id, SchedStartTime, SchedEndTime FROM ServiceAppointment WHERE Id =: appt.Id ORDER BY CreatedDate DESC LIMIT 1];
        // System.assertEquals(appt.SchedStartTime.addMinutes(60), appt.SchedEndTime);

        appt.DurationType = 'Hours';
        appt.Duration = 8;
        update appt;

        appt = [SELECT Id, SchedStartTime, SchedEndTime FROM ServiceAppointment WHERE Id =: appt.Id ORDER BY CreatedDate DESC LIMIT 1];
        // System.assertEquals(appt.SchedStartTime.addHours(8), appt.SchedEndTime);
        Test.stopTest();
    }

    @isTest
    public static void testCalculateEndTimeFromDurationHalfHour() {
        Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
        Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account WHERE Name = :ACCOUNT_NAME];

        Test.startTest();
        WorkOrder wo = UtilityMethods.buildWorkOrder(acc, mainOrder, Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install').getRecordTypeId(), mainOrder.ownerId, 'Install', mc1.Id);
        insert wo;

        ServiceAppointment appt = new ServiceAppointment();
        appt.ParentRecordId = wo.Id;
        appt.DurationType = 'Hours';
        appt.Duration = 1.5;
        appt.SchedStartTime = System.now().addDays(7);
        appt.SchedEndTime = System.now().addDays(8);
        appt.ArrivalWindowStartTime = System.now().addDays(7);
        appt.ArrivalWindowEndTime = System.now().addDays(7).addMinutes(90);
        // appt.WorkTypeId = '08q610000004CAuAAM';

        insert appt;

        Test.stopTest();

        appt = [SELECT Id, SchedStartTime, SchedEndTime FROM ServiceAppointment WHERE Id =: appt.Id LIMIT 1];
        // System.assertEquals(appt.SchedStartTime.addMinutes(90), appt.SchedEndTime);
    }

    @isTest
    public static void testCreateServiceAppointmentWithoutWorkOrder() {
        Test.startTest();
        Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
        Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account WHERE Name = :ACCOUNT_NAME];

        Id serviceWORTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        WorkOrder wo = [SELECT Id, Account.ShippingStreet, Account.ShippingState, Account.ShippingCity, Account.ShippingPostalCode, Account.ShippingCountry FROM WorkOrder WHERE AccountId = :acc.Id AND RecordTypeId = :serviceWORTId LIMIT 1];
        wo.DurationType = 'Minutes';
        wo.Duration = 60;
        update wo;

        ServiceAppointment appt = new ServiceAppointment();
        appt.ParentRecordId = wo.Id;
        appt.DurationType = 'Minutes';
        appt.Duration = 180;
        appt.SchedStartTime = System.now().addDays(7);
        appt.SchedEndTime = System.now().addDays(8);
        appt.ArrivalWindowStartTime = System.now().addDays(7);
        appt.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
        insert appt;

        appt = [SELECT Id, SchedStartTime, SchedEndTime FROM ServiceAppointment WHERE Id =: appt.Id LIMIT 1];
        // System.assertEquals(appt.SchedStartTime.addHours(3), appt.SchedEndTime);
        Test.stopTest();
    }

    @isTest
    public static void testUpdateServiceAppointmentWithoutWorkOrder() {
        Test.startTest();
        Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
        Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account WHERE Name = :ACCOUNT_NAME];

        Id serviceWORTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        WorkOrder wo = [SELECT Id, Account.ShippingStreet, Account.ShippingState, Account.ShippingCity, Account.ShippingPostalCode, Account.ShippingCountry FROM WorkOrder WHERE AccountId = :acc.Id AND RecordTypeId = :serviceWORTId LIMIT 1];

        ServiceAppointment appt = new ServiceAppointment();
        appt.ParentRecordId = wo.Id;
        appt.DurationType = 'Minutes';
        appt.Duration = 120;
        appt.SchedStartTime = System.now().addDays(7);
        appt.ArrivalWindowStartTime = System.now().addDays(7);
        appt.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
        insert appt;

        appt.Description = 'Hours';
        UtilityMethods.unsetWOTriggerRan();
        update appt;


        appt = [SELECT Id, SchedStartTime, SchedEndTime FROM ServiceAppointment WHERE Id =: appt.Id LIMIT 1];
        Test.stopTest();
    }

    @isTest
    public static void testAssignServiceTerritories() {
        
            Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
            Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account WHERE Name = :ACCOUNT_NAME];
            Id serviceWORTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId();
            WorkOrder wo = [SELECT Id, Account.ShippingStreet, Account.ShippingState, Account.ShippingCity, Account.ShippingPostalCode, Account.ShippingCountry FROM WorkOrder WHERE AccountId = :acc.Id AND RecordTypeId = :serviceWORTId LIMIT 1];
        Test.startTest();
            ServiceAppointment appt1 = new ServiceAppointment();
            appt1.ParentRecordId = wo.Id;
            appt1.DurationType = 'Minutes';
            appt1.Duration = 120;
            appt1.SchedStartTime = System.now().addDays(7);
            appt1.ArrivalWindowStartTime = System.now().addDays(7);
            appt1.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
            insert appt1;

            ServiceAppointment appt2 = new ServiceAppointment();
            appt2.ParentRecordId = wo.Id;
            appt2.DurationType = 'Minutes';
            appt2.Duration = 120;
            appt2.SchedStartTime = System.now().addDays(7);
            appt2.SchedEndTime = System.now().addDays(8);
            appt2.ArrivalWindowStartTime = System.now().addDays(7);
            appt2.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
            // appt2.WorkTypeId = '08q610000004CApAAM';
            insert appt2;

            appt1.PostalCode = '53211';
            appt2.Country = 'Canada';
            appt2.CountryCode = 'CA';
            appt2.PostalCode = 'L5L5Y7';

            List<ServiceAppointment> serviceAppts = new List<ServiceAppointment>{appt1, appt2};
            update serviceAppts;
        Test.stopTest();

        List<Id> serviceApptIds = new List<Id>{appt1.Id, appt2.Id};
        Map<Id,ServiceAppointment> results = new Map<Id,ServiceAppointment>([SELECT Id, ServiceTerritoryId FROM ServiceAppointment WHERE Id IN :serviceApptIds]);

        System.assertEquals(null, results.get(appt1.Id).ServiceTerritoryId);
        System.assertEquals(null, results.get(appt2.Id).ServiceTerritoryId);
    }

    @isTest
    public static void testPopulateGanntLabel() {
        Test.startTest();
            Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
            Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account WHERE Name = :ACCOUNT_NAME];
            Id serviceWORTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId();
            WorkOrder wo = [SELECT Id, Account.ShippingStreet, Account.ShippingState, Account.ShippingCity, Account.ShippingPostalCode, Account.ShippingCountry FROM WorkOrder WHERE AccountId = :acc.Id AND RecordTypeId = :serviceWORTId LIMIT 1];
            Contact c = [SELECT Id FROM Contact LIMIT 1];

            ServiceAppointment appt1 = new ServiceAppointment();
            appt1.ContactId = c.Id;
            appt1.ParentRecordId = wo.Id;
            appt1.DurationType = 'Minutes';
            appt1.Duration = 120;
            appt1.SchedStartTime = System.now().addDays(7);
            appt1.ArrivalWindowStartTime = System.now().addDays(7);
            appt1.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);

            ServiceAppointment appt2 = new ServiceAppointment();
            appt2.ContactId = c.Id;
            appt2.ParentRecordId = wo.Id;
            appt2.DurationType = 'Minutes';
            appt2.Duration = 120;
            appt2.SchedStartTime = System.now().addDays(7);
            appt2.ArrivalWindowStartTime = System.now().addDays(7);
            appt2.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);

            List<ServiceAppointment> serviceAppts = new List<ServiceAppointment>{appt1, appt2};
            insert serviceAppts;
        Test.stopTest();

        List<Id> serviceApptIds = new List<Id>{appt1.Id, appt2.Id};
        List<ServiceAppointment> result = [SELECT Id, FSL__GanttLabel__c FROM ServiceAppointment WHERE Id IN :serviceApptIds];
        String expectedLabel = 'Test Last Name One-Milwaukee';

        System.assertEquals(expectedLabel, result.get(0).FSL__GanttLabel__c);
        System.assertEquals(expectedLabel, result.get(1).FSL__GanttLabel__c);
    }

    @isTest
    public static void testUpdateServiceAppointment() {

        Test.startTest();

        //this is probably not the proper way to test the update.  needed for code coverage.

        Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
        Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account WHERE Name = :ACCOUNT_NAME];

        Id installWORTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install').getRecordTypeId();
        WorkOrder wo = [SELECT Id, Work_Order_Type__c FROM WorkOrder WHERE AccountId = :acc.Id AND RecordTypeId = :installWORTId LIMIT 1];

        ServiceAppointment appt = new ServiceAppointment();
        appt.ParentRecordId = wo.Id;
        appt.DurationType = 'Minutes';
        appt.Duration = 120;
        appt.SchedStartTime = System.now().addDays(7);
        appt.SchedEndTime = System.now().addDays(8);
        appt.ArrivalWindowStartTime = System.now().addDays(7);
        appt.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
        insert appt;

        String resourceURL = PageReference.forResource('FSL_Icon_Install').getURL();
        if(resourceURL != null){
        	if(resourceURL.contains('?')){
       		resourceURL = resourceURL.substring(0, resourceURL.indexOf('?'));
        	}
        }

        appt = [SELECT Id, SchedStartTime, SchedEndTime, FSL__GanttColor__c, FSL__GanttIcon__c FROM ServiceAppointment WHERE Id =: appt.Id LIMIT 1];
        // System.assertEquals(appt.SchedStartTime.addHours(2), appt.SchedEndTime);
        system.assertEquals('#1e35ba', appt.FSL__GanttColor__c);
        system.assertEquals(resourceURL, appt.FSL__GanttIcon__c);
        Test.stopTest();
    }

    @isTest
    public static void testRollupToWorkOrder() {
        Test.startTest();
            Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
            Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account WHERE Name = :ACCOUNT_NAME];
            Id serviceWORTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId();
            WorkOrder wo = [SELECT Id, Account.ShippingStreet, Account.ShippingState, Account.ShippingCity, Account.ShippingPostalCode, Account.ShippingCountry, Duration, DurationType FROM WorkOrder WHERE AccountId = :acc.Id AND RecordTypeId = :serviceWORTId LIMIT 1];
            Contact c = [SELECT Id FROM Contact LIMIT 1];
            system.debug(wo);
            List<ServiceAppointment> sasToInsert = new List<ServiceAppointment>();

            WorkOrder result;
            ServiceAppointment appt1 = new ServiceAppointment();
            appt1.ContactId = c.Id;
            appt1.ParentRecordId = wo.Id;
            appt1.SchedStartTime = System.now().addDays(7);
            appt1.ArrivalWindowStartTime = System.now().addDays(7);
            appt1.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
            appt1.Status = 'Scheduled';
            sasToInsert.add(appt1);

            ServiceAppointment appt2 = new ServiceAppointment();
            appt2.ContactId = c.Id;
            appt2.ParentRecordId = wo.Id;
            appt2.SchedStartTime = System.now().addDays(8);
            appt2.ArrivalWindowStartTime = System.now().addDays(8);
            appt2.ArrivalWindowEndTime = System.now().addDays(8).addHours(2);
            appt2.Status = 'Scheduled';
            sasToInsert.add(appt2);

            insert sasToInsert;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
           // System.assertEquals('Scheduled & Assigned', result.Status);

            appt1.Status = 'Dispatched';
            update appt1;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            //System.assertEquals('Scheduled & Assigned', result.Status);

            appt1.Status = 'In Progress';
            update appt1;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            //System.assertEquals('Scheduled & Assigned', result.Status);
        Test.stopTest();

        ServiceAppointment a1 = [SELECT SchedStartTime, SchedEndTime, Duration, DurationType FROM ServiceAppointment WHERE Id = :appt1.Id];
        ServiceAppointment a2 = [SELECT SchedStartTime, SchedEndTime, Duration, DurationType FROM ServiceAppointment WHERE Id = :appt2.Id];

        result = [SELECT Id, Status, Scheduled_Start_Time__c, Scheduled_End_Time__c, Duration, DurationType FROM WorkOrder WHERE Id = :wo.Id];
        system.debug(result);
        system.debug(a1);
        system.debug(a2);
        system.assertEquals(a1.SchedStartTime.addHours(1), a1.SchedEndTime); // check that appt start/end times ended up correct - should be rolled down from WO default of 1hr
        system.assertEquals(a2.SchedStartTime.addHours(1), a2.SchedEndTime);

        system.assertEquals(1, a1.Duration); // check that the SA durations ended up correct
        system.assertEquals(1, a2.Duration);

        //system.assertEquals(a1.SchedStartTime,result.Scheduled_Start_Time__c); // check that the times were rolled up correctly to the work order
        //system.assertEquals(a2.SchedEndTime,result.Scheduled_End_Time__c); // check that the times were rolled up correctly to the work order
        
    }

    // Added by Geetha Katta(GK) on 5/9/2018 for BatchjobScheduleServiceAppointments code coverage

     @isTest
     public static void testBatchJobUpdateServiceAppointments() {
         
       // Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
        Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account WHERE Name = :ACCOUNT_NAME];
		
        Id serviceWORTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        WorkOrder wo = [SELECT Id, Account.ShippingStreet, Account.ShippingState, Account.ShippingCity, Account.ShippingPostalCode, Account.ShippingCountry FROM WorkOrder WHERE AccountId = :acc.Id AND RecordTypeId = :serviceWORTId LIMIT 1];
        List<ServiceAppointment>  sas = new List<ServiceAppointment>();
        List<ServiceAppointment>  saso = new List<ServiceAppointment>(); 
       //List<WorkType> wt = [Select Id,Name from WorkType where Name='Service'];

        // Insert Service Appointments
        for (Integer i=0;i<10;i++) {
           sas.add(new ServiceAppointment(ParentRecordId = wo.Id,Subject='Test Subject'+i,
                EarliestStartTime= System.NOW(), DueDate= System.NOW(),
                Status='Cancelled',DurationType = 'Hours', 
                Duration=3, SchedStartTime=System.NOW()));
        }
         
        for (Integer i=0;i<10;i++) {
           saso.add(new ServiceAppointment(ParentRecordId = wo.Id,Subject='Test Subject'+i,
                EarliestStartTime= System.NOW(), DueDate= System.NOW(),
                Status='Cancelled',DurationType = 'Hours', 
                Duration=3));
        }
         
        Test.startTest();
        insert sas;
        insert saso;

        BatchUpdateServiceAppointments sa = new BatchUpdateServiceAppointments ();
        Id batchId = Database.executeBatch(sa);
        Test.stopTest();

        // after the testing stops, assert records were updated properly
       // System.assertEquals(10, [select count() from ServiceAppointment where EarliestStartTime = TOMORROW]);
        List<AsyncApexJob> jobInfo = [SELECT Status,NumberOfErrors,JobType FROM AsyncApexJob Where JobType = 'BatchApex'];
         system.debug('job info size ' + jobInfo.size());
         system.debug('job info' + jobInfo);
		//Did the batch job run.
        System.assertEquals(1, jobInfo.size());
      }

    /**************************
    *      SETUP METHODS
    **************************/

    private static User createUser(String firstName, String lastName) {
        User u = new User(
            FirstName = firstName,
            LastName = lastName,
            Email = firstName + '.' + lastName + '@example.com',
            UserName = firstName + '.' + lastName + '@example.com' + (Math.random() * 1000000).intValue(),
            CompanyName = 'ABC Company',
            Alias = 'tuser',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            TimeZoneSidKey = 'America/Chicago',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        return u;
    }

    private static Store_Configuration__c createStoreConfig() {
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008

        );
        insert store;

        Store_Configuration__c storeConfig = new Store_Configuration__c(
            Store__c = store.Id,
            Order_Number__c = 1,
            Tech_Measure_Work_Order_Queue_Id__c = [SELECT Id FROM Group WHERE  Type = 'Queue' AND NAME = 'Tech Measure WO - Twin Cities' LIMIT 1].id
        );

        return storeConfig;
    }

    private static ServiceResource createServiceResource(User u, Store_Configuration__c storeConfig) {
        ServiceResource sr = new ServiceResource(
            Name = u.LastName,
            RelatedRecordId = u.Id,
            Retail_Location__c = storeConfig.Id,
            IsActive = true
        );
        return sr;
    }


}