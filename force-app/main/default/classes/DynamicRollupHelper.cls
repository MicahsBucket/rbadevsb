/**
 * @author Micah Johnson - Demand Chain 2020
 * @description The job os this "Helper" class is to process rollup records that are ties to one specific parent/child object relationship
 * @param The parameter passed in is the child object and a string (Name) of the parent object that data should be rolled up to
 * @returns This "Helper" class returns a list of all parent object rollups to the DynamicRollUpUtility where they are compiles into a List<sObject> to be updated
 **/

public without sharing class DynamicRollupHelper {

    /*
    * @author Micah Johnson - Demand Chain
    * @date 2020
    * @description: Method to retrieve a List of records to be rolled up.
    * @param objectName - Name of Child object
    * @param recordList - List of Child records
    * @param parentObjectName - Name of Parent object
    * @param customMDTList - List of Custom Metadata Type records
    * @return recordUpdateList - List of records to be updated
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * 20210401      Jason Flippen           Refactored to include bulkification and resolve SOQL 101 errors.
    * ====================================================================================================
    */
    public static List<SObject> RollUpHelper(String objectName,
                                             List<SObject> recordList,
                                             String parentObjectName,
                                             List<Custom_Rollup_Rolldown__mdt> customMDTList) {

        Map<Id,SObject> recordUpdateMap = new Map<Id,SObject>();
        
        // Grab a Set of Ids from the Child records being rolled up.
        Set<Id> recordIdSet = new Set<Id>();
        for (SObject record : recordList) {
            recordIdSet.add(record.Id);
        }
        System.debug(LoggingLevel.DEBUG, '***** recordIdSet: ' + recordIdSet);

        Set<String> parentObjects = new Set<String>();                                                
        Set<String> childFieldsSet = new Set<String>();
        for (Custom_Rollup_Rolldown__mdt customMDT : customMDTList) {
            String lowerCaseField = customMDT.Relationship_Field__c.toLowerCase();
            childFieldsSet.add(lowerCaseField);
            parentObjects.add(customMDT.Parent_Object__c);
        }
        System.debug(LoggingLevel.DEBUG, '***** childFieldsSet: ' + childFieldsSet);

        String stringtoSend = '';
        for (String field : childFieldsSet) {
            stringtoSend += field + ',';
        }
        stringtoSend = stringtoSend.removeEnd(',');

        // Get the query string used to query the Child records.
        String childRelatedFieldQueryString = getChildRelatedField(stringtoSend, recordIdSet, objectName);
        System.debug(LoggingLevel.DEBUG, '***** childRelatedFieldQueryString: ' + childRelatedFieldQueryString);

        // Execute the query to retrieve the Child records.
        List<SObject> childQuery = Database.query(childRelatedFieldQueryString);
        System.debug(LoggingLevel.DEBUG, '***** childQuery: ' + childQuery);

        // Iterate through the Custom Metadata Type and the
        // Child records to build Maps to be used later.
        Map<String,Set<String>> metadataParentFieldMap = new Map<String,Set<String>>();
        Map<Id,Set<Id>> metadataParentSetMap = new Map<Id,Set<Id>>();
        for (Custom_Rollup_Rolldown__mdt customMDT: customMDTList) {

            // Build Custom Metadata Type (Id)-to-Parent Rollup Field Set Map.
            if (!metadataParentFieldMap.containsKey(customMDT.Id)) {
                metadataParentFieldMap.put(customMDT.Id, new Set<String>());
                metadataParentFieldMap.get(customMDT.Id).add(customMDT.Aggregate_Result_Field__c);
            }
            else {
                metadataParentFieldMap.get(customMDT.Id).add(customMDT.Aggregate_Result_Field__c);
            }

            // Build Custom Metadata Type (Id)-to-Parent (Id) Set Map.
            if (!metadataParentSetMap.containsKey(customMDT.Id)) {
                metadataParentSetMap.put(customMDT.Id, new Set<Id>());
            }
            for (SObject childQueryRecord : childQuery) {
                String parentId = String.ValueOf(childQueryRecord.get(customMDT.Relationship_Field__c));
                if (String.isNotBlank(parentId)) {
                    metadataParentSetMap.get(customMDT.Id).add(parentId);
                }
            }

        }

        // Iterate through the Custom Metadata Types and the Parent Ids associated to them
        // to build a Custom Metadata (Id)-to-Parent (Id)-to-Child Field Value List Map.
        Map<Id,Map<Id,List<String>>> metadataParentChildFieldValueListMap = new Map<Id,Map<Id,List<String>>>(); 
        for (Custom_Rollup_Rolldown__mdt customMDT : customMDTList) {

            if (!metadataParentChildFieldValueListMap.containsKey(customMDT.Id)) {
                metadataParentChildFieldValueListMap.put(customMDT.Id, new Map<Id,List<String>>());
            }

            // Iterate through the Parents to be updated by the current Custom Metadata Type record.
            for (Id parentId : metadataParentSetMap.get(customMDT.Id)) {

                // Get the query string used to query the records to be rolled up to this Parent.
                String childRollupQuery = CreateChildRollupQuery(customMDT,parentId);
                System.debug(LoggingLevel.DEBUG, '***** childRollupQuery: ' + childRollupQuery);
                
                if (String.isNotBlank(childRollupQuery)) {

                    // Execute the query to retrieve the records to be rolled up to this Parent.
                    List<SObject> queriedChildObject = Database.query(childRollupQuery);
                    System.debug(LoggingLevel.DEBUG, '***** queriedChildObject: ' + queriedChildObject);
                    
                    if (!queriedChildObject.isEmpty()) {

                        // Iterate through the records and add the field values to the
                        // Custom Metadata (Id)-to-Parent (Id)-to-Child Field Value List Map.
                        for (SObject queriedChildObjectRecord : queriedChildObject) {

                            if (!metadataParentChildFieldValueListMap.get(customMDT.Id).containsKey(parentId)) {
                                metadataParentChildFieldValueListMap.get(customMDT.Id).put(parentId, new List<String>());
                            }

                            if (customMDT.Aggregate_Operation__c == 'Sum' || customMDT.Aggregate_Operation__c == 'Count') {
                                metadataParentChildFieldValueListMap.get(customMDT.Id).get(parentId).add(String.valueOf(queriedChildObjectRecord.get('expr0')));
                            }
                            else {
                                metadataParentChildFieldValueListMap.get(customMDT.Id).get(parentId).add(String.valueOf(queriedChildObjectRecord.get(customMDT.Field_to_Aggregate__c)));
                            }

                        }

                    }
                }

            }

        }
        System.debug(LoggingLevel.DEBUG, '***** metadataParentChildFieldValueListMap: ' + metadataParentChildFieldValueListMap);

        // Iterate through the packaged data and add the appropriate records to the Wrapper List.
        List<RelationshipWrapper> wrapperList = new List<RelationshipWrapper>();
        for (String customMDTId : metadataParentChildFieldValueListMap.keySet()) {
            for (Id parentId : metadataParentChildFieldValueListMap.get(customMDTId).keySet()) {
                for (String childFieldValue : metadataParentChildFieldValueListMap.get(customMDTId).get(parentId)) {
                    if (metadataParentFieldMap.containsKey(customMDTId)) {
                        for (String parentField : metadataParentFieldMap.get(customMDTId)) {
                            RelationshipWrapper wrapper = new RelationshipWrapper();
                            wrapper.ParentId = parentId;
                            wrapper.ParentField = parentField;
                            wrapper.ChildResult = childFieldValue;
                            wrapperList.add(wrapper);
                        }
                    }
                }
            }
        }
        System.debug(LoggingLevel.DEBUG, '***** wrapperList: ' + wrapperList);

        // Iterate through the Wrapper List and build the List of records to be updated.
        for (relationshipWrapper wrapper : wrapperList) {

            // Determine the Field (Data) Type.
            Map<String,Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType schemaObjectType = schemaMap.get(parentObjectName);
            Map<String,Schema.SObjectField> fieldMap = schemaObjectType.getDescribe().fields.getMap();
            Schema.DisplayType fieldType = fieldMap.get(wrapper.ParentField).getDescribe().getType();

            Id parentRecordId = wrapper.ParentId;
            
            // Create a new instance of the Parent record to be updated and then check if we're
            // already updating that parent and if so, set the recordUpdate reference to that.
            Boolean recordAlreadyInMap = false;
            SObject recordUpdate = parentRecordId.getSobjectType().newSObject(parentRecordId);
            if (recordUpdateMap.containsKey(parentRecordId)) {
                recordAlreadyInMap = true;
                recordUpdate = recordUpdateMap.get(parentRecordId);
            }

            Boolean addToMap = false;
            system.debug('String.valueOf(fieldType): ' + String.valueOf(fieldType) );
            system.debug('wrapper.ChildResult: ' + wrapper.ChildResult );
            if (String.valueOf(fieldType) == 'DOUBLE' ) {
                ////// Micah Johnson..... 4/6/2021 .... Updated so Zero is included on the rollup////////
                Double childFieldValue = 0;
                if (wrapper.ChildResult != null) {
                    childFieldValue = Double.valueOf(wrapper.ChildResult);
                }
                recordUpdate.put(wrapper.ParentField, childFieldValue);
                if (recordAlreadyInMap == false) {
                    addToMap = true;
                }
            }

            if (String.valueOf(fieldType) == 'TEXT' && wrapper.ChildResult != null) {
                recordUpdate.put(wrapper.ParentField, String.ValueOf(wrapper.ChildResult));
                if (recordAlreadyInMap == false) {
                    addToMap = true;
                }
            }

            if (String.valueOf(fieldType) == 'STRING' && wrapper.ChildResult != null) {
                recordUpdate.put(wrapper.ParentField, String.ValueOf(wrapper.ChildResult));
                if (recordAlreadyInMap == false) {
                    addToMap = true;
                }
            }

            if (String.valueOf(fieldType) == 'DATE' && wrapper.ChildResult != null) {
                recordUpdate.put(wrapper.ParentField, Date.ValueOf(Wrapper.ChildResult));
                if (recordAlreadyInMap == false) {
                    addToMap = true;
                }
            }

            if (String.valueOf(fieldType) == 'DATETIME' && wrapper.ChildResult != null) {
                recordUpdate.put(wrapper.ParentField, Datetime.ValueOf(wrapper.ChildResult));
                if (recordAlreadyInMap == false) {
                    addToMap = true;
                }
            }

            if (String.valueOf(fieldType) == 'CURRENCY') {
                ////// Micah Johnson..... 4/6/2021 .... Updated so Zero is included on the rollup////////
                Double childFieldValue = 0;
                if (wrapper.ChildResult != null) {
                    childFieldValue = Double.valueOf(wrapper.ChildResult);
                }
                recordUpdate.put(wrapper.ParentField, childFieldValue);
                if (recordAlreadyInMap == false) {
                    addToMap = true;
                }
            }

            if (String.valueOf(fieldType) == 'REFERENCE') {
                recordUpdate.put(wrapper.ParentField, Id.ValueOf(wrapper.ChildResult));
                if (recordAlreadyInMap == false) {
                    addToMap = true;
                }
            }

            if (String.valueOf(fieldType) == 'PICKLIST' && wrapper.ChildResult != null) {
                recordUpdate.put(wrapper.ParentField, String.ValueOf(wrapper.ChildResult));
                if (recordAlreadyInMap == false) {
                    addToMap = true;
                }
            }

            // If this records needs to be added to our Update Map then add it.
            if (addToMap == true) {
                recordUpdateMap.put(recordUpdate.Id, recordUpdate);
            }
            
        }
        System.debug(LoggingLevel.DEBUG, '***** recordUpdateMap: ' + recordUpdateMap);

        return recordUpdateMap.values();

    }

    public static string CreateChildRollupQuery(Custom_Rollup_Rolldown__mdt metadata, String idToCheck) {
        string stringToQuery = '';
        if(idToCheck != null){
            system.debug(metadata.Aggregate_Operation__c);
            stringToQuery = 'SELECT ';
            if(metadata.Aggregate_Operation__c != 'First' && metadata.Aggregate_Operation__c != 'Last' && metadata.Aggregate_Operation__c != 'Max'){
                stringToQuery += metadata.Aggregate_Operation__c +'('+ metadata.Field_to_Aggregate__c + ') ';
            }
            else{
                stringToQuery += metadata.Field_to_Aggregate__c;
            }
            
            if(String.ValueOf(metadata.Relationship_Field__c).endsWith('c') && metadata.Aggregate_Operation__c != 'Max' && metadata.Aggregate_Operation__c != 'First' && metadata.Aggregate_Operation__c != 'Last' ){
                stringToQuery = stringToQuery.removeEnd(',');
                stringToQuery += ' FROM '+ String.ValueOf(metadata.Child_Object__c) + ' WHERE '+ metadata.Relationship_Field__c +' = \''+ idToCheck+'\'';
                if(metadata.Relationship_Criteria__c != 'null' && metadata.Relationship_Criteria__c != ''){
                    stringToQuery += ' AND ' +metadata.Relationship_Criteria__c;
                }
            }
            else{ 
                stringToQuery = stringToQuery.removeEnd(',');
                stringToQuery += ' FROM '+ String.ValueOf(metadata.Child_Object__c) + ' WHERE '+ metadata.Relationship_Field__c +' = \''+ idToCheck+'\'';
                if(metadata.Relationship_Criteria__c != 'null' && metadata.Relationship_Criteria__c != ''){
                    stringToQuery += ' AND ' +metadata.Relationship_Criteria__c;
                    if(metadata.Aggregate_Operation__c == 'First' || metadata.Aggregate_Operation__c == 'Last' || metadata.Aggregate_Operation__c == 'Max'){
                        string direction = '';
                        if(metadata.Aggregate_Operation__c == 'First'){
                            direction = 'ASC';
                        }
                        else if(metadata.Aggregate_Operation__c == 'Last'){
                            direction = 'DESC';
                        }
                        else if(metadata.Aggregate_Operation__c == 'Max'){
                            direction = 'DESC';
                        }
                        stringToQuery += ' AND ' + metadata.Field_to_Aggregate__c + ' != NULL ';
                        if(metadata.Field_to_Aggregate__c == 'Primary_Installer_FSL__c' ){
                            stringToQuery += ' ORDER BY ' + 'Scheduled_Start_Time__c ' + ' ' + direction + ' LIMIT 1';
                        }
                        else{
                            stringToQuery += ' ORDER BY ' + metadata.Field_to_Aggregate__c + ' ' + direction + ' LIMIT 1';
                        }
                    }
                }
            }
        }
        return stringToQuery;
    }

    public static String getChildRelatedField(String fieldsToQuery, Set<Id> recordIdSet, String objectName) {
        String queryString = 'SELECT Id, ' + fieldsToQuery + ' FROM ' + objectName + ' WHERE Id IN :recordIdSet';
        return queryString;
    }

    public class RelationshipWrapper {
        public String ParentId;
        public String ParentField;
        public String ChildResult;
    }


}