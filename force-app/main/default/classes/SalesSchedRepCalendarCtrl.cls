/**
 * @File Name          : SalesRepCalendarCtrl.cls
 * @Description        :
 * @Author             : phUser@phDomain.com
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/14/2019, 9:37:14 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    1/21/2019, 2:01:06 PM   phUser@phDomain.com     Initial Version
**/
public with sharing class SalesSchedRepCalendarCtrl {

    //Call
    @AuraEnabled(cacheable=true)
    public static ScheduleWrapper getScheduleWrapper(String monthStart, String salesRepId){
        try{
            ScheduleWrapper sw = new ScheduleWrapper();
            List<Date> dateRange = getDateRange(monthStart);

            System.debug('JWL: salesRepId (param): ' + salesRepId);
            Id salesRepIdCasted = (String.isEmpty(salesRepId)) ? null : (Id)salesRepId;
            //Below loggedInUser variable will capture the logged in user.
			User loggedInUser=SalesSchedUtils.getSalesRep(null);
            User currentUser = SalesSchedUtils.getSalesRep(salesRepIdCasted, dateRange[0], dateRange[1]);
            System.debug('JWL: currentUser: ' + currentUser);
            SalesSchedUtils.OptionWrapper storeOptionWrapper = SalesSchedUtils.getStoreOptions(currentUser, true);
            System.debug('JWL: store option wrapper: ' + storeOptionWrapper);
            System.debug('JWL: store option wrapper size: ' + storeOptionWrapper.options.size());


            SalesSchedUtils.OptionWrapper statusOptionWrapper = SalesSchedUtils.getCapacityStatusOptions();

            sw.title = 'Sales Rep Calendar for ' + currentUser.Name;
            sw.days = getDayWrappers(salesRepIdCasted,currentUser.Sales_Capacities__r, currentUser, loggedInUser, dateRange, storeOptionWrapper);

            if(storeOptionWrapper.options.isEmpty()){
            	storeOptionWrapper.options.add(new SalesSchedUtils.Option('Unavailable','Unavailable'));
            }else{
            	storeOptionWrapper.options.add(0,new SalesSchedUtils.Option('Unavailable','Unavailable'));
            }
            //If the logged in user role is manager then add "Deferred" option
            if(loggedInUser.UserRoleId!=null && loggedInUser.UserRole.Name.contains('Manager'))
            {
                //Below code is added by Ratna, here creating Deferred option for store ----> SS19-432
                SalesSchedUtils.OptionWrapper deferredOptions=new SalesSchedUtils.OptionWrapper();
                for (SalesSchedUtils.Option storeOption:storeOptionWrapper.options) {
                    if (storeOption.value!='Unavailable') {
                    	deferredOptions.options.add(new SalesSchedUtils.Option('Deferred-'+storeOption.value,'Deferred-'+storeOption.label));    
                    }
                }
                if (!deferredOptions.options.isEmpty()) {
                    storeOptionWrapper.options.addAll(deferredOptions.options);
                }
                //Till Here 
            	storeOptionWrapper.options.add(new SalesSchedUtils.Option('Deferred','Deferred'));
            }

            sw.storeOptions = storeOptionWrapper.options;
            sw.statusOptions = statusOptionWrapper.options;
            sw.selectedStoreId = storeOptionWrapper.selectedOption;
            sw.monthStart = getFirstCapacityDate(!String.isEmpty(salesRepId)).toStartOfWeek();
            return sw;
        }
        catch(Exception ex){
            //UtilityMethods.logException(ex,'SalesSchedRepCalendaCtrl.getScheduleWrapper');
            System.debug('Exception message: ' + ex.getMessage());
            System.debug('Exception stack trace: ' + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());
        }
    }

    //Called by component to save capacity information.  Only saves editable capacities.
    @AuraEnabled
    public static void saveCapacities(List<CapacityWrapper> capacityWrappers, String salesRepId){
        Date editableDate = getFirstCapacityDate(!String.isEmpty(salesRepId));
        Set<String> statuses = new Set<String>{'Unavailable','Deferred'};
        List<Sales_Capacity__c> capacities = new List<Sales_Capacity__c>();
        for(CapacityWrapper cw : capacityWrappers){
            if(cw != null && cw.capacity != null && !String.isEmpty(cw.store) && cw.capacity.Date__c > = editableDate){
                if(statuses.contains(cw.store)){
                    cw.capacity.Status__c = cw.store;
                }else if(cw.store.contains('Deferred-')) { //this else-if block added by ratna and purpose is to capture store/status---SS19-432
                    cw.capacity.Status__c = cw.store.split('-')[0];
                    cw.capacity.Store__c = cw.store.split('-')[1];
                }
                else{
                    cw.capacity.Status__c = 'Available';
                    cw.capacity.Store__c = cw.store;
                }
                cw.capacity.OwnerId = cw.capacity.User__c;
                cw.capacity.External_Id__c = getExternalIdForCapacity(cw.capacity.User__c, cw.capacity.Date__c, cw.capacity.Slot__c);
                capacities.add(cw.capacity);
            }
        }
        upsert capacities External_Id__c;
    }

    //Helper method to get the current calendar month range (could include a few days of previous month and next month)
    private static List<Date> getDateRange(String monthStartString){
        Date monthStart = Date.today().toStartOfMonth();
        if(!String.isEmpty(monthStartString)){
            monthStart = Date.valueOf(monthStartString).toStartOfMonth();
        }
         
		//Date Days= monthStart.toStartOfWeek();
        Date calendarBeginning = monthStart.toStartOfWeek();
        Date monthEnd = monthStart.addMonths(1).addDays(-1);
        Date calendarEnd = monthEnd.toStartOfWeek().addDays(6);
        return new List<Date>{calendarBeginning,calendarEnd};
    }

    private static Map<String,List<Slot__c>> getSlotMap(SalesSchedUtils.OptionWrapper storeOptionWrapper){
        Set<Id> storeIds = new Set<Id>();
        for(SalesSchedUtils.Option option : storeOptionWrapper.options){
            storeIds.add(option.value);
        }

        Set<String> slotGroups = new Set<String>();
        for(Account a : [Select Active_Store_Configuration__r.Slot_Group__c From Account Where Id in :storeIds]){
            slotGroups.add(a.Active_Store_Configuration__r.Slot_Group__c);
        }

        Map<String,List<Slot__c>> daySlotMap = new Map<String,List<Slot__c>>();
        for(Slot__c slot : [Select Id,Day__c,Name From Slot__c Where Slot_Group__c in :slotGroups Order By Day__c Asc, Order__c Asc]){
            List<Slot__c> slots = daySlotMap.get(slot.Day__c);
            if(slots == null){
                slots = new List<Slot__c>();
                daySlotMap.put(slot.Day__c,slots);
            }
            slots.add(slot);
        }
        return daySlotMap;
    }

    private static List<DayWrapper> getDayWrappers(Id salesRepId, List<Sales_Capacity__c> capacities, User currentUser,User loggedInUser, List<Date> dateRange,SalesSchedUtils.OptionWrapper storeOptionWrapper){
        Date editableDate = getFirstCapacityDate(!String.isEmpty(salesRepId));
        List<DayWrapper> days = new List<DayWrapper>();
        Map<String,DayWrapper> dayMap = new Map<String,DayWrapper>();

        Map<String,List<Slot__c>> slotMap = new Map<String,List<Slot__c>>();
        if(storeOptionWrapper != null){
            slotMap = getSlotMap(storeOptionWrapper);
        }

        Map<String,CapacityWrapper> wrapperMap = new Map<String,CapacityWrapper>();
        for(Date d = dateRange.get(0); d <= dateRange.get(1); d = d.addDays(1)){
            DayWrapper dw = new DayWrapper(d,editableDate);

            Datetime dt = (Datetime)d;
            List<Slot__c> slots = slotMap.get(dt.formatGmt('EEEE'));
            for(Slot__c slot : slots){
                String externalId = getExternalIdForCapacity(currentUser.Id, d, slot.Id);
                CapacityWrapper cw = new CapacityWrapper(
                    new Sales_Capacity__c(
                        Date__c = dw.day,
                        User__c = currentUser.Id,
                        Slot__c = slot.Id,
                        Slot__r = slot,
                        OwnerId = currentUser.Id,
                        External_Id__c = externalId,
                        Status__c = 'Unavailable'
                    )
                );
                cw.isManager=false;
                if(currentUser.UserRoleId!=null && currentUser.UserRole.Name.contains('Manager'))
                {
                    cw.isManager=true;
                }
                wrapperMap.put(externalId,cw);
                dw.capacityWrappers.add(cw);
            }
            days.add(dw);
        }
		for(Sales_Capacity__c capacity : capacities){
            String externalId = getExternalIdForCapacity(currentUser.Id, capacity.Date__c, capacity.Slot__c);
            System.Debug('JWL: externalId: '+ externalId);
            CapacityWrapper cw = wrapperMap.get(externalId);
            //If the Status is Deferred and logged in user role is not a manager then don't add "Deferred"
            if(capacity.Status__c!=null&&capacity.Status__c=='Deferred' && loggedInUser.UserRoleId!=null && loggedInUser.UserRole.Name.contains('Manager'))
            {
            	cw.store=capacity.Status__c;    
                if(capacity.Store__c!=null) {//this if-block is added by Ratna ---->SS19-432
                    cw.store += '-'+capacity.Store__c; 
                }
            }
            cw.isManager=false;
            if(currentUser.UserRoleId!=null && currentUser.UserRole.Name.contains('Manager'))
            {
                cw.isManager=true;
            }
            //Might be null if the slot_group switched from three slots to five slots after A capacity record was created.
            if(cw != null){
                cw.capacity = capacity;
                if('Available' == cw.capacity.Status__c && cw.capacity.Store__c != null){
                    cw.store = cw.capacity.Store__c;
                }
            }
        }
        return days;
    }

    @AuraEnabled(cacheable=true)
    public static Date getDefaultMonth(String salesRepId){
        Id salesRepIdCasted = (String.isEmpty(salesRepId) ? null : (Id)salesRepId);
        return getFirstCapacityDate(salesRepIdCasted != null);
    }

    //Determines the first date a rep or manager is allowed to modify, as far as capacity is concerned.
    public static Date getFirstCapacityDate(boolean isSalesManager){
        Date today = Date.today();
        if(isSalesManager){
            return today;
        }

        Integer dayOfMonth = 15;
        try{
            User currentRep =
                [Select
                    Contact.Account.Active_Store_Configuration__r.Rep_Schedules_Due__c
                From
                    User
                Where
                    Id = :UserInfo.getUserId()
                    And Contact.Account.Active_Store_Configuration__r.Rep_Schedules_Due__c != null];
            dayOfMonth =
                Integer.valueOf(currentRep.Contact.Account.Active_Store_Configuration__r.Rep_Schedules_Due__c);
        }catch(QueryException qe){}

        Date startOfMonth = today.toStartOfMonth();
        Date fifteenthOfThisMonth = startOfMonth.addDays(dayOfMonth - 1);
        return (today < fifteenthOfThisMonth) ? startOfMonth.addMonths(1) : startOfMonth.addMonths(2);
    }

    private static String getExternalIdForCapacity(Id userId, Date d, Id slotId){
        return userId + '_' + ((Datetime)d).formatGmt('yyyy-MM-dd') + '_' + slotId;
    }

    private static boolean isCapacityReadOnly(Sales_Capacity__c capacity, Date capacityEditableDate, boolean isSalesManager){
        return capacity.Date__c >= capacityEditableDate;
    }

    public class ScheduleWrapper{
        @AuraEnabled public String title {get; set;}
        @AuraEnabled public List<DayWrapper> days {get; set;}
        @AuraEnabled public List<SalesSchedUtils.Option> storeOptions {get; set;}
        @AuraEnabled public List<SalesSchedUtils.Option> statusOptions {get; set;}
        @AuraEnabled public String selectedStoreId {get; set;}
        @AuraEnabled public Date monthStart {get; set;}

        public ScheduleWrapper(){
            this.storeOptions = new List<SalesSchedUtils.Option>();
            this.statusOptions = new List<SalesSchedUtils.Option>();
            this.days = new List<DayWrapper>();
        }
    }

    public class DayWrapper{
        @AuraEnabled public Date day {get; set;}
        @AuraEnabled public List<CapacityWrapper> capacityWrappers {get; set;}
        @AuraEnabled public boolean readOnly {get; set;}

        public DayWrapper(Date day, Date editableDate){
            this.capacityWrappers = new List<CapacityWrapper>();
            this.readOnly = day < editableDate;
            this.day = day;
        }

        public void addCapacityWrapper(CapacityWrapper cw){
            this.addCapacityWrapper(cw);
        }
    }

    public class CapacityWrapper{
        @AuraEnabled public Sales_Capacity__c capacity {get; set;}
        @AuraEnabled public String store {get; set;}
        @AuraEnabled public String key {get; set;}
        @AuraEnabled public Boolean isManager{get;set;}

        public CapacityWrapper(Sales_Capacity__c capacity){
            this.capacity = capacity;
            //this.store = capacity.Store__c;
            String storeValue;
            System.debug('_____capacity.Store__c______'+capacity.Store__c);
            System.debug('_____capacity.Status__c______'+capacity.Status__c);
            if(String.isEmpty(capacity.Store__c)){
                if(capacity.Status__c == 'Deferred'){
                    storeValue = 'Deferred';
                }else{
                    storeValue = 'Unavailable';
                }
            }else{
                capacity.Status__c = 'Available';
                storeValue=capacity.Store__c;
            }
            this.store=storeValue;
            this.key = this.capacity.Date__c + '_' + this.capacity.Slot__c;
            System.debug('_____store______'+this.store);
            System.debug('_____key________'+this.key);
        }

        public CapacityWrapper(){}
    }

    // get the default store location for the login user
    @AuraEnabled(cacheable=true)
    public static String getDefaultStore(String userId){
        String store = '';
        User usr = new USER();
        if(userId != null) {
            usr = [SELECT Default_Store_Location__c FROM USER WHERE ID =:userID LIMIT 1];
        }
        if(usr != null) {
            store = [SELECT Store__c, Id FROM Store_Configuration__c WHERE Store__r.Name =:usr.Default_Store_Location__c LIMIT 1].Store__c;
        }
        return store;
    }
}