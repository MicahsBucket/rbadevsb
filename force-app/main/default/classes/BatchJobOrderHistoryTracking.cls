/******************************************************************
 * Author : Pavan Gunna
 * Class Name :BatchJobOrderHistoryTracking
 * Createdate :
 * Description : Batch Job For Updating Order Status History For Reporting.
 * Change Log
 * ---------------------------------------------------------------------------
 * Date      Name          Description
 * ---------------------------------------------------------------------------
 * 4/24/2020 - Demand Chain BT - Code Review, ApexDoc, Cleanup
 *                               replaced "list<order> ordList" with scope since scope was just copied by loop into ordList.
 * 4/27/2020 - Demand Chain BT - Fixed issue with duplicate Ids in the orders update list.
 *                               Created indication that the order was needing to be updated.
 *                               Found OrderHistory was not generated from test methods, and it's not creatable either.
 *                               Added class OrderHistoryCls to mimic the OrderHistory object so that test data could be provided for needed coverage.
 *                               Finished BatchJobOrderHistoryTrackingTest to gain 94% (146/154) Code Coverage. This included a getTestOrderHistoryData to generate mock OrderHistory data.
 *                               Mock data is retrieved below in a Test.isRunningTest() if statement.
 *
 * ----------------------------------------------------------------------------
 * *****************************************************************/

/**
  * @description Batch Job For Updating Order Status History For Reporting.
  */
global class BatchJobOrderHistoryTracking implements Database.Batchable<sObject> {

    String query;
    Date TodaysDate = date.today();
    Date YesterdaysDate = Date.today().addDays(-3);
    Id serviceRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Service').getRecordTypeId();
	global Date CustomStartDate = null; //'2019-09-01T13:04:19.000Z';
	global Date CustomEndDate = null; //'2019-09-01T13:04:19.000Z';


    /**
      * @description Constructor. Currently Empty.
      */
    global BatchJobOrderHistoryTracking() {
    }

    /**
      * @description Required by the implementation of Database.Batchable. This is executed once at the very begining of the batch execution.
      * @param BC Database.BatchableContext
      * @return
      * @example
      */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String queryStartDate;
        String queryEndDate;
        if (CustomStartDate != null){
            queryStartDate = Datetime.newInstanceGmt(CustomStartDate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');           
        } else{
            queryStartDate = Datetime.newInstanceGmt(YesterdaysDate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');  
        }
        if (CustomEndDate != null){
            queryEndDate = Datetime.newInstanceGmt(CustomEndDate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');            
        }else {
            queryEndDate = Datetime.newInstanceGmt(TodaysDate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        }
        Query = 'SELECT Id, status, recordTypeId, lastmodifieddate, Time_New__c ,Time_Product_Ordered__c ,Time_Service_Scheduled__c ,'+
            'Time_Warranty_Submitted__c ,Time_Warranty_Rejected__c ,Time_Service_on_Hold__c ,Time_Customer_Call_Back__c ,Time_Service_to_be_Scheduled__c ,'+
            'Time_Service_Complete__c ,Time_To_Be_Ordered__c ,Time_Seasonal_Service__c ,Time_Quote__c ,Time_Draft__c,Time_On_Hold__c,'+
            'Time_Tech_Measure_Needed__c ,Time_Tech_Measure_Scheduled__c ,Time_Ready_To_Order__c ,Time_Order_Released__c ,Time_Install_Needed__c ,'+
            'Time_Install_Scheduled__c ,Time_Install_Complete__c ,Time_Job_In_Progress__c ,Time_Job_Close__c ,Job_Close_Date__c ,'+
            'Time_Pending_Cancellation__c ,Time_Cancelled__c, Order_Processed_Date__c  FROM Order WHERE lastmodifieddate > ' + queryStartDate +
            'AND lastmodifieddate < '+ queryEndDate;
        return Database.getQueryLocator(query);
    }

    /**
      * @description Required by the implementation of Database.Batchable. This is executed after the "start" method completes.
      * @param BC Database.BatchableContext
      * @param scope List<Order>
      * @example
      */
    global void execute(Database.BatchableContext BC, List<Order> scope) {
        set<order> ordToUpdate = new set<order>(); //I'm not sure why this was defined as a set instead of a list, but I'll leave it. BT 4/24.
        Map<id, list<OrderHistoryCls>> OrderToHistory = new Map<id,list<OrderHistoryCls>>();

        // Get all the OrderHistory Records
        if(!scope.isEmpty()){
            if(Test.isRunningTest()){
                for(Order ord :scope ){
                    OrderToHistory.put(ord.Id, BatchJobOrderHistoryTrackingTest.getTestOrderHistoryData(ord.Id, (ord.recordTypeId ==  serviceRecordTypeId) ));
                }
            }else{
                for(OrderHistory history: [SELECT CreatedById,CreatedDate,Field,Id,NewValue,OldValue,OrderId FROM OrderHistory WHERE Orderid = : scope AND Field='Status' ORDER By CreatedDate Desc]){
                    if(OrderToHistory.get(history.orderid) == null){
                        OrderToHistory.put(history.orderid, new list<OrderHistoryCls>());
                    }
                    OrderHistoryCls clsHistory = new OrderHistoryCls(history.OrderId, history.Field, String.valueOf(history.NewValue), String.valueOf(history.OldValue), history.CreatedById, history.CreatedDate);
                    OrderToHistory.get(history.orderid).add(clsHistory);
                }
            }
        }
        // Evaluate Order Status Fields from the Order History
        system.debug('Scope outside of loop... ');
        for(Order ord :scope ){
           // system.debug(ord.Status + ' ' + ord.LastModifiedDate );
            if(ord.Status == 'Install Needed'){
            system.debug('Ord inside loop ' + ord.Id + ' status = ' + ord.Status +  'record type id = ' + ord.RecordTypeId + ' service record type id = ' + serviceRecordTypeId);                
            }
            if(OrderToHistory.containsKey(ord.id)){
                Boolean updateToOrderNeeded = false;
                for(OrderHistoryCls history: OrderToHistory.get(ord.id)) {
                    if( ord.recordTypeId !=  serviceRecordTypeId ){
                        /* commented out by mtr 7-6-20 during testing. seems redundent. if adding back in remove the closing parenthesis and opening code bracket from the line above ){  
                       * && (  (history.NewValue == 'Draft' && ((ord.Time_Draft__c < history.CreatedDate && ord.Time_Draft__c != history.CreatedDate) || ord.Time_Draft__c== null))
                                ||(history.NewValue == 'On Hold' && ((ord.Time_On_Hold__c < history.CreatedDate && ord.Time_On_Hold__c != history.CreatedDate) || ord.Time_On_Hold__c == null))
                                ||(history.NewValue == 'Tech Measure Needed' && ((ord.Time_Tech_Measure_Needed__c < history.CreatedDate && ord.Time_Tech_Measure_Needed__c != history.CreatedDate) || ord.Time_Tech_Measure_Needed__c== null))
                                ||(history.NewValue == 'Tech Measure Scheduled' && ((ord.Time_Tech_Measure_Scheduled__c < history.CreatedDate && ord.Time_Tech_Measure_Scheduled__c != history.CreatedDate) || ord.Time_Tech_Measure_Scheduled__c ==null))
                                ||(history.NewValue == 'Ready to Order' && ((ord.Time_Ready_To_Order__c < history.CreatedDate && ord.Time_Ready_To_Order__c != history.CreatedDate) || ord.Time_Ready_To_Order__c== null))
                                ||(history.NewValue == 'Order Released' && ((ord.Time_Order_Released__c < history.CreatedDate && ord.Time_Order_Released__c != history.CreatedDate) || ord.Time_Order_Released__c == null))
                                ||(history.NewValue == 'Install Needed' && ((ord.Time_Install_Needed__c < history.CreatedDate && ord.Time_Install_Needed__c != history.CreatedDate) ||ord.Time_Install_Needed__c == null))
                                ||(history.NewValue == 'Install Scheduled' && ((ord.Time_Install_Scheduled__c < history.CreatedDate && ord.Time_Install_Scheduled__c != history.CreatedDate) || ord.Time_Install_Scheduled__c == null))
                                ||(history.NewValue == 'Install Complete' && ((ord.Time_Install_Complete__c < history.CreatedDate && ord.Time_Install_Complete__c != history.CreatedDate) || ord.Time_Install_Complete__c == null))
                                ||(history.NewValue == 'Job in Progress'  && ((ord.Time_Job_In_Progress__c < history.CreatedDate && ord.Time_Job_In_Progress__c != history.CreatedDate) || ord.Time_Job_In_Progress__c== null))
                                ||(history.NewValue == 'Job Closed' && ((ord.Time_Job_Close__c < history.CreatedDate && ord.Time_Job_Close__c != history.CreatedDate) || ord.Time_Job_Close__c== null))
                                ||(history.NewValue == 'Pending Cancellation' && ((ord.Time_Pending_Cancellation__c < history.CreatedDate && ord.Time_Pending_Cancellation__c != history.CreatedDate) || ord.Time_Pending_Cancellation__c== null))
                                ||(history.NewValue == 'Cancelled' && ((ord.Time_Cancelled__c < history.CreatedDate && ord.Time_Cancelled__c != history.CreatedDate) || ord.Time_Cancelled__c == null))
                                ||(history.NewValue != 'Draft' && history.OldValue == 'Draft' && ((ord.Order_Processed_Date__c < history.CreatedDate && ord.Order_Processed_Date__c != history.CreatedDate) || ord.Order_Processed_Date__c== null))
                            )
                    ){*/
                        if(history.NewValue == 'Draft' && ((ord.Time_Draft__c < history.CreatedDate && ord.Time_Draft__c != history.CreatedDate) || ord.Time_Draft__c== null)){
                            ord.Time_Draft__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'On Hold' && ((ord.Time_On_Hold__c < history.CreatedDate && ord.Time_On_Hold__c != history.CreatedDate) || ord.Time_On_Hold__c == null)){
                            ord.Time_On_Hold__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Tech Measure Needed' && ((ord.Time_Tech_Measure_Needed__c < history.CreatedDate && ord.Time_Tech_Measure_Needed__c != history.CreatedDate) || ord.Time_Tech_Measure_Needed__c== null)){
                            ord.Time_Tech_Measure_Needed__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Tech Measure Scheduled' && ((ord.Time_Tech_Measure_Scheduled__c < history.CreatedDate && ord.Time_Tech_Measure_Scheduled__c != history.CreatedDate) || ord.Time_Tech_Measure_Scheduled__c ==null)){
                            ord.Time_Tech_Measure_Scheduled__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Ready to Order' && ((ord.Time_Ready_To_Order__c < history.CreatedDate && ord.Time_Ready_To_Order__c != history.CreatedDate) || ord.Time_Ready_To_Order__c== null)){
                            ord.Time_Ready_To_Order__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Order Released' && ((ord.Time_Order_Released__c < history.CreatedDate && ord.Time_Order_Released__c != history.CreatedDate) || ord.Time_Order_Released__c == null)){
                            ord.Time_Order_Released__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        system.debug('above install needed in batch job for order id of ' + ord.Id + 'with status of '+ ord.Status);
                        if(history.NewValue == 'Install Needed' && ((ord.Time_Install_Needed__c < history.CreatedDate && ord.Time_Install_Needed__c != history.CreatedDate) ||ord.Time_Install_Needed__c == null)){
                            system.debug('Hit install needed in batch job for order id of ' + ord.Id);
                            ord.Time_Install_Needed__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Install Scheduled' && ((ord.Time_Install_Scheduled__c < history.CreatedDate && ord.Time_Install_Scheduled__c != history.CreatedDate) || ord.Time_Install_Scheduled__c == null)){
                            ord.Time_Install_Scheduled__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Install Complete' && ((ord.Time_Install_Complete__c < history.CreatedDate && ord.Time_Install_Complete__c != history.CreatedDate) || ord.Time_Install_Complete__c == null)){
                            ord.Time_Install_Complete__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Job Closed' && ((ord.Time_Job_Close__c < history.CreatedDate && ord.Time_Job_Close__c != history.CreatedDate) || ord.Time_Job_Close__c== null)){
                            ord.Time_Job_Close__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Job Closed' && ((ord.Job_Close_Date__c < history.CreatedDate && ord.Job_Close_Date__c != history.CreatedDate) || ord.Job_Close_Date__c== null)){
                            ord.Job_Close_Date__c = Date.valueof(history.CreatedDate);
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Pending Cancellation' && ((ord.Time_Pending_Cancellation__c < history.CreatedDate && ord.Time_Pending_Cancellation__c != history.CreatedDate) || ord.Time_Pending_Cancellation__c== null)){
                            ord.Time_Pending_Cancellation__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Cancelled' && ((ord.Time_Cancelled__c < history.CreatedDate && ord.Time_Cancelled__c != history.CreatedDate) || ord.Time_Cancelled__c == null)){
                            ord.Time_Cancelled__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Job in Progress'  && ((ord.Time_Job_In_Progress__c < history.CreatedDate && ord.Time_Job_In_Progress__c != history.CreatedDate) || ord.Time_Job_In_Progress__c== null)){
                            ord.Time_Job_In_Progress__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue != 'Draft' && history.OldValue == 'Draft' && ((ord.Order_Processed_Date__c < history.CreatedDate && ord.Order_Processed_Date__c != history.CreatedDate) || ord.Order_Processed_Date__c== null)){
                            ord.Order_Processed_Date__c = Date.valueof(history.CreatedDate);
                            updateToOrderNeeded = true;
                        }
                    }else if(ord.recordTypeId ==  serviceRecordTypeId ){
                        /* commented out by mtr 7-6-20 during testing. seems redundent. if adding back in remove the closing parenthesis and opening code bracket from the line above ){  
                        /*&& Quote Product Ordered Service Scheduled Warranty Submitted Warranty Rejected Service On Hold Customer Call Back Service To Be Scheduled Service Complete To Be Ordered Seasonal Service
                                ( (history.NewValue == 'New'  && ((ord.Time_New__c < history.CreatedDate && ord.Time_New__c != history.CreatedDate || ord.Time_New__c== null)))
                                    ||(history.NewValue == 'Product Ordered'  && ((ord.Time_Product_Ordered__c < history.CreatedDate && ord.Time_Product_Ordered__c != history.CreatedDate) || ord.Time_Product_Ordered__c== null))
                                    ||(history.NewValue == 'Service Scheduled'  && (( ord.Time_Service_Scheduled__c < history.CreatedDate && ord.Time_Service_Scheduled__c != history.CreatedDate) || ord.Time_Service_Scheduled__c == null))
                                    ||(history.NewValue == 'Warranty Submitted'  && ((ord.Time_Warranty_Submitted__c < history.CreatedDate && ord.Time_Warranty_Submitted__c != history.CreatedDate) || ord.Time_Warranty_Submitted__c==null))
                                    ||(history.NewValue == 'Warranty Rejected'  && ((ord.Time_Warranty_Rejected__c < history.CreatedDate && ord.Time_Warranty_Rejected__c != history.CreatedDate) || ord.Time_Warranty_Rejected__c == null))
                                    ||(history.NewValue == 'Service On Hold'  && ((ord.Time_Service_on_Hold__c < history.CreatedDate && ord.Time_Service_on_Hold__c != history.CreatedDate) || ord.Time_Service_on_Hold__c == null))
                                    ||(history.NewValue == 'Customer Call Back'  && (( ord.Time_Customer_Call_Back__c < history.CreatedDate && ord.Time_Customer_Call_Back__c != history.CreatedDate) || ord.Time_Customer_Call_Back__c== null))
                                    ||(history.NewValue == 'Service To Be Scheduled'  && ((ord.Time_Service_to_be_Scheduled__c < history.CreatedDate && ord.Time_Service_to_be_Scheduled__c != history.CreatedDate) || ord.Time_Service_to_be_Scheduled__c== null))
                                    ||(history.NewValue == 'Service Complete'  && ((ord.Time_Service_Complete__c < history.CreatedDate && ord.Time_Service_Complete__c != history.CreatedDate) || ord.Time_Service_Complete__c == null))
                                    ||(history.NewValue == 'To Be Ordered'  && ((ord.Time_To_Be_Ordered__c < history.CreatedDate && ord.Time_To_Be_Ordered__c != history.CreatedDate) || ord.Time_To_Be_Ordered__c == null))
                                    ||(history.NewValue == 'Seasonal Service'  && ((ord.Time_Seasonal_Service__c < history.CreatedDate && ord.Time_Seasonal_Service__c != history.CreatedDate) || ord.Time_Seasonal_Service__c == null))
                                    ||(history.NewValue == 'Quote'  && ((ord.Time_Quote__c < history.CreatedDate && ord.Time_Quote__c != history.CreatedDate) || ord.Time_Quote__c== null))
                        )){*/
                        if(history.NewValue == 'New'  && ((ord.Time_New__c < history.CreatedDate && ord.Time_New__c != history.CreatedDate || ord.Time_New__c== null))){
                            ord.Time_New__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Product Ordered'  && ((ord.Time_Product_Ordered__c < history.CreatedDate && ord.Time_Product_Ordered__c != history.CreatedDate) || ord.Time_Product_Ordered__c== null)){
                            ord.Time_Product_Ordered__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Service Scheduled'  && (( ord.Time_Service_Scheduled__c < history.CreatedDate && ord.Time_Service_Scheduled__c != history.CreatedDate) || ord.Time_Service_Scheduled__c == null)){
                            ord.Time_Service_Scheduled__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Warranty Submitted'  && ((ord.Time_Warranty_Submitted__c < history.CreatedDate && ord.Time_Warranty_Submitted__c != history.CreatedDate) || ord.Time_Warranty_Submitted__c==null)){
                            ord.Time_Warranty_Submitted__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Warranty Rejected'  && ((ord.Time_Warranty_Rejected__c < history.CreatedDate && ord.Time_Warranty_Rejected__c != history.CreatedDate) || ord.Time_Warranty_Rejected__c == null)){
                            ord.Time_Warranty_Rejected__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Service On Hold'  && ((ord.Time_Service_on_Hold__c < history.CreatedDate && ord.Time_Service_on_Hold__c != history.CreatedDate) || ord.Time_Service_on_Hold__c == null)){
                            ord.Time_Service_on_Hold__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Customer Call Back'  && (( ord.Time_Customer_Call_Back__c < history.CreatedDate && ord.Time_Customer_Call_Back__c != history.CreatedDate) || ord.Time_Customer_Call_Back__c== null)){
                            ord.Time_Customer_Call_Back__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Service To Be Scheduled'  && ((ord.Time_Service_to_be_Scheduled__c < history.CreatedDate && ord.Time_Service_to_be_Scheduled__c != history.CreatedDate) || ord.Time_Service_to_be_Scheduled__c== null)){
                            ord.Time_Service_to_be_Scheduled__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Service Complete'  && ((ord.Time_Service_Complete__c < history.CreatedDate && ord.Time_Service_Complete__c != history.CreatedDate) || ord.Time_Service_Complete__c == null)){
                            ord.Time_Service_Complete__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'To Be Ordered'  && ((ord.Time_To_Be_Ordered__c < history.CreatedDate && ord.Time_To_Be_Ordered__c != history.CreatedDate) || ord.Time_To_Be_Ordered__c == null)){
                            ord.Time_To_Be_Ordered__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Seasonal Service'  && ((ord.Time_Seasonal_Service__c < history.CreatedDate && ord.Time_Seasonal_Service__c != history.CreatedDate) || ord.Time_Seasonal_Service__c == null)){
                            ord.Time_Seasonal_Service__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                        if(history.NewValue == 'Quote'  && ((ord.Time_Quote__c < history.CreatedDate && ord.Time_Quote__c != history.CreatedDate) || ord.Time_Quote__c== null)){
                            ord.Time_Quote__c = history.CreatedDate;
                            updateToOrderNeeded = true;
                        }
                    }
                }
                if(updateToOrderNeeded == true){
                    ordToUpdate.add(ord);
                }
            }
        }
        if(!ordToUpdate.isEmpty()){
            List<order> ordToUpdateList = new List<order>(ordToUpdate);
            Update ordToUpdateList ;
        }
    }

    /**
      * @description Required by the implementation of Database.Batchable. This is executed after the "execute" method completes.  Currently Empty.
      * @param BC Database.BatchableContext
      * @example
      */
    global void finish(Database.BatchableContext BC) {
    }


    
    /**
      * @description This class mimics the OrderHistory Object. The reason it is needed is for Test Method Execution.
      */
    global class OrderHistoryCls{
        //Created: 4/27/2020 - Demand Chain, Brian Tremblay
        //This class mimics the OrderHistory Object.
        //The reason it is needed is for Test Method Execution.
        //Test Methods do not create History records, and they are not creatable
        //  with NewValue because the field is not writable, so using this in its place
        //  allows us to create test data to mimic the data that will be otherwise
        //  created in normal use. (*) indicates a required property.

        global Id OrderId {get;set;} //(*)recordId for the order that was modified
        global String Field {get;set;} //(*)name of the field that was modified
        global String NewValue {get;set;} //(*)value of the field was changed to
        global String OldValue {get;set;} //value of the field was changed from
        global Datetime CreatedDate {get;set;} //date of the modification
        global Id CreatedById {get;set;} //who made the modification

        global OrderHistoryCls(Id xOrderId, String xField, String xNewValue, String xOldValue, Id xCreatedById, Datetime xCreatedDate){
            this.OrderId = xOrderId;
            this.Field = xField;
            this.NewValue = xNewValue;
            this.OldValue = xOldValue;
            this.CreatedById = xCreatedById;
            this.CreatedDate = xCreatedDate;
            if(CreatedDate == null){
                this.CreatedDate = system.now();
            }
        }
    }
}