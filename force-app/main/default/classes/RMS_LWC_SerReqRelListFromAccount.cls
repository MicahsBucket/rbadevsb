/*
* @author Jason Flippen
* @date 11/30/2020 
* @description Class to provide functionality for the serReqRelListFromAccounts & serviceRequestButtons LWCs.
*
*              Test code coverage provided by the following Test Class:
*			   - RMS_LWC_SerReqRelListFromAccountTest
*/
public class RMS_LWC_SerReqRelListFromAccount {
    
/*
    @AuraEnabled(cacheable=true)
    public static string getAccountId(String accId) {
        return accId;
    }
*/

    @AuraEnabled(cacheable=true)
    public static Boolean assetExits(String accId) {
        Boolean returnValue = false;
        List<Asset> assets = [SELECT Id FROM Asset WHERE AccountId = : accId];
        if (assets.size() > 0) {
            returnValue = true;
        }
        return returnValue;
    }

    @AuraEnabled(cacheable=true)
    public static List<Order> fetchOrders(String accId) {

        Id idCoroRecType = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Record_Type').getRecordTypeId();

        List<Order> orders = [SELECT Id,
                                     Activated_Date__c,
                                     Installation_Date__c,
                                     OrderNumber,
                                     Revenue_Recognized_Date__c,
                                     Status
                              FROM   Order 
                              WHERE  AccountId =: accId 
                              AND    RecordTypeId =: idCoroRecType
                              AND    Status NOT IN('Cancelled')
                              ORDER BY Installation_Date__c DESC];
        system.debug('@@@@@orders '+orders);

        return orders;

    }

    @AuraEnabled(cacheable=true)
    public static List<ServiceRequest> getServiceReqs(String pAccId){
        
        Id idServReqRecType = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Service').getRecordTypeId();

        system.debug('### pAccId: '+pAccId);
        List<ServiceRequest> listServReqs = new List<ServiceRequest>();
        for(Order loopOrder : [SELECT Id,Name,OrderNumber,Status,EffectiveDate,Contract.Name,ContractId,TotalAmount,Amount_Due__c,
                                Retail_Total__c,RecordType.Name,Sold_Order__c,Description,Service_Type__c,Sold_Order__r.OrderNumber, Account.No_Service__c
                                FROM Order WHERE AccountId = :pAccId AND RecordTypeId = :idServReqRecType 
                                ORDER BY CreatedDate Desc]){
            listServReqs.add(new ServiceRequest(loopOrder));
        }
        return listServReqs;
    }

    @AuraEnabled(cacheable=true)
    public static string allValidationsCheck(String pAccId){
       
       string ErrorMessage = null ; 
       
         Account Acc = [select id,No_Service__c, ShippingPostalCode, shippingCountry , shippingCountryCode  from Account  where id =: pAccId limit 1] ;
         string formattedPostalCode ;
          if ((Acc.ShippingCountry != null && (Acc.ShippingCountry.equalsIgnoreCase('us') || Acc.ShippingCountry.equalsIgnoreCase('usa') || Acc.ShippingCountry.equalsIgnoreCase('united states')
                        || Acc.ShippingCountry.equalsIgnoreCase('u.s.') || Acc.ShippingCountry.equalsIgnoreCase('u.s.a.') || Acc.ShippingCountry.equalsIgnoreCase('united states of america')))
                        || (Acc.ShippingCountryCode != null && Acc.ShippingCountryCode.equalsIgnoreCase('us'))) {

                    formattedPostalCode = Acc.ShippingPostalCode.substring(0, 5);
                } else if ((Acc.ShippingCountry != null && (Acc.ShippingCountry.equalsIgnoreCase('ca') || Acc.ShippingCountry.equalsIgnoreCase('canada')))
                        || (Acc.ShippingCountryCode != null && Acc.ShippingCountryCode.equalsIgnoreCase('ca'))) {
                    formattedPostalCode = Acc.ShippingPostalCode.deleteWhitespace().remove('-').toUpperCase();
                }
           list<Service_Territory_Zip_Code__c > Zipcodes = [SELECT Id, Name FROM Service_Territory_Zip_Code__c WHERE Name =: formattedPostalCode Limit 1 ];
          
           If (Acc.No_Service__c){
               ErrorMessage = 'This Account is not serviceable. Please see the No Service Notes for additional information';
           } else if(Zipcodes.isEmpty()){
               //ErrorMessage = 'You cannot create a Service Request for this Account.  This Account zip code is missing a corresponding Service Territory Zip Code record.Please contact rSupport for assistance.';
           }
           
           
       return ErrorMessage;
       
    }
    
/*
    @AuraEnabled(cacheable=true)
    public static String prepareViewAllURL(String pAccId){
        String strURL;
        String strAccName;
        String strRecTypeJSON;
        Set<Id> recTypeIds;
        
        system.debug('### pAccId: '+pAccId);
        recTypeIds = new Set<Id>();
        recTypeIds.add(UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order'));
        strRecTypeJSON = JSON.serialize(recTypeIds);
        strAccName = [SELECT Id,Name FROM Account WHERE Id = :pAccId].Name;
        strURL = '/apex/RMS_ViewAllServiceOrdersForAcc?id='+pAccId+'&recTypeIds='+strRecTypeJSON+'&accName='+strAccName+'&console=false';
        system.debug('### strURL: '+strURL);
        return strURL;
    }
    
    @AuraEnabled(cacheable=true)
    public static boolean assetsforAccount(String pAccId){
        
      List<Asset>  Asset = [select id from asset where Accountid =: pAccId];
      boolean Assetexist = false ;
      system.debug('@pavan pAccId'+pAccId);
      system.debug('@pavan Asset'+Asset);
       decimal Assetsize = Integer.valueof( Asset.size());
       system.debug('@pavan Assetsize'+Assetsize);
       if (Assetsize == 0){
         Assetexist = false;
       } else {
           Assetexist = True ;
       }
       return Assetexist ;
       
    }
    
    @AuraEnabled(cacheable=true)
    public static boolean accountNoService(String pAccId){
        
       Boolean NoService = [select id,No_Service__c from Account  where id =: pAccId limit 1].No_Service__c;
       
       return NoService ;
       
    }
*/

    public class ServiceRequest{
        @AuraEnabled
        public String strSoldOrderNumber{get;set;}
        @AuraEnabled
        public String strSoldOrderId{get;set;}
        @AuraEnabled
        public String strEditURL{get;set;}
        @AuraEnabled
        public Order objOrder{get;set;}
        @AuraEnabled
        public String RequestNumber{get;set;}
        @AuraEnabled
        public String SoldOrder{get;set;}
        @AuraEnabled
        public String Status{get;set;}
        @AuraEnabled
        public date EffectiveDate{get;set;}
        @AuraEnabled
        public String ServiceType{get;set;}
        @AuraEnabled
        public Decimal AmountDue {get;set;}
        @AuraEnabled
        public String Description{get;set;}
        @AuraEnabled
        public String EditText{get;set;}
        @AuraEnabled
        public String RequestNumberURL{get;set;}
        @AuraEnabled
        public boolean NoService{get;set;}
        @AuraEnabled
        public decimal Assetsize {get;set;}
        @AuraEnabled
        public String RequestId{get;set;}

        public ServiceRequest(Order pObjOrder){
            objOrder = pObjOrder;
            strEditURL = '/apex/RMS_editServiceRequest?id=' +pObjOrder.Id;
            strSoldOrderId = objOrder.Sold_Order__c;
            strSoldOrderNumber = objOrder.Sold_Order__r.OrderNumber ;
            EditText = 'Edit';
            RequestId = pObjOrder.Id;
            RequestNumberURL = '/'+pObjOrder.Id;
            RequestNumber = objOrder.OrderNumber ;
            SoldOrder = objOrder.Sold_Order__r.OrderNumber ;
            Status = objOrder.Status;
            EffectiveDate =  objOrder.EffectiveDate ;
            ServiceType = objOrder.Service_Type__c ;
            AmountDue =  objOrder.Amount_Due__c;
            Description = objOrder.Description;
            NoService = objOrder.Account.No_Service__c ;
            //Assetsize = [select count() from asset where Accountid =: pObjOrder.Accountid];
        }
    }

}