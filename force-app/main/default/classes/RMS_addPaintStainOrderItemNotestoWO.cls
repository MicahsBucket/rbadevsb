public with sharing class RMS_addPaintStainOrderItemNotestoWO {

    public void addPaintStainNotesWO (List<WorkOrder> listNew){

        //List of ids for the related Sold Orders
        List<Id> relatedOrder = new list<Id>();

        //for the new RbA Work Order, add to the related Order Id list the Sold Order field on the WO
        for(WorkOrder wo : listNew){
            if(wo.Sold_Order__c != null && wo.Work_Order_Type__c == 'Paint/Stain'){
                relatedOrder.add(wo.Sold_Order__c);
            }
        }

        //Loops through the Order Items to get the Notes and Name, but not for Cancelled Order Items
        List<String> oiNotes = new List<String>();
        map<id, list<String>> orderToNotes = new map<id, list<String>> ();

        for(OrderItem oi : [SELECT  Id, Notes__c, Product2.Name, Status__c, OrderId, Product2.recordtype.Name
                            FROM OrderItem
                            WHERE OrderId = :relatedOrder
                            AND Status__c != 'Cancelled'
                            AND Product2.recordtype.Name = 'Misc Job and Unit Charges'
                            AND ((Product2.Name LIKE '%Paint%') OR (Product2.Name LIKE '%Stain%')) ]){
            // test to see if Long Text field is null
            if ( String.isNotBlank(oi.Notes__c)){
                oiNotes.add(oi.Product2.Name+'-- '+oi.Notes__c+'\n'+'\n'+'linebreak');
                if(orderToNotes.containsKey(oi.OrderId)){
                    orderToNotes.remove(oi.OrderId);
                    orderToNotes.put(oi.OrderId, oiNotes);
                }else{
                    orderToNotes.put(oi.OrderId, oiNotes);
                }
            }
        }

        for(WorkOrder wo : listNew){
            if(wo.Work_Order_Type__c == 'Paint/Stain' && orderToNotes.containsKey(wo.Sold_Order__c)){
                wo.Paint_Stain_Notes__c = String.valueOf(orderToNotes.get(wo.Sold_Order__c)).removeEnd(')').removeStart('(').remove('linebreak,').remove('linebreak');
            }
        }
    }
}