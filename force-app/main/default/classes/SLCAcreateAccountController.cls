@RestResource(urlMapping='/SLCAcreateAccount/*')
global with sharing class SLCAcreateAccountController
{

global class AccountResponseClass {
        public String accountId;
        public String status;
        public String message;
    }

    @HttpPost
      global static AccountResponseClass createNewAccount(RequestWrapper rqst) {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        AccountResponseClass response = new AccountResponseClass();
        
            //Create Account
            Account a             = new Account();
            String accid = rqst.acct.accountZip+', '+rqst.acct.accountStreet;
            List<Account> searchResults = [SELECT Id, Name FROM Account WHERE Name LIKE :(accid + '%') ];
            
            if(searchResults != null && searchResults.size() > 0) {
               a = searchResults[0];
               response.accountId = a.Id;
            }
            else{
                a.Name                = rqst.acct.accountName;
                a.Type                = 'New';
                a.BillingCountry      = rqst.acct.accountCountry;
                a.BillingCountryCode  = rqst.acct.accountCountryCode;
                a.ShippingStreet      = rqst.acct.accountStreet;
                a.ShippingCity        = rqst.acct.accountCity;
                a.ShippingState       = rqst.acct.accountState;
                a.ShippingPostalCode  = rqst.acct.accountZip;
                a.ShippingCountry     = rqst.acct.accountCountry;
                a.ShippingState       = rqst.acct.accountState;
                a.ShippingStateCode   = rqst.acct.accountStateCode;
                a.ShippingCountryCode = rqst.acct.accountCountryCode;
                a.Store_Location__c   = rqst.acct.accountStoreLocation; 
                a.RecordTypeId        = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dwelling').getRecordTypeId();
                
                try {
                    insert a;
                }
                catch(Exception exc) {
                    res.StatusCode     = 500;
                    response.accountId = '';
                    response.status    = 'Error';
                    response.message   = 'Your request failed with the following error: ' + exc.getMessage();
                }
            }
            

            //Create Account
            String accointid = a.Id;
            String coApplicantID = '';
            String applicantID = '';


            for(ContactWrapper con : rqst.contList) {
                    Boolean contactInfoExists = false;
                    Contact c     = null;
                    
                    List<Contact> contactList     = [SELECT Id FROM Contact WHERE LastName LIKE :con.contactLastName  AND FirstName LIKE :con.contactFirstName AND accountId = :a.Id];
                    if(contactList != null && contactList.size() > 0) {
                      c =    contactList[0];
                      contactInfoExists = true;
                    }else{
                        c     = new Contact();
                        c.accountId   = a.Id;
                        c.LastName    = con.contactLastName;
                        c.FirstName   = con.contactFirstName;  
                    }
                    
                    c.HomePhone   = con.contactHomePhone;
                    c.MobilePhone = con.contactMobilePhone;
                    c.Email       = con.contactEmail;
                    c.Birthdate   = Date.valueOf(con.contactBirthdate);

                    c.MailingStreet     = con.contactStreet;
                    c.MailingCity       = con.contactCity;
                    c.MailingState      = con.contactState;
                    c.MailingPostalCode = con.contactState;
                    c.MailingStateCode  = con.contactStateCode;

                    c.LASERCA__Home_Address__c   = con.contactStreet;
                    c.LASERCA__Home_City__c      = con.contactCity;
                    c.LASERCA__Home_Country__c   = con.contactState;
                    c.LASERCA__Home_State__c     = con.contactStateCode;
                    c.LASERCA__Home_Zip__c       = con.contactPostalCode;
                    c.LASERCA__Middle_Initial__c = con.contactMiddleInitial;
                    c.LASERCA__Social_Security_Number__c = con.contactSsn;
                    c.Employer_Phone_Number__c   = con.contactEmployerPhone;

                    c.Employer__c                 = con.contactEmployer;
                    c.Position__c                 = con.contactPosition;
                    c.Applicant_ID__c             = decimal.valueOf(con.contactCAAppID);
                    c.Applicant_ID_Type__c        = con.contactApplicantIDType;
                    c.ID_Issuing_State__c         = con.contactIDIssuingState;
                    c.Applicant_Name_On_ID__c     = con.contactNameOnID;
                    c.ID_Number__c                = con.contactApplicantID;

                    if(con.contactIDExpDate != '0000-00-00'){
                        c.ID_Expiration_Date__c   = Date.valueOf(con.contactIDExpDate);
                    }


                    if(con.mainApplicant == '0'){
                       
                    
                        try{
                            if(contactInfoExists){
                                update c;
                            }else{
                                insert c;
                            }
                        }
                        catch(Exception exc) {
                            res.StatusCode     = 500;
                            response.accountId = '';
                            response.status    = 'Error';
                            response.message   = 'Your request failed with the following error: ' + exc.getMessage();
                        }
                        coApplicantID = c.Id;
                    }else{
                        c.Customer_Authorization_Date__c  = Date.valueOf(con.ContactDate);
                        c.Has_Customer_Authorization_Picklist__c = 'Yes';
                        c.Has_Finance_Application_Picklist__c    = 'Yes';
                        c.Finance_Pending_Picklist__c            = 'Yes'; 
                        c.Mortgage_Account_Number__c  = con.contactMortAccNumber;
                        c.Mortgage_Lender__c          = con.contactMortLender;
                        c.Mortgage_Monthly_Payment__c = decimal.valueOf(con.contactMortMontPayment);
                        if(coApplicantID != ''){
                            c.Co_Applicant__c = coApplicantID;
                        }
                        try{
                            if(contactInfoExists){
                                update c;
                            }else{
                                insert c;
                            }
                        }
                        catch(Exception exc) {
                            res.StatusCode     = 500;
                            response.accountId = '';
                            response.status    = 'Error';
                            response.message   = 'Your request failed with the following error: ' + exc.getMessage();
                        }
                        applicantID = c.Id;
                    }

            }
            rqst.finance.Customer__c = applicantID;
            if(coApplicantID != ''){
                rqst.finance.Co_Applicant_Borrower__c = coApplicantID;
            }
            if(rqst.finance.Name !=''){
                insert rqst.finance; 
            }

            response.accountId = a.Id;
            res.StatusCode     = 200;
            response.status    = 'Success';
            response.message   = 'Your Accounts have been created successfully';
        
        
        return response;
    }

    global class RequestWrapper {
        public AccountWrapper acct;
        public List<ContactWrapper> contList;
        public Finance_Application__c finance;
    }

    global class AccountWrapper{
        public String accountName;
        public String accountCountry;
        public String accountCountryCode;
        public String accountStreet;
        public String accountCity;
        public String accountState;
        public String accountStateCode;
        public String accountZip;
        public String accountStoreLocation;
    }

    global class ContactWrapper{
        public String contactaccountID = Null;
        public String contactCAAppID;
        public String contactLastName;
        public String contactFirstName;
        public String contactHomePhone;
        public String contactMobilePhone;
        public String contactEmail;
        public String contactBirthdate;
        public String contactMiddleInitial;
        public String contactSsn;
        public String contactStreet;
        public String contactCity;
        public String contactState;
        public String contactStateCode;
        public String contactPostalCode;
        public String contactEmployerPhone;
        public String contactEmployer;
        public String contactPosition;
        public String contactApplicantID;
        public String contactApplicantIDType;
        public String contactIDIssuingState;
        public String contactNameOnID;
        public String contactIDExpDate;
        public String contactMortAccNumber;
        public String contactMortLender;
        public String contactMortMontPayment;
        public String ContactDate;
        public String mainApplicant;
    }
}