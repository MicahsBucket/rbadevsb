public with sharing class RMS_uploadOrderExtension {

    public Id oppId{get;set;}
    public boolean editModePendingOrderItems {get;set;}
    public List<OrderItem> pendingOrderItems {get;set;}
    public Opportunity myOpp {get;set;}

    public Attachment projFile {
        get {
            if (projFile == null){
                projFile = new Attachment();
            }
            return projFile;
        }
        set;
    }


    public boolean pageHasOppId {
        get{
            return String.isNotBlank(oppId);
        }
    }

    public boolean fileHasBeenUploaded {
        get{
            boolean returnBool =false;
            for(Attachment a : [SELECT Name FROM Attachment WHERE ParentId = :oppId]){
                if(a.Name.toLowerCase().endsWith('.rbaproj')){
                    returnBool = true;
                }
            }
            return returnBool;
        }
    }

    public String orderPriceBookId{
        get{
            if(projFileOrderId != null && orderPriceBookId == null){
                orderPriceBookId = [SELECT PriceBook2Id, PriceBook2.Name FROM Order WHERE Id = :projFileOrderId].PriceBook2Id;
            }
            return orderPriceBookId;
        }
        private set;
    }

    public boolean orderHasBeenCreated{
        get{
            if(orderHasBeenCreated == null){
            orderHasBeenCreated = projFileOrderId != null;
            }
            return orderHasBeenCreated;
        }
        private set;
    }

    public boolean pendingProductsExist {
        get{           
                pendingProductsExist = false;
                if(rSuiteOrderIdsFromFile != null){
                for(String s : rSuiteOrderIdsFromFile){
                    if(!existingRSuiteIds.contains(s)){
                        pendingProductsExist = true;
                    }
                }
            }            
            
            return pendingProductsExist;
        }
        private set;
    }


 
private Set<String> existingRSuiteIds {
    get{
        if(existingRSuiteIds == null){        
            existingRSuiteIds = new Set<String>();
            if(orderHasBeenCreated){
                for(OrderItem oi : [SELECT Id, rSuite_Id__c FROM OrderItem WHERE OrderId = :projFileOrderId]){
                    existingRSuiteIds.add(oi.rSuite_Id__c);
                }
            }
        }
        return existingRSuiteIds;
    }
    private set;
}

public set<OrderItem> projFileOrderItems {
    get{
         if(projFileOrderItems == null){
           
            projFileOrderItems = new set<OrderItem>();
            Attachment attach = projFile;
            for(Attachment a : [SELECT Name, Body FROM Attachment WHERE ParentId = :oppId ORDER BY CreatedDate DESC]){
                if(a.Name.toLowerCase().endsWith('.rbaproj')){
                    attach = a;
                    break;
                }
            }
            if(attach.body != null && attach.Name.toLowerCase().endsWith('.rbaproj')){
                try{
                    RSuiteDTO.SalesAppointment sa = new RSuiteDTO.SalesAppointment(attach.Body.toString());
                    RMS_mobileAppRequestHelper helper = new RMS_mobileAppRequestHelper();
                    sa = helper.resultSalesAppointments(UserInfo.getUserId(), sa);
                    String jsonString1 = System.JSON.serializePretty(sa);
                    if( sa != null){
                        if(sa.opptyContainer != null ){
                            if(sa.opptyContainer.anOrderContainer != null ){
                                if(sa.opptyContainer.anOrderContainer.oiContainerList != null){
                                    for( RSuiteDTO.OrderItemContainer oic : sa.opptyContainer.anOrderContainer.oiContainerList ){
                                        if(oic.Oi != null ){
                                            projFileOrderItems.add(oic.Oi);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (exception e){
                    system.debug('pasring issue: '+ e);
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'Unable to parse .rbaproj file.');
                        ApexPages.addMessage(myMsg);
                }
            }

        }
        return projFileOrderItems;
    }
    private set;
}

private Set<String> rSuiteOrderIdsFromFile {
    get{
        if(rSuiteOrderIdsFromFile == null && fileHasBeenUploaded){
            rSuiteOrderIdsFromFile = new Set<String>();
            for(OrderItem oi : projFileOrderItems ){
                rSuiteOrderIdsFromFile.add(oi.rSuite_Id__c);
            }

        }
        return rSuiteOrderIdsFromFile;
    }
    private set;
}

    public id projFileOrderId {
        get{
            if(projFileOrderId == null && fileHasBeenUploaded){
             set<Id> orderIds = new set<Id>();
                    if(!rSuiteOrderIdsFromFile.isEmpty()){
                        for(OrderItem oi : [SELECT Id, OrderId FROM OrderItem WHERE rSuite_Id__c IN :rSuiteOrderIdsFromFile]){
                            orderIds.add(oi.OrderId);
                        }
                    }
                    if(orderIds.size() == 1){
                        for(String s : orderIds ){
                            projFileOrderId = (Id)s;
                        }
                    }
                }
            
                return projFileOrderId;
            }
            private set;
        }


     public RMS_uploadOrderExtension(ApexPages.StandardController controller) {
            oppId = ApexPages.currentPage().getParameters().get('oppId');
            myOpp = (Opportunity)controller.getRecord();
        }

       

    public PageReference saveAttachment(){


        if(projFile.Name == null || !projFile.Name.toLowerCase().endsWith('.rbaproj')){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'You must select an attachment with file extension .rbaproj');
            ApexPages.addMessage(myMsg);
            return null;
        }

        projFile.OwnerId = UserInfo.getUserId();
        projFile.ParentId = oppId; // the record the file is attached to
        projFile.IsPrivate = false;

        try {
            insert projFile;
            } catch (DMLException e) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
                return null;
            } finally {
                projFile = new Attachment(); 
            }
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
        boolean stillhavePendingOrderItems = pendingProductsExist;
        return null;
    }


    public PageReference viewOrder(){
    //this method will take the results of the create order method and redirect the user to the Order
        PageReference returnReference;
        if (String.isNotBlank(projFileOrderId)){
            returnReference = new PageReference('/' + projFileOrderId );
        }
           
            return returnReference;
        }


        public PageReference makeModificationsToPendingOrderItems(){
            Set<String> myStringSet = existingRSuiteIds;
            pendingOrderItems = new List<OrderItem>();

            for(OrderItem oi : projFileOrderItems){
                if(!myStringSet.contains(oi.rSuite_Id__c)){
                    OrderItem myOi = oi.Clone(false, false, false, false);
                    myOi.PricebookEntryId = null;
                    pendingOrderItems.add(myOi);
                }
            }
            editModePendingOrderItems = true;
           return null;
    }

     public PageReference CancelModificationsToPendingOrderItems(){
        pendingOrderItems.clear();
        editModePendingOrderItems = false;       
        return null;
    }



   
    public PageReference savePendingOrderItems(){
        
        Order order = [SELECT PriceBook2Id, PriceBook2.Name FROM Order WHERE Id = :projFileOrderId];
        Id orderPriceBookId = order.PriceBook2Id;
        String pricebookName = order.PriceBook2.Name;
        Set<Id> product2Ids = new Set<Id>();
        for(OrderItem oi : pendingOrderItems){
            if(oi.Product2Id != null){
                product2Ids.add(oi.Product2Id);
            }
        }
        Map<Id,PriceBookEntry> product2IdToPriceBookEntry = new Map<Id,PriceBookEntry>();
        for(PriceBookEntry pbe :  [SELECT Id, Product2Id, PriceBook2.Name, Product2.Name FROM PriceBookEntry WHERE PriceBook2Id = :orderPriceBookId]){
                product2IdToPriceBookEntry.put(pbe.Product2Id, pbe);
        }
        List<Id> productIds = new List<Id>();
        for(OrderItem oi : pendingOrderItems){
            if(oi.Product2Id != null){
                productIds.add(oi.Product2Id);
            }
        }
        Map<Id,String> productIdToName = new Map<Id,String>();
        for(Product2 prod : [SELECT Id, Name FROM Product2 WHERE Id IN : productIds]){
            productIdToName.put(prod.id, prod.name);
        }
      
        List<OrderItem> orderItemsToInsert = new List<OrderItem>();
        for(OrderItem oi : pendingOrderItems){
            if(oi.Product2Id != null){
                if(product2IdToPriceBookEntry.get(oi.product2Id) == null){
                    ApexPages.Message myMsg;
                    myMsg  = new ApexPages.Message(ApexPages.Severity.INFO, 'No pricebook entry in '+ pricebookName + ' for ' +productIdToName.get(oi.product2Id) );
                    ApexPages.addMessage(myMsg);
                } else {
                oi.PriceBookEntryId = product2IdToPriceBookEntry.get(oi.product2Id).id;
                oi.OrderId = projFileOrderId;
                orderItemsToInsert.add(oi);  
                }          
            } else if (oi.PriceBookEntryId !=null){
                oi.OrderId = projFileOrderId;
                orderItemsToInsert.add(oi);  
            }
        }


        if(orderItemsToInsert.isEmpty()){
                ApexPages.Message myMsg;
                myMsg  = new ApexPages.Message(ApexPages.Severity.INFO, 'Please select a valid product for at least one pending order item before saving modifications.' );
                ApexPages.addMessage(myMsg);
                return null;

            }

            System.SavePoint sp = Database.setSavePoint();
            try{
                insert orderItemsToInsert;
                pendingOrderItems.clear();
                editModePendingOrderItems = false;
                ApexPages.Message myMsg;
                myMsg  = new ApexPages.Message(ApexPages.Severity.INFO, 'Order Items successfully added to the order' );
                ApexPages.addMessage(myMsg);


                } catch (Exception e){
                    Database.rollBack(sp);
                    ApexPages.Message myMsg;
                    myMsg  = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to add Order Items.' );
                    System.debug('DML Error Occured while attempting to import :'+ e);
                    ApexPages.addMessage(myMsg);
                }
        existingRSuiteIds = null;
        pendingProductsExist = null;
        boolean stillhavePendingOrderItems = pendingProductsExist;
       
        return null;
    }



public PageReference uploadFile(){
//this method calls a method created in another class to look up the attachments related to this opportunity and unpack the results

    if (String.isNotBlank(oppId)){
        ApexPages.Message myMsg;
        RSuiteFileHandler.FileUploadResultDTO fr = new RSuiteFileHandler.FileUploadResultDTO();
        fr = RSuiteFileHandler.uploadProjectFile( oppId );
        if (fr.messages != null){
            for (String message: fr.messages) {
                myMsg  = new ApexPages.Message(ApexPages.Severity.INFO, message );
                ApexPages.addMessage(myMsg);
            }
        }
        if (fr.errors != null){
            for (String error: fr.errors) {
                myMsg  = new ApexPages.Message(ApexPages.Severity.ERROR, error );
                ApexPages.addMessage(myMsg);
            }
        }
        if (fr.orderId != null){
            projFileOrderId = fr.orderId;
            myMsg  = new ApexPages.Message(ApexPages.Severity.INFO, 'Order Created Successfully: '+projFileOrderId );
            ApexPages.addMessage(myMsg);
            orderHasBeenCreated = true;
            } else {
                myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No Order created');
                ApexPages.addMessage(myMsg);
            } 
            } else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'No Opportunity Id');
                ApexPages.addMessage(myMsg);
            }

    		existingRSuiteIds = null;
        	pendingProductsExist = null;
            boolean stillhavePendingOrderItems = pendingProductsExist;
            String pbId = orderPriceBookId;
            return null;
        }


}