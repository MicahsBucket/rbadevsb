global class SandboxPostRefresh implements SandboxPostCopy {
    global void runApexClass(SandboxContext context) {
        System.debug(context.organizationId());
        System.debug(context.sandboxId());
        System.debug(context.sandboxName());
        
        if(context.sandboxName() == 'E2E' || context.sandboxName() == 'QA' || context.sandboxName() == 'UAT'){
            SandboxPostRefreshHelper.resetAdminEmailAddresses();
        } else {
            SandboxPostRefreshHelper.populateStoreAccountRecords();
            SandboxPostRefreshHelper.populateWorkTypes();
            SandboxPostRefreshHelper.populateServiceTerritories();
        }
    }
}