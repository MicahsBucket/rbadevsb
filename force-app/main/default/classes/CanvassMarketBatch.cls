global class CanvassMarketBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful 
{
    //database.executebatch(new CanvassMarketBatch("Market Id Goes Here"),1);
    
    private string marketId; 
    private set<string> zipCodeSet;
    private string zipCode;
    
    global CanvassMarketBatch(string oldMarketId)
    {
        marketId = oldMarketId;
        zipCodeSet = new set<string>();
        zipCode = null;
    }

    global CanvassMarketBatch(string oldMarketId, string oldZipcode)
    {
        zipCodeSet = new set<string>();
        zipCode = oldZipcode;
        
        list<Canvass_Market_Zip_Code__c> canvassMarketZipCode = [SELECT Id, Name, Zip_Code__c, Canvass_Market__c
                                                                 FROM Canvass_Market_Zip_Code__c
                                                                 WHERE Zip_Code__c = :zipCode];
        if(canvassMarketZipCode.size() > 0)
        {
            marketId = canvassMarketZipCode[0].Canvass_Market__c;
        }
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        string canvassUnitMarketBatchJobQuery = null;
        
        if(zipCode == null)
        {
            canvassUnitMarketBatchJobQuery = 'SELECT Id, Name, Canvass_Market__c, Zip_Code__c '+ 
                                             'FROM Canvass_Market_Zip_Code__c '+
                                             'WHERE Canvass_Market__c = :marketId';
        }
        else
        {
            canvassUnitMarketBatchJobQuery = 'SELECT Id, Name, Canvass_Market__c, Zip_Code__c '+
                                             'FROM Canvass_Market_Zip_Code__c '+
                                             'WHERE Zip_Code__c = :zipCode';
        }
        
        return Database.getQueryLocator (canvassUnitMarketBatchJobQuery);
    }
    
    global void execute (Database.BatchableContext BC, List<Canvass_Market_Zip_Code__c> scope)
    {
        for(Canvass_Market_Zip_Code__c zip :scope)
        {
            zipCodeSet.add(zip.Zip_Code__c);
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        Canvass_Market_Batch_Job__c canvassMarketBatchJob = new Canvass_Market_Batch_Job__c();
        canvassMarketBatchJob.Canvass_Market__c = marketId;
        canvassMarketBatchJob.Status__c = 'In Progress';
        canvassMarketBatchJob.User__c = UserInfo.getUserId();
        insert canvassMarketBatchJob;

        if (!Test.isRunningTest())
        {
            database.executebatch(new CanvassUnitBatch(zipCodeSet, marketId, canvassMarketBatchJob.Id),1);
        }
    }
}