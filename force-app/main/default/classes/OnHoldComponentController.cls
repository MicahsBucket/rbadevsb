/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Signature Service Community
* @date 6/17
* @description gets all survey hold information associated with a store.  Also creates new worker record
**/

public with sharing class OnHoldComponentController {

	//returns how many holds a store is allowed
	@AuraEnabled
	public static Integer getHolds(){
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Store').getRecordTypeId();
		Account storeAccount = [SELECT Id, Survey_Holds__c, RecordTypeId FROM Account WHERE Survey_Holds__c != NULL LIMIT 1];
		System.debug('storeAccount: ' + storeAccount.Id);
		Integer holds = storeAccount.Survey_Holds__c.intValue();
		return holds;
	}

	//returns the total number of surveys currently on hold for a particular store
	@AuraEnabled
	public static List<Integer> holdsUsed(){
        Id recordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get('Post Install').getRecordTypeId();
		List<Integer> usedList = new List<Integer>();
		List<Survey__c> surveysOnHold = [SELECT Id FROM Survey__c WHERE Survey_Status__c = 'Hold' AND RecordTypeId = :recordTypeId];
		List<Survey__c> surveysIncomplete = [SELECT ID FROM Survey__c WHERE Survey_Status__c = 'Incomplete Data' AND RecordTypeId = :recordTypeId];
		Integer holdsUsed = surveysOnHold.size();
		usedList.add(holdsUsed);
		Integer incompleteUsed = surveysIncomplete.size();
		usedList.add(incompleteUsed);
		return usedList;
	}
}