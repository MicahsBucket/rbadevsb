/**
 * @File Name          : SalesSchedDateSchedule.cls
 * @Description        : 
 * @Author             : James Loghry (Demand Chain)
 * @Group              : 
 * @Last Modified By   : James Loghry (Demand Chain)
 * @Last Modified On   : 5/7/2019, 11:59:16 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/3/2019, 2:48:04 PM   James Loghry (Demand Chain)     Initial Version
**/
public class SalesSchedDateSchedule{

    public static Date setNextDate(Time storeCloseTime){
        List<Store_Configuration__c> configs = [Select Current_Sales_Date__c From Store_Configuration__c Where Call_Center_Close_Time__c = :storeCloseTime];
        Date currentSalesDate = null;
        for(Store_Configuration__c config : configs){
            if(currentSalesDate == null || config.Current_Sales_Date__c > currentSalesDate){
                currentSalesDate = config.Current_Sales_Date__c.addDays(1);
            }
        }

        for(Store_Configuration__c config : configs){
            config.Current_Sales_Date__c = currentSalesDate;
        }
        update configs;

        return currentSalesDate;
    }

    public static void executeMetrics(Date day, Time storeCloseTime){
        Database.executeBatch(new SalesSchedCalculateSalesRepMetricsBatch(day,storeCloseTime),100);
    }
}