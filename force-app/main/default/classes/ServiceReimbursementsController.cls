/*
* @author Jason Flippen
* @date 10/21/2020 
* @description Class to provide functionality for the serviceReimbursements LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - ServiceReimbursementsControllerTest
*/
public with sharing class ServiceReimbursementsController {

    @AuraEnabled(cacheable=true)
    public static ReimbursementsWrapper getReimbursements(Id orderId) {

        ReimbursementsWrapper wrapper = new ReimbursementsWrapper();

        Order currentOrder = [SELECT Id,
                                     Reimbursement_Labor__c,
                                     Reimbursement_Mileage__c,
                                     Reimbursement_Parts__c
                              FROM   Order
                              WHERE  Id = :orderId];
        
        wrapper.orderId = currentOrder.Id;
        wrapper.reimbursementLaborAmount = 0;
        if (currentOrder.Reimbursement_Labor__c != null) {
            wrapper.reimbursementLaborAmount = currentOrder.Reimbursement_Labor__c;
        }
        wrapper.reimbursementMileageAmount = 0;
        if (currentOrder.Reimbursement_Mileage__c != null) {
            wrapper.reimbursementMileageAmount = currentOrder.Reimbursement_Mileage__c;
        }
        wrapper.reimbursementPartsAmount = 0;
        if (currentOrder.Reimbursement_Parts__c != null) {
            wrapper.reimbursementPartsAmount = currentOrder.Reimbursement_Parts__c;
        }
        wrapper.reimbursablePartList = new List<ReimbursablePartWrapper>();

        for (Reimbursable_Part__c reimbursablePart : [SELECT Id,
                                                               Cost__c,
                                                               Description__c,
                                                               Order_Product__r.Sold_Order_Product_Asset__r.Name,
                                                               Order_Product__r.Sold_Order_Product_Asset__r.Variant_Number__c,
                                                               Part__c,
                                                               Part__r.ProductCode,
                                                               Quantity__c,
                                                               Reason__c
                                                      FROM     Reimbursable_Part__c
                                                      WHERE    Order__c = :orderId
                                                      ORDER BY part__r.ProductCode]) {
            
            ReimbursablePartWrapper partWrapper = new ReimbursablePartWrapper();
            partWrapper.id = reimbursablePart.Id;
            partWrapper.cost = reimbursablePart.Cost__c;
            partWrapper.partId = reimbursablePart.Part__c;
            partWrapper.partCode = reimbursablePart.Part__r.ProductCode;
            partWrapper.partDescription = reimbursablePart.Description__c;
            String productName = reimbursablePart.Order_Product__r.Sold_Order_Product_Asset__r.Name + ' | ' + reimbursablePart.Order_Product__r.Sold_Order_Product_Asset__r.Variant_Number__c;
            partWrapper.productName = productName;
            partWrapper.quantity = reimbursablePart.Quantity__c;
            partWrapper.reason = reimbursablePart.Reason__c;
            wrapper.reimbursablePartList.add(partWrapper);
        }
        System.debug('***** wrapper: ' + wrapper);

        return wrapper;

    }


/** Wrapper Classes **/


    /*
    * @author Jason Flippen
    * @date 10/21/2020
    * @description Wrapper Class for Reimbursements
    */
    @TestVisible
    public class ReimbursementsWrapper {
        
        @AuraEnabled
        public String orderId {get;set;}

        @AuraEnabled
        public Decimal reimbursementLaborAmount {get;set;}

        @AuraEnabled
        public Decimal reimbursementMileageAmount {get;set;}

        @AuraEnabled
        public Decimal reimbursementPartsAmount {get;set;}

        @AuraEnabled
        public List<ReimbursablePartWrapper> reimbursablePartList {get;set;}

    }
    
    /*
    * @author Jason Flippen
    * @date 10/21/2020
    * @description Wrapper Class for Reimbursable Parts
    */
    @TestVisible
    public class ReimbursablePartWrapper {
        
        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public Decimal cost {get;set;}

        @AuraEnabled
        public String partId {get;set;}

        @AuraEnabled
        public String partCode {get;set;}

        @AuraEnabled
        public String partDescription {get;set;}

        @AuraEnabled
        public String productName {get;set;}

        @AuraEnabled
        public Decimal quantity {get;set;}

        @AuraEnabled
        public String reason {get;set;}

    }

}