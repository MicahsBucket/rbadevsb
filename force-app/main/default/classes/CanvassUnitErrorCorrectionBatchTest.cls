@isTest(SeeAllData=false)
global class CanvassUnitErrorCorrectionBatchTest {

    public static testMethod void test1()
    {        
        CNVSS_Canvass_Market__c canvassMarket = new CNVSS_Canvass_Market__c();
        canvassMarket.Name = 'Test';
        canvassMarket.CNVSS_Canvass_Market_Manager__c = userInfo.getUserId();
        canvassMarket.CNVSS_ISC_Phone_Number__c = '5555555555';
        insert canvassMarket;
        
        Custom_Data_Layer_Batch_Tracking__c errorMessages = new Custom_Data_Layer_Batch_Tracking__c();
        errorMessages.ZipCode__c = '28217';
        errorMessages.Market__c = canvassMarket.id;
        insert errorMessages;
        
        Custom_Data_Layer_Batch_Queue__c queue = new Custom_Data_Layer_Batch_Queue__c();
        queue.ZipCode__c = '28217';
        queue.Market_Id__c = canvassMarket.id;

        Test.startTest();
        database.executebatch(new CanvassUnitErrorCorrectionBatch(),1);
        Test.stopTest();
    }
}