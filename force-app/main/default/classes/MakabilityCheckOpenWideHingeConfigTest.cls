@isTest
public with sharing class MakabilityCheckOpenWideHingeConfigTest {
    @isTest
    public static void passingMakabilityTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSizeDetailConfig(pc);
        createTestWohHeightConfig(pc);
        Field_Control_List__c fcl = createFieldControlLists('Hardware Option');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        createProductFieldControlDependencies('test',pfc,pfc);        
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,50,true,1 ,'1' ,17);
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
       List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
        string assertMsg = results[0].errorMessages[0];
        //system.assertEquals(assertMsg, 'Size Detail Config  - passed');
    }
    
    @isTest
    public static void passingMakabilityTestAbove() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSizeDetailConfig(pc);
        createTestWohHeightConfig(pc);
        Field_Control_List__c fcl = createFieldControlLists('Hardware Option');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        createProductFieldControlDependencies('test',pfc,pfc);        
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,50,true,1 ,'1' ,25);
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
       List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
        string assertMsg = results[0].errorMessages[0];
        //system.assertEquals(assertMsg, 'Size Detail Config  - passed');
    }
    
    @isTest
    public static void passingMakabilityTestError() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSizeDetailConfig(pc);
        createTestWohHeightConfig(pc);
        Field_Control_List__c fcl = createFieldControlLists('Hardware Option');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        createProductFieldControlDependencies('test',pfc,pfc);        
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,50,true,1 ,'1' ,20);
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
       List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
        string assertMsg = results[0].errorMessages[0];
        //system.assertEquals(assertMsg, 'Size Detail Config  - passed');
    }
    private static List<Product2> createTestProduct(Integer num){
        List<Product2> pros = new List<Product2>();
        for(Integer i = 0; i < num;i++){
            Product2 p = new Product2();
            p.name = 'test product ' + i;
            pros.add(p);            
        }        
        insert pros;
        return pros;
    }
    
    private static List<Product_Configuration__c> createTestProConfig(Integer num){
        List<Product2> pros = createTestProduct(num);
        List<Product_Configuration__c> pcs = new List<Product_Configuration__c>();
        for(Integer i = 0 ; i < num; i++){
            Product_Configuration__c pc = new Product_Configuration__c();
            pc.name = 'test config ' + i;
            pc.Tempered_Glass_Required_at_UI__c = 100;  
            pc.Product__c = pros[i].id;  
            pcs.add(pc);
        }
        insert pcs;
        return pcs;
    }

    private static Field_Control_List__c createFieldControlLists(String fieldName){
        Field_Control_List__c fcl = new Field_Control_List__c();
        fcl.Name = fieldName;
        fcl.rForce__c = true;
        fcl.Sales__c = true;
        fcl.Tech__c = true;
        insert fcl;
        return fcl;
    }    

    private static Product_Field_Control__c createProductFieldControl(Product_Configuration__c pc, Field_Control_List__c fcl ){
        Product_Field_Control__c pfc = new Product_Field_Control__c();
        pfc.Name = fcl.Name;
        pfc.Field_Control_ID__c = fcl.id;
        pfc.Product_Configuration_ID__c = pc.id;
        pfc.Required__c = true;
        insert pfc;
        return pfc;
    }

    // private static map<String,Product_Field_Control__c> createPfcMap(Id prodId,Product_Field_Control__c pfc){
    //     map<String,Product_Field_Control__c> result = new map<String,Product_Field_Control__c>();
    //     String key = prodId + '-' + pfc.Name;
    //     result.put(key,pfc);
    //     return result;
    // }
    private static void createProductFieldControlDependencies(String controllingValue, Product_Field_Control__c controllingField, Product_Field_Control__c dependentField ){
        Product_Field_Control_Dependency__c pfcd = new Product_Field_Control_Dependency__c();
        pfcd.Action_Taken__c = 'Disable';
        pfcd.Controlling_Field__c = controllingField.id;
        pfcd.Controlling_Value__c = controllingValue;
        pfcd.Dependent_Field__c = dependentField.id;
        insert pfcd;
        
    }       
    
    private static Size_Detail_Configuration__c createTestSizeDetailConfig(Product_Configuration__c pc){
        Size_Detail_Configuration__c sdc = new Size_Detail_Configuration__c();  
            sdc.Product_Configuration__c = pc.id;
            sdc.Performance_Category__c = 'DP Upgrade';
            sdc.Extended_Max_Height_Inches__c = 80;
            sdc.Extended_Max_Height_Fraction__c = 'Even';
            sdc.Extended_Max_Width_Inches__c = 80;
            sdc.Extended_Max_Width_Fraction__c = 'Even';
            sdc.Lock_Max_Width_Inches__c = 15;
            sdc.Lock_Max_Height_Inches__c = 15;
            sdc.Lock_Min_Width_Inches__c = 25;
            sdc.Lock_Min_Height_Inches__c = 25;
            sdc.Lock_Max_Width_Fraction__c = 'Even';
            sdc.Lock_Min_Width_Fraction__c = 'Even';
            sdc.Lock_Max_Height_Fraction__c = 'Even';
            sdc.Lock_Min_Height_Fraction__c = 'Even';
            sdc.Lock_Min_Width_Locks__c = '1';
            sdc.Lock_Max_Width_Locks__c = '1';
            sdc.Lock_Max_Height_Locks__c = '1';
            sdc.Lock_Min_Height_Locks__c = '1';
            sdc.Positive_Force__c = 50;
            sdc.Frame_Type__c = 'Flat Sill';
            sdc.Hardware_Options__c = 'Wide Opening Hinge';
            sdc.Negative_Force__c = 50;
            sdc.Max_Height_Inches__c = 40;
            sdc.Max_Height_Fraction__c = 'Even';
            sdc.Max_Width_Inches__c = 40;
            sdc.Max_Width_Fraction__c = 'Even';
            sdc.Min_Height_Inches__c = 40;
            sdc.Min_Height_Fraction__c = 'Even';        
            sdc.Min_Width_Inches__c = 40;
            sdc.Min_Width_Fraction__c = 'Even';
            sdc.Sash_Operation__c = 'Right';
            sdc.Sash_Ratio__c = '1:1';
            sdc.Specialty_Shape__c = 'Circle';
            sdc.United_Inch_Maximum__c = 120;
            sdc.United_Inch_Minimum__c = 30;
            sdc.WOH_Allowed_Above_Inches__c=24;
            sdc.WOH_Required_Below_Inches__c=18;
            sdc.WOH_Required_Below_Fractions__c='1/2';
            sdc.WOH_Allowed_Above_Fractions__c='1/2';
            sdc.WOH_Allowed_Below_Inches__c=20;
            sdc.WOH_Allowed_Below_Fractions__c='1/2';
            insert sdc;
            return sdc;
    }

    private static Map<String,MakabilityRestResource.OrderItem>  createOrderItem(Id prodId, Decimal wInch, Decimal hInch, Decimal posForce,Boolean dpUpgrade,Decimal locks,String oiId , Integer width){
        Map<string,MakabilityRestResource.OrderItem> orderItems = new Map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
        MakabilityCalculator mc = new MakabilityCalculator();
    ProductConfiguration pc = new ProductConfiguration();
        pc.widthInches = width;
        pc.widthFractions = 'Even';
        pc.heightInches = hInch;
        pc.heightFractions = 'Even';        
        pc.frame = 'Flat Sill';
        pc.hardwareOption = 'Wide Opening Hinge';
        pc.sashOperation = 'Right';
        pc.sashRatio = '1:1';
        pc.externalForce = posForce;
        pc.internalForce = 50;
        pc.highPerformance = dpUpgrade;
        pc.leftLegInches = null;
        pc.leftLegFraction = null;
        pc.rightLegInches = null;
        pc.rightLegFraction = null;
        pc.locks = locks; 
        pc.specialtyShape = 'Circle';
        mc.checkSpecialShape = true;
        mc.hasLeftLeg = false;
        mc.hasRightLeg = false;
        mc.checkLegCalc = false;
        /////////////////////       
        pc.productId = prodId; 
        oi.orderItemId = oiId;
        oi.makabilityCalculator = mc;
        oi.productConfiguration = pc;
        orderItems.put(oiId,oi);       
        return orderItems;
    }

    private static Wide_Open_Hinge_Height_Configuration__c createTestWohHeightConfig(Product_Configuration__c pc){
        Wide_Open_Hinge_Height_Configuration__c wohc = new Wide_Open_Hinge_Height_Configuration__c();
        
            wohc.Ending_Width_Fractions__c = 'Even';
            wohc.Ending_Width_Inches__c = 40;
            wohc.Max_Height_Inches__c = 40;
            wohc.Max_Height_Fractions__c ='Even';
            wohc.Product_Configuration__c = pc.id;
            wohc.Sash_Ratio__c = '1:1';
            wohc.Starting_Width_Fractions__c = 'Even';
            wohc.Starting_Width_Inches__c = 1;
            
            insert wohc;
            return wohc;
    }
}