@isTest
public class WorkOrderRelatedListControllerTest {
    private static Account storeAccount;
    private static WorkOrder measureWO;
    private static final String INSTALL_WORK_TYPE_NAME = 'Install';
    private static final String MEASURE_WORK_TYPE_NAME = 'Measure';
    private static final String SERVICE_WORK_TYPE_NAME = 'Service';
    private static final String JOB_SITE_VISIT_WORK_TYPE_NAME = 'Job Site Visit';
    private static Order ord;
    
    @TestSetup
    static void setup(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        // FSLRollupRolldownController.disableFSLRollupRolldownController = true;
        
        storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
        Database.insert(storeAccount, true);
        Id storeAccountId = storeAccount.Id;
        Id storeConfigId = storeAccount.Active_Store_Configuration__c;
        Id accountId = TestDataFactoryStatic.createAccount('Calvins', storeAccountId).Id;
        Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'CalvinPrime');
        Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('Calvins Opportunity', accountId, storeAccountId, 'Sold', date.today());
        OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
        ord = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        
        OperatingHours oh1 = new OperatingHours();
        oh1.Name = 'Test Operating Hours One';
        oh1.TimeZone = 'America/Chicago';
        insert oh1;
        
        ServiceTerritory st1 = new ServiceTerritory();
        st1.Name = 'Test Service Territory One';
        st1.OperatingHoursId = oh1.Id;
        st1.IsActive = true;
        insert st1;
        
        WorkType installWorkType = new WorkType(
            Name = INSTALL_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType measureWorkType = new WorkType(
            Name = MEASURE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType serviceWorkType = new WorkType(
            Name = SERVICE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType jobSiteVisitWorkType = new WorkType(
            Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 1,
            DurationType = 'Hours'
        );
        
        insert new List<WorkType>{installWorkType, measureWorkType, serviceWorkType, jobSiteVisitWorkType};
        
        measureWO = new WorkOrder(
                Recommended_Crew_Size__c = 2,
                Street = '219 North Milwaukee Street',
                City = 'Milwaukee',
                State = 'Wisconsin',
                PostalCode = '53208',
                CountryCode = 'US',
                WorkTypeId = measureWorkType.Id,
                ServiceTerritoryId = st1.Id
            );
        insert measureWO;
        
        WorkOrderLineItem woli = new WorkOrderLineItem(
            WorkOrderId = measureWO.Id
        );
        insert woli;
        
        // FSLRollupRolldownController.disableFSLRollupRolldownController = false;
    }
    
    @isTest
    static void testgetWorkOrderLineItems(){
        WorkOrder wo = [SELECT id FROM WorkOrder LIMIT  1];
        WorkOrderLineItem woliRec = [SELECT Id,
            List_Index__c,
            LineItemNumber,
            Subject,
            Description,
            Job_Notes__c,
            Status,
            WorkOrderId,
            Visibility__c,
            Line_Item_Type__c
            FROM WorkOrderLineItem
            LIMIT 1];
        WorkOrderRelatedListController.getWorkOrderLineItems(wo.id);
        WorkOrderRelatedListController.WorkOrderLineItemWrapper woliWrap = new WorkOrderRelatedListController.WorkOrderLineItemWrapper(woliRec);
    }
    
    static  testmethod void testgetAttachments() {
        WorkOrder wo = [SELECT id FROM WorkOrder LIMIT  1];
        
        ContentVersion testdata = new ContentVersion(
                Title = 'Test.rbaproj',
                PathOnClient = 'Test.rbaproj',
                IsMajorVersion = true,
                VersionData =Blob.valueOf('Test Content')
            );
            insert testdata;
            ContentVersion myTestData = [select Id, Title, PathOnClient, VersionData, IsMajorVersion, ContentDocumentId from ContentVersion where id =:testdata.Id];
            
            ContentDocumentLink testDataLink = new ContentDocumentLink(
                ContentDocumentId = myTestData.ContentDocumentId,
                LinkedEntityId = wo.Id,
                ShareType = 'V',
                Visibility = 'InternalUsers'
                
            );
            insert testDataLink;  
        
        
        test.starttest();
        WorkOrderRelatedListController.getAttachments(wo.id);
        WorkOrderRelatedListController.ContentDocWrapper ContentDocWrap = new WorkOrderRelatedListController.ContentDocWrapper(testDataLink);
        Test.stopTest();
    }

}