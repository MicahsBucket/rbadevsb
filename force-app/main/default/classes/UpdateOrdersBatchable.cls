global class UpdateOrdersBatchable implements Database.Batchable<sObject> {
	
	String query;
	
	global UpdateOrdersBatchable() {
		
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query= 'Select Id from Order where Opportunity.Owner.Commission_User__c = true ' +
		'and Opportunity.Owner.isActive = true and (CALENDAR_YEAR(Order_Processed_Date__c) >= 2016 ' +
		'or CALENDAR_YEAR(CreatedDate) >= 2016) and Cancel_Order_Subtotal__c = null';
		//'or CALENDAR_YEAR(CreatedDate) >= 2016) and Cancel_Order_Subtotal__c = null';

		//'and (CALENDAR_YEAR(Order_Processed_Date__c) >= 2016)';

		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {		
		update scope;

		// Also check change histories
		//Set<Id> oIdSet = new Set<Id>();
		//for(sObject o : scope){
		//	oIdSet.add(o.Id);
		//}

		//List<Change_History__c> chList = [select Id, Changes__c, Order__c, Order_Product__c, 
		//	Retail_Price__c, CreatedDate, Retail_Price_Change__c, Line_Processed_Date__c
		//	from Change_History__c 
		//	where Order__c in :oIdSet
		//	and Subtype__c = 'Change'];

		//CommissionsManagement.checkChangeHistoryPriceChange(chList);
	}

	global void finish(Database.BatchableContext BC) {
		
	}
	
}