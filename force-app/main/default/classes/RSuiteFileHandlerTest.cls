/**
 *@class	RSuiteFileHandlerTest
 *@brief	test class for RSuiteFileHandler
 *@author	Mark Wochnick (Slalom.MAW)
 *@version	2017/04/14  Slalom.MAW
 *@copyright  (c)2017 Slalom/Renewal by Andersen.  All Rights Reserved.  Unauthorized use is prohibited.
 */
@isTest
private class RSuiteFileHandlerTest {

    static testMethod void uploadProjectFileTest() {
		StaticResource rbaprojfile = [SELECT Id, Body FROM StaticResource WHERE Name = 'RSuiteFileHandlerTestData' LIMIT 1];
		StaticResource savedatafile = [SELECT Id, Body FROM StaticResource WHERE Name = 'RSuiteFileHandlerTestData2' LIMIT 1];
//System.debug('&&&&&&&&&&' + rbaprojfile.Body.toString());
		// Turn off the financial trigger to avoid SOQL limits in test class
		RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name='Turn Financial Transactions Off', Value__c = 'Yes');
		insert turnOffFinancialTrigger;
		
		TestUtilityMethods tUtil = new TestUtilityMethods();
		tUtil.setUpConfigs();
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
		// setup configuration stuff
		Datetime tmApptTime = Datetime.now().addDays(5);

		User u = tUtil.createUser(p.id);
		insert u;
		tUtil.createSalesAppt(u.Id, tmApptTime, false, false);
		// build out the container
		Account store1 = [SELECT id, Active_Store_Configuration__c FROM Account Where Name = '77 - Twin Cities, MN'][0];
		Id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        Account dwelling = [Select Id from Account where Name != 'RbA' AND Name != 'Unassigned Account' AND RecordTypeId =: dwellingRT];
		Opportunity opp = [select Id, StageName, rSuite_Id__c from Opportunity where AccountId =: dwelling.id];

		// create pricebook
		Pricebook2 pb = tutil.createPricebook2('RSTestDT', store1.Active_Store_Configuration__c);
		insert pb;
		// update the opportunity
		opp.StageName = 'New';
		opp.Pricebook2Id = pb.Id;
		update opp;
		// create a variety of products to cover the variations
		List<Product2> prods = 	tUtil.createRbAProducts();
		tUtil.addProductsToPricebook(prods, Test.getStandardPricebookId());
		List<PricebookEntry> pbeList = tUtil.addProductsToPricebook(prods, pb.Id);
		// create the attachments
		List<Attachment> rSuiteProjFiles = new List<Attachment>();
		Attachment att = new Attachment(Name = 'savedata', ParentId = opp.Id, Body = savedatafile.Body);
		rSuiteProjFiles.add(att);
		att = new Attachment(Name = '9BFDF5B9-02C0-43DA-A33B-D553833D199D.rbaproj', ParentId = opp.Id, Body = rbaprojfile.Body);
		rSuiteProjFiles.add(att);
		insert rSuiteProjFiles;

/*        System.runAs(u) {
        	Test.startTest();
	        RSuiteFileHandler.FileUploadResultDTO result = RSuiteFileHandler.uploadProjectFile(opp.Id);
	        Test.stopTest();
	        Order o = [select Id, Name from Order where OpportunityId = :opp.Id limit 1];
	        List<Quote> quoteList = [select Id, Name from Quote where OpportunityId = :opp.Id];
	        Opportunity oppty = [select Id, StageName from Opportunity where Id = :result.opp.Id limit 1];
	        List<OrderItem> oiList = [select Id, PricebookEntry.Product2.Name from OrderItem where OrderId = :o.Id];
	        System.debug('####: result: ' + result);
	        System.assert(result != null);
	        System.assert(result.orderId != null);
	        System.assertEquals(oppty.StageName, 'Sold');
	        System.assertEquals(o.Id, result.orderId);
	        System.assertEquals(quoteList.size(),3);
	        System.assertEquals(3,result.quotes.size());
	        System.assertEquals(0,result.errors.size());
	        System.assertEquals(1,result.messages.size());
	        System.assertEquals(19, oiList.size());
        }
*/    }

    static testMethod void uploadProjectFileFailureTest() {
		StaticResource rbaprojfile = [SELECT Id, Body FROM StaticResource WHERE Name = 'RSuiteFileHandlerTestData' LIMIT 1];
		StaticResource savedatafile = [SELECT Id, Body FROM StaticResource WHERE Name = 'RSuiteFileHandlerTestData2' LIMIT 1];
//System.debug('&&&&&&&&&&' + rbaprojfile.Body.toString());
		// Turn off the financial trigger to avoid SOQL limits in test class
		RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name='Turn Financial Transactions Off', Value__c = 'Yes');
		insert turnOffFinancialTrigger;
		
		TestUtilityMethods tUtil = new TestUtilityMethods();
		tUtil.setUpConfigs();
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
		// setup configuration stuff
		Datetime tmApptTime = Datetime.now().addDays(5);

		User u = tUtil.createUser(p.id);
		insert u;
		tUtil.createSalesAppt(u.Id, tmApptTime, false, false);
		// build out the container
		Account store1 = [SELECT id, Active_Store_Configuration__c FROM Account Where Name = '77 - Twin Cities, MN'][0];
		
		Discount__c discount1 = new Discount__c(Name = 'my discount');
	  insert discount1;

		Store_Discount__c storeDiscount = new Store_Discount__c();
	  storeDiscount.Name = RSuiteDTO.genericDiscountName;
	  storeDiscount.Discount__c = discount1.Id;
	  storeDiscount.Store_Configuration__c = store1.Active_Store_Configuration__c;
	  storeDiscount.Active__c = true;
	  insert storeDiscount;

		Id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        Account dwelling = [Select Id from Account where Name != 'RbA' AND Name != 'Unassigned Account' AND RecordTypeId =: dwellingRT];
		Opportunity opp = [select Id, StageName, rSuite_Id__c from Opportunity where AccountId =: dwelling.id];

		// create pricebook
		Pricebook2 pb = tutil.createPricebook2('RStDT2', store1.Active_Store_Configuration__c);
		insert pb;
		// update the opportunity
		opp.StageName = 'New';
		opp.Pricebook2Id = null;
		update opp;
		// create a variety of products to cover the variations
		List<Product2> prods = 	tUtil.createRbAProducts();
		tUtil.addProductsToPricebook(prods, Test.getStandardPricebookId());
		List<PricebookEntry> pbeList = tUtil.addProductsToPricebook(prods, pb.Id);
		// create the attachments
		List<Attachment> rSuiteProjFiles = new List<Attachment>();
		Attachment att = new Attachment(Name = 'savedata', ParentId = opp.Id, Body = savedatafile.Body);
		rSuiteProjFiles.add(att);
		att = new Attachment(Name = '9BFDF5B9-02C0-43DA-A33B-D553833D199D.rbaproj', ParentId = opp.Id, Body = rbaprojfile.Body);
		rSuiteProjFiles.add(att);
		insert rSuiteProjFiles;

        System.runAs(u) {
        	Test.startTest();
	        RSuiteFileHandler.FileUploadResultDTO result = RSuiteFileHandler.uploadProjectFile(opp.Id);
	        Test.stopTest();
	        List<Order> ords = [select Id, Name from Order where OpportunityId = :opp.Id];
	        List<Quote> quoteList = [select Id, Name from Quote where OpportunityId = :opp.Id];
	        Opportunity oppty = [select Id, StageName from Opportunity where Id = :opp.Id limit 1];
	        System.debug('####: result: ' + result);
/*	        System.assert(result != null);
	        System.assert(!result.saveSuccessful);
	        System.assert(result.orderId == null);
	        System.assertEquals(0,ords.size());
	        System.assertEquals(oppty.StageName, 'New');
	        System.assertEquals(quoteList.size(),0);
	        System.assertEquals(28,result.messages.size());
	        System.assertEquals(1,result.errors.size());
*/        }
    }
}