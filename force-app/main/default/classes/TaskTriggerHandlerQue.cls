public class TaskTriggerHandlerQue implements Queueable{
    public map<Id, Task> newMap;
    public map<Id, Task> oldMap;

    public TaskTriggerHandlerQue(map<Id, Task> newMap, map<Id, Task> oldMap) {
        this.newMap = newMap;
        this.oldMap = oldMap;
        
    }

    public void execute(QueueableContext context) {
        TaskTriggerHandler.updateOrderStatus(oldMap,newMap);
    }
}