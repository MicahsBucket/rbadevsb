/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Customer Portal
* @date 1/18
* @description Finds the Sales Rep, Primary Installer and Primary Tech Measure 
*              associated the Users Order
**/
public class CustomerOrderWorkersController  {

    //finds the current order based on the Order Id passed from the Collapsable Content Parent Component
    @AuraEnabled
    public static Order getOrder(Id orderId){
        Order currentOrder = [SELECT Sales_Rep__c, Primary_Installer__c, Primary_Tech_Measure__c, Id FROM Order WHERE Id= :orderId LIMIT 1]; 
        return currentOrder;
    }

    //Finds the name of either the installer or the tech measure from an order using the resource object
    @AuraEnabled
    public static String getWorker(Boolean isInstaller, Order ord){
        Id workerId;
        if(isInstaller){
            workerId = ord.Primary_Installer__c;
        } else {
            workerId = ord.Primary_Tech_Measure__c;
        }
        List<Resource__c> workers = [SELECT Name FROM Resource__c WHERE Id = :workerId LIMIT 1];
        String workerName ='';
		if (workers.size()>0)
         workerName = workers[0].Name;					 						  
        return workerName;
    }

    //returns the phone number of the Sales Rep associated to the current Order
    @AuraEnabled
    public static User getNumber(String orderId){
         try{
        String salesRepName = [SELECT Sales_Rep__c FROM Order WHERE Id = :orderId LIMIT 1].Sales_Rep__c;
        List<String> fullNameList = salesRepName.split(' ');
        String firstName = fullNameList[0];
        String lastName = fullNameList[1];
		User salesRep = [SELECT Email, MobilePhone FROM User WHERE Name =:salesRepName  LIMIT 1];
        return salesRep!= null?salesRep:new User();
        }
            catch(exception e)
            {
                return new User();
                }

    }
    @AuraEnabled
    public static String getUsers(Id orderId){
       String profileId = UserInfo.getProfileId();
        String currentUserId = UserInfo.getUserId();
        Profile p = [Select Id,Name from Profile where Id=:profileId];
        String salesRep='';
        String techMeasure='';
        String installer='';
        if (p.Name=='Super Administrator'){
            List<Order> orders = [Select Id,Sales_Rep__c from Order where Id = :orderId];
            if (Orders.size()>0){
                salesRep = orders[0].Sales_Rep__c;
                List<WorkOrder> wks = [Select Id, RecordType.Name,Primary_Installer_FSL__c,Primary_Installer_FSL__r.Name, Primary_Tech_Measure_FSL__c,Primary_Tech_Measure_FSL__r.Name 
                                       from WorkOrder where (status ='Scheduled & Assigned' OR  status='Appt Complete / Closed') and Sold_Order__c = : orders[0].Id];
                for (WorkOrder workord: wks){
                    if (workord.RecordType.Name=='Tech Measure'){
                        if (workord.Primary_Tech_Measure_FSL__c!=null)
                        techMeasure=workord.Primary_Tech_Measure_FSL__r.Name;
                    }
                    if (workord.RecordType.Name=='Install'){
                        if (workord.Primary_Installer_FSL__c!=null)
                        installer = workord.Primary_Installer_FSL__r.Name;
                    }
                }
            }
        }
        return salesRep+'@@'+techMeasure+'@@'+installer;
    }

}