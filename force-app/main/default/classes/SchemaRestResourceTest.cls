/**
* @author bill.hodena@andersencorp.com
* @description Unit test for SchemaRestResource
*/ 
@istest
public class SchemaRestResourceTest  {
    
    /**
    * @description Test getPickList method
    */ 
    @isTest
    static void testGetPickllists(){

        /* 
            Test empty parameters, this should return a list of all picklists related
            to the objects that are listed in the related metadata class. As this data is variable,
            and object configurations can change, a mock response is not created and thus, there 
            is nothing to assert here.
        */
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/api/v1/schema/'; 
        req.httpMethod = 'GET'; 
        RestContext.request = req;
        RestContext.response = res;
        SchemaRestResource.getPicklists();
        Test.stopTest();    
    }
}