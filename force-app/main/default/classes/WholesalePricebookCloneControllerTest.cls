/**
* @author: Jason Flippen
* @description: Test class for Wholesale Pricebook cloning
* @class coverage: WholesalePricebookCloneController
*/
@isTest
public class WholesalePricebookCloneControllerTest {

    /**
    * @author: Jason Flippen
    * @description: Method to test the functionality in the Controller.
    * @params: N/A
    * @returns: N/A
    */ 
    private static testMethod void testController() {

        // Create Test Data.
        Wholesale_Pricebook__c testWholesalePricebook = new Wholesale_Pricebook__c(Name = 'Test Wholesale Pricebook',
                                                                                   Status__c = 'In Process',
                                                                                   Effective_Date__c = Date.today());
        insert testWholesalePricebook;

        Id pcRecordTypeId = Schema.SObjectType.Pricing_Configuration__c.getRecordTypeInfosByDeveloperName().get('Window_Pricing_Configuration').getRecordTypeId();
        Pricing_Configuration__c testPricingConfiguration = new Pricing_Configuration__c(Name = 'Test Pricing Configuration',
                                                                                         RecordTypeId = pcRecordTypeId,
                                                                                         Wholesale_Pricebook__c = testWholesalePricebook.Id);
        insert testPricingConfiguration;

        Test.startTest();

            // Retrieve the List of Pricing Configurations related to the Test Wholesale Pricebook.
            List<Pricing_Configuration__c> pricingConfigurationList = WholesalePricebookCloneController.getRelatedPricingConfigurations(testWholesalePricebook.Id);
            System.assertEquals(1,pricingConfigurationList.size());

            Wholesale_Pricebook__c newWholesalePricebook = new Wholesale_Pricebook__c(Name = 'Test Wholesale Pricebook #2',
                                                                                      Cloned_Wholesale_Pricebook__c = testWholesalePricebook.Id,
                                                                                      Status__c = 'In Process',
                                                                                      Effective_Date__c = Date.today());

            // Clone the Wholesale Pricebook and its Pricing Configurations.
            String newWholesalePricebookId = WholesalePricebookCloneController.saveWholesalePricebook(newWholesalePricebook,
                                                                                                      true,
                                                                                                      pricingConfigurationList);
            System.assertEquals(false, String.isBlank(newWholesalePricebookId));

            // Retrieve the List of cloned Pricing Configurations related to the new Wholesale Pricebook.
            List<Pricing_Configuration__c> clonedPricingConfigurationList = [SELECT Id
                                                                             FROM   Pricing_Configuration__c
                                                                             WHERE  Wholesale_Pricebook__c = :newWholesalePricebookId];
            System.assertEquals(1,clonedPricingConfigurationList.size());

        Test.stopTest();

    }

}