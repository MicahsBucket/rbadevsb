@isTest
public with sharing class OpportunityUpdateQueTEST {
    @isTest
    public static void OpportunityUpdateTEST() {
        UtilityMethods.disableAllFslTriggers();
        TestUtilityMethods utility = new TestUtilityMethods();

        utility.setUpConfigs();
        Account dwelling = TestDataFactoryStatic.createDwellingAccount('OpptyBeforeInsertDwelling');
		Account store = TestDataFactoryStatic.createStoreAccount('OpptyBeforeInsertStore');
		upsert dwelling;
		upsert store;

		// Create related contacts
		Contact resident = TestDataFactory.createCustomerContact(dwelling.id);
		upsert resident;

		// Create opportunity
        List<Opportunity> oppList = new List<Opportunity>();
		Opportunity opt = new Opportunity();
		opt.Name = 'OpptyAfterInsertOppty123';
		opt.AccountId = dwelling.Id;
		opt.StageName = 'New';
		opt.Store_Location__c = store.Id;
		opt.CloseDate = Date.newInstance(2018, 12, 31);
        insert opt;
		oppList.add(opt);
		// Insert opportunity, this will create the Opportunity
		Test.startTest();		
		    System.enqueueJob(new OpportunityUpdateQue(oppList));
		Test.stopTest();
    }
}