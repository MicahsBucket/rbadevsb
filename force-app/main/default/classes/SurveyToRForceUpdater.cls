/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Signature Service Community
* @date 8/03
* @description Updates contact records associated to surveys when surveys are updated
**/

public class SurveyToRForceUpdater  {
    
    //Method : created by sundeep CXMT-328
    public static void associatingParentActStoreCon(List<Survey__c> newList){
        set<id> actIds = new set<id>(); 
        Map<id,id> actParentMap = new Map<id,id>();
        for(Survey__c sr:newList){
            if(sr.Store_Account__c != null){
                actIds.add(sr.Store_Account__c );
            }
            
        }
        if(!actIds.isEmpty()){
            for(Account ac:[SELECT Id,Active_Store_Configuration__r.Store__c FROM Account WHERE Id In:actIds] ) {
                actParentMap.put(ac.Id,ac.Active_Store_Configuration__r.Store__c);
            }
            
        }
        for(Survey__c sr:newList){
            if(actParentMap.containskey(sr.Store_Account__c) &&  actParentMap.get(sr.Store_Account__c)!= null){
                sr.Store_Account__c = actParentMap.get(sr.Store_Account__c);
                
            }
            
        }
        
    }
	
	public static void SurveyToRForceContactUpdater (List<Survey__c> newList, Map<Id, Survey__c> updatedSurveysMap, Map<Id, Survey__c> oldMap) {
		
		Boolean updatePrimContact = false;
		Boolean updateSeconContact = false;
		Set<String> primContFieldsToUpdate = new Set<String>();
		Set<String> seconContactFieldsToUpdate = new Set<String>();
		List<Contact> contactsToUpdate = new List<Contact>();
		
		Set<String> primContactFields = new Set<String>{
			'Primary_Contact_First_Name__c', 'Primary_Contact_Last_Name__c', 'Primary_Contact_Email__c',
		 	'Primary_Contact_Mobile_Phone__c','Primary_Contact_Home_Phone__c',
		 	'Primary_Contact_Work_Phone__c','Street__c',
		 	'City__c','State__c',
		 	'Zip__c','Country__c'
		};
		Set<String> seconContactFields = new Set<String>{
			'Secondary_Contact_First_Name__c',
		 	'Secondary_Contact_Last_Name__c',
		 	'Secondary_Contact_email__c',
		 	'Secondary_Contact_Mobile_Phone__c',
		 	'Secondary_Contact_Home_Phone__c',
		 	'Secondary_Contact_Work_Phone__c'
		};
		
		//seperate primary contacts from secondary contacts
		Map<Id, Contact> primContactMap = createSurveytoContactMap('Primary', newList);
		Map<Id, Contact> secondaryContactMap = createSurveytoContactMap('Secondary', newList);
        
		//determines which fields have been updated for primary contact
		for(Survey__c s : newList){
			for(String f : primContactFields){
				if(s.get(f) != oldMap.get(s.Id).get(f)){
					primContFieldsToUpdate.add(f);
					updatePrimContact = true;
				}
			}
		}

		//Checks which fields should be updated for secondary contact then updates them
		if(updatePrimContact){
			for(Survey__c s : newList){
				if(primContactMap.get(s.Id) != null){
					if(primContFieldsToUpdate.contains('Primary_Contact_First_Name__c')) primContactMap.get(s.Id).FirstName = s.Primary_Contact_First_Name__c;
					if(primContFieldsToUpdate.contains('Primary_Contact_Last_Name__c')) primContactMap.get(s.Id).LastName = s.Primary_Contact_Last_Name__c;
					if(primContFieldsToUpdate.contains('Primary_Contact_Email__c')) primContactMap.get(s.Id).Email = s.Primary_Contact_Email__c;
					if(primContFieldsToUpdate.contains('Primary_Contact_Mobile_Phone__c')) primContactMap.get(s.Id).MobilePhone = s.Primary_Contact_Mobile_Phone__c;
					if(primContFieldsToUpdate.contains('Primary_Contact_Home_Phone__c')) primContactMap.get(s.Id).HomePhone = s.Primary_Contact_Home_Phone__c;
					if(primContFieldsToUpdate.contains('Primary_Contact_Work_Phone__c')) primContactMap.get(s.Id).Phone = s.Primary_Contact_Work_Phone__c;
					if(primContFieldsToUpdate.contains('Street__c')) primContactMap.get(s.Id).MailingStreet = s.Street__c;
					if(primContFieldsToUpdate.contains('City__c')) primContactMap.get(s.Id).MailingCity = s.City__c; 
					if(primContFieldsToUpdate.contains('State__c') && s.State__c != 'NH') primContactMap.get(s.Id).MailingCountryCode = s.State__c;
					if(primContFieldsToUpdate.contains('Zip__c')) primContactMap.get(s.Id).MailingPostalCode = s.Zip__c;
					if(primContFieldsToUpdate.contains('Country__c')) primContactMap.get(s.Id).MailingCountry = s.Country__c;
				}
			}
		}
		contactsToUpdate.addAll(primContactMap.values());

		//determines which fields have been updated for secondary contact
		for(Survey__c s : newList){
			for(String f : seconContactFields){
				if(s.get(f) != oldMap.get(s.Id).get(f)){
					seconContactFieldsToUpdate.add(f);
					updateSeconContact = true;
				}
			}
		}

		//Checks which fields should be updated for secondary contact then updates them
		if(updateSeconContact){
			for(Survey__c s : newList){
				if(secondaryContactMap.get(s.Id) != null){
					if(seconContactFieldsToUpdate.contains('Secondary_Contact_First_Name__c')) secondaryContactMap.get(s.Id).FirstName = s.Secondary_Contact_First_Name__c;
					if(seconContactFieldsToUpdate.contains('Secondary_Contact_Last_Name__c')) secondaryContactMap.get(s.Id).LastName = s.Secondary_Contact_Last_Name__c;
					if(seconContactFieldsToUpdate.contains('Secondary_Contact_email__c')) secondaryContactMap.get(s.Id).Email = s.Secondary_Contact_email__c;
					if(seconContactFieldsToUpdate.contains('Secondary_Contact_Mobile_Phone__c')) secondaryContactMap.get(s.Id).MobilePhone = s.Secondary_Contact_Mobile_Phone__c;
					if(seconContactFieldsToUpdate.contains('Secondary_Contact_Home_Phone__c')) secondaryContactMap.get(s.Id).HomePhone = s.Secondary_Contact_Home_Phone__c;
					if(seconContactFieldsToUpdate.contains('Secondary_Contact_Work_Phone__c')) secondaryContactMap.get(s.Id).Phone = s.Secondary_Contact_Work_Phone__c;
				}
			}
		}
		contactsToUpdate.addAll(secondaryContactMap.values());
		Database.update(contactsToUpdate, true);
	}

	/*******************************************************************************************************
	* @description creates map of survey id to contact 
	* @param roleType distinguishes between primary or secondary contact
	* @param roleType distinguishes between primary or secondary contact
	* @return Map<Id, Contact>
	* @example
	* createSurveytoContactMap('Primary', newList);
	*/
	public static Map<Id, Contact> createSurveytoContactMap(String roleType, List<Survey__c> newList){
		Map<Id, Contact> surveyToContactMap = new Map<Id, Contact>();
		Map<Id, Id> surveyToContactIdMap = new Map<Id, Id>();
		List<Id> surveyIds = new List<Id>();

       	for(Survey__c s : newList){
			surveyIds.add(s.Id);
		}
		List<Contact_Survey__c> conSurvJuncList = [SELECT Id, Contact_Id__c, Survey_Id__c, Type__c 
                                    FROM Contact_Survey__c 
                                    WHERE Survey_Id__c IN :surveyIds AND Type__c = :roleType];
        for(Contact_Survey__c cs : conSurvJuncList){
        	surveyToContactIdMap.put(cs.Contact_Id__c, cs.Survey_Id__c);
        }
        List<Contact> contactList = [SELECT Id, FirstName, LastName, Email, MobilePhone, HomePhone, Phone, 
        									MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry 
        									FROM Contact WHERE Id IN :surveyToContactIdMap.keySet()];
        for(Contact c : contactList){
        	Id surveyId = surveyToContactIdMap.get(c.Id);
        	surveyToContactMap.put(surveyId, c);
        }
        return surveyToContactMap;
    }
}