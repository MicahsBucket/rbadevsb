/**
 * @File Name          : MakabilityCheckGlazingConfigTest.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 5/1/2019, 1:34:27 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/1/2019, 1:34:27 PM   mark.rothermal@andersencorp.com     Initial Version
**/
@isTest
public with sharing class MakabilityCheckGlazingConfigTest {
   @isTest
    public static void passingMakabilityTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestGlazingConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,'Even','Even',True,True,'Fern','Fern' ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGlazingConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Glazing Config  - passed');
    }
    @isTest
    public static void noMatchingProductIdTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestGlazingConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,'Even','Even',True,True,'Fern','Fern' ,'1');
        set<id> productIds = new set<id>();
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGlazingConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingProductIdTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Salesforce - No Glazing Configurations found for this product Id.');
    }
    @isTest
    public static void doesNotMatchTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestGlazingConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,60,60,'Even','Even',False,False,'Fern','Fern' ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGlazingConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results doesNotMatchTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        string assertMsg2 = results[0].errorMessages[1];
        string assertMsg3 = results[0].errorMessages[2];
        string assertMsg4 = results[0].errorMessages[3];
        string assertMsg5 = results[0].errorMessages[4];
        system.assertEquals(assertMsg, 'Glazing Config - Requires Tempered Glass due to size. Tempered glass required over 99 united inches.'); 
        system.assertEquals(assertMsg2, 'Glazing Config - S3 Requires Tempered Glass due to size. Tempered glass required over 99 united inches.');                
        system.assertEquals(assertMsg3, 'Glazing Config - s1Pattern Height or Width Greater than Allowed');        
        system.assertEquals(assertMsg4, 'Glazing Config - s2Pattern Height or Width Greater than Allowed');        
        system.assertEquals(assertMsg5, 'Glazing Config - s3Pattern Height or Width Greater than Allowed');        
    }  

    @isTest
    public static void doesNotMatchPatternTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestGlazingConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,'Even','Even',False,False,'Reed','Reed' ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGlazingConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results doesNotMatchTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        string assertMsg2 = results[0].errorMessages[1];
        string assertMsg3 = results[0].errorMessages[2];
        string assertMsg4 = results[0].errorMessages[3];
        string assertMsg5 = results[0].errorMessages[4];                
        system.assertEquals(assertMsg, 'Glazing Config - Requires Tempered Glass due to size. Tempered glass required over 99 united inches.'); 
        system.assertEquals(assertMsg2, 'Glazing Config - S3 Requires Tempered Glass due to size. Tempered glass required over 99 united inches.');                
        system.assertEquals(assertMsg3, 'Glazing Config - s1Pattern unable to match configuration record.');        
        system.assertEquals(assertMsg4, 'Glazing Config - s2Pattern unable to match configuration record.');
        system.assertEquals(assertMsg5, 'Glazing Config - s3Pattern unable to match configuration record.');                
    }   
    @isTest
    public static void noMatchingAnythingTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestGlazingConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = new Map<string,MakabilityRestResource.OrderItem>();
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGlazingConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingAnythingTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Something bad happened with the Glazing config');        
    }    
     
    

    private static List<Product2> createTestProduct(Integer num){
        List<Product2> pros = new List<Product2>();
        for(Integer i = 0; i < num;i++){
            Product2 p = new Product2();
            p.name = 'test product ' + i;
            pros.add(p);            
        }        
        insert pros;
        return pros;
    }
    
    private static List<Product_Configuration__c> createTestProConfig(Integer num){
        List<Product2> pros = createTestProduct(num);
        List<Product_Configuration__c> pcs = new List<Product_Configuration__c>();
        for(Integer i = 0 ; i < num; i++){
            Product_Configuration__c pc = new Product_Configuration__c();
            pc.name = 'test config ' + i;
            pc.Tempered_Glass_Required_at_UI__c = 99;	
            pc.Product__c = pros[i].id;  
            pcs.add(pc);
        }
        insert pcs;
        return pcs;
    }
    
    private static void createTestGlazingConfig(Product_Configuration__c pc){
        List<Glazing_Configuration__c> gcs = new List<Glazing_Configuration__c>();
        Glazing_Configuration__c gc1 = new Glazing_Configuration__c();
        gc1.Glass_Pattern__c = 'Fern';
        gc1.Glazing__c = 'High Performance';
        gc1.Max_Height_Fraction__c = 'Even';
        gc1.Max_Height_Inches__c = 55;
        gc1.Max_Width_Inches__c = 55;
        gc1.Max_Width_Fraction__c = 'Even';
        gc1.Product_Configuration__c = pc.id;
        gc1.Tempered__c = True;
        gc1.Frame_Type__c ='Full Frame';
        gcs.add(gc1);
        
        Glazing_Configuration__c gc2 = new Glazing_Configuration__c();
        gc2.Glass_Pattern__c = 'Fern';
        gc2.Glazing__c = 'High Performance';
        gc2.Max_Height_Fraction__c = 'Even';
        gc2.Max_Height_Inches__c = 55;
        gc2.Max_Width_Inches__c = 55;
        gc2.Max_Width_Fraction__c = 'Even';
        gc2.Product_Configuration__c = pc.id;
        gc2.Tempered__c = false; 
        gc2.Frame_Type__c ='Full Frame';
        gcs.add(gc2);
        
        insert gcs;
    }

    private static Map<String,MakabilityRestResource.OrderItem>  createOrderItem(Id prodId, Decimal wInch, Decimal hInch, String wFrac, String Hfrac,
     Boolean s1Temp, Boolean s2Temp,String s1Pat,String s2Pat, String oiId){
        Map<string,MakabilityRestResource.OrderItem> orderItems = new Map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
        MakabilityCalculator mc = new MakabilityCalculator();
		ProductConfiguration pc = new ProductConfiguration();
        pc.widthInches = wInch;
        pc.widthFractions = wFrac;
        pc.heightInches = hInch;
        pc.heightFractions = hFrac;        
        pc.s1Tempering = s1Temp;
        pc.s2Tempering = s2Temp;
        pc.S3Tempering = s1Temp;          
        pc.s1Pattern = s1Pat;
        pc.s2Pattern = s2Pat;
        pc.s3Pattern = s1Pat;
        pc.s1Type = 'High Performance';
        pc.s2Type = 'High Performance';
        pc.s3Type = 'High Performance'; 
        pc.productId = prodId;
        pc.frame ='Full Frame';  
        mc.checkS3 = true; 
        oi.orderItemId = oiId;
        oi.makabilityCalculator = mc;
        oi.productConfiguration = pc;
        orderItems.put(oiId,oi);       
        return orderItems;
    }
}