/**
 * @description       : Glass pattern makability
 * @author            : mark.rothermal@andersencorp.com
 * @group             : 
 * @last modified on  : 02-24-2021
 * @last modified by  : mark.rothermal@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                            Modification
 * 1.0   12-12-2019  mark.rothermal@andersencorp.com   Initial Version
**/
public without sharing class MakabilityCheckGlassPatternConfig implements MakabilityService {
    
    
    /**
    * @description Check Makability function- compare orderItem Request against Product Config makability records
    * @author Manchala.Ramakrishna@andersencorp.com | 1/30/2020
    * @param map<string, MakabilityRestResource.OrderItem> requests
    * @param set<id> productIds
    * @return List<MakabilityRestResource.MakabilityResult>
    */
    Public Static List<MakabilityRestResource.MakabilityResult> checkCompatibility( map <string,MakabilityRestResource.OrderItem> requests, set<id> productIds ){
        
        List<MakabilityRestResource.MakabilityResult> results = new List<MakabilityRestResource.MakabilityResult>(); 
        Map<id,Glazing_Type_Pattern_Configuration__c> glazingTypePatternConfigMap = getConfigOptions(productIds);
        Map<id,map<string,string>> prodIdToTypePatternMap = createGlazingTypeToPatternMap(glazingTypePatternConfigMap);  
   
        
         // handle no config record found.
        if(glazingTypePatternConfigMap.keyset().size() == 0){
            for(String r:requests.keyset()){
                MakabilityRestResource.OrderItem oi = requests.get(r);
                MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult(); 
                List<string> errMessages = new List<string>();
                string errMessage = 'Salesforce - No Galzing Pattern Type Configurations  found for this productId.';  
                errMessages.add(errMessage);
                result.errorMessages = errMessages;
                result.orderId = oi.orderId;
                result.orderItemId = oi.orderItemId;
                results.add(result);                
            }            
            return results;
        }
        
        ////////////////////////////// 
        // run makability check  
        // //////////////////////////
        for(String r:requests.keyset()){
                MakabilityRestResource.OrderItem req = requests.get(r);                  
                List<string> errMessages = new List<string>();        
                MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
                result.orderItemId = req.orderItemId;
                result.orderId = req.orderId;
                Map<string,string> currentGlazingPatternMap = prodIdToTypePatternMap.get(req.productConfiguration.productId);
                String gType = req.productConfiguration.s1Type;
                String gPattern = req.productConfiguration.s1Pattern;
                String gType2 = req.productConfiguration.s2Type;
                String gPattern2 = req.productConfiguration.s2Pattern;
                system.debug('the incorrect glazing are '+gtype+gpattern+gtype2+gpattern2);
                system.debug('the incorrect glazing map  are '+currentGlazingPatternMap);
                if(gtype!=null&&!currentGlazingPatternMap.containskey(gType))
                {
                 errMessages.add('Glazing Pattern Config. Incorrect Glazing Type, available options are: ' + currentGlazingPatternMap.keyset());                     
                    result.isMakable = false;
                    result.errorMessages =  errMessages;                               
                    results.add(result);  
                }
                if(gtype2!=null&&!currentGlazingPatternMap.containskey(gType2))
                {
                
                 errMessages.add('Glazing Pattern Config. Incorrect Glazing Type, available options are: ' + currentGlazingPatternMap.keyset());                     
                    result.isMakable = false;
                    result.errorMessages =  errMessages;                               
                    results.add(result); 
                }
                if(gtype!=null&&currentGlazingPatternMap.containskey(gType)){
                    String glazingPatternOptions = currentGlazingPatternMap.get(gType);
                    if(checkInteriorColorConfig(glazingPatternOptions,gPattern)){
                     system.debug('the inside 1');
                        errMessages.add('Glazing Type Pattern  - passed');  
                        result.isMakable = true;
                        result.errorMessages = errMessages;                
                        results.add(result);                       
                    }else {
                        errMessages.add ('Glazing Type Pattern Config. Incorrect  Glazing  Pattern ' 
                                         + gPattern + 
                                         ' is not an option for an  Glazing Type ' +  gType +
                                         ' . available options are: ' + glazingPatternOptions );                         
                        result.isMakable = false;
                        result.errorMessages = errMessages;                               
                        results.add(result);                        
                    }
                }
              if(gtype2!=null&&currentGlazingPatternMap.containskey(gType2)){
                    String glazingPatternOptions = currentGlazingPatternMap.get(gType2);
                    if(checkInteriorColorConfig(glazingPatternOptions,gPattern2)){
                    system.debug('the inside 2');
                        errMessages.add('Glazing Type Pattern  - passed');  
                        result.isMakable = true;
                        result.errorMessages = errMessages;                
                        results.add(result);                       
                    }else {
                        errMessages.add ('Glazing Type Pattern Config. Incorrect  Glazing  Pattern ' 
                                         + gPattern2 + 
                                         ' is not an option for an  Glazing Type ' +  gType2 +
                                         ' . available options are: ' + glazingPatternOptions );                         
                        result.isMakable = false;
                        result.errorMessages = errMessages;                               
                        results.add(result);                        
                    }
                }
                
                
                   
            
        }
        system.debug('results check ' + results );        
        if(results.size() == 0){
            List<string> errMessages = new List<string>();                    
            errMessages.add('Something bad happened with the Glazing pattern Type config');
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
            result.errorMessages = errMessages;
            result.isMakable = false;
            results.add(result);
        }
        return results;
    }
    
    /**
    * @description compare 2 strings and return a boolean
    * @author Manchala.Ramakrishna@andersencorp.com | 1/30/2020
    * @param String config
    * @param string request
    * @return boolean
    */
    public static boolean checkInteriorColorConfig(String config, string request ){
        boolean makable = false;
        if(config.contains(request)){
            makable = true;
        }
        return makable;
    }

    /**
    * @description builds a map of config records to be used in main method
    * @author Manchala.Ramakrishna@andersencorp.com | 1/30/2020
    * @param set<id> productIds
    * @return Map<id,  Glazing_Type_Pattern_Configuration__c>
    */
    public static Map<id,Glazing_Type_Pattern_Configuration__c> getConfigOptions(set<id> productIds){
          Map<id,Glazing_Type_Pattern_Configuration__c> glazingTypePatternConfigMap= new Map<id,Glazing_Type_Pattern_Configuration__c>( [
            SELECT id,
            Glazing_Type__c,
            Glazing__c,
            Product_Configuration__r.Product__c
            from Glazing_Type_Pattern_Configuration__c
            where Product_Configuration__r.Product__c in :productids] );  
        return glazingTypePatternConfigMap ;
    }
    /**
    * @description builds a map of glazing type to glazing pattern.
    * @author Manchala.Ramakrishna@andersencorp.com | 1/30/2020
    * @param Map<id,Glazing_Type_Pattern_Configuration__c  >  glazingTypePatternConfigMap 
    * @return Map<id, map<string, string>>
    */
    public static Map<id,map<string,string>> createGlazingTypeToPatternMap(Map<id,Glazing_Type_Pattern_Configuration__c>glazingTypePatternConfigMap ){
        Map<id,map<string,string>> prodIdToTypePatternMap = new Map<id,map<string,string>>(); 
        for(id cc :glazingTypePatternConfigMap.keyset()){
            Glazing_Type_Pattern_Configuration__c  currentConfig =  glazingTypePatternConfigMap .get(cc);          
            if(prodIdToTypePatternMap.containskey(currentConfig.Product_Configuration__r.Product__c)){
                Map<string,string> glazingTypeToPatternMapPlaceholder = prodIdToTypePatternMap.get(currentConfig.Product_Configuration__r.Product__c); 
                if(! glazingTypeToPatternMapPlaceholder.containsKey(currentConfig.Glazing_Type__c)){
                     glazingTypeToPatternMapPlaceholder.put(currentConfig.Glazing_Type__c,currentConfig.Glazing__c);
                }
            } else {
                Map<string,string>  glazingTypeToPatternMapPlaceholder = new map<string,string>();
                 glazingTypeToPatternMapPlaceholder.put(currentConfig.Glazing_Type__c,currentConfig.Glazing__c);
                prodIdToTypePatternMap.put(currentConfig.Product_Configuration__r.Product__c,glazingTypeToPatternMapPlaceholder);
            }
        }
        return prodIdToTypePatternMap;
    }
}