/*
* @author Jason Flippen
* @date 03/11/2020
* @description: Provides database functionality for the serviceRequestRecordPageHeader LWC.
*
*			   Code Coverage provided by the following Test Class:
*              - ServiceReqRecordPageHeaderControllerTest
*/ 
public with sharing class ServiceReqRecordPageHeaderController {

    private static Set<String> cancelRequestProfileSet = new Set<String>{'HomeOwner Resolution','Super Administrator', 'System Administrator'};

    /*
    * @author Jason Flippen
    * @date 03/11/2020
    * @description: Retrieves Order object with specific fields.
    */ 
    @AuraEnabled(cacheable=true)
    public static OrderWrapper getOrder(Id orderId) {

        Order currentOrder = [SELECT Id,
                                     Cancel_Eligible__c,
                                     Install_Complete_Date__c,
                                     Name,
                                     OrderNumber,
                                     Order_Account_ID__c,
                                     Save_Reason__c,
                                     Secondary_Save_Reason__c,
                                     Service_Type__c,
                                     Sold_Order__c,
                                     Status,
                                     Sold_Order_JIP_Eligible__c,
                                     (SELECT Id, Product2Id, Product2.Name, Defect_Code__c  FROM OrderItems)
                              FROM   Order
                              WHERE  Id = :orderId];

        Set<String> TwentyYearWarrantCodesSet = new Set<String>();
        for(X20YearWarrantyCode__mdt warrantyLabel: [SELECT Id, MasterLabel FROM X20YearWarrantyCode__mdt]){
            TwentyYearWarrantCodesSet.add(warrantyLabel.MasterLabel);
        }
        system.debug(TwentyYearWarrantCodesSet);
        List<OrderItem> GlassOrderItems = new List<OrderItem>();
        List<OrderItem> NONGlassOrderItems = new List<OrderItem>();
        for(OrderItem oi: currentOrder.OrderItems){
            system.debug(oi);
            system.debug(oi.Product2.Name);
            system.debug(oi.Defect_Code__c);
            if(TwentyYearWarrantCodesSet.contains(oi.Defect_Code__c)){
                GlassOrderItems.add(oi);
            }
            else{
                NONGlassOrderItems.add(oi);
            }
        }
        system.debug('GlassOrderItems: '+GlassOrderItems);
        system.debug('NONGlassOrderItems: '+NONGlassOrderItems);
        OrderWrapper wrapper = new OrderWrapper();
        wrapper.id = currentOrder.Id;
        wrapper.cancelEligible = currentOrder.Cancel_Eligible__c;
        wrapper.name = currentOrder.Name;
        wrapper.orderNumber = currentOrder.OrderNumber;
        wrapper.orderAccountId = currentOrder.Order_Account_ID__c;
        wrapper.saveReason = currentOrder.Save_Reason__c;
        wrapper.secondarySaveReason = currentOrder.Secondary_Save_Reason__c;
        wrapper.serviceType = currentOrder.Service_Type__c;
        wrapper.soldOrderId = currentOrder.Sold_Order__c;
        wrapper.status = currentOrder.Status;
        wrapper.soldOrderJIPEligible = currentOrder.Sold_Order_JIP_Eligible__c;

        wrapper.eligibleForReimbursement = true;
        if (currentOrder.Install_Complete_Date__c != null) {
            Date installCompleteDate = currentOrder.Install_Complete_Date__c;
            Date currentDate = Date.today();
            Integer daysBetween = installCompleteDate.daysBetween(currentDate);
            Integer tenYearsNumberOfDays = (365 * 10);
            Integer twentyYearsNumberOfDays = (365 * 20);
            boolean isGlassEligible = true;
            boolean isNONGlassEligible = true;
            // Date TESTCompletedDate = Date.Today().addYears(-11);
            // Integer TESTdaysBetween = TESTCompletedDate.daysBetween(currentDate);
            // System.debug('TESTCompletedDate: '+ TESTCompletedDate);
            // System.debug('TESTdaysBetween: '+ TESTdaysBetween);
            // System.debug('tenYearsNumberOfDays: '+ tenYearsNumberOfDays);
            // System.debug('twentyYearsNumberOfDays: '+ twentyYearsNumberOfDays);
            if(NONGlassOrderItems.size() > 0){
                if (daysBetween > tenYearsNumberOfDays) {
                    isNONGlassEligible = false;
                }
            }
            if(GlassOrderItems.size() > 0){
                if (daysBetween > twentyYearsNumberOfDays) {
                    isGlassEligible = false;
                }
            }

            if(isGlassEligible == false || isNONGlassEligible == false){
                System.debug('NOT eligibleForReimbursement');
                wrapper.eligibleForReimbursement = false;
            }
        }
        return wrapper;
    }

    /*
    * @author Jason Flippen
    * @date 03/11/2020
    * @description: Retrieves Map of Service Types to be used in the Service Type picklist.
    */ 
    @AuraEnabled(cacheable=true)
    public static Map<String,String> getServiceTypeOptionMap(Boolean soldOrderJIPEligible) {

        Map<String,String> returnOptionMap = new Map<String,String>();


        Schema.DescribeFieldResult fieldResult = Order.Service_Type__c.getDescribe();
        List<Schema.PicklistEntry> peList = fieldResult.getPicklistValues();
        
        if (soldOrderJIPEligible == true) {

            for (Schema.PicklistEntry entry : peList) {
                if (entry.getValue() == 'Job In Progress' ||
                    entry.getValue() == 'Save'||
                    entry.getValue() == 'Legacy') {
                    returnOptionMap.put(entry.getLabel(), entry.getValue());
                }
            }

        }
        else {

            for (Schema.PicklistEntry entry : peList) {
                if (entry.getValue() != 'Job In Progress') {
                    returnOptionMap.put(entry.getLabel(), entry.getValue());
                }
            } 

        }
        
        return returnOptionMap;
        
    }

    /*
    * @author Jason Flippen
    * @date 03/11/2020
    * @description Retrieve flag indicating whether or not the
    *              Reject Request button should be shown.
    */ 
    @AuraEnabled(cacheable=true)
    public static Boolean getShowRejectRequestButton(String orderStatus) {
        
        Boolean returnValue = false;

        User currentUser = [SELECT Id,
                                   Profile.Name
                            FROM   User
                             WHERE  Id = :UserInfo.getUserId()];
        
        if (cancelRequestProfileSet.contains(currentUser.Profile.Name) && orderStatus != 'Warranty Rejected') {
            returnValue = true;
        }

        return returnValue;

    }
    
    /*
    * @author Jason Flippen
    * @date 03/11/2020
    * @description Updates Status of Order to "Cancelled".
    */ 
    @AuraEnabled
    public static String updateStatusCancelled(Id orderId) {
		
        String returnValue = 'Success';
        
        try {
            Order updateOrder = new Order(Id = orderId,
                                          Status = 'Cancelled',
                                          Date_Cancelled__c = Date.today());
            update updateOrder;
        }
        catch (Exception ex) {
            System.debug('*** UpdateStatusCancelled Error: ' + ex.getMessage());
            returnValue = ex.getMessage();
        }
        
        return returnValue;
        
    }
    
    /*
    * @author Jason Flippen
    * @date 03/11/2020
    * @description Updates Status of Order to "Warranty Rejected".
    */ 
    @AuraEnabled
    public static String updateStatusRejected(Id orderId) {
		
        String returnValue = 'Success';
        
        try {
            Order updateOrder = new Order(Id = orderId, Status = 'Warranty Rejected');
            update updateOrder;
        }
        catch (Exception ex) {
            System.debug('*** UpdateStatusRejected Error: ' + ex.getMessage());
            returnValue = ex.getMessage();
        }
        
        return returnValue;
        
    }
    
    /*
    * @author Jason Flippen
    * @date 03/11/2020
    * @description Updates Service Type of Order.
    */ 
    @AuraEnabled
    public static String updateServiceType(Order updateOrder) {
		
        String returnValue = 'Success';
        
        try {
            update updateOrder;
        }
        catch (Exception ex) {
            returnValue = ex.getMessage();
        }
        
        return returnValue;
        
    }
    
    /*
    * @author Jason Flippen
    * @date 03/11/2020
    * @description Method to return the data specific to the Conga Print buttons.
    */
    @AuraEnabled
    public static CongaPrintWrapper getCongaPrintData() {

        CongaPrintWrapper wrapper = new CongaPrintWrapper();

        // Are we in a Community?
        Boolean sourceIsCommunity = true;
        String networkId = Network.getNetworkId();
        if (networkId == null) {
            sourceIsCommunity = false;
        }
        
        wrapper.sourceIsCommunity = sourceIsCommunity;
        wrapper.baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        
        return wrapper;

    }


/** Wrapper Classes **/


    /*
    * @author Jason Flippen
    * @date 01/15/2021
    * @description Wrapper Class for Order record.
    */
    @TestVisible
    public class OrderWrapper {

        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public Boolean cancelEligible {get;set;}

        @AuraEnabled
        public Boolean eligibleForReimbursement {get;set;}

        @AuraEnabled
        public String name {get;set;}

        @AuraEnabled
        public String orderNumber {get;set;}

        @AuraEnabled
        public String orderAccountId {get;set;}

        @AuraEnabled
        public String saveReason {get;set;}

        @AuraEnabled
        public String secondarySaveReason {get;set;}

        @AuraEnabled
        public String serviceType {get;set;}

        @AuraEnabled
        public String soldOrderId {get;set;}

        @AuraEnabled
        public String status {get;set;}

        @AuraEnabled
        public Boolean soldOrderJIPEligible {get;set;}

    }

    /*
    * @author Jason Flippen
    * @date 03/11/2020
    * @description Conga Print Wrapper Class.
    */
    @TestVisible
    public class CongaPrintWrapper {

        @AuraEnabled
        public Boolean sourceIsCommunity {get;set;}

        @AuraEnabled
        public String baseUrl {get;set;}

    }

}