@isTest
private class CollapsableContent_UnitTest {
    @testSetup
    static void setup(){
        TestDataFactoryStatic.setUpConfigs();
        Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
        Database.insert(storeAccount, true);
        Id storeAccountId = storeAccount.Id;
        Id storeConfigId = storeAccount.Active_Store_Configuration__c;
        Id accountId = TestDataFactoryStatic.createAccount('Calvins', storeAccountId).Id;
        Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'CalvinPrime');
        Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('Calvins Opportunity', accountId, storeAccountId, 'Sold', date.today());
        System.debug('opportunity.Store_Location__c: ' + opportunity.Store_Location__c);
        OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
        Order ord = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        Order ord2 = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        Order ord3 = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        ord.Order_Processed_Date__c = date.today();
        ord2.Order_Processed_Date__c = date.today().addDays(-1);
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'Super Administrator' LIMIT 1].Id;
        User currentUser = TestDataFactoryStatic.createUser(profileID);
        currentUser.FirstName = 'testUser1';
        Database.insert(currentUser);
        ord.CustomerPortalUser__c = currentUser.Id;
        ord2.CustomerPortalUser__c = currentUser.Id;
        List<Order> ordList = new List<Order>{ord, ord2, ord3};
            Database.update(ordList);
    }

    @isTest
    static void getOrderIdTest(){
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
            Id orderId = CollapsableContentController.getOrderId();
            Order ord = [SELECT Order_Processed_Date__c FROM Order ORDER BY createdDate DESC LIMIT 1];
            // System.assertEquals(Date.today(), ord.Order_Processed_Date__c);
        }
    }

    @isTest
    static void getOrdersTest(){
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
            List<Order> orderList = CollapsableContentController.getOrders();
            System.assertEquals(2, orderList.size());
        }
    }
    @isTest
    static void getStoreInfoTest() {
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
            CollapsableContentController.OrderStoreConfigWrap thisOrder = CollapsableContentController.getStoreInfo();
            Order StoreInfo = [SELECT Id, Store_Location__c, Store_Location__r.Legal_Name__c, Store_Location__r.Phone,
                               Store_Location__r.BillingStreet, Store_Location__r.BillingCity, Store_Location__r.BillingPostalCode,
                               Store_Location__r.BillingState, Store_Location__r.BillingCountry
                               FROM Order
                               ORDER BY createdDate DESC LIMIT 1];
        }
    }
    @isTest
    static void getNewOrderId(){
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
            Id ordActualId = CollapsableContentController.getNewOrderId(String.valueOf(Date.today().addDays(-1)));
            Id ordExpectedId = [SELECT Id FROM Order WHERE Order_Processed_Date__c = :Date.today().addDays(-1)].Id;
            System.assertEquals(ordExpectedId, ordActualId);
        }
    }
    /////////////////////
    @isTest
    private static void getSelectedOrderTest(){
        User testUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        testUser.Selected_Order__c = 'test-initial-value';
        update testUser;
        System.runAs(testUser){
            Test.startTest();
            LightningResponse lr = CollapsableContentController.getSelectedOrder();
            Test.stopTest();
            //system.debug('lr in getSelectedOrder ' + lr );
            string u = lr.jsonResponse;
            System.assert(u.contains('test-initial-value'));
        }
    }
    @isTest
    private static void updateSelectedOrderTest(){
        User testUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        testUser.Selected_Order__c = 'test-initial-value';
        Id updateId = UserInfo.getUserId();
        update testUser;
        System.runAs(testUser){
            Test.startTest();
            LightningResponse lr = CollapsableContentController.updateSelectedOrder(updateId);
            Test.stopTest();
            User testResults = [SELECT Id, Selected_Order__c FROM User WHERE FirstName = 'testUser1'];
            system.assertEquals(updateId, testResults.Selected_Order__c);
            //system.debug('lr in getAllORders ' + lr );
        }
    }
    @isTest
    private static void getAllOrdersTest(){
        User testUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(testUser){
            Test.startTest();
            LightningResponse lr = CollapsableContentController.getAllOrders();
            Test.stopTest();
            //system.debug('lr in getAllORders ' + lr.jsonResponse );
            System.assert(lr.state == 'SUCCESS');
        }
    }

}