@isTest(SeeAllData=false)
global class CanvassUnitInsertionUpdateQueueableTest {

    public static testMethod void test1()
    {
        CNVSS_Canvass_Market__c canvassMarket = new CNVSS_Canvass_Market__c();
        canvassMarket.Name = 'Test';
        canvassMarket.CNVSS_Canvass_Market_Manager__c = userInfo.getUserId();
        canvassMarket.CNVSS_ISC_Phone_Number__c = '5555555555';
        insert canvassMarket;
        
        Canvass_Market_Zip_Code__c canvassMarketZip = new Canvass_Market_Zip_Code__c();
        canvassMarketZip.Name = '12345';
        canvassMarketZip.Zip_Code__c = '12345';
        canvassMarketZip.Canvass_Market__c = canvassMarket.id;
        insert canvassMarketZip;
        
        Custom_Data_Layer_Batch_Queue__c canvassDataLayerQueue = new Custom_Data_Layer_Batch_Queue__c();
        canvassDataLayerQueue.Status__c = 'Waiting for Insertion And Update';
        canvassDataLayerQueue.Field_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.ZipCode__c = '12345';
        canvassDataLayerQueue.Market_Id__c = canvassMarket.Id;
        insert canvassDataLayerQueue;
        
        CNVSS_Canvass_Unit__c CU = new CNVSS_Canvass_Unit__c();
        CU.CNVSS_Canvass_Market__c = canvassMarket.Id;
        CU.CNVSS_House_No__c = '123';
        CU.CNVSS_Street__c = 'Fake St 0';
        CU.Street_Type__c = 'Dr';
        CU.CNVSS_City__c = 'Charlotte';
        CU.CNVSS_State__c = 'NC';
        CU.CNVSS_Zip_Code__c = '12345';
        CU.Fullstreetaddress__c = '123 Fake St 0 Dr, Charlotte NC 12345';
        CU.Auto_Created__c = true;
        CU.Auto_Modified__c = false;
        CU.APN__c = '1234567890';
        CU.Data_Marker_ID__c = '1234567890';
        insert CU;
        
        CNVSS_Canvass_Unit__c CU2 = new CNVSS_Canvass_Unit__c();
        CU2.CNVSS_Canvass_Market__c = canvassMarket.Id;
        CU2.CNVSS_House_No__c = '1234';
        CU2.CNVSS_Street__c = 'Fake St 1';
        CU2.Street_Type__c = 'Dr';
        CU2.CNVSS_City__c = 'Charlotte';
        CU2.CNVSS_State__c = 'NC';
        CU2.CNVSS_Zip_Code__c = '12345';
        CU2.Fullstreetaddress__c = '1234 Fake St 1 Dr, Charlotte NC 12345';
        CU2.Auto_Created__c = true;
        CU2.Auto_Modified__c = false;
        CU2.APN__c = '1234567896';
        insert CU2;
        
        CNVSS_Canvass_Unit__c CU3 = new CNVSS_Canvass_Unit__c();
        CU3.CNVSS_Canvass_Market__c = canvassMarket.Id;
        CU3.CNVSS_House_No__c = '12345';
        CU3.CNVSS_Street__c = 'Fake St 2';
        CU3.Street_Type__c = 'Dr';
        CU3.CNVSS_City__c = 'Charlotte';
        CU3.CNVSS_State__c = 'NC';
        CU3.CNVSS_Zip_Code__c = '12345';
        CU3.Fullstreetaddress__c = '12345 Fake St 2 Dr, Charlotte NC 12345';
        CU3.Auto_Created__c = true;
        CU3.Auto_Modified__c = false;
        CU3.APN__c = '1';
        insert CU3;
        
        CNVSS_Canvass_Unit_Temporary__c canvassUnit = new CNVSS_Canvass_Unit_Temporary__c();
        canvassUnit.CNVSS_Canvass_Market__c = canvassMarket.Id;
        canvassUnit.CNVSS_House_No__c = '5200';
        canvassUnit.CNVSS_Street__c = '77 Center';
        canvassUnit.Street_Type__c = 'Dr';
        canvassUnit.CNVSS_City__c = 'Charlotte';
        canvassUnit.CNVSS_State__c = 'NC';
        canvassUnit.CNVSS_Zip_Code__c = '12345';
        canvassUnit.Fullstreetaddress__c = '5200 77 Center Dr #400, Charlotte, NC 28217';
        canvassUnit.Auto_Created__c = true;
        canvassUnit.Auto_Modified__c = false;
        canvassUnit.APN__c = '1234567890';
        canvassUnit.Data_Marker_ID__c = '1234567890';
        insert canvassUnit;
        
        CNVSS_Canvass_Unit_Temporary__c canvassUnitTwo = new CNVSS_Canvass_Unit_Temporary__c();
        canvassUnitTwo.CNVSS_Canvass_Market__c = canvassMarket.Id;
        canvassUnitTwo.CNVSS_House_No__c = '52001';
        canvassUnitTwo.CNVSS_Street__c = '77 Center';
        canvassUnitTwo.Street_Type__c = 'Dr';
        canvassUnitTwo.CNVSS_City__c = 'Charlotte';
        canvassUnitTwo.CNVSS_State__c = 'NC';
        canvassUnitTwo.CNVSS_Zip_Code__c = '12345';
        canvassUnitTwo.Fullstreetaddress__c = '5200 77 Center Dr #400, Charlotte, NC 28217';
        canvassUnitTwo.Auto_Created__c = true;
        canvassUnitTwo.Auto_Modified__c = false;
        canvassUnitTwo.APN__c = '1234567896';
        canvassUnitTwo.Data_Marker_ID__c = '1234567899';
        insert canvassUnitTwo;
        
        CNVSS_Canvass_Unit_Temporary__c AnotherCanvassUnit = new CNVSS_Canvass_Unit_Temporary__c();
        AnotherCanvassUnit.CNVSS_Canvass_Market__c = canvassMarket.Id;
        AnotherCanvassUnit.CNVSS_House_No__c = '12345';
        AnotherCanvassUnit.CNVSS_Street__c = 'Fake St';
        AnotherCanvassUnit.Street_Type__c = 'Dr';
        AnotherCanvassUnit.CNVSS_City__c = 'Charlotte';
        AnotherCanvassUnit.CNVSS_State__c = 'NC';
        AnotherCanvassUnit.CNVSS_Zip_Code__c = '12345';
        AnotherCanvassUnit.Fullstreetaddress__c = '12345 Fake St Dr, Charlotte, NC 28217';
        AnotherCanvassUnit.Auto_Created__c = true;
        AnotherCanvassUnit.Auto_Modified__c = false;
        AnotherCanvassUnit.APN__c = '1234567890';
        AnotherCanvassUnit.Data_Marker_ID__c = '1234567898';
        insert AnotherCanvassUnit;
        
        CNVSS_Canvass_Unit_Temporary__c AnotherCanvassUnit2 = new CNVSS_Canvass_Unit_Temporary__c();
        AnotherCanvassUnit2.CNVSS_Canvass_Market__c = canvassMarket.Id;
        AnotherCanvassUnit2.CNVSS_House_No__c = '123456';
        AnotherCanvassUnit2.CNVSS_Street__c = '123456 Fake Street';
        AnotherCanvassUnit2.Street_Type__c = 'Dr';
        AnotherCanvassUnit2.CNVSS_City__c = 'Charlotte';
        AnotherCanvassUnit2.CNVSS_State__c = 'NC';
        AnotherCanvassUnit2.CNVSS_Zip_Code__c = '12345';
        AnotherCanvassUnit2.Fullstreetaddress__c = '123456 123456 Fake Street 0 Dr, Charlotte, NC 28217';
        AnotherCanvassUnit2.Auto_Created__c = true;
        AnotherCanvassUnit2.Auto_Modified__c = false;
        AnotherCanvassUnit2.APN__c = '888888888888';
        AnotherCanvassUnit2.Data_Marker_ID__c = '999999999999';
        insert AnotherCanvassUnit2;
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('MockPropertyDataFields');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
                
        list<Custom_Data_Layer_Batch_Queue__c> scope = [SELECT Id, Name, Market_Id__c, Status__c, ZipCode__c 
                                                        FROM Custom_Data_Layer_Batch_Queue__c 
                                                        WHERE Status__c = 'Waiting for Insertion And Update'];
        // iterate over scope
        for(Custom_Data_Layer_Batch_Queue__c queuedJob :scope)
        {
            // start queueable job
            System.enqueueJob(new CanvassUnitInsertionUpdateQueueable(queuedJob.Market_Id__c,queuedJob.ZipCode__c,queuedJob.Id));
            
            list<string> canvassUnitTemp = new list<string>();
            canvassUnitTemp.add(canvassUnit.Id);
            canvassUnitTemp.add(AnotherCanvassUnit.Id);
            
            System.enqueueJob(new CanvassUnitInsertionUpdateQueueable(queuedJob.Market_Id__c,queuedJob.ZipCode__c,queuedJob.Id,canvassUnitTemp));
        }
        
        Test.stopTest();
    }
}