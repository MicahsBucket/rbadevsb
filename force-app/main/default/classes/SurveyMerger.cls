global with sharing class SurveyMerger implements Schedulable {
	
	global void execute(SchedulableContext sc) {
		//Variable Instantiation
		Date today = Date.today();
		Map<String, Survey__c> enabledIdtoSurveyMap = new Map<String, Survey__c>();
		Map<String, Survey__c> existingEnabledIdtoSurveyMap = new Map<String, Survey__c>();
		Map<String, Survey__c> surveysToDeleteMap = new Map<String, Survey__c>();
		Map<Id, List<Survey_Worker__c>> surveyIdToOldSurveyWorkersMap = new Map<Id, List<Survey_Worker__c>>();
		Map<Id, List<Survey_Worker__c>> surveyIdToNewSurveyWorkersMap = new Map<Id, List<Survey_Worker__c>>();
		List<Survey_Worker__c> newSurveyWorkersToUpdate = new List<Survey_Worker__c>();
		List<Id> oldSurveyIds = new List<Id>();
		List<Id> newSurveyIds = new List<Id>();
		List<Survey__c> surveysToUpdate = new List<Survey__c>();
		List<Survey_Worker__c> surveyWorkersToUpdate = new List<Survey_Worker__c>();
		List<String> enabledIds = new List<String>();
		Set<String> existingEnabledIds = new Set<String>();

		//Find new surveys and create map to enabled Id
		List<Survey__c> newSurveys = [SELECT Id, Installation_Date__c, Enabled_Appointment_Id__c FROM Survey__c WHERE CreatedDate = :today AND Source_System__c = 'rForce'];
		for(Survey__c s : newSurveys){
			enabledIds.add(s.Enabled_Appointment_Id__c);
			enabledIdtoSurveyMap.put(s.Enabled_Appointment_Id__c, s);
		}

		//Find split installs and create a map of enabled Id to the surveys
		List<Survey__c> existingSurveys = [SELECT Id, Installation_Date__c, Enabled_Appointment_Id__c FROM Survey__c WHERE Enabled_Appointment_Id__c IN :enabledIds AND Source_System__c = 'rForce' AND CreatedDate != :today];
		for(Survey__c s : existingSurveys){
			existingEnabledIds.add(s.Enabled_Appointment_Id__c);
			existingEnabledIdtoSurveyMap.put(s.Enabled_Appointment_Id__c, s);
			oldSurveyIds.add(s.Id);
		}
		//Create map of new surveys to enabled Ids that already have existing surveys with the same enabled Id
		for(String enabledId : enabledIdtoSurveyMap.keySet()){
			if(existingEnabledIds.contains(enabledId)){
				surveysToDeleteMap.put(enabledId, enabledIdtoSurveyMap.get(enabledId));
				newSurveyIds.add(enabledIdtoSurveyMap.get(enabledId).Id);
			}
		}

		//Get and create maps for old and new survey workers
		List<Survey_Worker__c> oldSurveyWorkers = [SELECT Survey_Id__c, Worker_Id__c FROM Survey_Worker__c WHERE Survey_Id__c IN :oldSurveyIds];
		List<Survey_Worker__c> newSurveyWorkers = [SELECT Survey_Id__c, Worker_Id__c FROM Survey_Worker__c WHERE Survey_Id__c IN :newSurveyIds];
		surveyIdToOldSurveyWorkersMap = createSurveyWorkerMap(oldSurveyWorkers);
		surveyIdToNewSurveyWorkersMap = createSurveyWorkerMap(newSurveyWorkers);

		//create list of Survey Workers to assign to old Survey
		for(String enabledId : existingEnabledIds){
			List<Survey_Worker__c> tempSurveyWorker = updateSurveyWorkers(surveysToDeleteMap.get(enabledId), existingEnabledIdtoSurveyMap.get(enabledId), surveyIdToOldSurveyWorkersMap, surveyIdToNewSurveyWorkersMap);
			//update install date of old survey to that of the new survey
			Survey__c oldSurvey = existingEnabledIdtoSurveyMap.get(enabledId);
			oldSurvey.Installation_Date__c = surveysToDeleteMap.get(enabledId).Installation_Date__c;
			surveysToUpdate.add(oldSurvey);
			if(tempSurveyWorker != null){
				surveyWorkersToUpdate.addAll(tempSurveyWorker);
			}
		}

		//Perform DML operations on Surveys and Survey Workers
		Database.update(surveyWorkersToUpdate);
		Database.update(surveysToUpdate);
		Database.delete(surveysToDeleteMap.values());

	}

	//Reassigns Survey Workers from the new Survey to the old Survey and updates Install Date of old Survey
	public static List<Survey_Worker__c> updateSurveyWorkers(Survey__c newSurvey, Survey__c oldSurvey, Map<Id, List<Survey_Worker__c>> oldSurveyWorkers, Map<Id, List<Survey_Worker__c>> newSurveyWorkers){
		List<Survey_Worker__c> newWorkersToUpdate = new List<Survey_Worker__c>();
		List<Survey_Worker__c> newWorkers = newSurveyWorkers.get(newSurvey.Id);
		if(newWorkers == null){
			return null;
		}
		if(oldSurveyWorkers.get(oldSurvey.Id) != null){
			Set<Survey_Worker__c> oldWorkers = new Set<Survey_Worker__c>(oldSurveyWorkers.get(oldSurvey.Id));
			for(Survey_Worker__c sw : newWorkers){
				if(!oldWorkers.contains(sw)){
					newWorkersToUpdate.add(sw);
				}
			}
			for(Survey_Worker__c sw : newWorkersToUpdate){
				sw.Survey_Id__c = oldSurvey.Id;
			}
	    return newWorkersToUpdate;
		} else {
			return null;
		}
		

	}

	//Create Survey Worker Map
	public static Map<Id, List<Survey_Worker__c>> createSurveyWorkerMap(List<Survey_Worker__c> surveyWorkers){
		Map<Id, List<Survey_Worker__c>> surveyIdToSurveyWorkersMap = new Map<Id, List<Survey_Worker__c>>();
		List<Survey_Worker__c> tempSurveyWorkerList = new List<Survey_Worker__c>();
		for(Survey_Worker__c sw : surveyWorkers){
			if(surveyIdToSurveyWorkersMap.get(sw.Survey_Id__c) != null){
				tempSurveyWorkerList.add(sw);
				surveyIdToSurveyWorkersMap.put(sw.Survey_Id__c, tempSurveyWorkerList);
				tempSurveyWorkerList = null;
			} else {
				List<Survey_Worker__c> tempList = new List<Survey_Worker__c>();
				tempList.add(sw);
				surveyIdToSurveyWorkersMap.put(sw.Survey_Id__c, tempList);
			}
		}
		return surveyIdToSurveyWorkersMap;
	}

}