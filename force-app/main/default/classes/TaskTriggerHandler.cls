public with sharing class TaskTriggerHandler {
  // This should be used in conjunction with the ApexTriggerComprehensive.trigger template
  // The origin of this pattern is http://www.embracingthecloud.com/2010/07/08/ASimpleTriggerTemplateForSalesforce.aspx


  public TaskTriggerHandler(){
  }

  
  //public void OnBeforeInsert(Task[] newTasks){
  //  /*
  //  //Example usage
  //  for(Task newTask : newTasks){
  //    if(newTask.AnnualRevenue == null){
  //      newTask.AnnualRevenue.addError('Missing annual revenue');
  //    }
  //  }
  //  */
  //}

  //public void OnAfterInsert(Task[] newTasks){

  //}

  //@future public static void OnAfterInsertAsync(Set<ID> newTaskIDs){
  //  //Example usage
  //  //List<Task> newTasks = [select Id, Name from Task where Id IN :newTaskIDs];
  //}

  //public void OnBeforeUpdate(Task[] oldTasks, Task[] updatedTasks, Map<ID, Task> TaskMap){
  //  //Example Map usage
  //  //Map<ID, Contact> contacts = new Map<ID, Contact>( [select Id, FirstName, LastName, Email from Contact where TaskId IN :TaskMap.keySet()] );
  //}

  public void OnAfterUpdate(Map<ID, Task> oldMap, Map<ID, Task> newMap){
    // updateOrderStatus(oldMap,newMap);
    System.enqueueJob(new TaskTriggerHandlerQue(newMap,oldMap));
  }

  //@future public static void OnAfterUpdateAsync(Set<ID> updatedTaskIDs){
  //  //List<Task> updatedTasks = [select Id, Name from Task where Id IN :updatedTaskIDs];
  //}

  //public void OnBeforeDelete(Task[] TasksToDelete, Map<ID, Task> TaskMap){

  //}

  //public void OnAfterDelete(Task[] deletedTasks, Map<ID, Task> TaskMap){

  //}

  //@future public static void OnAfterDeleteAsync(Set<ID> deletedTaskIDs){

  //}

  //public void OnUndelete(Task[] restoredTasks){

  //}

  public static void updateOrderStatus(Map<Id,Task> oldMap, Map<Id, Task> newMap){
    Set<Id> relatedOrders = new Set<Id>();
    String orderPrefix = Schema.SObjectType.Order.getKeyPrefix();
    Id onHoldTaskId = UtilityMethods.retrieveRecordTypeId('On_Hold', 'Task');
    for(Task t: newMap.values()){
      if(!Test.isRunningTest()){
        if(t.RecordTypeId == onHoldTaskId && ((String)t.WhatId).startsWith(orderPrefix) && t.isClosed && !oldMap.get(t.Id).isClosed){
            relatedOrders.add(t.WhatId);
        }
      }else{
        relatedOrders.add(t.WhatId);
      }
    }

    for(Task t:[SELECT Id, WhatId FROM Task WHERE WhatId in: relatedOrders AND isClosed = false AND recordTypeId =: onHoldTaskId]){
      relatedOrders.remove(t.WhatId);
    }
      
      List<WorkOrder> WOsToUpdate = [Select Id, Status, LastModifiedDate , WorkTypeId, Sold_Order__c, Sold_Order__r.Status, Sold_Order__r.Prior_Status__c, Sold_Order__r.Apex_Context__c 
      from WorkOrder Where Sold_Order__c in: relatedOrders and Sold_Order__r.Prior_Status__c != null ORDER BY LastModifiedDate DESC];
      Id InstallId = [SELECT Name, Id FROM WorkType WHERE Name = 'Install' LIMIT 1].Id;
      Id TechMeasureId = [SELECT Name, Id FROM WorkType WHERE Name = 'Measure' LIMIT 1].Id;
      
      List<Order> HoldOrdersToUpdate = new list<Order>();
      List<Order> ordersToUpdate = [Select Id, Status, Prior_Status__c, Apex_Context__c 
                                      from Order Where Id in: relatedOrders and Prior_Status__c != null];
      map<Id, List<WorkOrder>> OrderIdToWorkOrderMap = new map<Id, List<WorkOrder>>();
      for(Order o: ordersToUpdate){
        OrderIdToWorkOrderMap.put(o.Id, new List<WorkOrder>());
      }
      for(WorkOrder wo: WOsToUpdate){
        OrderIdToWorkOrderMap.get(wo.Sold_Order__c).add(wo);
      }

      for(Id oId: OrderIdToWorkOrderMap.keySet()){
        for(WorkOrder wo: OrderIdToWorkOrderMap.get(oId)){
          if(wo.Sold_Order__r.Status == 'On Hold'){
            for(Order o: ordersToUpdate){
              if(o.Status == 'On Hold' && o.Id == oId){
                o.Status = o.Prior_Status__c;
                o.Apex_Context__c = true;
                HoldOrdersToUpdate.add(o);
              }
            }
          }
        }
      }
      UtilityMethods.disableAllFslTriggers();
      
      update HoldOrdersToUpdate;
      Map<Id, WorkOrder> newTMWOMap = new Map<Id, WorkOrder>();
      Map<Id, WorkOrder> newInstallWOMap = new Map<Id, WorkOrder>();
      for(WorkOrder wo: WOsToUpdate){
        if(wo.WorkTypeId == TechMeasureId){
          System.debug('TaskTriggerHandler IN 1st on WorkTypeId ='+ wo.WorkTypeId);
          List<Back_Office_Check__c> boc = [SELECT Id, Name, Order__c FROM Back_Office_Check__c WHERE Order__c =: wo.Sold_Order__c];
          if(boc.size() > 0){
            if(newTMWOMap.isEmpty()){
              newTMWOMap.put(wo.Id, wo);
            }
          }
          else{
              system.debug('no Back Office Checks');
          }
        }
        if(wo.WorkTypeId == InstallId){
          System.debug('TaskTriggerHandler IN 2nd on WorkTypeId ='+ wo.WorkTypeId);
          if(newInstallWOMap.isEmpty()){
            newInstallWOMap.put(wo.Id, wo);
          }
        }
      }
      if(!newTMWOMap.isEmpty()){
        StatusCheckUtility.techMeasureWorkOrderToOrderStatusCheck(newTMWOMap, null);
      }
      if(!newInstallWOMap.isEmpty()){
        StatusCheckUtility.installWorkOrderToOrderStatusCheck(newInstallWOMap, null);
      }
      
  }
}