/**
 * @File Name          : MakabilityCheckSpecialtyShapeConfig.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 02-24-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/4/2019, 7:54:35 PM   mark.rothermal@andersencorp.com     Initial Version
**/
public without sharing class MakabilityCheckSpecialtyShapeConfig  {
    
    /**
    * @description
    * @author mark.rothermal@andersencorp.com | 5/4/2019
    * @param MakabilityRestResource.OrderItem request
    * @param Size_Detail_Configuration__c currentConfig
    * @param map<string, Product_Field_Control__c> uniKeyToPfcMap
    * @param map<string, Product_Field_Control_Dependency__c> uniKeyToPfcdMap
    * @return specialShapeResult
    */
    public static specialShapeResult checkShape( MakabilityRestResource.OrderItem request,  Size_Detail_Configuration__c currentConfig, map<string,Product_Field_Control__c> uniKeyToPfcMap, map<string,Product_Field_Control_Dependency__c> uniKeyToPfcdMap ){
        specialShapeResult result = new specialShapeResult();
        boolean makabilityPasses = true;
		result.errMessages = new List<string>();
        ProductConfiguration p = request.ProductConfiguration;
        Boolean checkSquareFootMax = false;
        Boolean checkWidthToHeightRatios = false;
        Boolean checkPeakHeight = false;
        Boolean checkLegHeightsMatch = false;
        Boolean configReqRightLeg = false;
        Boolean configReqLeftLeg = false;
        Double heightTimesWidth = MakabilityUtility.multiplyHeightbyWidth(p.heightInches, p.heightFractions, p.widthInches, p.widthFractions);
        Double widthDividedByHeight = MakabilityUtility.divideWidthByHeight(p.heightInches, p.heightFractions, p.widthInches, p.widthFractions);
        Double shortLeg;
        Double convertedSingleLeg;
        Double convertedLeftLeg;
        Double convertedRightLeg;
        Double convertedMinLegHeight;// = currentConfig.Min_Leg_Height_Inches__c + Constants.fractionConversionMap.get(currentConfig.Min_Leg_Height_Fraction__c);
        Double convertedMaxLegHeight;// = currentConfig.Max_Leg_Height_Inches__c + Constants.fractionConversionMap.get(currentConfig.Max_Leg_Height_Fraction__c);
        Double convertedHeight = p.heightInches + Constants.fractionConversionMap.get(p.heightFractions);
        Double convertedWidth = p.widthInches + Constants.fractionConversionMap.get(p.widthFractions);

        system.debug('current config in special shape check ' + currentConfig);

        // determine additional checks to perform.
        if(currentConfig.Peak_Height_Min__c != null || currentConfig.Peak_Height_Max__c != null){
            checkPeakHeight = true;
        }
        if(currentConfig.Glass_Square_Foot_Max__c != null){
            checkSquareFootMax = true;
        }
        if(currentConfig.Width_to_Height_Minimum__c != null && currentConfig.Width_to_Height_Maximum__c != null){
            checkWidthToHeightRatios = true;
        }
        if(currentConfig.Match_Leg_Heights__c == true){
            checkLegHeightsMatch = true;
        }

        // Updated  4/22/19 mtr - product config requires right or left leg values based on if a product field control record for that leg exists
        // and that the leg is not disabled by product field control depenancies. the initial conditional statements below also looked to see
        // if the fields were required from product field controls. that data was changed to no longer reflect that. all leg fields are now required
        // through product field control dependencies only. 
        if(uniKeyToPfcMap.containsKey(p.productId + '-' + 'Right Leg Inches')){
            //Product_Field_Control__c pfc = uniKeyToPfcMap.get(p.productId + '-' + 'Right Leg Inches'); // commented out 4/22/19 mtr
            // removed pfc.required__c && . as part of rfbuild-110. mtr 4/22/19
            if(checkPfcd(p.productId + '-' + 'Right Leg Inches' + '-' + p.specialtyShape,uniKeyToPfcdMap)){
                configReqRightLeg = true;
            }
        }
        if(uniKeyToPfcMap.containsKey(p.productId + '-' + 'Left Leg Inches')){
            //Product_Field_Control__c pfc = uniKeyToPfcMap.get(p.productId + '-' + 'Left Leg Inches'); // commented out 4/22/19 mtr
            // removed pfc.required__c && . as part of rfbuild-110. see above mtr 4/22/19
            if(checkPfcd(p.productId + '-' + 'Left Leg Inches' + '-' + p.specialtyShape,uniKeyToPfcdMap)){
                configReqLeftLeg = true;
            }
        }
        // set config leg values
        if(currentConfig.Min_Leg_Height_Inches__c != null && currentConfig.Min_Leg_Height_Fraction__c != null){
            convertedMinLegHeight = currentConfig.Min_Leg_Height_Inches__c + Constants.fractionConversionMap.get(currentConfig.Min_Leg_Height_Fraction__c); 
        }
        if(currentConfig.Max_Leg_Height_Inches__c != null && currentConfig.Max_Leg_Height_Fraction__c != null){
            convertedMaxLegHeight = currentConfig.Max_Leg_Height_Inches__c + Constants.fractionConversionMap.get(currentConfig.Max_Leg_Height_Fraction__c);
        }
        // determine short leg and set convertedSingleLeg
        if(request.makabilityCalculator.hasLeftLeg && request.makabilityCalculator.hasRightLeg){
                shortLeg = MakabilityUtility.setShortLeg(p.leftLegInches,  p.leftLegFraction,  p.rightLegInches,  p.rightLegFraction);
                convertedLeftLeg = p.leftLegInches + Constants.fractionConversionMap.get(p.leftLegFraction);
                convertedRightLeg = p.rightLegInches + Constants.fractionConversionMap.get(p.rightLegFraction);
        } else {
            if(request.makabilityCalculator.hasLeftLeg){
                shortLeg = p.leftLegInches + Constants.fractionConversionMap.get(p.leftLegFraction);
                convertedSingleLeg = p.leftLegInches + Constants.fractionConversionMap.get(p.leftLegFraction);
            }
            if(request.makabilityCalculator.hasRightLeg){
                shortLeg = p.rightLegInches + Constants.fractionConversionMap.get(p.rightLegFraction);
                convertedSingleLeg = p.rightLegInches + Constants.fractionConversionMap.get(p.rightLegFraction);
            }
        }
        //**********start makability flow*************\\

        // legs makability
        //    system.debug('hit check legs makability ' + configReqLeftLeg + configReqRightLeg);
        if(configReqRightLeg || configReqLeftLeg){
            // check single leg. if true, run checks for the converted single leg.
            if(currentConfig.Single_Leg__c){
                // check min leg height
                if(convertedSingleLeg < convertedMinLegHeight){
                makabilityPasses = false;
                result.errMessages.add('Speciality Shape Config - Leg Height does not meet the minimum height of ' + currentConfig.Min_Leg_Height_Inches__c  + ' ' + currentConfig.Min_Leg_Height_Fraction__c  + ' Inches');
                }
                // check max leg height
                if(convertedSingleLeg > convertedMaxLegHeight){
                 makabilityPasses = false;
                result.errMessages.add('Speciality Shape Config - Leg Height exceeds the maximum height of ' + currentConfig.Max_Leg_Height_Inches__c  + ' ' + currentConfig.Max_Leg_Height_Fraction__c  + ' Inches');
                }

            }else{
            // run checks for both legs 
            // check min leg height  
                if(convertedLeftLeg < convertedMinLegHeight || convertedRightLeg < convertedMinLegHeight){
                makabilityPasses = false;
                result.errMessages.add('Speciality Shape Config - Leg Heights does not meet the minimum heights of ' + currentConfig.Min_Leg_Height_Inches__c  + ' ' + currentConfig.Min_Leg_Height_Fraction__c  + ' Inches');
                }
            // check max leg height
                if(convertedLeftLeg > convertedMaxLegHeight || convertedRightLeg > convertedMaxLegHeight){
                makabilityPasses = false;
                result.errMessages.add('Speciality Shape Config - Leg Heights exceeds the maximum heights of ' + currentConfig.Max_Leg_Height_Inches__c  + ' ' + currentConfig.Max_Leg_Height_Fraction__c  + ' Inches');
                }
            }

            if(checkPeakHeight){
                // check peak height min
                if(convertedWidth / (convertedHeight - shortLeg) <= currentConfig.Peak_Height_Min__c){
                makabilityPasses = false;
                result.errMessages.add('Speciality Shape Config - Unit does not meet the minimum peak height ratio of ' + currentConfig.Peak_Height_Min__c);
                }
                // check peak height max
                if(convertedWidth / (convertedHeight - shortLeg) >= currentConfig.Peak_Height_Max__c){
                makabilityPasses = false;
                result.errMessages.add('Speciality Shape Config - Unit exceeds the peak height ratio of ' + currentConfig.Peak_Height_Max__c);
                }
            }
            // verify that legs that are suppose to match height, do match height. equal leg arch vs unequal leg arch scenario
            // rule added 4/19/19 mtr rfbuild 110.
            if(checkLegHeightsMatch){
                if(convertedLeftLeg != convertedRightLeg){
                    makabilityPasses = false;
                    result.errMessages.add('Speciality Shape Config - This Unit requires matching leg heights.');
                }
            }
        } 
        
        //******************* check square foot max and width to height ratios. ****************\\ 

        if(checkSquareFootMax){
            // check  square foot max  
            if(heightTimesWidth / 144 > currentConfig.Glass_Square_Foot_Max__c){
                makabilityPasses = false;
                result.errMessages.add('Speciality Shape Config - Unit exceeds the rectangular limitation of ' + currentConfig.Glass_Square_Foot_Max__c);
            }
        }

    	if(checkWidthToHeightRatios) {
    		// check min width to height ratios
		    if(widthDividedByHeight <= currentConfig.Width_to_Height_Minimum__c) {
	    		makabilityPasses= false;
    			result.errMessages.add('Speciality Shape Config - Unit does not meet the minimum width to height ratio of ' + currentConfig.Width_to_Height_Minimum__c);
		    }
	    	// check max width to height ratios
    		if(widthDividedByHeight >= currentConfig.Width_to_Height_Maximum__c ) {
			    makabilityPasses = false;
		    	result.errMessages.add('Speciality Shape Config - Unit does not meet the maximum width to height ratio of ' + currentConfig.Width_to_Height_Maximum__c);
	    	}
    	}

//******************************************************************************\\
        if(makabilityPasses){
            result.productMakable = true;
          //  result.errMessages.add('Speciality Shape Config - passed');
            
        } else{
            result.productMakable = false;
        }
        system.debug('special shape result '+ result);
        return result;
    }
//*************************End Makability****************************\\
    

    /**
    * @description method for checking product field dependency records against request.  to determine if right or left leg values are actually required.

    * @author mark.rothermal@andersencorp.com | 5/4/2019
    * @param string uniKey
    * @param Map<string, Product_Field_Control_Dependency__c> uniKeyToPfcdMap
    * @return boolean
    */
    public static boolean checkPfcd(string uniKey, Map<string,Product_Field_Control_Dependency__c> uniKeyToPfcdMap ){
        boolean result = true;
        Product_Field_Control_Dependency__c pfcd;
        system.debug('uniKeypfcdmap ' + uniKeyToPfcdMap);
        if(uniKeyToPfcdMap.containsKey(uniKey)){
            pfcd = uniKeyToPfcdMap.get(uniKey);
        }
        if(pfcd != null && pfcd.Action_Taken__c == 'Disable'){
            result = false;
        }
        return result;
    }


    public class specialShapeResult {
      public  Boolean productMakable {get;set;}
      public  List<String>  errMessages {get;set;}
    }

        
}