@IsTest
public class WorkOrderPhotoPickerControllerTest {

    private static final String INSTALL_WORK_TYPE_NAME = 'Install';
    private static final String MEASURE_WORK_TYPE_NAME = 'Measure';
    private static final String SERVICE_WORK_TYPE_NAME = 'Service';
    private static final String JOB_SITE_VISIT_WORK_TYPE_NAME = 'Job Site Visit';


    @IsTest
    public static void test_getListOfFileIds() {
        //SETUP
        Blob myBlob = Blob.valueof('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8BQDwAEhQGAhKmMIQAAAABJRU5ErkJggg==');
        
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
        RMS_Settings__c customSetting2 = new RMS_Settings__c(Value__c = '1234567890', Name='Ordering Portal Profile ID');

		insert new List<RMS_Settings__c>{customSetting1,customSetting2};

        // Account acct = new Account(Name='TEST_ACCT');
        // insert acct;

        WorkType installWorkType = new WorkType(
                Name = INSTALL_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 8,
                DurationType = 'Hours'
        );

        WorkType measureWorkType = new WorkType(
                Name = MEASURE_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 8,
                DurationType = 'Hours'
        );

        WorkType serviceWorkType = new WorkType(
                Name = SERVICE_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 8,
                DurationType = 'Hours'
        );

        WorkType jobSiteVisitWorkType = new WorkType(
                Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 1,
                DurationType = 'Hours'
        );
        insert new List<WorkType>{installWorkType, measureWorkType, serviceWorkType, jobSiteVisitWorkType};

        Map<String, RecordType> woRecordTypeMap = createWorkOrderRecordTypeMap();
        WorkOrder wo = new WorkOrder();
        // wo.Sold_Order__c = mainOrder.Id;
        // wo.AccountId = acct.Id;
        wo.RecordTypeId = woRecordTypeMap.get('Install').Id;
        insert wo;


        ContentVersion vers = new ContentVersion(
            Title = 'Test Title',
            PathOnClient = 'Test.rbaproj',
            VersionData = myBlob,
            IsMajorVersion = true
        );
        insert vers;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        ContentDocumentLink fileLink = new ContentDocumentLink();
        fileLink.LinkedEntityId = wo.Id;
        // fileLink.ContentDocumentId = [SELECT Id, ContentDocumentId From ContentVersion WHERE Id = :vers.Id].ContentDocumentId;
        fileLink.ContentDocumentId = documents[0].Id;
        fileLink.Visibility = 'InternalUsers';
        fileLink.ShareType = 'V';
        insert fileLink;


        Attachment attach = new Attachment();
        attach.Name='Unit Test Attachment';
        Blob bodyBlob = myBlob;
        attach.body=bodyBlob;
        attach.parentId=wo.id;
        insert attach;


        //TEST
        List<Id> results = new List<Id>();

        Test.startTest();
            results = WorkOrderPhotoPickerController.getListOfFileIds(wo.Id);
        Test.stopTest();


        //ASSERT
        System.assertEquals(0, results.size());
    }



    @IsTest
    public static void test_getListOfAttachmentIds() {
        //SETUP
        Blob myBlob = Blob.valueof('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8BQDwAEhQGAhKmMIQAAAABJRU5ErkJggg==');
        
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
        RMS_Settings__c customSetting2 = new RMS_Settings__c(Value__c = '1234567890', Name='Ordering Portal Profile ID');

		insert new List<RMS_Settings__c>{customSetting1,customSetting2};

        // Account acct = new Account(Name='TEST_ACCT');
        // insert acct;

        WorkType installWorkType = new WorkType(
                Name = INSTALL_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 8,
                DurationType = 'Hours'
        );

        WorkType measureWorkType = new WorkType(
                Name = MEASURE_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 8,
                DurationType = 'Hours'
        );

        WorkType serviceWorkType = new WorkType(
                Name = SERVICE_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 8,
                DurationType = 'Hours'
        );

        WorkType jobSiteVisitWorkType = new WorkType(
                Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 1,
                DurationType = 'Hours'
        );
        insert new List<WorkType>{installWorkType, measureWorkType, serviceWorkType, jobSiteVisitWorkType};

        Map<String, RecordType> woRecordTypeMap = createWorkOrderRecordTypeMap();
        WorkOrder wo = new WorkOrder();
        // wo.Sold_Order__c = mainOrder.Id;
        // wo.AccountId = acct.Id;
        wo.RecordTypeId = woRecordTypeMap.get('Install').Id;
        insert wo;


        ContentVersion vers = new ContentVersion(
            Title = 'Test Title',
            PathOnClient = 'Test.rbaproj',
            VersionData = myBlob,
            IsMajorVersion = true
        );
        insert vers;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        ContentDocumentLink fileLink = new ContentDocumentLink();
        fileLink.LinkedEntityId = wo.Id;
        // fileLink.ContentDocumentId = [SELECT Id, ContentDocumentId From ContentVersion WHERE Id = :vers.Id].ContentDocumentId;
        fileLink.ContentDocumentId = documents[0].Id;
        fileLink.Visibility = 'InternalUsers';
        fileLink.ShareType = 'V';
        insert fileLink;


        Attachment attach = new Attachment();
        attach.Name='Unit Test Attachment';
        Blob bodyBlob = myBlob;
        attach.body=bodyBlob;
        attach.parentId=wo.id;
        insert attach;


        //TEST
        List<Id> results = new List<Id>();

        Test.startTest();
            results = WorkOrderPhotoPickerController.getListOfAttachmentIds(wo.Id);
        Test.stopTest();


        //ASSERT
        System.assertEquals(1, results.size());
    }


     @IsTest
    public static void test_getFile() {
        //SETUP
        Blob myBlob = Blob.valueof('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8BQDwAEhQGAhKmMIQAAAABJRU5ErkJggg==');
        
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
        RMS_Settings__c customSetting2 = new RMS_Settings__c(Value__c = '1234567890', Name='Ordering Portal Profile ID');

		insert new List<RMS_Settings__c>{customSetting1,customSetting2};

        // Account acct = new Account(Name='TEST_ACCT');
        // insert acct;

        WorkType installWorkType = new WorkType(
                Name = INSTALL_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 8,
                DurationType = 'Hours'
        );

        WorkType measureWorkType = new WorkType(
                Name = MEASURE_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 8,
                DurationType = 'Hours'
        );

        WorkType serviceWorkType = new WorkType(
                Name = SERVICE_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 8,
                DurationType = 'Hours'
        );

        WorkType jobSiteVisitWorkType = new WorkType(
                Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 1,
                DurationType = 'Hours'
        );
        insert new List<WorkType>{installWorkType, measureWorkType, serviceWorkType, jobSiteVisitWorkType};

        Map<String, RecordType> woRecordTypeMap = createWorkOrderRecordTypeMap();
        WorkOrder wo = new WorkOrder();
        // wo.Sold_Order__c = mainOrder.Id;
        // wo.AccountId = acct.Id;
        wo.RecordTypeId = woRecordTypeMap.get('Install').Id;
        insert wo;


        ContentVersion vers = new ContentVersion(
            Title = 'Test Title',
            PathOnClient = 'Test.rbaproj',
            VersionData = myBlob,
            IsMajorVersion = true
        );
        insert vers;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        ContentDocumentLink fileLink = new ContentDocumentLink();
        fileLink.LinkedEntityId = wo.Id;
        // fileLink.ContentDocumentId = [SELECT Id, ContentDocumentId From ContentVersion WHERE Id = :vers.Id].ContentDocumentId;
        fileLink.ContentDocumentId = documents[0].Id;
        fileLink.Visibility = 'InternalUsers';
        fileLink.ShareType = 'V';
        insert fileLink;


        Attachment attach = new Attachment();
        attach.Name='Unit Test Attachment';
        Blob bodyBlob = myBlob;
        attach.body=bodyBlob;
        attach.parentId=wo.id;
        insert attach;


        //TEST
        WorkOrderPhotoPickerController.WorkOrderPhotoWrapper results;

        Test.startTest();
            results = WorkOrderPhotoPickerController.getFile(vers.Id);
        Test.stopTest();

        Boolean actual = false;
        if(results != null){
            actual = true;
        }

        //ASSERT
        System.assertEquals(true, actual);
    }


     @IsTest
    public static void test_getAttachment() {
        //SETUP
        Blob myBlob = Blob.valueof('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8BQDwAEhQGAhKmMIQAAAABJRU5ErkJggg==');
        
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
        RMS_Settings__c customSetting2 = new RMS_Settings__c(Value__c = '1234567890', Name='Ordering Portal Profile ID');

		insert new List<RMS_Settings__c>{customSetting1,customSetting2};

        // Account acct = new Account(Name='TEST_ACCT');
        // insert acct;

        WorkType installWorkType = new WorkType(
                Name = INSTALL_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 8,
                DurationType = 'Hours'
        );

        WorkType measureWorkType = new WorkType(
                Name = MEASURE_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 8,
                DurationType = 'Hours'
        );

        WorkType serviceWorkType = new WorkType(
                Name = SERVICE_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 8,
                DurationType = 'Hours'
        );

        WorkType jobSiteVisitWorkType = new WorkType(
                Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
                MinimumCrewSize = 1,
                RecommendedCrewSize = 2,
                EstimatedDuration = 1,
                DurationType = 'Hours'
        );
        insert new List<WorkType>{installWorkType, measureWorkType, serviceWorkType, jobSiteVisitWorkType};

        Map<String, RecordType> woRecordTypeMap = createWorkOrderRecordTypeMap();
        WorkOrder wo = new WorkOrder();
        // wo.Sold_Order__c = mainOrder.Id;
        // wo.AccountId = acct.Id;
        wo.RecordTypeId = woRecordTypeMap.get('Install').Id;
        insert wo;


        ContentVersion vers = new ContentVersion(
            Title = 'Test Title',
            PathOnClient = 'Test.rbaproj',
            VersionData = myBlob,
            IsMajorVersion = true
        );
        insert vers;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        ContentDocumentLink fileLink = new ContentDocumentLink();
        fileLink.LinkedEntityId = wo.Id;
        // fileLink.ContentDocumentId = [SELECT Id, ContentDocumentId From ContentVersion WHERE Id = :vers.Id].ContentDocumentId;
        fileLink.ContentDocumentId = documents[0].Id;
        fileLink.Visibility = 'InternalUsers';
        fileLink.ShareType = 'V';
        insert fileLink;


        Attachment attach = new Attachment();
        attach.Name='Unit Test Attachment';
        Blob bodyBlob = myBlob;
        attach.body=bodyBlob;
        attach.parentId=wo.id;
        attach.ContentType = 'image/png';
        insert attach;


        //TEST
        WorkOrderPhotoPickerController.WorkOrderPhotoWrapper results;

        Test.startTest();
            results = WorkOrderPhotoPickerController.getAttachment(attach.Id);
        Test.stopTest();

        Boolean actual = false;
        if(results != null){
            actual = true;
        }

        //ASSERT
        System.assertEquals(true, actual);
    }




    // @IsTest
    // public static void test_getWorkOrderPhotos() {

    //     //SETUP
    //     Blob myBlob = Blob.valueof('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8BQDwAEhQGAhKmMIQAAAABJRU5ErkJggg==');
        
    //     RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
    //     RMS_Settings__c customSetting2 = new RMS_Settings__c(Value__c = '1234567890', Name='Ordering Portal Profile ID');

	// 	insert new List<RMS_Settings__c>{customSetting1,customSetting2};

    //     // Account acct = new Account(Name='TEST_ACCT');
    //     // insert acct;

    //     WorkType installWorkType = new WorkType(
    //             Name = INSTALL_WORK_TYPE_NAME,
    //             MinimumCrewSize = 1,
    //             RecommendedCrewSize = 2,
    //             EstimatedDuration = 8,
    //             DurationType = 'Hours'
    //     );

    //     WorkType measureWorkType = new WorkType(
    //             Name = MEASURE_WORK_TYPE_NAME,
    //             MinimumCrewSize = 1,
    //             RecommendedCrewSize = 2,
    //             EstimatedDuration = 8,
    //             DurationType = 'Hours'
    //     );

    //     WorkType serviceWorkType = new WorkType(
    //             Name = SERVICE_WORK_TYPE_NAME,
    //             MinimumCrewSize = 1,
    //             RecommendedCrewSize = 2,
    //             EstimatedDuration = 8,
    //             DurationType = 'Hours'
    //     );

    //     WorkType jobSiteVisitWorkType = new WorkType(
    //             Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
    //             MinimumCrewSize = 1,
    //             RecommendedCrewSize = 2,
    //             EstimatedDuration = 1,
    //             DurationType = 'Hours'
    //     );
    //     insert new List<WorkType>{installWorkType, measureWorkType, serviceWorkType, jobSiteVisitWorkType};

    //     Map<String, RecordType> woRecordTypeMap = createWorkOrderRecordTypeMap();
    //     WorkOrder wo = new WorkOrder();
    //     // wo.Sold_Order__c = mainOrder.Id;
    //     // wo.AccountId = acct.Id;
    //     wo.RecordTypeId = woRecordTypeMap.get('Install').Id;
    //     insert wo;

    //     ContentVersion vers = new ContentVersion(
    //             Title = 'Test Title',
    //             PathOnClient = 'Test.rbaproj',
    //             VersionData = myBlob,
    //             IsMajorVersion = true
    //     );
    //     insert vers;

    //     ContentDocumentLink fileLink = new ContentDocumentLink();
    //     fileLink.LinkedEntityId = wo.Id;
    //     fileLink.ContentDocumentId = [SELECT Id, ContentDocumentId From ContentVersion WHERE Id = :vers.Id].ContentDocumentId;
    //     fileLink.Visibility = 'InternalUsers';
    //     fileLink.ShareType = 'V';


    //     Attachment attach = new Attachment();    
    //     attach.Name='Unit Test Attachment';
    //     Blob bodyBlob = myBlob;
    //     attach.body=bodyBlob;
    //     attach.parentId=wo.id;
    //     insert attach;

        
    //     //TEST
    //     List<WorkOrderPhotoPickerController.WorkOrderPhotoWrapper> results = new List<WorkOrderPhotoPickerController.WorkOrderPhotoWrapper>();

    //     Test.startTest();
    //         results = WorkOrderPhotoPickerController.getWorkOrderPhotos(wo.Id);
    //     Test.stopTest();

    //     //ASSERT
    //     System.assertEquals(true, true);

    // }

    private static Map<String, RecordType> createWorkOrderRecordTypeMap() {
        List<RecordType> woRecordTypes = [select Id, DeveloperName from RecordType where sObjectType = 'WorkOrder'];
        Map<String, RecordType> woRecordTypeMap = new Map<String, RecordType>();
        for (RecordType rt : woRecordTypes) {
            woRecordTypeMap.put(rt.DeveloperName, rt);
        }
        System.debug(woRecordTypeMap);
        return woRecordTypeMap;
    }
}