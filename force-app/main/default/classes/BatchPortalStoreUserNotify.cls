/** Class to Create Portal User and as well Send Notifications */
global class BatchPortalStoreUserNotify implements Database.Batchable<Sobject>, Database.Stateful{
	global final Set<String> emailList=new Set<String>();
    global final Map<ID,List<ID>> contactType=new Map<ID,List<ID>>();
    global Database.QueryLocator  start(Database.BatchableContext bc) {
        return Database.getQueryLocator('Select Id,AccountId,BookingWithInvitationDelay__c,OpportunityId,BillToContactId,Primary_Contact__c,Secondary_Contact__c,Eligible_For_Portal_User__c,EffectiveDate,Store_Location__c,Store_Location__r.Active_Store_Configuration__r.Invitation_Delay_Days__c,Store_Location__r.Active_Store_Configuration__r.Time_Zone__c,Store_Location__r.Active_Store_Configuration__r.Store_Default_email__c,Store_Location__r.Active_Store_Configuration__r.Remit_to_Company_Name__c,ArePortalUsersCreated__c from Order WHERE  BookingWithInvitationDelay__c<=TODAY and ArePortalUsersCreated__c=false and Eligible_For_Portal_User__c=true and Status != \'Cancelled\' and Status !=\'Install Complete\' and Status !=\'Job Closed\' and Status !=\'Pending Cancellation\' and RecordType.Name=\'CORO Record Type\'');
    }  
    global void execute(Database.BatchableContext BC, List<Sobject> scope){
        List<Order> allOrder = (List<Order>) scope;
        PortalUserCreation.ResultSet resultSet=PortalUserCreation.validateAndCreatePortalUser(allOrder,emailList,contactType);
        emailList.addAll(resultSet.emailList);        
    }    
    global void finish(Database.BatchableContext BC){
        // Final Email to Send Completion of Batch
        System.debug('________emailList_________'+emailList);
    }
}