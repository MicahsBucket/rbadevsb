public with sharing class RMS_customPOLookupController {

  private Apexpages.StandardController standardController;
 
  public Purchase_Order__c po {get;set;} 
  public List<Purchase_Order__c> results {get;set;} // search results
  public String searchString {get;set;} // search keyword
  public String orderString {get;set;} // order Number
  public Order ord {get;set;} // 
  public String orderItemId {get;set;}
  public String vendorString {get;set;}
  
  public RMS_customPOLookupController() {
    po = new Purchase_Order__c();
  	Set<String> vendorProductNames = new Set<String>();
    Set<String> vendorPONames = new Set<String>();
  	List<String> vendorsToSearchList = new List<String>();
  	String[] queryForVendorSearch = new String[0];
  	vendorString = '';
  	

    // getting Order ID and Product ID from query string, which I added to script on prod config page
    String productId = System.currentPageReference().getParameters().get('prodid');
    String orderId = System.currentPageReference().getParameters().get('orderid');

    // getting Order Number from Order
    Order ord = [Select id, OrderNumber from Order where Id =: orderId LIMIT 1];
    orderString = ord.OrderNumber;  // this is variable which gets sent to performSearch method to add to search query string

    List<Vendor_Product__c> vp = [Select ID, Vendor__r.Name from Vendor_Product__c where Product__r.ID =: productId]; // not inactive?
    for(Vendor_Product__c v : vp){
    	vendorProductNames.add(v.Vendor__r.Name);
    }

    List<Purchase_Order__c> po = [Select ID, Vendor__r.Name, Order__r.OrderNumber, Owner.Alias, Total__c, Date__c from Purchase_Order__c where Order__r.OrderNumber =: orderString];  // not cancelled?
    for(Purchase_Order__c p : po){
    	vendorPONames.add(p.Vendor__r.Name);
    }

   	if(vendorProductNames != null && vendorPONames != null){
   		
	    for(String match : vendorProductNames ){
	    	if(vendorPONames.contains(match))
	    		vendorsToSearchList.add(match);
	    }

   	}
   
    if(vendorsToSearchList != null){
		
		    for(String item: vendorsToSearchList) {
		    	queryForVendorSearch.add(String.valueOf(item));
			}
		vendorString = String.join(queryForVendorSearch, '\' OR Name = \'');
		
		vendorString = ' AND (Vendor__r.Name = \'' + vendorString + '\' )';
    }
    
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    runSearch();  
  }
   
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
  
  // prepare the query and issue the search command
  private void runSearch() {
    results = performSearch(searchString, orderString, vendorString);               
  } 
  
  // run the search and return the records found. 
  private List<Purchase_Order__c> performSearch(string searchString, string orderString, string vendorString) {

    String soql = 'SELECT ID, Name, RecordType.Name, Vendor__r.Name, Total__c, Order__r.OrderNumber, Owner.Alias, Date__c FROM Purchase_Order__c WHERE Order__r.OrderNumber =\'' + orderString + '\' ' + vendorString ;
    if(searchString != '' && searchString != null)
    soql = soql + '  ORDER BY Name ASC NULLS LAST LIMIT 100';
    System.debug(soql);
    return database.query(soql); 

  }
    
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
    
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }

}