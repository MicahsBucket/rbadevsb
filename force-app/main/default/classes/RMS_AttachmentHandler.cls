/*******************************************************//**

@class	  RMS_AttachmentHandler

@brief	 Handler for the Attachment trigger

@author	 Ian Fitzpatrick (Slalom.IF)

@version	2017-3-1  Slalom.IF
	Created

@see		RMS_AttachmentHandlerTests

@copyright  (c)2017 Slalom.  All Rights Reserved.
			Unauthorized use is prohibited.

***********************************************************/
public with sharing class RMS_AttachmentHandler {
	public static void onBeforeInsert(list<Attachment> newList){
		//if the Parent Id is for an Opportunity
		map<String,ID> orderMap = new map<String, ID>();
		Set<ID> orderIds = new Set<ID>();
		Set<String> rSuiteIds = new Set<String>();

		//loop over all the attachments sent in and only look at the Opportunity Attachments.
		for (Attachment a:newList){
			String testRsuiteId = getrSuiteId(a.Name);
			if(a.ParentId.getSObjectType()==Schema.Opportunity.SObjectType && testRsuiteId != null){
				rSuiteIds.add(getrSuiteId(a.Name));
				orderIds.add(a.ParentId);
			}
		}

		//if there are no valid Opportunities in this list exit this method.
		if(rSuiteIds == null){
			return;
		}

		//Query for Orders related to these opportunities
		list<Order> orderList = [select Id, rSuite_Id__c, OpportunityId 
								from Order 
								where rSuite_Id__c in: rSuiteIds 
								and OpportunityId in :orderIds];	
		for(Order o:orderList){
			//create a map 
			orderMap.put(o.OpportunityId + '-' + o.rSuite_Id__c, o.Id);
		}	

		//now loop over the attachments and look for Orders match this rSuite Id and update their association.
		for (Attachment a:newList){
			//we are looking for orders that match
			if(orderMap.containsKey(a.ParentId + '-' + getrSuiteId(a.Name))){
				a.ParentId = orderMap.get(a.ParentId + '-' + getrSuiteId(a.Name));
				a.Name = getFileName(a.Name);
				continue;
			}
		}
	}


	public static String getrSuiteId(String initialName){
		String returnString = null;
		if(initialName.contains('-') && initialName.countMatches('-') == 6 ) {
			list<String> splitString = initialName.split('-',7);
			returnString = splitString[1]+'-' + splitString[2]+'-' + 
			splitString[3]+'-' + splitString[4]+'-' + splitString[5];
		} else {
			return null;
		}					
		return returnString;
	}

	private static String getFileName(String initialName){
		if(initialName.contains('-') && initialName.countMatches('-') == 6 ) {
			return initialName.split('-',7)[6];
		} else {
			return initialName;
		}
	}
}