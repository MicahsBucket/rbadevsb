/**
 * @author Micah Johnson - Demand Chain 2020
 * @description This Queueable delays the start of status rollbacks until the system has the necessary processing power
 * @param The parameters passed in are the old and new maps of the sObject that was inserted/updated as well as a string defining which object needs to be checked
 * @returns N/A
 **/

public class RollBackStatusQue implements Queueable{
    public map<Id, sObject> oMap;
    public map<Id, sObject> nMap;
    public string oType;
    public RollBackStatusQue(map<Id, sObject> newMap, map<Id, sObject> oldMap, string type) {
        this.nMap = newMap;
        this.oMap = oldMap;
        this.oType = type;
    }

    public void execute(QueueableContext context) {
        System.debug('nMap>>>> '+ nMap);
        System.debug('oType>>>> '+ oType);
        RollBackUtility.RollBackStatuses(nMap, oMap, oType);
    }
}