public class OpportunityUpdateQue implements Queueable{
    public List<Opportunity> opps;

    public OpportunityUpdateQue(List<Opportunity> oppsToUpdate) {
        this.opps = oppsToUpdate;
        
    }

    public void execute(QueueableContext context) {
        // UtilityMethods.disableAllFslTriggers();
        update opps;
    }
}