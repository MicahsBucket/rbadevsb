@isTest
private class EditWorkersApexController_UnitTest {
	@testSetup
	static void setup(){
		TestDataFactory.setUpConfigs();
		Account testStoreAccount = TestDataFactory.createStoreAccount('Test Store');
		Database.insert(testStoreAccount, true);
		Contact testContact = TestDataFactory.createCustomerContact(testStoreAccount.Id);
		Database.insert(testContact, true);
		User partnerUser = TestDataFactory.createPartnerUser('Test', 'Partner RMS-RSR', testContact.Id);
		Database.insert(partnerUser, true);
	}

	@isTest static void canGetWorkers() {
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Set<Worker__c> salesSet = new Set<Worker__c>(TestDataFactory.createWorkers('Sales Rep', 5, accountId));
		Set<Id> salesIdSet = new Set<Id>();
		for(Worker__c w : salesSet){
			salesIdSet.add(w.Id);
		}
		Set<Worker__c> installerSet = new Set<Worker__c>(TestDataFactory.createWorkers('Installer', 4, accountId));
		Set<Id> installerIdSet = new Set<Id>();
		for(Worker__c w : installerSet){
			installerIdSet.add(w.Id);
		}
		Set<Worker__c> inactiveSet = new Set<Worker__c>(TestDataFactory.createWorkers('Inactive', 3, accountId));
		Set<Id> inactiveIdSet = new Set<Id>();
		for(Worker__c w : inactiveSet){
			inactiveIdSet.add(w.Id);
		}
		test.startTest();
				List<Worker__c> salesList = EditWorkersApexController.getWorkers('Sales Rep');
				List<Worker__c> inactiveList = EditWorkersApexController.getWorkers('Inactive');
				system.assertEquals(true, salesIdSet.contains(salesList[0].Id));
				system.assertEquals(true, inactiveIdSet.contains(inactiveList[0].Id));
				system.assertNotEquals(true, installerIdSet.contains(salesList[0].Id));
				system.assertEquals(5, salesSet.size());
		test.stopTest();
	}
	
	@isTest static void canSaveWorkers() {
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Id surveyId = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0].Id;
		Worker__c worker = TestDataFactory.createWorkers('Tech Measurer', 1, accountId, surveyId)[0];
		test.startTest();
				String originalFirstName = worker.First_Name__c;
				String originalLastName = worker.Last_Name__c;
				String originalEmail = worker.Email__c;
				String originalPhone = worker.Mobile__c;
				String originalRole = worker.Role__c;
				String firstName = 'Calvin';
				String lastName = 'OKeefe';
				String email = 'calvin.okeefe@gmail.com';
				String phone = '6517657777';
				String newRoles = 'Installer';
				Worker__c updatedWorker = EditWorkersApexController.saveWorker(worker, firstName, lastName, phone, email, newRoles);
				system.assertEquals(true, updatedWorker.First_Name__c == 'Calvin');
				system.assertNotEquals(true, updatedWorker.Role__c == 'Tech Measurer');
			
		test.stopTest();
	}

	@isTest static void canFindWorkers() {
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Worker__c w = new Worker__c(
				First_Name__c = 'worker0',
				Last_Name__c = 'lastname0',
				Role__c = 'Sales Rep',
				Store_Account__c = accountId,
				Email__c = 'test@test.com'
			);
		Database.insert(w, true);
		List<Worker__c> nullWorkers = EditWorkersApexController.findWorker(null);
		Worker__c worker = [SELECT Id, First_Name__c FROM Worker__c];
		system.assertEquals(null, nullWorkers);
		List<Worker__c> duplicateWorkers = EditWorkersApexController.findWorker('worker0 ');
		List<Worker__c> duplicateWorkers2 = EditWorkersApexController.findWorker('test@test.com');
	}
	
}