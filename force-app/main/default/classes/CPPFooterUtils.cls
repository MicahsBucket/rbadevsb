public class CPPFooterUtils {    
    @AuraEnabled
    public static FooterWrapper loadFooterDetails() {
        FooterWrapper fwrapper=new FooterWrapper();
        String userId=UserInfo.getUserId();
        User userRecord=[select Id,name,ContactId,Contact.firstname,Contact.lastname,Contact.email,Contact.HomePhone,Contact.MailingPostalCode,Contact.MobilePhone,Selected_Order__c from User where Id=:userId];
        fwrapper.contactRecord=userRecord.Contact;
        fwrapper.userRecord=userRecord;
        fwrapper.isInstallationComplete=false;
        fwrapper.isSurveySent=false;
        if(userRecord.Selected_Order__c!=null) 
        {
			fwrapper.orderRecord=new Order();            
        	fwrapper.orderRecord=[select Id,Installation_Date__c,Store_Location__c,Store_Location__r.Active_Store_Configuration__c,(select Survey_Status__c,Date_Sent__c, RecordTypeId  from Surveys__r where RecordType.DeveloperName='Post_Install' and Date_Sent__c!=null),
                               Store_Location__r.Active_Store_Configuration__r.Customer_Service_Number__c,Status from Order where Id=:userRecord.Selected_Order__c];    
        	if(fwrapper.orderRecord.Installation_Date__c<System.today())
            {
                fwrapper.isInstallationComplete=true;
            }
            if(fwrapper.orderRecord.Surveys__r.size()>0)
            {
                fwrapper.isSurveySent=true;
            }
        }
        return fwrapper;
    }
    @AuraEnabled
    public static RBAFormResponse submitRBAFrom(String contactRecord)
    {
        System.debug('_____contactRecord______'+contactRecord);
        Contact contactDetails=(Contact)JSON.deserialize(contactRecord, Contact.class);
        
        //String userId=UserInfo.getUserId();
        //User userRecord=[select Id,name,ContactId,Contact.firstname,Contact.lastname,Contact.email,Contact.phone,Contact.MailingPostalCode,Contact.MobilePhone from User where Id=:userId];
        RBA_Form_Integration__mdt rbaFormDetails=[select Id,DeveloperName,Authorization__c,FormType__c,Request_URL__c,Sender__c,
                                                  Source__c,SourceBreakdown__c from RBA_Form_Integration__mdt 
                                                  where DeveloperName='Consultation_Request'];
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(rbaFormDetails.Request_URL__c);
        req.setMethod('POST');
        req.setHeader('content-type', HttpFormBuilder.GetContentType());
        req.setHeader('authorization',rbaFormDetails.Authorization__c);
        String errormessage;
        // string contentType = EinsteinVision_HttpBodyPart.GetContentType();
        
        string form64 = '';
        try
        {
            form64 += HttpFormBuilder.WriteBoundary();
            form64 += HttpFormBuilder.WriteBodyParameter('FirstName', contactDetails.firstname);
            
            form64 += HttpFormBuilder.WriteBoundary();
            form64 += HttpFormBuilder.WriteBodyParameter('LastName', contactDetails.lastname);
            
            form64 += HttpFormBuilder.WriteBoundary();
            form64 += HttpFormBuilder.WriteBodyParameter('EmailAddress', contactDetails.email);
            
            form64 += HttpFormBuilder.WriteBoundary();
            form64 += HttpFormBuilder.WriteBodyParameter('PhoneNumber', contactDetails.MobilePhone);    
            
            form64 += HttpFormBuilder.WriteBoundary();
            form64 += HttpFormBuilder.WriteBodyParameter('Zipcode', contactDetails.MailingPostalCode);      
            
            form64 += HttpFormBuilder.WriteBoundary();
            form64 += HttpFormBuilder.WriteBodyParameter('Form Type', rbaFormDetails.FormType__c);
            
            form64 += HttpFormBuilder.WriteBoundary();
            form64 += HttpFormBuilder.WriteBodyParameter('RbA Source', rbaFormDetails.Source__c);
            
            form64 += HttpFormBuilder.WriteBoundary();
            form64 += HttpFormBuilder.WriteBodyParameter('RbA Breakdown', rbaFormDetails.SourceBreakdown__c);
            
            form64 += HttpFormBuilder.WriteBoundary();
            form64 += HttpFormBuilder.WriteBodyParameter('Sender', rbaFormDetails.Sender__c);
            form64 += HttpFormBuilder.WriteBoundary(HttpFormBuilder.EndingType.CrLf);
            
            Blob formBlob = EncodingUtil.base64Decode(form64);
            String contentLength = string.valueOf(formBlob.size());
            
            req.setBodyAsBlob(formBlob);
            req.setHeader('Connection', 'keep-alive');
            req.setHeader('Content-Length', contentLength);
            req.setTimeout(60*1000);
            
            Http http = new Http();
            HTTPResponse res = http.send(req);
            String responseBody=res.getBody();
            System.debug('____responseBody______'+responseBody);
            RBAFormResponse responseCustom=RBAFormResponse.parse(responseBody);
            if(Test.isRunningTest())
            {
                responseCustom.data='test data';
                responseCustom.message='test data';
                responseCustom.status='200K';
            }
            System.debug('____responseCustom_____'+responseCustom);
            
            return responseCustom;
        }
        catch(Exception e)
        {
            System.debug('_____e______'+e.getMessage());
            RBAFormResponse formError=new RBAFormResponse();
            formError.status='500';
            formError.data='Error';
            formError.message='Error Occurred while connecting to Rba form:'+e.getMessage();
            return formError;
        }
    }
    public class FooterWrapper
    {
        @AuraEnabled public Contact contactRecord{get;set;}
        @AuraEnabled public User userRecord{get;set;}
        @AuraEnabled public Order orderRecord{get;set;}
        @AuraEnabled public Boolean isInstallationComplete{get;set;}
        @AuraEnabled public Boolean isSurveySent{get;set;}
    }
}