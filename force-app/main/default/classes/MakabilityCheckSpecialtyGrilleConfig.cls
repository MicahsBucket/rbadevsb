/**
 * @description       : 
 * @author            : mark.rothermal@andersencorp.com
 * @group             : 
 * @last modified on  : 02-24-2021
 * @last modified by  : mark.rothermal@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                            Modification
 * 1.0   12-24-2019  mark.rothermal@andersencorp.com   Initial Version
**/
public without sharing class MakabilityCheckSpecialtyGrilleConfig implements MakabilityService {

public static List<MakabilityRestResource.MakabilityResult>  checkCompatibility( map <string,MakabilityRestResource.OrderItem> requests, set<id> productIds){
	List<MakabilityRestResource.MakabilityResult> results = new List<MakabilityRestResource.MakabilityResult>();
	Map<id,Specialty_Grille_Configuration__c> configMap = getConfigOptions(productIds);
	Set<String> spcConfigKey = new Set<String>();

	// handle no config records found in salesforce.
	if(configMap.keyset().size() == 0) {
		for(String r:requests.keyset()) {
			MakabilityRestResource.OrderItem oi = requests.get(r);
			MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
			List<string> errMessages = new List<string>();
			string errMessage = 'Salesforce - No Specialty Grille configurations found for this productId.';
			errMessages.add(errMessage);
			result.errorMessages = errMessages;
			result.orderId = oi.orderId;
			result.orderItemId = oi.orderItemId;
			results.add(result);
		}
		return results;
	}
	// build pattern to grille config set.
	for(id cId:configMap.keyset()) {
		Specialty_Grille_Configuration__c spc  = configMap.get(cId);
		String uniKey = createUniqueKey(spc);
		spcConfigKey.add(uniKey);

	}
	//////////////////////////////
	// run makability check
	// //////////////////////////
	for(String r:requests.keyset()) {
		MakabilityRestResource.OrderItem req = requests.get(r);
		List<string> errMessages = new List<string>();
		MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
		result.orderItemId = req.orderItemId;
		result.orderId = req.orderId;
		ProductConfiguration p = req.ProductConfiguration;
		Boolean requestHasSpecialityGrille = false;
		String requestKey = 'placeholder';
		Boolean specialGrillePasses = true;
		// do we check makability.
		if(req.makabilityCalculator.checkSpecialGrille) {
			requestKey = createRequestKey(p);
			requestHasSpecialityGrille = true;
			system.debug('unique key in request ' + requestKey);
		}
		if(requestHasSpecialityGrille) {
			if(!spcConfigKey.contains(requestKey) && req.productConfiguration.grillePattern != 'No Pattern' && req.productConfiguration.grillePattern != 'No Grille' ) {
				specialGrillePasses = false;
				errMessages.add('Specialty Grille Config - You have selected an invalid grille configuration.'+
				                ' Please consult technical documentation for valid pattern, style,'+
				                'lite, hub & spoke configurations for the specified specialty shape');
			}
		}
		if(!requestHasSpecialityGrille) {
			result.isMakable = true;
			return results;
		}
		if(specialGrillePasses) {
			errMessages.add('Specialty Grille Config - passed');
			result.isMakable = true;
		} else{
			result.isMakable = false;
		}

		result.errorMessages = errMessages;
		results.add(result);
	}
	system.debug('results check ' + results );

	// should never hit the code block below.
	if(results.size() == 0) {
		List<string> errMessages = new List<string>();
		errMessages.add('Something bad happened with the Specialty Grille config');
		MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
		result.errorMessages = errMessages;
		result.isMakable = false;
		results.add(result);
	}
	return results;
}

public static Map<id,Specialty_Grille_Configuration__c> getConfigOptions(set<id> productIds){
	Map<id,Specialty_Grille_Configuration__c> configMap= new Map<id,Specialty_Grille_Configuration__c>( [
		SELECT id,
		Grille_Pattern__c,
		Grille_Style__c,
		Hubs__c,
		Lites_High__c,
		Lites_Wide__c,
		Specialty_Shape__c,
		Spokes__c,
		Product_Configuration__r.Product__c
		from Specialty_Grille_Configuration__c
		where Product_Configuration__r.Product__c in: productids] );
	return configMap;
}

private static string createUniqueKey(Specialty_Grille_Configuration__c spc ){
	string key;

	string prodId = spc.Product_Configuration__r.Product__c;
	string gpattern = spc.Grille_Pattern__c;
	string gstyle = spc.Grille_Style__c;
	string sshape = spc.Specialty_Shape__c;
	string hubs = spc.Hubs__c == null ? '0' : String.valueOf(spc.Hubs__c);
	string spokes  = spc.Spokes__c == null ? '0' :  String.valueOf(spc.Spokes__c);
	string litesHigh = spc.Lites_High__c == null ? '0' :  String.valueOf(spc.Lites_High__c);
	string litesWide = spc.Lites_Wide__c == null ? '0' :  String.valueOf(spc.Lites_Wide__c);

	key = prodId +'-'+ sshape + gpattern + gstyle + hubs + spokes + litesHigh + litesWide;
	return key;
}

private static string createRequestKey(ProductConfiguration pc){
	string key;

	string gpattern = pc.grillePattern;
	string gstyle = pc.grilleStyle;
	string sshape = pc.specialtyShape;
	string hubs = pc.hubs == null ? '0' : String.valueOf(pc.hubs);
	string spokes  = pc.spokes == null ? '0' :  String.valueOf(pc.spokes);
	string litesHigh = pc.litesHigh == null ? '0' :  String.valueOf(pc.litesHigh);
	string litesWide = pc.litesWide == null ? '0' :  String.valueOf(pc.litesWide);

	key = pc.productId +'-'+ sshape +gpattern + gstyle + hubs + spokes + litesHigh + litesWide;
	return key;
}



}