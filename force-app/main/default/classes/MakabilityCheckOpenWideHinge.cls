/**
 * @description       : 
 * @author            : mark.rothermal@andersencorp.com
 * @group             : 
 * @last modified on  : 02-24-2021
 * @last modified by  : mark.rothermal@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                            Modification
 * 1.0   02-24-2021   mark.rothermal@andersencorp.com   Initial Version
**/
public without sharing class MakabilityCheckOpenWideHinge {    
    
    public static openWideHingeResult  checkOpenWideHinge( MakabilityRestResource.OrderItem request,  Size_Detail_Configuration__c currentConfig, map<string,Product_Field_Control__c> uniKeyToPfcMap ){
        openWideHingeResult result = new openWideHingeResult();
        boolean makabilityPasses = true;
        result.errMessages = new List<string>();
        ProductConfiguration p = request.ProductConfiguration;
        Boolean configRequiresWideHinge = false;        
        // determine if lock values are required
        if(uniKeyToPfcMap.containsKey(p.productId + '-' + 'Hardware Option')){
            Product_Field_Control__c pfc = uniKeyToPfcMap.get(p.productId + '-' + 'Hardware Option');
            if(pfc.required__c){
                configRequiresWideHinge = true;
                
            }
        }
        
        //**********start makability flow*************\\
        
        if(configRequiresWideHinge){
            // Open wide Hinge  Makability specific variables
            if(currentConfig.WOH_Required_Below_Inches__c!=null &&currentConfig.WOH_Allowed_Above_Inches__c!=null) {
                Double convertedWidth = p.widthInches + Constants.fractionConversionMap.get(p.widthFractions);
                
                Double convertedRequiredBelowWidth = currentConfig.WOH_Required_Below_Inches__c+ Constants.fractionConversionMap.get(currentConfig.WOH_Required_Below_Fractions__c);
                Double convertedAllowedAboveWidth  = currentConfig.WOH_Allowed_Above_Inches__c + Constants.fractionConversionMap.get(currentConfig.WOH_Allowed_Above_Fractions__c);
                Double convertedAllowedBelowWidth  = currentConfig.WOH_Allowed_Below_Inches__c + Constants.fractionConversionMap.get(currentConfig.WOH_Allowed_Below_Fractions__c);
                
                if(p.hardwareOption=='wide opening hinge'){
                    if(convertedWidth <= convertedAllowedAboveWidth){ 
                        if(convertedWidth >= convertedAllowedBelowWidth){              
                             makabilityPasses = false;
                             result.errMessages.add('A Wide Opening Hinge is not allowed on a unit of this width ');
                        }
                    }
                }else{
                 if(convertedWidth<convertedRequiredBelowWidth){ 
                        makabilityPasses = false;
                        result.errMessages.add('You Must Have a Wide Opening Hinge on a unit of this width');
                    }
                }              
            }
           
            //************end makability flow***************\\
        }
        if(makabilityPasses){
            result.productMakable = true;
            //    result.errMessages.add('Check Lock Size - passed');
            
        } else{
            result.productMakable = false;
        }
        return result;
    }

    //WOH Height Makability
    public static openWideHingeResult checkOpenWideHingeHeight( MakabilityRestResource.OrderItem request,  Size_Detail_Configuration__c currentConfig,  Wide_Open_Hinge_Height_Configuration__c currentwohConfig){
    
        openWideHingeResult result = new openWideHingeResult();
        boolean makabilityPasses = true;
        result.errMessages = new List<string>();
        ProductConfiguration p = request.ProductConfiguration;
       
         if(currentwohConfig.Starting_Width_Inches__c!=null && currentwohConfig.Ending_Width_Inches__c!=null) {
            
            Double convertedWidth = p.widthInches + Constants.fractionConversionMap.get(p.widthFractions);
            
            Double convertedRequiredStartingWidth = currentwohConfig.Starting_Width_Inches__c+ Constants.fractionConversionMap.get(currentwohConfig.Starting_Width_Fractions__c);
            
            Double convertedRequiredEndingWidth;
            if(String.isEmpty(currentwohConfig.Ending_Width_Fractions__c) || String.isBlank(currentwohConfig.Ending_Width_Fractions__c)){
                 convertedRequiredEndingWidth   = currentwohConfig.Ending_Width_Inches__c;
            }else{
                 convertedRequiredEndingWidth   = currentwohConfig.Ending_Width_Inches__c+ Constants.fractionConversionMap.get(currentwohConfig.Ending_Width_Fractions__c);
            }
           
            //todo - review nested if statements below. should not need to recheck if current width is greater than starting width, or less than ending width
            // this should already have happened when the config record was identified. also. the error messages do not make sense for what is happening.
            // as to hit the height check woh is already required.. MTR
            //if( convertedRequiredStartingWidth <= convertedWidth ){
                
                //if(convertedWidth <= convertedRequiredEndingWidth){
                    //Height Comparision
                    Double convertedHeight = p.heightInches + Constants.fractionConversionMap.get(p.heightFractions);
                    
                    Double convertedRequiredMaxHeight = currentwohConfig.Max_Height_Inches__c+ Constants.fractionConversionMap.get(currentwohConfig.Max_Height_Fractions__c);
                   
                        if(convertedRequiredMaxHeight < convertedHeight){
                                makabilityPasses = false;
                                String message = 'Wide Open Hinge - A window '+ p.widthInches + ' Inches ' + p.widthFractions + ' wide, must be below ' + currentwohConfig.Max_Height_Inches__c 
                                + ' Inches ' +currentwohConfig.Max_Height_Fractions__c + ' height, for a Wide Open Hinge';
                                result.errMessages.add(message); 
                        }
               // }
            //     else{                   
            //         makabilityPasses = false;
            //         result.errMessages.add('You Must Have a Wide Opening Hinge on a unit of this height'); 
            //     }
            // }else{               
            //     makabilityPasses = false;
            //     //result.errMessages.add('You Must Have a Wide Opening Hinge on a unit of this Height'); 
            // }
               
        }
        if(makabilityPasses){
            result.productMakable = true;        
        } else{
            result.productMakable = false;
        }
        return result;
    }


    public class  openWideHingeResult{
        public  Boolean productMakable {get;set;}
        public  List<String>  errMessages {get;set;}
    }
    
}