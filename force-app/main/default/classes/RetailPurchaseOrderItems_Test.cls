@Istest
private class RetailPurchaseOrderItems_Test {
   static  Id orderItemid;
     private static Id createMockData(){
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
       /*RMS_Settings__c customSetting = new RMS_Settings__c(
            Value__c = '1234567890', 
            Name='Data Loading Profile ID'
        );
        insert customSetting;   
       */  
        
        Id pbId = Test.getStandardPricebookId();
        
        Account a = new Account(
            Name = 'Test Dwelling',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dwelling').getRecordTypeId()      
        );
        insert a;
        
        Product2 p = new Product2(
            Name = 'Test x',
            IsActive = true        
        );
        insert p;
        
        PriceBookEntry pbe = new PriceBookEntry(
            Pricebook2Id = pbId,
            Product2Id = p.Id,
            UnitPrice = 1.00,
            IsActive = true     
        );
        insert pbe;
        
        Order o = new Order(
            Pricebook2Id = pbId,
            AccountId = a.Id,
            Apex_Context__c = true,
            EffectiveDate = System.today(),
            Status = 'New'        
        ); 
        insert o;
        
        OrderItem oi = new OrderItem(
            OrderId = o.Id,
            Quantity = 1,
            UnitPrice = 0,
            PriceBookEntryId = pbe.Id
        ); 
        insert oi;
         orderItemId=oi.id;
        
        return o.Id;        
    }
    
    @istest
    public static void getRelatedorderItem()
    {
           Id oId = createMockData();
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;
        
        Test.startTest();
        Retail_Purchase_Order__c rpo = new Retail_Purchase_Order__c(
            Order__c = oId ,Store_Location__c=store.id ,Store_Number__c='1234'        
        );
        insert rpo;
        RetailPurchaseOrderItemsController.getRelatedOrderProducts(rpo.id);
        RetailPurchaseOrderItemsController.getStatus(rpo.id);
        
        Test.stopTest();
          
    }
    @istest
    public static void cancelRelatedorderItem()
    {
           Id oId = createMockData();
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;
        
        Test.startTest();
        Retail_Purchase_Order__c rpo = new Retail_Purchase_Order__c(
            Order__c = oId ,Store_Location__c=store.id ,Store_Number__c='1234'        
        );
        insert rpo;
        RetailPurchaseOrderItemsController.cancelRelatedOrderItem(orderItemId);
        
        Test.stopTest();
          
    }
    
    @istest
    public static void cancelRelatedCostItem()
    {
       TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Account store = new Account(
            Type = 'South',
            Name = 'Renewal by Andersen',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;
          Product2 p = new Product2(
            Name = 'Test x',
            IsActive = true 
            
     
        );
        p.Vendor__c=store.id;
        p.Inventoried_Item__c=true;
        insert p;
        
        Retail_Purchase_Order__c po = new Retail_Purchase_Order__c();
        po.Invoice_Number__c='test';
        po.Store_Location__c=store.id;
        po.Store_Number__c='1234' ;
        
        Id devRecordTypeId = Schema.SObjectType.Retail_Purchase_Order__c.getRecordTypeInfosByName().get('Cost Purchase Order').getRecordTypeId();
        po.RecordTypeId=devRecordTypeId;
        insert po;
        
       Retail_Purchase_Order_Items__c rpoi = new   Retail_Purchase_Order_Items__c();
        rpoi.Retail_Purchase_Order__c=po.id;
        rpoi.Product__c=p.id;
        insert rpoi;
        RetailPurchaseOrderItemsController.getRelatedLineItems(po.id);
        RetailPurchaseOrderItemsController.cancelRelatedCostItem(rpoi.id);
        
        
    }

}