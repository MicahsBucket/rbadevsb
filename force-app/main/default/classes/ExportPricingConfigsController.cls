/**
* @author Jason Flippen
* @description Provides database functionality for the ExportPricingConfigurations Aura Component.
*/ 
public with sharing class ExportPricingConfigsController {

    /**
    * @author Jason Flippen
    * @description Retrieves List of Pricing Configuration and cloned Pricing Configuration records.
    * @param Id of the related Wholesale Pricebook record.
    * @returns Wrapper Class with Lists of Pricing Configuration and cloned Pricing Configuration records.
    */ 
    @AuraEnabled
    public static PricingConfigurationWrapper getPricingConfigurations(Id wholesalePricebookId) {
        
        PricingConfigurationWrapper returnWrapper = new PricingConfigurationWrapper();

        Map<String,String> fieldMap = new Map<String,String>();
        List<String> fieldAPIList = new List<String>();
        for (Pricing_Configuration_Export_Setting__mdt customMDT : [SELECT MasterLabel,
                                                                           Field_API__c,
                                                                           Sort_Order__c
                                                                    FROM   Pricing_Configuration_Export_Setting__mdt
                                                                    ORDER BY Sort_Order__c]) {
            // Add the record twice. Once for the original value
            // and a second time for the cloned value.
            fieldMap.put('Original ' + customMDT.Field_API__c,'Original ' + customMDT.MasterLabel);
            fieldMap.put(customMDT.Field_API__c,customMDT.MasterLabel);

            fieldAPIList.add(customMDT.Field_API__c);
        }

        returnWrapper.fieldMap = fieldMap;

        // Build the query string and include the List of Pricing Configuration fields.        
        String queryString = 'SELECT Id,Cloned_Pricing_Configuration__c,' + String.join(fieldAPIList, ',') +
                            ' FROM Pricing_Configuration__c' +
                            ' WHERE Wholesale_Pricebook__c = :wholesalePricebookId';
        
        List<Pricing_Configuration__c> pricingConfigurationList = Database.query(queryString);

        if (!pricingConfigurationList.isEmpty()) {
            returnWrapper.pricingConfigurationList = pricingConfigurationList;
            List<Pricing_Configuration__c> clonedPricingConfigurationList = getClonedPricingConfigurations(wholesalePricebookId);
            if (!clonedPricingConfigurationList.isEmpty()) {
                returnWrapper.clonedPricingConfigurationList = clonedPricingConfigurationList;
            }
        }

        return returnWrapper;

    } // End Method: getPricingConfigurations()

    /**
    * @author Jason Flippen
    * @description Retrieves List of Pricing Configurations from the cloned record.
    * @param Id of the Wholesale Pricebook created from the clone.
    * @returns List of Pricing Configurations from the cloned record.
    */ 
    private static List<Pricing_Configuration__c> getClonedPricingConfigurations(Id wholesalePricebookId) {

        List<Pricing_Configuration__c> returnList = new List<Pricing_Configuration__c>();

        List<Wholesale_Pricebook__c> clonedWholesalePricebookList = [SELECT Cloned_Wholesale_Pricebook__c
                                                                     FROM   Wholesale_Pricebook__c
                                                                     WHERE  Id = :wholesalePricebookId];
        
        Id clonedWholesalePricebookId = clonedWholesalePricebookList[0].Cloned_Wholesale_Pricebook__c;

        if (clonedWholesalePricebookId != null) {
            
            List<String> fieldAPIList = new List<String>();
            for (Pricing_Configuration_Export_Setting__mdt customMDT : [SELECT Field_API__c
                                                                        FROM   Pricing_Configuration_Export_Setting__mdt
                                                                        ORDER BY Sort_Order__c]) {
                fieldAPIList.add(customMDT.Field_API__c);
            }

            // Build the query string and include the List of Pricing Configuration fields.        
            String queryString = 'SELECT Id,Cloned_Pricing_Configuration__c,' + String.join(fieldAPIList, ',') +
                                ' FROM Pricing_Configuration__c' +
                                ' WHERE Wholesale_Pricebook__c = :clonedWholesalePricebookId';
            
            returnList = Database.query(queryString);

        }

        return returnList;

    } // End Method: getPricingConfigurations()


    /**
    * @author Jason Flippen
    * @description Class to combine (wrap) Pricing Configuration and cloned Pricing Configuration records.
    */
    @TestVisible
    public class PricingConfigurationWrapper {

        @AuraEnabled
        public Map<String,String> fieldMap {get;set;}
        @AuraEnabled
        public List<Pricing_Configuration__c> pricingConfigurationList {get;set;}
        @AuraEnabled
        public List<Pricing_Configuration__c> clonedPricingConfigurationList {get;set;}

        public PricingConfigurationWrapper() {
            this.fieldMap = new Map<String,String>();
            this.pricingConfigurationList = new List<Pricing_Configuration__c>();
            this.clonedPricingConfigurationList = new List<Pricing_Configuration__c>();
        }

    }

}