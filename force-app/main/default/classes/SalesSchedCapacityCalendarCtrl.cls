/**
 * @File Name          : SalesSchedCapacityCalendarCtrl.cls
 * @Description        :
 * @Author             : James Loghry (Demand Chain)
 * @Group              :
 * @Last Modified By   : Connor.Davis@andersencorp.com
 * @Last Modified On   : 03-25-2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/8/2019, 12:11:28 PM   James Loghry (Demand Chain)     Initial Version
**/
public with sharing class SalesSchedCapacityCalendarCtrl {

    @AuraEnabled(cacheable=true)
    public static MonthWrapper getMonth(String day, String store){
        try{
            MonthWrapper month = new MonthWrapper();
            List<Date> dateRange = getDateRange(day);
            StoresWrapper storeWrapper = getStores(dateRange, store);

            month.title = 'Sales Capacity Calendar';
            month.weeks = getWeeks(dateRange, storeWrapper);
            month.storeOptions = storeWrapper.optionWrapper.options;
            month.selectedStoreId = storeWrapper.optionWrapper.selectedOption;
            month.monthStart = dateRange.get(0);
            return month;
        }catch(Exception ex){
            System.debug('Exception message: ' + ex.getMessage());
            System.debug('Exception stack trace: ' + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());
        }
    }

    @AuraEnabled(cacheable=true)
    public static Week getWeek(String day,String store){
        Week week = new Week();
        List<Date> dateRange = getWeekRange(day);

        StoresWrapper storesWrapper = getStores(dateRange, store);
        week.storeOptions = storesWrapper.optionWrapper.options;

        for(Date d = dateRange.get(0); d <= dateRange.get(1); d = d.addDays(1)){
            DayWrapper dw = new DayWrapper(d, storesWrapper);
            Datetime dt = (Datetime)d;
            dw.label = dt.formatGmt('EEE, MMM d');
            week.days.add(dw);
        }
        return week;
    }

    @AuraEnabled
    public static void addCapacity(Id userId,String dateString,Id slotId,Id storeId){
        if(userId == null || String.isEmpty(dateString) || slotId == null || storeId == null){
            throw new AuraHandledException('Failed to add capacity.  One or more required parameters missing.');
        }

        Date d = Date.valueOf(dateString);
        upsert new Sales_Capacity__c(
            User__c = userId,
            OwnerId = userId,
            Store__c = storeId,
            Status__c = 'Available',
            Date__c = d,
            Slot__c = slotId,
            External_Id__c = userId + '_' + ((Datetime)d).formatGmt('yyyy-MM-dd') + '_' + slotId
        ) External_Id__c;
    }

    //Returns a map of days with a map of that days slots, with the number of appointments per slot.
    private static Map<String,Map<Id,Integer>> getAppointments(Set<Id> stores, List<Date> dateRange){
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);

        Map<String,Map<Id,Integer>> result = new Map<String,Map<Id,Integer>>();

        for(AggregateResult ar :
            [Select
                Appointment_Date__c day,
                Slot__c slot,
                count(Id) appointments
             From
                Sales_Appointment__c
             Where
                Appointment_Date__c >= :startDate
                And Appointment_Date__c <= :endDate
                And Cancelled__c = false
                And (Sales_Order__r.Opportunity__r.Store_Location__r.Sales_Gateway_Store__c in : stores OR Sales_Order__r.Opportunity__r.Store_Location__c in : stores)
             Group By 
                Appointment_Date__c,
                Slot__c]){
            Date day = (Date)ar.get('day');
            String dayFormatted = ((Datetime)day).formatGmt('yyyy-MM-dd');
            Map<Id,Integer> slotMap = result.get(dayFormatted);
            if(slotMap == null){
                slotMap = new Map<Id,Integer>();
                result.put(dayFormatted, slotMap);
            }
            slotMap.put((Id)ar.get('slot'),(Integer)ar.get('appointments'));
        }
        return result;
    }

    //Helper method to get the current calendar month range (could include a few days of previous month and next month)
    private static List<Date> getWeekRange(String weekStartString){
        Date today = String.isEmpty(weekStartString) ? Date.today() : Date.valueOf(weekStartString);
        Date weekStart = today.toStartOfWeek();
        Date weekEnd = weekStart.addDays(6);
        return new List<Date>{weekStart,weekEnd};
    }

    //Helper method to get the current calendar month range (could include a few days of previous month and next month)
    private static List<Date> getDateRange(String monthStartString){
        Date today = String.isEmpty(monthStartString) ? Date.today() : Date.valueOf(monthStartString);
        Date monthStart = today.toStartOfMonth();
        Date calendarBeginning = monthStart.toStartOfWeek();
        Date monthEnd = monthStart.addMonths(1).addDays(-1);
        Date calendarEnd = monthEnd.toStartOfWeek().addDays(6);
        return new List<Date>{calendarBeginning,calendarEnd};
    }

    private static List<Week> getWeeks(List<Date> dateRange, StoresWrapper sw){
        List<Week> weeks = new List<Week>();
        Date nextStartOfWeek = dateRange.get(0);
        Week currentWeek = null;

        for(Date d = dateRange.get(0); d <= dateRange.get(1); d = d.addDays(1)){
            if(d == nextStartOfWeek){
                currentWeek = new Week();
                weeks.add(currentWeek);
                nextStartOfWeek = d.addDays(7);
            }

            DayWrapper dw = new DayWrapper(d, sw);

            //display edit button on the first day of the week (Sunday)
            if(currentWeek.days.isEmpty()){
                dw.displayEditWeek = true;
            }

            currentWeek.days.add(dw);
        }

        return weeks;
    }

    private static StoresWrapper getStores(List<Date> dateRange, String store){
        StoresWrapper result = new StoresWrapper();
        User currentUser = SalesSchedUtils.getSalesRep(null, dateRange[0], dateRange[1]);

        result.optionWrapper = SalesSchedUtils.getStoreOptions(currentUser, true);
        result.storeIds = new Set<Id>();
        Set<String> storeNames = new Set<String>();
        for(SalesSchedUtils.Option option : result.optionWrapper.options){
            if(store == 'All' || store == option.value){
                if('All' != option.label){
                    result.storeIds.add(option.value);
                    storeNames.add(option.label);
                }
            }
        }

        Id firstStoreId = (new List<Id>(result.storeIds)).get(0);
        Account storeAccount = [Select Id,Name,Active_Store_Configuration__r.Slot_Group__c From Account Where Id = :firstStoreId];
        result.slots = [Select Id,Day__c,Name From Slot__c Where Slot_Group__c = :storeAccount.Active_Store_Configuration__r.Slot_Group__c Order By Day__c Asc, Order__c Asc];
        result.members = SalesSchedUtils.getMyTeamWithCapacities(dateRange,storeNames).members;
        result.appointments = getAppointments(result.storeIds, dateRange);
        return result;
    }

    public class MonthWrapper{
        @AuraEnabled public String title {get; set;}
        @AuraEnabled public List<Week> weeks {get; set;}
        @AuraEnabled public List<SalesSchedUtils.Option> storeOptions {get; set;}
        @AuraEnabled public String selectedStoreId {get; set;}
        @AuraEnabled public Date monthStart {get; set;}

        public MonthWrapper(){
            this.storeOptions = new List<SalesSchedUtils.Option>();
            this.weeks = new List<Week>();
        }
    }

    public class Week{
        @AuraEnabled public List<DayWrapper> days {get; set;}
        @AuraEnabled public List<SalesSchedUtils.Option> storeOptions {get; set;}
        public Week(){
            this.days = new List<DayWrapper>();
            this.storeOptions = new List<SalesSchedUtils.Option>();
        }
    }

    public class AppointmentsToCapacityWrapper{

        @AuraEnabled public Integer appointmentCount {get; set;}
        @AuraEnabled public Integer capacityCount {get; set;}
        @AuraEnabled public String slot {get; set;}

        public AppointmentsToCapacityWrapper(Integer appointments, Integer capacity, String slot){
            appointmentCount = appointments;
            capacityCount = capacity;
            this.slot = slot;
        }
    }

    public class DayWrapper{
        @AuraEnabled public Date day {get; set;}
        @AuraEnabled public List<AppointmentsToCapacityWrapper> appointmentsAndCapacities {get; set;}
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public List<SlotWrapper> slots {get; set;}
        @AuraEnabled public boolean displayEditWeek {get; set;}
        @AuraEnabled public String formatted {get; set;}
        @AuraEnabled public boolean isPastDated{get;set;}

        public DayWrapper(Date day, StoresWrapper storesWrapper){
            this.day = day;
            this.appointmentsAndCapacities = new List<AppointmentsToCapacityWrapper>();
            this.displayEditWeek = false;

            Datetime dt = (Datetime)day;
            this.label = (day.day() == 1) ? dt.formatGmt('MMM d') : dt.formatGmt('d');
            this.formatted = dt.formatGmt('yyyy-MM-dd');
			this.isPastDated=day<System.today();
            Map<Id,Integer> slotMap = storesWrapper.appointments.get(this.formatted);
            this.slots = new List<SlotWrapper>();

            if(UtilityMethods.isSandboxOrg()){
                this.isPastDated = false;
            }

            if(storesWrapper.slots != null){
                for(Slot__c slot : storesWrapper.slots){
                    if(slot.Day__c == dt.formatGmt('EEEE')){
                        SlotWrapper slotWrapper = new SlotWrapper(storesWrapper, slot, slotMap, day);
                        this.appointmentsAndCapacities.add(new AppointmentsToCapacityWrapper(slotWrapper.appointments, slotWrapper.capacity, slotWrapper.slot));
                        this.slots.add(slotWrapper);
                    }
                }
            }
        }
    }

    public class SlotWrapper{
        @AuraEnabled public String slot {get; set;}
        @AuraEnabled public List<User> availableUsers {get; set;}
        @AuraEnabled public List<User> unavailableUsers {get; set;}
        @AuraEnabled public List<User> assignedUsers {get; set;}
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public Id slotId {get; set;}
        @AuraEnabled public Integer appointments;
        @AuraEnabled public Integer capacity;

        public SlotWrapper(StoresWrapper sw, Slot__c slot, Map<Id,Integer> slotMap, Date day){
            this.slot = slot.Name;
            this.slotId = slot.Id;
            this.unavailableUsers = new List<User>();
            this.availableUsers = new List<User>();
            this.assignedUsers = new List<User>();
            this.capacity = 0;

            Integer appointmentCount = (slotMap == null ? 0 : slotMap.get(slot.Id));
            this.appointments = (appointmentCount == null ? 0 : appointmentCount);

            Set<String> processed = new Set<String>();
            for(SalesSchedUtils.TeamMemberWrapper tmw : sw.members){
                boolean foundUser = false;
                for(SalesSchedUtils.CapacityWrapper cw : tmw.capacityWrappers){
                    if(cw.capacity.Status__c != 'Unavailable' && cw.capacity.Slot__c == slot.Id && cw.capacity.Date__c == day){
                        String key = tmw.user.Id + '_' + slot.Id + '_' + day.format();
                        if(!processed.contains(key)){
                            if(sw.storeIds.contains(cw.capacity.Store__c)){
                                this.capacity++;
                            }

                            if(!cw.assignable){
                                add(this.assignedUsers, tmw);
                                foundUser = true;
                            }else if(sw.storeIds.contains(cw.capacity.Store__c)){
                                add(this.availableUsers, tmw);
                                foundUser = true;
                            }
                            processed.add(key);
                        }
                    }
                }

                if(!foundUser){
                    add(this.unavailableUsers, tmw);
                }
            }
            this.label = this.slot + ' ' + this.appointments + ' / ' + this.capacity;
        }

        private void add(List<User> users, SalesSchedUtils.TeamMemberWrapper tmw){
            if(!(new Map<Id,User>(users)).containsKey(tmw.user.Id)){
                users.add(tmw.user);
                users.sort();
            }
        }
    }

    public class StoresWrapper{
        public Set<Id> storeIds {get; set;}
        public SalesSchedUtils.OptionWrapper optionWrapper {get; set;}
        public List<Slot__c> slots {get; set;}
        public List<SalesSchedUtils.TeamMemberWrapper> members {get; set;}
        public Map<String,Map<Id,Integer>> appointments {get; set;}
    }
}