/**
 * @File Name          : SalesSchedDateScheduleTest.cls
 * @Description        :
 * @Author             : James Loghry (Demand Chain)
 * @Group              :
 * @Last Modified By   : Connor.Davis@andersencorp.com
 * @Last Modified On   : 02-16-2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/7/2019, 2:21:01 PM   James Loghry (Demand Chain)     Initial Version
**/
@isTest
private class SalesSchedDateScheduleTest {

    static testmethod void testExecute(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
        Sales_Appointment__c sa = [Select Id,Appointment_Date_Time__c From Sales_Appointment__c];
        sa.Appointment_Date_Time__c = sa.Appointment_Date_Time__c.addDays(-7);
		update sa;
        
        sstu.salesOrder.Result__c = 'Sale';
        sstu.salesOrder.Time_Arrived__c = Datetime.now().addDays(-7).time();
        sstu.salesOrder.Time_Left__c = Datetime.now().addDays(-7).time();

        update sstu.salesOrder;

		sstu.opportunity.Same_Rep_Resit__c = true;
        sstu.opportunity.Repeat_Sale__c = true;
        sstu.opportunity.Follow_Up__c = true;
        sstu.opportunity.Cancel_Save__c = true;
        update sstu.opportunity;
        
        Date d = Date.today().addDays(-7);
        String todayStr = ((Datetime)d).formatGmt('yyyy-MM-dd');
        Sales_Capacity__c sc =
            new Sales_Capacity__c(
                User__c=sstu.capacities.get(0).User__c,
                OwnerId=sstu.capacities.get(0).User__c,
                Store__c= sstu.store.Id,
                Status__c='Available',
                Date__c=d,
                Slot__c=sstu.capacities.get(0).Slot__c,
                External_Id__c = sstu.capacities.get(0).User__c + '_' + todayStr + '_' + sstu.capacities.get(0).Slot__c
            );
        insert sc;

       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sc.Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
        insert resources;
        
        resources.get(0).Status__c = 'Resulted';
        update resources;

        Test.startTest();

        SalesSchedDateSchedule6PM schedule = new SalesSchedDateSchedule6PM();
        schedule.execute(null);
        List<User> ul = [Select Id From User Where Profile.Name = 'Partner RMS-Sales' And Firstname='TestFirst'];
        SalesSchedCalculateSalesRepMetrics.calculateMetrics(Date.today(), ul);
        Test.stopTest();
    }

    static testmethod void testReexecute(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);

        sstu.salesOrder.Result__c = 'Sale';
        sstu.salesOrder.Time_Arrived__c = Datetime.now().time();
        sstu.salesOrder.Time_Left__c = Datetime.now().time();
        update sstu.salesOrder;

		sstu.opportunity.Same_Rep_Resit__c = true;
        sstu.opportunity.Repeat_Sale__c = true;
        sstu.opportunity.Follow_Up__c = true;
        sstu.opportunity.Cancel_Save__c = true;
        update sstu.opportunity;

        List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual'
            )
        );
        insert resources;
        
        resources.get(0).Status__c = 'Resulted';
        update resources;

        insert new Sales_Rep_Report__c(User__c=sstu.reps.get(0).Id);

        Test.startTest();
        SalesSchedDateSchedule6PM schedule = new SalesSchedDateSchedule6PM();
        schedule.execute(null);

        Test.stopTest();

        System.assertEquals(1,[Select Id From Store_Configuration__c Where Current_Sales_Date__c = TOMORROW].size());
        //System.assertEquals(1,[Select Id From Sales_Rep_Report__c].size());
    }

    static testmethod void testExecute5PM(){
        Test.startTest();
        SalesSchedDateSchedule5PM schedule = new SalesSchedDateSchedule5PM();
        schedule.execute(null);
        Test.stopTest();
    }

    static testmethod void testExecute7PM(){
        Test.startTest();
        SalesSchedDateSchedule7PM schedule = new SalesSchedDateSchedule7PM();
        schedule.execute(null);
        Test.stopTest();
    }

    static testmethod void testExecute8PM(){
        Test.startTest();
        SalesSchedDateSchedule8PM schedule = new SalesSchedDateSchedule8PM();
        schedule.execute(null);
        Test.stopTest();
    }
    static testmethod void testAdvanceDateOnly() {
		Test.startTest();
		SalesSchedAdvanceCurrSalesDate schedule = new SalesSchedAdvanceCurrSalesDate();
		schedule.execute(null);
		Test.stopTest();
	}
     static testmethod void autoassign2(){
        Test.startTest();
         //testExecute();
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
 /*       
          Sales_Appointment__c sa = [Select Id,Appointment_Date_Time__c From Sales_Appointment__c];
        sa.Appointment_Date_Time__c = sa.Appointment_Date_Time__c.addDays(-7);
		update sa;
        
        sstu.salesOrder.Result__c = 'Sale';
        sstu.salesOrder.Time_Arrived__c = Datetime.now().addDays(-7).time();
        sstu.salesOrder.Time_Left__c = Datetime.now().addDays(-7).time();

        update sstu.salesOrder;

		sstu.opportunity.Same_Rep_Resit__c = true;
        sstu.opportunity.Repeat_Sale__c = true;
        sstu.opportunity.Follow_Up__c = true;
        sstu.opportunity.Cancel_Save__c = true;
        update sstu.opportunity;
        
        Date d = Date.today().addDays(-7);
        String todayStr = ((Datetime)d).formatGmt('yyyy-MM-dd');
        Sales_Capacity__c sc =
            new Sales_Capacity__c(
                User__c=sstu.capacities.get(0).User__c,
                OwnerId=sstu.capacities.get(0).User__c,
                Store__c= sstu.store.Id,
                Status__c='Available',
                Date__c=d,
                Slot__c=sstu.capacities.get(0).Slot__c,
                External_Id__c = sstu.capacities.get(0).User__c + '_' + todayStr + '_' + sstu.capacities.get(0).Slot__c
            );
        insert sc;

       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sc.Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
        insert resources;
*/
       // List<User> ul = [Select Id From User Where Profile.Name = 'Partner RMS-Sales' And Firstname='TestFirst'];
        Time storeCloseTime = Time.newInstance(18,0,0,0);
		Date nextDate = SalesSchedDateSchedule.setNextDate(storeCloseTime);
        SalesSchedAssignmentBatchss dupl = new SalesSchedAssignmentBatchss(nextDate,storeCloseTime);
        database.executebatch(dupl);
        Test.stopTest();
    }
    
    
    
     static testmethod void autoassignss(){
         
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
         String today = ((Datetime)Date.today()).formatGmt('yyyy-MM-dd');
         Date currentDay = SalesSchedUtils.getCurrentDate(today);
         Map<Id,Account> accountMap = new Map<Id,Account>([Select Id From Account Where Active_Store_Configuration__r.Call_Center_Close_Time__c != null]);
         
         List<String> rules = new List<String>{'RankRepsByCloseRatess'};
             for(Id storeId : accountMap.keySet()){
                 SalesSchedAssignmentRulesFactoryss.processAllWorkss(storeId, currentDay, rules,'method');
             }
     }
    
    static testmethod void SalesSchedProcessDataWrappertest(){
       Date day = Date.today();
        Map<Id,Id> aptSlot = new Map<Id,id>();
        Map<Id,List<Sales_Appointment__c>> aptMap = new Map<Id,List<Sales_Appointment__c>>();
        Map<Id,List<SalesSchedProcessDataWrapperss.UserRankS>> slotToUserRankMap = new Map<Id,List<SalesSchedProcessDataWrapperss.UserRankS>>();
         String dayFormatted = ((Datetime)day).formatGmt('EEEE');
        List<String> daysOfWeek = new List<String>{'Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'};
        List<Slot__c> slotsToInsert = new List<Slot__c>();
        Map<String,List<Slot__c>> slotMap = new Map<String,List<Slot__c>>();
        for(String str : daysOfWeek){
            List<Slot__c> slots = new List<Slot__c>();
        	slots.add(new Slot__c(Name='AM',Day__c = str,Start__c=Time.newInstance(8,0,0,0),End__c=Time.newInstance(12,0,0,0),Slot_Group__c='Three Slots'));
            slots.add(new Slot__c(Name='Mid',Day__c = str,Start__c=Time.newInstance(12,0,0,0),End__c=Time.newInstance(14,0,0,0),Slot_Group__c='Three Slots'));
            slots.add(new Slot__c(Name='PM',Day__c = str,Start__c=Time.newInstance(14,0,0,0),End__c=Time.newInstance(17,0,0,0),Slot_Group__c='Three Slots'));
            slots.add(new Slot__c(Name='Early AM',Day__c = str,Start__c=Time.newInstance(8,0,0,0),End__c=Time.newInstance(10,0,0,0),Slot_Group__c='Five Slots'));
            slots.add(new Slot__c(Name='Late AM',Day__c = str,Start__c=Time.newInstance(10,0,0,0),End__c=Time.newInstance(12,0,0,0),Slot_Group__c='Five Slots'));
            slots.add(new Slot__c(Name='Mid',Day__c = str,Start__c=Time.newInstance(12,0,0,0),End__c=Time.newInstance(14,0,0,0),Slot_Group__c='Five Slots'));
            slots.add(new Slot__c(Name='Early PM',Day__c = str,Start__c=Time.newInstance(14,0,0,0),End__c=Time.newInstance(17,0,0,0),Slot_Group__c='Five Slots'));
            slots.add(new Slot__c(Name='Late PM',Day__c = str,Start__c=Time.newInstance(17,0,0,0),End__c=Time.newInstance(20,0,0,0),Slot_Group__c='Five Slots'));
            slotsToInsert.addAll(slots);
            slotMap.put(str,slots);
        }
        insert slotsToInsert;

        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        
        sstu.createAppointments(1);
        
        
        Slot__c slot = slotMap.get(dayFormatted).get(0);
        Sales_Appointment__c sa = [Select Id,Appointment_Date_Time__c, slot__c From Sales_Appointment__c];
        sa.Appointment_Date_Time__c = system.today();
        sa.Slot__c = slot.id;
        update sa;
        Date ds = Date.today();
        String todayStr = ((Datetime)ds).formatGmt('yyyy-MM-dd');
        Sales_Capacity__c sc =
            new Sales_Capacity__c(
                User__c=sstu.capacities.get(0).User__c,
                OwnerId=sstu.capacities.get(0).User__c,
                Store__c= sstu.store.Id,
                Status__c='Available',
                Date__c=ds,
                Slot__c=slot.Id
               // External_Id__c = sstu.capacities.get(0).User__c + '_' + todayStr + '_' + sstu.capacities.get(0).Slot__c
            );
        insert sc;

        for(Sales_Appointment__c sap : [Select Id,Appointment_Date_Time__c, slot__c,Sales_Order__r.Opportunity__r.Account.ShippingAddress From Sales_Appointment__c]){
            slotToUserRankMap.put(sap.slot__c,new List<SalesSchedProcessDataWrapperss.UserRankS>());
           aptSlot.put(sap.id,sap.Slot__c); 
            if(aptMap.containskey(sap.Slot__c)){
                 aptMap.get(sap.slot__c).add(sap);
            }else{
                aptMap.put(sap.slot__c,new list<Sales_Appointment__c>{sap});
            }
        }
        
        list<SalesSchedProcessDataWrapperss.UserRankS> ranks = slotToUserRankMap.get(slot.Id);
        user u =[select id , name,Contact.MailingAddress from user where id =: userinfo.getUserId()];
         SalesSchedProcessDataWrapperss.UserRankS ur = new SalesSchedProcessDataWrapperss.UserRankS(u,null);
                    ur.capacityId = sc.Id;
                    ur.slotId = sc.Slot__c;
                    ranks.add(ur);
        
         String today = ((Datetime)Date.today()).formatGmt('yyyy-MM-dd');
         Date currentDay = SalesSchedUtils.getCurrentDate(today);
        list<Account> sct = new list<Account>();
        for(Account ac : [Select Id From Account Where Active_Store_Configuration__r.Call_Center_Close_Time__c != null]){
            sct.add(ac);
        }
        
        SalesSchedProcessDataWrapperss pdw = new SalesSchedProcessDataWrapperss(sct[0].id,currentDay);
        pdw.appointmentsToSlotsMap = aptSlot;
        pdw.slotToAppointmentsMap = aptMap;
        pdw.slotToUserRankMap = slotToUserRankMap;
        pdw.processRuless(pdw);
        
    }
    
    //To increase the code coverage of SalesSchedProcessDataWrapper ProcessAll Method
    //By sundeep
    static testmethod void processRuleTestmethod(){
         Date day = Date.today();
        Map<Id,Id> aptSlot = new Map<Id,id>();
        Map<Id,List<Sales_Appointment__c>> aptMap = new Map<Id,List<Sales_Appointment__c>>();
        Map<Id,List<SalesSchedProcessDataWrapper.UserRank>> slotToUserRankMap = new Map<Id,List<SalesSchedProcessDataWrapper.UserRank>>();
         String dayFormatted = ((Datetime)day).formatGmt('EEEE');
        List<String> daysOfWeek = new List<String>{'Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'};
        List<Slot__c> slotsToInsert = new List<Slot__c>();
        Map<String,List<Slot__c>> slotMap = new Map<String,List<Slot__c>>();
        for(String str : daysOfWeek){
            List<Slot__c> slots = new List<Slot__c>();
        	slots.add(new Slot__c(Name='AM',Day__c = str,Start__c=Time.newInstance(8,0,0,0),End__c=Time.newInstance(12,0,0,0),Slot_Group__c='Three Slots'));
            slots.add(new Slot__c(Name='Mid',Day__c = str,Start__c=Time.newInstance(12,0,0,0),End__c=Time.newInstance(14,0,0,0),Slot_Group__c='Three Slots'));
            slots.add(new Slot__c(Name='PM',Day__c = str,Start__c=Time.newInstance(14,0,0,0),End__c=Time.newInstance(17,0,0,0),Slot_Group__c='Three Slots'));
            slots.add(new Slot__c(Name='Early AM',Day__c = str,Start__c=Time.newInstance(8,0,0,0),End__c=Time.newInstance(10,0,0,0),Slot_Group__c='Five Slots'));
            slots.add(new Slot__c(Name='Late AM',Day__c = str,Start__c=Time.newInstance(10,0,0,0),End__c=Time.newInstance(12,0,0,0),Slot_Group__c='Five Slots'));
            slots.add(new Slot__c(Name='Mid',Day__c = str,Start__c=Time.newInstance(12,0,0,0),End__c=Time.newInstance(14,0,0,0),Slot_Group__c='Five Slots'));
            slots.add(new Slot__c(Name='Early PM',Day__c = str,Start__c=Time.newInstance(14,0,0,0),End__c=Time.newInstance(17,0,0,0),Slot_Group__c='Five Slots'));
            slots.add(new Slot__c(Name='Late PM',Day__c = str,Start__c=Time.newInstance(17,0,0,0),End__c=Time.newInstance(20,0,0,0),Slot_Group__c='Five Slots'));
            slotsToInsert.addAll(slots);
            slotMap.put(str,slots);
        }
        insert slotsToInsert;

        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        
        sstu.createAppointments(1);
        
        
        Slot__c slot = slotMap.get(dayFormatted).get(0);
        Sales_Appointment__c sa = [Select Id,Appointment_Date_Time__c, slot__c From Sales_Appointment__c];
        sa.Appointment_Date_Time__c = system.today();
        sa.Slot__c = slot.id;
        update sa;
        Date ds = Date.today();
        String todayStr = ((Datetime)ds).formatGmt('yyyy-MM-dd');
        Sales_Capacity__c sc =
            new Sales_Capacity__c(
                User__c=sstu.capacities.get(0).User__c,
                OwnerId=sstu.capacities.get(0).User__c,
                Store__c= sstu.store.Id,
                Status__c='Available',
                Date__c=ds,
                Slot__c=slot.Id
               // External_Id__c = sstu.capacities.get(0).User__c + '_' + todayStr + '_' + sstu.capacities.get(0).Slot__c
            );
        insert sc;

        for(Sales_Appointment__c sap : [Select Id,Appointment_Date_Time__c, slot__c,Sales_Order__r.Opportunity__r.Account.ShippingAddress From Sales_Appointment__c]){
            slotToUserRankMap.put(sap.slot__c,new List<SalesSchedProcessDataWrapper.UserRank>());
           aptSlot.put(sap.id,sap.Slot__c); 
            if(aptMap.containskey(sap.Slot__c)){
                 aptMap.get(sap.slot__c).add(sap);
            }else{
                aptMap.put(sap.slot__c,new list<Sales_Appointment__c>{sap});
            }
        }
        
        list<SalesSchedProcessDataWrapper.UserRank> ranks = slotToUserRankMap.get(slot.Id);
        user u =[select id , name,Contact.MailingAddress from user where id =: userinfo.getUserId()];
         SalesSchedProcessDataWrapper.UserRank ur = new SalesSchedProcessDataWrapper.UserRank(u,null);
                    ur.capacityId = sc.Id;
                    //ur.slotId = sc.Slot__c;
                    ranks.add(ur);
        
         String today = ((Datetime)Date.today()).formatGmt('yyyy-MM-dd');
         Date currentDay = SalesSchedUtils.getCurrentDate(today);
        list<Account> sct = new list<Account>();
        for(Account ac : [Select Id From Account Where Active_Store_Configuration__r.Call_Center_Close_Time__c != null]){
            sct.add(ac);
        }
        
        SalesSchedProcessDataWrapper pdw = new SalesSchedProcessDataWrapper(sct[0].id,currentDay);
        pdw.slots = new list<slot__c>{slot};
        pdw.slotToAppointmentsMap = aptMap;
        pdw.slotToUserRankMap = slotToUserRankMap;
        pdw.processRule(pdw);
        
        
    }
    
}