@isTest(SeeAllData=false)
global class CanvassUnitRecordCountBatchTest {

    public static testMethod void test1()
    {   
        list<Canvass_Unit_Batch_Fields__c> canvassUnitBatchFieldsList = new list<Canvass_Unit_Batch_Fields__c>();
        Canvass_Unit_Batch_Fields__c canvassUnitBatchField = new Canvass_Unit_Batch_Fields__c();
        canvassUnitBatchField.Name = 'test';
        canvassUnitBatchField.Apex_Batch_Job_Limit__c = 50;
        canvassUnitBatchFieldsList.add(canvassUnitBatchField);
        insert canvassUnitBatchFieldsList;
    
        CNVSS_Canvass_Market__c canvassMarket = new CNVSS_Canvass_Market__c();
        canvassMarket.Name = 'Test';
        canvassMarket.CNVSS_Canvass_Market_Manager__c = userInfo.getUserId();
        canvassMarket.CNVSS_ISC_Phone_Number__c = '5555555555';
        insert canvassMarket;
        
        Custom_Data_Layer_Batch_Queue__c canvassDataLayerQueue = new Custom_Data_Layer_Batch_Queue__c();
        canvassDataLayerQueue.Status__c = 'Waiting For Record Count';
        canvassDataLayerQueue.Field_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids2__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids3__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids4__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids5__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids6__c = '123, 456, 789';
        canvassDataLayerQueue.ZipCode__c = '12345';
        canvassDataLayerQueue.Market_Id__c = canvassMarket.id;
        insert canvassDataLayerQueue;
        
        Test.startTest();
        database.executebatch(new CanvassUnitQueueableBatch(),1);
        
        CanvassUnitRecordCountBatch rsb = new CanvassUnitRecordCountBatch();
        String sch = '0 0 * * * ?';
        system.schedule('Canvass Unit Record Count Batch 9999', sch, rsb);
    
        Test.stopTest();
    }
}