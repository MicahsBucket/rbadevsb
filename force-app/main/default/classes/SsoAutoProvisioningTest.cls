@isTest
public class SsoAutoProvisioningTest {
static String saml = 
		'<?xml version="1.0" encoding="UTF-8"?>' +
		'<samlp:Response xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" ID="_fa73d787-cdcb-47f8-a31f-d4490766027c" Version="2.0" IssueInstant="2018-02-05T17:19:58.268Z" Destination="https://andersen--lmodev1.cs25.my.salesforce.com?so=00D1b000000D4DG" Consent="urn:oasis:names:tc:SAML:2.0:consent:unspecified">' +
		'<Issuer xmlns="urn:oasis:names:tc:SAML:2.0:assertion">http://stsdev.awdev.com/adfs/services/trust</Issuer><samlp:Status>' +
		'<samlp:StatusCode Value="urn:oasis:names:tc:SAML:2.0:status:Success" /></samlp:Status>' +
		'<Assertion xmlns="urn:oasis:names:tc:SAML:2.0:assertion" ID="_59cc5dba-fd31-4ba2-8664-6b594aae93d5" IssueInstant="2018-02-05T17:19:58.268Z" Version="2.0">' +
		'<Issuer>http://stsdev.awdev.com/adfs/services/trust</Issuer><ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">' +
		'<ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" />' +
		'<ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256" />' +
		'<ds:Reference URI="#_59cc5dba-fd31-4ba2-8664-6b594aae93d5"><ds:Transforms>' +
		'<ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />' +
		'<ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" /></ds:Transforms>' +
		'<ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256" />' +
		'<ds:DigestValue>QIcJtwaNv/GX+BBTOTbYuwLNlgqUaQdsEXpcfazokGI=</ds:DigestValue></ds:Reference></ds:SignedInfo>' +
		'<ds:SignatureValue>DNCH7Caz2ASVXrFObIxUEqekRLMPKhTjn3oIwKHRWFCKKhrEZrxyl2QnznN6SHJClhm932O3F0NDigWiVNw/rKL5nBwsmETY6SFVs9Pe+2b/L850IJB1eoigMVu2eWVrvhHlWPlkQpeYghpHoHyGjGzoovUTA07V3F4eig7gdX/8pP/QgUxSTjgao9jyNEadGIOeamtVpEgZXS5GUZT4cYf+7LVEFG4tXV3uL1FkktQyhlEUApW+8TOwIPKZuyY1aWgkN32gCAb7JrXaXCXMW4HPouhn+4RKSU12SNFWB35gWb6CMqjmT0Wqkt7zISQzYiYa55pKHs2H1vfbl3SoNw==</ds:SignatureValue>' +
		'<KeyInfo xmlns="http://www.w3.org/2000/09/xmldsig#"><ds:X509Data>' +
		'<ds:X509Certificate>MIIC3DCCAcSgAwIBAgIQKZvN87N/WJ1P4TBGesxKhTANBgkqhkiG9w0BAQsFADAqMSgwJgYDVQQDEx9BREZTIFNpZ25pbmcgLSBzdHNkZXYuYXdkZXYuY29tMB4XDTE3MTIwODIxNDA1NloXDTE4MTIwODIxNDA1NlowKjEoMCYGA1UEAxMfQURGUyBTaWduaW5nIC0gc3RzZGV2LmF3ZGV2LmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL7jTGIhCNCV6hINZGangIgSovI0MCpgyxu70cvCNNUFiIg6kP7x6aFIwqPHeyHayOF87jMwRNmJ1H+QUO9llFC+X3i2ZpZGNochwrQTKle90rmOyfbZQVewyCjvlEAxtZ/8EE1XOpqUSrkyoYNdpS9AAw6fXH27EomVxD5cfM6jH7M9un3GPcdbOGWTYlOKrnG7p5cnrs8bCtTQlZZK1gomjU+R4yUb8/1fDGZHvDq0oUxCKFbr30MDi8NspWwr1ddsVHz6S7M1q7BiKvY8MdNXrB7knuW7hic6zZA8t+mefdZsCagBV6hhqSJWHqkBJpDACU+ZpdBYRNQmoqF4lRsCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAKMur+e5G2YWm8TsLA6/6AnoHkSBfm+ybFc8LDu7zolBazYupKYjHTMBijL9XWsmKdFzWIkjllpj39k/Utnc5OddFQd1bXaba8RZr1CiTuZYMbmsBtNQSeyx+4qJ2Zpk2357B16bXUO63WhOr7KEv6ZDzZXjqOIGWludM6kwbyPfXV1t88uPO3B54HF5TxhhWkK22Y4Zt0Lg9qxY5J2GO/TEICGcwfdLEprEm+i00hbhDRRCY/y0eld3M+FyJ5KqnLM/lX85pn9cu7g03PlrIdqFCBptSa2rYggmBU9nT+PbTktibTYRYrCxTIXavqs1QLS298ttM0BM6UZrAp2WlaA==</ds:X509Certificate>' +
		'</ds:X509Data></KeyInfo></ds:Signature><Subject><NameID>a50005@awdev.com</NameID>' +
		'<SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">' +
		'<SubjectConfirmationData NotOnOrAfter="2018-02-05T17:24:58.268Z" Recipient="https://andersen--lmodev1.cs25.my.salesforce.com?so=00D1b000000D4DG" />' +
		'</SubjectConfirmation></Subject><Conditions NotBefore="2018-02-05T17:19:58.264Z" NotOnOrAfter="2018-02-05T18:19:58.264Z">' +
		'<AudienceRestriction><Audience>https://lmodev1-andersen.cs25.force.com/partners</Audience></AudienceRestriction></Conditions>' +
		'<AttributeStatement>' +
		'<Attribute Name="User.email"><AttributeValue>a50005@awdev.com</AttributeValue></Attribute>' +
		'<Attribute Name="User.FederationID"><AttributeValue>a50005@awdev.com</AttributeValue></Attribute>' +
		'<Attribute Name="User.FirstName"><AttributeValue>TEST</AttributeValue></Attribute>' +
		'<Attribute Name="User.Lastname"><AttributeValue>ssoCode</AttributeValue></Attribute>' +
		'<Attribute Name="User.RenewUIsAllowed"><AttributeValue>true</AttributeValue></Attribute>' +
		'<Attribute Name="User.DefaultStore"><AttributeValue>800</AttributeValue></Attribute>' +
		'<Attribute Name="User.AllStores"><AttributeValue>800,535</AttributeValue></Attribute>' +
		//'<Attribute Name="Contact.PersonId"><AttributeValue>123123</AttributeValue></Attribute>' +
		'<Attribute Name="User.Department"><AttributeValue>12345</AttributeValue></Attribute>' +
		//'<Attribute Name="User.Email"><AttributeValue>portaldevint@andersencorp.com</AttributeValue></Attribute><Attribute Name="Contact.Email">' +
		//'<AttributeValue>portaldevint@andersencorp.com</AttributeValue></Attribute>' +
        //'<Attribute Name="User.Permissions"><AttributeValue>Domain Users</AttributeValue><AttributeValue>Users - AWX COE Internal Users</AttributeValue>' +
        //'<AttributeValue>grp_int_basic_portal</AttributeValue><AttributeValue>record_for_unit_test_CPLM</AttributeValue><AttributeValue>grp_int_view_initial_quality</AttributeValue>' +
        //'<AttributeValue>grp_int_create_order</AttributeValue><AttributeValue>record_for_unit_test_CPLM_Mgr</AttributeValue></Attribute>' +
		'</AttributeStatement>' +
		'<AuthnStatement AuthnInstant="2020-01-06T17:19:58.125Z" SessionIndex="_59cc5dba-fd31-4ba2-8664-6b594aae93d5"><AuthnContext>' +
		'<AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport</AuthnContextClassRef>' +
		'</AuthnContext></AuthnStatement></Assertion></samlp:Response>';

		static testMethod void createUserTest() {
		System.debug('createUserTest method in Sso Test Class');
	    Id samlSSOId = null;
	    Id portalId = null;
		Id communityId = '00e61000001TMQr';
	    String federationId = null;
	    String assertion = EncodingUtil.base64Encode(Blob.valueOf(saml.trim()));
		//Network net = [select Id, Name from Network where Name = 'Partners' limit 1];
		System.debug('create attribute Map, call populateAttribute Map in testUtility');
		Map<String,String> attributeMap = testUtilitySSOcode.populateAttributeMap('ssoLogin');

		// find the account as we will not create an account initially.
		//Account a = [select Id, Name, Enabled_StoreId__c from Account where Enabled_StoreId__c = '800'];
		//Account a = testUtilitySSOcode.createStoreAccount('testsso','800');
		TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
            Account a = utility.createAccount('testsso');
           		a.Enabled_StoreId__c = '800';
            insert a;
		//Account a = testUtility.createDealerAccount('CPuserReg1', true); //was used during a create account situation.
		System.debug('Find account: ' + a.Name);

		// create the 
		System.debug('call createCommunityContact in testUtilitySSOcode from SSO test class');
	    Contact c = testUtilitySSOcode.createCommunityContact('ssoLogin', a, attributeMap.get('User.email'), true);
		System.debug('Created contact: ' + c);
		SSOAutoProvisioning autoProv = new SSOAutoProvisioning();
		// start the test
		Test.startTest();
		User u = autoProv.createUser(samlSSOId, portalId, communityId, federationId, attributeMap, assertion); //did have net.Id
		Test.stopTest();
		System.assert(u.Permissions_List__c.contains('LMS User'));
        System.assert(u.isActive);
	    System.assert(u.ProfileId != null);
    }

 /*  static testMethod void updateUserTest() {
	    Id samlSSOId = null;
	    Id portalId = null;
		Id communityId = '00e61000001TMQr';
        String federationId = null;
	    String assertion = EncodingUtil.base64Encode(Blob.valueOf(saml.trim()));
		//Network net = [select Id, Name from Network where Name = 'Partners' limit 1];
		Map<String,String> attributeMap = testUtilitySSOcode.populateAttributeMap('RenewU1');

		// work around mixed dml error
        Contact c = null;
		Id uid = UserInfo.getUserId();
		UserInfo.getUserName();
		User otherUser = [select Id from User where IsActive = true and Profile.Name = 'Super Administrator' and Id != :uid limit 1];
		System.runAs(otherUser) {
			// lookup the account
		TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
            Account a = utility.createAccount('testsso');
           		a.Enabled_StoreId__c = '800';
            insert a;
            System.debug('account Name: ' + a.Name);
			// create the contact
		    c = testUtilitySSOcode.createCommunityContact('RenewU1', a, attributeMap.get('User.email'), true);
		}

		SSOAutoProvisioning autoProv = new SSOAutoProvisioning();
		// start the test
		Test.startTest();
		// create the user
		User u = autoProv.createUser(samlSSOId, portalId, communityId, federationId, attributeMap, assertion); //did have net.Id
		//u.Permissions_List__c = 'LMS User';
		insert u;
		system.debug('u.id: ' + u.Id + u.Permissions_List__c);
		System.assert(u.Permissions_List__c.contains('LMS User'));
		// update the user
		autoProv.updateUser(u.Id, samlSSOId, communityId, portalId, federationId, attributeMap, assertion); //did have net.Id
		u = [SELECT Id, FirstName, Permissions_List__c, isActive, ProfileId FROM User WHERE Id=:u.Id];

		Test.stopTest();
		System.assert(u.Permissions_List__c.contains('LMS User'));
		System.assert(u.Permissions_List__c.contains('LMS External User'));
		System.assert(u.isActive);
	    System.assert(u.ProfileId != null);
    }

     static testMethod void createUserTestNoPermissionAssignments() {
	    Id samlSSOId = null;
	    Id portalId = null;
		Id communityID = null;
	    String federationId = null;
	    String assertion = EncodingUtil.base64Encode(Blob.valueOf(saml.trim()));
		//Network net = [select Id, Name from Network where Name = 'Partners' limit 1];
		Map<String,String> attributeMap = testUtilitySSOcode.populateAttributeMap('RenewU2');

		Account a = null;
		// lookup the account
		a = [select Id from Account where Enabled_StoreId__c != null LIMIT 1];

		// create the contact
	    Contact c = testUtilitySSOcode.createCommunityContact('RenewU2', a, attributeMap.get('User.email'), true);

		SSOAutoProvisioning autoProv = new SSOAutoProvisioning();
		// start the test
		Test.startTest();
		User u = autoProv.createUser(samlSSOId, communityID, portalId, federationId, attributeMap, assertion); //did have net.Id
		Test.stopTest();
		System.assert(u.Permissions_List__c == null);
		System.assert(!u.isActive);
    }
*/
    static testMethod void createUsermissingFieldsTest() {
	    Id samlSSOId = null;
	    Id portalId = null;
		Id communityId = '00e61000001TMQr';
	    String federationId = null;
	    String assertion = EncodingUtil.base64Encode(Blob.valueOf(saml.trim()));
		//Network net = [select Id, Name from Network where Name = 'Partners' limit 1];
		Map<String,String> attributeMap = testUtilitySSOcode.populateAttributeMap('RenewU3');

		// lookup the account
		TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
            Account a = utility.createAccount('testsso');
           		a.Enabled_StoreId__c = '800';
            insert a;

		// create the contact
	    Contact c = testUtilitySSOcode.createCommunityContact('RenewU3', a, attributeMap.get('User.email'), true);

		attributeMap.remove('User.email');
		attributeMap.remove('User.Username');
		attributeMap.remove('User.LocaleSidKey');
		attributeMap.remove('User.LanguageLocaleKey');
		attributeMap.remove('User.Alias');
		attributeMap.remove('User.TimeZoneSidKey');
		attributeMap.remove('User.EmailEncodingKey');
        attributeMap.remove('User.RenewUIsAllowed');

		SSOAutoProvisioning autoProv = new SSOAutoProvisioning();
		// start the test
		Test.startTest();
		try {
			User u = autoProv.createUser(samlSSOId, communityId,portalId, federationId, attributeMap, assertion); // did have net.Id
			System.assert(false);
		} catch (Exception e) {
			System.debug('##############: ' + e);
			System.debug('##############: ' + e.getStackTraceString());
			System.assert(e.getMessage().contains('You do not have permission to access this site. Contact rSupport for assistance'));

		}
		Test.stopTest();
    }

    //static testMethod void nullIDsTest() {
		//SSOAutoProvisioning autoProv = new SSOAutoProvisioning();
		//// start the test
		//Test.startTest();
		//try {
			//User u = autoProv.createUser(null, null, null, null, null, null);
			//System.assert(false);
		//} catch (Exception e) {
			//System.assert(e.getMessage().contains('Community Id and '));
		//}
		//Test.stopTest();
    //}
}