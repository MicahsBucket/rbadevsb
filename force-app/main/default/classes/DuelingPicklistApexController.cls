/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Signature Service Community
* @date 6/17
* @description Creates lists of installers based on their association to 
* the survey via junction object
**/

public with sharing class DuelingPicklistApexController {
 
    //creates a list of installers that are not currently associated to the survey via the Survey_Worker__c junciton object 
    @AuraEnabled
    public static List<SelectOption> getAllAvailableInstallers(Id recordId)
    {
        List<Id> activeWorkerIds = new List<Id>();
        List<SelectOption> allActiveWorkers = new List<SelectOption>();

        List<Survey_Worker__c> activeWorkerJunctions = [SELECT Worker_Id__c, Survey_Id__c, Role__c FROM Survey_Worker__c WHERE Survey_Worker__c.Survey_Id__c = :recordId AND Role__c != 'Sales Rep' AND Role__c != 'Tech Measurer'];
        for(Survey_Worker__c sw : activeWorkerJunctions){
           activeWorkerIds.add(sw.Worker_Id__c);
        }
        List<Worker__c> workers = [Select Role__c, First_Name__c, Last_Name__c, Id,Name FROM Worker__c 
                                    WHERE Worker__c.Role__c INCLUDES ('Installer') 
                                    AND Worker__c.Role__c EXCLUDES ('Inactive') 
                                    AND Worker__c.Id != :activeWorkerIds];
        for(Worker__c w : workers)
        {
            allActiveWorkers.add(new SelectOption(w.Id, w.Last_Name__c + ', ' + w.First_Name__c));
        }
        return allActiveWorkers;
    }

    //creates a list of installers that are currently associated to the survey via Survey_Worker__c
    @AuraEnabled
    public static List<SelectOption> getAllSelectedInstallers(Id recordId)
    {
        List<Id> activeWorkerIds = new List<Id>();
        List<SelectOption> allSelectedWorkers = new List<SelectOption>();

        List<Survey_Worker__c> activeWorkerJunctions = [SELECT Worker_Id__c, Survey_Id__c 
                                                        FROM Survey_Worker__c 
                                                        WHERE Survey_Worker__c.Survey_Id__c = :recordId 
                                                        AND Role__c = 'Installer'];
        for(Survey_Worker__c sw : activeWorkerJunctions){
            activeWorkerIds.add(sw.Worker_Id__c);
        }
        List<Worker__c> workers = [Select Role__c, First_Name__c, Last_Name__c, Id,Name from Worker__c WHERE Worker__c.Role__c INCLUDES ('Installer') AND Worker__c.Role__c EXCLUDES ('Inactive') AND Worker__c.Id = :activeWorkerIds];
        for(Worker__c w : workers)
        {
            allSelectedWorkers.add(new SelectOption(w.Id, w.Last_Name__c + ', ' + w.First_Name__c));
        }
        return allSelectedWorkers;
    }

    //Creates select option type used to send both the workers name and id to and from the Javascript page
    public class SelectOption{
        public SelectOption (String value, String label)
        {
            this.value = value;
            this.label = label;
            this.disabled = false;
            this.escapeItem = false;
            this.selected = false;
        }
        
        @AuraEnabled
        public String label { get;set; }
        @AuraEnabled
        public String value { get;set; }
        @AuraEnabled
        public Boolean disabled { get;set; }
        @AuraEnabled
        public Boolean escapeItem { get;set; }
        @AuraEnabled
        public Boolean selected{get;set;}
        
    }

}