public class CustomerHeroCtrl {
    @AuraEnabled
    public static String getUserName() {
        String welcomeName;//=userinfo.getFirstName();
        User userRecord=[select id,name,firstname,lastname,ContactId,Contact.Firstname,Contact.Lastname,Contact.Spouse__c,Contact.Spouse__r.FirstName from User where Id=:UserInfo.getUserId()];
        if(userRecord.ContactId==null)
        {
            welcomeName=userRecord.firstname;
        }
        else
        {
            welcomeName=userRecord.Contact.Firstname;
            /*if(userRecord.Contact.Spouse__c!=null)
            {
                welcomeName+=' & '+userRecord.Contact.Spouse__r.FirstName;
            }*/
        }
        System.debug('UserFirstName: ' + welcomeName);
        
        /*
        // get list of order for this user based on order.customerportal user = current logged in user order by created date desc
        List<Order> OrderList = [select id, OpportunityId from Order where CustomerPortalUser__c= :userinfo.getUserId() ];
        
        ID thisOpportunity = null;
        
        if (OrderList!=null && OrderList.size() >0) {
            thisOpportunity = OrderList[0].OpportunityId;
        }
        
        List <ID> contactIDList = new List <ID>();
        List<Contact> contactList = new List <Contact>();
        if (thisOpportunity!=null) {
            List <OpportunityContactRole> OpportunityContactRoleList = [select ContactId from OpportunityContactRole where OpportunityId =:thisOpportunity];
            for (OpportunityContactRole OCR : OpportunityContactRoleList) {
                contactIDList.add(OCR.ContactId);
            }
        }
        if(contactIDList.size() > 0 && contactIDList != null){
            contactList = [select id, FirstName, LastName, Spouse__c, Spouse__r.FirstName from Contact where id in :contactIDList];        
        }
        
        // Loop through the contacts to find out the right contact and then get the spouse name
        Contact thisUserContact = null;
        if(contactList.size()>0 && contactList != null){
            for (Contact c :contactList ) {
                if  (c.FirstName == userinfo.getFirstName()) {
                    thisUserContact = c;
                    break;
                }
            }           
        }
        
        
        if (thisUserContact!= null && thisUserContact.Spouse__c != null && thisUserContact.Spouse__r.FirstName != null)  {
            welcomeName += ' & ' + thisUserContact.Spouse__r.FirstName;
        }
        */
        return welcomeName;
    }
}