@isTest
private class CFW_ContactManagementTest {

	@testSetup static void setupData(){
		//User thisUser = [Select Id from User where Id = :UserInfo.getUserId()];
		TestUtilityMethods util = new TestUtilityMethods();

		RMS_Settings__c customSetting1 = new RMS_Settings__c(
			Value__c = '1234567890', Name='Data Loading Profile ID');
		insert customSetting1;

		Account a = util.createAccount('Acme');
		insert a;

		RMS_Settings__c customSetting2 = new RMS_Settings__c(
			Value__c = a.Id, Name='Unassigned Account Id');
		insert customSetting2;
	}
	
	@isTest static void testCfwContactMgmt() {
		TestUtilityMethods util = new TestUtilityMethods();
		Id customerRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
		String coApplicantName = 'Tester2';
		String coApplicantSSN = '123-12-1234';
		Account a = [Select Id from Account limit 1];
		Contact c = util.createContact(a.Id, 'Tester');
		Contact c2 = util.createContact(a.Id, coApplicantName);
		c2.LASERCA__Social_Security_Number__c = coApplicantSSN;
		insert new List<Contact>{c, c2};
		c.RecordTypeId = customerRecordTypeId;
		c.Co_Applicant__c = c2.Id;
		c.Has_Customer_Authorization_Picklist__c = 'Yes'; 
		c.Pull_Credit_Report__c = CFW_ContactManagement.SOFT_PULL;

		update c;

		c = [Select Id, LASERCA__Social_Security_Number__c, LASERCA__Co_Applicant_Name__c, 
			LASERCA__Co_Applicant_Last_Name__c, LASERCA__Co_Applicant_Social_Security_Number__c
			from Contact where Id = :c.Id];
	/*
		System.assertEquals(CFW_ContactManagement.DEFAULT_SSN, 
			c.LASERCA__Social_Security_Number__c);
		System.assertEquals(coApplicantName+'Test', c.LASERCA__Co_Applicant_Name__c);
		System.assertEquals(coApplicantName+'Contact', c.LASERCA__Co_Applicant_Last_Name__c);
		System.assertEquals(coApplicantSSN, c.LASERCA__Co_Applicant_Social_Security_Number__c);
	*/
	}
	
}