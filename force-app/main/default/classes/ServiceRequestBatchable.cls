global class ServiceRequestBatchable implements Database.Batchable<sObject> {
	
	String query;

	global Database.QueryLocator start(Database.BatchableContext BC) {
		Date cutoffDate = System.today().addDays(-7);
		String warrantySubmitted = 'Warranty Submitted';
		Date rejectedDateToClose = System.today().addDays(-45);
		String warrantyRejected = 'Warranty Rejected';

    query = 'Select Id, Status, Warranty_Date_Accepted__c, Warranty_Rejected_Date__c, Warranty_Processed_Date__c from Order ' +
      		'where (Status = :warrantySubmitted and Warranty_Processed_Date__c <= :cutoffDate) or' +
      		'(Status = :warrantyRejected and Warranty_Rejected_Date__c < :rejectedDateToClose)';

		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Order> scope) {
		List<Id> allScopeOrderIds = new List<Id>();
		Date cutoffDate = System.today().addDays(-7);
		Date rejectedDateToClose = System.today().addDays(-45);

	for(Order o : scope){
	
	if(o.Status == 'Warranty Submitted' && o.Warranty_Processed_Date__c <= cutoffDate){
		
		o.Status = 'Warranty Accepted';
	
		} else if (o.Status == 'Warranty Rejected' && o.Warranty_Rejected_Date__c < rejectedDateToClose){
			
			o.Status =  'Closed';

			}
		}

	Database.update(scope, false);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}