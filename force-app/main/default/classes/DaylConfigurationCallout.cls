/**
 * @description       : 
 * @author            : mark.rothermal@andersencorp.com
 * @group             : 
 * @last modified on  : 05-05-2021
 * @last modified by  : mark.rothermal@andersencorp.com
 * Test Class         : DaylConigurationCalloutTest
 * Ver   Date         Author                            Modification
 * 1.0   03-22-2021   mark.rothermal@andersencorp.com   Initial Version
**/
global with sharing class DaylConfigurationCallout {

    // need to get callout settings
    // need to test callout
    // need to return and display response in ui
    // need to be able to reuse same call again...
    // need to figure out how to store Session id and screenOptionId - maybe platform cache??

    //TODO - need to add sandbox check to get callout settings. and beef up 

    /**
    * @description 
    * @author mark.rothermal@andersencorp.com | 03-29-2021 
    * @param namespace - Should match an available custom metadata callout setting namespace example 'TEST'
    * @param name - Should match an available custom metadata callout setting name - example 'SimpleRuleset'
    * @return LightningResponse //
    **/
    @AuraEnabled(cacheable=false)
    Public static LightningResponse getUrlforConfiguration(String namespace,String name,String headerId, String detailId, Boolean updateExistingProduct, String redirectUrl, String orderId){
        System.debug('hit start configuration');
        System.debug('Related order id: '+ orderId);
        LightningResponse lr = new LightningResponse();
        HttpRequest req = new HttpRequest();
        RequestWrapper reqWrap = new RequestWrapper();
        Infor_Cpq_Api__mdt calloutSettings = getCalloutSettings(namespace,name);
        if(calloutSettings != null ){
            reqWrap = createBody(calloutSettings, headerId, detailId, updateExistingProduct, redirectUrl);
        }
        try {
            req.setEndpoint(calloutSettings.Base_Url__c + 'api/v3/ProductConfigurator.svc/json/PrepareForInteractiveConfiguration'); 
            // changed from ->'api/v4/ProductConfiguratorUI.svc/json/StartConfiguration');
            req.setMethod('POST');
        //    req.setHeader('Authorization', 'ApiKey ' + calloutSettings.ApiKey__c); // format for startConfig call..
            req.setHeader( 'ApiKey' , calloutSettings.ApiKey__c);
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Connection', 'keep-alive');
            req.setHeader('Accept', 'application/json');
            String body = JSON.serialize(reqWrap);
            System.debug('MTR **** Daylight Request body in start config ' + body);
            req.setBody(body);
            Http http = new Http();
            HTTPResponse res = http.send(req);
            lr.jsonResponse = (String)JSON.deserialize(res.getBody(), String.class);   
            createRelatedOrderItem(orderId, headerId, detailId);
        } catch (Exception e) {
            lr.error = e.getMessage();
            System.debug('MTR **** Daylight Callout Error... ' + lr.error);
            return lr;
        }
        System.debug('MTR **** Daylight Callout Success!!! ' + lr.jsonResponse );
        return lr;
    }
    
    /**
    * @description 
    * @author mark.rothermal@andersencorp.com | 03-29-2021 
    * @param namespace 
    * @param name 
    * @return Infor_Cpq_Api__mdt 
    **/
    Private static Infor_Cpq_Api__mdt getCalloutSettings(String namespace,String name ){
        System.debug('hit get callout settings');
        Infor_Cpq_Api__mdt calloutSettings = new Infor_Cpq_Api__mdt();
        //TODO - determine if sandbox or production... replace environment with variable.
        try{
            calloutSettings  = [Select 
            ApiKey__c,
            Base_Url__c,
            Tenant__c,
            Namespace__c,
            Name__c,
            Environment__c 
            FROM Infor_Cpq_Api__mdt 
            WHERE Environment__c = 'Sandbox' 
            AND Namespace__c = :namespace 
            AND Name__c = :name 
            limit 1];    
        }catch(Exception ex){
            system.debug(ex.getMessage());
        }
        return calloutSettings;
    }

    /**
    * @description 
    * @author mark.rothermal@andersencorp.com | 03-29-2021 
    * @param settings 
    * @return InputParameters
    * TODO - Header and DetailId will need to be generated or provided by project file
        filling out source header detail is what enables editing an existing product.... 
        for new product only provide header detail 
    **/
    Private static RequestWrapper createBody(Infor_Cpq_Api__mdt settings, String headerId, String detailId,Boolean updateExistingProduct, String redirectUrl){
        String sourceHeaderId ='';
        String sourceDetailId ='';
        if(updateExistingProduct == true){
            sourceHeaderId = headerId;
            sourceDetailId = detailId;
        }
        RequestWrapper body = new RequestWrapper(); 
        InputParameters ip = new InputParameters();
        IntegrationParametersWrapper ipw = new IntegrationParametersWrapper();
        IntegrationParametersWrapper ipw2 = new IntegrationParametersWrapper();
        IntegrationParametersWrapper ipw3 = new IntegrationParametersWrapper();
        List<IntegrationParametersWrapper> ipws = new List<IntegrationPArametersWrapper>();
        List<RapidOptionsWrapper> rows = new List<RapidOptionsWrapper>();
        Application a = new Application();
        a.Instance = settings.Tenant__c;
        a.Name = settings.Tenant__c;
        a.User = 'test';
        Part p = new Part();
        p.Namespace = settings.Namespace__c;
        p.Name = settings.Name__c;
        HeaderDetail hd = new HeaderDetail();
        hd.HeaderId = headerId; 
        hd.DetailId = detailId;
        HeaderDetail shd = new HeaderDetail();
        shd.HeaderId = sourceHeaderId; 
        shd.DetailId = sourceDetailId; 
        ip.Application = a;
        ip.Part = p;
        ip.HeaderDetail = hd;
        ip.SourceHeaderDetail = shd;
        ip.Mode = 0;
        ip.Profile = 'Default';
        ip.VariantKey = null;
        ipw.Name = 'BUS_PROCESS';
        ipw.Type = 0;
        ipw.IsNull = false;
        ipw.SimpleValue = 'CSR'; // needs to be made dynamic at some point.
        ipws.add(ipw);
        ipw2.Name = 'ORDER_NUMBER';
        ipw2.Type = 1;
        ipw2.IsNull = false;
        ipw2.SimpleValue = '1'; // needs to be made dynamic at some point.
        ipws.add(ipw2);
        ipw3.Name = 'ORDER_PRODUCT';
        ipw3.Type = 0;
        ipw3.IsNull = false;
        ipw3.SimpleValue = 'test'; // needs to be made dynamic at some point.
        ipws.add(ipw3);
        ip.IntegrationParameters = ipws;
        ip.RapidOptions = rows; 
        body.inputParameters = ip;
        body.redirectUrl = redirectUrl;
        body.pageCaption = ''; // todo figure out what page Caption is for
        System.debug('Body to send request: '+ body);
        return body;
    }

    @AuraEnabled
    public static void createRelatedOrderItem(String oid, string headerId, string detailId){
        system.debug('oid: '+oid);
        Order relatedOrderData = [SELECT Id, OpportunityId, Opportunity.PriceBook2Id, PriceBook2Id FROM Order WHERE Id =: oid ];

        Product2 pdct = [SELECT Id, Name, ProductCode, Description, Family FROM Product2 WHERE Name = 'Entry Door Opening'];

        List<PricebookEntry> pbeList = new List<PricebookEntry>();

        pbeList = [SELECT Id, Name, UnitPrice, Pricebook2Id, Product2Id FROM PricebookEntry WHERE Product2Id =: pdct.Id ];
        if(pbeList.size() == 0){
            PricebookEntry pbe = new PricebookEntry();
            pbe.Pricebook2Id = relatedOrderData.PriceBook2Id;
            pbe.Product2Id = pdct.Id;
            pbe.UnitPrice = 1;
            pbe.isActive = true;
            pbeList.add(pbe);
            insert pbeList;
        }
        List<OrderItem> oiList = new List<OrderItem>();
        List<PricebookEntry> pbeListToCheck = [SELECT Id, Name, UnitPrice, Pricebook2Id, Product2Id FROM PricebookEntry WHERE Product2Id =: pdct.Id AND Pricebook2Id =: relatedOrderData.Pricebook2Id];
        for(PricebookEntry pb: pbeListToCheck){
            OrderItem newOrderItem = new OrderItem();
            newOrderItem.OrderId = oid;
            newOrderItem.Description = pb.Name;
            newOrderItem.PricebookEntryId = pb.Id;
            newOrderItem.Quantity = 1;
            newOrderItem.UnitPrice = 0.01;
            newOrderItem.HeaderId__c = headerId;
            newOrderItem.DetailId__c = detailId;
            newOrderItem.needsDaylightConfigPricing__c = true;
            oiList.add(newOrderItem);
        }

        if(oiList.size() > 0){
            insert oiList;
        }
    }

    @AuraEnabled
    public static string setOrderItemEligibleforReconfiguration(String orderItemId){
        Orderitem oiData = [SELECT Id, needsDaylightConfigPricing__c, HeaderId__c, DetailId__c, OrderId, UnitPrice, ConfigDescription__c FROM Orderitem WHERE Id =: orderItemId ];
        oiData.needsDaylightConfigPricing__c = true;
        Database.SaveResult SR = Database.update(oiData);
        String returnResponse;
        if(SR.isSuccess() == true){
            returnResponse = 'Success';
        }else{
            returnResponse = 'Error';
        }
        return returnResponse;
    }

    @AuraEnabled
    Public static LightningResponse getUrlforReconfiguration(String namespace,String name, String orderItemId, string redirectUrl){
        System.debug('hit start configuration');
        System.debug('Related order id: '+ orderItemId);
        Orderitem oiData = [SELECT Id, needsDaylightConfigPricing__c, HeaderId__c, DetailId__c, OrderId, UnitPrice, ConfigDescription__c FROM Orderitem WHERE Id =: orderItemId ];
        // oiData.needsDaylightConfigPricing__c = true;
        // update oiData;
        System.Debug(oiData);
        LightningResponse lr = new LightningResponse();
        HttpRequest req = new HttpRequest();
        RequestWrapper reqWrap = new RequestWrapper();
        Infor_Cpq_Api__mdt calloutSettings = getCalloutSettings(namespace,name);

        string jsonBody = '{"inputParameters": {'+
            '"Application": { "Instance": "ANDERSENCORP_PP1","Name": "ANDERSENCORP_PP1","User": "test" },'+
            '"Part": { "Namespace": "POC", "Name": "IED"},'+
            '"Mode": 0,'+
            '"Profile": "default",'+
            '"HeaderDetail" : { "HeaderId": "'+oiData.HeaderId__c+'", "DetailId": "'+oiData.DetailId__c+'"  },'+
            '"SourceHeaderDetail" : { "HeaderId": "'+oiData.HeaderId__c+'", "DetailId": "'+oiData.DetailId__c+'" },'+
            '"VariantKey" : null,'+
            '"IntegrationParameters" : [{"Name":"BUS_PROCESS","Type":0,"IsNull":"false","SimpleValue":"CSR"},{"Name":"ORDER_NUMBER","Type":1,"IsNull":"false","SimpleValue":"1"},{"Name":"ORDER_PRODUCT","Type":0,"IsNull":"false","SimpleValue":"test"}],'+
            '"RapidOptions" : []'+
        '},'+
            '"redirectUrl": "'+redirectUrl+'",'+
            '"pageCaption": ""'+
        '}';
        // if(calloutSettings != null ){
        //     reqWrap = createBody(calloutSettings, headerId, detailId, updateExistingProduct, redirectUrl);
        // }
        try {
            req.setEndpoint(calloutSettings.Base_Url__c + 'api/v3/ProductConfigurator.svc/json/PrepareForInteractiveConfiguration'); 
            // changed from ->'api/v4/ProductConfiguratorUI.svc/json/StartConfiguration');
            req.setMethod('POST');
        //    req.setHeader('Authorization', 'ApiKey ' + calloutSettings.ApiKey__c); // format for startConfig call..
            req.setHeader( 'ApiKey' , calloutSettings.ApiKey__c);
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Connection', 'keep-alive');
            req.setHeader('Accept', 'application/json');
            // String body = JSON.serialize(reqWrap);
            System.debug('MTR **** Daylight Request body in start config ' + jsonBody);
            req.setBody(jsonBody);
            Http http = new Http();
            HTTPResponse res = http.send(req);
            lr.jsonResponse = (String)JSON.deserialize(res.getBody(), String.class);   
            // createRelatedOrderItem(orderId, headerId, detailId);
        } catch (Exception e) {
            lr.error = e.getMessage();
            System.debug('MTR **** Daylight Callout Error... ' + lr.error);
            return lr;
        }
        System.debug('MTR **** Daylight Callout Success!!! ' + lr.jsonResponse );
        
        return lr;
    }

    Private class RequestWrapper {
        Private    InputParameters inputParameters {get;set;}
        Private    String redirectUrl {get;set;}
	    Private    String pageCaption {get;set;}  
    }
    Private class InputParameters {
        Private    Application Application {get; set;}
        Private    Part Part {get; set;}
        Private    Integer Mode  {get; set;}
        Private    String Profile {get; set;}
        Private    HeaderDetail HeaderDetail {get; set;}
        Private    HeaderDetail SourceHeaderDetail {get; set;}
        Private    String VariantKey {get; set;}
        Private    List<IntegrationParametersWrapper> IntegrationParameters {get; set;}
        Private    list<RapidOptionsWrapper> RapidOptions {get; set;}
    }
    Private class Application{
        Private    String Instance {get; set;}
        Private    String Name {get; set;}
        Private    String User {get; set;}
    }
    Private class Part{
        Private    String Namespace {get; set;}
        Private    String Name {get; set;}
    }
    Private class HeaderDetail{
        Private    String HeaderId {get; set;}
        Private    String DetailId {get; set;}
    }
    Private class IntegrationParametersWrapper{
        Private String Name {get;set;}
        Private Integer Type {get;set;}
        Private Boolean IsNull {get;set;}
        Private String SimpleValue {get;set;}
    }
    //"variableName": opts[j].Name, "valueExpression": attrVal
    Private class RapidOptionsWrapper{
        Private String variableName {get;set;}
        Private String valueExpression {get;set;}
    }
}