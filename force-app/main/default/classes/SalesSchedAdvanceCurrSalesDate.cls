/**
 * @File Name          : SalesSchedADvanceCurrSalesDate.cls
 * @Description        : Some stores do not want the auto-scheduling to run, but they still need the next date to advance. This class will be scheduled to run later in the evening to advance the sales date only. 
 * @Author             : Julie Brueggeman
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/23/2020, 12:49:51 PM   Julie Brueggeman          Initial Version
**/
public class SalesSchedAdvanceCurrSalesDate  implements Schedulable{

    public void execute(SchedulableContext sc){
	    System.debug('entering the execution of the next date');
        Time storeCloseTime = Time.newInstance(15,0,0,0);
        Date nextDate = SalesSchedDateSchedule.setNextDate(storeCloseTime);
		System.debug('called the salesscheddateschedule');
       
    }
}