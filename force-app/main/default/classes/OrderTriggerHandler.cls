/*******************************************************//**

@class  OrderTriggerHandler

@brief  Controller to hold logic for Order2 Trigger   
 
@author Pavan Gunna  

@see    OrderTriggerHandlerTest

***********************************************************/ 

/**
 * @author Micah Johnson - Demand Chain 2020
 * @update Updated to remove some OrderAction callouts that are now fired at the record level
 **/

public without sharing class OrderTriggerHandler{

    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    public static boolean IsFromBachJob ;
    public static boolean isFromUploadAPI=false;
    
    //Boolean to avoid recursion 
    public static Boolean isBeforeInsert = true;
    public static Boolean isBeforeUpdate = true;
    public static Boolean isAfterInsert = true;
    public static Boolean isAfterUpdate = true;
    public static Boolean isWorkOrdersCreated = false;
    public static Boolean beforeUpdateCreatedWO = false;
    
    // Collections for Before Insert
    public static List<Order> newOrderBI = new List<Order>();
    public static List<Order> newSerReqBI = new List<Order>();
    // Collections for After Insert
    public static Map<ID, Order> newOrderMapAI = new Map<ID, Order>();    
    public static Map<ID, Order> newSerReqMapAI = new Map<ID, Order>();
    // Collection for Before Update
    public static  Map<ID, Order> newOrderMapBU = new Map<ID, Order>();
    public static  Map<ID, Order> newSerReqMapBU = new Map<ID, Order>();

    // Collection for After Update
    public static  Map<ID, Order> newOrderMapAU = new Map<ID, Order>();
    public static  Map<ID, Order> newSerReqMapAU = new Map<ID, Order>();
    public static  Map<ID, Order> newSerReqChangeOrderMapAU = new Map<ID, Order>();

    
    RMS_financialTransactionManager financialTransactionManager = new RMS_financialTransactionManager();

    
    public OrderTriggerHandler(boolean isExecuting, integer size)
    {
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    

  public void OnBeforeInsert(List<Order> newOrder)
    {
        for(Order OrderRec: newOrder)
        {
            if(OrderRec.RecordTypeName__c == 'CORO_Service')
            {
                newSerReqBI.add(OrderRec);
            } else 
            {
                newOrderBI.add(OrderRec);
            }
            System.debug('OnBeforeInsert Service_Type__c:'+ OrderRec.Service_Type__c);
           
        }
        if(isBeforeInsert){
            isBeforeInsert = false;
            system.debug('Start Order Trigger On Before Insert');
            OrderActions.setNewTimeStamps(newOrder);
            OrderActions.setPriceBookId(newOrder);  
            OrderActions.updateSalesRepContact(newOrder);
            newOrder[0].Time_Draft__c = Datetime.now();
            system.debug('End Order Trigger On Before Insert');
        }        
    }    
    public void OnBeforeUpdate( Map<ID, Order> newOrderMap , Map<ID, Order> oldOrderMap )
    {
        Map<Id, List<Opportunity>> OrderOppIdToOppMap = new Map<Id, List<Opportunity>>();
        for(Order OrderRec: newOrderMap.values())
        {
            System.debug('OnBeforeUpdate RecordTypeName__c:'+ OrderRec.RecordTypeName__c);
            System.debug('OnBeforeUpdate Status:'+ OrderRec.Status);
            Map<Id, String> orderIDsToStatusMap = new Map<Id, String>();
            if(OrderRec.RecordTypeName__c == 'CORO_Service')
            {
                DateTime now = Datetime.now();
                newSerReqMapBU.put(OrderRec.id, OrderRec);
                if(OrderRec.Status == 'Draft' && oldOrderMap.get(OrderRec.id).Status != 'Draft'){
                    OrderRec.Time_Draft__c = now; // keep
                  }else if(OrderRec.Status == 'On Hold' && oldOrderMap.get(OrderRec.id).Status != 'On Hold'){
                    OrderRec.Time_On_Hold__c = now;
                  }else if(OrderRec.Status == 'New' && oldOrderMap.get(OrderRec.id).Status != 'New'){
                    OrderRec.Time_New__c = now;//keep
                  }else if(OrderRec.Status == 'Product Ordered' && oldOrderMap.get(OrderRec.id).Status != 'Product Ordered'){
                    OrderRec.Time_Product_Ordered__c = now;//keep
                  }else if(OrderRec.Status == 'Service Scheduled' && oldOrderMap.get(OrderRec.id).Status != 'Service Scheduled'){
                    OrderRec.Time_Service_Scheduled__c = now;//keep
                  }else if(OrderRec.Status == 'Warranty Submitted' && oldOrderMap.get(OrderRec.id).Status != 'Warranty Submitted'){
                    OrderRec.Time_Warranty_Submitted__c = now;
                    orderIDsToStatusMap.put(OrderRec.Id, OrderRec.Status);
                  }else if(OrderRec.Status == 'Warranty Rejected' && oldOrderMap.get(OrderRec.id).Status != 'Warranty Rejected'){
                    OrderRec.Time_Warranty_Rejected__c = now;
                  }else if(OrderRec.Status == 'Service On Hold' && oldOrderMap.get(OrderRec.id).Status != 'Service On Hold'){
                    OrderRec.Time_Service_on_Hold__c = now;//keep
                  }else if(OrderRec.Status == 'Customer Call Back' && oldOrderMap.get(OrderRec.id).Status != 'Customer Call Back'){
                    OrderRec.Time_Customer_Call_Back__c = now;//keep
                  }else if(OrderRec.Status == 'Service To Be Scheduled' && oldOrderMap.get(OrderRec.id).Status != 'Service To Be Scheduled'){
                    OrderRec.Time_Service_to_be_Scheduled__c = now;//keep
                  }else if(OrderRec.Status == 'Service Complete' && oldOrderMap.get(OrderRec.id).Status != 'Service Complete'){
                    OrderRec.Time_Service_Complete__c = now;//keep
                  }else if(OrderRec.Status == 'To Be Ordered' && oldOrderMap.get(OrderRec.id).Status != 'To Be Ordered'){
                    OrderRec.Time_To_Be_Ordered__c = now; // keep
                  }else if(OrderRec.Status == 'Seasonal Service' && oldOrderMap.get(OrderRec.id).Status != 'Seasonal Service'){
                    OrderRec.Time_Seasonal_Service__c = now;//keep
                  }else if(OrderRec.Status == 'Quote' && oldOrderMap.get(OrderRec.id).Status != 'Quote'){
                    OrderRec.Time_Quote__c = now; // keep
                  }else if(OrderRec.Status == 'Tech Measure Needed' && oldOrderMap.get(OrderRec.id).Status != 'Tech Measure Needed'){
                    OrderRec.Time_Tech_Measure_Needed__c = now;
                  }else if((OrderRec.Status == 'Tech Measure Scheduled' && oldOrderMap.get(OrderRec.id).Status != 'Tech Measure Scheduled') && OrderRec.Time_Tech_Measure_Scheduled__c == Null){
                    OrderRec.Time_Tech_Measure_Scheduled__c = now;
                  }else if((OrderRec.Status == 'Ready to Order' && oldOrderMap.get(OrderRec.id).Status != 'Ready to Order') && OrderRec.Time_Ready_To_Order__c == NULL){
                    OrderRec.Time_Ready_To_Order__c = now;
                  }else if(OrderRec.Status == 'Order Released' && oldOrderMap.get(OrderRec.id).Status != 'Order Released'){
                    OrderRec.Time_Order_Released__c = now;
                  }else if(OrderRec.Status == 'Install Needed' && oldOrderMap.get(OrderRec.id).Status != 'Install Needed'){
                    OrderRec.Time_Install_Needed__c = now;
                  }else if((OrderRec.Status == 'Install Scheduled' && oldOrderMap.get(OrderRec.id).Status != 'Install Scheduled') && OrderRec.Time_Install_Scheduled__c == Null ){
                    OrderRec.Time_Install_Scheduled__c = now;
                  }else if(OrderRec.Status == 'Install Complete' && oldOrderMap.get(OrderRec.id).Status != 'Install Complete'){
                    OrderRec.Time_Install_Complete__c = now;
                  }else if(OrderRec.Status == 'Job in Progress' && oldOrderMap.get(OrderRec.id).Status != 'Job in Progress'){
                    OrderRec.Time_Job_In_Progress__c = now;
                  }else if((OrderRec.Status == 'Job Closed' && oldOrderMap.get(OrderRec.id).Status != 'Job Closed') || (OrderRec.Status == 'Closed' && oldOrderMap.get(OrderRec.id).Status != 'Closed')){
                    OrderRec.Time_Job_Close__c = now;
                    OrderRec.Job_Close_Date__c = Date.today();
                    orderIDsToStatusMap.put(OrderRec.Id, OrderRec.Status);
                  }else if(OrderRec.Status == 'Pending Cancellation' && oldOrderMap.get(OrderRec.id).Status != 'Pending Cancellation'){
                    OrderRec.Time_Pending_Cancellation__c = now;
                  }else if(OrderRec.Status == 'Cancelled' && oldOrderMap.get(OrderRec.id).Status != 'Cancelled'){
                    OrderRec.Time_Cancelled__c = now;
                    orderIDsToStatusMap.put(OrderRec.Id, OrderRec.Status);
                  }
            } else {
                newOrderMapBU.put(OrderRec.id, OrderRec);
                OrderOppIdToOppMap.put(OrderRec.OpportunityId, new List<Opportunity>());
            }
            if(OrderRec.RecordTypeName__c == 'Change_Order' ){
                system.debug('CHANGE ORDER IN ORDER TRIGGER' + OrderRec);
            }
            List<WorkOrder> ListOfWorkOrdersToUpdate = new List<WorkOrder>();
            List<ServiceAppointment> ListOfServiceAppointmentsToUpdate = new List<ServiceAppointment>();
            if(!orderIDsToStatusMap.isEmpty()){
                // List<WorkOrder> woListToUpdate = [SELECT Id, Status, Sold_Order__c, Sold_Order__r.Status FROM WorkOrder WHERE Sold_Order__c IN: orderIDsToStatusMap.keySet()];
                for(WorkOrder wo: [SELECT Id, Status, Sold_Order__c, Cancel_Reason__c, (SELECT Id, Status FROM Service_Appointments__r) FROM WorkOrder WHERE Sold_Order__c IN: orderIDsToStatusMap.keySet()]){
                    if(orderIDsToStatusMap.get(wo.Sold_Order__c) == 'Warranty Submitted' && (wo.Status == 'Scheduled & Assigned' || wo.Status == 'Appt Complete/Closed' || wo.Status == 'Appt Complete / Closed')){
                        wo.Status = 'Appt Complete / Closed';
                        ListOfWorkOrdersToUpdate.add(wo);
                        for(ServiceAppointment sa: wo.Service_Appointments__r){
                            sa.Status = 'Completed';
                            ListOfServiceAppointmentsToUpdate.add(sa);
                        }
                    }
                    else if((orderIDsToStatusMap.get(wo.Sold_Order__c) == 'Cancelled' || orderIDsToStatusMap.get(wo.Sold_Order__c) == 'Closed' || orderIDsToStatusMap.get(wo.Sold_Order__c) == 'Job Closed') && (wo.Status == 'Scheduled & Assigned' || wo.Status == 'Appt Complete/Closed' || wo.Status == 'Appt Complete / Closed')){
                        wo.Status = 'Appt Complete / Closed';
                        ListOfWorkOrdersToUpdate.add(wo);
                        for(ServiceAppointment sa: wo.Service_Appointments__r){
                            if(sa.Status == 'New'){
                                sa.Status = 'Canceled';
                            }
                            else{
                                sa.Status = 'Completed';
                            }
                            ListOfServiceAppointmentsToUpdate.add(sa);
                        }
                    }
                    else if((orderIDsToStatusMap.get(wo.Sold_Order__c) == 'Cancelled' || orderIDsToStatusMap.get(wo.Sold_Order__c) == 'Closed' || orderIDsToStatusMap.get(wo.Sold_Order__c) == 'Job Closed') && (wo.Status != 'Scheduled & Assigned' || wo.Status != 'Appt Complete / Closed' || wo.Status != 'Appt Complete/Closed')){
                        wo.Status = 'Canceled';
                        wo.Cancel_Reason__c = 'Order Canceled';
                        ListOfWorkOrdersToUpdate.add(wo);
                        for(ServiceAppointment sa: wo.Service_Appointments__r){
                            if(sa.Status == 'New'){
                                sa.Status = 'Canceled';
                            }
                            else{
                                sa.Status = 'Canceled';
                            }
                            ListOfServiceAppointmentsToUpdate.add(sa);
                        }
                    }
                    else if(orderIDsToStatusMap.get(wo.Sold_Order__c) == 'Cancelled' && (wo.Status != 'Scheduled & Assigned' || wo.Status != 'Appt Complete / Closed' ||  wo.Status != 'Appt Complete/Closed')){
                        wo.Status = 'Canceled';
                        wo.Cancel_Reason__c = 'Order Canceled';
                        ListOfWorkOrdersToUpdate.add(wo);
                        for(ServiceAppointment sa: wo.Service_Appointments__r){
                            sa.Status = 'Canceled';
                            ListOfServiceAppointmentsToUpdate.add(sa);
                        }
                    }
                }
            }

            if(ListOfWorkOrdersToUpdate.size() > 0){
                update ListOfWorkOrdersToUpdate;
            }
            if(ListOfServiceAppointmentsToUpdate.size() > 0){
                update ListOfServiceAppointmentsToUpdate;
            }
        }
        if(isBeforeUpdate){
            isBeforeUpdate = false ;
            system.debug('Start Order Trigger On Before Update');
            
            OrderActions.checkLockedByStatus(newOrderMap,oldOrderMap, 'Order');
            RMS_WorkOrderCreationManager.createWorkOrderOnOrderActivation(oldOrderMap.values(),newOrderMapBU.values(),oldOrderMap,newOrderMapBU);
            OrderActions.updateRevenueRecDate(oldOrderMap,newSerReqMapBU );            
            OrderActions.updateChangeHistories(oldOrderMap, newOrderMapBU);
            OrderActions.updateSalesRepContact(newOrderMap,oldOrderMap );              
            CommissionsManagement.checkCancelOrderAmounts(oldOrderMap,newOrderMap);
            OrderActions.checkRouteMileage(oldOrderMap, newSerReqMapBU);
            OrderActions.callDLRS(newOrderMap.values());
            ///MRJ Demand Chain 10/21/2020 - Added below line to fire JE's. Moved from 'AfterUpdate' logic/////
            financialTransactionManager.onAfterUpdateOrder(oldOrderMap.values(), newOrderMap.values(), oldOrderMap, newOrderMap);

/* 20201215 - Jason Flippen - Bulkified to code shown below this.
            if( newOrderMap.values()[0].Status == 'On Hold' && oldOrderMap.values()[0].Status != 'On Hold'){
                OrderActions.createOnHoldTasks(oldOrderMap, newOrderMap);
            }
            
            if( newOrderMap.values()[0].Status == 'Install Complete' && newOrderMap.values()[0].Time_Install_Complete__c == null){
                newOrderMap.values()[0].Time_Install_Complete__c = Datetime.now();
                newOrderMap.values()[0].Installation_Date_Complete__c = date.today();
            }
            if(newOrderMap.values()[0].Time_Job_Close__c == null && newOrderMap.values()[0].Status == 'Job Closed' && newOrderMap.values()[0].Job_Close_Date__c == null){
                newOrderMap.values()[0].Time_Job_Close__c = Datetime.now();
                newOrderMap.values()[0].Job_Close_Date__c = Date.today();
            }
            List<ServiceAppointment> saListToUpdate = new List<ServiceAppointment>();
            if(newOrderMap.values()[0].Status == 'Warranty Submitted' && oldOrderMap.values()[0].Status != 'Warranty Submitted'){
                List<WorkOrder> woList = [SELECT Id, Sold_Order__c , Work_Order_Type__c, (SELECT Id, Status, Order__c, Is_Primary_Service_Resource__c FROM Service_Appointments__r WHERE Is_Primary_Service_Resource__c = true) FROM WorkOrder WHERE Sold_Order__c IN: newOrderMap.keySet() AND (Work_Order_Type__c = 'Service' OR  Work_Order_Type__c = 'Install')];
                System.debug(woList);
                for(WorkOrder wo: woList){
                    if(wo.Service_Appointments__r.size() > 0){
                        for(ServiceAppointment sa: wo.Service_Appointments__r){
                            if(sa.Order__c == null){
                                sa.Order__c = wo.Sold_Order__c;
                            }
                            saListToUpdate.add(sa);
                        }
                    }
                }
            }
            System.debug(saListToUpdate);
            if(saListToUpdate.size() > 0){
                update saListToUpdate;
                System.enqueueJob(new DynamicRollUpQue(saListToUpdate));
            }
*/

            Set<Id> warrantySubmittedOrderIdSet = new Set<Id>();
            for (Order o : newOrderMap.values()) {
                if (o.Status != oldOrderMap.get(o.Id).Status && o.Status == 'Warranty Submitted') {
                    warrantySubmittedOrderIdSet.add(o.Id);
                }
            }

            Map<Id,List<WorkOrder>> orderWorkOrderListMap = new Map<Id,List<WorkOrder>>();
            if (!warrantySubmittedOrderIdSet.isEmpty()) {

                for (WorkOrder wo : [SELECT Id,
                                            Sold_Order__c,
                                            Work_Order_Type__c,
                                            (SELECT Id,
                                                    Status,
                                                    Order__c,
                                                    Is_Primary_Service_Resource__c
                                             FROM   Service_Appointments__r
                                             WHERE  Is_Primary_Service_Resource__c = true)
                                     FROM   WorkOrder
                                     WHERE  Sold_Order__c IN :warrantySubmittedOrderIdSet
                                     AND    (Work_Order_Type__c = 'Service' OR  Work_Order_Type__c = 'Install')]) {
                    
                    if (!orderWorkOrderListMap.containsKey(wo.Sold_Order__c)) {
                        orderWorkOrderListMap.put(wo.Sold_Order__c, new List<WorkOrder>());
                    }
                    orderWorkOrderListMap.get(wo.Sold_Order__c).add(wo);
                }

            }

            Map<Id,Order> onHoldOrderMap = new Map<Id,Order>();
            List<ServiceAppointment> saListToUpdate = new List<ServiceAppointment>();
            List<ServiceAppointment> saListToRollup = new List<ServiceAppointment>();
            for (Order o : newOrderMap.values()) {

                if (o.Status != oldOrderMap.get(o.Id).Status) {

                    if (o.Status == 'Install Complete' && o.Time_Install_Complete__c == null) {
                        o.Time_Install_Complete__c = Datetime.now();
                        o.Installation_Date_Complete__c = date.today();
                    }
                    else if (o.Status == 'Job Closed' && o.Time_Job_Close__c == null && o.Job_Close_Date__c == null) {
                        o.Time_Job_Close__c = Datetime.now();
                        o.Job_Close_Date__c = Date.today();
                    }
                    else if (o.Status == 'Job In Progress') {
                        if (o.Time_Job_in_Progress__c == null) {
                            o.Time_Job_in_Progress__c = Datetime.now();
                        }
                    }
                    else if (o.Status == 'On Hold') {
                        if (o.Time_On_Hold__c == null) {
                            o.Time_On_Hold__c = Datetime.now();
                        }
                        onHoldOrderMap.put(o.Id, o);
                    }
                    else if (o.Status == 'Warranty Submitted' && !orderWorkOrderListMap.isEmpty()) {
                        if (orderWorkOrderListMap.containsKey(o.Id)) {
                            for (WorkOrder wo : orderWorkOrderListMap.get(o.Id)) {
                                for (ServiceAppointment sa : wo.Service_Appointments__r) {
                                    if (sa.Order__c == null) {
                                        sa.Order__c = wo.Sold_Order__c;
                                        saListToUpdate.add(sa);
                                    }
                                    saListToRollup.add(sa);
                                }
                            }
                        }
                    }

                }

            }

            if (!onHoldOrderMap.isEmpty()) {
                OrderActions.createOnHoldTasks(oldOrderMap, onHoldOrderMap);
            }

            System.debug('***** saListToUpdate: ' + saListToUpdate);
            if (!saListToUpdate.isEmpty()) {
                update saListToUpdate;
            }
            if(!saListToRollup.isEmpty()){
                System.enqueueJob(new DynamicRollUpQue(saListToRollup));
            }

            system.debug('End Order Trigger On Before Update');
        } 
         else if(!beforeUpdateCreatedWO &&!newOrderMapBU.isEmpty()){
           RMS_WorkOrderCreationManager.createWorkOrderOnOrderActivation(oldOrderMap.values(),newOrderMapBU.values(),oldOrderMap,newOrderMapBU);
        }   
        
    }    
    public void OnAfterInsert( Map<ID, Order> newOrderMap)
    {
        for(Order OrderRec: newOrderMap.values())
        {
            if(OrderRec.RecordTypeName__c == 'CORO_Service')
            {
                newSerReqMapAI.put(OrderRec.id, OrderRec);
            } else 
            {
                newOrderMapAI.put(OrderRec.id, OrderRec);
            }
        }
        if(isAfterInsert){
            isAfterInsert = false;
            system.debug('Start Order Trigger On After Insert');
             RMS_WorkOrderCreationManager.createWorkOrderOnOrderCreation(newOrderMapAI.values(), newOrderMapAI);
             OrderActions.createBackOfficeChecksOnOrderCreation(newOrderMapAI.values(), newOrderMapAI);
             OrderActions.checkLockedByStatus(newOrderMapAI,null, 'Order');
             OrderActions.checkRouteMileage(null, newSerReqMapAI);
             system.debug('End Order Trigger On After Insert');
        }        
    }
    public void OnAfterUpdate( Map<ID, Order> newOrderMap , Map<ID, Order> oldOrderMap )
    {
        Map<Id, List<Opportunity>> OrderOppIdToOppMap = new Map<Id, List<Opportunity>>();
         for(Order OrderRec: newOrderMap.values())
        {
            if(OrderRec.RecordTypeName__c == 'CORO_Service')
            {
                newSerReqMapAU.put(OrderRec.id, OrderRec);
            } else 
            {
                newOrderMapAU.put(OrderRec.id, OrderRec);
                OrderOppIdToOppMap.put(OrderRec.OpportunityId, new List<Opportunity>());
            }
            if(OrderRec.RecordTypeName__c == 'CORO_Service' || OrderRec.RecordTypeName__c == 'Change_Order'){
                newSerReqChangeOrderMapAU.put(OrderRec.id, OrderRec);
            }
        }
        if(isAfterUpdate){
            isAfterUpdate = false;
            system.debug('Start Order Trigger On After Update ');
            if(!isWorkOrdersCreated){
               RMS_WorkOrderCreationManager.createWorkOrderOnOrderSoldOrderBeingAssigned(oldOrderMap.values(), newSerReqMapAU.values(), oldOrderMap, newSerReqMapAU);
               isWorkOrdersCreated = true;
         }
            financialTransactionManager.onAfterUpdateOrder(oldOrderMap.values(), newOrderMap.values(), oldOrderMap, newOrderMap);
            OrderActions.customerPickup(oldOrderMap,newOrderMap);
            OrderActions.createOnHoldTasks(oldOrderMap, newOrderMap);  
            OrderActions.updateUniqueIdentifier(oldOrderMap, newOrderMapAU, false);
            OrderActions.updateRelatedWOOfSerReq(newSerReqMapAU, oldOrderMap);
            OrderActions.callDLRS(newSerReqChangeOrderMapAU.values());
            OrderActions.callDLRSForSR(newSerReqChangeOrderMapAU.values(), oldOrderMap);
          
            system.debug('End Order Trigger On After Update '); 
        }
         else if(!newSerReqMapAU.isEmpty() && !isWorkOrdersCreated){
           RMS_WorkOrderCreationManager.createWorkOrderOnOrderSoldOrderBeingAssigned(oldOrderMap.values(), newSerReqMapAU.values(), oldOrderMap, newSerReqMapAU); 
           isWorkOrdersCreated = true;
             
        }
        //Below method is written by Ratna for handling community user activation/deactivation based on order status-Cancelled
        OrderActions.handleCancelOrders(oldOrderMap,newOrderMap);
    }
    
    public void OnAfterDelete(Map<ID, Order> oldOrderMap){
        
    }
     public void OnAfterUnDelete(Map<ID, Order> newOrderMap){
        
    }

    @future 
    public static void OnAfterUpdateAsync(Set<ID> newOrderIDs)
    {
    
    }      
    public boolean IsTriggerContext
    {
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext
    {
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext
    {
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext
    {
        get{ return !IsTriggerContext;}
    }
}