Global class ProductConfiguration {
    public  Decimal widthInches {get;set;}
    public  String widthFractions {get;set;}
    public  Decimal heightInches {get;set;}
    public  String heightFractions {get;set;}
    public  String specialtyShape {get;set;}
    // grille and specialty grille inputs
    public  String grillePattern {get;set;}
    public  String grilleStyle {get;set;}
    public  Decimal spokes {get;set;}    
    public  Decimal hubs {get;set;}
    public  Decimal litesHigh {get;set;}
    public 	Decimal litesWide {get;set;} 
    // screen inputs 
    public String screenType {get;set;}
    public String screenSize {get;set;}
    // color inputs
    public  String exteriorColor {get;set;}
    public  String interiorColor {get;set;} 
    // size detail inputs
    public String  frame {get;set;}
    public String  hardwareOption {get;set;}    
    public String  sashOperation {get;set;}
    public String  sashRatio {get;set;}
    public Decimal externalForce {get;set;}
    public Decimal internalForce {get;set;}
    public Boolean highPerformance {get;set;} 
    public String performanceCategory{get;set;}
    // glazing inputs 
    public Boolean s1Tempering {get;set;}
    public Boolean s2Tempering {get;set;}
    public String s1Pattern {get;set;}
    public String s2Pattern {get;set;}
    public String s1Type {get;set;}
    public String s2Type {get;set;}
    // special options
    public Boolean s3Tempering {get;set;}
    public String s3Pattern {get;set;}
    public String s3Type {get;set;}
    // insert frame inputs
    public Boolean insertFrame {get;set;}
    public String exteriorTrim {get;set;}
    // specialty shape legs
    public Decimal leftLegInches {get;set;}
    public String  leftLegFraction {get;set;}
    public Decimal rightLegInches {get;set;}
    public String  rightLegFraction {get;set;}
    // Ej color config
    public String ejSpecies {get;set;}
    public String ejColor {get;set;}
    // Locks
    public Decimal locks {get;set;}
    // product id
    public  Id productId {get;set;}
    public String productName {get;set;}
}