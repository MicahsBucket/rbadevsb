/**
* @author Jason Flippen
* @date 01/26/2021
* @description Class to provide functionality for the onHoldAction LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - OrderOnHoldActionControllerTest
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/
public with sharing class OrderOnHoldActionController {

    /**
    * @author Jason Flippen
    * @date 01/26/2021
    * @description: Method to retrieve Order data.
    * @param orderId
    * @return OrderWrapper
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @AuraEnabled(cacheable=true)
    public static Order getOrderData(String orderId) {

        // Retrieve Order data.
        Order order = [SELECT Id,
                              Status
                       FROM   Order
                       WHERE  Id = :orderId];        
        
        System.debug('***** order: ' + order);
                                                       
        return order;

    }

    /**
    * @author Jason Flippen
    * @date 01/26/2021
    * @description: Method to update an Order.
    * @param orderId
    * @return saveResult
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @AuraEnabled
    public static String updateOrder(Order order) {

        String saveResult = 'Update Order Success';

        Order updateOrder = new Order (Id = order.Id,
                                       Status = 'On Hold',
                                       Apex_Context__c = true);

        if (order.Status != 'On Hold') {
            updateOrder.Prior_Status__c = order.Status;
        }
        
        try {
            update updateOrder;
        }
        catch (Exception ex) {
            saveResult = ex.getMessage();
        }

        return saveResult;

    }

 }