public class ServiceResourceDetailCtrl {
    
    @AuraEnabled(cacheable=true)
    public static ServiceResource getServiceResources(String SerResourceId) {
        return [SELECT Id,Name, Retail_Location_Formula__c,User_Type__c,
                Secondary_Resource_Type_s__c,Crew_Size__c,General_Insurance_Expiration_Date__c,
                Auto_Insurance_Expiration_Date__c,Work_Comp_Insurance_Expiration_Date__c
                FROM ServiceResource
                WHERE Id = :SerResourceId];
    }
    
}