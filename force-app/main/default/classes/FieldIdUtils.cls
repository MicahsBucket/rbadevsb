public with sharing class FieldIdUtils {

    public final static List<String> DEPENDENT_FIELDS = new List<String> { 'Labor__c.Related_FSL_Work_Order__c',
                                                                            'Labor__c.Store_Location__c',
                                                                            'WorkOrder.Sold_Order__c',
                                                                            'WorkOrder.Opportunity__c' };

    public static String callToolingApi(String queryString) {
        // Constructs the Tooling API wrapper (default constructor uses user session Id)
        ToolingAPI toolingAPI = new ToolingAPI();
        ToolingAPIWSDL.QueryResult queryResult = toolingAPI.query(queryString);
        ToolingAPIWSDL.sObject_x customObject;
        String customObjectId = '';
        if(queryResult.records != null) {
            customObject = queryResult.records[0];
            customObjectId = customObject.Id;
        }
        return customObjectId;
    }

    public static String getFieldIdFromNameFromTooling(String sObjectName, String fieldName) {
        // No __c suffixes are necessary in the queries
        sObjectName = sObjectName.replace('__c', '');
        fieldName = fieldName.replace('__c', '');
         
        // Query CustomObject object by DeveloperName
        String queryString = 'SELECT Id, ' +
                                    'DeveloperName, ' +
                                    'NamespacePrefix ' +
                             'FROM CustomObject ' +
                             'WHERE DeveloperName = \'' + sObjectName + '\'';
        String customObjectId = callToolingApi(queryString);

        // Query CustomField by DeveloperName and CustomObject id 
        queryString = 'SELECT Id, ' +
                             'DeveloperName, ' +
                             'NamespacePrefix ' +
                      'FROM CustomField ' +
                      'WHERE TableEnumOrId = \'' + customObjectId + '\' ' +
                      'AND DeveloperName = \'' + fieldName + '\'';
        String customFieldId = callToolingApi(queryString);
        if(customFieldId == '') {
             // Query CustomField by DeveloperName and CustomObject Name 
            queryString = 'SELECT Id, ' +
                                 'DeveloperName, ' +
                                 'NamespacePrefix, ' +
                                 'TableEnumOrId ' +
                          'FROM CustomField ' +
                          'WHERE TableEnumOrId = \'' + sObjectName + '\' ' +
                          'AND DeveloperName = \'' + fieldName + '\'';
            customFieldId = callToolingApi(queryString); 
        }

        return customFieldId;
    }

    public static void runInstall() {
        runInstall(DEPENDENT_FIELDS);
    }

    public static void runInstall(List<String> fieldsToInstall) {
        Map<String, String> fieldsToIds = new Map<String, String>();
        // Call tooling api to build map of ids
        for(String dependentField : fieldsToInstall) {
            String sObjectName = dependentField.split('\\.')[0];
            String fieldName = dependentField.split('\\.')[1];
            String fieldId = getFieldIdFromNameFromTooling(sObjectName, fieldName);
            fieldsToIds.put(dependentField, fieldId);
        }

        Id deployEnqueueId = deployFieldIdsMDT(fieldsToIds);
        System.debug('Creating metadata types for field ids and the Id of the deploy process is ' + deployEnqueueId);
    }

    

    public static Id deployFieldIdsMDT(Map<String, String> fieldsToIds) {
        Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();

        for(String field : fieldsToIds.keySet()) {
            String sObjectName = field.split('\\.')[0];
            String fieldName = field.split('\\.')[1];
            
            // No __c suffixes are necessary in the queries
            sObjectName = sObjectName.replace('__c', '');
            fieldName = fieldName.replace('__c', '');

            String fieldId = fieldsToIds.get(field);

            // Set up custom metadata to be created
            Metadata.CustomMetadata fieldIdMDT =  new Metadata.CustomMetadata();
            fieldIdMDT.fullName = 'Field_Id.' + sObjectName + '_' + fieldName;
            fieldIdMDT.label = sObjectName + '_' + fieldName;

            Metadata.CustomMetadataValue sObjectNameField = new Metadata.CustomMetadataValue();
            sObjectNameField.field = 'SObject_API_Name__c';
            sObjectNameField.value = sObjectName;
            fieldIdMDT.values.add(sObjectNameField);

            Metadata.CustomMetadataValue fieldNameField = new Metadata.CustomMetadataValue();
            fieldNameField.field = 'Field_API_Name__c';
            fieldNameField.value = fieldName;
            fieldIdMDT.values.add(fieldNameField);

            Metadata.CustomMetadataValue fieldIdField = new Metadata.CustomMetadataValue();
            fieldIdField.field = 'Field_Id__c';
            fieldIdField.value = fieldId;
            fieldIdMDT.values.add(fieldIdField);
            
            mdContainer.addMetadata(fieldIdMDT);
        }
        
        // Enqueue custom metadata deployment
        // jobId is the deployment ID
        Id jobId;
        if(!Test.isRunningTest()) {
            jobId = Metadata.Operations.enqueueDeployment(mdContainer, null);    
        }

        return jobId;
    }

    public static String getFieldIdFromName(String sObjectName, String fieldName) {
        // No __c suffixes are necessary in the queries
        sObjectName = sObjectName.replace('__c', '');
        fieldName = fieldName.replace('__c', '');

        Field_Id__mdt[] fieldIdMDT = [SELECT Field_Id__c 
                                      FROM Field_Id__mdt
                                      WHERE SObject_API_Name__c =: sObjectName
                                      AND Field_API_Name__c =: fieldName];
        String fieldId;
        try {
            fieldId = fieldIdMDT[0].Field_Id__c;
        } catch(Exception e) {
            System.debug('The system was not able to find a Field Id metadata record matching that query.');
            return '';
        }
        return fieldId;
    }

    public static String getFormattedLookupIdFromName(String sObjectName, String fieldName) {
        String lookupFieldId = getFieldIdFromName(sObjectName, fieldName);
        if(lookupFieldId.length() > 15) {
            lookupFieldId = lookupFieldId.substring(0, 15);
        }
        lookupFieldId = 'CF' + lookupFieldId;
        return lookupFieldId;
    }

    public static String getFormattedLKIDFromName(String sObjectName, String fieldName) {
        String formattedLookupFieldId = getFormattedLookupIdFromName(sObjectName, fieldName);
        formattedLookupFieldId = formattedLookupFieldId + '_lkid';
        return formattedLookupFieldId;
    }    


}