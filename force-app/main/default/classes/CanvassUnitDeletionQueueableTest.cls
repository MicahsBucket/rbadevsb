@isTest(SeeAllData=false)
global class CanvassUnitDeletionQueueableTest {

    public static testMethod void test1()
    {
        CNVSS_Canvass_Market__c canvassMarket = new CNVSS_Canvass_Market__c();
        canvassMarket.Name = 'Test';
        canvassMarket.CNVSS_Canvass_Market_Manager__c = userInfo.getUserId();
        canvassMarket.CNVSS_ISC_Phone_Number__c = '5555555555';
        insert canvassMarket;
        
        Canvass_Market_Zip_Code__c canvassMarketZip = new Canvass_Market_Zip_Code__c();
        canvassMarketZip.Name = '28217';
        canvassMarketZip.Zip_Code__c = '28217';
        canvassMarketZip.Canvass_Market__c = canvassMarket.id;
        insert canvassMarketZip;
        
        Custom_Data_Layer_Batch_Queue__c canvassDataLayerQueue = new Custom_Data_Layer_Batch_Queue__c();
        canvassDataLayerQueue.Status__c = 'Holding';
        canvassDataLayerQueue.Field_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.ZipCode__c = '60609';
        canvassDataLayerQueue.Market_Id__c = canvassMarket.Id;
        insert canvassDataLayerQueue;
        
        CNVSS_Canvass_Unit__c canvassUnit = new CNVSS_Canvass_Unit__c();
        canvassUnit.CNVSS_Canvass_Market__c = canvassMarket.Id;
        canvassUnit.CNVSS_House_No__c = '5200';
        canvassUnit.CNVSS_Street__c = '77 Center';
        canvassUnit.Street_Type__c = 'Dr';
        canvassUnit.CNVSS_City__c = 'Charlotte';
        canvassUnit.CNVSS_State__c = 'NC';
        canvassUnit.CNVSS_Zip_Code__c = '28217';
        canvassUnit.Fullstreetaddress__c = '5200 77 Center Dr #400, Charlotte, NC 28217';
        canvassUnit.Auto_Created__c = true;
        canvassUnit.Auto_Modified__c = false;
        insert canvassUnit;
        
        CNVSS_Canvass_Unit__c AnotherCanvassUnit = new CNVSS_Canvass_Unit__c();
        AnotherCanvassUnit.CNVSS_Canvass_Market__c = canvassMarket.Id;
        AnotherCanvassUnit.CNVSS_House_No__c = '12345';
        AnotherCanvassUnit.CNVSS_Street__c = '123456 Fake Street';
        AnotherCanvassUnit.Street_Type__c = 'Dr';
        AnotherCanvassUnit.CNVSS_City__c = 'Charlotte';
        AnotherCanvassUnit.CNVSS_State__c = 'NC';
        AnotherCanvassUnit.CNVSS_Zip_Code__c = '28217';
        AnotherCanvassUnit.Fullstreetaddress__c = '12345 123456 Fake Stree Dr, Charlotte, NC 28217';
        AnotherCanvassUnit.Auto_Created__c = true;
        AnotherCanvassUnit.Auto_Modified__c = false;
        insert AnotherCanvassUnit;
        
        CNVSS_Canvass_Lead_Sheet__c canvassUnitLeadSheet = new CNVSS_Canvass_Lead_Sheet__c();
        canvassUnitLeadSheet.CNVSS_Canvass_Unit__c = canvassUnit.Id;
        insert canvassUnitLeadSheet;
        
        set<string> zipSet = new set<string>();
        zipSet.add(canvassMarketZip.Zip_Code__c);
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('MockPropertyDataFields');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
                
        list<Custom_Data_Layer_Batch_Queue__c> scope = [SELECT Id, Name, Field_Ids__c, Marker_Ids__c, Marker_Ids2__c, Market_Id__c, Status__c, ZipCode__c 
                                                        FROM Custom_Data_Layer_Batch_Queue__c 
                                                        WHERE Status__c = 'Holding'];
        // iterate over scope
        for(Custom_Data_Layer_Batch_Queue__c queuedJob :scope)
        {
            list<string> fieldIdList = queuedJob.Field_Ids__c.split(', ');
            list<string> markerIdList = queuedJob.Marker_Ids__c.split(', ');
            list<string> markerIdList2 = new list<string>();
            
            if(queuedJob.Marker_Ids2__c != null)
            {
                markerIdList2 = queuedJob.Marker_Ids2__c.split(', ');
            }
            
            //set<string> zipCodeSet = new set<string>();
            //zipCodeSet.add(queuedJob.ZipCode__c);
            
            list<object> fieldIdListObj = new list<object>();
            for(string field :fieldIdList)
            {
                fieldIdListObj.add(field);
            }
            
            list<object> markerIdListObj = new list<object>();
            for(string marker :markerIdList)
            {
                markerIdListObj.add(integer.valueOf(marker));
            }
            
            for(string marker :markerIdList2)
            {
                markerIdListObj.add(integer.valueOf(marker));
            }
            
            // start queueable job
            System.enqueueJob(new CanvassUnitDeletionQueueable('28217',canvassMarket.id,canvassDataLayerQueue.id));
            
            list<string> idList = new list<string>();
            idList.add('123456789123456789');
            
            System.enqueueJob(new CanvassUnitDeletionQueueable('28217',canvassMarket.id,canvassDataLayerQueue.id,idList));
        }
        
        Test.stopTest();
    }
}