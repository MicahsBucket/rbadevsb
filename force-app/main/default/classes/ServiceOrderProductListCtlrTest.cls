@isTest
public class ServiceOrderProductListCtlrTest {

    @testSetup static void setupData() {

        RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name = 'Turn Financial Transactions Off', Value__c = 'Yes');
        insert turnOffFinancialTrigger;

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        Account testVenderAcct = testUtility.createVendorAccount('Test Vendor Account');
        insert testVenderAcct;

        Account testStoreAcct = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAcct = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAcct.Store_Location__c = testStoreAcct.Id;
        Store_Configuration__c storeConfig1 = [SELECT id FROM Store_Configuration__c WHERE Store__c = :testStoreAcct.id ];
        insert testDwellingAcct;

        Contact contact1 = new contact ( FirstName='Contact',LastName='1',AccountId=testDwellingAcct.id);
        insert contact1;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAcct.Id, 'Closed - Won');
        insert testOpportunity;
    
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;

        List<Product2> testProductList = new List<Product2>();
        // Parent Product
        Product2 testParentProduct = new Product2(Name = 'Test Parent Product',
                                                  RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId(),
                                                  Vendor__c = testVenderAcct.Id,
                                                  Cost_PO__c = true,
                                                  Number_Service_Products__c = 1,
                                                  IsActive = true,
                                                  Account_Number__c =  testFan.Id);
        testProductList.add(testParentProduct);
 
        Product2 masterProduct = new Product2( name = 'master',Number_Service_Products__c = 1,IsActive = true, recordTypeId=Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Master_Product').getRecordTypeId());
        testProductList.add(masterProduct);
        Product2 servProduct = new Product2( name = 'service', Number_Service_Products__c = 1, IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        testProductList.add(servProduct);
        Product2 servProduct2 = new Product2(name = 'service2',Number_Service_Products__c = 1, IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        testProductList.add(servProduct2);

        insert testProductList;

        List<Asset> testAssetList = new List<Asset>();

        Asset testAsset = new Asset(Name = 'Test Asset',
                                    AccountId = testDwellingAcct.Id);
        testAssetList.add(testAsset);

        insert testAssetList;

        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        //Id pricebookId = Test.getStandardPricebookId();

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        // Parent PricebookEntry
        PricebookEntry testParentPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testParentProduct.Id);
        testPBEList.add(testParentPBEStandard);
        PricebookEntry testParentPBE = testUtility.createPricebookEntry(testPricebook.Id, testParentProduct.Id);
        testPBEList.add(testParentPBE);
        //insert testPBEList;

        PricebookEntry testParentPBEStandard1 = testUtility.createPricebookEntry(Test.getStandardPricebookId(), masterProduct.Id);
        testPBEList.add(testParentPBEStandard1);
        PricebookEntry pricebookEntryMaster = testUtility.createPricebookEntry(testPricebook.Id, masterProduct.Id);   
        testPBEList.add(pricebookEntryMaster);  

        PricebookEntry testParentPBEStandard2 = testUtility.createPricebookEntry(Test.getStandardPricebookId(), servProduct.Id);
        testPBEList.add(testParentPBEStandard2);
        PricebookEntry pricebookEntryServ = testUtility.createPricebookEntry(testPricebook.Id, servProduct.Id);  
        testPBEList.add(pricebookEntryServ);  

        PricebookEntry testParentPBEStandard3 = testUtility.createPricebookEntry(Test.getStandardPricebookId(), servProduct2.Id);
        testPBEList.add(testParentPBEStandard3);
        PricebookEntry pricebookEntryServ2 = testUtility.createPricebookEntry(testPricebook.Id, servProduct2.Id); 
        testPBEList.add(pricebookEntryServ2);     
        //List<PriceBookEntry> pbEntries = new List<PricebookEntry>{pricebookEntryMaster,pricebookEntryServ,pricebookEntryServ2};
        //insert pbEntries;
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order', 
                                     AccountId = testDwellingAcct.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAcct.Id,
                                     OpportunityId = testOpportunity.Id,                                 
                                     Status = 'Draft',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = testPricebook.Id);
        insert testOrder;

        Order soldOrder =  new Order(   Name='Sold Order 1', 
                                 AccountId = testDwellingAcct.id, 
                                 BilltoContactId = contact1.id,
                                 EffectiveDate= Date.Today(), 
                                 Store_Location__c = testStoreAcct.Id,                           
                                 Status ='Draft', 
                                 Pricebook2Id = testPricebook.Id);
        insert soldOrder;
        
        OrderItem testOIParent = new OrderItem(OrderId = testOrder.Id,
                                               PricebookentryId = pricebookEntryServ.Id,
                                               Quantity = 2,
                                               Quanity_Ordered__c = 3,
                                               UnitPrice = 100);
        insert testOIParent;

        OrderItem testOIParent1 = new OrderItem(OrderId = testOrder.Id,
                                               PricebookentryId = pricebookEntryServ.Id,
                                               Quantity = 2,
                                               Quanity_Ordered__c = 3,
                                               UnitPrice = 100);
        insert testOIParent1;

        Service_Product__c newServiceProduct = new Service_Product__c (Master_Product__c = masterProduct.Id);

        insert newServiceProduct;

        OrderItem orderItemMaster = new OrderItem(OrderId = testOrder.id, 
                                    PricebookentryId = pricebookEntryMaster.Id, 
                                    Quantity = 2, UnitPrice = 100, MTO_Source_Code__c = 'D - GBG',
                                    Defect_Code__c = 'B - Broken');
        insert orderItemMaster;

        Asset testAsset1 = new Asset (Name='Asset1',
                                 Original_Order_Product__c = orderItemMaster.Id,
                                 Product2Id= masterProduct.Id,
                                 AccountId = testDwellingAcct.id,
                                 ContactId = contact1.id,
                                 Variant_Number__c = '1234ABC',
                                 Unit_Wholesale_Cost__c = 200,
                                 Store_Location__c = testStoreAcct.id,
                                 Quantity = 1,
                                 Price = 100,
                                 Status = 'Installed',
                                 Sold_Order__c = soldOrder.Id,
                                 PurchaseDate = Date.Today());
        
        insert testAsset1;

        Asset testAsset2 = new Asset (Name='Asset2',
                                 Original_Order_Product__c = orderItemMaster.Id,
                                 Product2Id= servProduct.Id,
                                 AccountId = testDwellingAcct.id,
                                 ContactId = contact1.id,
                                 Variant_Number__c = '1234AB',
                                 Unit_Wholesale_Cost__c = 200,
                                 Store_Location__c = testStoreAcct.id,
                                 Quantity = 1,
                                 Price = 100,
                                 Status = 'Installed',
                                 Sold_Order__c = soldOrder.Id,
                                 PurchaseDate = Date.Today());
        
        insert testAsset2;

    }

    /*
    * @author Wallace Wylie
    * @description Method to test the functionality in the Controller.
    */ 
    private static testMethod void testController() {

        Test.startTest();

            List<Order> testOrderList = [SELECT Id, AccountId, Sold_Order__c FROM Order WHERE Name = 'Test Order'];

    
            List<ServiceOrderProductListCtlr.OrderWrapper> oWrapperList = ServiceOrderProductListCtlr.getOrder(testOrderList[0].Id);
            System.assertEquals(1,oWrapperList.size());

            List<ServiceOrderProductListCtlr.OrderItemWrapper> oiWrapperList = ServiceOrderProductListCtlr.getServiceOrderProductList(testOrderList[0].Id);
            System.assertEquals(2,oiWrapperList.size());

            List<ServiceOrderProductListCtlr.AssetProductWrapper> assetsWithProducts = ServiceOrderProductListCtlr.getAssetsWithProducts(testOrderList[0].Id);
            System.assertEquals(2,oiWrapperList.size());

            String updateResult = ServiceOrderProductListCtlr.updateNotes(oiWrapperList[0].Id, 'Test Notes');
            System.assertEquals('Update Notes Success',updateResult);

            OrderItem updateOrderItem = new OrderItem(Id = oiWrapperList[0].id);
            updateOrderItem.Quote_Accepted__c = true;
            String updateProductResult = ServiceOrderProductListCtlr.updateOrderProduct(updateOrderItem);
            System.assertEquals('Update Product Success',updateProductResult);

        Test.stopTest();

    }

    private static testMethod void testControllerCancel() {
        List<Order> testOrderList = [SELECT Id, AccountId, Sold_Order__c FROM Order WHERE Name = 'Test Order'];
        List<ServiceOrderProductListCtlr.OrderItemWrapper> oiWrapperList = ServiceOrderProductListCtlr.getServiceOrderProductList(testOrderList[0].Id);
        System.debug('oiWrapperList: '+oiWrapperList);
        String cancelProductResult = ServiceOrderProductListCtlr.cancelProduct(oiWrapperList[0]);
            System.debug('cancelProductResult'+ cancelProductResult);
    }
    
    private static testMethod void testControllergetMTOCodes() {
        List<Order> testOrderList = [SELECT Id, AccountId, Sold_Order__c FROM Order WHERE Name = 'Test Order'];
        List<ServiceOrderProductListCtlr.OrderItemWrapper> oiWrapperList = ServiceOrderProductListCtlr.getServiceOrderProductList(testOrderList[0].Id);
        System.debug('oiWrapperList: '+oiWrapperList);
        String getMTOResult = String.valueOf(ServiceOrderProductListCtlr.getMTOCodes(oiWrapperList[0].productId));
        System.debug('getMTOResult'+ getMTOResult);
        ServiceOrderProductListCtlr.getDefectCodes(null, 'Customer');
    }
}