/*
* @author Jason Flippen
* @date 02/28/2021
* @description: Apex Batch Class to convert Work Order Attachments to Files.
*
*			   Code Coverage provided by the following Test Class:
*              - ConvertWOAttachmentsToFilesBatchTest
*/ 
public class ConvertWOAttachmentsToFilesBatch implements Database.Batchable<SObject> {

    private String queryString;
    private Datetime earliestCreatedDateTime;
    private Set<Id> serviceTerritoryIdSet;
    private Id workOrderId;
    private List<String> resultEmailList;

    public ConvertWOAttachmentsToFilesBatch(String queryString,
                                            Datetime earliestCreatedDateTime,
                                            Set<Id> serviceTerritoryIdSet,
                                            Id workOrderId,
                                            List<String> resultEmailList) {
        this.queryString = queryString;
        this.earliestCreatedDateTime = earliestCreatedDateTime;
        this.serviceTerritoryIdSet = serviceTerritoryIdSet;
        this.workOrderId = workOrderId;
        this.resultEmailList = resultEmailList;
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(this.queryString);
    }

    public void execute(Database.BatchableContext bc, List<WorkOrder> workOrderList) {

        if (!workOrderList.isEmpty()) {

            Set<Id> parentRecordIds = new Set<Id>();
            for (WorkOrder wo : workOrderList) {
                parentRecordIds.add(wo.Id);
            }
            System.debug('Work Order Id Count: ' + parentRecordIds.size());
            //System.debug('Work Order Ids: ' + parentRecordIds);

            if (!parentRecordIds.isEmpty()) {

                // get list of CDLs (i.e. existing files)

                Set<Id> existingCdIds = new Set<Id>();

                List<ContentDocumentLink> cdlsForUpdate = new List<ContentDocumentLink>();
                // set all existing CDLS to visibility = 'AllUsers'
                for (ContentDocumentLink cdl : [SELECT Id,
                                                    ContentDocumentId,
                                                    Visibility
                                                FROM   ContentDocumentLink
                                                WHERE  LinkedEntityId IN :parentRecordIds]) {
                    if (cdl.visibility != 'AllUsers') {
                        cdl.visibility = 'AllUsers';
                        cdlsForUpdate.add(cdl);
                    }
                    existingCdIds.add(cdl.ContentDocumentId);
                }
                System.debug('cdlsForUpdate Count: ' + cdlsForUpdate.size());
                
                if (!cdlsForUpdate.isEmpty()) {
                    update cdlsForUpdate; 
                }

                // get list of existing CDs (i.e. existing files)
                Map<String,ContentDocument> cdTitleToObject = new Map<String,ContentDocument>();
                if (!existingCdIds.isEmpty()) {
                    for (ContentDocument cd : [SELECT ContentSize, FileExtension, FileType, Title
                                            FROM ContentDocument
                                            WHERE IsDeleted = false
                                            AND Id IN :existingCdIds]) {
                        cdTitleToObject.put(cd.Title, cd);
                    }
                }
                System.debug('cdTitleToObject Count: ' + cdTitleToObject.size());

                // See if any attachments are already there as files.  If so, exclude
                List<Attachment> attachmentsToMigrate = new List<Attachment>();
                for (Attachment attachment : [SELECT Body,
                                                    BodyLength,
                                                    IsPrivate,
                                                    Name,
                                                    OwnerId,
                                                    ParentId
                                            FROM   Attachment
                                            WHERE  IsDeleted = false
                                            AND    IsPrivate = false
                                            AND    ParentId IN :parentRecordIds]) {
/*
                    System.debug('attachment.BodyLength: ' + attachment.BodyLength);
                    System.debug('attachment.Name: ' + attachment.Name);
                    System.debug('attachment.OwnerId: ' + attachment.OwnerId);
                    System.debug('attachment.ParentId: ' + attachment.ParentId);
*/
            
                    String titleWithoutExtension = attachment.Name.contains('.') ? attachment.Name.left(attachment.Name.indexOf('.')) : attachment.Name;
                    if (cdTitleToObject.containsKey(attachment.Name) || cdTitleToObject.containsKey(titleWithoutExtension)) {
/*
                        System.debug('cdTitleToObject.get(attachment.Name): ' + cdTitleToObject.get(attachment.Name));
                        System.debug('cdTitleToObject.get(titleWithoutExtension): ' + cdTitleToObject.get(titleWithoutExtension));
*/
                
                        // DO NOTHING
                        
                        // Note: the following commented-out code was an attempt to also match on file size; however, file sizes appear to be 
                        // calculated differently for some file types (e.g. pdfs) depending on if the file is an attachment or file
                        
                        //ContentDocument cd = cdTitleToObject.containsKey(attachment.Name) ? cdTitleToObject.get(attachment.Name) : cdTitleToObject.get(titleWithoutExtension);
                        // there's already a matching attachment name as a file; check that the sizes are the same
                        //if (cd.ContentSize == attachment.BodyLength) {
                        // duplicate - do nothing
                        //} else {
                        // not duplicate - migrate
                        //attachmentsToMigrate.add(attachment);
                        //}
                    } else {
                        // migrate
                        attachmentsToMigrate.add(attachment);
                    }
                }
                System.debug('attachmentsToMigrate Count: ' + attachmentsToMigrate.size());

                List<ContentVersion> cvsForInsert = new List<ContentVersion>();

                // copy the remaining attachments to files
                for (Attachment attachment : attachmentsToMigrate) {
                    ContentVersion contentVersion = new ContentVersion(
                        ContentLocation = 'S',
                        PathOnClient = attachment.Name,
                        Origin = 'H',
                        Title = attachment.Name,
                        VersionData = attachment.Body
                    );
                    cvsForInsert.add(contentVersion);
//        		System.debug('contentVersion.Title: ' + contentVersion.Title);
                }
                System.debug('cvsForInsert Count: ' + cvsForInsert.size());
                
                if (!cvsForInsert.isEmpty()) {
                
                    // insert the files
                    insert cvsForInsert;

                    // SF blocks creation of a ContentVersion with a ownerId other than the current user, but it allows updating ownerId:
                    Integer j = 0;
                    for (ContentVersion cv : cvsForInsert) {
                        cv.OwnerId = attachmentsToMigrate[j].OwnerId;
                        j++;
                    }

                    update cvsForInsert;

                    Set<Id> cvIds = new Set<Id>();
                    for (ContentVersion cv : cvsForInsert) {
                        cvIds.add(cv.Id);
                    }

                    Map<Id,ContentVersion> insertedContentVersions = new Map<Id,ContentVersion>([SELECT ContentDocumentId FROM ContentVersion WHERE Id IN :cvIds]);
//              	System.debug('insertedContentVersions: ' + insertedContentVersions);

                    // note: cvsForInsert is the same size, and in the same order, as attachmentsToMigrate
                    // make sure this ordering is preserved after querying ContentDocumentIds by back-populating this on the original objects

                    List<ContentDocumentLink> cdlsForInsert = new List<ContentDocumentLink>();

                    Integer i = 0;
                    for (ContentVersion cv : cvsForInsert) {
                        ContentDocumentLink contentDocumentLink = new ContentDocumentLink(
                            ContentDocumentId = insertedContentVersions.get(cv.Id).ContentDocumentId,
                            LinkedEntityId = attachmentsToMigrate[i].ParentId,
                            ShareType = 'I',
                            Visibility = 'AllUsers'
                        );
                        cdlsForInsert.add(contentDocumentLink);
                        i++;
                    }
//             		System.debug('cdlsForInsert: ' + cdlsForInsert);

                    insert cdlsForInsert;
                    
                }
            
            }

        }

    }

    public void finish(Database.BatchableContext bc) {

        AsyncApexJob job = [SELECT Id,
                                   Status,
                                   NumberOfErrors,
                                   JobItemsProcessed,
                                   TotalJobItems,
                                   CreatedBy.Email
                            FROM   AsyncApexJob
                            WHERE  Id = :bc.getJobId()];
        
        Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
        emailMessage.setToAddresses(resultEmailList);
        emailMessage.setSubject('Convert WO Attachments to Files Batch ' + job.Status);
        String emailBody = 'The Apex Batch job processed ' + job.TotalJobItems + ' batches with ' + job.NumberOfErrors + ' failures.\n\n';
        emailMessage.setPlainTextBody(emailBody);
        if (!Test.isRunningTest()) {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailMessage});
        }

    }
    
}