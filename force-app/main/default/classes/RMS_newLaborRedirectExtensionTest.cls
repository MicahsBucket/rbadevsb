/*******************************************************//**

@class  RMS_newLaborRedirectExtensionTest

@brief  Test Class for RMS_newLaborRedirectExtensionTest

@author  Creston Kuenzi (Slalom.CDK)

@version    2015-2-2  Slalom.CDK,  Created.
            2018-2-7  Penrod - Gabe Rholl,  Edited.

@see        RMS_newLaborRedirectExtension

@copyright  (c)2016 Slalom.  All Rights Reserved. 
            Unauthorized use is prohibited.

***********************************************************/
@isTest
private class RMS_newLaborRedirectExtensionTest {
   

    /*******************************************************
                    redirectTestWithOrder
    *******************************************************/
    static testmethod void redirectTestFromOrder(){
        
        TestUtilityMethods utility = new TestUtilityMethods();
        
        utility.createOrderTestRecords();
        
          Account store = [SELECT id FROM Account WHERE Name='77 - Twin Cities, MN'];    
    Store_Configuration__c storeConfig = [SELECT id FROM Store_Configuration__c WHERE Store__c = :store.id ];
    storeconfig.Allowable_Labor__c = 10;
    Update storeconfig; 

        
        Order soldOrder = [SELECT Id
                           FROM Order 
                           WHERE Name = 'Sold Order 1' and  Store_Location__c =:store.id];
    

        WorkOrder wo = new WorkOrder();
        wo.Sold_Order__c = soldOrder.Id;
        insert wo;

        Labor__c l = new Labor__c();
        l.Related_FSL_Work_Order__c = wo.Id;
        

        Test.startTest();
        Test.setMock(WebServiceMock.class, new ToolingAPIWSDLMock.MockQuery());

        PageReference pageRef = Page.RMS_prodConfigRedirect;
        Test.setCurrentPage(pageRef);
        
         PageReference page = Page.RMS_LaborCustomEdit;
        Test.setCurrentPage(page);

        ApexPages.StandardController stdController = new ApexPages.StandardController(l);
        RMS_newLaborRedirectExtension customController  = new RMS_newLaborRedirectExtension(stdController);
                
        customController.redirect();
        Test.stopTest();

        System.assertEquals('/apex/rms_prodconfigredirect', pageRef.getUrl());

    }
    
    /*******************************************************
                    redirectTestWithoutWorkOrder
    *******************************************************/
    static testmethod void redirectTestWithoutWorkOrder(){
        Test.setMock(WebServiceMock.class, new ToolingAPIWSDLMock.MockQuery());

        TestUtilityMethods utility = new TestUtilityMethods();
        
        utility.createOrderTestRecords();

        Test.startTest();
        Labor__c l = new Labor__c();

        PageReference pageRef = Page.RMS_prodConfigRedirect;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdController = new ApexPages.StandardController(l);
        RMS_newLaborRedirectExtension customController  = new RMS_newLaborRedirectExtension(stdController);
        
        // test with store location
        customController.testStoreLocation = '77 - Twin Cities, MN';                
        customController.redirect();

        // test with null store location
        customController.testStoreLocation = null;              
        customController.redirect();
        Test.stopTest();

        System.assertEquals('/apex/rms_prodconfigredirect', pageRef.getUrl());

    }
    /*******************************************************
                    CustomEditController
    *******************************************************/    
   static testmethod void customeditcontroller(){
   
   Test.startTest();
   PageReference pageRef = Page.RMS_LaborCustomEdit;
        
   TestUtilityMethods utility = new TestUtilityMethods();
   utility.createOrderTestRecords();
   Account store = [SELECT id FROM Account WHERE Name='77 - Twin Cities, MN'];    
   Store_Configuration__c storeConfig = [SELECT id FROM Store_Configuration__c WHERE Store__c = :store.id ];
   storeconfig.Allowable_Labor__c = 10;
   Update storeconfig; 

   Order soldOrder = [SELECT Id FROM Order WHERE Name = 'Sold Order 1' and  Store_Location__c =:store.id];
    
   WorkOrder wo = new WorkOrder();
   wo.Sold_Order__c = soldOrder.Id;
   insert wo;

   Labor__c l = new Labor__c();
   l.Related_FSL_Work_Order__c = wo.Id;
        
   Id RecordTypeId = Schema.SObjectType.Labor__c.getRecordTypeInfosByName().get('Labor').getRecordTypeId();

   Test.setCurrentPage(pageRef);
   pageRef.getParameters().put('RecordType',RecordTypeId);
   pageRef.getParameters().put('StoreLocation',store.Id);
   pageRef.getParameters().put('RelatedWorkOrder',wo.id);

   ApexPages.StandardController sc = new ApexPages.standardController(l);
   RMS_LaborCustomEditController  controller = new RMS_LaborCustomEditController(sc);
   controller.save();
   controller.savenew();
   controller.popUp();
   controller.dosavenew();
   //controller.closePopup();
   
   test.stopTest();

    }
    
    /*******************************************************
                    CustomRedirectController
    *******************************************************/   
    /*
    static testmethod void customeditredirectcontroller(){
    
    TestUtilityMethods utility = new TestUtilityMethods();
    utility.createOrderTestRecords();
    Account store = [SELECT id FROM Account WHERE Name='77 - Twin Cities, MN'];    
    Store_Configuration__c storeConfig = [SELECT id FROM Store_Configuration__c WHERE Store__c = :store.id ];
    storeconfig.Allowable_Labor__c = 10;
    Update storeconfig; 

    Order soldOrder = [SELECT Id FROM Order WHERE Name = 'Sold Order 1' and  Store_Location__c =:store.id];
    
    WorkOrder wo = new WorkOrder();
    wo.Sold_Order__c = soldOrder.Id;
    insert wo;

    Labor__c lo = new Labor__c();
    lo.RecordTypeId = Schema.SObjectType.Labor__c.getRecordTypeInfosByName().get('Labor').getRecordTypeId();
    lo.Related_FSL_Work_Order__c = wo.Id;
    lo.Store_Location__c = store.Id;
    insert lo;

    Test.setCurrentPageReference(new PageReference('Page.RMS_EditLaborRedirect'));
    System.currentPageReference().getParameters().put('Id', lo.Id);

    ApexPages.StandardController sc = new ApexPages.standardController(lo);
    RMS_LaborEditRedirectController controller = new RMS_LaborEditRedirectController(sc);       
    controller.getRedir();
    }*/
}