/**
 * @author Micah Johnson - Demand Chain (Updated 2020)
 * @description Trigger Framework for the ServiceAppointment object (Note: Micah J updated logic for the dispatchStatusCheck Utility)
 * @param The parameters passed in are the old and new maps of the ServiceAppointment that was inserted/updated 
 * @returns N/A
 **/

public without sharing class ServiceAppointmentTriggerHandler {

    public static Boolean scheduledViaCustomComponent = false;

    // If an appointment is being scheduled or rescheduled, sets the Scheduled_Via__c picklist for tracking purposes
    public static void setScheduledViaOnScheduling(List<ServiceAppointment> serviceAppts, Map<Id, ServiceAppointment> oldMap) {
        if (oldMap == null) {oldMap = new Map<Id, ServiceAppointment>();}
        for (ServiceAppointment sa : serviceAppts) {

            // Define "scheduling" as a non-null start time on insert, a changed start time, 
            //     a changed duration, or a changed primary resource (ignore unscheduling/deletion)
            if (sa.SchedStartTime != null 
                && (oldMap.get(sa.Id) == null
                    || sa.SchedStartTime != oldMap.get(sa.Id).SchedStartTime
                    || sa.Duration != oldMap.get(sa.Id).Duration
                    || (oldMap.get(sa.Id).Primary_Service_Resource__c != null && sa.Primary_Service_Resource__c != null 
                        && sa.Primary_Service_Resource__c != oldMap.get(sa.Id).Primary_Service_Resource__c)
                    )
                ) {

                // If the scheduling occurred by a means other than the custom components, pull the value from FSL__Schedule_Mode__c
                if (!scheduledViaCustomComponent && sa.FSL__Schedule_Mode__c != null
                    && (!sa.ARs_Scheduled_Via_Custom_Component__c || (oldMap.get(sa.Id) != null && sa.SchedStartTime != oldMap.get(sa.Id).SchedStartTime))) {
                    switch on sa.FSL__Schedule_Mode__c{
                        when 'Manual' {
                            sa.Scheduled_Via__c = 'FSL - Manual';
                        }
                        when 'Automatic' {
                            sa.Scheduled_Via__c = 'FSL - Automatic';
                        }
                        when 'Optimization' {
                            sa.Scheduled_Via__c = 'FSL - Optimization';
                        }
                        when 'None' {
                            sa.Scheduled_Via__c = 'None';
                        }
                    }
                }
            }
        }
    }

    public static void serviceAppointmentUpdateIcon(List<ServiceAppointment> serviceAppts, Map<Id, ServiceAppointment> oldMap){

        // Grab a set of Work Order Ids related to the new Service Appointments.
        Set<Id> workOrderIdSet = new Set<Id>();
        Map<Id,ServiceAppointment> eligibleServiceAppointmentMap = new Map<Id,ServiceAppointment>();
        for (ServiceAppointment sa : serviceAppts) {
            if (sa.Work_Order__c != null) {
                workOrderIdSet.add(sa.Work_Order__c);
                eligibleServiceAppointmentMap.put(sa.Id,sa);
            }
        }

        // Grab a Map of Work Orders related to the new Service Appointment records.
        Map<Id,WorkOrder> workOrderMap = new Map<Id,WorkOrder>([SELECT Id, Work_Order_Type__c FROM WorkOrder WHERE Id IN :workOrderIdSet]);

        for (ServiceAppointment serviceAppt : eligibleServiceAppointmentMap.values()) {

            if (workOrderMap.containsKey(serviceAppt.Work_Order__c)) {
                WorkOrder relatedWorkOrder = workOrderMap.get(serviceAppt.Work_Order__c);
                String workOrderType = relatedWorkOrder.Work_Order_Type__c;
                if(serviceAppt.Status != 'Completed' && serviceAppt.Status != 'Canceled'){
                    switch on workOrderType{
                        when 'Service'{
                            serviceAppt.FSL__GanttColor__c = '#e34317';
                            String resourceURL = PageReference.forResource('FSL_Icon_Service').getURL();
                            if(resourceURL != null){
                                if(resourceURL.contains('?')){
                                    resourceURL = resourceURL.substring(0, resourceURL.indexOf('?'));
                                    serviceAppt.FSL__GanttIcon__c = resourceURL;    
                                }else{
                                    serviceAppt.FSL__GanttIcon__c = resourceURL;
                                }  
                            }
                        }
                        when 'Install'{
                            if(serviceAppt.SoldOrderType__C == true ){
                                serviceAppt.FSL__GanttColor__c = '#24b6f0';
                            } else {
                                serviceAppt.FSL__GanttColor__c = '#1e35ba';   
                            }
                            String resourceURL = PageReference.forResource('FSL_Icon_Install').getURL();
                            if(resourceURL != null){
                                if(resourceURL.contains('?')){
                                    resourceURL = resourceURL.substring(0, resourceURL.indexOf('?'));
                                    serviceAppt.FSL__GanttIcon__c = resourceURL;    
                                }else{
                                serviceAppt.FSL__GanttIcon__c = resourceURL;
                                }  
                            }
                        }
                        when 'Tech Measure'{
                        serviceAppt.FSL__GanttColor__c = '#12e042';
                            String resourceURL = PageReference.forResource('FSL_Icon_TechMeasure').getURL();
                            if(resourceURL != null){
                                if(resourceURL.contains('?')){
                                    resourceURL = resourceURL.substring(0, resourceURL.indexOf('?'));
                                    serviceAppt.FSL__GanttIcon__c = resourceURL;    
                                }else{
                                    serviceAppt.FSL__GanttIcon__c = resourceURL;
                                }  
                            }
                        }
                        when 'Job Site Visit'{
                            serviceAppt.FSL__GanttColor__c = '#9808eb';
                            String resourceURL = PageReference.forResource('FSL_Icon_JobSiteVisit').getURL();
                            if(resourceURL != null){
                                if(resourceURL.contains('?')){
                                    resourceURL = resourceURL.substring(0, resourceURL.indexOf('?'));
                                    serviceAppt.FSL__GanttIcon__c = resourceURL;    
                                }else{
                                    serviceAppt.FSL__GanttIcon__c = resourceURL;
                                }  
                            }
                        }
                        when null{
                            System.debug('ServiceAppointmentUpdateIcon: null work order type');
                        }
                        when else{
                            System.debug('ServiceAppointmentUpdateIcon: Default Icon');
                        }
                    }
                } else{
                    serviceAppt.FSL__GanttColor__c = '';
                    system.debug(serviceAppt.FSL__GanttColor__c);
                }
           }
        }
    }
    public static void calculateEndTimeFromDuration(List<ServiceAppointment> serviceAppts, Map<Id, ServiceAppointment> oldMap) {
        for(ServiceAppointment serviceAppt : serviceAppts) {
            if(!serviceAppt.FSL__IsMultiDay__c && serviceAppt.Duration != null && serviceAppt.SchedStartTime != null) {
                if(serviceAppt.DurationType == 'Minutes') {
                    serviceAppt.SchedEndTime = serviceAppt.SchedStartTime.addMinutes(Integer.valueOf(serviceAppt.Duration));
                } else {
                    serviceAppt.SchedEndTime = serviceAppt.SchedStartTime.addMinutes(Integer.valueOf(serviceAppt.Duration * 60));
                }
            }
        }
    }

    public static void populateWorkOrderField(List<ServiceAppointment> serviceAppts) {
        for(ServiceAppointment serviceAppt : serviceAppts) {
            if(serviceAppt.ParentRecordId != null) {
                serviceAppt.Work_Order__c = serviceAppt.ParentRecordId;
            }
        }
    }

    public static void populateAddressFields(List<ServiceAppointment> serviceAppts, Map<Id, ServiceAppointment> oldMap) {
        Map<Id, List<ServiceAppointment>> woIdsToServiceAppts = new Map<Id, List<ServiceAppointment>>();
        for(ServiceAppointment serviceAppt : serviceAppts) {
            if(serviceAppt.Work_Order__c != null) {
                if(Trigger.isInsert
                        || serviceAppt.Work_Order__c != oldMap.get(serviceAppt.Id).Work_Order__c) {
                    if(!woIdsToServiceAppts.containsKey(serviceAppt.Work_Order__c)) {
                        woIdsToServiceAppts.put(serviceAppt.Work_Order__c, new List<ServiceAppointment> {serviceAppt});
                    } else {
                        List<ServiceAppointment> tempServiceAppts = woIdsToServiceAppts.get(serviceAppt.Work_Order__c);
                        tempServiceAppts.add(serviceAppt);
                        woIdsToServiceAppts.put(serviceAppt.Work_Order__c, tempServiceAppts);
                    }
                }
            }
        }

        for(WorkOrder wo : [SELECT Id,
                                   AccountId,
                                   Account.ShippingCity,
                                   Account.ShippingPostalCode,
                                   Account.ShippingState,
                                   Account.ShippingStreet,
                                   Account.ShippingCountry,
                                   Account.ShippingCountryCode
                            FROM WorkOrder
                            WHERE Id IN :woIdsToServiceAppts.keySet()]) {
            if(wo.AccountId != null) {
                for(ServiceAppointment serviceAppt : woIdsToServiceAppts.get(wo.Id)) {
                    serviceAppt.City = wo.Account.ShippingCity;
                    serviceAppt.PostalCode = wo.Account.ShippingPostalCode;
                    serviceAppt.State = wo.Account.ShippingState;
                    serviceAppt.Street = wo.Account.ShippingStreet;
                    serviceAppt.Country = wo.Account.ShippingCountry;
                    serviceAppt.CountryCode = wo.Account.ShippingCountryCode;
                }
            }
        }
    }

    public static void populateGanttLabel(List<ServiceAppointment> serviceAppts, Map<Id, ServiceAppointment> oldMap) {

        Set<Id> workOrderIds = new Set<Id>();
        for(ServiceAppointment serviceAppt : serviceAppts) {
            if (serviceAppt.Work_Order__c != null) {
                workOrderIds.add(serviceAppt.Work_Order__c);
            }
        }

        Map<Id, WorkOrder> workOrderIdsToWorkOrderContact = new Map<Id, WorkOrder>([SELECT Id, ContactId FROM WorkOrder WHERE Id IN :workOrderIds]);

        Map<Id, List<ServiceAppointment>> conIdsToServiceAppts = new Map<Id, List<ServiceAppointment>>();
        for(ServiceAppointment serviceAppt : serviceAppts) {
            if(Trigger.isInsert
                    || serviceAppt.ContactId != oldMap.get(serviceAppt.Id).ContactId
                    || serviceAppt.City != oldMap.get(serviceAppt.Id).City
                    || (serviceAppt.ContactId != null && serviceAppt.City != null && serviceAppt.FSL__GanttLabel__c == null)) {
                
                // set the contact Id
                if (serviceAppt.Work_Order__c != null 
                    && serviceAppt.ContactId == null 
                    && workOrderIdsToWorkOrderContact.containsKey(serviceAppt.Work_Order__c)) {
                    serviceAppt.ContactId = workOrderIdsToWorkOrderContact.get(serviceAppt.Work_Order__c).ContactId;
                }
                    
                if(serviceAppt.ContactId != null && serviceAppt.City != null) {
                    if(!conIdsToServiceAppts.containsKey(serviceAppt.ContactId)) {
                        conIdsToServiceAppts.put(serviceAppt.ContactId, new List<ServiceAppointment> {serviceAppt});
                    } else {
                        List<ServiceAppointment> tempServiceAppts = conIdsToServiceAppts.get(serviceAppt.ContactId);
                        tempServiceAppts.add(serviceAppt);
                        conIdsToServiceAppts.put(serviceAppt.ContactId, tempServiceAppts);
                    }
                }
            }
        }

        for(Contact con : [SELECT Id,
                                  LastName
                           FROM Contact
                           WHERE Id IN :conIdsToServiceAppts.keySet()]) {
            for(ServiceAppointment serviceAppt : conIdsToServiceAppts.get(con.Id)) {
                serviceAppt.FSL__GanttLabel__c = con.LastName + '-' + serviceAppt.City;
            }
        }
    }


    // Added by Penrod 7/26 to replace Service Appointment Service Territory Assignment Process Builder
    // Sets service appointments' ServiceTerritoryId based on the zip-to-serviceTerritory information in custom object Service_Territory_Zip_Code__c
    public static void assignServiceTerritories(List<ServiceAppointment> serviceAppts, Map<Id, ServiceAppointment> oldMap) {

        Set<String> postalCodes = new Set<String>();
        List<ServiceAppointment> apptsForUpdate = new List<ServiceAppointment>();
        Map<String, Id> zipcodeAndWorktypeKeyToServiceTerritory = new Map<String, Id>();

        if(oldMap == null) {
            oldMap = new Map<Id, ServiceAppointment>();
        }
		/*
        Id profileId = userinfo.getProfileId(); 
        String profileName = [Select Id, Name, UserLicense.Name from Profile where Id=:profileId].Name; // TODO: Look for ways to remove this SOQL.
		*/
        // needed to update this profile name query because it was breaking one of Neil's scheduled flows - performed by the automated process user which 
        // as of this time does not have a profile - MTR 8/13/20
        Id profileId = userinfo.getProfileId(); 
        String userName = userinfo.getName();
        String ProfileName = '';
        if(userName != 'Automated Process'){        
            profileName = [Select Id, Name, UserLicense.Name from Profile where Id=:profileId].Name; // TODO: Look for ways to remove this SOQL.
        }
        // if user is any one of these profiles (e.g. 'RMS-Data Migration'), do not set service territories.
        // NOTE: this may cause test to fail - depending on profile of user running test
        if(!(profileName == 'RMS-Data Migration' && profileName != '')) {
            for(ServiceAppointment sa : serviceAppts) {
                
                // Check if the postal code on the service appt changed, 
                // or if the ServiceTerritoryId is undefined but we know the PostalCode.
                if((oldMap.containsKey(sa.Id) && sa.PostalCode!=oldMap.get(sa.Id).PostalCode) || 
                    (sa.ServiceTerritoryId == null && sa.PostalCode != null)) {

                    if ((sa.Country != null && (sa.Country.equalsIgnoreCase('us') || sa.Country.equalsIgnoreCase('usa') || sa.Country.equalsIgnoreCase('united states')
                            || sa.Country.equalsIgnoreCase('u.s.') || sa.Country.equalsIgnoreCase('u.s.a.') || sa.Country.equalsIgnoreCase('united states of america')))
                            || (sa.CountryCode != null && sa.CountryCode.equalsIgnoreCase('us'))) {

                        String zip5Char = sa.PostalCode.substring(0, 5);
                        if(!String.isBlank(zip5Char)) {
                            postalCodes.add(zip5Char);
                            apptsForUpdate.add(sa);
                        }
                    } else if ((sa.Country != null && (sa.Country.equalsIgnoreCase('ca') || sa.Country.equalsIgnoreCase('canada')))
                            || (sa.CountryCode != null && sa.CountryCode.equalsIgnoreCase('ca'))) {
                        String formattedPostalCode = sa.PostalCode.deleteWhitespace().remove('-').toUpperCase();
                        if(!String.isBlank(formattedPostalCode)) {
                            postalCodes.add(formattedPostalCode);
                            apptsForUpdate.add(sa);
                        }
                    }
                }
            } 

            // iterate over all Service_Territory_Zip_Code__c records matching the zip codes from the service appts
            for(Service_Territory_Zip_Code__c serviceTerritoryZipCode : [SELECT Id,
                                                                            Name,
                                                                            Service_Territory__c,
                                                                            WorkType__c
                                                                     FROM Service_Territory_Zip_Code__c
                                                                     WHERE Name IN :postalCodes
                                                                     AND Service_Territory__c != null]) 
            {
                // populate map linking a hybrid key of zip code + worktype to the serviceTerritory Id
                String hybridKey = serviceTerritoryZipCode.Name;
                
                if (serviceTerritoryZipCode.WorkType__c != null) {
                    hybridKey += serviceTerritoryZipCode.WorkType__c; // concat the key only if WorkType__c is not null
                }

                zipcodeAndWorktypeKeyToServiceTerritory.put(hybridKey, serviceTerritoryZipCode.Service_Territory__c);
            }

            // get map of wt Ids to wt objects so we can convert the wt Ids on the service appts to a wt name to match the Service_Territory_Zip_Code__c records
            Map<Id, WorkType> worktypeIdsToWorkTypes = new Map<Id, WorkType>([SELECT Id, Name FROM WorkType]);

            // iterate over the service appts we want to update, and assign the service territory based on the best match to Service_Territory_Zip_Code__c records
            for (ServiceAppointment sa : apptsForUpdate) {

                String hybridKey;
                String formattedPostalCode = '';

                if ((sa.Country != null && (sa.Country.equalsIgnoreCase('us') || sa.Country.equalsIgnoreCase('usa') || sa.Country.equalsIgnoreCase('united states')
                        || sa.Country.equalsIgnoreCase('u.s.') || sa.Country.equalsIgnoreCase('u.s.a.') || sa.Country.equalsIgnoreCase('united states of america')))
                        || (sa.CountryCode != null && sa.CountryCode.equalsIgnoreCase('us'))) {

                    formattedPostalCode = sa.PostalCode.substring(0, 5);
                } else if ((sa.Country != null && (sa.Country.equalsIgnoreCase('ca') || sa.Country.equalsIgnoreCase('canada')))
                        || (sa.CountryCode != null && sa.CountryCode.equalsIgnoreCase('ca'))) {
                    formattedPostalCode = sa.PostalCode.deleteWhitespace().remove('-').toUpperCase();
                }

                // first: try to find a matching service territory based on both zip code and work type
                if (sa.WorkTypeId != null) {
                    hybridKey = formattedPostalCode + worktypeIdsToWorkTypes.get(sa.WorkTypeId).Name;
                    if (zipcodeAndWorktypeKeyToServiceTerritory.containsKey(hybridKey)) {
                        sa.ServiceTerritoryId = zipcodeAndWorktypeKeyToServiceTerritory.get(hybridKey);
                        continue; 
                    }
                }

                // then: try to find a matching service territory based on zip code and a work type of 'Other'
                hybridKey = formattedPostalCode + 'Other';
                if (zipcodeAndWorktypeKeyToServiceTerritory.containsKey(hybridKey)) {
                    sa.ServiceTerritoryId = zipcodeAndWorktypeKeyToServiceTerritory.get(hybridKey);
                    continue; 
                }

                // finally: try to find a matching service territory based on zip code and an empty (null) work type
                hybridKey = formattedPostalCode;
                if (zipcodeAndWorktypeKeyToServiceTerritory.containsKey(hybridKey)) {
                    sa.ServiceTerritoryId = zipcodeAndWorktypeKeyToServiceTerritory.get(hybridKey);
                    continue; 
                }

                // At this point, we couldn't find a matching Service_Territory_Zip_Code__c record for this service appt,
                // and so the service appt's service territory remains un-set.
            }
        }
    }
    
     private static Map<String, RecordType> createWorkOrderRecordTypeMap() {
        List<RecordType> woRecordTypes = [select Id, DeveloperName from RecordType where sObjectType = 'WorkOrder'];
        Map<String, RecordType> woRecordTypeMap = new Map<String, RecordType>();
        for (RecordType rt : woRecordTypes) {
            woRecordTypeMap.put(rt.DeveloperName, rt);
        }
        return woRecordTypeMap;
    }
    private static final Map<String, RecordType> woRecordTypeMap = createWorkOrderRecordTypeMap();
    
    /*public static void assignServiceResourceTOJobSiteVisit(Map<Id, ServiceAppointment> newMap, Map<Id, ServiceAppointment> oldMap){
        
        String JobSiteVistId = woRecordTypeMap.get('Job_Site_Visit').Id;
        Set<Id> workOrderIdSet = new Set<ID>();
        list<WorkOrder> UpdateWOList = new list<WorkOrder>();
        Map<id,list<ServiceAppointment>> WOToRelatedSerAppMap = new Map <ID, list<ServiceAppointment>>();
        Map<id,WorkOrder> WorkOrderMap = new Map<id,WorkOrder>();
        for( ServiceAppointment SerApp : NewMap.values()){        
            if(SerApp.ParentRecordid != Null ){
                workOrderIdSet.add(SerApp.ParentRecordid);
            }
        }
        
        for(WorkOrder ParRec : [Select id ,Primary_Visitor__c, RecordTypeid , 
                                (Select id,Primary_Service_Resource__c from Service_Appointments__r) from WorkOrder 
                                WHERE id IN: workOrderIdSet AND RecordTypeid =: JobSiteVistId]){
                                    
                                    WorkOrderMap.put(ParRec.id, ParRec);
                                    WOToRelatedSerAppMap.put(ParRec.id, ParRec.Service_Appointments__r);
        
                                } 
        for(ServiceAppointment SerApp : NewMap.values()){
               
            if(WOToRelatedSerAppMap.containsKey(SerApp.ParentRecordid) && WOToRelatedSerAppMap.get(SerApp.ParentRecordid).size() == 1){
                if( WorkOrderMap.get(SerApp.ParentRecordid).Primary_Visitor__c != SerApp.Primary_Service_Resource__c){
                    WorkOrder wo = WorkOrderMap.get(SerApp.ParentRecordid);
                    wo.Primary_Visitor__c = SerApp.Primary_Service_Resource__c ;
                    UpdateWOList.add(wo);
                }
                
            }  else{
                if(WOToRelatedSerAppMap.containsKey(SerApp.ParentRecordid) && WorkOrderMap.get(SerApp.ParentRecordid).Primary_Visitor__c != null){
                    WorkOrder wo = WorkOrderMap.get(SerApp.ParentRecordid);
                    wo.Primary_Visitor__c = null ;
                    UpdateWOList.add(wo);
                }
            }
         } 
        if(!UpdateWOList.isEmpty())
            Update UpdateWOList;
    }  
*/
 /* When Appointment is schedule we are populating Appointment Scheduled on to todays timestamp.
     * This can be accomplised by WFR, but considering 101 SOQL issue on firing DLRS this is written in trigger.
     */
    public static void whenAppointmentScheduled(Map<Id, ServiceAppointment> newMap, Map<Id, ServiceAppointment> oldMap){
        
        for(ServiceAppointment sa: newMap.values()){
            
        //    Commented out MTR 10-22-20 replaced with lines below. change is for Pavans USer suspended code which has not been properly tested.
        //    if(sa.Status == 'Scheduled' && sa.SchedStartTime != Null && sa.SchedEndTime != Null &&
        //       sa.Status != oldMap.get(sa.id).Status && (oldMap.get(sa.id).Status =='None' || oldMap.get(sa.id).Status =='User Suspended') ){            
            
            if(sa.Status == 'Scheduled' && sa.SchedStartTime != Null && sa.SchedEndTime != Null &&
               sa.Status != oldMap.get(sa.id).Status && oldMap.get(sa.id).Status =='None'  ){
                   
            sa.Appointment_Scheduled_On__c =  system.today();                 
                
            }            
        }
    }

    public static void dispatchStatusCheck(Map<Id, ServiceAppointment> newMap, Map<Id, ServiceAppointment> oldMap){
        system.debug(newMap);
        system.debug(oldMap);
        UtilityMethods.disableAllFslTriggers();
        Map<String,Id> workTypeMap = new Map<String,Id>();
        for (WorkType wt : [SELECT Id, Name FROM WorkType ]) {
            workTypeMap.put(wt.Name,wt.Id);
        }
        Map<Id, ServiceAppointment> TechMeasureSAMap = new Map<Id, ServiceAppointment>();
        Map<Id, ServiceAppointment> InstallSAMap = new Map<Id, ServiceAppointment>();
        Map<Id, ServiceAppointment> OtherSAMap = new Map<Id, ServiceAppointment>();
        Map<Id, ServiceAppointment> ServiceSAMap = new Map<Id, ServiceAppointment>();
        Map<Id, ServiceAppointment> JobSiteSAMap = new Map<Id, ServiceAppointment>();
        for(ServiceAppointment v: newMap.values()){
            if( v.WorkTypeId == workTypeMap.get('Measure') ){
                TechMeasureSAMap.put(v.Id, v);
            }
            else if( v.WorkTypeId == workTypeMap.get('Install')){
                InstallSAMap.put(v.Id, v);
            }
            else if(v.WorkTypeId == workTypeMap.get('Service')){
                ServiceSAMap.put(v.Id, v);
            }
            else if(v.WorkTypeId == workTypeMap.get('Job Site Visit')){
                JobSiteSAMap.put(v.Id, v);
            }
            else{
                OtherSAMap.put(v.Id, v);
            }
        }
        if(!TechMeasureSAMap.isEmpty()){
            System.enqueueJob(new StatusCheckQue(TechMeasureSAMap, null, 'ServiceAppointment'));
            // if(!Test.isRunningTest()){
                System.debug('TechMeasureSAMap size '+ TechMeasureSAMap.values().size());
                //System.enqueueJob(new DynamicRollUpQue(TechMeasureSAMap.values()));
            // }
        }
        if(!InstallSAMap.isEmpty()){
            System.enqueueJob(new StatusCheckQue(InstallSAMap, null, 'InstallAppointment'));
            // if(!Test.isRunningTest()){
                //System.enqueueJob(new DynamicRollUpQue(InstallSAMap.values()));
            // }
        }
        if(!ServiceSAMap.isEmpty()){
            System.enqueueJob(new StatusCheckQue(ServiceSAMap, null, 'ServiceTypeAppointment'));
            // StatusCheckUtility.ServiceTypeAppointmentToWorkOrderStatusCheck(ServiceSAMap, null);
            // if(!Test.isRunningTest()){
                //System.enqueueJob(new DynamicRollUpQue(ServiceSAMap.values()));
            // }
        }
        if(!JobSiteSAMap.isEmpty()){
            // System.enqueueJob(new StatusCheckQue(ServiceSAMap, null, 'JobSiteAppointment'));
            StatusCheckUtility.JobSiteTypeAppointmentToWorkOrderStatusCheck(JobSiteSAMap, null);
            // if(!Test.isRunningTest()){
                //System.enqueueJob(new DynamicRollUpQue(JobSiteSAMap.values()));
            // }
        }
        if(!OtherSAMap.isEmpty()){
            // if(!Test.isRunningTest()){
                System.enqueueJob(new DynamicRollUpQue(OtherSAMap.values()));
            // }
        }
    }
/*  Commented out - MTR - 10-22-20 - Pavans user suspended code. has not been properly tested yet.
    public static void UpdateToUserSuspended (Map<Id, ServiceAppointment> newMap, Map<Id, ServiceAppointment> oldMap){
        
        for(ServiceAppointment sa: newMap.values()){
            
            if(sa.Status != oldMap.get(sa.id).Status && sa.Status == 'None' && oldMap.get(sa.id).Status =='Scheduled'){
                   
            sa.Status ='User Suspended';                 
                
            }            
        }
    }
     public static void whenUserSuspended (Map<Id, ServiceAppointment> newMap, Map<Id, ServiceAppointment> oldMap){
        
        for(ServiceAppointment sa: newMap.values()){
            
            if(sa.Status != oldMap.get(sa.id).Status && (sa.Status == 'None'||sa.Status =='User Suspended') && oldMap.get(sa.id).Status =='Scheduled' && sa.Primary_Service_Resource__c !=null ){
                   
            sa.Primary_Service_Resource__c =  null;                 
                
            }            
        }
    }
*/

}