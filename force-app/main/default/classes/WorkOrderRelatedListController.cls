public with sharing class WorkOrderRelatedListController {
    
    @AuraEnabled(cacheable=true)
    public static List<WorkOrderLineItemWrapper> getWorkOrderLineItems(String workOrderId){
        List<WorkOrderLineItemWrapper> workOrderLineItemWrapList = new List<WorkOrderLineItemWrapper>();
        for( WorkOrderLineItem wolItem : [SELECT Id,
            List_Index__c,
            LineItemNumber,
            Subject,
            Description,
            Job_Notes__c,
            Status,
            WorkOrderId,
            Visibility__c,
            Line_Item_Type__c
            FROM WorkOrderLineItem
            WHERE WorkOrderId = :workOrderId
            ORDER BY List_Index__c ASC NULLS LAST
            ]){
                WorkOrderLineItemWrapper workOrderLineItemWrap = new WorkOrderLineItemWrapper(wolItem);
                workOrderLineItemWrapList.add(workOrderLineItemWrap);
            }
        return workOrderLineItemWrapList;
    }


    @AuraEnabled(cacheable=true)
    public static List<ContentDocWrapper> getAttachments(String workOrderId){
        List<ContentDocWrapper>wraps = new List<ContentDocWrapper>();
        for (ContentDocumentLink contentDoc  : [Select id, LinkedEntityId,
                                                ContentDocumentId,
                                                ContentDocument.Title,
                                                ContentDocument.LastModifiedDate,
                                                ContentDocument.CreatedBy.Name,
                                                ContentDocument.CreatedDate FROM 
                                                ContentDocumentLink WHERE 
                                                LinkedEntityId = : workOrderId]) {
            ContentDocWrapper cw = new ContentDocWrapper(contentDoc);
            wraps.add(cw);
        }
        return wraps;  
    }

    public class ContentDocWrapper{
        @AuraEnabled
        public String title{get;set;}
        @AuraEnabled
        public String titleUrl{get;set;}
        @AuraEnabled
        public string createdBy{get;set;}
        @AuraEnabled
        public Datetime createdDate{get;set;}
        @AuraEnabled
        public Datetime LastModifiedDate{get;set;}
        @AuraEnabled
        public String contentdocId{get;set;}

        public ContentDocWrapper(ContentDocumentLink contentLink){
            this.title = contentLink.ContentDocument.Title;
            this.titleUrl = '/'+contentLink.ContentDocumentId;
            this.createdDate = contentLink.ContentDocument.CreatedDate;
            this.LastModifiedDate = contentLink.ContentDocument.LastModifiedDate;
            this.contentdocId = contentLink.ContentDocumentId;
            this.createdBy = contentLink.ContentDocument.CreatedBy.Name;
        }
    }

    public class WorkOrderLineItemWrapper{
        @AuraEnabled
        public String subject{get;set;}
        @AuraEnabled
        public String officeNotes{get;set;}
        @AuraEnabled
        public String jobNotes{get;set;}
        @AuraEnabled
        public String customerFacing{get;set;}
        @AuraEnabled
        public String LineType{get;set;} 
        @AuraEnabled
        public String status{get;set;}     
        
        public WorkOrderLineItemWrapper(WorkOrderLineItem orderRec){
            subject = orderRec.Subject;
            status= orderRec.Status;
            officeNotes = orderRec.Description;
            jobNotes = orderRec.Job_Notes__c;
            customerFacing = orderRec.Visibility__c;
            LineType = orderRec.Line_Item_Type__c;
        }
    }
}