/**
* @author: Jason Flippen
* @description: Provides database functionality for the wholesalesPricebookClone LWC.
* @test class: WholesalePricebookCloneControllerTest
*/ 
public with sharing class WholesalePricebookCloneController {

    /*
    * @author: Jason Flippen
    * @description: Retrieves the List of Pricing Configuration records
    *               related to a Wholesale Pricebook.
    * @params: Id of the Wholesale Pricebook.
    * @returns: List of related Pricing Configuration records.
    */ 
    @AuraEnabled
    public static List<Pricing_Configuration__c> getRelatedPricingConfigurations(Id wholesalePricebookId) {

        // Grab the List of Pricing Configurations fields to be included in the Clone.
        Map<String,Schema.SObjectField> fieldMap = Pricing_Configuration__c.getSObjectType().getDescribe().fields.getMap(); 
        List<String> fieldNameList = new List<String>();
        for (String fieldName : fieldMap.keySet()) {
            fieldNameList.add(fieldName);
        }

        // Build the query string and include the List of Pricing Configuration fields.        
        String queryString = 'SELECT ' + String.join(fieldNameList, ',') +
                            ' FROM Pricing_Configuration__c' +
                            ' WHERE Wholesale_Pricebook__c = :wholesalePricebookId';
        
        List<Pricing_Configuration__c> returnList = Database.query(queryString);

        return returnList;

    } // end Method getRelatedPricingConfigurations()

    /**
    * @author: Jason Flippen
    * @description: Inserts new Wholesale Pricebook with or without Pricing
    *               Configurations from original Wholesale Pricebook.
    * @params: Name, Status and Effective Date of new Wholesale Pricebook, Flag to
    *          indicate whether or not to clone the Pricing Configuration records,
    *          and List of Pricing Configuration records to be cloned.
    * @returns: Result of the update. Id of new Wholesale Pricebook if
    *           successful, empty string if unsuccessful.
    */ 
    @AuraEnabled
    public static String saveWholesalePricebook(Wholesale_Pricebook__c newWholesalePricebook,
                                                Boolean includePricingConfigurations,
                                                List<Pricing_Configuration__c> pricingConfigurationList) {

        String returnValue = '';

        Savepoint savePoint = Database.setSavepoint();

        try {

            Database.SaveResult saveResult = Database.insert(newWholesalePricebook);
            if (saveResult.isSuccess()) {

                // Operation was successful, so get the ID of the record that was processed
                Id newWholesalePricebookId = saveResult.getId();
                
                // Do we need to clone the Pricing Configuration records
                // related to the original Wholesale Pricebook?
                if (includePricingConfigurations == true && !pricingConfigurationList.isEmpty()) {
                    Boolean cloneSuccess = clonePricingConfigurations(newWholesalePricebookId, pricingConfigurationList);
                    if (cloneSuccess == false) {
                        newWholesalePricebookId = null;
                        Database.rollback(savePoint);
                    }
                }

                returnValue = newWholesalePricebookId;

            }
        
        }
        catch (Exception ex) {
            System.debug('WholesalePricebookCloneController.saveWholesalePricebook Exception: ' + ex.getMessage());
            Database.rollback(savePoint);
        }

        return returnValue;
        
    } // End Method: saveWholesalePricebook()

    /**
    * @author: Jason Flippen
    * @description: Clone's Pricing Configuration records related to original Wholesale
    *               Pricebook and adds them to the new Wholesale Pricebook record.
    * @params: New Wholesale Pricebook Id & List of Pricing Configuration records.
    * @returns: N/A
    */ 
    private static Boolean clonePricingConfigurations(Id newWholesalePricebookId,
                                                      List<Pricing_Configuration__c> pricingConfigurationList) {

        Boolean returnValue = false;

        try {

            // Clone the Pricing Configuration records and associate
            // them to the new Wholesale Pricebook record.            
            List<Pricing_Configuration__c> newPCList = new List<Pricing_Configuration__c>();
            for (Pricing_Configuration__c pc : pricingConfigurationList) {
                Pricing_Configuration__c newPC = pc.clone(false, false, false, false);
                newPC.Wholesale_Pricebook__c = newWholesalePricebookId;
                newPC.Cloned_Pricing_Configuration__c = pc.Id;
                newPCList.add(newPC);
            }

            // If we have records to insert, insert them.
            if (!newPCList.isEmpty()) {
                insert newPCList;
                returnValue = true;
            }

        }
        catch (Exception ex) {
            System.debug('WholesalePricebookCloneController.clonePricingConfigurations Exception: ' + ex.getMessage());
            returnValue = false;
        }

        return returnValue;

    } // End Method: clonePricingConfigurations()

}