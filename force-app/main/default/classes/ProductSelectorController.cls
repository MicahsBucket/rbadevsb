public with sharing class ProductSelectorController {
    
    @AuraEnabled(cacheable=true)
    public static List<Product2> findProducts(String searchTerm, Boolean inRop){
        System.debug('in rop? ' + inRop);
        if(String.isBlank(searchTerm)){
            return new List<Product2>();
        }
        searchTerm = '%' + searchTerm.trim() + '%';
        if(inRop == true){
            return [SELECT Id, Name_Part_Number__c,ProductCode  
                    FROM Product2
                    WHERE Inventoried_Item__c = true 
                    AND IsActive = true 
                    AND Vendor__r.Name = 'Renewal by Andersen'
                    AND (RecordType.DeveloperName = 'Master_Product'
                    OR RecordType.DeveloperName ='Door_Components' 
                    OR RecordType.DeveloperName ='Legacy_Service'
                    OR RecordType.DeveloperName ='Parts_and_Accessories')
                    AND Name_Part_Number__c LIKE :searchTerm
                    LIMIT 20];
        }
        return [SELECT Id, Name_Part_Number__c,ProductCode  
                FROM Product2 
                WHERE Inventoried_Item__c = true 
                AND IsActive = true 
                AND Name_Part_Number__c LIKE :searchTerm
                LIMIT 20];
    }
}