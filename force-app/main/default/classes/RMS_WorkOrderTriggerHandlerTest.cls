@isTest
private class RMS_WorkOrderTriggerHandlerTest {
      /*  
			Matt Azlin commenting out 5/16 -- this method works with RbA_Work_Order__c.  no longer being used post FSL.
    @isTest static void OrderStatusReadyToOrderUpdated() {

		System.debug('In Testmethod OrderStatusReadyToOrderUpdated');
		TestUtilityMethods utility = new TestUtilityMethods();
		
		utility.createOrderTestRecords();
		

		test.startTest();
		Order soldOrder = [SELECT Id, Name, Status, AccountId FROM Order WHERE Name = 'Sold Order 1'];
		Account store = [SELECT Id, Active_Store_Configuration__c from Account Where Name = '77 - Twin Cities, MN'];
		System.assertEquals('Draft', soldOrder.Status);

		Id serviceOrderVisitRecordTypeId = UtilityMethods.retrieveRecordTypeId('Visit', 'RbA_Work_Order__c');
		RbA_Work_Order__c visitWorkOrder = new RbA_Work_Order__c(  	RecordTypeId = serviceOrderVisitRecordTypeId, 
													                Sold_Order__c = soldOrder.Id,
													                Work_Order_Type__c = 'Tech Measure',
													                Account__c = soldOrder.AccountId  
												                    );
    	insert visitWorkOrder;
		System.debug('visitWorkOrder inserted');

    	System.assertEquals(UtilityMethods.completeWorkOrders(soldOrder.Id), 'success');
    	visitWorkOrder = [Select Work_Order_Status__c from RbA_Work_Order__c where Id =: visitWorkOrder.Id];
		System.assertEquals(visitWorkOrder.Work_Order_Status__c,'Appt Complete / Closed');
		Order soldOrder1 = [SELECT Id, Name, Status, AccountId, Time_Ready_To_Order__c FROM Order WHERE Name = 'Sold Order 1'];
		//System.debug([SELECT Id, Name, Status, AccountId FROM Order WHERE Name = 'Sold Order 1']);
    // @MitchSpano 4/20/18
    //System.assertNotEquals(null, soldOrder1.Time_Ready_To_Order__c);
    //System.assertEquals('Ready to Order', soldOrder1.Status);

		test.stopTest();
    }
		*/
}