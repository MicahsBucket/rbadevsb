public without sharing class cancelOrderButtonController {
    @AuraEnabled
    public static Order getOrder(Id recordId) {
        System.debug(recordId);
        Order o = [SELECT Id, Status, Revenue_Recognized_Date__c, Retail_Total__c, Amount_Due__c,Cancellation_Reason__c,
        (SELECT Id FROM Work_Orders__r WHERE Scheduled_Start_Time__c != NULL AND Status NOT IN ('Cancelled','Canceled','Closed','Complete','Appt Complete / Closed') AND Work_Order_Type__c IN ('Tech Measure','Install'))
        FROM Order WHERE Id =: recordId];
        return o;
    }

    /// closeJob method: When "Close Job" button is clicked, the Id of the Order to close is passed into the method
    /// Order Status and Job_Close_Date__c are updated (if no errors) and the returnText is sent back to the Javascript controller
    @AuraEnabled
    public static string cancelOrder(Id OrderId, string Reason){
        System.debug('OrderId in cancelOrderButtonController '+ OrderId);
        System.debug('Reason in cancelOrderButtonController '+ Reason);
        List<SObject> sobjectsToUpdate = new List<SObject>();
        Order o = [SELECT Id, Status, Apex_Context__c, Job_Close_Date__c, Cancellation_Reason__c,RecordTypeName__c,
                    Revenue_Recognized_Date__c, Change_Order_Rev_Recognized_Date__c,
                    (SELECT Id, Status__c FROM OrderItems),
                    (SELECT Id, Status, Cancel_Reason__c FROM Work_Orders__r),
                    (SELECT Id, Status FROM Tasks),
                    (SELECT Id, Status__c FROM Purchase_Orders__r),
                    (SELECT Id, Status__c FROM Discount_Applied__r),
                    (SELECT Id, Canceled_Order__c FROM Opportunities__r)
                    FROM Order WHERE Id =: OrderId];
        
        System.debug('ORDER: '+o);
        o.Apex_Context__c = true;
        o.Date_Cancelled__c = Date.today();
        o.Status = 'Cancelled';
        if(Reason != '' && Reason != null){
            o.Cancellation_Reason__c = Reason;
        }
        if(o.RecordTypeName__c == 'Change Order'){
            o.Revenue_Recognized_Date__c = Date.today();
            o.Change_Order_Rev_Recognized_Date__c = Date.today();
        }
        sobjectsToUpdate.add(o);
        
        System.debug('OrderItems: '+o.OrderItems);
        for(OrderItem oi: o.OrderItems){
            oi.Status__c = 'Cancelled';
            oi.Cancellation_Reason__c = Reason;
            sobjectsToUpdate.add(oi);
        }

        for(Purchase_Order__c po: o.Purchase_Orders__r){
            po.Status__c = 'Cancelled';
            sobjectsToUpdate.add(po);
        }
        List<Order_Discount__c> discountsToUpdate = new List<Order_Discount__c>();
        for(Order_Discount__c od: o.Discount_Applied__r){
            od.Status__c = 'Cancelled';
            discountsToUpdate.add(od);
        }

        if(discountsToUpdate.size() > 0){
            update discountsToUpdate;
        }

        for(Opportunity opp: o.Opportunities__r){
            if(opp.Canceled_Order__c != true){
                opp.Canceled_Order__c = true;
                sobjectsToUpdate.add(opp);
            }
        }
        
        System.debug('Work_Orders__r: '+o.Work_Orders__r);
        for(WorkOrder wo: o.Work_Orders__r){
            if(wo.Status != 'Appt Complete / Closed' && wo.Status != 'Appt Complete/Closed'){
                wo.Status = 'Canceled';
                wo.Cancel_Reason__c = 'Order Canceled';
                sobjectsToUpdate.add(wo);
            }
        }

        for(Task t: o.Tasks){
            if(t.Status != 'Completed'){
                t.Status = 'Cancelled';
                sobjectsToUpdate.add(t);
            }
        }

        system.debug('sobjectsToUpdate: '+sobjectsToUpdate);
        string returnText = '';
        try{
            update sobjectsToUpdate;
            returnText = 'Order Cancelled';
        }
        catch (Exception ex) {
            returnText = 'ERROR Cancelling order';
        }
        return returnText;
    }
}