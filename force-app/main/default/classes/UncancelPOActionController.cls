/*
* @author Jason Flippen
* @date 01/10/2020 
* @description Class to provide functionality for the uncancelPurchaseOrderAction LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - UncancelPOActionControllerTest
*/
public with sharing class UncancelPOActionController {

    /*
    * @author Jason Flippen
    * @date 01/10/2020
    * @description Method to return (wrapped) data from a Purchase Order.
    * @param purchaseOrderId
    * @returns List of (Wrapped) Purchase Order records
    */
    @AuraEnabled(cacheable=true)
    public static PurchaseOrderWrapper getPurchaseOrderData(Id purchaseOrderId) {

        PurchaseOrderWrapper wrapper = new PurchaseOrderWrapper();

        String currentUserProfileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;

        // Retrieve data from the Purchase Order record.
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  Confirmed_Timestamp__c,
                                                  Status__c
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];

        // Set the Wrapper properties.
        wrapper.id = purchaseOrder.Id;
        wrapper.confirmedTimestamp = purchaseOrder.Confirmed_Timestamp__c;
        wrapper.currentUserProfileName = currentUserProfileName.toLowerCase();
        wrapper.status = purchaseOrder.Status__c;

        // Return the Wrapper Class.
        return wrapper;

    }

    /*
    * @author Jason Flippen
    * @date 01/10/2020
    * @description Method to update the Purchase Order Status to "In Process".
    * @param purchaseOrderId
    * @returns String containing the save result (Success/Error)
    */
    @AuraEnabled
    public static String uncancelPurchaseOrder(Id purchaseOrderId) {
        
        String returnResult = null;

        // Get Purchase Order and associated Products.
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  Canceled_Timestamp__c,
                                                  Status__c
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];  

        // Update the Purchase Order.
        purchaseOrder.Status__c = 'In Process';
        purchaseOrder.Canceled_Timestamp__c = null;

        try {
            update purchaseOrder;
            returnResult = 'Uncancel PO Success';
        }
        catch (Exception ex) {
            System.debug('***** PO Update Error: ' + ex);
            returnResult = RMS_ErrorMessages.UPDATE_PO_EXCEPTION;
        }
        
        return returnResult;
        
    }


/** Wrapper Class **/


    /*
    * @author Jason Flippen
    * @date 01/10/2020
    * @description WRapper Class for Purchase Order.
    */
    @TestVisible
    public class PurchaseOrderWrapper {
        
        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public Datetime confirmedTimestamp {get;set;}

        @AuraEnabled
        public String currentUserProfileName {get;set;}

        @AuraEnabled
        public String status {get;set;}

    }

}