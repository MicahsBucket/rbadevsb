/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Signature Service Community
* @date 6/17
* @description Creates lists of installers based on their association to 
* the survey via junction object and creates those junction objects when 
* new workers are associated with the survey.  Also updates all other field changes
**/

public with sharing class NewManualPISApexController {

@AuraEnabled
	public static Survey__c getSurvey(Id recordId){
		Survey__c survey = [SELECT Id, Name, Primary_Contact_First_Name__c,
			Primary_Contact_Last_Name__c,
			Primary_Contact_Email__c,
			Primary_Contact_Mobile_Phone__c,
			State__c, City__c, Street__c,
			Country__c, Zip__c,
			Shipping_Date__c,
			Installation_Date__c,
			Send_Date__c,
            Days_Left__c,
            Admin_Notes__c,
            Comments__c,
			Survey_Status__c FROM Survey__c WHERE Id = :recordId LIMIT 1];
		return survey;
	}

//returns a list of workers based on their worker role
@AuraEnabled 
	public static List<Worker__c> getWorkers(String workerRole) {	
		//finds all workers of the specified role (workerRole) and excludes inactive users
		List<Worker__c> workers = [SELECT Id, First_Name__c, Last_Name__c, Role__c 
									FROM Worker__c 
									WHERE Worker__c.Role__c 
									INCLUDES (:workerRole) AND Worker__c.Role__c EXCLUDES ('Inactive') ORDER BY Worker__c.Last_Name__c ASC];
		if(workers.size() == 0){
			workers = new List<Worker__c>();
		}
		return workers;
	}

@AuraEnabled
	public static Worker__c getOriginalWorker(Id recordId, String role){
		Worker__c worker = new Worker__c();
        if(role == 'Sales Rep'){
            Id recordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get('Post Appointment').getRecordTypeId();

            String uniqueId = [SELECT Unique_Job_Id__c FROM Survey__c WHERE Id = :recordId LIMIT 1].Unique_Job_Id__c;
            system.debug('uniqueId: ' + uniqueId);
            if(uniqueId != null){
                Id surveyPASId;
                try{
                	surveyPASId = [SELECT Id FROM Survey__c WHERE Unique_Job_Id__c = :uniqueId AND RecordTypeId = :recordTypeId LIMIT 1].Id;
                } catch(Exception e){
                    
                }
                if(surveyPASId != null){
                    recordId = surveyPASId;
                    system.debug('recordId: ' + recordId);
                }
            }    
        }
        try{
            Id workerId = [SELECT Id, Survey_Id__c, Worker_Id__c, Role__c FROM Survey_Worker__c 
                           WHERE Survey_Id__c = :recordId AND Role__c = :role LIMIT 1].Worker_Id__c;
            system.debug('workerId: ' + workerId);
            worker = [SELECT Id, First_Name__c, Last_Name__c , Role__c
                      FROM Worker__c 
                      WHERE Worker__c.Id = :workerId LIMIT 1];
            system.debug('worker: ' + worker);
        } catch(Exception e){
            return null;
        }
        return worker;
	}

//creates junction objects for all workers that have been associated with the survey
@AuraEnabled
	public static String savePIS(Id recordId, String firstName, String lastName, String email, 
									String phone, String street, String city, String state, String zip, String country,
									Id techMeasurer, Id salesRep, List<Id> installers, Worker__c originalSalesRep, Worker__c originalTechMeasurer, String comment
									){
		Set<Id> originalInstallerIds = new Set<Id>();
		List<Id> installersToDelete = new List<Id>();
		List<Id> installerJunctionsToCreate = new List<Id>();
        List<Survey_Worker__c> junctionList = new List<Survey_Worker__c>();
        Set<Survey_Worker__c> removeJunctionList = new Set<Survey_Worker__c>();
        
        if(phone != null){
        	if(phone.length() == 10){
	        	String first3Digits = phone.substring(0,3);
	        	String last9Characters = phone.substring(3,10);
	        	String first5Characters = '(' + first3Digits + ')';
	        	phone = first5Characters + last9Characters;
        	}
        }
        

        //updates survey information (not including workers)
		Survey__c survey = [SELECT Id, Primary_Contact_First_Name__c,
			Primary_Contact_Last_Name__c,
			Primary_Contact_Email__c,
			Primary_Contact_Mobile_Phone__c,
			Street__c, City__c, State__c, Zip__c, Country__c, Comments__c
		    FROM Survey__c WHERE Id = :recordId LIMIT 1];
		
		survey.Primary_Contact_First_Name__c = firstName;
		survey.Primary_Contact_Last_Name__c = lastName;
		survey.Primary_Contact_Email__c = email;
		survey.Primary_Contact_Mobile_Phone__c = phone;
		survey.Street__c = street;
		survey.City__c = city;
		survey.State__c = state;
		survey.Zip__c = zip;
		survey.Country__c = country;
		survey.Comments__c = comment;
		system.debug('city: ' + survey.City__c);
		//finds existing junctions and workers for the survey
		List<Survey_Worker__c> originalInstallerJunc = [SELECT Worker_Id__c, Survey_Id__c 
														FROM Survey_Worker__c 
														WHERE Survey_Worker__c.Survey_Id__c = :recordId 
														AND Survey_Worker__c.Role__c = 'Installer'];
        for(Survey_Worker__c sw : originalInstallerJunc){
            originalInstallerIds.add(sw.Worker_Id__c);
        }

        //finds all installers who were previously associated with the survey
        List<Worker__c> originalInstallers = [Select Role__c, First_Name__c, Last_Name__c, Id,Name FROM Worker__c 
                                    WHERE Worker__c.Role__c INCLUDES ('Installer')
                                    AND Worker__c.Id = :originalInstallerIds];
        //list to delete old junctions
        for(Worker__c w : originalInstallers){
        	installersToDelete.add(w.Id);
        }
        //list to create new junctions
        for(Id w : installers){
        	installerJunctionsToCreate.add(w);
        }
        //creates a list of junctions objects for active installers to create
		if(installerJunctionsToCreate.size() > 0){
			for(Id w : installerJunctionsToCreate){
			Survey_Worker__c installerJunc = new Survey_Worker__c(
				Survey_Id__c = survey.Id,
				Worker_Id__c = w,
				Role__c = 'Installer'
			);
			junctionList.add(installerJunc);
			}
		}
		//creates a list of installer junctions to delete
		if(installersToDelete.size() > 0){
			List<Survey_Worker__c> junctionToDelete = [SELECT Id, Worker_Id__c FROM Survey_Worker__c 
												WHERE Survey_Worker__c.Worker_Id__c = :installersToDelete 
												AND Survey_Worker__c.Survey_Id__c = :recordId];
			for(Survey_Worker__c sw : junctionToDelete){
				removeJunctionList.add(sw);
			}
			
        }
        //creates a list of original sales rep junctions to delete
		if(originalSalesRep != null){
			Id osrId = originalSalesRep.Id;
			Survey_Worker__c junctionToDelete = [SELECT Id, Worker_Id__c FROM Survey_Worker__c 
												WHERE Survey_Worker__c.Worker_Id__c = :osrId 
												AND Survey_Worker__c.Survey_Id__c = :recordId LIMIT 1];
			removeJunctionList.add(junctionToDelete);
		}
		//creates a list of original tech measurers junctions to delete
		if(originalTechMeasurer != null){
			Id otmId = originalTechMeasurer.Id;
			Survey_Worker__c junctionToDelete = [SELECT Id, Worker_Id__c FROM Survey_Worker__c 
												WHERE Survey_Worker__c.Worker_Id__c = :otmId 
												AND Survey_Worker__c.Survey_Id__c = :recordId LIMIT 1];
			removeJunctionList.add(junctionToDelete);
		}
		//builds a list of sales rep junctions to create
		if(salesRep != null){
			Survey_Worker__c salesRepJunction = new Survey_Worker__c(
			Survey_Id__c = survey.Id,
			Worker_Id__c = salesRep,
			Role__c = 'Sales Rep'
			);
			junctionList.add(salesRepJunction);	
		}
		//builds a list of tech measurer junctions to create
		if(techMeasurer != null){
			Survey_Worker__c techMeasurerJunction = new Survey_Worker__c(
			Survey_Id__c = survey.Id,
			Worker_Id__c = techMeasurer,
			Role__c = 'Tech Measurer'
			);
			junctionList.add(techMeasurerJunction);
		}
		
		//deletes all old junction objects for all old workers that have been removed from survey
		if(removeJunctionList.size() > 0){
			List<Survey_Worker__c> deleteList = new List<Survey_Worker__c>(removeJunctionList);
			Database.delete(deleteList, true);
		}

		//upserts all new information
		if(junctionList.size() > 0){
			Database.upsert(junctionList, true);
		}
		
		//sends correct error information to controller to give user correct error message
		try{
			Database.UpsertResult upsertError = Database.upsert(survey, true);
		} catch(DmlException dml){
			if(String.valueOf(dml).contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
				return 'Validation Error';
			}
		}
		return 'No Errors';

	}

	//updates status to hold or send based on which button was pressed, if survey is already on hold survey is unchanged
@AuraEnabled
	public static String updateStatus(String status, Id recordId){
		Survey__c survey = [SELECT Survey_Status__c, Send_Date__c, Sent_To_Medallia__c, Name, Send_to_Medallia__c FROM Survey__c WHERE Survey__c.Id = :recordId LIMIT 1];
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Store').getRecordTypeId();
        Id installRecordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get('Post Install').getRecordTypeId();
		if(status.equals('Send')){
			if(survey.Send_to_Medallia__c){
				return 'already sent';
			} else {
				survey.Send_to_Medallia__c = true;
				Database.update(survey, true);
				return 'send';
			}
        } else if(survey.Sent_To_Medallia__c == true){
            return 'already sent';
		} else if(survey.Survey_Status__c != 'Hold'){   
			Integer surveysOnHold = [SELECT Id FROM Survey__c WHERE (Survey_Status__c = 'Hold' OR Survey_Status__c = 'Incomplete Data') AND RecordTypeId = :installRecordTypeId].size();
			system.debug('total: '+ surveysOnHold);
            Integer totalHolds = [SELECT Survey_Holds__c, RecordTypeId FROM Account WHERE RecordTypeId = :recordTypeId LIMIT 1].Survey_Holds__c.intValue();

			Boolean holdsLeft = false;
			if(totalHolds - surveysOnHold > 0) holdsLeft = true;
			if(survey.Survey_Status__c != 'Send' && survey.Survey_Status__c != 'Sent'){
				if(holdsLeft){
					Date todayPlus56 = Date.today() + 56;
					if(todayPlus56 <= survey.Send_Date__c){
						Integer difference = Date.today().daysBetween(survey.Send_Date__c);
						String difString = String.valueOf(difference);
						return 'has ' + difString + ' days until auto send date.';
					} else {
						survey.Survey_Status__c = 'Hold';
					}
				} else {
					return 'hold limit';
				}
			} else {
				return 'already sent';
			}
			Database.update(survey, true);
			return 'hold';
		} else return 'already held';
	}
}