/**
* @author Ramya Bangalore, Consultant.
* @group Customer Portal
* @date 11/18
* @description Finds the Individual MileStone Stage Dates associated to the current Order and MileStone Dates from the Order and Opportunity Objects.
* @Location used in Customer Portal "CustomerOrderDates" Component which displays the MileStone Dates and Status. Under "myPROJECT" Tab.

* Consultation -> Opportunity.CloseDate
* Order Placed->Order.BookingDate
* Tech Measure (could be multiple) -> WorkOrder(TechMeasure in right status).Scheduled Start Date and Scheduled End Date.
* Custom Build -> No specific date, will be the time between max measure date, and the first installation date.
* Installation -> WorkOrder(Install of the right status).Scheduled Start Date.
* Survey -> Survey SENT date
Used in MileStone Controller for Customer Portal Community.
*/
public class MilestoneDates {

    @AuraEnabled public Date consultationDate{get;set;}
    @AuraEnabled public Date orderPlaceDate{get;set;}
    @AuraEnabled public List<DateTime> techMeasureDate{get;set;}
    @AuraEnabled public List<DateTime> techMeasureEndDate{get;set;}
    @AuraEnabled public DateTime installDate{get;set;}  // Ramya comment later as we need a list
    @AuraEnabled public List<DateTime> installDates{get;set;}
    @AuraEnabled public Date surveySentDate{get;set;}
    //@AuraEnabled public Time customBuildTime{get;}
    @AuraEnabled public List<WorkOrder> lstInstall ;
    @AuraEnabled public List<WorkOrder> lstTechMeasure ;
    @AuraEnabled public Date ReinstallDate{get;set;} 
	@AuraEnabled public Boolean isOnHold ; 
    @AuraEnabled public Date EstimatedShipDate;
    @AuraEnabled public Boolean showShipDetails ;
    @AuraEnabled public String isOnHoldMessage;
    @AuraEnabled public List<String> checklistItems;
    
    public MilestoneDates()
    {
		lstInstall = new List<WorkOrder>() ;
        lstTechMeasure = new List<WorkOrder>() ;
        checklistItems = new List<String>();
    }
}