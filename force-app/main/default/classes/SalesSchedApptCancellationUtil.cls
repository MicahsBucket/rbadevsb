/**
 * @File Name          : SalesSchedApptCancellationUtil.cls
 * @Description        : 
 * @Author             : Brian Tremblay (Demand Chain)
 * @Group              : 
 * @Last Modified By   : Connor.Davis@andersencorp.com
 * @Last Modified On   : 02-16-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    4/24/2019, 10:20:54 AM   James Loghry (Demand Chain)     Initial Version
 * 2.0    3/11/2020, 09:28:48 AM   Ramya Bangalore                James Version
 * 3.0    9/08/2020, 12:56:24 PM   Connor Davis                   Ramya Version
 **/
public class SalesSchedApptCancellationUtil{

    public static void sendApptCancellationEmails(List<Sales_Appointment__c> lCanceledAppts){
        list<string>  status = new list<string> {'Confirmed', 'Accepted', 'Checked In', 'Checked Out', 'Resulted'};
        for(Sales_Appointment_Resource__c resource :
            [Select
                Sales_Capacity__r.User__r.Contact.Email,
                Sales_Capacity__r.User__r.Contact.Secondary_Email__c,
                Sales_Appointment__r.Sales_Order__r.Opportunity__r.Name,
                Sales_Appointment__r.Sales_Order__r.Opportunity__r.Store_Location__r.Email_For_SalesSched__c,
                Sales_Appointment__r.Appointment_Date_Time__c, 
                Primary__c, Finalized_By__r.Contact.Email,
                Sales_Appointment__r.Slot__r.Name
             From
                Sales_Appointment_Resource__c
             Where
                Sales_Appointment__c IN :lCanceledAppts
                And Sales_Capacity__r.User__r.Contact.Email != null
                And Status__c =: status]){
            system.debug('line 28 ' + resource);
            String oppName  = resource.Sales_Appointment__r.Sales_Order__r.Opportunity__r.Name; // might not be needed
            Datetime apptDate = resource.Sales_Appointment__r.Appointment_Date_Time__c;
            String dtStr = apptDate.format('a');
            String ApDate = apptDate.format('EEE MMM d');
            String slot = resource.Sales_Appointment__r.Slot__r.Name;

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<String> emailIDs = new List<String>();
            if(String.isNotBlank(resource.Sales_Capacity__r.User__r.Contact.Email)){
                emailIDs.add(resource.Sales_Capacity__r.User__r.Contact.Email);
            }
            if(String.isNotBlank(resource.Sales_Capacity__r.User__r.Contact.Secondary_Email__c)){  
                emailIDs.add(resource.Sales_Capacity__r.User__r.Contact.Secondary_Email__c);
            }   
            mail.setToAddresses(emailIDs);
            if(resource.Primary__c){ 
                List<String> emails = new List<String>();
                String emailsString = resource.Sales_Appointment__r.Sales_Order__r.Opportunity__r.Store_Location__r.Email_For_SalesSched__c;
                System.Debug('CD: Email string: ' + emailsString);
                if(String.isNotBlank(emailsString)){
                    emails = emailsString.split(',');
                    System.Debug('CD: Email string split, first is ' + emails[0]);
                    mail.setCcAddresses(emails);
                }
                else if((resource.Finalized_By__r.Contact.Email)!= null){
                    mail.setCcAddresses(new List<String>{resource.Finalized_By__r.Contact.Email});
                }
                 
           }
            mail.setSubject('Appointment Cancelled - Do Not Reply');
            String body = 'Your Appointment, set for ' + ApDate +''+ ' in the ' + slot +''+ ' timeslot has been cancelled. Please remember to check your schedule for any replacement Appointments.\n Do not reply to this email - this mailbox is not monitored, contact your manager with any questions.';
            mail.setPlainTextBody(body);
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
        }
    }
}