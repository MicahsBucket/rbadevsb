/*
* @author Jason Flippen
* @date 03/10/2020 
* @description Class to provide functionality for the CongaPrintPOAction Aura Component.
*
*              Test code coverage provided by the following Test Class:
*			   - CongaPrintPOActionControllerTest
*/
public with sharing class CongaPrintPOActionController {

    /*
    * @author Jason Flippen
    * @date 03/10/2020
    * @description Method to return the appropriate Conga Print URL.
    * @param Purchase Order Id
    * @returns URL String
    */
    @AuraEnabled
    public static String getCongaPrintUrl(Id purchaseOrderId) {
        
        String congaPrintUrl = '';

        Id costPORecordTypeId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Cost_Purchase_Order').getRecordTypeId();
        Id servicePORecordTypeId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Service_Purchase_Order').getRecordTypeId();

        Purchase_Order__c purchaseOrder = [SELECT Id, Account_ID__c, RecordTypeId FROM Purchase_Order__c WHERE Id = :purchaseOrderId];
        
        // Are we in a Community?
        Boolean sourceIsCommunity = true;
        String siteId = Site.getSiteId();
        if (siteId == null) {
            sourceIsCommunity = false;
        }

        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();

        // Default to non-Cost and non-Service PO URL.
        congaPrintUrl = 'https://rforce.renewalbyandersen.com/apex/APXTConga4__Conga_Composer?serverUrl=' + baseUrl + '/01I61000000vyzR&Id=' + purchaseOrderId +
                        '&QueryId=[POwithProductDetails]a22610000018c59,[OrderwPO]a22610000018c9E,[POswithStoreConfig]a22610000018cCM' +
                        '&TemplateId=a2A610000003C1u&DefaultPDF=1&DS7=3&DS4=1&DS0=1';
        
        if (sourceIsCommunity == false) {
            congaPrintUrl = 'https://composer.congamerge.com?SolMgr=1&serverUrl=' + baseUrl + '/01I61000000vyzR&Id=' + purchaseOrderId +
                            '&QueryId=[POwithProductDetails]a22610000018c59,[OrderwPO]a22610000018c9E,[POswithStoreConfig]a22610000018cCM' +
                            '&TemplateId=a2A610000003C1u&DefaultPDF=1&DS7=3&DS4=1&DS0=1';
        }

        // Is the Purchase Order a Cost or Service Purchase Order?
        if (purchaseOrder.RecordTypeId == costPORecordTypeId) {

            // Cost PO

            congaPrintUrl = 'https://rforce.renewalbyandersen.com/apex/APXTConga4__Conga_Composer?serverUrl=' + baseUrl + '/01I61000000vyzR&Id=' + purchaseOrderId +
                            '&QueryId=[POswithOrderProducts]a2261000001FMVb,[POswithStoreConfig]a22610000018cCM,[OrderwPO]a22610000018c9E' +
                            '&TemplateId=a2A61000000iaZs&DS7=3&DefaultPDF=1&AC0=1&SC0=1&SC1=Attachments';
            
            if (sourceIsCommunity == false) {
                congaPrintUrl = 'https://composer.congamerge.com?SolMgr=1&serverUrl=' + baseUrl + '/01I61000000vyzR&Id=' + purchaseOrderId +
                                '&QueryId=[POswithOrderProducts]a2261000001FMVb,[POswithStoreConfig]a22610000018cCM,[OrderwPO]a22610000018c9E' +
                                '&TemplateId=a2A61000000iaZs&DS7=3&DefaultPDF=1&AC0=1&SC0=1&SC1=Attachments';
            }
        }
        else if (purchaseOrder.RecordTypeId == servicePORecordTypeId) {

            // Service PO

            congaPrintUrl = 'https://rforce.renewalbyandersen.com/apex/APXTConga4__Conga_Composer?serverUrl=' + baseUrl + '/01I61000000vyzR&Id=' + purchaseOrderId +
                            '&QueryId=[POswithOrderProducts]a2261000001FMVb,[POswithStoreConfig]a22610000018cCM,' +
                            '[OrderwPO]a22610000018c9E,[AcctwithSO]a2261000001FMVg?pv0=' + purchaseOrder.Account_ID__c +
                            '&TemplateId=a2A610000002wP6&AC0=1&DefaultPDF=1&DS4=1&DS0=1&DS7=3';

            if (sourceIsCommunity == false) {
                congaPrintUrl = 'https://composer.congamerge.com?SolMgr=1&serverUrl=' + baseUrl + '/01I61000000vyzR&Id=' + purchaseOrderId +
                                '&QueryId=[POswithOrderProducts]a2261000001FMVb,[POswithStoreConfig]a22610000018cCM,' +
                                '[OrderwPO]a22610000018c9E,[AcctwithSO]a2261000001FMVg?pv0=' + purchaseOrder.Account_ID__c +
                                '&TemplateId=a2A610000002wP6&AC0=1&DefaultPDF=1&DS4=1&DS0=1&DS7=3';
            }
        }

        return congaPrintUrl;
        
    }

}