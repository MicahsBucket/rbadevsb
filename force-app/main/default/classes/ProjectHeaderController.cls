/**
* @author Mark Rothermal, Aim
* @group Customer Portal
* @date 1/18
* @description finds/updates the current order and also returns a list of all orders
**/
public without sharing class ProjectHeaderController {
    @AuraEnabled
    public static LightningResponse getSelectedOrder(){
        LightningResponse lr = new LightningResponse();
        CustomWrapper cwrap=new CustomWrapper();
        try{
            user userRecord = [Select id,Selected_Order__c,ContactId,SessionId__c,isSessionChanged__c
                               from User where id = :UserInfo.getUserId() limit 1];
            if(userRecord.Selected_Order__c!=null)
            {
                Order ordRecord=[select Id,Message_Pop_up__c,Notification_Message_Pop_Up__c,Store_Location__r.Active_Store_Configuration__r.Project_Approval__c,
                                 Store_Location__r.Active_Store_Configuration__r.Home_Owners_Decision__c                                 
                                 from Order where id=:userRecord.Selected_Order__c];
                List<Task> taskList=[select Primary_Reason__c,RecordTypeId,Status from Task where RecordType.Name='On Hold' and WhatId=:ordRecord.Id
                                     and status!='Completed' and (Primary_Reason__c='Homeowner Decision' or Primary_Reason__c='Project Approval')
                                     order by createdDate asc limit 1];
                if(!taskList.isEmpty())
                {
                    cwrap.taskRecord=new Task();
                    cwrap.taskRecord=taskList[0];
                }
                String sessionId=UserInfo.getOrganizationId().substring(0, 15) + ' ' +  UserInfo.getSessionId().substring(15);
                String sessionWithOrderId=sessionId+userRecord.Selected_Order__c;
                String sessionWithOrderId2=userRecord.SessionId__c+userRecord.Selected_Order__c;
                System.debug('_____sessionId_______'+sessionId);
                if(sessionWithOrderId!=sessionWithOrderId2)
                {
                    userRecord.isSessionChanged__c=true;
                    userRecord.SessionId__c=sessionId;    
                }  
                cwrap.orderRecord=ordRecord;
                
            }
            cwrap.userRecord=userRecord;
            lr.jsonResponse = JSON.serialize(cwrap);   
            System.debug('_______jsonResponse_____'+lr.jsonResponse);
            if(userRecord.isSessionChanged__c==true)
            {
                userRecord.isSessionChanged__c=false;
                update userRecord;
            }
        } catch(exception e){
            lr = new LightningResponse(e);
        }
        return lr;
    }
    @AuraEnabled
    public static LightningResponse updateSelectedOrder(id orderId){
        system.debug(orderId);
        LightningResponse lr = new LightningResponse();
        CustomWrapper cwrap=new CustomWrapper();
        try{
            user userRecord = [Select id,Selected_Order__c,ContactId,SessionId__c,isSessionChanged__c
                               from User where id = :UserInfo.getUserId() limit 1];
            String sessionWithOrderId2=userRecord.SessionId__c+userRecord.Selected_Order__c;
            userRecord.Selected_Order__c = orderId;
            Order ordRecord=[select id,Message_Pop_up__c,Notification_Message_Pop_Up__c,Store_Location__r.Active_Store_Configuration__r.Project_Approval__c,
                             Store_Location__r.Active_Store_Configuration__r.Home_Owners_Decision__c                              
                             from Order where id=:userRecord.Selected_Order__c];    
            List<Task> taskList=[select Primary_Reason__c,RecordTypeId,Status from Task where RecordType.Name='On Hold' and WhatId=:ordRecord.Id
                                 and status!='Completed' and (Primary_Reason__c='Homeowner Decision' or Primary_Reason__c='Project Approval')
                                 order by createdDate asc limit 1];
            if(!taskList.isEmpty())
            {
                cwrap.taskRecord=new Task();
                cwrap.taskRecord=taskList[0];
            }   
            String sessionId=UserInfo.getOrganizationId().substring(0, 15) + ' ' +  UserInfo.getSessionId().substring(15);
            String sessionWithOrderId=sessionId+orderId;
            
            System.debug('_____sessionId_______'+sessionId);
            if(sessionWithOrderId!=sessionWithOrderId2)
            {
                userRecord.isSessionChanged__c=true;
                userRecord.SessionId__c=sessionId;    
            }
            cwrap.orderRecord=ordRecord;
            cwrap.userRecord=userRecord;
            lr.jsonResponse = JSON.serialize(cwrap);
            if(userRecord.isSessionChanged__c==true)
            {
                userRecord.isSessionChanged__c=false;
                userRecord.SessionId__c=sessionId;
            }
            update userRecord;
            /*User u = new User();
            u.id = UserInfo.getUserId();
            u.Selected_Order__c = orderId;
            system.debug(u);
            update u;
            lr.jsonResponse = JSON.serialize(u);*/
        }catch(exception e){
            lr = new LightningResponse(e);
            System.debug('e' + e);
        }
        return lr;
    }
    @AuraEnabled
    public static LightningResponse getAllOrders() {

        LightningResponse lr = new LightningResponse();
        Id userId = UserInfo.getUserId();
        User userRecord=[select id,ContactId from User where id=:userId];
        System.debug('_______userRecord________'+userRecord);
        system.debug('user id in getAllOrders '+userId);

        try{

            // Get the list of primary order data
            List<Order> orders = [SELECT Id, OrderNumber, Status, Order_Processed_Date__c,Discount_Amount__c,Primary_Contact__c,BillToContactId,
                                    Retail_Subtotal__c, Payments_Received__c, OpportunityCloseDate__c,EffectiveDate,Message_Pop_up__c,
                                    Amount_Financed__c, Amount_Due__c, Retail_Total__c,Amount_Refunded__c,Notification_Message_Pop_Up__c,
                                    Store_Location__c, Store_Location__r.Legal_Name__c,Business_Adjustments__c,
                                    Store_Location__r.Phone, Store_Location__r.BillingStreet,
                                    Store_Location__r.Active_Store_Configuration__r.Start_Day_Of_The_Week__c,
                                    Store_Location__r.Active_Store_Configuration__r.End_Day_Of_The_Week__c,
                                    Store_Location__r.Active_Store_Configuration__r.Open_Time__c,
                                    Store_Location__r.Active_Store_Configuration__r.Close_Time__c,
                                    Store_Location__r.BillingCity, Store_Location__r.BillingPostalCode,
                                    Store_Location__r.BillingState, Store_Location__r.BillingCountry,
                                    Store_Location__r.Active_Store_Configuration__r.Display_Unit_Prices__c,
                                    Store_Location__r.Active_Store_Configuration__r.Display_Total_Price__c,
                                    Store_Location__r.Active_Store_Configuration__r.Display_Discount_Price__c,
                                    Sales_Rep__c, Primary_Installer__c, Primary_Tech_Measure__c, Store_Location__r.Active_Store_Configuration__r.Payment_Message__c,
                                    Store_Location__r.Active_Store_Configuration__r.Payment_Url__c,
                                    (SELECT
                                        Id, Parent_Order_Item__c, PricebookEntry.Product2.Name, Quantity, Total_Retail_Price__c, Unit_Id__c, Room__c,
                                        Interior_Color__c, Exterior_Color__c, Glazing_S1__c, Glass_Pattern_S1__c, Tempered_S1__c,
                                        Glazing_S2__c, Glass_Pattern_S2__c, Tempered_S2__c,
                                        Glazing_S3__c, Glass_Pattern_S3__c, Tempered_S3__c,
                                        Glazing_S4__c, Glass_Pattern_S4__c, Tempered_S4__c,
                                        Grille_Pattern__c, Grille_Style__c, Height_Inches__c, Height_Fraction__c, Width_Inches__c,
                                        Width_Fraction__c, Status__c, Specialty_Shape__c, Sash_Operation__c,Variant_Number__c, PricebookEntry.Product2.Hide_In_Customer_Portal__c
                                        FROM OrderItems
										WHERE Status__c != 'Cancelled' AND PricebookEntry.Product2.Hide_In_Customer_Portal__c != TRUE
                                        ORDER BY Unit_Id__c),
                					(SELECT
                                        Id, Payment_Date__c, Payment_Amount__c, Payment_Method__c
                                        FROM Payments__r ORDER BY Payment_Date__c ASC),
                                    (SELECT Id, Amount__c, Date__c, Refund_Method__c FROM Refunds__r)
                FROM Order WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId))))
				ORDER BY createdDate DESC];
            
            lr.jsonResponse = JSON.serialize(orders);
             system.debug('lightning response in try block for getAllOrders' + lr);

        } catch(exception e){
            lr = new LightningResponse(e);
             system.debug('lightning response in catch block for getAllOrders' + lr);

        }
        return lr;
    }
	
	@AuraEnabled
   public static OrderStoreConfigWrap getStoreInfo() {
        Id userId = UserInfo.getUserId();
        User userRecord=[select id,ContactId from User where id=:userId];
        System.debug('_______userRecord________'+userRecord);
        OrderStoreConfigWrap osConfig = new OrderStoreConfigWrap();
        Order thisOrder = [SELECT Id, Status, Sales_Rep__c,Sales_Rep_Email__c,Sales_Rep_Phone__c,Primary_Installer_FSL__r.Name,Primary_Installer_FSL__r.RelatedRecord.Namees__c,EffectiveDate, BillToContactId,
                           Primary_Tech_Measure_FSL__r.Name,Primary_Tech_Measure_FSL__r.RelatedRecord.Namees__c,Order_Concierge__r.Name,Primary_Tech_Measure_Email__c,Order_Concierge__r.Email,
                           Order_Concierge__r.FullPhotoUrl,Sales_Rep_Active_Status__c,Primary_Tech_Measure_Active_Status__c,
                           Primary_Tech_Measure_FSL__r.RelatedRecord.FullPhotoUrl,Order_Concierge__r.IsActive,Order_Concierge__r.Namees__c,
                           Owner.Name,Owner.Email,Owner.Phone,Owner.IsActive,Order_Owner_Mobile__c,
                           Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.Name,
                           Store_Location__r.Active_Store_Configuration__r.Generic_Concierge_Team__c,
                           Store_Location__r.Active_Store_Configuration__r.Concierge_team__c,
                           Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.FullPhotoUrl,
                           Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.Email,
                           Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.Phone,
                           Store_Location__r.Active_Store_Configuration__r.Start_Day_Of_The_Week__c,
                           Store_Location__r.Active_Store_Configuration__r.End_Day_Of_The_Week__c,
                           Store_Location__r.Active_Store_Configuration__r.Open_Time__c,
                           Store_Location__r.Active_Store_Configuration__r.Close_Time__c,
                           Primary_Installer_FSL__r.RelatedRecord.FullPhotoUrl,Opportunity.owner.FullPhotoUrl,Opportunity.owner.Namees__c,
                           Store_Location__r.Active_Store_Configuration__r.Sales_Rep_Phone_Icon__c,
                           Store_Location__r.Active_Store_Configuration__r.Concierge_Phone_Icon__c,
                           Store_Location__r.Active_Store_Configuration__r.Measure_Technician_Phone_Icon__c,
                           Store_Location__r.Active_Store_Configuration__r.Sales_Rep_Description__c,
                           Store_Location__r.Active_Store_Configuration__r.Concierge_Description__c,
                           Store_Location__r.Active_Store_Configuration__r.Measure_Technician_Description__c,
                           Store_Location__r.Active_Store_Configuration__r.Primary_Installer_Description__c,
                           Order_Concierge__r.Phone,Primary_Tech_Measure_FSL__r.RelatedRecord.Phone,
                           Order_Concierge__r.MobilePhone,Primary_Tech_Measure_FSL__r.RelatedRecord.MobilePhone,Sales_Rep_Mobile__c,
                           Store_Location__c, BillToContact.FirstName, Store_Location__r.Legal_Name__c, Store_Location__r.Phone,
                           Store_Location__r.BillingStreet, Store_Location__r.BillingCity, Store_Location__r.BillingPostalCode,
                           Store_Location__r.BillingState, Store_Location__r.BillingCountry,Store_Location__r.Active_Store_Configuration__c,
                           Store_Location__r.Active_Store_Configuration__r.Sales_Rep_Label__c,
                           Store_Location__r.Active_Store_Configuration__r.Measure_Technician_Label__c,Store_Location__r.Active_Store_Configuration__r.concierges__c,
                           Store_Location__r.Active_Store_Configuration__r.Primary_Installer_Label__c,Store_Location__r.Active_Store_Configuration__r.Concierge_Label__c
                           FROM Order
                           WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId))))
                           ORDER BY createdDate DESC LIMIT 1];
       
          String photostring = [select id, fullphotourl from user WHERE id =: thisOrder.ownerid LIMIT 1].fullphotourl;
       /*Map<id,String> UserIDToPhotoUrl = new Map<id,String>();
       for(User u: [select id, fullphotourl from user WHERE id IN:]){
           if(UserIDToPhotoUrl.get(u.id) == Null){
               UserIDToPhotoUrl.put(u.id, u.fullphotourl);
           }
       }*/
         osConfig.myOrder = thisOrder;
         osConfig.myStore = thisOrder.Store_Location__r;
         osConfig.myStoreConfig = thisOrder.Store_Location__r.Active_Store_Configuration__r;
         osConfig.billToContact = thisOrder.BillToContact.FirstName;
       	 osConfig.salesRepEmail = thisOrder.Sales_Rep_Email__c;
         osConfig.techMeasureEmail = thisOrder.Primary_Tech_Measure_Email__c;
         osConfig.conciergeEmail = thisOrder.Order_Concierge__r.Email;
         osConfig.conciergeImage = thisOrder.Order_Concierge__r.FullPhotoUrl;
         osConfig.techMeasureImage = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.FullPhotoUrl;
         osConfig.primaryInstallerImage = thisOrder.Primary_Installer_FSL__r.RelatedRecord.FullPhotoUrl;
         osConfig.salesRepImage = thisOrder.Opportunity.owner.FullPhotoUrl;
         osConfig.concierge = thisOrder.Order_Concierge__r.Name;
         osConfig.concierges = thisOrder.Order_Concierge__r.Namees__c;
         osConfig.techMeasure = thisOrder.Primary_Tech_Measure_FSL__r.Name;
       	 osConfig.techMeasures = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.Namees__c;
         osConfig.primaryInstallerss = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.Namees__c;
         osConfig.conciergePhone = thisOrder.Order_Concierge__r.Phone;
         osConfig.techMeasurePhone = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.Phone;
         osConfig.salesRepPhone = thisOrder.Sales_Rep_Phone__c;
         osConfig.conciergeMobile = thisOrder.Order_Concierge__r.MobilePhone;
       	 osConfig.techMeasureMobile = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.MobilePhone;
       	 osConfig.salesRepMobile = thisOrder.Sales_Rep_Mobile__c;
         osConfig.salesRepActiveStatus = thisOrder.Sales_Rep_Active_Status__c;
       	 osConfig.primaryTechMeasureActiveStatus = thisOrder.Primary_Tech_Measure_Active_Status__c;
       	 osConfig.orderConciergeActiveStatus = thisOrder.Order_Concierge__r.IsActive;
		 osConfig.genericConciergeUser = thisOrder.Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.Name;
         osConfig.genericConciergeTeamRole = thisOrder.Store_Location__r.Active_Store_Configuration__r.Generic_Concierge_Team__c;
         osConfig.conciergeTeamCheckBox = thisOrder.Store_Location__r.Active_Store_Configuration__r.Concierge_team__c;
       	 osConfig.genericConciergeUserFullImage = thisOrder.Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.FullPhotoUrl;
         osConfig.genericConciergeUserEmail = thisOrder.Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.Email;
         osConfig.genericConciergeUserPhone = thisOrder.Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.Phone;
       	 osConfig.salesrepNamees = thisOrder.Opportunity.owner.Namees__c;
       	 osConfig.conciergeDes = thisOrder.Store_Location__r.Active_Store_Configuration__r.Concierge_Description__c;
       	 osConfig.salesrepDes = thisOrder.Store_Location__r.Active_Store_Configuration__r.Sales_Rep_Description__c;
       	 osConfig.techMeasuresDes = thisOrder.Store_Location__r.Active_Store_Configuration__r.Measure_Technician_Description__c;
         osConfig.primaryInstallerssDes = thisOrder.Store_Location__r.Active_Store_Configuration__r.Primary_Installer_Description__c;
         osConfig.conciergePicklist = thisOrder.Store_Location__r.Active_Store_Configuration__r.concierges__c;
       	 osConfig.orderOwner = thisOrder.Owner.Name;
       	 osConfig.orderOwnerPhone = thisOrder.Owner.Phone;
		 osConfig.orderOwnerMobile = thisOrder.Order_Owner_Mobile__c;
		 osConfig.orderOwnerEmail = thisOrder.Owner.Email;
         osConfig.orderOwnerIsActive = thisOrder.Owner.IsActive;
       	 osConfig.userPhotoURL = photostring;
       
       	
       
       
       System.debug('Primary Tech Measeure Active Status'+ osConfig.primaryTechMeasureActiveStatus);
        return osConfig;
    }

    @AuraEnabled
    public static OrderStoreConfigWrap getStoreInfoUpdated(String orderId) {
       Id userId = UserInfo.getUserId();
       OrderStoreConfigWrap osConfig = new OrderStoreConfigWrap();
       Order thisOrder = [SELECT Id, Status, Sales_Rep__c,Sales_Rep_Email__c,Sales_Rep_Phone__c,Primary_Installer_FSL__r.Name,Primary_Installer_FSL__r.RelatedRecord.Namees__c,EffectiveDate,
                          Primary_Tech_Measure_FSL__r.Name,Primary_Tech_Measure_FSL__r.RelatedRecord.Namees__c,Order_Concierge__r.Name,Primary_Tech_Measure_Email__c,Order_Concierge__r.Email,
                          Order_Concierge__r.FullPhotoUrl,Primary_Tech_Measure_FSL__r.RelatedRecord.FullPhotoUrl,Order_Concierge__r.Namees__c,
                          Primary_Installer_FSL__r.RelatedRecord.FullPhotoUrl,Opportunity.owner.FullPhotoUrl,
                          Owner.Name,Owner.Email,Owner.Phone,Owner.IsActive,Order_Owner_Mobile__c,
                          Sales_Rep_Active_Status__c,Primary_Tech_Measure_Active_Status__c,Order_Concierge__r.IsActive,
                          Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.Name,
                          Store_Location__r.Active_Store_Configuration__r.Generic_Concierge_Team__c,
                          Store_Location__r.Active_Store_Configuration__r.Concierge_team__c,
                          Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.FullPhotoUrl,
                          Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.Email,
                          Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.Phone,
                          Store_Location__r.Active_Store_Configuration__r.Start_Day_Of_The_Week__c,
                          Store_Location__r.Active_Store_Configuration__r.End_Day_Of_The_Week__c,
                          Store_Location__r.Active_Store_Configuration__r.Open_Time__c,
                          Store_Location__r.Active_Store_Configuration__r.Close_Time__c,
                          Store_Location__r.Active_Store_Configuration__r.Sales_Rep_Phone_Icon__c,
                          Store_Location__r.Active_Store_Configuration__r.Concierge_Phone_Icon__c,
                          Store_Location__r.Active_Store_Configuration__r.Measure_Technician_Phone_Icon__c,
                          Store_Location__r.Active_Store_Configuration__r.Sales_Rep_Description__c,
                          Store_Location__r.Active_Store_Configuration__r.Concierge_Description__c,
                          Store_Location__r.Active_Store_Configuration__r.Measure_Technician_Description__c,
                          Store_Location__r.Active_Store_Configuration__r.Primary_Installer_Description__c,
                          Order_Concierge__r.Phone,Primary_Tech_Measure_FSL__r.RelatedRecord.Phone,Opportunity.owner.Namees__c,
                          Order_Concierge__r.MobilePhone,Primary_Tech_Measure_FSL__r.RelatedRecord.MobilePhone,Sales_Rep_Mobile__c,
                          Store_Location__c, BillToContact.FirstName, Store_Location__r.Legal_Name__c, Store_Location__r.Phone,
                          Store_Location__r.BillingStreet, Store_Location__r.BillingCity, Store_Location__r.BillingPostalCode,
                          Store_Location__r.BillingState, Store_Location__r.BillingCountry,Store_Location__r.Active_Store_Configuration__c,
                          Store_Location__r.Active_Store_Configuration__r.Sales_Rep_Label__c,
                          Store_Location__r.Active_Store_Configuration__r.Measure_Technician_Label__c,Store_Location__r.Active_Store_Configuration__r.concierges__c,
                          Store_Location__r.Active_Store_Configuration__r.Primary_Installer_Label__c,Store_Location__r.Active_Store_Configuration__r.Concierge_Label__c
                          FROM Order
                          WHERE Id =:orderId
                          ORDER BY createdDate DESC LIMIT 1];
         String photostrings = [select id, fullphotourl from user WHERE id =: thisOrder.ownerid LIMIT 1].fullphotourl;
        osConfig.myOrder = thisOrder;
        osConfig.myStore = thisOrder.Store_Location__r;
        osConfig.myStoreConfig = thisOrder.Store_Location__r.Active_Store_Configuration__r;
        osConfig.billToContact = thisOrder.BillToContact.FirstName;
        osConfig.salesRepEmail = thisOrder.Sales_Rep_Email__c;
        osConfig.techMeasureEmail = thisOrder.Primary_Tech_Measure_Email__c;
        osConfig.conciergeImage = thisOrder.Order_Concierge__r.FullPhotoUrl;
        osConfig.techMeasureImage = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.FullPhotoUrl;
        osConfig.primaryInstallerImage = thisOrder.Primary_Installer_FSL__r.RelatedRecord.FullPhotoUrl;
        osConfig.salesRepImage = thisOrder.Opportunity.owner.FullPhotoUrl;
        osConfig.conciergeEmail = thisOrder.Order_Concierge__r.Email;
        osConfig.concierge = thisOrder.Order_Concierge__r.Name;
        osConfig.concierges = thisOrder.Order_Concierge__r.Namees__c;
        osConfig.techMeasure = thisOrder.Primary_Tech_Measure_FSL__r.Name;
        osConfig.techMeasures = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.Namees__c;
        osConfig.primaryInstallerss = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.Namees__c;
        osConfig.salesRepPhone = thisOrder.Sales_Rep_Phone__c;
        osConfig.conciergePhone = thisOrder.Order_Concierge__r.Phone;
        osConfig.techMeasurePhone = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.Phone;
        osConfig.conciergeMobile = thisOrder.Order_Concierge__r.MobilePhone;
        osConfig.techMeasureMobile = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.MobilePhone;
        osConfig.salesRepMobile = thisOrder.Sales_Rep_Mobile__c;
        osConfig.salesRepActiveStatus = thisOrder.Sales_Rep_Active_Status__c;
        osConfig.primaryTechMeasureActiveStatus = thisOrder.Primary_Tech_Measure_Active_Status__c;
        osConfig.orderConciergeActiveStatus = thisOrder.Order_Concierge__r.IsActive;
        osConfig.genericConciergeUser = thisOrder.Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.Name;
        osConfig.genericConciergeTeamRole = thisOrder.Store_Location__r.Active_Store_Configuration__r.Generic_Concierge_Team__c;
        osConfig.conciergeTeamCheckBox = thisOrder.Store_Location__r.Active_Store_Configuration__r.Concierge_team__c;
        osConfig.genericConciergeUserFullImage = thisOrder.Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.FullPhotoUrl;
        osConfig.genericConciergeUserEmail = thisOrder.Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.Email;
        osConfig.genericConciergeUserPhone = thisOrder.Store_Location__r.Active_Store_Configuration__r.Generic_Concierge__r.Phone;
        osConfig.salesrepNamees = thisOrder.Opportunity.owner.Namees__c;
        osConfig.conciergeDes = thisOrder.Store_Location__r.Active_Store_Configuration__r.Concierge_Description__c;
        osConfig.salesrepDes = thisOrder.Store_Location__r.Active_Store_Configuration__r.Sales_Rep_Description__c;
        osConfig.techMeasuresDes = thisOrder.Store_Location__r.Active_Store_Configuration__r.Measure_Technician_Description__c;
        osConfig.primaryInstallerssDes = thisOrder.Store_Location__r.Active_Store_Configuration__r.Primary_Installer_Description__c;
        osConfig.conciergePicklist = thisOrder.Store_Location__r.Active_Store_Configuration__r.concierges__c;
        osConfig.orderOwner = thisOrder.Owner.Name;
        osConfig.orderOwnerEmail = thisOrder.Owner.Email;
		osConfig.orderOwnerPhone = thisOrder.Owner.Phone;
		osConfig.orderOwnerMobile = thisOrder.Order_Owner_Mobile__c;
        osConfig.orderOwnerIsActive = thisOrder.Owner.IsActive;
        osConfig.userPhotoURL = photostrings; 
        
          System.debug('Primary Tech Measeure Active Status event'+ osConfig.primaryTechMeasureActiveStatus);
        return osConfig;
   }

    public class OrderStoreConfigWrap{
       	@AuraEnabled Public Order myOrder;
        @AuraEnabled Public Account myStore;
        @AuraEnabled Public Store_Configuration__c myStoreConfig;
        @AuraEnabled Public String billToContact;
        @AuraEnabled Public String concierge;
        @AuraEnabled Public String techMeasure;
        @AuraEnabled Public String techMeasureEmail;
        @AuraEnabled Public String conciergeEmail;
        @AuraEnabled Public String primaryInstallerImage;
        @AuraEnabled Public String conciergeImage;
        @AuraEnabled Public String techMeasureImage;
        @AuraEnabled Public String salesRepImage;
        @AuraEnabled Public String conciergePhone;
        @AuraEnabled Public String techMeasurePhone;
        @AuraEnabled Public String salesRepPhone;
        @AuraEnabled Public String salesRepEmail;
        @AuraEnabled Public String conciergeMobile;
        @AuraEnabled Public String techMeasureMobile;
        @AuraEnabled Public String salesRepMobile;
        @AuraEnabled Public Boolean salesRepActiveStatus;
        @AuraEnabled Public Boolean primaryTechMeasureActiveStatus;
        @AuraEnabled Public Boolean orderConciergeActiveStatus;
        @AuraEnabled Public String  genericConciergeUser;
        @AuraEnabled Public String genericConciergeTeamRole;
        @AuraEnabled Public Boolean conciergeTeamCheckBox;
        @AuraEnabled Public String genericConciergeUserFullImage;
        @AuraEnabled Public String genericConciergeUserEmail;
        @AuraEnabled Public String genericConciergeUserPhone;
        @AuraEnabled Public String salesrepNamees;
        @AuraEnabled Public String salesrepDes;
        @AuraEnabled Public String techMeasures;
        @AuraEnabled Public String techMeasuresDes;
  		@AuraEnabled Public String primaryInstallerss;
        @AuraEnabled Public String primaryInstallerssDes;
        @AuraEnabled Public String concierges;
        @AuraEnabled Public String conciergeDes;
        @AuraEnabled Public String conciergePicklist;
        @AuraEnabled Public String orderOwner;
        @AuraEnabled Public String orderOwnerEmail;
        @AuraEnabled Public String orderOwnerPhoto;
        @AuraEnabled Public String orderOwnerPhone;
        @AuraEnabled Public String orderOwnerMobile;
        @AuraEnabled Public boolean orderOwnerIsActive;
        @AuraEnabled Public String userPhotoURL;
        
    }
    
     //finds the most current Order and uses that to populate the all the child components
    @AuraEnabled
    public static Id getOrderId() {
        Id userId = UserInfo.getUserId();
        User userRecord=[select id,ContactId from User where Id=:userId];
        Id orderId = [SELECT Id FROM Order WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId)))) ORDER BY createdDate DESC LIMIT 1].Id;
        return orderId;
    }
    
    //returns a list of Orders that the user has been or is currently associated to
    @AuraEnabled
    public static List<Order> getOrders() {
        Id userId = UserInfo.getUserId();
        User userRecord=[select id,ContactId from User where Id=:userId];
        List<Order> orderList = [SELECT Id,Status, EffectiveDate, BillToContactId FROM Order WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId)))) ORDER BY createdDate DESC];
        return orderList;
    }
      //finds the order Id associated to the order the user desires to see the information for
    @AuraEnabled
    public static Id getNewOrderId(String bookingDate){
        Id userId = UserInfo.getUserId();
        User userRecord=[select id,ContactId from User where Id=:userId];
        Date pDate = date.valueOf(bookingDate);
        Id orderId = [SELECT Id FROM Order WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId)))) AND EffectiveDate =:pDate LIMIT 1].Id;
        return orderId;
    }
  
	
}