public with sharing class RetailPurchaseOrderItemsController {
    
    
    @AuraEnabled(cacheable=true)
    public  static   List<Retail_Purchase_Order_Items__c>  getRelatedLineItems(ID relatedPID) {
        
        List<Retail_Purchase_Order_Items__c> relatedCostItems =[Select Product__r.Name,Product_Description__c,Quantity__c,Total_Price__c,Unit_Price__c,Unit_of_Measure__c from Retail_Purchase_Order_Items__c where     Retail_Purchase_Order__c=:relatedPID ];
        return  relatedCostItems;    
    }
    
    @AuraEnabled()
    public  static   void  cancelRelatedCostItem(ID relatedPID) {
        
        Retail_Purchase_Order_Items__c rpoi = new Retail_Purchase_Order_Items__c(id= relatedPID);
        // edited 9/8/2020 mtr - recently the relationship between retail purchase order items and retail purchase orders was changed to a master detail. 
        // we can no longer allow rpoi's to be orphaned. updating this method to delete the rpoi instead of orphaning it.
        //rpoi.Retail_Purchase_Order__c=null;
        //update rpoi;
        delete rpoi;
        
        
    }
    
    @AuraEnabled(cacheable=true)
    public  static   List<OrderItem>  getRelatedOrderProducts(ID relatedPID) {
        
        List<OrderItem> relatedOrderItems =[Select Quantity,Unit_Wholesale_Cost__c,View_Record__c,Product2.Name,Unit_Id__c,Status__c from OrderItem where     Retail_Purchase_Order__c=:relatedPID ];
        return  relatedOrderItems;    
    }
    
    @AuraEnabled()
    public  static   void  cancelRelatedOrderItem(ID relatedPID) {
        
        OrderItem oi = new OrderItem(id= relatedPID);
        oi.Retail_Purchase_Order__c=null;
        //oi.Order=null;
        oi.Status__c = 'Cancelled';                                     
        update oi;
        
        
    }
    
    @AuraEnabled(cacheable=true)
    public static Retail_Purchase_Order__c    getStatus(String purchaseId) {
        
        Retail_Purchase_Order__c po = [select id ,Recordtype.developername,status__c,Order__c,Released_Timestamp__c,Confirmed_Timestamp__c  from Retail_Purchase_Order__c where id =:purchaseId limit 1];  
        
        return po;
    }
    
    
}