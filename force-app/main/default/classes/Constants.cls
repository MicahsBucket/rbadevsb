public with sharing class Constants {
	
	// Record Type Developer names
	public static final String DWELLING_ACCOUNT_RECORD_TYPE_NAME = 'Dwelling';
	public static final String PERMIT_MUNICIPALITY_RECORD_TYPE_NAME = 'Permit';
	public static final String MASTER_PRODUCT_RECORD_TYPE_NAME = 'Master_Product';
	public static final String CHILD_PRODUCT_RECORD_TYPE_NAME = 'Child_Product';
	public static final String MULLION_PRODUCT_RECORD_TYPE_NAME = 'Mullion';
	public static final String CONSTRUCTION_MATERIALS_PRODUCT_RECORD_TYPE_NAME = 'Construction_Materials';
	public static final String MISC_JOB_AND_UNIT_CHARGES_PRODUCT_RECORD_TYPE_NAME = 'Misc_Job_and_Unit_Charges';
 
	// opportunity stages
	public static final String OPP_STAGE_CANCELED_AND_SOLD = 'Canceled and Sold';
	public static final String OPP_STAGE_PENDING_CANCELATION = 'Pending Cancelation';
	public static final String OPP_STAGE_CANCELED = 'Canceled';
	public static final String OPP_STAGE_SOLD = 'Sold';
	public static final String OPP_STAGE_QUOTED = 'Quoted';
	public static final String OPP_STAGE_IN_PROGRESS = 'In Progress';
	public static final String OPP_STAGE_NEW = 'New';
	public static final String OPP_STAGE_DONT_GENERATE_ORDER = 'Fake Stage to not create an order in Trigger';
	
	// order statuses
	public static final String ORDER_STATUS_PENDING_CANCELLATION = 'Pending Cancellation';
	public static final String ORDER_STATUS_DRAFT = 'Draft';
	public static final String ORDER_STATUS_CANCELLED = 'Cancelled';
	public static final String ORDER_STATUS_JOB_CLOSED = 'Job Closed';
	public static final String ORDER_STATUS_ACTIVATED = 'Activated';

	// quote statuses
	public static final String QUOTE_STATUS_CANCELED = 'Canceled';
	public static final String QUOTE_STATUS_DRAFT = 'Draft';

	// Product Family constants
	public static final String PF_PATIO_DOOR = 'Patio Door';
	public static final String PF_SPECIALTY = 'Specialty';
	public static final String PF_WINDOW = 'Window';

	// locked Order Fields by Status
	public static final Map<String, List<String>> lockedOrderFieldMap = new Map<String, List<String>>
	{UtilityMethods.RecordTypeFor('Order','CORO_Record_Type') + 'Order Released'=>new String[]
		{	'AccountId',
			'Name',
			'BillToContactId',
			'BillingStreet', 
			'BillingCity',
			'BillingState',
			'BillingStateCode',
			'BillingPostalCode',
			'OpportunityId',
			'RecordTypeId',
			'EffectiveDate',
			'QuoteId',
			'ShippingStreet', 
			'ShippingCity',
			'ShippingState',
			'ShippingStateCode',
			'ShippingPostalCode',
			'Store_Location__c'
		},
		UtilityMethods.RecordTypeFor('Order','Change_Order') + 'In Progress'=>new String[]
		{	'AccountId',
			'Name',
			'Sold_Order__c',
			'Store_Location__c',
			'OpportunityId',
			'RecordTypeId',
			'EffectiveDate',
			'QuoteId'
		},
		UtilityMethods.RecordTypeFor('Order','Change_Order') + 'Job Closed'=>new String[]
		{	'AccountId',
			'Name',
			'Sold_Order__c',
			'Store_Location__c',
			'OpportunityId',
			'RecordTypeId',
			'EffectiveDate',
			'QuoteId'
		},
		UtilityMethods.RecordTypeFor('Order','Change_Order') + 'Cancelled'=>new String[]
		{	'AccountId',
			'Name',
			'Sold_Order__c',
			'Store_Location__c',
			'OpportunityId',
			'RecordTypeId',
			'EffectiveDate',
			'QuoteId'
		},
		'TestClass'=>new String[]
		{	'Name'

		}
	};

    // updated conversion map to correct values... mtr 6-11-19
	public static final Map<String,Double> fractionConversionMap = new Map<String,Double>{
		'Even' => 0.0d, '1/16' => 0.0625d,'1/8' => 0.125d,'3/16' => 0.1875d, 
		'1/4' => 0.25d, '5/16' => 0.3125d, '3/8' => 0.375d, '7/16' => 0.4375d, 
		'1/2' => 0.5d, '9/16' => 0.5625d, '5/8' => 0.625d, '11/16' => 0.6875d, 
		'3/4' => 0.75d, '13/16' => 0.8125d, '7/8' => 0.875d, '15/16' => 0.9375d
	};

	//////////////////////////////////////////////
	// rSuite project file processing contants
	// rSuite XML Node constants
	public static final String NODE_NAME_KEY = 'key';
	public static final String NODE_NAME_DICT = 'dict';
	public static final String NODE_NAME_ARRAY = 'array';
	public static final String NODE_NAME_STRING = 'string';
	public static final String NODE_NAME_FALSE = 'false';
	public static final String NODE_NAME_TRUE = 'true';
	public static final String NODE_NAME_REAL = 'real';
	public static final String NODE_NAME_INTEGER = 'integer';

	// rSuite field name translation maps
	public static final Map<String,String> rSuiteAccountFieldNameMap = new Map<String,String>{
		'sample' => 'Sample__c'
	};
	public static final Map<String,String> rSuiteContactFieldNameMap = new Map<String,String>{
		'sample' => 'Sample__c'
	};
	public static final Map<String,String> rSuiteOpportunityFieldNameMap = new Map<String,String>{
		'sample' => 'Sample__c'
	};
	public static final Map<String,String> rSuiteQuoteFieldNameMap = new Map<String,String>{
		'sample' => 'Sample__c'
	};
	public static final Map<String,String> rSuiteQLIFieldNameMap = new Map<String,String>{
		'crossReference' => 'Cross_Reference__c',
		'isInactive' => 'isInactive__c',
		'Hardware_Style__c' => 'Hardware_Finish__c',
		'Hardware_Finish__c' => 'Hardware_Style__c',

		
		//'Estate_Finish_Hand_Lift__c' => 'Estate_Finish_Hand_Lift__c',
		// @mitchSpano 4/18/18 This attribute is not present in any sample .rbaProj file I have seen 'Estate_Finish_Extra_Hand_Lift__c' =>'',
		'Estate_Finish_Recessed_Hand_Lift' => 'Estate_Finish_Recessed_Hand_Lift__c',
		'Estate_Finish_Extra_Recessed_Hand_Lift' => 'Estate_Finish_Extra_Recessed_Hand_Lift__c'

		/*'Standard_Color_Lift__c' =>'Standard_Color_Hand_Lift__c',
		'Standard_Color_Extra_Hand_Lift__c' => 'Standard_Color_Extra_Hand_Lift__c',
		'Standard_Color_Recessed_Hand_Lift__c' => 'Standard_Color_Recessed_Hand_Lift__c',
		'Standard_Color_Extra_Recessed_Hand_Lift__c' => 'Standard_Color_Extra_Recessed_Hand_Lift__c'*/
		
		

	};
	public static final Map<String,String> rSuiteQuoteDiscountFieldNameMap = new Map<String,String>{
		'Name' => 'Pivotal_Id__c'
	};
	public static final Map<String,String> rSuiteQuoteFinancingFieldNameMap = new Map<String,String>{
		'sample' => 'Sample__c'
	};
	public static final Map<String,String> rSuiteOrderFieldNameMap = new Map<String,String>{
		'sample' => 'Sample__c' 
	};
	public static final Map<String,String> rSuiteOIFieldNameMap = new Map<String,String>{
		'crossReference' => 'Cross_Reference__c'
	};
	public static final Map<String,String> rSuiteOrderDiscountFieldNameMap = new Map<String,String>{
		'sample' => 'Sample__c' 
	};
	public static final Map<String,String> rSuiteOrderFinancingFieldNameMap = new Map<String,String>{
		'sample' => 'Sample__c' 
	};
	public static final Map<String,Map<String,String>> rSuiteObjectFieldNameMap = new Map<String,Map<String,String>>{
		'Account' => rSuiteAccountFieldNameMap, 'Contact' => rSuiteContactFieldNameMap, 'Opportunity' => rSuiteOpportunityFieldNameMap,
		'Quote' => rSuiteQuoteFieldNameMap, 'QuoteLineItem' => rSuiteQLIFieldNameMap, 'Quote_Discount__c' => rSuiteQuoteDiscountFieldNameMap,
		'Quote_Financing__c' => rSuiteQuoteFinancingFieldNameMap,'Order' => rSuiteOrderFieldNameMap, 'OrderItem' => rSuiteOIFieldNameMap,
		'Order_Discount__c' => rSuiteOrderDiscountFieldNameMap, 'Order_Financing__c' => rSuiteOrderFinancingFieldNameMap
	};
	
	// rSuite field type translation map
	public static final Map<String,String> rSuiteAccountFieldTypeMap = new Map<String,String>{
		'sample' => NODE_NAME_STRING
	};
	public static final Map<String,String> rSuiteContactFieldTypeMap = new Map<String,String>{
		'sample' => NODE_NAME_STRING
	};
	public static final Map<String,String> rSuiteOpportunityFieldTypeMap = new Map<String,String>{
		'sample' => NODE_NAME_STRING
	};
	public static final Map<String,String> rSuiteQuoteFieldTypeMap = new Map<String,String>{
		'sample' => NODE_NAME_STRING
	};
	public static final Map<String,String> rSuiteQLIFieldTypeMap = new Map<String,String>{
		'Quantity' => NODE_NAME_REAL, 'Finger_Lifts__c' => NODE_NAME_STRING, 'Locks_Sash__c' => NODE_NAME_STRING,
		'Number_of_Panels__c' => NODE_NAME_STRING, 'Lifts_Pulls__c' => NODE_NAME_STRING
	};
	public static final Map<String,String> rSuiteQuoteDiscountFieldTypeMap = new Map<String,String>{
		'Name' => NODE_NAME_STRING
	};
	public static final Map<String,String> rSuiteQuoteFinancingFieldTypeMap = new Map<String,String>{
		'sample' => NODE_NAME_STRING
	};
	public static final Map<String,String> rSuiteOrderFieldTypeMap = new Map<String,String>{
		'sample' => NODE_NAME_STRING 
	};
	public static final Map<String,String> rSuiteOIFieldTypeMap = new Map<String,String>{
		'Quantity' => NODE_NAME_REAL, 'Finger_Lifts__c' => NODE_NAME_STRING, 'Locks_Sash__c' => NODE_NAME_STRING,
		'Number_of_Panels__c' => NODE_NAME_STRING, 'Lifts_Pulls__c' => NODE_NAME_STRING
	};
	public static final Map<String,String> rSuiteOrderDiscountFieldTypeMap = new Map<String,String>{
		'sample' => NODE_NAME_STRING 
	};
	public static final Map<String,String> rSuiteOrderFinancingFieldTypeMap = new Map<String,String>{
		'sample' => NODE_NAME_STRING 
	};
	public static final Map<String,Map<String,String>> rSuiteObjectFieldTypeMap = new Map<String,Map<String,String>>{
		'Account' => rSuiteAccountFieldTypeMap, 'Contact' => rSuiteContactFieldTypeMap, 'Opportunity' => rSuiteOpportunityFieldTypeMap,
		'Quote' => rSuiteQuoteFieldTypeMap, 'QuoteLineItem' => rSuiteQLIFieldTypeMap, 'Quote_Discount__c' => rSuiteQuoteDiscountFieldTypeMap,
		'Quote_Financing__c' => rSuiteQuoteFinancingFieldTypeMap, 'Order' => rSuiteOrderFieldTypeMap, 'OrderItem' => rSuiteOIFieldTypeMap,
		'Order_Discount__c' => rSuiteOrderDiscountFieldTypeMap, 'Order_Financing__c' => rSuiteOrderFinancingFieldTypeMap
	};
	// end rSuite project file processing contants
	//////////////////////////////////////////////
}