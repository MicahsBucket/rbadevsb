/**
 * @description       : 
 * @author            : Connor.Davis@andersencorp.com
 * @group             : 
 * @last modified on  : 02-16-2021
 * @last modified by  : Connor.Davis@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                          Modification
 * 1.0   02-03-2021   Connor.Davis@andersencorp.com   Initial Version
**/
public without sharing class SalesSchedProcessDataWrapperss {
    //public boolean assignmentsCompleted  {get; set;}
    public Account store {get; set;}
    public List<Slot__c> slots {get; set;}
    public List<User> reps {get; set;}
    public Map<Id,List<UserRankS>> slotToUserRankMap {get; set;}
    public Map<Id,List<Sales_Appointment__c>> slotToAppointmentsMap {get; set;}
    public Map<Id,Id> appointmentsToSlotsMap {get; set;}
    public List<Sales_Appointment_Resource__c> assignments {get; set;} //these will be inserted at the end of the rule processing
    public Map<Id,Location> repToLastLocationMap {get; set;}
    public Date day {get; set;}
    public Map<Id,Sales_Appointment__c> appointmentsToUpdate {get; set;}
     public list<Sales_Appointment__c> appointMentSlots{get;set;}
    
    
     public SalesSchedProcessDataWrapperss(Id storeId,Date day){
        this.store = [Select Name,Active_Store_Configuration__r.Distance_Warning__c,Active_Store_Configuration__r.Slot_Group__c From Account Where Id = :storeId];
        this.day = day;
		this.assignments = new List<Sales_Appointment_Resource__c>();
    	this.repToLastLocationMap = new Map<Id,Location>();
        this.appointmentsToUpdate = new Map<Id,Sales_Appointment__c>();
        this.appointMentSlots = new list<Sales_Appointment__c>(); 
    }

    public void process(){

        this.slotToAppointmentsMap = new Map<Id,List<Sales_Appointment__c>>();
        this.appointmentsToSlotsMap = new Map<id,id>();

        System.debug('JWL: store: ' + store);
        System.debug('JWL: day: ' + day);

        String dayFormatted = ((Datetime)this.day).formatGmt('EEEE');

		this.slots =
            [Select
             	Id,
				Name,
				Start__c,
				End__c,
				Order__c
			 From
             	Slot__c
			 Where
             	Slot_Group__c = :this.store.Active_Store_Configuration__r.Slot_Group__c
            	And Day__c = :dayFormatted
             Order By
             Start__c Asc];
        System.debug('JWL: slots: ' + this.slots);

        this.slotToUserRankMap = new Map<Id,List<UserRankS>>();
        for(Slot__c slot : this.slots){
            this.slotToUserRankMap.put(slot.Id,new List<UserRankS>());
        }
        
	list<Sales_Appointment__c> apt = new list<Sales_Appointment__c>();
	list<Sales_Appointment__c> apt1 = new list<Sales_Appointment__c>();

        for(Sales_Appointment__c sa : [Select
             	Id,
				Sales_Order__r.Opportunity__r.Units__c,
                Appointment_Date_Time__c,
				Slot__c,
                Slot__r.Name,
             	Sales_Order__r.Opportunity__r.Account.ShippingAddress
			 From
             	Sales_Appointment__c
			 Where
             	Slot__r.Id IN :slots
				And Appointment_Date__c = :day
				And Sales_Appointment_Resources__c = 0
				And Sales_Order__r.Opportunity__r.Store_Location__r.Id = :store.Id
                And Sales_Order__r.Opportunity__r.Revisit__c= false                     
             Order By
             	Sales_Order__r.Opportunity__r.Units__c Desc,
                                       Appointment_Date_Time__c ASC]){
                                           if(sa.Sales_Order__r.Opportunity__r.Units__c ==99){
                                               apt1.add(sa);
                                           }else{
                                               apt.add(sa);
                                           } 
                                           
                                       }
        apt.addAll(apt1);
        for(Sales_Appointment__c sa:apt){
                
            appointmentsToSlotsMap.put(sa.Id, sa.Slot__c);
            appointMentSlots.add(sa);
            List<Sales_Appointment__c> appointments = this.slotToAppointmentsMap.get(sa.Slot__c);
            if(appointments == null){
                appointments = new List<Sales_Appointment__c>();
                this.slotToAppointmentsMap.put(sa.Slot__c,appointments);
             }
             appointments.add(sa);
        }
      	System.debug('JWL: slots: ' + this.appointMentSlots);

        Map<Id,User> userMap = new Map<Id,User>(
         	[Select
             	Id,
				Name,
             	Contact.MailingAddress,
				(Select
					Slot__c,
					Status__c,
					Store__c
				 From
					Sales_Capacities__r
				 Where
					Status__c = 'Available'
				    And Store__c = :store.Id
                 	And Date__c = :day
                	And Slot__c in :slotToUserRankMap.keySet())
			From
				User
			Where
				Store_Locations__c includes (:Store.Name)
				And IsActive = true
				And Profile.Name = 'Partner RMS-Sales']);

        Set<Id> capacityIds = new Set<Id>();
        for(User u : userMap.values()){
            Map<Id,Sales_Capacity__c> capacityMap = new Map<Id,Sales_Capacity__c>(u.Sales_Capacities__r);
            capacityIds.addAll(capacityMap.keySet());
        }

        Set<Id> assignedCapacities = new Set<Id>();
        for(Sales_Appointment_Resource__c sar : [Select Sales_Capacity__c From Sales_Appointment_Resource__c Where Sales_Appointment__r.Cancelled__c = false And Sales_Capacity__c in :capacityIds]){
            assignedCapacities.add(sar.Sales_Capacity__c);
        }

        List<UserRankS> userRanking = new List<UserRankS>();
        Map<Id,Sales_Rep_Report__c> reportMap = new Map<Id,Sales_Rep_Report__c>();
        for(Sales_Rep_Report__c srr :
            [Select
             	User__c,
             	Appointments_14_Days__c
             From
             	Sales_Rep_Report__c
             Where
             	Date__c = :day
            	And User__c in :userMap.keySet()
             Order By
            	CloseRate__c Desc,
            	Appointments_3_Days__c Asc,
             	RPA6__c Desc]){
            UserRankS ur = new UserRankS(userMap.get(srr.User__c),srr.Appointments_14_Days__c);
            userRanking.add(ur);
            userMap.remove(srr.User__c);
        }

        for(User u : userMap.values()){
            userRanking.add(new UserRankS(u,0));
        }

        Map<Id,Location> userIdToLastLocationMap = new Map<Id,Location>();
        for(UserRankS rep : userRanking){
            Set<Id> repAssignedSlots = new Set<Id>();
            repToLastLocationMap.put(rep.user.Id, rep.user.Contact.MailingAddress);
            for(Sales_Capacity__c capacity : rep.user.Sales_Capacities__r){
                if(!assignedCapacities.contains(capacity.Id) && !repAssignedSlots.contains(capacity.Slot__c)){
                    List<UserRankS> ranks = this.slotToUserRankMap.get(capacity.Slot__c);
                    UserRankS ur = new UserRankS(rep.user,rep.appointments14Days);
                    ur.capacityId = capacity.Id;
                    ur.slotId = capacity.Slot__c;
                    ranks.add(ur);
                    repAssignedSlots.add(capacity.Slot__c);
                }
            }
        }

        for(List<UserRankS> ranks : this.slotToUserRankMap.values()){
            Integer userRankHalfway = (ranks.size() / 2);
            for(Integer i = ranks.size()-1; i >= userRankHalfway; i--){
                UserRankS current = ranks.get(i);
              	if(current.appointments14Days == 0){
                    UserRankS halfway = ranks.get(userRankHalfway);
                    if(halfway.appointments14Days > 0){
                        ranks.add(userRankHalfway,current);
                        ranks.remove(i+1);
                    }
                }
            }
        }
        system.debug('slotToUserRankMap--->'+this.slotToUserRankMap);
    }
    
     public class UserRankS{
        public Id slotId {get; set;}
        public User user {get; private set;}
        public Id capacityId {get; set;}
        public Location previousLocation {get; private set;}
        public Decimal appointments14Days {get; set;}

        public UserRankS(User user,Decimal appointments14Days){
            this.user = user;
            this.appointments14Days = (appointments14Days == null) ?  0 : appointments14Days;
            this.previousLocation = user.Contact.MailingAddress;
        }
    }

    public SalesSchedProcessDataWrapperss processRuless(SalesSchedProcessDataWrapperss pdw){

        System.debug('JWL: in processRule');
        Sales_Schedule_Setting__mdt sss = [Select Distance_Warning__c From Sales_Schedule_Setting__mdt Where QualifiedApiName = 'Default'];

        Decimal distanceWarning = sss.Distance_Warning__c;
        if(pdw.store.Active_Store_Configuration__r.Distance_Warning__c != null){
            distanceWarning = pdw.store.Active_Store_Configuration__r.Distance_Warning__c;
        }
        
        set<string> salCap = new set<string>();

        //for(Slot__c slot : pdw.slots){
        for(Id sapp :pdw.appointmentsToSlotsMap.keyset()){	
          Id slot = pdw.appointmentsToSlotsMap.get(sapp);	
            List<Sales_Appointment__c> appointments = pdw.slotToAppointmentsMap.get(slot);
            List<UserRankS> repRanks = pdw.slotToUserRankMap.get(slot);

            boolean assignAppointments = !appointments.isEmpty() && !repRanks.isEmpty();
            while(assignAppointments){
                Sales_Appointment__c sa = appointments.get(0);
                Location appointmentLocation = sa.Sales_Order__r.Opportunity__r.Account.ShippingAddress;

                //Tie break for proximity.
                Location previousLocation = pdw.repToLastLocationMap.get(repRanks.get(0).user.Id);
                Integer repRankIndex = null;
                for(Integer i=0; repRankIndex == null && i < repRanks.size(); i++){
                    if(previousLocation == null || appointmentLocation == null || Location.getDistance(previousLocation, appointmentLocation, 'mi') <= distanceWarning){
                        repRankIndex = i;
                    }
                }

                if(repRankIndex != null){
                     if(!salCap.contains(repRanks.get(repRankIndex).capacityId)){
                    Id capacityId = repRanks.get(repRankIndex).capacityId;
					 salCap.add(capacityId);
                    pdw.assignments.add(
                        new Sales_Appointment_Resource__c(
                            Assignment_Reason__c = 'Auto Assignment'
                            ,Status__c = 'Assigned'
                            ,Primary__c = true
                            ,Sales_Capacity__c = capacityId
                            ,Sales_Appointment__c = sa.Id
                        )
                    );
                    System.debug('JWL: sales appointment resource added...');

                    pdw.appointmentsToUpdate.put(sa.Id,new Sales_Appointment__c(Id=sa.Id,Suggested_Capacity__c=capacityId));
					System.debug('JWL: added appointment to update for : ' + sa.Id);
                    
                    repToLastLocationMap.put(repRanks.get(repRankIndex).user.Id, appointmentLocation);
                    repRanks.remove(repRankIndex);
                     }
                }else{
                	System.debug('JWL: no reps found within proximity.');
                }
                appointments.remove(0);
                assignAppointments = !appointments.isEmpty() && !repRanks.isEmpty();
            }
        }
        System.debug('JW: appointments to update: ' + pdw.appointmentsToUpdate);

        return pdw;
    }
}