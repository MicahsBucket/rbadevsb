/**
    * @author Calvin O'Keefe, Slalom Consulting
    * @group Customer Portal
    * @date 1/18
    * @description finds the current order and also returns a list of old orders
    **/
    
    public without sharing class CollapsableContentController  {
    
        //finds the most current Order and uses that to populate the all the child components
        @AuraEnabled
        public static Id getOrderId() {
            Id userId = UserInfo.getUserId();
            User userRecord=[select id,Contactid from User where id=:userId];
            Id orderId = [SELECT Id FROM Order WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId))))  ORDER BY createdDate DESC LIMIT 1].Id;
            return orderId;
        }
    
        //returns a list of Orders that the user has been or is currently associated to
        @AuraEnabled
        public static List<Order> getOrders() {
            Id userId = UserInfo.getUserId();
            User userRecord=[select id,Contactid from User where id=:userId];
            List<Order> orderList = [SELECT Id, Order_Processed_Date__c FROM Order WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId))))  ORDER BY createdDate DESC];
            return orderList;
        }
          @AuraEnabled
        public static String getOrderPrimaryContact(String orderId) {
            Id userId = UserInfo.getUserId();
            Order objOrder = [SELECT Id, Primary_Contact__r.Name FROM Order WHERE Id = :orderId LIMIT 1];
            return objOrder.Primary_Contact__r.Name;
        }
         //returns the store info for the most recent order
       @AuraEnabled
       public static OrderStoreConfigWrap getStoreInfo() {
            Id userId = UserInfo.getUserId();
            User userRecord=[select id,Contactid from User where id=:userId];
            OrderStoreConfigWrap osConfig = new OrderStoreConfigWrap();
            Order thisOrder = [SELECT Id, Sales_Rep__c,Sales_Rep_Email__c,Sales_Rep_Phone__c,Primary_Installer_FSL__r.Name,EffectiveDate,
                               Primary_Tech_Measure_FSL__r.Name,Order_Concierge__r.Name,Primary_Tech_Measure_Email__c,Order_Concierge__r.Email,
                               Order_Concierge__r.FullPhotoUrl,Primary_Tech_Measure_FSL__r.RelatedRecord.FullPhotoUrl,
                               Primary_Installer_FSL__r.RelatedRecord.FullPhotoUrl,Opportunity.owner.FullPhotoUrl,
                               Store_Location__r.Active_Store_Configuration__r.Sales_Rep_Phone_Icon__c,
                               Store_Location__r.Active_Store_Configuration__r.Concierge_Phone_Icon__c,
                               Store_Location__r.Active_Store_Configuration__r.Measure_Technician_Phone_Icon__c,
                               Order_Concierge__r.Phone,Primary_Tech_Measure_FSL__r.RelatedRecord.Phone,
                               Store_Location__c, Primary_Contact__r.FirstName, Store_Location__r.Legal_Name__c, Store_Location__r.Phone,
                               Store_Location__r.BillingStreet, Store_Location__r.BillingCity, Store_Location__r.BillingPostalCode,
                               Store_Location__r.BillingState, Store_Location__r.BillingCountry,Store_Location__r.Active_Store_Configuration__c,
                               Store_Location__r.Active_Store_Configuration__r.Sales_Rep_Label__c,
                               Store_Location__r.Active_Store_Configuration__r.Measure_Technician_Label__c,
                               Store_Location__r.Active_Store_Configuration__r.Primary_Installer_Label__c,Store_Location__r.Active_Store_Configuration__r.Concierge_Label__c
                               FROM Order
                               WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId)))) 
                               ORDER BY createdDate DESC LIMIT 1];
             osConfig.myOrder = thisOrder;
             osConfig.myStore = thisOrder.Store_Location__r;
             osConfig.myStoreConfig = thisOrder.Store_Location__r.Active_Store_Configuration__r;
             osConfig.billToContact = thisOrder.Primary_Contact__r.FirstName;
             osConfig.techMeasureEmail = thisOrder.Primary_Tech_Measure_Email__c;
             osConfig.conciergeEmail = thisOrder.Order_Concierge__r.Email;
             osConfig.conciergeImage = thisOrder.Order_Concierge__r.FullPhotoUrl;
             osConfig.techMeasureImage = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.FullPhotoUrl;
             osConfig.primaryInstallerImage = thisOrder.Primary_Installer_FSL__r.RelatedRecord.FullPhotoUrl;
             osConfig.salesRepImage = thisOrder.Opportunity.owner.FullPhotoUrl;
             osConfig.concierge = thisOrder.Order_Concierge__r.Name;
             osConfig.techMeasure = thisOrder.Primary_Tech_Measure_FSL__r.Name;
             osConfig.conciergePhone = thisOrder.Order_Concierge__r.Phone;
             osConfig.techMeasurePhone = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.Phone;
             osConfig.salesRepPhone = thisOrder.Sales_Rep_Phone__c;
    
            return osConfig;
        }
    
        @AuraEnabled
        public static OrderStoreConfigWrap getStoreInfoUpdated(String orderId) {
           Id userId = UserInfo.getUserId();
           User userRecord=[select id,Contactid from User where id=:userId];
           OrderStoreConfigWrap osConfig = new OrderStoreConfigWrap();
           Order thisOrder = [SELECT Id, Sales_Rep__c,Sales_Rep_Email__c,Sales_Rep_Phone__c,Primary_Installer_FSL__r.Name,EffectiveDate,
                              Primary_Tech_Measure_FSL__r.Name,Order_Concierge__r.Name,Primary_Tech_Measure_Email__c,Order_Concierge__r.Email,
                              Order_Concierge__r.FullPhotoUrl,Primary_Tech_Measure_FSL__r.RelatedRecord.FullPhotoUrl,
                              Primary_Installer_FSL__r.RelatedRecord.FullPhotoUrl,Opportunity.owner.FullPhotoUrl,
                              Store_Location__r.Active_Store_Configuration__r.Sales_Rep_Phone_Icon__c,
                              Store_Location__r.Active_Store_Configuration__r.Concierge_Phone_Icon__c,
                              Store_Location__r.Active_Store_Configuration__r.Measure_Technician_Phone_Icon__c,
                              Order_Concierge__r.Phone,Primary_Tech_Measure_FSL__r.RelatedRecord.Phone,
                              Store_Location__c, Primary_Contact__r.FirstName, Store_Location__r.Legal_Name__c, Store_Location__r.Phone,
                              Store_Location__r.BillingStreet, Store_Location__r.BillingCity, Store_Location__r.BillingPostalCode,
                              Store_Location__r.BillingState, Store_Location__r.BillingCountry,Store_Location__r.Active_Store_Configuration__c,
                              Store_Location__r.Active_Store_Configuration__r.Sales_Rep_Label__c,
                              Store_Location__r.Active_Store_Configuration__r.Measure_Technician_Label__c,
                              Store_Location__r.Active_Store_Configuration__r.Primary_Installer_Label__c,Store_Location__r.Active_Store_Configuration__r.Concierge_Label__c
                              FROM Order
                              WHERE Id =:orderId
                              ORDER BY createdDate DESC LIMIT 1];
            osConfig.myOrder = thisOrder;
            osConfig.myStore = thisOrder.Store_Location__r;
            osConfig.myStoreConfig = thisOrder.Store_Location__r.Active_Store_Configuration__r;
            osConfig.billToContact = thisOrder.Primary_Contact__r.FirstName;
            osConfig.techMeasureEmail = thisOrder.Primary_Tech_Measure_Email__c;
            osConfig.conciergeImage = thisOrder.Order_Concierge__r.FullPhotoUrl;
            osConfig.techMeasureImage = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.FullPhotoUrl;
            osConfig.primaryInstallerImage = thisOrder.Primary_Installer_FSL__r.RelatedRecord.FullPhotoUrl;
            osConfig.salesRepImage = thisOrder.Opportunity.owner.FullPhotoUrl;
            osConfig.conciergeEmail = thisOrder.Order_Concierge__r.Email;
            osConfig.concierge = thisOrder.Order_Concierge__r.Name;
            osConfig.techMeasure = thisOrder.Primary_Tech_Measure_FSL__r.Name;
            osConfig.salesRepPhone = thisOrder.Sales_Rep_Phone__c;
            osConfig.conciergePhone = thisOrder.Order_Concierge__r.Phone;
            osConfig.techMeasurePhone = thisOrder.Primary_Tech_Measure_FSL__r.RelatedRecord.Phone;
            return osConfig;
       }
    
        public class OrderStoreConfigWrap{
            @AuraEnabled Public Order myOrder;
            @AuraEnabled Public Account myStore;
            @AuraEnabled Public Store_Configuration__c myStoreConfig;
            @AuraEnabled Public String billToContact;
            @AuraEnabled Public String concierge;
            @AuraEnabled Public String techMeasure;
            @AuraEnabled Public String techMeasureEmail;
            @AuraEnabled Public String conciergeEmail;
            @AuraEnabled Public String primaryInstallerImage;
            @AuraEnabled Public String conciergeImage;
            @AuraEnabled Public String techMeasureImage;
            @AuraEnabled Public String salesRepImage;
            @AuraEnabled Public String conciergePhone;
            @AuraEnabled Public String techMeasurePhone;
            @AuraEnabled Public String salesRepPhone;
    
        }
    
        //finds the order Id associated to the order the user desires to see the information for
        @AuraEnabled
        public static Id getNewOrderId(String processDate){
            Id userId = UserInfo.getUserId();
            Date pDate = date.valueOf(processDate);
            Id orderId = [SELECT Id FROM Order WHERE CustomerPortalUser__c = :userId AND Order_Processed_Date__c =:pDate LIMIT 1].Id;
            return orderId;
        }
        @AuraEnabled
        public static List<String> getOrderProcessedDates(){
            List<Order> ordList = [Select Id, Order_Processed_Date__c from order where CustomerPortalUser__c = :UserInfo.getUserId() order by CreatedDate DESC];
            List<String> allDates = new List<String>();
            for (Order ord: ordList){
                //ord.Order_Processed_Date__c
                String formattedDate = ord.Order_Processed_Date__c.format();
                allDates.add(formattedDate);
            }
            return allDates;
        }
        ////////////////////////////////////////////////////////////
        // methods below created by Mark Rothermal - inital use in ProjectHeader Component
        ////////////////////////////////////////////////////////////
        @AuraEnabled
        public static LightningResponse getSelectedOrder(){
            LightningResponse lr = new LightningResponse();
            try{
                user u = [Select id,Selected_Order__c from User where id = :UserInfo.getUserId() limit 1];
                lr.jsonResponse = JSON.serialize(u);
            } catch(exception e){
                lr = new LightningResponse(e);
            }
            return lr;
        }
        @AuraEnabled
        public static LightningResponse updateSelectedOrder(id orderId){
            system.debug(orderId);
            LightningResponse lr = new LightningResponse();
            try{
                User u = new User();
                u.id = UserInfo.getUserId();
                u.Selected_Order__c = orderId;
                system.debug(u);
                update u;
                lr.jsonResponse = JSON.serialize(u);
            }catch(exception e){
                lr = new LightningResponse(e);
            }
            return lr;
        }
        @AuraEnabled
        public static LightningResponse getAllOrders() {
            LightningResponse lr = new LightningResponse();
            Id userId = UserInfo.getUserId();
            User userRecord=[select id,Contactid from User where id=:userId];
            try{
                List<Order> orders = [SELECT Id,Order_Processed_Date__c,Discount_Amount__c,Retail_Subtotal__c, Payments_Received__c,
                                      Amount_Financed__c, Amount_Due__c, Retail_Total__c,OpportunityCloseDate__c,
                                      Store_Location__c, Store_Location__r.Legal_Name__c, Store_Location__r.Phone,
                                      Store_Location__r.BillingStreet, Store_Location__r.BillingCity, Store_Location__r.BillingPostalCode,
                                      Store_Location__r.BillingState, Store_Location__r.BillingCountry, Store_Location__r.Active_Store_Configuration__r.Display_Unit_Prices__c,
                                      Store_Location__r.Active_Store_Configuration__r.Display_Total_Price__c, Store_Location__r.Active_Store_Configuration__r.Display_Discount_Price__c,Sales_Rep__c, Primary_Installer__c,
                                      Primary_Tech_Measure__c FROM Order WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId))))  ORDER BY createdDate DESC];
                lr.jsonResponse = JSON.serialize(orders);
            } catch(exception e){
                lr = new LightningResponse(e);
            }
            return lr;
        }
      ///////////////////////////////////////////////////////////////////////////////////////////////////////
    }