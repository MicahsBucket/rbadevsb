@IsTest
private class Renewal_BaseControllerTest {
    @IsTest(SeeAllData=true)
    static void testCheckIfSuperAdminTrue() {
        User testUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()][0];
        Set<String> permissionLabels = new Set<String>();
        for(Renewal_Super_Admin_Permission_Set__mdt adminPermissionSet : [SELECT Id, Label FROM Renewal_Super_Admin_Permission_Set__mdt]){
            permissionLabels.add(adminPermissionSet.Label);
        }
        Set<String> assignedLabels = new Set<String>();
        for(PermissionSetAssignment userPermission : [SELECT Id, PermissionSet.Name, PermissionSet.Label,AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :UserInfo.getUserId() AND PermissionSet.Label IN :permissionLabels]){
            assignedLabels.add(userPermission.PermissionSet.Label);
        }
        Test.startTest();
        permissionLabels.removeAll(assignedLabels);
        if(!permissionLabels.isEmpty()){
            List<PermissionSetAssignment> testAssignments = new List<PermissionSetAssignment>();
            for(PermissionSet testPermissionSet : [SELECT Id FROM PermissionSet WHERE Label IN :permissionLabels]){
                PermissionSetAssignment testAssignment = new PermissionSetAssignment();
                testAssignment.AssigneeId = testUser.Id;
                testAssignment.PermissionSetId = testPermissionSet.Id;
                testAssignments.add(testAssignment);
            }
            insert testAssignments;
        }
        Renewal_Response testResponse = Renewal_BaseController.checkIfSuperAdmin(false);
        System.assert(testResponse.success == true);
        //System.assert(testResponse.renewalResults[0].rforceAdmin == true);
        //System.assert(testResponse.renewalResults[0].dashboardAdmin == true);
        Test.stopTest();
    }
    @IsTest
    static void testCheckIfSuperAdminException() {
        Test.startTest();
        Renewal_Response testResponse = Renewal_BaseController.checkIfSuperAdmin(true);
        System.assert(testResponse.success == false);
        Test.stopTest();
    }
}