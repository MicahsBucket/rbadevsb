public with sharing class MultipleWorkOrderLineItemsController {

    @AuraEnabled
    public static List<WorkOrderLineItem> getWorkOrderLineItems(String workOrderId){

        Map<Id, WorkOrderLineItem> workOrderLineItems = new Map<Id, WorkOrderLineItem>(
            [SELECT Id,
            List_Index__c,
            LineItemNumber,
            Subject,
            Description,
            Job_Notes__c,
            Status,
            WorkOrderId,
            Visibility__c,
            Line_Item_Type__c
            FROM WorkOrderLineItem
            WHERE WorkOrderId = :workOrderId
            ORDER BY List_Index__c ASC NULLS LAST
            ]);

        return workorderlineitems.values();
    }

    @AuraEnabled
    public static SaveResultsWrapper setWorkOrderLineItems(String workOrderId, String woliDetails){
        List<WorkOrderLineItem> workOrderLineItems = (List<WorkOrderLineItem>)JSON.deserialize(woliDetails, List<WorkOrderLineItem>.class);
        SaveResultsWrapper result = new SaveResultsWrapper();

        try {
            upsert workOrderLineItems;
        } catch(DmlException e){
            System.debug(e);
            return result;
        }

        result.isSuccess = true;
        result.updatedData = getWorkOrderLineItems(workOrderId);
        return result;
    }

    @AuraEnabled
    public static Boolean deleteLineItem(String woliId){
        WorkOrderLineItem woli = [
            SELECT Id
            FROM WorkOrderLineItem
            WHERE Id = :woliId
            LIMIT 1
        ];

        try {
            delete woli;
        } catch(DmlException e){
            System.debug(e);
            return false;
        }

        return true;
    }

    @AuraEnabled
    public static List<String> getWOLIStatuses(){
        Schema.DescribeFieldResult fieldResult = WorkOrderLineItem.Status.getDescribe();
        List<Schema.PicklistEntry> statuses = fieldResult.getPicklistValues();
        List<String> statusOptions = new List<String>();

        for(Schema.PicklistEntry s : statuses){
            statusOptions.add(s.getValue());
        }

        return statusOptions;
    }

    public class SaveResultsWrapper{
        @AuraEnabled public Boolean isSuccess = false;
        @AuraEnabled public List<WorkOrderLineItem> updatedData = new List<WorkOrderLineItem>();
    }

}