/******************************************************************
* Author : Sundeep
* Class Name :PostServiceSurveyHandler
* Createdate :
* Description : Methods for Creating the Service Survey
* Change Log
* ---------------------------------------------------------------------------
* Date:  11/1/2019       Name: Sundeep Goddety         Description
* ---------------------------------------------------------------------------
* 
* 
* ----------------------------------------------------------------------------
* *****************************************************************/
public class PostServiceSurveyHandler {
    public static Final string CLASSNAME ='PostServiceSurveyHandler';
    public static void surveyServiceProcessCreation(list<Order> scope){
        try {  
            list<Order> ordsList = new list<Order>();
            Map<Id,Id> LegacyOrdActMap = new Map<Id,Id>();
            Map<id,id> soldOrdMap = new Map<Id,Id>();
            Set<Id> actIds = new set<Id>();
            Set<Id> OrdIds = new set<Id>();
            Map<Id,Opportunity> salOppMap = new Map<Id,Opportunity>();
            set<Id> serOrds = new set<Id>();
            string serStatus = 'Service,Save,Field Service,Legacy';
            Map<Id,Id> billToContSingle = new Map<Id,Id>();
            Map<Id,Id> billToCon = new Map<Id,Id>();
            Map<Id,contact> sigleBillToContactMap = new Map<Id,contact>();
            String sourceSystem = 'rForce';
            Id SurveyrecordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get('Service').getRecordTypeId();
            Map<Id,set<id>> serviceOrdWorkOrd = new Map<Id,set<id>>();
            Map<Id,Id> storeMap = new Map<Id,Id>();
            for(Order od: scope){
                
                storeMap.put(od.id, od.Store_Location__c);
                if(od.BillToContactid == null && od.Sold_Order__c == null ) {
                    //Need to transverse Accout--> Contact History--> Contact and get the date from Order and Opportunity Related Lists
                    LegacyOrdActMap.put(od.Id,od.AccountId);
                }
                else if(od.BillToContactid!= null && od.Sold_Order__c != null ){
                    // Get the data that's avaliable in sold Order and Bill To Contact
                    soldOrdMap.put(od.Id,od.Sold_Order__c);
                    billToCon.put(od.Id, od.BillToContactId);
                }else if(od.BillToContactid!= null && od.Sold_Order__c == null){
                    
                    // need to transverse contact ---> opportunity -->order
                    billToContSingle.put(od.Id, od.BillToContactId);
                }
                
                serOrds.add(od.Id);
            }
            string stfilter = 'Appt Complete / Closed'; 	

            Map<id,Integer> workOrdeRecOunt = new Map<Id,integer>();
            set<Id> primarySourceIds = new set<Id>();
            // Getting the Counting of the Workorder with Status as 'Appt Complete / Closed' only in Service Request Records
            if(!serOrds.isEmpty()){
                for( AggregateResult  result : [SELECT COUNT(Id)rCount,Sold_Order__c  FROM WorkOrder WHERE Sold_Order__c IN : serOrds and Status =:stfilter GROUP BY Sold_Order__c]){
                    workOrdeRecOunt.put((Id)result.get('Sold_Order__c'), (Integer)result.get('rCount'));                
                    
                } 
                
                for(WorkOrder wOrd: [Select Id,Sold_Order__c,Primary_Service_FSL__r.RelatedRecordId from WorkOrder where Sold_Order__c IN : serOrds and Status =:stfilter ]){
                    system.debug('wOrd----------->'+wOrd);
                    if(wOrd.Primary_Service_FSL__r.RelatedRecordId!=null){
                        primarySourceIds.add(wOrd.Primary_Service_FSL__r.RelatedRecordId);                        
                    }                    
                    if(!serviceOrdWorkOrd.containsKey(wOrd.Sold_Order__c)){
                        if(wOrd.Primary_Service_FSL__r.RelatedRecordId!=null){
                            serviceOrdWorkOrd.put(wOrd.Sold_Order__c, new set<Id>{wOrd.Primary_Service_FSL__r.RelatedRecordId});
                        }else{
                            serviceOrdWorkOrd.put(wOrd.Sold_Order__c, new set<Id>());  
                        }
                    }else{
                        serviceOrdWorkOrd.get(wOrd.Sold_Order__c).add(wOrd.Primary_Service_FSL__r.RelatedRecordId);
                    }
                }
                
            }
            system.debug('serviceOrdWorkOrd------>'+serviceOrdWorkOrd);
            Map<Id,user> userMap = new Map<Id,User>();
            Map<Id,Worker__c > existingWrkrMap = new Map<Id,Worker__c >();
            Map<string,Worker__c> FinalOrdPriSorcWorkMap = New Map<string,Worker__c>();
            Map<string,Worker__c> newworkerCreMap = new Map<string,Worker__c>();
            if(!primarySourceIds.isEmpty()){
                for(User U: [SELECT Id, Email, FirstName, LastName, MobilePhone, Enabled_User_Id__c, Pivotal_Id__c FROM User WHERE Id IN :primarySourceIds]){
                    userMap.put(u.id,u);
                }
                For(Worker__c existingWorkers : [SELECT Id, Enabled_User_Id__c, User__c FROM Worker__c WHERE  User__c IN :primarySourceIds ]){
                    existingWrkrMap.put(existingWorkers.User__c,existingWorkers);
                    
                } 
                
            }
            if(!serviceOrdWorkOrd.isEmpty()){
                for(Id serviceOrderId : serviceOrdWorkOrd.keySet() ){
                    
                    
                    if(!serviceOrdWorkOrd.get(serviceOrderId).isEmpty()){
                        system.debug('serviceOrderId------>'+serviceOrderId);
                        for(Id prmyUserId: serviceOrdWorkOrd.get(serviceOrderId)){
                            if(existingWrkrMap.containskey(prmyUserId)){
                                system.debug('inside existing worker------>');
                                FinalOrdPriSorcWorkMap.put(serviceOrderId+''+prmyUserId, existingWrkrMap.get(prmyUserId));                                
                            }else{                                
                                user u = userMap.get(prmyUserId);
                                boolean isDes = true;
                                Decimal enabledDec;
                                if(u.Enabled_User_Id__c != null){
                                    try{
                                        enabledDec = Decimal.valueOf(u.Enabled_User_Id__c);
                                        
                                    }
                                    catch(exception e){
                                        isDes = false;
                                    }
                                }
                                Worker__c newWorker = new Worker__c();
                                newWorker.Email__c = u.Email;
                                if(isDes){
                                    newWorker.Enabled_User_Id__c = enabledDec;
                                }                                
                                newWorker.First_Name__c = u.FirstName;
                                newWorker.Last_Name__c = u.LastName;
                                newWorker.Mobile__c = u.MobilePhone;
                                //  Pivotal_Employee_Id__c = u.Pivotal_Id__c,
                                newWorker.User__c = u.Id;
                                newWorker.Role__c = 'Service Tech';
                                newWorker.Store_Account__c = storeMap.get(serviceOrderId);
                                newWorker.Source_System__c = 'rForce';
                                system.debug('inside new  worker------>');
                                
                                newworkerCreMap.put(serviceOrderId+''+prmyUserId, newWorker);
                            }
                            
                            
                        }
                    }
                    
                }
                
            }
            if(!newworkerCreMap.isEmpty()){
                database.insert(newworkerCreMap.values(),false);
                system.debug('newworkerCreMap----'+newworkerCreMap);
                
            }
            FinalOrdPriSorcWorkMap.putAll(newworkerCreMap);
            system.debug('FinalOrdPriSorcWorkMap----'+FinalOrdPriSorcWorkMap);
            Map<Id,Opportunity> ActOrtyMap = new Map<Id,Opportunity>();
            Map<Id,integer> OptyCount = new Map<id,integer>();
            Map<Id,Contact_History__c> contHist = New Map<Id,Contact_History__c>();
            Map<Id,Order> ActOrdMap = new Map<Id,Order>();
            Map<Id,integer> OrdCount = new Map<Id,integer>();
            Map<Id,Account> nobillStoreActMap = new Map<Id,Account>();
            Map<Id,Account> billtoContAccountSt = new Map<Id,Account>();
            if(!LegacyOrdActMap.isEmpty()){
                //Creating a Map of Account to Order, Opportunity and Getting the Size of the Orders, Opportunities
                for(Account act: [select id, name,Store_Location__c,(Select Id,Name,EffectiveDate,Installation_Date__c,OrderNumber,AccountId From Orders),(select  Id, Name,Enabled_Lead_Id__c,Enabled_Appointment_Id__c,Pivotal_Id__c,LeadSource,AccountId From Opportunities)
                                  from account where Id IN  :LegacyOrdActMap.values()]){
                                      nobillStoreActMap.put(act.id,act);
                                      if(!act.Opportunities.isEmpty() && act.Opportunities != null ){
                                          OptyCount.put(act.Id, act.Opportunities.size());
                                          if(act.Opportunities.size() == 1){
                                              for(Opportunity op : act.Opportunities){
                                                  ActOrtyMap.put(act.Id,op);
                                                  //salOppMap.put(op.Id,Op);
                                              }
                                              
                                          }
                                      } 
                                      if(!act.Orders.isEmpty() && act.Orders!= null){
                                          if(act.Orders.size() == 1){
                                              for(Order ord : act.Orders){
                                                  ActOrdMap.put(act.Id,ord);
                                              } 
                                              
                                          }
                                          
                                      }   
                                      
                                  }
                //Creating a Map of Account to Contact History and Contact
                for(Contact_History__c ch:[select Dwelling__c,Contact__c,Contact__r.MailingState,Contact__r.MailingStreet, Contact__r.MailingPostalCode,
                                           Contact__r.MailingCity,Contact__r.MailingCountry,Contact__r.Email,Contact__r.FirstName,
                                           Contact__r.LastName,Contact__r.HomePhone,Contact__r.MobilePhone,Contact__r.Phone from Contact_History__c where Dwelling__c In : LegacyOrdActMap.values() and Type__c ='Current Resident' and
                                           Primary_Contact__c	=true]){
                                               contHist.put(ch.Dwelling__c, ch);                      
                                           }
            }
            
            
            Map<id,Order> salOrdOptyMap = new Map<Id,Order>();
            Map<Id,List<OpportunityContactRole>> mapOpportunityIdToPrimaryContactIdList = new Map<Id,List<OpportunityContactRole>>();
            Map<Id,List<OpportunityContactRole>> mapOpportunityIdToSecondaryContactIdList = new Map<Id,List<OpportunityContactRole>>();
            if(!soldOrdMap.isEmpty()){
                //create a map of Sold Order Ids to Orders
                for(Order salOrd : [select id,Name,EffectiveDate,Installation_Date__c,OpportunityId,Opportunity.Store_Location__c, Opportunity.Enabled_Lead_Id__c,Opportunity.Enabled_Appointment_Id__c,Opportunity.Pivotal_Id__c,Opportunity.LeadSource from Order where Id In : soldOrdMap.values() ]){
                    salOrdOptyMap.put(salOrd.Id,salOrd);
                    
                }
            }
            
            Map<Id,Contact> ContctsMap = new Map<Id,Contact>();
            Map<Id,id> singleBillToConAct = new Map<Id,id>();
            Map<Id,Opportunity> singleBilltoConOpty = New Map<Id,Opportunity>();
            Map<Id,Order> singleBillToConOrdr = new Map<Id,Order>();
            
            if(!billToCon.values().isEmpty()){
                //create a map of BillToContact Ids to Contact 
                for(contact conts: [Select Id,MailingState,MailingStreet, MailingPostalCode,MailingCity,MailingCountry,Email,FirstName,
                                    LastName,HomePhone,MobilePhone,Phone From Contact where Id In : billToCon.values()]){
                                        ContctsMap.put(conts.Id, conts);
                                        
                                    }
                
            }
            
            //This is Used when there is no Sold Order 
            if(!billToContSingle.values().isEmpty()){
                //create a map of BillToContact Ids to Contact 
                for(contact conts: [Select Id, Accountid,MailingState,MailingStreet, MailingPostalCode,MailingCity,MailingCountry,Email,FirstName,
                                    LastName,HomePhone,MobilePhone,Phone From Contact where Id In : billToContSingle.values()]){
                                        sigleBillToContactMap.put(conts.Id, conts);
                                        singleBillToConAct.put(conts.Id,conts.Accountid); 
                                    }
                
                
                if(!singleBillToConAct.isEmpty()){
                    for(Account act: [select id, name, Store_Location__c,(Select Id,Name,EffectiveDate,Installation_Date__c,OrderNumber,AccountId From Orders),(select  Id, Name,Enabled_Lead_Id__c,Enabled_Appointment_Id__c,Pivotal_Id__c,LeadSource,AccountId From Opportunities)
                                      from account where Id IN  :singleBillToConAct.values()]){
                                          billtoContAccountSt.put(act.id, act);              
                                          if(!act.Opportunities.isEmpty() && act.Opportunities != null ){
                                              if(act.Opportunities.size() == 1){
                                                  for(Opportunity op : act.Opportunities){
                                                      singleBilltoConOpty.put(act.Id,op);
                                                      
                                                  }
                                                  
                                              }
                                          } 
                                          System.debug('Single Bill to Contact Orders Section1111 ' + act.Orders);
                                          System.debug('Single Bill to Contact Orders Section.size ' + act.Orders.size());
                                          if(!act.Orders.isEmpty() && act.Orders!= null){
                                              System.debug('Single Bill to Contact Orders Section2222 ' + act.Orders);
                                              if(act.Orders.size() == 1){
                                                  System.debug('Single Bill to Contact Orders Section333 ' + act.Orders);
                                                  for(Order ord : act.Orders){
                                                      singleBillToConOrdr.put(act.Id,ord);
                                                  } 
                                                  
                                              }
                                              
                                          }   
                                          
                                      }
                    
                }
                System.debug('End of the Single Bill to Contact :++' + singleBillToConOrdr);
            }
            
            // Survey Creation
            
            Map<Id, Survey__c> primaryContactSurveyMap = new Map<Id, Survey__c>();
            Map<Id, Survey__c> secondaryContactSurveyMap = new Map<Id, Survey__c>();        
            List<Survey__c> surveyList = new List<Survey__c>();
            for(Order ords: scope){
                
                if(ords.BillToContactid== null && ords.Sold_Order__c == null ) {
                    Id  AccountId = LegacyOrdActMap.get(ords.id);
                    if(contHist.get(AccountId)!= null){   
                        Survey__c newSurvey = new Survey__c(); 
                        newSurvey.RecordTypeId = SurveyrecordTypeId;
                        newSurvey.Contact_Name__c = contHist.get(AccountId).Contact__c;
                        newSurvey.Primary_Contact_First_Name__c = contHist.get(AccountId).Contact__r.FirstName;
                        newSurvey.Primary_Contact_Last_Name__c = contHist.get(AccountId).Contact__r.LastName;
                        newSurvey.Primary_Contact_Email__c = contHist.get(AccountId).Contact__r.Email;
                        newSurvey.Primary_Contact_Home_Phone__c = contHist.get(AccountId).Contact__r.HomePhone;
                        newSurvey.Primary_Contact_Work_Phone__c = contHist.get(AccountId).Contact__r.Phone;
                        newSurvey.Primary_Contact_Mobile_Phone__c = contHist.get(AccountId).Contact__r.MobilePhone;
                        newSurvey.State__c = contHist.get(AccountId).Contact__r.MailingState;
                        newSurvey.City__c = contHist.get(AccountId).Contact__r.MailingCity;
                        newSurvey.Country__c = contHist.get(AccountId).Contact__r.MailingCountry;
                        newSurvey.Street__c = contHist.get(AccountId).Contact__r.MailingStreet;
                        newSurvey.Zip__c = contHist.get(AccountId).Contact__r.MailingPostalCode;
                        newSurvey.Store_Account__c = nobillStoreActMap.get(AccountId).Store_Location__c;
                        newSurvey.Service_Request_Number__c = ords.Id;
                        newSurvey.Service_Type__c = ords.Service_Type__c;
                        newSurvey.Service_Number_of_Visits__c =  workOrdeRecOunt.get(ords.Id);
                        newSurvey.Service_Description__c = ords.Description;
                        newSurvey.Service_Submission_Date__c = ords.Revenue_Recognized_Date__c;
                        newSurvey.Warranty__c = ords.Warranty_Date_Submitted__c!=null ? true : false;
                        newSurvey.Source_System__c = 'rForce';
                        
                        if(ActOrtyMap.get(AccountId)!= null){
                            newSurvey.Opportunity__c =  ActOrtyMap.get(AccountId).Id;
                            newSurvey.Enabled_Appointment_Id__c = ActOrtyMap.get(AccountId).Enabled_Appointment_Id__c;
                            newSurvey.Enabled_Lead_Id__c = ActOrtyMap.get(AccountId).Enabled_Lead_Id__c;
                            newSurvey.Pivotal_Rn_AppointmentID__c = ActOrtyMap.get(AccountId).Pivotal_Id__c;
                            newSurvey.Lead_Source__c = ActOrtyMap.get(AccountId).LeadSource;        
                            
                        } 
                        if(ActOrdMap.get(AccountId)!= null){
                            newSurvey.Order_Name__c = ActOrdMap.get(AccountId).Id;  
                            newSurvey.Appointment_Date__c = ActOrdMap.get(AccountId).EffectiveDate;
                            newSurvey.Installation_Date__c = ActOrdMap.get(AccountId).Installation_Date__c;
                        }
                        
                        
                        primaryContactSurveyMap.put(newSurvey.Contact_Name__c, newSurvey);
                        surveyList.add(newSurvey);
                        
                    }  
                    
                    
                }
                else if(ords.BillToContactid!= null && ords.Sold_Order__c != null ){
                    Id contId =  billToCon.get(ords.Id);
                    
                    Order soldOrder =  salOrdOptyMap.get(ords.Sold_Order__c);
                    
                    if(ContctsMap.get(contId)!= null){                    
                        Survey__c newSurvey = new Survey__c(); 
                        newSurvey.RecordTypeId = SurveyrecordTypeId;
                        newSurvey.Contact_Name__c = ContctsMap.get(contId).id;
                        newSurvey.Primary_Contact_First_Name__c = ContctsMap.get(contId).FirstName;
                        newSurvey.Primary_Contact_Last_Name__c = ContctsMap.get(contId).LastName;
                        newSurvey.Primary_Contact_Email__c = ContctsMap.get(contId).Email;
                        newSurvey.Primary_Contact_Home_Phone__c = ContctsMap.get(contId).HomePhone;
                        newSurvey.Primary_Contact_Work_Phone__c = ContctsMap.get(contId).Phone;
                        newSurvey.Primary_Contact_Mobile_Phone__c = ContctsMap.get(contId).MobilePhone;
                        newSurvey.State__c = ContctsMap.get(contId).MailingState;
                        newSurvey.City__c = ContctsMap.get(contId).MailingCity;
                        newSurvey.Country__c = ContctsMap.get(contId).MailingCountry;
                        newSurvey.Street__c = ContctsMap.get(contId).MailingStreet;
                        newSurvey.Zip__c = ContctsMap.get(contId).MailingPostalCode;
                        newSurvey.Opportunity__c = soldOrder.opportunityId;
                        newSurvey.Enabled_Appointment_Id__c = soldOrder.Opportunity.Enabled_Appointment_Id__c;
                        newSurvey.Enabled_Lead_Id__c = soldOrder.Opportunity.Enabled_Lead_Id__c;
                        newSurvey.Pivotal_Rn_AppointmentID__c = soldOrder.Opportunity.Pivotal_Id__c;
                        newSurvey.Lead_Source__c = soldOrder.Opportunity.LeadSource;
                        newSurvey.Order_Name__c = soldOrder.Id;
                        newSurvey.Appointment_Date__c = soldOrder.EffectiveDate;
                        newSurvey.Installation_Date__c = soldOrder.Installation_Date__c;
                        newSurvey.Service_Request_Number__c = ords.Id;
                        newSurvey.Store_Account__c = soldOrder.Opportunity.Store_Location__c;
                        newSurvey.Service_Request_Number__c = ords.Id;
                        newSurvey.Service_Type__c = ords.Service_Type__c;
                        newSurvey.Service_Number_of_Visits__c =  workOrdeRecOunt.get(ords.Id);
                        newSurvey.Service_Description__c = ords.Description;
                        newSurvey.Service_Submission_Date__c = ords.Revenue_Recognized_Date__c;
                        newSurvey.Warranty__c = ords.Warranty_Date_Submitted__c!=null ? true : false;
                        newSurvey.Source_System__c = 'rForce';
                        /*  if(!secondOptyContRl.isEmpty() ){//&& secondOptyContRl[0].contactId != null){
newSurvey.Secondary_Contact_First_Name__c = secondOptyContRl[0].Contact.FirstName;
newSurvey.Secondary_Contact_Last_Name__c = secondOptyContRl[0].Contact.LastName;
newSurvey.Secondary_Contact_Email__c = secondOptyContRl[0].Contact.Email;
newSurvey.Secondary_Contact_Home_Phone__c = secondOptyContRl[0].Contact.HomePhone;
newSurvey.Secondary_Contact_Mobile_Phone__c = secondOptyContRl[0].Contact.MobilePhone;
newSurvey.Secondary_Contact_Work_Phone__c = secondOptyContRl[0].Contact.Phone;
secondaryContactSurveyMap.put(secondOptyContRl[0].contactId, newSurvey); 

}*/
                        primaryContactSurveyMap.put(newSurvey.Contact_Name__c, newSurvey);
                        surveyList.add(newSurvey);
                        
                        
                    }
                    
                    
                }
                else if(ords.BillToContactid!= null && ords.Sold_Order__c == null){
                    Id contId =  billToContSingle.get(ords.id);
                    Id actId =    singleBillToConAct.get(contId);
                    System.debug('Single BIll to contact Survey Cretion:'+singleBillToConOrdr.get(actId));
                    if(sigleBillToContactMap.get(contId)!= null){
                        Survey__c newSurvey = new Survey__c();
                        newSurvey.RecordTypeId = SurveyrecordTypeId;                    
                        newSurvey.Contact_Name__c = sigleBillToContactMap.get(contId).id;
                        newSurvey.Primary_Contact_First_Name__c = sigleBillToContactMap.get(contId).FirstName;
                        newSurvey.Primary_Contact_Last_Name__c = sigleBillToContactMap.get(contId).LastName;
                        newSurvey.Primary_Contact_Email__c = sigleBillToContactMap.get(contId).Email;
                        newSurvey.Primary_Contact_Home_Phone__c = sigleBillToContactMap.get(contId).HomePhone;
                        newSurvey.Primary_Contact_Work_Phone__c = sigleBillToContactMap.get(contId).Phone;
                        newSurvey.Primary_Contact_Mobile_Phone__c = sigleBillToContactMap.get(contId).MobilePhone;
                        newSurvey.State__c = sigleBillToContactMap.get(contId).MailingState;
                        newSurvey.City__c = sigleBillToContactMap.get(contId).MailingCity;
                        newSurvey.Country__c = sigleBillToContactMap.get(contId).MailingCountry;
                        newSurvey.Street__c = sigleBillToContactMap.get(contId).MailingStreet;
                        newSurvey.Zip__c = sigleBillToContactMap.get(contId).MailingPostalCode;
                        newSurvey.Service_Request_Number__c = ords.Id;
                        newSurvey.Service_Type__c = ords.Service_Type__c;
                        newSurvey.Service_Number_of_Visits__c =  workOrdeRecOunt.get(ords.Id);
                        newSurvey.Service_Description__c = ords.Description;
                        newSurvey.Service_Submission_Date__c = ords.Revenue_Recognized_Date__c;
                        newSurvey.Warranty__c = ords.Warranty_Date_Submitted__c!=null ? true : false;
                        newSurvey.Store_Account__c = billtoContAccountSt.get(actId).Store_Location__c;
                        newSurvey.Source_System__c = 'rForce';
                        if(singleBilltoConOpty.get(actId)!= null){
                            newSurvey.Opportunity__c =  singleBilltoConOpty.get(actId).Id;
                            newSurvey.Enabled_Appointment_Id__c = singleBilltoConOpty.get(actId).Enabled_Appointment_Id__c;
                            newSurvey.Enabled_Lead_Id__c = singleBilltoConOpty.get(actId).Enabled_Lead_Id__c;
                            newSurvey.Pivotal_Rn_AppointmentID__c = singleBilltoConOpty.get(actId).Pivotal_Id__c;
                            newSurvey.Lead_Source__c = singleBilltoConOpty.get(actId).LeadSource;        
                            
                        } 
                        if(singleBillToConOrdr.get(actId)!= null){
                            newSurvey.Order_Name__c = singleBillToConOrdr.get(actId).Id; 
                            newSurvey.Appointment_Date__c = singleBillToConOrdr.get(actId).EffectiveDate;
                            newSurvey.Installation_Date__c = singleBillToConOrdr.get(actId).Installation_Date__c;
                        }
                        primaryContactSurveyMap.put(newSurvey.Contact_Name__c, newSurvey);
                        surveyList.add(newSurvey);
                        
                    }
                    
                }
                
            }
            Set<Id> servids = new Set<Id>();
            if(!surveyList.isEmpty()){

                List<Database.saveResult> results = Database.insert(surveyList, false);
                list<Database.Error> errList = new list<Database.Error>();
                for(Database.saveResult result:results) {
                    
                    if(result.isSuccess()){
                        
                        servids.add(result.getId());
                    }else{
                        for(Database.Error err : result.getErrors()) {
                            System.debug('The following error has occurred.');             
                            
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            
                            System.debug('Survey fields that affected this error: ' + err.getFields());
                            
                        }
                        
                    }
                }
                
                
            }
            List<Survey_Worker__c> junctionsToCreate = new List<Survey_Worker__c>();
            
            List<Contact_Survey__c> contactSurveyJuncList = new List<Contact_Survey__c>();
            if(!servids.IsEmpty()){
                Map<id,Id> serOrdServeyIds = new Map<Id,Id>();
                for(Survey__c sur:[SELECT Id,Service_Request_Number__c From Survey__c where Id IN:servids]){
                    serOrdServeyIds.put(sur.Service_Request_Number__c, sur.Id);
                    system.debug('sur-----------'+sur);
                }
                if(!serOrdServeyIds.IsEmpty()){
                    for(Id serOrdId:serOrdServeyIds.keySet() ){
                        system.debug('serOrdId-----------'+serOrdId);
                        if(serviceOrdWorkOrd.containsKey(serOrdId) && !serviceOrdWorkOrd.get(serOrdId).isEmpty()){
                            system.debug(' serviceOrdWorkOrd.get(serOrdId)-----------'+ serviceOrdWorkOrd.get(serOrdId));
                            for(Id userId : serviceOrdWorkOrd.get(serOrdId)){
                                system.debug('userId-----------'+userId);
                                Survey_Worker__c newSalRepSurJn = new Survey_Worker__c();
                                newSalRepSurJn.Survey_Id__c = serOrdServeyIds.get(serOrdId);
                                newSalRepSurJn.Worker_Id__c = FinalOrdPriSorcWorkMap.get(serOrdId+''+userId).Id;
                                newSalRepSurJn.Role__c = 'Service Tech';
                                
                                junctionsToCreate.add(newSalRepSurJn); 
                            }
                        }
                        
                    }
                    
                }
                
                
                for(Id c : primaryContactSurveyMap.keySet()){
                    if(c != null){
                        Contact_Survey__c primConSurvJunc = new Contact_Survey__c();
                        primConSurvJunc.Type__c = 'Primary';
                        primConSurvJunc.Contact_Id__c = c;
                        primConSurvJunc.Survey_Id__c = primaryContactSurveyMap.get(c).Id;
                        contactSurveyJuncList.add(primConSurvJunc);
                    }
                }
                Database.insert(junctionsToCreate, false);
                Database.insert(contactSurveyJuncList, false);
            }
            
            
            
            
        }catch(exception ex){
            PostServiceSurveyHandler.LogExceptionSMethod(ex.getStackTraceString(),ex.getMessage(),CLASSNAME);
        }
        
    }
    
    public static void LogExceptionSMethod(string stacktr, string mesg, string c){
        
        insert new Error__c( Class__c = c,Stack_Trace__c = stacktr,Message__c = mesg
                            ,User__c = userinfo.getUserId()
                           );
        
    }
    
    
    
}