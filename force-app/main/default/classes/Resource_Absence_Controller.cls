public with sharing class Resource_Absence_Controller {

    @AuraEnabled(cacheable=true)
    public static ResourceAbsence getResourceAbsence(String resourceAbsenceId) {
        return [SELECT Id,AbsenceNumber,Type,FSL__GanttLabel__c,ResourceId,
                       FSL__Approved__c,Resource.Name,Start,End,
                       Street,City,State,PostalCode,Country,StateCode
                FROM ResourceAbsence 
                WHERE Id = :resourceAbsenceId];
    }

    @AuraEnabled(cacheable=true)
    public static String getCurrentLoggedInUserTimeZone() {
        TimeZone tz = UserInfo.getTimeZone();
        return tz.getID();
    }

}