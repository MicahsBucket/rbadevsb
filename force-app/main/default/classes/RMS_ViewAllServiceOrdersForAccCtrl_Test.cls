@isTest
public class RMS_ViewAllServiceOrdersForAccCtrl_Test {
    public static testMethod void test1(){
        RMS_ViewAllServiceOrdersForAccCtrl obj = new RMS_ViewAllServiceOrdersForAccCtrl();
        Id recType = UtilityMethods.retrieveRecordTypeId('Change_Order','Order');
        List<Id> listIds = new List<Id>();
        listIds.add(recType);
        ApexPages.currentPage().getParameters().put('recTypeIds',JSON.serialize(listIds));
        system.debug('### '+obj.listServiceOrders);
    }
}