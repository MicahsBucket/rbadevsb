/*
* @author Jason Flippen
* @date 12/30/2019 
* @description Class to provide functionality for the cancelPurchaseOrderAction LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - CancelPOActionControllerTest
*/
public with sharing class CancelPOActionController {

    /*
    * @author Jason Flippen
    * @date 12/31/2019
    * @description Method to return (wrapped) data from a Purchase Order.
    * @param purchaseOrderId
    * @returns List of (Wrapped) Purchase Order records
    */
    @AuraEnabled(cacheable=true)
    public static PurchaseOrderWrapper getPurchaseOrderData(Id purchaseOrderId) {

        PurchaseOrderWrapper wrapper = new PurchaseOrderWrapper();

        // Retrieve data from the Purchase Order record.
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  Confirmed_Timestamp__c,
                                                  Submitted_Timestamp__c,
                                                  Status__c
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];

        // Set the Wrapper properties.
        wrapper.id = purchaseOrder.Id;
        wrapper.confirmedTimestamp = purchaseOrder.Confirmed_Timestamp__c;
        wrapper.submittedTimestamp = purchaseOrder.Submitted_Timestamp__c;
        wrapper.status = purchaseOrder.Status__c;

        // Return the Wrapper Class.
        return wrapper;

    }

    /*
    * @author Jason Flippen
    * @date 12/30/2019
    * @description Method to update the Purchase Order Status to "Cancelled".
    * @param purchaseOrderId
    * @returns String containing the save result (Success/Error)
    */
    @AuraEnabled
    public static String cancelPurchaseOrder(Id purchaseOrderId) {
        
        String returnResult = null;

        // Get Purchase Order and associated Products.
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  Canceled_Timestamp__c,
                                                  Released_Timestamp__c,
                                                  Status__c,
                                                  (
                                                      SELECT Id,
                                                             Has_PO__c,
                                                             Purchase_Order__c
                                                      FROM   Order_Products__r
                                                  )
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];  

        // Grab List of related Products to be updated (if applicable).
        List<OrderItem> updateOIList = new List<OrderItem>();
        for (OrderItem oi : purchaseOrder.Order_Products__r) {
            oi.Purchase_Order__c = null;
            oi.Has_PO__c = false;
            updateOIList.add(oi);
        }

        // If we have related Products to update, update them.
        Boolean continueProcessing = true;
        if (!updateOIList.isEmpty()) {

            // Create a savepoint prior to making any changes so
            // we can roll them back if we encounter an error.
            Savepoint sp = Database.setSavepoint();

            try {
                update updateOIList;
            }
            catch (Exception ex) {
                System.debug('***** PO OrderItem Update Error: ' + ex);
                // Rollback the changes and return the error message.
                Database.rollback(sp);
                continueProcessing = false;
                returnResult = RMS_ErrorMessages.UPDATE_PO_LINES_EXCEPTION;
            }

        }
        
        // Can we continue processing?
        if (continueProcessing == true) {

            // Now let's update the actual Purchase Order.

            purchaseOrder.Status__c = 'Cancelled';
            purchaseOrder.Released_Timestamp__c = null;
            if (purchaseOrder.Canceled_Timestamp__c == null) {        
                purchaseOrder.Canceled_Timestamp__c = System.now();
            }

            // Create a savepoint prior to making any changes so
            // we can roll them back if we encounter an error.
            Savepoint sp = Database.setSavepoint();

            try {
                update purchaseOrder;
                returnResult = 'Cancel PO Success';
            }
            catch (Exception ex) {
                System.debug('***** PO Update Error: ' + ex);
                // Rollback the changes and return the error message.
                Database.rollback(sp);
                returnResult = RMS_ErrorMessages.UPDATE_PO_EXCEPTION;
            }
        
        }
        
        return returnResult;
        
    }


/** Wrapper Class **/


    /*
    * @author Jason Flippen
    * @date 12/30/2019
    * @description WRapper Class for Purchase Order.
    */
    @TestVisible
    public class PurchaseOrderWrapper {
        
        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public Datetime confirmedTimestamp {get;set;}

        @AuraEnabled
        public Datetime submittedTimestamp {get;set;}

        @AuraEnabled
        public String status {get;set;}

    }

}