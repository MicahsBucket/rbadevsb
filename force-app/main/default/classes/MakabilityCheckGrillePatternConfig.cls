/**
 * @File Name          : MakabilityCheckGrillePatternConfig.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 02-24-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/1/2019, 2:56:11 PM   mark.rothermal@andersencorp.com     Initial Version
**/
public without sharing class MakabilityCheckGrillePatternConfig implements MakabilityService {
    /**
    * @description run makability on order items and return results
    * @author mark.rothermal@andersencorp.com | 5/1/2019
    * @param map<string, MakabilityRestResource.OrderItem> requests
    * @param set<id> productIds
    * @return List<MakabilityRestResource.MakabilityResult>
    */
    public static List<MakabilityRestResource.MakabilityResult>  checkCompatibility( map <string,MakabilityRestResource.OrderItem> requests, set<id> productIds){
        List<MakabilityRestResource.MakabilityResult> results = new List<MakabilityRestResource.MakabilityResult>();
        Map<id,Grille_Pattern_Configuration__c> grilleConfigMap = getConfigOptions(productIds);
        Map<string,Grille_Pattern_Configuration__c> uniKeyToConfigMap = new Map<string,Grille_Pattern_Configuration__c>();
        Map<id,Decimal> prodIdToTempReqUi = new map<id,decimal>();
        
        // handle no config record found.
        if(grilleConfigMap.keyset().size() == 0){
            for(String r:requests.keyset()){
                MakabilityRestResource.OrderItem oi = requests.get(r);
                MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult(); 
                List<string> errMessages = new List<string>();
                string errMessage = 'Salesforce - No Grille Pattern configurations found for this productId.';  
                errMessages.add(errMessage);
                result.errorMessages = errMessages;
                result.orderId = oi.orderId;
                result.orderItemId = oi.orderItemId;
                results.add(result);                
            }            
            return results;
        }
        // build pattern to grille config map.
        for(id gId:grilleConfigMap.keyset()){
            Grille_Pattern_Configuration__c gc  = grilleConfigMap.get(gId);
            String uniKey = gc.Product_Configuration__r.Product__c +'-' +gc.Grille_Pattern__c;
            if(!uniKeyToConfigMap.containsKey(uniKey)){
                uniKeyToConfigMap.put(uniKey,gc);
            }

        }        
        ////////////////////////////// 
        // run makability check  
        // //////////////////////////        
        for(String r:requests.keyset()){
            MakabilityRestResource.OrderItem req = requests.get(r);                  
            List<string> errMessages = new List<string>();        
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
            result.orderItemId = req.orderItemId;
            result.orderId = req.orderId;
            string requestKey = req.productConfiguration.productId +'-'+req.productConfiguration.grillePattern;
            system.debug('unique key in request ' + requestKey);
            Boolean patternPasses = true;
            // is there a config to check.
            if(req.makabilityCalculator.checkGrille){
                    // check pattern map
                    if(uniKeyToConfigMap.containsKey(requestKey) ){
                       Grille_Pattern_Configuration__c  currentConfig = uniKeyToConfigMap.get(requestKey) ;                 
                        // check width
                        if(!MakabilityUtility.compareMinHeightOrWidth(req.productConfiguration.widthInches,req.productConfiguration.widthFractions, currentConfig.Min_Width_Inches__c, currentConfig.Min_Width_Fraction__c )){
                            patternPasses = false;
                            errMessages.add ('Grille Config - Width Less than Allowed for Selected Pattern. ' +
                                             'Min width = ' + currentConfig.Min_Width_Inches__c + ' '+currentConfig.Min_Width_Fraction__c +' inches.' );                         
                            result.isMakable = false;                        
                        }
                        // check height
                        if(!MakabilityUtility.compareMinHeightOrWidth(req.productConfiguration.heightInches,req.productConfiguration.heightFractions, currentConfig.Min_Height_Inches__c, currentConfig.Min_Height_Fraction__c )){
                            patternPasses = false;
                            errMessages.add ('Grille Config -  Height Less than Allowed for Selected Pattern. ' +
                                             'Mininum Height = ' + currentConfig.Min_Height_Inches__c +' '+currentConfig.Min_Height_Fraction__c +' inches.' );   
                            result.isMakable = false;                        
                        }
                    } else{
                        // pattern doesnt match and pattern is not No Pattern or No Grille
                        if(req.productConfiguration.grillePattern != 'No Pattern' && req.productConfiguration.grillePattern != 'No Grille'){
                            patternPasses = false;
                            List<String> availablePatterns = MakabilityUtility.splitUniqueKey(req.productConfiguration.productId, uniKeyToConfigMap.keyset());
                            errMessages.add ('Grille Config - Pattern Not Found available patterns include:' + availablePatterns );   
                            result.isMakable = false;                               
                        }
                        
                    }  
                if(patternPasses){
                    errMessages.add('Grille Config  - passed');  
                    result.isMakable = true;                          
                }   
                // no configuration record found
            } else {
              //  errMessages.add ('Grille Config - No configuration record found');   
                result.isMakable = true;                         
            }
            result.errorMessages = errMessages;                               
            results.add(result);    
        }
        system.debug('results check ' + results ); 
        
        // should never hit the code block below.
        if(results.size() == 0){
            List<string> errMessages = new List<string>();                    
            errMessages.add('Something bad happened with the Grille Pattern config');
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
            result.errorMessages = errMessages;
            result.isMakable = false;
            results.add(result);
        }        
        return results;
    }
    
    /**
    * @description get config records for makability testing
    * @author mark.rothermal@andersencorp.com | 5/1/2019
    * @param set<id> productIds
    * @return Map<id, Grille_Pattern_Configuration__c>
    */
    public static Map<id,Grille_Pattern_Configuration__c> getConfigOptions(set<id> productIds){
        Map<id,Grille_Pattern_Configuration__c> grillePatternConfigMap= new Map<id,Grille_Pattern_Configuration__c>( [
            SELECT id,
            Min_Height_Fraction__c,
            Min_Height_Inches__c,
            Min_Width_Fraction__c,
            Min_Width_Inches__c,
            Grille_Pattern__c,
            Product_Configuration__r.Product__c
            from Grille_Pattern_Configuration__c 
            where Product_Configuration__r.Product__c in :productids] );  
        return grillePatternConfigMap;
    }
}