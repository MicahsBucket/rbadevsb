/**
* @author Ramya Bangalore, Consultant.
* @group Customer Portal
* @date 11/18
* @description Finds the Individual MileStone Stage Dates associated to the current Order and MileStone Dates from the Order and Opportunity Objects.
* @Location used in Customer Portal "MileStoneComponent" Component which displays the MileStone Dates and Status. Under "myPROJECT" Tab.
**/
public without sharing class MileStoneController {
    //returns a list of Orders that the user has been or is currently associated to
    @AuraEnabled
    public static Order getOrderDates(Id orderId){
        Order ord = [SELECT Id, Status, Order_Processed_Date__c, Tech_Measure_Date__c, Time_Ready_to_Order__c, Time_Install_Scheduled__c, Install_Complete_Date__c, Job_Close_Date__c, Reinstall_Date__c FROM Order WHERE Id = :orderId LIMIT 1];
        return ord;
    }
    @AuraEnabled
    public static Id getOrderId() {
        List<Order> orders = [SELECT Id,CreatedDate,CustomerPortalUser__c FROM Order WHERE CustomerPortalUser__c = :UserInfo.getUserId() ORDER BY CreatedDate DESC];
        return orders[0].Id;
    }   
    @AuraEnabled
    public static MilestoneDates getAllMileStonesForOrder(Id orderId){
        Boolean isOnHold = false ;
        MilestoneDates mdates = new MilestoneDates();
        mdates.showShipDetails = false;
        String isOnHoldMessage = '';
                                    
      
       Order ord = [Select Id,Opportunity.CloseDate, EffectiveDate, Date_Sat_Survey_Run__c, Survey_Sent__c, Store_Location__r.Active_Store_Configuration__r.Display_Shipping_MileStone__c, Status, Reinstall_Date__c,Estimated_Ship_Date__c, Store_Location__r.Active_Store_Configuration__r.Home_Owners_Decision__c, Store_Location__r.Active_Store_Configuration__r.Project_Approval__c, (select Id,Primary_Reason__c from Tasks where Status = 'Open' AND  RecordType.Name='On Hold') from Order where Id= :orderId];
       
        If(ord != null)
       {
        System.debug('---------------inside if') ;
          /* for(Task objTask : ord.Tasks) {
               if(objTask.Primary_Reason__c == 'Project Approval' || objTask.Primary_Reason__c == 'Homeowner Decision') {
                    isOnHold = true ;
               
                   if(objTask.Primary_Reason__c == 'Project Approval' && ord.Store_Location__r.Active_Store_Configuration__r.Project_Approval__c != null ) {
                    	isOnHoldMessage = ord.Store_Location__r.Active_Store_Configuration__r.Project_Approval__c;
                   } else if(objTask.Primary_Reason__c == 'Homeowner Decision' && ord.Store_Location__r.Active_Store_Configuration__r.Home_Owners_Decision__c != null) {
                        isOnHoldMessage = ord.Store_Location__r.Active_Store_Configuration__r.Home_Owners_Decision__c;
                   }
                   mdates.isOnHoldMessage = isOnHoldMessage;
           		}
           }*/
       }
       
       Date consultationDate = ord.EffectiveDate;
       Date orderPlaceDate = ord.Opportunity.CloseDate;
       Date ReinstallDate = ord.Reinstall_Date__c;
        
       
        if (ord.Store_Location__r.Active_Store_Configuration__r.Display_Shipping_MileStone__c != null && 
            ord.Store_Location__r.Active_Store_Configuration__r.Display_Shipping_MileStone__c == true){
        	mdates.showShipDetails = true;
            mdates.EstimatedShipDate = ord.Estimated_Ship_Date__c;    
         } else
            mdates.showShipDetails = false;

        mdates.consultationDate = consultationDate;
        mdates.orderPlaceDate = orderPlaceDate;
        mdates.ReinstallDate = ReinstallDate;
        
        
        List<DateTime> techMeasureDates = new List<DateTime>(); // To display Multiple Tech Measure Start Date.
        List<DateTime> techMeasureEndDates = new List<DateTime>();// To display Multiple Tech Measure End Date.
        List<DateTime> installDateList = new List<DateTime>(); //To display multiple Installations.
        
        List<WorkOrder> lstInstall = new List<WorkOrder>() ;
        List<WorkOrder> lstTechMeasure = new List<WorkOrder>() ;
        List<WorkOrder>wkds = [Select Id,Scheduled_Start_Time__c,Scheduled_End_Time__c,RecordType.Name from WorkOrder where Sold_Order__c = :orderId and (RecordType.Name='Install' OR RecordType.Name ='Tech Measure') order by Scheduled_Start_Time__c ,Scheduled_End_Time__c asc];
        
        for (WorkOrder wk : wkds){
             if (wk.RecordType.Name=='Install'){
                if(wk.Scheduled_Start_Time__c!=null && wk.Scheduled_End_Time__c!=null ){
                    installDateList.add(wk.Scheduled_Start_Time__c);
                    installDateList.add(wk.Scheduled_End_Time__c);
                    lstInstall.add(wk);
                }
                 mdates.installDate = wk.Scheduled_Start_Time__c;  // To display one Installation             
            } 
            else if (wk.RecordType.Name=='Tech Measure'){
                if(wk.Scheduled_Start_Time__c!=null && wk.Scheduled_End_Time__c!=null) {
                    techMeasureDates.add(wk.Scheduled_Start_Time__c);
                    techMeasureEndDates.add(wk.Scheduled_End_Time__c);
                    lstTechMeasure.add(wk) ;
                }
            }
        }
        
        mdates.techMeasureDate = techMeasureDates;
        mdates.techMeasureEndDate = techMeasureEndDates;
        mdates.installDates = installDateList;
        
        if(!lstInstall.isEmpty()) 
            mdates.lstInstall = lstInstall ;
        
        if(!lstTechMeasure.isEmpty())
            mdates.lstTechMeasure = lstTechMeasure ;
        
        Datetime mostRecentTechMesureDate = null;
        Boolean isTechMeasureDateInFuture = true;
        
        for (Datetime dt : techMeasureDates) {
            if (mostRecentTechMesureDate== null || dt > mostRecentTechMesureDate)
                mostRecentTechMesureDate = dt;
        }   

        if (mostRecentTechMesureDate!=null && mostRecentTechMesureDate<DateTime.now())
            isTechMeasureDateInFuture = false;
        
        List<Survey__c> srv = [SELECT id, Date_Sent__c FROM Survey__c where order_name__c= :orderId order by CreatedDate desc ];
        
        Date surveySentDate;
         if (srv.size()> 0 && srv.get(0).Date_Sent__c!=null) {
            surveySentDate = srv.get(0).Date_Sent__c;
        }
        mdates.surveySentDate = surveySentDate; 
        mdates.isOnHold = isOnHold ;

        List<String> checklistItems = new List<String>();
        String checklistKeysString = [SELECT Store_Location__r.Active_Store_Configuration__r.Checklist_Items__c FROM Order WHERE Id= :orderId LIMIT 1].Store_Location__r.Active_Store_Configuration__r.Checklist_Items__c;
        if (checklistKeysString != '' && checklistKeysString != null){
            for (Pre_Installation_Checklist_Item__mdt item: 
                [SELECT Full_Checklist_Item__c FROM Pre_Installation_Checklist_Item__mdt WHERE Key__c IN :checklistKeysString.split(';')]){
                checklistItems.add(item.Full_Checklist_Item__c);
            }
        }

        if(!checklistItems.isEmpty()){
            mdates.checklistItems = checklistItems;
        }

        return mdates;
    }
}