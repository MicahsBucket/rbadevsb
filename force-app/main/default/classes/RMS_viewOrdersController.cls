/*******************************************************//**

@class  RMS_viewOrdersController

@brief  Controller for RMS_viewCOROOrders and RMS_viewServiceOrders visualforce page

@author  Kai Ruan (Slalom)

@version    2016-3-11  Slalom
Created.

@copyright  (c)2016 Slalom.  All Rights Reserved. 
Unauthorized use is prohibited.

***********************************************************/
public with sharing class RMS_viewOrdersController {
    
    
    public String strurl;
    public String baseUrl{get;set;}
    public String createServiceRequestURL{get;set;}
	public String createLegacyServiceRequestURL{get;set;}
    public String createAssetURL{get;set;}
    public integer num_assets;
    
    public Id serviceRecordTypeId{get;set;}
    public Id CORORecordTypeId{get;set;}
    public id changeOrderTypeId{get;set;}
    public List<Order> orders{get;set;}
    public Set<ID> recordTypeIDs{get;set;}
    public String allServiceOrdersURL{get;set;}
    
    /******* Set up Standard Controller for Purchase_Order__c  *****************/
    private Apexpages.StandardController standardController;
    private final Account acc;
    
    
    //Constructor
    public RMS_viewOrdersController(ApexPages.StandardController stdController) {
        if(!test.isRunningTest()){
            stdController.addFields(new List<String>{'Store_Location__c'});
        }
        this.acc = (Account)stdController.getRecord();
        
        strurl = ApexPages.currentPage().getUrl();
        strurl = strurl.split('apex/')[1];
        baseUrl = URL.getSalesforceBaseUrl().toExternalForm();

        try {
          String netId = Network.getNetworkId();
          ConnectApi.Community comm = ConnectApi.Communities.getCommunity(netId);
          baseUrl = comm.siteUrl;
        } catch(Exception e) {
          
        }
        
        changeOrderTypeId = UtilityMethods.retrieveRecordTypeId('Change_Order', 'Order');
        serviceRecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
        CORORecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Record_Type', 'Order');
        
        //Id recordTypeId = null;
        recordTypeIDs = new set<ID>();
        System.debug(strurl);
        System.debug(serviceRecordTypeId);
        if(strurl.Contains('viewService')){
            recordTypeIds.add(serviceRecordTypeId);
        }else {
            recordTypeIds.add(CORORecordTypeId);
            recordTypeIds.add(changeOrderTypeId);   
        }
        
        orders = [SELECT    Name,
                  OrderNumber,
                  Status,
                  EffectiveDate,
                  Contract.Name,
                  ContractId,
                  TotalAmount,
                  Amount_Due__c,
                  Retail_Total__c,
                  RecordType.Name,
                  Sold_Order__c,
                  Description,
                  Service_Type__c,
                  Sold_Order__r.OrderNumber
                  FROM  Order
                  WHERE     RecordTypeId in: recordTypeIds
                  AND   AccountId =: acc.Id];    
        
        system.debug(orders);
        
        List<AggregateResult> results2=[select COUNT (Id) 
                                        FROM Asset 
                                        WHERE AccountId = :acc.Id];
        num_assets = (Integer) results2[0].get('expr0');
        
        If (num_assets > 0){
            createServiceRequestURL = '/apex/RMS_createServiceRequestRedirect?aid='+acc.Id+'&slid='+acc.Store_Location__c; 
				//@pavan rForce-1166
			createLegacyServiceRequestURL = '/apex/RMS_createServiceRequestRedirect?aid='+acc.Id+'&slid='+acc.Store_Location__c+'&ServiceType=Legacy';
        } else {
            createServiceRequestURL = '/apex/RMS_addAssetstoServiceLegacy?id='+acc.Id+'&retURL='+acc.Id;   
			//@pavan rForce-1166
        createLegacyServiceRequestURL =	'/apex/RMS_addAssetstoServiceLegacy?id='+acc.Id+'&retURL='+acc.Id;	
        }        
        
        
        createAssetURL = '/apex/RMS_addAssetstoServiceLegacy?id='+acc.Id+'&retURL='+acc.Id; 
        
    }
    public String tabURL{get;set;}
    public void gotoFullList(){
        String recTypesJSON;
        Account objacc;
        
        recTypesJSON = JSON.serialize(recordTypeIDs);
        objacc = [SELECT Id,Name FROM Account WHERE Id = :acc.Id];
        allServiceOrdersURL = baseUrl+'/apex/RMS_ViewAllServiceOrdersForAcc?id='+objacc.Id+'&recTypeIds='+recTypesJSON+'&accName='+objacc.Name+'&console=false';
        tabURL = allServiceOrdersURL;
        system.debug('### allServiceOrdersURL: '+allServiceOrdersURL);
    }
}