global class CanvassUnitDeletionQueueable implements Queueable, Database.AllowsCallouts
{
    //System.enqueueJob(new CanvassUnitDeletionQueueable());
    //System.enqueueJob(new CanvassUnitDeletionQueueable(fieldIds))
    
    private string zipCode;
    private string marketId;
    private string queuedJobId;
    private list<string> canvassUnitIdWithLeadSheetList;
    
    private list<object> markerIdsTempList;
    
    global CanvassUnitDeletionQueueable(string oldZipCode, string oldMarketId, string queuedJobIdString)
    {
        zipCode = oldZipCode;
        marketId = oldMarketId;        
        queuedJobId = queuedJobIdString;
        canvassUnitIdWithLeadSheetList = new list<string>();
    }
    
    global CanvassUnitDeletionQueueable(string oldZipCode, string oldMarketId, string queuedJobIdString, list<string> canvassUnitIdList)
    {
        zipCode = oldZipCode;
        marketId = oldMarketId;        
        queuedJobId = queuedJobIdString;
        canvassUnitIdWithLeadSheetList = canvassUnitIdList;
    }

    global void execute(QueueableContext context)  
    {
        list<Custom_Data_Layer_Batch_Queue__c> queueList = [SELECT Id, Status__c 
                                                            FROM Custom_Data_Layer_Batch_Queue__c 
                                                            WHERE Id = :queuedJobId];
        try
        {
            /*list<string> canvassUnitsWithLeadSheetsIdList = new list<string>();
            for(CNVSS_Canvass_Lead_Sheet__c canvassUnitLeadSheet :[SELECT Id, Name, CNVSS_Canvass_Unit__c 
                                                                   FROM CNVSS_Canvass_Lead_Sheet__c 
                                                                   WHERE CNVSS_Canvass_Unit__c IN (SELECT Id 
                                                                                                   FROM CNVSS_Canvass_Unit__c 
                                                                                                   WHERE CNVSS_Canvass_Market__c = :marketId 
                                                                                                   AND CNVSS_Zip_Code__c = :zipCode)])
            {
                canvassUnitsWithLeadSheetsIdList.add(canvassUnitLeadSheet.CNVSS_Canvass_Unit__c);
            }

            list<CNVSS_Canvass_Unit__c> canvassUnitRecordsToDelete = [SELECT Id
                                                                      FROM CNVSS_Canvass_Unit__c 
                                                                      WHERE CNVSS_Canvass_Market__c = :marketId 
                                                                      AND CNVSS_Zip_Code__c = :zipCode
                                                                      AND Id NOT IN :canvassUnitsWithLeadSheetsIdList
                                                                      LIMIT 9999]; */
            // Josh made changes to fix 50k lead sheet problem
            
            list<CNVSS_Canvass_Unit__c> canvassUnitRecordsToDelete = new list<CNVSS_Canvass_Unit__c>();
            list<CNVSS_Canvass_Unit__c> canvassUnitList = [SELECT Id, (SELECT Id 
                                                                       FROM Canvass_Lead_Sheets__r
                                                                       LIMIT 1) 
                                                           FROM CNVSS_Canvass_Unit__c
                                                           WHERE CNVSS_Canvass_Market__c = :marketId
                                                           AND CNVSS_Zip_Code__c = :zipCode
                                                           AND Id NOT IN :canvassUnitIdWithLeadSheetList
                                                           LIMIT 9999]; 
            
            for(CNVSS_Canvass_Unit__c canvassUnit :canvassUnitList)
            {
                if(canvassUnit.Canvass_Lead_Sheets__r.size() == 0)
                {
                    canvassUnitRecordsToDelete.add(canvassUnit);
                }
                else
                {
                    canvassUnitIdWithLeadSheetList.add(canvassUnit.Id);
                }
            }           
            
            boolean makeRecursiveCall = false;
            if(canvassUnitList.size() == 9999)
            {
                makeRecursiveCall = true;
            }
            
            list<CNVSS_Canvass_Unit__c> tempCanvassUnitRecordsToDelete = new list<CNVSS_Canvass_Unit__c>();
            for(integer i = 0; i < canvassUnitRecordsToDelete.size(); i++)
            {
                tempCanvassUnitRecordsToDelete.add(canvassUnitRecordsToDelete[i]);
            }
            
            delete tempCanvassUnitRecordsToDelete;
            
            if(makeRecursiveCall == true)
            {
                System.enqueueJob(new CanvassUnitDeletionQueueable(zipCode,marketId,queuedJobId,canvassUnitIdWithLeadSheetList));
            }
            else
            {
                for(Custom_Data_Layer_Batch_Queue__c queue :queueList)
                {
                    queue.Status__c = 'Holding';
                }   
                update queueList;
            }
        }
        catch(Exception e)
        {
            system.debug('ERROR');
            system.debug(e);
            system.debug('ERROR Line Number');
            system.debug(e.getLineNumber());
            
            for(Custom_Data_Layer_Batch_Queue__c queue :queueList)
            {
                queue.Status__c = 'Failed';
            }

            update queueList;
        }
    }
}