@isTest
private class ScheduleIncompleteDataUpdater_UnitTest {
	@testSetup
	static void setup(){
		TestDataFactory.setUpConfigs();
	}
	
	@isTest static void installIncompleteTest() {
		Date todaysDate = Date.today();
		Account storeAccount = TestDataFactory.createStoreAccount('Calvins Store');
		Database.insert(storeAccount);
		Id accountId = storeAccount.Id;
		List<Survey__c> postInstallSurveyIncom = TestDataFactory.createSurveys('Post Install', 4, accountId, 'Pending');
		for(Survey__c s : postInstallSurveyIncom){
			s.Installation_Date__c = todaysDate.addDays(-56);
			s.Shipping_Date__c = todaysDate.addDays(-56);
			s.Source_System__c = 'EnabledPlus';
			s.Primary_Contact_First_Name__c = null;
			s.Primary_Contact_Last_Name__c = null;
			s.City__c = null;
			s.Street__c = null;
			s.State__c = null;
			s.Zip__c = null;
			s.Country__c = null;
		}

		Database.update(postInstallSurveyIncom);
		Date sendDate = [SELECT Send_Date__c FROM Survey__c LIMIT 1].Send_Date__c;
		System.debug('Send Date: ' + sendDate);
		System.debug('Today: ' + todaysDate);

		List<Survey__c> postInstallSurveyComp = TestDataFactory.createSurveys('Post Install', 6, accountId, 'Pending');
		for(Survey__c s : postInstallSurveyComp){
			s.Installation_Date__c = todaysDate.addDays(-56);
			s.Source_System__c = 'EnabledPlus';
			s.Primary_Contact_First_Name__c = 'testFN';
			s.Primary_Contact_Last_Name__c = 'testLN';
			s.City__c = 'city';
			s.Street__c = 'street';
			s.State__c = 'state';
			s.Zip__c = 'zip';
			s.Country__c = 'country';
		}
		Database.update(postInstallSurveyComp);
		SchedulableContext sc = null;
		ScheduleIncompleteDataUpdater sidu = new ScheduleIncompleteDataUpdater();
		sidu.execute(sc);

		Test.startTest();
			List<Survey__c> incompleteSurveys = [SELECT Survey_Status__c, Send_To_Medallia__c, On_Hold__c FROM Survey__c WHERE Send_To_Medallia__c = FALSE];
			List<Survey__c> sentSurveys = [SELECT Survey_Status__c, Send_To_Medallia__c, On_Hold__c FROM Survey__c WHERE Send_To_Medallia__c = TRUE];
			System.assertEquals(4, incompleteSurveys.size());
			System.assertEquals(6, sentSurveys.size());
			System.assertEquals('Incomplete Data', incompleteSurveys[0].Survey_Status__c);
			System.assertEquals(TRUE, sentSurveys[0].Send_To_Medallia__c);
			System.assertEquals(TRUE, sentSurveys[0].On_Hold__c);
		Test.stopTest();
	}

	//@isTest static void postAppointmentIncompleteTest() {
	//	Date todaysDate = Date.today();
	//	Account storeAccount = TestDataFactory.createStoreAccount('Calvins Store');
	//	Database.insert(storeAccount);
	//	Id accountId = storeAccount.Id;
	//	List<Survey__c> postApptSurveysIncomp = TestDataFactory.createSurveys('Post Appointment', 4, accountId, 'Recission Period');
	//	for(Survey__c s : postApptSurveysIncomp){
	//		s.Appointment_Date__c = todaysDate.addDays(-56);
	//		s.Source_System__c = 'EnabledPlus';
	//		s.Primary_Contact_Email__c = null;
	//		s.Primary_Contact_First_Name__c = null;
	//		s.Primary_Contact_Last_Name__c = null;
	//	}
	//	Database.update(postApptSurveysIncomp);

	//	List<Survey__c> postApptSurveysComp = TestDataFactory.createSurveys('Post Appointment', 6, accountId, 'Recission Period');
	//	for(Survey__c s : postApptSurveysComp){
	//		s.Appointment_Date__c = todaysDate.addDays(-56);
	//		s.Source_System__c = 'EnabledPlus';
	//		s.Primary_Contact_First_Name__c = 'testFN';
	//		s.Primary_Contact_Last_Name__c = 'testLN';
	//	}
	//	Database.update(postApptSurveysComp);
	//	SchedulableContext sc = null;
	//	ScheduleIncompleteDataUpdater sidu = new ScheduleIncompleteDataUpdater();
	//	sidu.execute(sc);

	//	Test.startTest();
	//		List<Survey__c> incompleteSurveys = [SELECT Survey_Status__c, On_Hold__c, Send_To_Medallia__c FROM Survey__c WHERE Send_To_Medallia__c = FALSE];
	//		List<Survey__c> sentSurveys = [SELECT Survey_Status__c, On_Hold__c, Send_To_Medallia__c FROM Survey__c WHERE Send_To_Medallia__c = TRUE];
	//		System.assertEquals(4, incompleteSurveys.size());
	//		System.assertEquals(6, sentSurveys.size());
	//		System.assertEquals('Processing', incompleteSurveys[0].Survey_Status__c);
	//	Test.stopTest();

	//}
	
}