public class PortalUserCreation 
{
    public static ResultSet validateAndCreatePortalUser(List<Order> allOrder,Set<String> emailList,Map<ID,List<ID>> contactType)
    {
        Set<Id> allDwellingAccts=new Set<Id>();//Collection ID's of standard account Id
        Map<Id,Date> accountStoreInvitationDelayMap = new Map<Id,Date>();
        Map<Id,Date> contactInvitationDelayMap = new Map<Id,Date>();
        Map<ID,Order> mapOppWithOrder=new Map<ID,Order>();
        Map<ID,List<Order>> mapOpportunityWithMulOrders=new Map<ID,List<Order>>();
        Set<ID> billToContacts=new Set<ID>();
        Map<Id,Order> mapContactWithOrder=new Map<ID,Order>();
        Set<ID> oppIds=new Set<ID>();
		// Create the Users..Note the Number of DML Limit restricted to 150        
        List<User> newUsers = new List<User>();         
        for (Order ord : allOrder)
        {
            Date d1= ord.EffectiveDate;
            d1 = d1.addDays(ord.Store_Location__r.Active_Store_Configuration__r.Invitation_Delay_Days__c.intValue());//Invitation Delay days
            allDwellingAccts.add(ord.AccountId);//Dwelling ID's collection(Standard Account lookup)
            accountStoreInvitationDelayMap.put(ord.AccountId,d1);//Map Account ID with Date d1
            mapOppWithOrder.put(ord.OpportunityId,ord);
            if(!mapOpportunityWithMulOrders.containsKey(ord.OpportunityId))
            {
                mapOpportunityWithMulOrders.put(ord.OpportunityId,new List<Order>());
            }
            mapOpportunityWithMulOrders.get(ord.OpportunityId).add(ord);
            oppIds.add(ord.OpportunityId);
        }
        //Below SOQL query will bring the contact details for creating community users
        Set<ID> dwellingContacts=new Set<ID>();
        List<OpportunityContactRole> conOppList=[Select Id,ContactId,Contact.AccountId,Contact.Name,Contact.Email,Contact.Salesforce_User_Name__c,
                                                 Contact.FirstName,Contact.LastName,Contact.Phone,Contact.MobilePhone,Contact.MailingStreet,
                                                 Contact.MailingCity,Contact.MailingState,Contact.MailingCountry,Contact.MailingPostalCode,
                                                 Contact.Portal_User_Failure_Msg__c,Contact.Portal_User_Creation_Date__c,OpportunityId
                                                 from OpportunityContactRole where OpportunityId in:oppIds and Contact.Email!=null and 
                                                 (Contact.RecordType.Name='Customer Contacts' or Contact.RecordType.Name='Customer') and 
                                                 Contact.Salesforce_User_Name__c=null and Contact.Email not in:emailList order by createdDate asc limit 49000];
        
        //Below soql will bring the already created user for contact for the DwellingAccounts
        List<OpportunityContactRole> alreadyCreatedOppUsers=[Select Id,ContactId,Contact.AccountId,Contact.Name,Contact.Email,Contact.Salesforce_User_Name__c,
                                                             Contact.FirstName,Contact.LastName,Contact.Phone,Contact.MobilePhone,Contact.MailingStreet,
                                                             Contact.MailingCity,Contact.MailingState,Contact.MailingCountry,Contact.MailingPostalCode,
                                                             Contact.Portal_User_Failure_Msg__c,Contact.Portal_User_Creation_Date__c,OpportunityId,
                                                             Contact.Salesforce_User_Name__r.isActive,Contact.Salesforce_User_Name__r.Activated_User__c from OpportunityContactRole where OpportunityId in:oppIds and 
                                                             (Contact.RecordType.Name='Customer Contacts' or Contact.RecordType.Name='Customer') and 
                                                             Contact.Salesforce_User_Name__c!=null order by createdDate asc limit 49000];
        
        //Below Set is used to capture the user who are in-active
        Map<ID,ID> inActiveUserSet=new Map<ID,ID>();
        //Below map-for loop will collect the accounts with already created users.
        Map<ID,List<OpportunityContactRole>> mapOpportunityContactsUserCreated=new Map<ID,List<OpportunityContactRole>>();
        
        for(OpportunityContactRole con:alreadyCreatedOppUsers)
        {
            if(!mapOpportunityContactsUserCreated.containsKey(con.OpportunityId))
            {
                mapOpportunityContactsUserCreated.put(con.OpportunityId,new List<OpportunityContactRole>());
            }
            mapOpportunityContactsUserCreated.get(con.OpportunityId).add(con);
            
            //Below if-condition will whether the user is in-active or not, if it is in-active then it will add it to inActiveUserSet set collection variable
            if(!con.Contact.Salesforce_User_Name__r.isActive && !con.Contact.Salesforce_User_Name__r.Activated_User__c)
            {
                inActiveUserSet.put(con.ContactID,con.Contact.Salesforce_User_Name__c);
                //below code will activate the in-active users.
                User activateUser=new User(ID=con.Contact.Salesforce_User_Name__c,ContactId=con.ContactId,Activated_User__c=true,Selected_Order__c=mapOppWithOrder.get(con.OpportunityId).ID);
                newUsers.add(activateUser);                    
            }
        }
        System.debug('____inActiveUserSet____'+inActiveUserSet);
        String profName ='Customer Community Login Primary Profile';
        String profName2 ='Customer Community Login Secondary Profile';
        //String profName2 ='Customer Community Login Profile';
        Profile p = [Select Id,Name from Profile where Name =: profName Limit 1];
        Profile p2 = [Select Id,Name from Profile where Name =: profName2 Limit 1];
        // Find all the Contacts which needs to be Created as Portal User or Create the Contacts
        Id portalProfileId = p.Id;
        Id portalProfileId2 = p2.Id;
        
        Boolean sendUserEmail = false;
        Boolean allOrNone = true;
        
        Map<ID,OpportunityContactRole> mapOppContactRole=new Map<ID,OpportunityContactRole>();
        
        Map<ID,String> mapContactWithUserFailureMsg=new Map<ID,String>();  
        
        if(!conOppList.isEmpty())
        {
            for (OpportunityContactRole conHis : conOppList) 
            { 
                //if the OpportunityId is not available in mapOpportunityContactsUserCreated then add it, as we will take this map further to give permission to order
                if(!mapOpportunityContactsUserCreated.containsKey(conHis.OpportunityId))
                {
                    mapOpportunityContactsUserCreated.put(conHis.OpportunityId,new List<OpportunityContactRole>()); 
                }
                //check whether each Opportunity has less than or equal to two contacts and if it is true the allow to create the community user
                if(mapOpportunityContactsUserCreated.get(conHis.OpportunityId).size()<2)
                {
                    if(!emailList.contains(conHis.Contact.Email))//validate the email is already taken in the scope or not.
                    {
                        //add the conHis to mapOpportunityContactsUserCreated map.
                        mapOpportunityContactsUserCreated.get(conHis.OpportunityId).add(conHis);
                        //if(mapAccountContacts.containsKey(conHis.Contact__r.AccountId)&&mapAccountContacts.get(conHis.Contact__r.AccountId).size()<=2)
                        //{
                        emailList.add(conHis.Contact.Email);
                        system.debug('condition true');
                        String alias = conHis.Contact.LastName;
                        if (alias.length() > 8) {
                            alias = alias.substring(0, 8);
                        } 
                        //alias+='__'+System.today().Month()+'/'+System.today().day()+'/'+System.today().year();
                        contactInvitationDelayMap.put(conHis.ContactId,accountStoreInvitationDelayMap.get(conHis.Contact.AccountId));
                        String nickName = conHis.Contact.Email;
                        if (nickName.length() > 25) {
                            nickName = nickName.substring(0, 25);
                        }
                        Order  userOrder=mapOppWithOrder.get(conHis.OpportunityId);
                        String storeName=userOrder.Store_Location__r.Active_Store_Configuration__r.Remit_to_Company_Name__c;
                        //create user
                        User newUser = new User(
                            UserName = conHis.Contact.Email+'.myproject',
                            LastName = conHis.Contact.LastName,
                            FirstName = conHis.Contact.FirstName,
                            Email = conHis.Contact.Email,
                            phone = conHis.Contact.Phone,
                            MobilePhone = conHis.Contact.MobilePhone,
                            Street = conHis.Contact.MailingStreet,
                            City = conHis.Contact.MailingCity,
                            State = conHis.Contact.MailingState,
                            Country = conHis.Contact.MailingCountry,
                            PostalCode = conHis.Contact.MailingPostalCode,
                            CommunityNickName = nickName+'.myproject',
                            Alias = alias,
                            Store_Location_Text__c=storeName,
                            Selected_Order__c=mapOppWithOrder.get(conHis.OpportunityId).ID,//mapBillToContactWithOrder.get(conHis.Contact__c).Id,
                            /* link to the contact and to the account through the contact... */
                            ContactId = conHis.ContactId,
                            
                            //ProfileId = (mapSecondaryContactWithOrder.containsKey(conHis.Contact__c)?portalProfileId2:portalProfileId),
                            ProfileId =(contactType.containsKey(conHis.OpportunityId)&&contactType.get(conHis.OpportunityId).size()==1)?portalProfileId2:portalProfileId,
                            
                            /* various user settings */
                            emailencodingkey = 'UTF-8',
                            languagelocalekey = 'en_US',
                            localesidkey = 'en_US',
                            timezonesidkey = 'America/Los_Angeles'
                        );
                        if(mapOppWithOrder.get(conHis.OpportunityId).Store_Location__r.Active_Store_Configuration__r.Time_Zone__c!=null)
                        {
                        	newUser.TimeZoneSidKey=mapOppWithOrder.get(conHis.OpportunityId).Store_Location__r.Active_Store_Configuration__r.Time_Zone__c;
                        }
                        newUser.Store_Default_email__c=mapOppWithOrder.get(conHis.OpportunityId).Store_Location__r.Active_Store_Configuration__r.Store_Default_email__c;
                        newUsers.add(newUser);
                        if(!contactType.containsKey(conHis.OpportunityId))
                        {
                            contactType.put(conHis.OpportunityId,new List<ID>());
                        }
                        contactType.get(conHis.OpportunityId).add(conHis.ContactId);
                        //below code will collect the contactIds which proceed for user creation
                        if(!mapOppContactRole.containsKey(conHis.contactId))
                        {
                            mapOppContactRole.put(conHis.ContactId,conHis);
                        }    
                    }
                    else //if the account has more than 2 contacts then this else will execute.
                    {
                        //below if-condition will check whether the contact already proceed for user creation, if yes then the contact will attach to the opportunity.
                        if(mapOppContactRole.containsKey(conHis.ContactId))
                        {
                            mapOpportunityContactsUserCreated.get(conHis.OpportunityId).add(conHis);    
                        }
                        else
                        {
                            String errorMsg='Error: Duplicate Username.<br>The username already exists in this or another Salesforce organization. Usernames must be unique across all Salesforce organizations. To resolve, use a different username (it doesn\'t need to match the user\'s email address).';
                            mapContactWithUserFailureMsg.put(conHis.ContactId,errorMsg);    
                        }
                    }
                }
                else if(emailList.contains(conHis.Contact.Email))// if the email is duplicate in the batch then this else will execute
                { 
                    String errorMsg='Error: Current Residents more than 2.';
                    mapContactWithUserFailureMsg.put(conHis.ContactId,errorMsg);
                }
            }
            System.debug('_________newUsers__before________'+newUsers);
        }
        if (newUsers.size()>0)//check the newUsers list has elements.
        {
            UserTriggerHandler.skipTrigger=true;
            System.debug('_________newUsers__before________'+newUsers);
            Database.UpsertResult[] srList;
            try
            {
                srList = Database.upsert(newUsers, false); 
            }
            catch(Exception e)
            {
                System.debug('________Error while creating user_____'+e);
            }
            
            System.debug('_________newUsers___after_______'+newUsers);
            //size of srList==size of newUsers
            for(Integer i=0;i<srList.size();i++)
            {
                Database.UpsertResult userRec=srList[i];
                User userRecord=newUsers[i];
                if(userRec.isSuccess())//checking whether the user is created successfully
                {
                    System.debug('_____userRec__________'+userRec);
                    ID userId=userRec.getId();//Id of the User
                    userRecord.Id=userId;//Assigning Id to the newUsers list because we are using newUsers list further in the code
                    System.debug('_____userRecord__________'+userRecord);
                }
                else
                {
                    //System.debug('__________Exception______'+userRec.getErrors();
                    System.debug('_____userRecord__________'+userRecord);
                    String errorMsge='Error: ';
                    for(Database.Error err:userRec.getErrors())
                    {
                        errorMsge+=err.getMessage()+'\n ';
                        System.debug('_____Exception Message_______'+err.getMessage());    
                    }
                    mapContactWithUserFailureMsg.put(userRecord.ContactId,errorMsge);
                }
            }

            //Set permission sets on user here because UserTriggerHandler uses a future method.
            Map<Id,User> newUserMap = new Map<Id,User>();
            for(User u : newUsers){
                if(u.Id != null){
                    newUserMap.put(u.Id,u);
                }
            }
            List<PermissionSetAssignment> permissionsToAdd = UserTriggerHandler.getAssignmentPermissions(null,newUserMap);
            if(permissionsToAdd.size() > 0){
                insert permissionsToAdd;
            }
            //insert newUsers; 
        }
        //below code will fetch the contactId from the successfully created user.
        Map<Id,Id> contUserIdMap = new Map<Id,Id>();
        if (newUsers.size()>0)
        {
            for (User u: newUsers)
            {
                if(u.Id!=null)
                {
                    contUserIdMap.put(u.ContactId,u.Id);
                }
            }
        }
        System.debug('______contUserIdMap______'+contUserIdMap);
        
        // Update Contacts
        List<Contact> contactToUpdate = new List<Contact>();
        //Below collection variable make sure the same contact didnot update multiple times (to save System.ListException: Duplicate id in list)
        Set<ID> contactIdToUpdate=new Set<ID>();
        //below for-loop will update the contact if the user is created successfully or update the error message.
        //mapOpportunityContactsUserCreated has the account Id with 1 or 2 contacts, and this contacts will get the access to order.
        for(ID accId:mapOpportunityContactsUserCreated.keySet())//this map holds OppId as key and opportunityContactRole as value, and each oppId will have max two opportunityContactRole.
        {//each opportunityId
            Integer count=0;
            for(OpportunityContactRole conHis:mapOpportunityContactsUserCreated.get(accId))
            {//each OpportunityContactRole
                //Below if-block will check the whether the user is created or already existed user in saleforce.
                if(mapOpportunityWithMulOrders.containsKey(conHis.OpportunityId)&&(contUserIdMap.keySet().contains(conHis.ContactId)||conHis.Contact.Salesforce_User_Name__c!=null))
                {
                    for(Order ordAcc:mapOpportunityWithMulOrders.get(conHis.OpportunityId))
                    {
                        if(count==0)
                        {
                            ordAcc.Primary_Contact__c=conHis.ContactId;
                        }
                        else
                        {
                            ordAcc.Secondary_Contact__c=conHis.ContactId;
                        }
                        ordAcc.ArePortalUsersCreated__c=true;
                        mapContactWithOrder.put(ordAcc.Id,ordAcc); 
                    }
                }
                if(contUserIdMap.keySet().contains(conHis.ContactId))
                {
                    if((!contactIdToUpdate.contains(conHis.ContactId)))
                    {   
                        Contact con=new Contact();
                        con.Id=conHis.ContactId;
                        con.Salesforce_User_Name__c=contUserIdMap.get(con.Id);
                        con.Portal_User_Creation_Date__c=System.today();
                        con.Portal_User_Failure_Msg__c='Portal User created successfully!';
                        contactToUpdate.add(con);
                        contactIdToUpdate.add(con.Id);
                    }
                }
                else if(mapContactWithUserFailureMsg.containsKey(conHis.ContactId))
                {
                    Contact con=new Contact();
                    con.Id=conHis.ContactId;
                    con.Portal_User_Failure_Msg__c=mapContactWithUserFailureMsg.get(con.id);
                    contactToUpdate.add(con);
                }
                count++;
            }
        }
        //update contactToUpdate; 
        System.debug('__________contactUpdates__________'+contactToUpdate);  
        List<Database.SaveResult> contactUpdates=Database.update(contactToUpdate, false);
        
        for(Integer i=0;i<contactUpdates.size();i++)
        {
            Database.SaveResult contactRec=contactUpdates[i];
            //User userRecord=newUsers[i];
            if(!contactRec.isSuccess())//checking whether the user is created successfully
            {
                for(Database.Error err:contactRec.getErrors())
                {
                    System.debug('_____Exception Message_______'+err.getMessage());    
                }
            }
        }
        //update orders;  
        List<Database.SaveResult> orderUpdates=Database.update(mapContactWithOrder.values(), false);
        System.debug('__________contactUpdates__________'+contactUpdates); 
        for(Integer i=0;i<orderUpdates.size();i++)
        {
            Database.SaveResult orderRec=orderUpdates[i];
            //User userRecord=newUsers[i];
            if(!orderRec.isSuccess())//checking whether the user is created successfully
            {
                for(Database.Error err:orderRec.getErrors())
                {
                    System.debug('_____Exception Message_______'+err.getMessage());    
                }
            }
        }   
        
        ResultSet rSet=new ResultSet();
        rSet.emailList=new Set<String>();
        rSet.emailList.addAll(emailList);
        return rSet;
    }
    public class ResultSet
    {
        public Set<String> emailList=new Set<String>();
        public Map<ID,List<ID>> contactType=new Map<ID,List<ID>>();
    }
}