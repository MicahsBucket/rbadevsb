@isTest
private class NewManualPASAPexController_UnitTest {
	@testSetup
	static void setup(){
		TestDataFactory.setUpConfigs();
		Account testStoreAccount = TestDataFactory.createStoreAccount('Test Store');
		Database.insert(testStoreAccount, true);
		Contact testContact = TestDataFactory.createCustomerContact(testStoreAccount.Id);
		Database.insert(testContact, true);
		User partnerUser = TestDataFactory.createPartnerUser('Test', 'Partner RMS-RSR', testContact.Id);
		Database.insert(partnerUser, true);
	}
	@isTest static void canGetWorkers() {
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Id surveyId = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0].Id;
		List<Worker__c> salesWorker = TestDataFactory.createWorkers('Sales Rep', 5, accountId, surveyId);
		salesWorker[0].Role__c = 'Sales Rep;Inactive';
		Database.update(salesWorker[0], true);
		test.startTest();
				List<Worker__c> salesReps =NewManualPASAPexController.getWorkers('Sales Rep');
				List<Worker__c> inactive = NewManualPASAPexController.getWorkers('Inactive');
				system.assertEquals(4, salesReps.size());
				system.assertEquals(0, inactive.size());
		test.stopTest();
	}
	
	@isTest static void canSavePAS() {
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Id surveyId = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0].Id;
		List<Worker__c> salesWorker = TestDataFactory.createWorkers('Sales Rep', 1, accountId, surveyId);
		String firstName = 'Calvin';
		String lastName = 'OKeefe';
		String email = 'c@c.com';
		String phone = '1234567890';
		String appointmentDate = '2016-03-03';
		String appointmentResult = 'No Demo';
		Id salesRepId = salesWorker[0].Id;
		String surveyStatus = 'Pending';
		Survey__c survey = NewManualPASApexController.savePAS(firstName, lastName, email, phone, appointmentDate, appointmentResult, salesRepId, surveyStatus);
		
		test.startTest();
				system.assertEquals('Calvin', survey.Primary_Contact_First_Name__c);
				system.assertEquals('OKeefe', survey.Primary_Contact_Last_Name__c);
				system.assertNotEquals('test0@user.com', survey.Primary_Contact_Email__c);
		test.stopTest();
	}
	
}