@istest
public class PurchaseOrderButtonControllerTest {
    
    static testmethod void setPurchaseOrderToReleased()
    {
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();



        

        Account acc = TestDataFactory.createStoreAccount('test1');
        insert acc;
        
        Account acc2 = TestDataFactory.createStoreAccount('test2');
        insert acc2;
        
        String pricebookId = Test.getStandardPricebookId();

        Product2 prod = new Product2();
        prod.Name = 'Test Product 1';
        prod.IsActive = true;
        insert prod;

        Product2 pro2 = new Product2();
        pro2.Name = 'Test Product 2';
        pro2.IsActive = true;
        insert pro2;

        List<Skill> mySkills = [SELECT Id FROM Skill];


        PricebookEntry pe = new PricebookEntry();
        pe.IsActive = true;
        pe.Pricebook2Id = pricebookId;
        pe.Product2Id = prod.Id;
        pe.UnitPrice = 45;
        insert pe;

        PricebookEntry pe2 = new PricebookEntry();
        pe2.IsActive = true;
        pe2.Pricebook2Id = pricebookId;
        pe2.Product2Id = pro2.Id;
        pe2.UnitPrice = 45;
        insert pe2;
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;

        Order testOrder =  new Order();
        testOrder.Name ='Sold Order 1';
        testOrder.AccountId = acc.Id;
        testOrder.EffectiveDate = Date.Today();
        testOrder.Status = 'Install Complete';
        testOrder.Pricebook2Id = pricebookId;
        testOrder.Customer_Pickup_All__c = FALSE;
        testOrder.Installation_Date__c = system.today()-1;
        insert testOrder;
        Id devRecordTypeId = Schema.SObjectType.Retail_Purchase_Order__c.getRecordTypeInfosByName().get('RbA Purchase Order').getRecordTypeId();
        Retail_Purchase_Order__c po = new Retail_Purchase_Order__c();
        po.Invoice_Number__c='test';
        po.Order__c=testorder.id;
        po.RecordTypeId= devRecordTypeId;
        po.Store_Location__c=store.id;
        po.Store_Number__c='1234' ;
        insert po;
        PurchaseOrderButtonController.setPurchaseOrderToReleased(po.id);
        PurchaseOrderButtonController.getStatus(po.id);
    }
       
    static testmethod void setPurchaseOrderToReleasedNegative()
    {
        
          TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;
        
        Retail_Purchase_Order__c po = new Retail_Purchase_Order__c();
        po.Invoice_Number__c='test';
        po.Store_Location__c=store.id;
        po.Store_Number__c='1234' ;
        
        insert po;
        PurchaseOrderButtonController.setPurchaseOrderToReleased(po.id);
        PurchaseOrderButtonController.getStatus(po.id);
    }
    
    static testmethod void setPurchaseOrderToReleasedCost()
    {
    
      TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;
        
        
        Retail_Purchase_Order__c po = new Retail_Purchase_Order__c();
        po.Invoice_Number__c='test';
        po.Store_Location__c=store.id;
        po.Store_Number__c='1234' ;
        
        Id devRecordTypeId = Schema.SObjectType.Retail_Purchase_Order__c.getRecordTypeInfosByName().get('Cost Purchase Order').getRecordTypeId();
        po.RecordTypeId=devRecordTypeId;
        insert po;
        PurchaseOrderButtonController.setPurchaseOrderToReleased(po.id);
        PurchaseOrderButtonController.getStatus(po.id);
    }

}