/*
* @author Jason Flippen
* @date 11/18/2020
* @description Test Class for the following Classes:
*              - PDFViewerController
*/ 
@isTest
public class PDFViewerControllerTest {

    /*
    * @author Jason Flippen
    * @date 11/18/2020
    * @description Method to create data for the Test Methods.
    */ 
    @TestSetup
    static void setupTestData() {
        
        ContentVersion testContentVersion = new ContentVersion(Title = 'Test rForce Release Notes',
                                                               PathOnClient = 'TestRForceReleaseNotes.pdf',
                                                               VersionData = Blob.valueOf('Test rForce Release Notes Data'),
                                                               IsMajorVersion = true);
        insert testContentVersion;

    }

    /*
    * @author Jason Flippen
    * @date 11/18/2020
    * @description Method to test the getRelatedFilesByRecordId method.
    */ 
    static testMethod void testGetRelatedFilesByRecordId() {

        Test.startTest();

            Map<Id,String> relatedFileMap = PDFViewerController.getRelatedFilesByRecordId();
            System.assertEquals(1,relatedFileMap.size(),'Unexpected File Count');

        Test.stopTest();

    }

    /*
    * @author Jason Flippen
    * @date 11/18/2020
    * @description Method to test the getReleaseDocNames method.
    */ 
    static testMethod void testGetReleaseDocNames() {
        
        Test.startTest();

            List<PDFViewerController.ReleaseDocuments> releaseDocumentList = PDFViewerController.getReleaseDocNames();
            System.assertEquals(1,releaseDocumentList.size(),'Unexpected Release Document Count');

        Test.stopTest();

    }

}