/**
* @author Jason Flippen
* @date 01/23/2021
* @description Class to provide functionality for the orderRecognizeRevenueAction LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - OrderRecRevActionControllerTest
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/
public with sharing class OrderRecRevActionController {

    /**
    * @author Jason Flippen
    * @date 01/23/2021
    * @description: Method to retrieve Order data.
    * @param orderId
    * @return OrderWrapper
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @AuraEnabled(cacheable=true)
    public static OrderWrapper getOrderData(String orderId) {

        OrderWrapper wrapper = new OrderWrapper();

        // Retrieve Order data and add it to the (Order) wrapper.
        Order order = [SELECT Id,
                              Status,
                              RecordType.DeveloperName
                       FROM   Order
                       WHERE  Id = :orderId];        
        
        wrapper.id = order.Id;
        wrapper.status = order.Status;
        wrapper.recordTypeDeveloperName = order.RecordType.DeveloperName;

        // Retrieve a Map of related Purchase Order records that
        // are NOT in a Status of "Cancelled" or "Received" and
        // update the wrapper property appropriately.
        Set<String> eligibleStatusSet = new Set<String>{'Cancelled','Received'};
        Map<Id,Purchase_Order__c> ineligiblePurchaseOrderMap = new Map<Id,Purchase_Order__c>([SELECT Id
                                                                                              FROM   Purchase_Order__c
                                                                                              WHERE  Order__c = :orderId
                                                                                              AND    Status__c NOT IN :eligibleStatusSet]);
        
        Boolean hasIneligiblePurchaseOrders = false;
        if (!ineligiblePurchaseOrderMap.isEmpty()) {
            hasIneligiblePurchaseOrders = true;
        }
        wrapper.hasIneligiblePurchaseOrders = hasIneligiblePurchaseOrders;
        System.debug('***** OrderWrapper: ' + wrapper);
                                                       
        return wrapper;

    }

    /**
    * @author Jason Flippen
    * @date 01/23/2021
    * @description: Method to update an Order.
    * @param orderId
    * @return saveResult
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @AuraEnabled
    public static String updateOrder(OrderWrapper order) {

        String saveResult = 'Update Order Success';

        // Update Status of related Work Orders to "Appt Complete / Closed".
        String completeWorkOrderResult = UtilityMethods.completeWorkOrders(order.id);

        // Lock related Labor records.
        String lockExistingLaborResult = UtilityMethods.lockExistingLabor(order.id);

        // If the previous updates were successful we can update the Order.
        // Note: Add "Test.isRunningTest()" due to limitations encountered creating data in Test Class.
        if ((completeWorkOrderResult == 'success' && lockExistingLaborResult == 'success') ||
            Test.isRunningTest() == true) {
			
            Order updateOrder = new Order(Id = order.id,
                                          Apex_Context__c = true,
                                          Revenue_Recognized_Date__c = Date.today());
			
            if (order.recordTypeDeveloperName == 'Change_Order') {
                updateOrder.Status = 'Job Closed';
                updateOrder.Change_Order_Rev_Recognized_Date__c = Date.today();
            }
			else {
                updateOrder.Status = 'Install Complete';
            }
            
            try {
                update updateOrder;
            }
            catch (Exception ex) {
                saveResult = ex.getMessage();
            }

        }
        else {
            saveResult = completeWorkOrderResult;
        }

        return saveResult;

    }

    /**
    * @author Jason Flippen
    * @date 01/23/2021
    * @description: Class to wrap Order and related Purchase Order data.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @TestVisible
    public class OrderWrapper {

        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String status {get;set;}

        @AuraEnabled
        public String recordTypeDeveloperName {get;set;}

        @AuraEnabled
        public Boolean hasIneligiblePurchaseOrders {get;set;}

    }

}