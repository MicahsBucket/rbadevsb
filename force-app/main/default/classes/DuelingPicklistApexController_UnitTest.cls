@isTest
private class DuelingPicklistApexController_UnitTest {
	@testSetup
	static void setup(){
		TestDataFactory.setUpConfigs();
		Account testStoreAccount = TestDataFactory.createStoreAccount('Test Store');
		Database.insert(testStoreAccount, true);
		Contact testContact = TestDataFactory.createCustomerContact(testStoreAccount.Id);
		Database.insert(testContact, true);
		User partnerUser = TestDataFactory.createPartnerUser('Test', 'Partner RMS-RSR', testContact.Id);
		Database.insert(partnerUser, true);
	}
	@isTest static void canGetAllAvailableInstallers() {
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Id surveyId = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0].Id;
		List<Worker__c> oldInstallers = TestDataFactory.createWorkers('Installer', 5, accountId, surveyId);
		List<Worker__c> newInstallers = TestDataFactory.createWorkers('Installer', 2, accountId);
		test.startTest();
			
			List<DuelingPicklistApexController.SelectOption> availableInstallers = DuelingPicklistApexController.getAllAvailableInstallers(surveyId);
			Set<Id> availableInstallerIds = new Set<Id>();
			for(DuelingPicklistApexController.SelectOption so : availableInstallers){
				availableInstallerIds.add(so.value);
			}	
			system.assertEquals(availableInstallerIds.contains(newInstallers[0].Id), true);
			system.assertNotEquals(availableInstallerIds.contains(oldInstallers[3].Id), true);
			
		test.stopTest();

	}
	
	@isTest static void canGetAllSelectedInstallers() {
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Id surveyId = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0].Id;
		List<Worker__c> oldInstallers = TestDataFactory.createWorkers('Installer', 5, accountId, surveyId);
		List<Worker__c> newInstallers = TestDataFactory.createWorkers('Installer', 2, accountId);
		test.startTest();
			List<DuelingPicklistApexController.SelectOption> selectedInstallers = DuelingPicklistApexController.getAllSelectedInstallers(surveyId);
			Set<Id> selectedInstallerIds = new Set<Id>();
			for(DuelingPicklistApexController.SelectOption so : selectedInstallers){
				selectedInstallerIds.add(so.value);
			}	
			system.assertEquals(selectedInstallerIds.contains(oldInstallers[3].Id), true);
			system.assertNotEquals(selectedInstallerIds.contains(newInstallers[0].Id), true);
			
		test.stopTest();
	}
	
}