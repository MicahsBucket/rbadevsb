/* 
Trigger Handler Anthony Strafaccia 2015

This is the class for the OpportunityTrigger that handles all the logic. It is designed so that only one trigger needs to be created for the object and allows for the customization of the order of execution.

Test Code will be handled inside OpportunityTriggerHandlerTest
*/


public without sharing class OpportunityTriggerHandler{

  public void OnBeforeInsert(List<Opportunity> listNew) {
    System.debug('entering updatePricebook');
    updatePricebook(listNew);
  }
  
  

    public void OnAfterInsert(List<Opportunity> listNew, Map<Id, Opportunity> mapNew){
        if (UtilityMethods.hasOpportunityTriggerRan()) return;
    try{

      // Create a map of the dwelling account and contact ids related to each opportunity
      Map<Id, Id> chMap = new Map<Id,Id>();
      for(Contact_History__c ch : [SELECT Dwelling__c, Contact__c
                    FROM Contact_History__c
                    WHERE Type__c = 'Current Resident' AND Dwelling__c IN (
                      SELECT AccountId
                      FROM Opportunity
                      WHERE Id IN :listNew
                    )])
        chMap.put(ch.Dwelling__c, ch.Contact__c);

      // Create a new opportunity contact role for each opportunity
      List<OpportunityContactRole> ocr = new List<OpportunityContactRole>();
      for(Opportunity o: listNew){
        if(chMap.get(o.AccountId) != null)
        ocr.add(new OpportunityContactRole(ContactId = chMap.get(o.AccountId),
                                  OpportunityId = o.Id,
                                  Role = 'Decision Maker',
                                  IsPrimary = true));
      }

      // Insert the new list of opportunity contact roles
      insert ocr;

    } catch(Exception e){
         system.debug('OpportunityTriggerHandler.OnAfterInsert Exception: ' + e.getMessage());
    }
    }

  public void updatePricebook(List<Opportunity> listNew){
    //configure pricebooks for each new opportunity
    system.debug('preparing to add the pricebook');
    List<Id> storeLocations = new List<Id>();
    Map<Opportunity, Id> storeLocationsMap = new Map<Opportunity, Id>();
    for(Opportunity o : listNew) {
      storeLocations.add(o.Store_Location__c);
      storeLocationsMap.put(o, o.Store_Location__c);
    }
    system.debug('list: ' + storeLocations);
    system.debug('map: ' + storeLocationsMap);
    List<Account> updatedStoreLocations = [select Id, Active_Store_Configuration__c from Account where Id =: storeLocations];

    system.debug('updatedStoreLocations: ' + updatedStoreLocations);

        //find related store configuration
    List<Id> storeConfiguration = new List<Id>();
    Map<Id, Id> storeConfigurationMap = new Map<Id, Id>();
    for(Account a : updatedStoreLocations) {
      storeConfiguration.add(a.Active_Store_Configuration__c);
      storeConfigurationMap.put(a.Id, a.Active_Store_Configuration__c); //map store id to store config id
    }

        //use that store configuration to find all related pricebooks that are not legacy pricebooks
    List<Pricebook2> pricebooks = [select Id, Store_Configuration__c from Pricebook2 where Store_Configuration__c =: storeConfiguration AND (NOT Name LIKE '%Legacy%')];
    
    system.debug('pricebooks: ' + pricebooks);
        //use the pricebook with the most recent effective date
    for(Opportunity o : listNew) {
      for(Pricebook2 p : pricebooks) {
        system.debug(storeLocationsMap.get(o));
        system.debug(p.Store_Configuration__c);
        if(storeConfigurationMap.get(storeLocationsMap.get(o)) == p.Store_Configuration__c) {
          o.Pricebook2Id = p.Id;
          system.debug('opportunity updated');
          system.debug(o.Pricebook2Id);
          continue;
        }
      }
    }
  }
  
  
  /*******************************************************************
     Populate the Sales Rep Contact on Order
  *******************************************************************/
  
  public void updateSalesRepContact(Map<Id,Opportunity> newOpp,Map<Id,Opportunity> oldOpp) {
      Map < Id,  Opportunity> mapOpp = new Map < Id, Opportunity >();
      List<Order> listOrder = new List<Order>();
    
      for(Opportunity opp : newOpp.values())
         mapOpp.put(opp.Id, opp);
    
      listOrder = [ SELECT Id, OpportunityId, Opportunity.Owner.ContactId, Sales_Rep_Contact__c FROM Order WHERE OpportunityId IN : mapOpp.keySet() ];
    
      if ( listOrder.size() > 0 ) {
        for ( Order ord : listOrder ) {
           if(mapOpp.get(ord.OpportunityId).OwnerId!=oldOpp.get(ord.OpportunityId).OwnerId)
            ord.Sales_Rep_Contact__c = mapOpp.get(ord.OpportunityId).Owner.ContactId;
            
         }
          update listOrder;
     }
   }

	public void DetermineIfJsonUploaded(Map<Id,Opportunity> newOpp,Map<Id,Opportunity> oldOpp) {
   		Map < Id,  Opportunity> mapOpp = new Map < Id, Opportunity >();
        try{
        for(Opportunity opp : newOpp.values()){
            mapOpp.put(opp.Id, opp);
            String ContentDocumentID;
            Try{
            	ContentDocumentID = [Select ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityID IN : mapOpp.keyset()].ContentDocumentId;
                Boolean ROPOrder = opp.ROP_Opportunity__c;
        		if(ContentDocumentID != Null && ROPOrder == True){
                    system.debug(ContentDocumentID);
            		opp.JsonFileUploaded__c = True;
                    system.debug('Json true!');
                } else {
                    opp.JsonFileUploaded__c = False;
                    system.debug('Json false!');
                }
            } catch(QueryException e){
                system.debug('The following exception has occured: ' + e.getMessage());
            }

        }            
        } catch (System.NullPointerException e){
            
        }        
    }

}