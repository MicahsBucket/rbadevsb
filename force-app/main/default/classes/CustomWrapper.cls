public class CustomWrapper 
{
	@AuraEnabled public Order orderRecord{get;set;}
    @AuraEnabled public Task taskRecord{get;set;}
    @AuraEnabled public User userRecord{get;set;}
    @AuraEnabled public String error {get; set;}
    @AuraEnabled public String state {get; set;}
    
    public CustomWrapper() {
        this.state = 'SUCCESS';
    }

    public CustomWrapper(Exception e){
    	this();
    	if(e != null){
    		this.state = 'ERROR';
            this.error = e.getMessage();
        }
    }
}