@isTest
public class InvalidateContactEmailTest 
{
	static testmethod void myTestMethod()
    {
        RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name = 'Turn Financial Transactions Off', Value__c = 'Yes');
        insert turnOffFinancialTrigger;
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        List<Account> dwellingsToInsert = new List<Account>();               
        Account dwelling1 = utility.createDwellingAccount('Dwelling Account');
        dwellingsToInsert.add(dwelling1);
        insert dwellingsToInsert;  
        
        ID contactRType=[select id from RecordType where SobjectType='Contact' and name='Customer Contacts'].id;
        List<Contact> contactsToInsert = new List<Contact>();       
        Contact contact1 = new contact (Email='RatnaTest@Ratnatest.com',
                                        Secondary_Email__c='RatnaTest@Ratnatest.com', 
                                        FirstName='Contact',
                                        LastName='1',
                                        Phone='(333)333-3333',
                                        MobilePhone='(444)444-4444',
                                        AccountId=dwelling1.id, 
                                        Primary_Contact__c =true,
                                        RecordTypeId=contactRType);
        contactsToInsert.add(contact1);
        Contact contact2 = new contact (Email='RatnaTest2@Ratnatest.com',
                                        Secondary_Email__c='RatnaTest@Ratnatest.com', 
                                        FirstName='Contact',
                                        LastName='12',
                                        Phone='(333)333-3333',
                                        MobilePhone='(444)444-4444',                                        
                                        AccountId=dwelling1.id, 
                                        Primary_Contact__c =false,
                                        RecordTypeId=contactRType);
        contactsToInsert.add(contact2);
        insert contactsToInsert;       
        
        Id batchjobId = Database.executeBatch(new InvalidateContactEmail(),200);
    }
}