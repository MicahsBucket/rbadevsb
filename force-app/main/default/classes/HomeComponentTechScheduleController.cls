/*
* @author Jason Flippen
* @date 11/13/2020
* @description Class to provide support for the homeComponentLightningCardTechScheduling LWC.
*
*			   Code Coverage provided by the following Test Class:
*              - HomeComponentTechScheduleControllerTest
*/ 
public without sharing class HomeComponentTechScheduleController {

    @AuraEnabled(cacheable=true)
    public static List<OpportunityWrapper> getOpportunities(String searchKey) {

		List<OpportunityWrapper> wrapperList = new List<OpportunityWrapper>();
		
		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
		String queryString = 'SELECT Id,' +
							 '       Name,' +
							 '       AccountId,' +
							 '       Account.Name,' +
							 '       CloseDate,' +
							 '       StageName ' +
							 'FROM   Opportunity ' +
							 'WHERE  CreatedDate = TODAY ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'AND (Name LIKE :tempSearchKey OR Account.Name LIKE :tempSearchKey OR StageName LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY Name ' +
					   'LIMIT 20';

		for (Opportunity o : Database.query(queryString)) {
			OpportunityWrapper wrapper = new OpportunityWrapper(o.Name,
																o.Id,
																o.Account.Name,
																o.AccountId,
																o.CloseDate,
																o.StageName);
			wrapperList.add(wrapper);
		}

		return wrapperList;
		   
    }

    @AuraEnabled(cacheable=true)
    public static List<WorkOrderAssignedWrapper> getAssignedWorkOrders(String searchKey) {

		List<WorkOrderAssignedWrapper> wrapperList = new List<WorkOrderAssignedWrapper>();
		
		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
		Id woTechMeasureRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Tech_Measure').getRecordTypeId();

		Set<String> eligibleStatusSet = new Set<String>{'Scheduled & Assigned'};
		String queryString = 'SELECT Id,' +
							 '       WorkOrderNumber,' +
							 '       AccountId,' +
							 '       Account.Name,' +
							 '       Contact.Name,' +
							 '       ContactId,' +
							 '       Sold_Order__c,' +
							 '       Sold_Order__r.Id,' +
							 '       Sold_Order__r.OrderNumber ' +
							 'FROM   WorkOrder ' +
							 'WHERE  Status IN :eligibleStatusSet ' +
							 'AND    RecordTypeId = :woTechMeasureRTId ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'AND (WorkOrderNumber LIKE :tempSearchKey OR Contact.Name LIKE :tempSearchKey OR Primary_Resource_Name__c LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY WorkOrderNumber ' +
					   'LIMIT 20';

		for (WorkOrder wo : Database.query(queryString)) {
			WorkOrderAssignedWrapper wrapper = new WorkOrderAssignedWrapper(wo.Id,
																			wo.WorkOrderNumber,
																			wo.Account.Name,
																			wo.AccountId,
																			wo.Contact.Name,
																			wo.ContactId,
																			wo.Sold_Order__r.OrderNumber,
																			wo.Sold_Order__r.Id);
			wrapperList.add(wrapper);
		}
		
		return wrapperList;
		
    }

    @AuraEnabled(cacheable=true)
    public static List<WorkOrderWrapper> getToBeScheduledWorkOrders(String searchKey) {

		List<WorkOrderWrapper> wrapperList = new List<WorkOrderWrapper>();
		
		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
		Id woTechMeasureRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Tech_Measure').getRecordTypeId();

		Set<String> eligibleStatusSet = new Set<String>{'To be Scheduled'};
		String queryString = 'SELECT Id,' +
							 '       WorkOrderNumber,' +
							 '       ContactId,' +
							 '       Contact.Name,' +
							 '       Scheduled_Start_Time__c,' +
							 '       Primary_Resource_Name__c ' +
							 'FROM   WorkOrder ' +
							 'WHERE  Status IN :eligibleStatusSet ' +
							 'AND    RecordTypeId = :woTechMeasureRTId ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'AND (WorkOrderNumber LIKE :tempSearchKey OR Account.Name LIKE :tempSearchKey OR Contact.Name LIKE :tempSearchKey OR Sold_Order__r.OrderNumber LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY WorkOrderNumber ' +
					   'LIMIT 20';

		for (WorkOrder wo : Database.query(queryString)) {
			WorkOrderWrapper wrapper = new WorkOrderWrapper(wo.Id,
															wo.WorkOrderNumber,
															wo.Contact.Name,
															wo.ContactId,
															wo.Scheduled_Start_Time__c,
															wo.Primary_Resource_Name__c);
			wrapperList.add(wrapper);
		}

		return wrapperList;
		
	}


/** Wrapper Classes **/


    public class OpportunityWrapper {

		@AuraEnabled
		public String opportunityId {get;set;}

    	@AuraEnabled
		public String opportunityName {get;set;}
		
		@AuraEnabled
		public String opportunityURL {get;set;}
		
		@AuraEnabled
		public String accountId {get;set;}
		
    	@AuraEnabled
		public String accountName {get;set;}
		
		@AuraEnabled
		public String accountURL {get;set;}
		
    	@AuraEnabled
		public Date closeDate {get;set;}
		
    	@AuraEnabled
		public String stageName {get;set;}

		public OpportunityWrapper(String oppName,
								  String oppId,
								  String acctName,
								  String acctId,
								  Date closeDate,
								  String stageName) {

    		this.opportunityName = oppName;
			if (oppId != null) {
				this.opportunityUrl = '/'+ oppId;
			}
			else {
				this.opportunityUrl = null;
			}					
    		this.accountName = acctName;
    		if (acctId != null) {
				this.accountURL = '/'+ acctId;
			}
			else {
				this.accountURL	= null;
			}
    		this.closeDate = closeDate;
    		this.stageName = stageName;
    	}
		
    }

    public class WorkOrderWrapper {

    	@AuraEnabled
		public String workOrderName;
		
		@AuraEnabled
		public String workOrderURL;
		
    	@AuraEnabled
		public String contactName;
		
    	@AuraEnabled
		public String contactURL;
		
		@AuraEnabled 
		public Datetime scheduledStartTime;

		@AuraEnabled 
		public String primaryResource;

		public WorkOrderWrapper(String woId,
								String woName,
								String conName,
								String conId,
								Datetime schedStartTime,
								String primResource) {
			
    		this.workOrderName = woName;
			if (woId != null) {
				this.workOrderUrl = '/'+ woId;
			}
			else {
				this.workOrderUrl = null;
			}
    		this.contactName = conName;
			if (conId != null) {
				this.contactUrl = '/'+ conId;
			}
			else {
				this.contactUrl = null;
			}
			this.scheduledStartTime = schedStartTime;
			this.primaryResource = primResource;
			
    	}

    }

	public class WorkOrderAssignedWrapper {

    	@AuraEnabled
		public String workOrderName;
		
		@AuraEnabled
		public String workOrderURL;
		
    	@AuraEnabled
		public String accountName;
		
		@AuraEnabled
		public String accountURL;
		
    	@AuraEnabled
		public String contactName;
		
    	@AuraEnabled
		public String contactURL;
		
		@AuraEnabled 
		public String soldOrderName;

		@AuraEnabled 
		public String soldOrderURL;

		public WorkOrderAssignedWrapper(String woId,
										String woName,
										String acctName,
										String acctId,
										String conName,
										String conId,
										String soName,
										String soId) {
			
    		this.workOrderName = woName;
			if (woId != null) {
				this.workOrderUrl = '/'+ woId;
			}
			else {
				this.workOrderUrl = null;
			}
    		this.accountName = acctName;
			if (acctId != null) {
    			this.accountURL = '/'+ acctId;
			}
			else {
				this.accountURL = null;
			}
    		this.contactName = conName;
			if (conId != null) {
				this.contactUrl = '/'+ conId;
			}
			else {
				this.contactUrl = null;
			}
			this.soldOrderName = soName;
			if (soId != null) {
				this.soldOrderUrl = '/'+ soId;
			}
			else {
				this.soldOrderUrl = null;
			}
    	}

    }

}