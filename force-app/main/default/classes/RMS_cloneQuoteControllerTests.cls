@isTest
private class RMS_cloneQuoteControllerTests {
	
	@isTest static void testCloeQuoteHappyPath() {
		// Implement test code
		
		RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name = 'Turn Financial Transactions Off', Value__c = 'Yes');
        insert turnOffFinancialTrigger;
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        //this should be created in the test data
        Account theAccount = [Select id from Account where name = 'Unassigned Account' limit 1];
		
		//create an opportunity
		Opportunity o = new Opportunity();
		o.AccountId = theAccount.Id;
		o.Name = 'Test Opportunity';
		o.StageName = 'Stage 1';
		o.CloseDate = System.today();
		insert o;


		utility.createRbAProducts();
		Product2 product1 = [select id from Product2 limit 1];

		utility.createSalesAppt(UserInfo.getUserId(), system.now(), true, true, 'testCloeQuoteHappyPath');
		Pricebook2 pricebook1 = [select id from Pricebook2 limit 1];

		//create a quote
		Quote q = new Quote();
		q.OpportunityId = o.id;
		q.Name = 'Test Quote';
		q.Status = 'Acccepted';
		q.Pricebook2Id = pricebook1.id;
		insert q;

		List<PricebookEntry> pbeList = [select Id, Product2Id, Pricebook2Id, UnitPrice, isActive 
										From PricebookEntry 
										Where Pricebook2Id =: pricebook1.id];

		QuoteLineItem qli = new QuoteLineItem();
		qli.QuoteId = q.id;
		qli.Description = 'test qli';
		qli.Quantity = 2;
		qli.UnitPrice = 1;
		qli.PricebookEntryId = pbeList[0].id;
		qli.Product2Id = product1.id;
		insert qli;

		PageReference pageRef = Page.RMS_cloneQuote;
		Test.setCurrentPage(pageRef);
   		pageRef.getParameters().put('id',q.id);
   		ApexPages.StandardController sc = new ApexPages.standardController(q);
   		RMS_cloneQuoteController  controller = new RMS_cloneQuoteController(sc);
   		controller.save();

	}
	
	@isTest static void testCloneQuoteInNewStatus() {
		// Implement test code
		
		RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name = 'Turn Financial Transactions Off', Value__c = 'Yes');
        insert turnOffFinancialTrigger;
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        //this should be created in the test data
        Account theAccount = [Select id from Account where name = 'Unassigned Account' limit 1];
		
		//create an opportunity
		Opportunity o = new Opportunity();
		o.AccountId = theAccount.Id;
		o.Name = 'Test Opportunity';
		o.StageName = 'Stage 1';
		o.CloseDate = System.today();
		insert o;


		utility.createRbAProducts();
		Product2 product1 = [select id from Product2 limit 1];

		utility.createSalesAppt(UserInfo.getUserId(), system.now(), true, true, 'testCloeQuoteHappyPath');
		Pricebook2 pricebook1 = [select id from Pricebook2 limit 1];

		//create a quote
		Quote q = new Quote();
		q.OpportunityId = o.id;
		q.Name = 'Test Quote';
		q.Status = 'New';
		q.Pricebook2Id = pricebook1.id;
		insert q;

		List<PricebookEntry> pbeList = [select Id, Product2Id, Pricebook2Id, UnitPrice, isActive 
										From PricebookEntry 
										Where Pricebook2Id =: pricebook1.id];

		QuoteLineItem qli = new QuoteLineItem();
		qli.QuoteId = q.id;
		qli.Description = 'test qli';
		qli.Quantity = 2;
		qli.UnitPrice = 1;
		qli.PricebookEntryId = pbeList[0].id;
		qli.Product2Id = product1.id;
		insert qli;

		PageReference pageRef = Page.RMS_cloneQuote;
		Test.setCurrentPage(pageRef);
   		pageRef.getParameters().put('id',q.id);
   		ApexPages.StandardController sc = new ApexPages.standardController(q);
   		RMS_cloneQuoteController  controller = new RMS_cloneQuoteController(sc);
   		controller.save();

	}

	@isTest static void testCloneQuoteInDraftStatus() {
		// Implement test code
		
		RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name = 'Turn Financial Transactions Off', Value__c = 'Yes');
        insert turnOffFinancialTrigger;
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        //this should be created in the test data
        Account theAccount = [Select id from Account where name = 'Unassigned Account' limit 1];
		
		//create an opportunity
		Opportunity o = new Opportunity();
		o.AccountId = theAccount.Id;
		o.Name = 'Test Opportunity';
		o.StageName = 'Stage 1';
		o.CloseDate = System.today();
		insert o;


		utility.createRbAProducts();
		Product2 product1 = [select id from Product2 limit 1];

		utility.createSalesAppt(UserInfo.getUserId(), system.now(), true, true, 'testCloeQuoteHappyPath');
		Pricebook2 pricebook1 = [select id from Pricebook2 limit 1];

		//create a quote
		Quote q = new Quote();
		q.OpportunityId = o.id;
		q.Name = 'Test Quote';
		q.Status = 'Draft';
		q.Pricebook2Id = pricebook1.id;
		insert q;

		List<PricebookEntry> pbeList = [select Id, Product2Id, Pricebook2Id, UnitPrice, isActive 
										From PricebookEntry 
										Where Pricebook2Id =: pricebook1.id];

		QuoteLineItem qli = new QuoteLineItem();
		qli.QuoteId = q.id;
		qli.Description = 'test qli';
		qli.Quantity = 2;
		qli.UnitPrice = 1;
		qli.PricebookEntryId = pbeList[0].id;
		qli.Product2Id = product1.id;
		insert qli;

		PageReference pageRef = Page.RMS_cloneQuote;
		Test.setCurrentPage(pageRef);
   		pageRef.getParameters().put('id',q.id);
   		ApexPages.StandardController sc = new ApexPages.standardController(q);
   		RMS_cloneQuoteController  controller = new RMS_cloneQuoteController(sc);
   		controller.save();

   		

	}
	
}