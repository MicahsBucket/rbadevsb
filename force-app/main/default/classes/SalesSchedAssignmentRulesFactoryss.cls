public class SalesSchedAssignmentRulesFactoryss{
    
    public interface IARClassFactoryss{
        SalesSchedProcessDataWrapperss processRuless(SalesSchedProcessDataWrapperss pdw);
    }
    
    // Assigment Rule Class Factory base class
    public virtual class ARClassFactoryBasess{
    }
    
    // Custom Exception Class
    public class ARException extends Exception {}
    
    
    
    public static void processAllWorkss(ID StoreId, Date ProcessingDate, List<String> rules,string methodName){
        Sales_Schedule_Setting__mdt sss = [Select Current_Sales_Date__c From Sales_Schedule_Setting__mdt Where QualifiedApiName = 'Default'];
        SalesSchedProcessDataWrapperss pdw = new SalesSchedProcessDataWrapperss(StoreId,ProcessingDate);
        pdw.process();
        ClassFactoryManagerss cfm = new ClassFactoryManagerss();
        for(String sRuleName : rules){
            String sClassName = 'SalesSchedAssignmentRulesFactoryss.' + sRuleName;
            System.debug('JWL: going to call process rule');
            pdw = cfm.newClassInstance(sClassName).processRuless(pdw);
        }
        //Determine if the current running user is a manager.
        List<User> managers = [Select Id From User Where Id = :UserInfo.getUserId() And UserRole.Name Like '%Manager'];
        if(managers.isEmpty()){
            insert pdw.assignments;
            update pdw.appointmentsToUpdate.values();
        }else{
            SalesSchedUtilsWithoutSharing.insertResources(pdw.assignments);
            SalesSchedUtilsWithoutSharing.updateAppointments(pdw.appointmentsToUpdate.values());
        }
        if(methodName =='batch'){
            sendAutoAssignmentCompleteEmail(pdw);
        }
        
        //System.debug('JWL: updating appointments to update: ' + pdw.appointmentsToUpdate.values());
        //update pdw.appointmentsToUpdate.values();
    }
    
    
    //==================Class Factory Manager====================
    public with sharing class ClassFactoryManagerss {
        public ClassFactoryManagerss(){}
        
        // Return the appropriate class instance based on className
        public SalesSchedAssignmentRulesFactoryss.IARClassFactoryss newClassInstance(String className){
            Type t = Type.forName(className);
            return (SalesSchedAssignmentRulesFactoryss.IARClassFactoryss) t.newInstance();
        }
    }
    public class RankRepsByCloseRatess extends ARClassFactoryBasess implements IARClassFactoryss{
        //Rule Intent: With remaining RepUsers, Highest ranked rep, for each slot, gets appointment with highest total window + door count.
        public SalesSchedProcessDataWrapperss processRuless(SalesSchedProcessDataWrapperss pdw){
            Sales_Schedule_Setting__mdt sss = [Select Distance_Warning__c From Sales_Schedule_Setting__mdt Where QualifiedApiName = 'Default'];
            Decimal distanceWarning = sss.Distance_Warning__c;
            if(pdw.store.Active_Store_Configuration__r.Distance_Warning__c != null){
                distanceWarning = pdw.store.Active_Store_Configuration__r.Distance_Warning__c;
            }
            
            pdw.appointmentsToUpdate = new Map<Id,Sales_Appointment__c>();
            
            Set<Id> assignedCapacityIds = new Set<Id>();
          /* for(Slot__c slot : pdw.slots){
                List<Sales_Appointment__c> appointments = pdw.slotToAppointmentsMap.get(slot.Id);
                List<SalesSchedProcessDataWrapperss.UserRanks> repRanks = pdw.slotToUserRankMap.get(slot.Id);
                
                System.debug('JWL: slot: ' + slot.Name);
                System.debug('JWL: repRanks: ' + repRanks);
                System.debug('JWL: appointments: ' + appointments);
                
                boolean assignAppointments = appointments != null && !appointments.isEmpty() && repRanks != null && !repRanks.isEmpty();
                System.debug('JWL: assignAppointments: ' + appointments);
                
                while(assignAppointments){
                    Sales_Appointment__c sa = appointments.get(0);
                    Location appointmentLocation = sa.Sales_Order__r.Opportunity__r.Account.ShippingAddress;
                    
                    //Tie break for proximity.
                    Location previousLocation = pdw.repToLastLocationMap.get(repRanks.get(0).user.Id);
                    Integer repRankIndex = null;
                    for(Integer i=0; repRankIndex == null && i < repRanks.size(); i++){
                        if(!assignedCapacityIds.contains(repRanks.get(i).capacityId)
                           && (
                               previousLocation == null ||
                               previousLocation.longitude == null ||
                               appointmentLocation == null ||
                               appointmentLocation.longitude == null ||
                               Location.getDistance(previousLocation, appointmentLocation, 'mi') <= distanceWarning
                           )
                          ){
                              repRankIndex = i;
                          }
                    }
                    
                    if(repRankIndex != null){
                        System.debug('JWL: assigning appointment ' + sa.Id + ' to user ' + repRanks.get(0).user.Name + ' with capacity ' + repRanks.get(repRankIndex).capacityId);
                        Id capacityId = repRanks.get(repRankIndex).capacityId;
                        pdw.assignments.add(
                            new Sales_Appointment_Resource__c(
                                Assignment_Reason__c = 'Auto Assignment'
                                ,Status__c = 'Assigned'
                                ,Primary__c = true
                                ,Sales_Capacity__c = capacityId
                                ,Sales_Appointment__c = sa.Id
                            )
                        );
                        assignedCapacityIds.add(capacityId);
                        
                        pdw.appointmentsToUpdate.put(sa.Id,new Sales_Appointment__c(Id=sa.Id,Suggested_Capacity__c = capacityId));
                        
                        pdw.repToLastLocationMap.put(repRanks.get(repRankIndex).user.Id, appointmentLocation);
                        repRanks.remove(repRankIndex);
                    }
                    appointments.remove(0);
                    assignAppointments = !appointments.isEmpty() && !repRanks.isEmpty();
                }
            
            }*/
            
            Map<id,list<id>> filledslots = new Map<id,List<id>>();
            try{
                for(Sales_Appointment__c sa:pdw.appointMentSlots){
                    if(pdw.slotToUserRankMap.containsKey(sa.Slot__c)){
                        List<SalesSchedProcessDataWrapperss.UserRanks> uranks = pdw.slotToUserRankMap.get(sa.Slot__c)!=null ? pdw.slotToUserRankMap.get(sa.Slot__c) : new List<SalesSchedProcessDataWrapperss.UserRanks> ();
                        for( SalesSchedProcessDataWrapperss.UserRanks u:uranks){
                            if(!filledslots.containsKey(u.User.Id) && sa.slot__c == u.slotId && filledslots.get(u.User.Id).size() == 0){
                                pdw.assignments.add(new Sales_Appointment_Resource__c(Assignment_Reason__c = 'Auto Assignment',Status__c = 'Assigned',Primary__c = true,Sales_Capacity__c = u.capacityId,Sales_Appointment__c = sa.Id));
                                filledslots.put(u.User.Id, new list<id>{sa.slot__c});
                                pdw.appointmentsToUpdate.put(sa.Id,new Sales_Appointment__c(Id=sa.Id,Suggested_Capacity__c = u.capacityId));
                            }else if(filledslots.containsKey(u.User.Id)){pdw.assignments.add(new Sales_Appointment_Resource__c(Assignment_Reason__c = 'Auto Assignment',Status__c = 'Assigned',Primary__c = true,Sales_Capacity__c = u.capacityId,Sales_Appointment__c = sa.Id));
                                                                         pdw.appointmentsToUpdate.put(sa.Id,new Sales_Appointment__c(Id=sa.Id,Suggested_Capacity__c = u.capacityId));
                                                                        }}}
                    
                }
            }catch (exception ex){
                System.debug('JWL: ex san: ' + ex);
            }
            
            System.debug('JWL: pdw: ' + pdw);
            
            return pdw;
        }

    }
    
    private static void sendAutoAssignmentCompleteEmail(SalesSchedProcessDataWrapperss pdw){
        OrgWideEmailAddress owa = [Select Id, Address, DisplayName from OrgWideEmailAddress Where DisplayName = 'Renewal Retail Support' LIMIT 1];
        List<Messaging.SingleEmailMessage> mail = new List<Messaging.SingleEmailMessage>();
        for(User u : [Select ContactId,Contact.Email,Contact.Secondary_Email__c From User Where UserRole.Name Like '%Manager' And Store_Locations__c includes (:pdw.store.Name) And Profile.Name = 'Partner RMS-Sales']){
            Messaging.SingleEmailMessage sem = new Messaging.SingleEmailMessage();
            sem.setTargetObjectId(u.ContactId);
            if(!String.isEmpty(u.Contact.Secondary_Email__c)){
                sem.setCcAddresses(new List<String>{u.Contact.Secondary_Email__c});
            }
            sem.setSubject('Auto Assignment Complete - Do Not Reply');
            sem.setPlainTextBody('The Auto Assignment job has completed for store ' + pdw.store.Name + ' on ' + ((Datetime)pdw.day).formatGmt('MM-dd-yyyy') + '\n'+ 'Do not reply to this email - this mailbox is not monitored, contact your manager with any questions.');
            mail.add(sem);
        }
        Messaging.sendEmail(mail);
    }
    
}