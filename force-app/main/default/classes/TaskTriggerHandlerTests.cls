@isTest
private class TaskTriggerHandlerTests {
	
	@testSetup static void setupData() {
		TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        List<Account> dwellingsToInsert = new List<Account>();  
        Account dwelling1 = utility.createDwellingAccount('Dwelling Account');
        Account dwelling2 = utility.createDwellingAccount('Dwelling Account 2');
        Account store1 = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
        Store_Configuration__c storeConfig1 = [SELECT id FROM Store_Configuration__c WHERE Store__c = :store1.id ];
        dwelling1.Store_Location__c = store1.Id;
        dwelling2.Store_Location__c = store1.Id; 
        dwellingsToInsert.add(dwelling1);
        dwellingsToInsert.add(dwelling2);
        insert dwellingsToInsert; 
        
        List<Contact> contactsToInsert = new List<Contact>();       
        Contact contact1 = new contact ( FirstName='Contact',LastName='1',AccountId=dwelling1.id);
        contactsToInsert.add(contact1);
        insert contactsToInsert;
        
        List<Opportunity> oppToInsert = new List<Opportunity>();
        Opportunity opp1 = utility.createOpportunity(dwelling1.Id, 'Sold');
        oppToInsert.add(opp1);
        insert oppToInsert;
        
        Financial_Account_Number__c finacialAccountNumber1 = new Financial_Account_Number__c (  Store_Configuration__c = storeConfig1.id, Name = '1');
        insert finacialAccountNumber1;
        Financial_Account_Number__c finacialAccountNumber2 = new Financial_Account_Number__c (  Store_Configuration__c = storeConfig1.id, Name = '2');
        insert finacialAccountNumber2;
        
        Financial_Transaction__c finacialTransaction1 = new Financial_Transaction__c(  Store_Configuration__c = storeConfig1.id,
                                                                                     Transaction_Type__c = 'Inventory Received - External Vendor',
                                                                                     Debit_Account_Number__c = finacialAccountNumber1.id,
                                                                                     Credit_Account_Number__c = finacialAccountNumber2.id);
        insert finacialTransaction1;
        
        Product2 masterProduct = new Product2( name = 'master',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Master_Product'));
        Product2 servProduct = new Product2( name = 'service',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        Product2 servProduct2 = new Product2(name = 'service2',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        
        Id pricebookId = Test.getStandardPricebookId();
        system.debug(pricebookId);
        
        List<Product2> products = new List<Product2>{masterProduct,servProduct,servProduct2};
            insert products;
        PricebookEntry pricebookEntryMaster= utility.createPricebookEntry(pricebookId, masterProduct.id);     
        PricebookEntry pricebookEntryServ = utility.createPricebookEntry(pricebookId, servProduct.id);    
        PricebookEntry pricebookEntryServ2 = utility.createPricebookEntry(pricebookId, servProduct2.id);      
        List<PriceBookEntry> pbEntries = new List<PricebookEntry>{pricebookEntryMaster,pricebookEntryServ,pricebookEntryServ2};
            insert pbEntries;
        
        
        Service_Product__c servProd1 = new Service_Product__c(Service_Product__c = servProduct.Id, Master_Product__c = masterProduct.Id);
        Service_Product__c servProd2 = new Service_Product__c(Service_Product__c = servProduct2.Id, Master_Product__c = masterProduct.Id);
        List<Service_Product__c> sprods = new List<Service_Product__c>{servProd1,servProd2};
            insert sprods;
        
		String orgId = UserInfo.getOrganizationId();
    	String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    	Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    	String uniqueName = orgId + dateString + randomInt;  
        
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Partner RMS-RSR%' Limit 1];
        User u = new User(
 			email = uniqueName + '@test' + orgId + '.org',
            Username = uniqueName + '@test' + orgId + '.org',            
            ContactId = contact1.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'          
        );
        insert u;
        
        List<Resource__c> resourceToInsert = new List<Resource__c>();
        Resource__c resource1 = new Resource__c(RbA_User__c=u.Id, Name='Resource1', Resource_Type__c='Tech Measure', Retail_Location__c=storeConfig1.id, Active__c=TRUE);
        insert resource1;        
        
        List<Order> ordersToInsert = new List<Order>();
        
        Order order =  new Order(   Name='Sold Order 1', 
                                 AccountId = dwelling1.id, 
                                 BilltoContactId = contact1.id,
                                 EffectiveDate= Date.Today(), 
                                 OpportunityId = opp1.Id,
                                 Store_Location__c = store1.Id,                           
                                 Status ='Draft', 
                                 Pricebook2Id = pricebookId
                                );
        ordersToInsert.add(order);
        
        Order order2 =  new Order(   Name='Sold Order 2', 
                                  AccountId = dwelling2.id, 
                                  BilltoContactId = contact1.id,
                                  EffectiveDate= Date.Today(), 
                                  Store_Location__c = store1.Id,                           
                                  Status ='Draft', 
                                  Pricebook2Id = pricebookId,
                                  recordTypeId=UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order')
                                 );
        ordersToInsert.add(order2);
        insert ordersToInsert;
        
        OrderItem orderItemMaster = new OrderItem(OrderId = Order.id, PricebookentryId = pricebookEntryMaster.Id, Quantity = 2, UnitPrice = 100);
        insert orderItemMaster;
        
        List<WorkOrder> wosToInsert = new List<WorkOrder>();
        WorkOrder rwo = new WorkOrder(
            recordTypeId=UtilityMethods.retrieveRecordTypeId('Tech_Measure', 'WorkOrder'),
            Sold_Order__c = order.Id,
            AccountId = dwelling1.id,
            ContactId = contact1.id,
            Status = 'To be Scheduled',
            Work_Order_Type__c = 'Service'
        );
        wosToInsert.add(rwo);
        
        WorkOrder rwo2 = new WorkOrder(
            recordTypeId=UtilityMethods.retrieveRecordTypeId('Tech_Measure', 'WorkOrder'),
            Sold_Order__c = order2.Id,
            AccountId = dwelling1.id,
            ContactId = contact1.id,
            Status = 'To be Scheduled',
            Work_Order_Type__c = 'Tech Measure'
        );
        wosToInsert.add(rwo2);
        
        WorkOrder rwo3 = new WorkOrder(
            recordTypeId=UtilityMethods.retrieveRecordTypeId('Install', 'WorkOrder'),
            Sold_Order__c = order2.Id,
            AccountId = dwelling1.id,
            ContactId = contact1.id,
            Status = 'To be Scheduled',
            Work_Order_Type__c = 'Install'
        );
        wosToInsert.add(rwo3);
        insert wosToInsert;
        
        // List<Assigned_Resources__c> arsToInstert = new List<Assigned_Resources__c>();
        // Assigned_Resources__c ar1 = new Assigned_Resources__c(
        //     isPrimary__c = TRUE,
        //     Scheduled_Resource__c = resource1.id,
        //     Work_Order__c = rwo.id);
        // arsToInstert.add(ar1);
        // Assigned_Resources__c ar2 = new Assigned_Resources__c(
        //     isPrimary__c = TRUE,
        //     Scheduled_Resource__c = resource1.id,
        //     Work_Order__c = rwo2.id);
        // arsToInstert.add(ar2);
        // Assigned_Resources__c ar3 = new Assigned_Resources__c(
        //     isPrimary__c = TRUE,
        //     Scheduled_Resource__c = resource1.id,
        //     Work_Order__c = rwo3.id);
        // arsToInstert.add(ar3);
        // insert arsToInstert;
        
        // update arsToInstert;
        
        List<Task> tasksToInsert = new List<Task>();
        Task t = new Task(
            Subject = 'test',
            WhatId = order.Id,
            Status = 'Open'
			// OwnerId = u.Id,
			// Assigned_To__c = u.Id,
            // // Service_Type__c = 'Service Task',
			// recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Task.sObjectType).get('On_Hold')
            // Service_Material_Owner_Id__c = rwo.Service_Material_Owner_Id__c
        );
        tasksToInsert.add(t);
        
        Task t2 = new Task(
            Subject = 'test2',
            WhatId = order2.Id,
            Status = 'Open'
			// OwnerId = u.Id,
			// Assigned_To__c = u.Id,
            // // Service_Type__c = 'Service Material',
			// recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Task.sObjectType).get('On_Hold')
            // Service_Material_Owner_Id__c = rwo.Service_Material_Owner_Id__c
        );
        tasksToInsert.add(t2);
        
        Task t3 = new Task(
        	Subject = 'on hold',
			// OwnerId = u.Id,
			// Assigned_To__c = u.Id,
        	// recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Task.sObjectType).get('On_Hold'),
            WhatId = order.Id
            );
        tasksToInsert.add(t3);
            
        insert tasksToInsert;
        
        update tasksToInsert;
		
		
	}
	
	@isTest
    static void TaskTriggerHandlerTEST() {
		List<Order> soldOrder = [Select Id, Name, Status, Tech_Measure_Status__c,Install_Order_Status__c, Store_Location__c, OpportunityId, OwnerId
									from Order];
		
		List<Order> OrdersToUpdate = new List<Order>();							
		for(Order o: soldOrder){
			o.Status = 'On Hold';
			o.Prior_Status__c = 'Draft';
			o.Apex_Context__c = true;
			OrdersToUpdate.add(o);
		}
		UtilityMethods.disableAllFslTriggers();
		update OrdersToUpdate;

		List<Task> taskList = [SELECT Id, Subject, WhatId, isClosed, recordTypeId FROM Task LIMIT 1];
		List<Task> taskList2 = [SELECT Id, Subject, WhatId, isClosed, recordTypeId FROM Task WHERE Id !=: taskList[0].Id ];
		map<Id,Task> taskMap1 = new map<Id,Task>();
		map<Id,Task> taskMap2 = new map<Id,Task>();
		taskMap1.putAll(taskList);
		taskMap2.putAll(taskList2);
		TaskTriggerHandler.updateOrderStatus(taskMap1, taskMap2);
	}
	
	/*@isTest static void testUpdateOrderStatus(){
		id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        Account dwelling = [Select Id from Account where Name != 'RbA' AND Name != 'Unassigned Account' AND RecordTypeId =: dwellingRT];
		List<Order> soldOrder = [Select Id, Name, Status, Tech_Measure_Status__c,Install_Order_Status__c, Store_Location__c, OpportunityId, OwnerId
								 from Order where accountId =: dwelling.Id 
												  and Name = 'TMNeeded'];
		System.assertEquals(soldOrder.size(),1);
		String priorStatus = 'Draft';
		Test.startTest();
		soldOrder[0].Status = 'On Hold';
		soldOrder[0].Prior_Status__c = 'Draft';
		soldOrder[0].Apex_Context__c = true;
		update soldOrder;
		List<Task> onHoldTasks = [Select Id, WhatId, OwnerId, recordTypeId from Task Where WhatId =: soldOrder[0].Id];
		//System.assertEquals(onHoldTasks.size(),1);
		//System.assertEquals(onHoldTasks[0].OwnerId, soldOrder[0].OwnerId);
		//System.assertEquals(onHoldTasks[0].recordTypeId, UtilityMethods.retrieveRecordTypeId('On_Hold', 'Task'));
		onHoldTasks[0].Status = 'Completed';
		update onHoldTasks;
		soldOrder = [Select Id, Name, Status from Order where Id =: soldOrder[0].Id];
		//System.assertEquals(soldOrder[0].Status, priorStatus);
		Test.stopTest();
	}
	*/
}