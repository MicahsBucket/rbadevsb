@isTest(SeeAllData=false)
global class CanvassUnitQueueableDeletionBatchTest {

    public static testMethod void test1()
    {       
        list<Canvass_Unit_Batch_Fields__c> canvassUnitBatchFieldsList = new list<Canvass_Unit_Batch_Fields__c>();
        Canvass_Unit_Batch_Fields__c canvassUnitBatchField = new Canvass_Unit_Batch_Fields__c();
        canvassUnitBatchField.Name = 'test';
        canvassUnitBatchField.Apex_Batch_Job_Limit__c = 50;
        canvassUnitBatchFieldsList.add(canvassUnitBatchField);
        insert canvassUnitBatchFieldsList;
    
        Custom_Data_Layer_Batch_Queue__c canvassDataLayerQueue = new Custom_Data_Layer_Batch_Queue__c();
        canvassDataLayerQueue.Status__c = 'Queued';
        canvassDataLayerQueue.Field_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids__c = '123, 456, 789';
        insert canvassDataLayerQueue;
        
        Test.startTest();
        database.executebatch(new CanvassUnitQueueableBatch(),1);
        
        CanvassUnitQueueableDeletionBatch rsb = new CanvassUnitQueueableDeletionBatch();
        String sch = '0 0 * * * ?';
        system.schedule('Canvass Unit Queueable Deletion Batch 999', sch, rsb);
    
        Test.stopTest();
    }
}