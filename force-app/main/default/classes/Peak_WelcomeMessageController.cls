/* Copyright © 2016-2018 7Summits, Inc. All rights reserved. */

// ===================
// Peak Welcome Message Controller
// ===================

public without sharing class Peak_WelcomeMessageController {
    @AuraEnabled
    public static Peak_Response getLoginCount() {
        Peak_Response peakResponse = new Peak_Response();
        Integer loginCount = 0;
        try{
            for(LoginHistory history : [SELECT Id, Status, LoginType, LoginUrl, NetworkId FROM LoginHistory WHERE UserId = :UserInfo.getUserId() LIMIT 1000]){
                if(history.Status == 'Success'){
                    loginCount++;
                }
            }
            peakResponse.success = true;
            Peak_ContentObject peakObject = new Peak_ContentObject();
            peakObject.count = loginCount;
            peakResponse.peakResults.add(peakObject);
        }catch(exception e){
            peakResponse.success = false;
            peakResponse.messages.add(e.getMessage());
        }
        return peakResponse;
    }

}