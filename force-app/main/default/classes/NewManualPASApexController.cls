/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Signature Service Community
* @date 6/17
* @description Creates lists of installers based on their association to 
* the survey via junction object and creates those junction objects when 
* new workers are associated with the survey.  Also updates all other field changes
**/

public with sharing class NewManualPASApexController {

//returns a list of all workers associated with a role that are not set to inactive	
@AuraEnabled 
	public static List<Worker__c> getWorkers(String workerRole) {	
		List<Worker__c> workers = new List<Worker__c>();
		Id recordTypeIdStore = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Store').getRecordTypeId();
		Id accountId = [SELECT Store_Number__c, RecordTypeId FROM Account WHERE RecordTypeId = :recordTypeIdStore LIMIT 1].Id;
		
		List<Worker__c> workersWithRoleWithInactive = [SELECT Id, Name, First_Name__c, Last_Name__c, Email__c, Mobile__c, Role__c, Store_Account__c  
													FROM Worker__c 
													WHERE Worker__c.Role__c 
													INCLUDES (:workerRole) AND Store_Account__c = :accountId ORDER BY Worker__c.Last_Name__c DESC];
		for(Worker__c w : workersWithRoleWithInactive){
			if(!w.Role__c.contains('Inactive')){
				workers.add(w);
			}
		}
		return workers;
	}

//saves the post appointment survey
@AuraEnabled
	public static Survey__c savePAS(String firstName, String lastName, String email, 
									String phone, String appointmentDate, String appointmentResult, 
									Id salesRep, String surveyStatus 
									){
		Id recordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName()
                  .get('Post Appointment').getRecordTypeId();
        Id recordTypeIdStore = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Store').getRecordTypeId();

        List<Survey_Worker__c> junctionList = new List<Survey_Worker__c>();
        Date appDate = date.valueOf(appointmentDate);
		Id accountId = [SELECT Store_Number__c, RecordTypeId FROM Account WHERE RecordTypeId = :recordTypeIdStore LIMIT 1].Id;
	
		Survey__c newSurvey = new Survey__c(
		RecordTypeId = recordTypeId,
		Primary_Contact_First_Name__c = firstName,
		Primary_Contact_Last_Name__c = lastName,
		Primary_Contact_Email__c = email,
		Primary_Contact_Mobile_Phone__c = phone,
		Appointment_Date__c = appDate,
		Store_Account__c = accountId,
		Survey_Status__c = surveyStatus,
		Source_System__c = 'Manual Process'
		);
		if(appointmentResult != '------'){
			newSurvey.Appointment_Result__c = appointmentResult;
		}
		if(surveyStatus == 'Send'){
			newSurvey.Send_To_Medallia__c = true;
		}
		Database.insert(newSurvey, true);
		
		//connects the selected sales rep to the current survey
		if(salesRep != null){
			Survey_Worker__c salesRepJunction = new Survey_Worker__c(
				Survey_Id__c = newSurvey.Id,
				Worker_Id__c = salesRep,
				Role__c = 'Sales Rep'
			);
			Database.insert(salesRepJunction, true);
		}
		
		return newSurvey;
	}
		
}