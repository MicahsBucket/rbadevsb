@isTest
public with sharing class closeJobButtonControllerTEST {
    private static final String ACCOUNT_NAME = 'Test Account';
    private static final String ACCOUNT_NAME2 = 'Test Account2'; 

    public static void testSetup(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Account acc = TestDataFactory.createStoreAccount(ACCOUNT_NAME);
        insert acc;
        
        Account acc2 = TestDataFactory.createStoreAccount(ACCOUNT_NAME2);
        insert acc2;
        
        String pricebookId = Test.getStandardPricebookId();
        
        Product2 prod = new Product2();
        prod.Name = 'Test Product 1';
        prod.IsActive = true;
        insert prod;
        
        Product2 pro2 = new Product2();
        pro2.Name = 'Test Product 2';
        pro2.IsActive = true;
        insert pro2;
        
        List<Skill> mySkills = [SELECT Id FROM Skill];
        
        
        PricebookEntry pe = new PricebookEntry();
        pe.IsActive = true;
        pe.Pricebook2Id = pricebookId;
        pe.Product2Id = prod.Id;
        pe.UnitPrice = 45;
        insert pe;
        
        PricebookEntry pe2 = new PricebookEntry();
        pe2.IsActive = true;
        pe2.Pricebook2Id = pricebookId;
        pe2.Product2Id = pro2.Id;
        pe2.UnitPrice = 45;
        insert pe2;
        
        Order testOrder =  new Order();
        testOrder.Name ='Sold Order 1';
        testOrder.AccountId = acc.Id;
        testOrder.EffectiveDate = Date.Today();
        testOrder.Status = 'Install Complete';
        testOrder.Pricebook2Id = pricebookId;
        testOrder.Customer_Pickup_All__c = FALSE;
        testOrder.Installation_Date__c = system.today()-1;
        insert testOrder;
        
        Order testOrder2 =  new Order();
        testOrder2.Name ='Sold Order 1';
        testOrder2.AccountId = acc2.Id;
        testOrder2.EffectiveDate = Date.Today();
        testOrder2.Status = 'Install Complete';
        testOrder2.Pricebook2Id = pricebookId;
        testOrder2.Customer_Pickup_All__c = FALSE;
        testOrder2.Installation_Date__c = system.today()-1;
        
        insert testOrder2;
        
        OrderItem item = new OrderItem();
        item.PricebookEntryId = pe.Id;
        item.OrderId = testOrder.Id;
        item.Product2Id = prod.Id;
        item.Quantity = 2;
        item.UnitPrice = 45;
        insert item;
        
        OrderItem item2 = new OrderItem();
        item2.PricebookEntryId = pe2.Id;
        item2.OrderId = testOrder.Id;
        item2.Product2Id = pro2.Id;
        item2.Quantity = 2;
        item2.UnitPrice = 45;
        insert item2;
    }
    @isTest
    public static void getOrderTEST() {
        testSetup();
        Order testOrder = [SELECT Id FROM Order Limit 1];
        system.debug(testOrder);
        closeJobButtonController.getOrder(testOrder.Id);
    }

    @isTest
    public static void closeJobTEST() {
        testSetup();
        Order testOrder = [SELECT Id FROM Order Limit 1];
        system.debug(testOrder);
        closeJobButtonController.closeJob(testOrder.Id);
    }
}