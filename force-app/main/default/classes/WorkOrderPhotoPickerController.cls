public class WorkOrderPhotoPickerController {
    
    public class WorkOrderPhotoWrapper {
        @AuraEnabled public Id contentVersionId;
        @AuraEnabled public String title;
        @AuraEnabled public String photoDataString;
        @AuraEnabled public String fileType;
    }

    public class RelatedWorkOrderWrapper {
        @AuraEnabled public Order orderRecord;
        @AuraEnabled public List<WorkOrder> workOrders;
        @AuraEnabled public String sObjectType;
    }

    @AuraEnabled
    public static List<Id> getListOfFileIds(String recordId){
        List<Id> fileIds = new List<Id>();
        List<String> validFileTypes = new List<String>{'JPEG', 'JPG', 'PNG'};

        // Get ContentDocumentId's
        List<ContentDocumentLink> contentLinks = [
            SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =:recordId
        ];

        //Get set of ContentDocumentIds
        Set<Id> ContentDocumentIds = new Set<Id>();
        for(ContentDocumentLink cdl : contentLinks){
            if(cdl.ContentDocumentId != null){
                ContentDocumentIds.add(cdl.ContentDocumentId);
            }
        }

        // GET CONTENT VERSION BLOB
        List<ContentVersion> contentVersions = [
            SELECT Id
            FROM ContentVersion
            WHERE ContentDocumentId IN :ContentDocumentIds
            AND IsLatest = true
            AND FileType IN :validFileTypes
        ];

        for(ContentVersion cv : contentVersions){
            fileIds.add(cv.Id);
        }

        return fileIds;

    }

    @AuraEnabled
    public static List<String> getListOfAttachmentIds(String recordId){
        // List<String> validAttachmentTypes = new List<String>{'image/jpeg', 'image/png'};
        list<String> attachmentIds = new List<String>();

        // GETS ATTACHMENTS
        List<Attachment> attachments = [
            SELECT Id
            FROM Attachment
            WHERE ParentId =:recordId
            // AND ContentType IN :validAttachmentTypes
        ];

        for (Attachment a : attachments) {
            attachmentIds.add(a.Id);
        }

        return attachmentIds;

    }

    @AuraEnabled
    public static WorkOrderPhotoWrapper getFile(String fileRecordId){
        //RETURN A LIST OF WorkOrderPhotoWrapper
        WorkOrderPhotoWrapper wopw = new WorkOrderPhotoWrapper();

        // GET CONTENT VERSION BLOB
        ContentVersion contentVersion = [
            SELECT Id,
            VersionData,
            Title,
            FileType
            FROM ContentVersion
            WHERE Id =:fileRecordId
            LIMIT 1
        ];

        if(contentVersion != null){
            // BLOB TO BASE64 STRING
            wopw.photoDataString = EncodingUtil.base64Encode(contentVersion.VersionData);

            String dataFileType = contentVersion.FileType.toLowerCase();
            
            //ADD TITLE, CONTENTVERSIONID AND FILE TYPE
            wopw.title = contentVersion.Title;
            wopw.contentVersionId = contentVersion.Id;
            wopw.fileType = dataFileType;
        }
        return wopw;
    }

    @AuraEnabled
    public static WorkOrderPhotoWrapper getAttachment(String attachmentRecordId){
        // Integer index = 0;
        // for(Attachment a : attachments){

            // String sObjectType = attachmentRecordId.getSObjectType().getDescribe().getName();
            
        
            Attachment att = [
                SELECT Id,
                Body,
                Name,
                ParentId,
                ContentType
                FROM Attachment
                WHERE Id =:attachmentRecordId
                Limit 1
            ];

            //WORKORDER LOGIC
            WorkOrderPhotoWrapper wopw = new WorkOrderPhotoWrapper();
            String photoName = att.Name;
            String fileType = '';

            // image/bmp
            // image/gif
            // image/jpeg
            // image/png
            // image/svg+xml
            // image/tiff
            // image/vnd.adobe.photoshop
            // image/vnd.dwg
            // image/x-photoshop

            // if(att.ContentType == 'image/jpeg'){
            //     fileType = 'jpeg';
            // } else if(att.ContentType == 'image/png'){
            //     fileType = 'png';
            // }

            Integer slashIndex = att.ContentType.indexOf('/');
            fileType = att.ContentType.substring(slashIndex+1, att.ContentType.length());

            // BLOB TO BASE64 STRING
            wopw.photoDataString = EncodingUtil.base64Encode(att.Body);

            //ADD TITLE, CONTENTVERSIONID AND FILE TYPE
            wopw.title = photoName;
            wopw.contentVersionId = att.Id;
            wopw.fileType = fileType;
            
            return wopw;
    }

    @AuraEnabled
    public static RelatedWorkOrderWrapper getRelatedWorkOrders(Id currentWorkOrderId){
        String objectAPIName = currentWorkOrderId.getSObjectType().getDescribe().getName();

        if(objectAPIName == 'Order'){
            //ORDER LOGIC HERE
            Order relatedOrder = [SELECT Id, Name FROM Order WHERE Id =:currentWorkOrderId LIMIT 1];
            List<WorkOrder> workOrders = [SELECT Id, Work_Order_Type__c, Sold_Order__c FROM WorkOrder WHERE Sold_Order__c =:currentWorkOrderId];
            RelatedWorkOrderWrapper rwow = new RelatedWorkOrderWrapper();
            // rwow.sObjectType = objectAPIName;
            rwow.orderRecord = relatedOrder;
            rwow.workOrders = workOrders;
            return rwow;
        } else {
            //GET RELATED ORDER ID
            WorkOrder currentWorkOrder = [SELECT Id, Sold_Order__r.Id, Sold_Order__c FROM WorkOrder WHERE Id =:currentWorkOrderId LIMIT 1];
            Id soldOrderId = currentWorkOrder.Sold_Order__r.Id;

            Order relatedOrder = [SELECT Id, Name FROM Order WHERE Id =:soldOrderId LIMIT 1];

            //GET ALL WORK ORDERS WITH ORDER ID
            List<WorkOrder> workOrders = [SELECT Id, Work_Order_Type__c, Sold_Order__c FROM WorkOrder WHERE Sold_Order__c =:soldOrderId];

            RelatedWorkOrderWrapper rwow = new RelatedWorkOrderWrapper();
            rwow.orderRecord = relatedOrder;
            rwow.workOrders = workOrders;

            return rwow;
        }
    }

    @AuraEnabled
    public static Boolean transferSelectedFiles(List<String> relatedRecordIds, List<String> selectedFileIds){
        List<ContentDocumentLink> cdlsToInsert = new List<ContentDocumentLink>();
        List<Attachment> attachmentsToInsert = new List<Attachment>();

        //TODO: Clone and move attachments.
        List<String> fileIds = new List<String>();
        List<String> attachmentIds = new List<String>();
        for(String sid : selectedFileIds){
            Id thisId = Id.valueOf(sid);
            String apiName = thisId.getSObjectType().getDescribe().getName();
            if(apiName == 'Attachment'){
                attachmentIds.add(sid);
            } else {
                attachmentIds.add(sid);
            }
        }

        //ADD Attachments
        List<Attachment> attachments = [SELECT Id,
            ParentId,
            Name,
            IsPrivate,
            Description,
            ContentType,
            Body,
            BodyLength
            FROM Attachment WHERE Id IN:attachmentIds];
        for(String rrid : relatedRecordIds){
            for(Attachment att : attachments){
                Attachment newAttachment = att.clone(false, true, false, false);
                newAttachment.parentId = rrid;
                attachmentsToInsert.add(newAttachment);
            }
        }

        //ADD FILES
        List<ContentVersion> contentVersions = [
            SELECT Id,
            ContentDocumentId
            FROM ContentVersion
            WHERE Id IN :fileIds
        ];

        for (String recordId : relatedRecordIds) {
            for(ContentVersion cv : contentVersions){
                ContentDocumentLink cdl  = new ContentDocumentLink();
                cdl.ContentDocumentId = cv.ContentDocumentId;
                cdl.LinkedEntityId = recordId;
                cdl.ShareType = 'I';
                cdlsToInsert.add(cdl);
            }
        }

        try {
            Database.insert(cdlsToInsert, true);
            Database.insert(attachmentsToInsert, true);
            return true;
        } catch (Exception e) {
            System.debug(e);
            throw e;
            // return false;
        }
    }


        // @AuraEnabled
    // public static List<WorkOrderPhotoWrapper> getWorkOrderPhotos(String recordId){
    //     //LIST OF VALID FILE TYPES TO LOOK FOR
    //     List<String> validFileTypes = new List<String>{'JPEG', 'JPG', 'PNG'};
    //     List<String> validAttachmentTypes = new List<String>{'image/jpeg', 'image/png'};

    //     //GETS FILES
    //     //==========================================================================================

    //     // Get ContentDocumentId's
    //     List<ContentDocumentLink> contentLinks = [
    //         SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =:recordId
    //     ];

    //     //Get set of ContentDocumentIds
    //     Set<Id> ContentDocumentIds = new Set<Id>();
    //     for(ContentDocumentLink cdl : contentLinks){
    //         if(cdl.ContentDocumentId != null){
    //             ContentDocumentIds.add(cdl.ContentDocumentId);
    //         }
    //     }

    //     // GET CONTENT VERSION BLOB
    //     List<ContentVersion> contentVersions = [
    //         SELECT Id,
    //         VersionData,
    //         Title,
    //         FileType
    //         FROM ContentVersion
    //         WHERE ContentDocumentId IN :ContentDocumentIds
    //         AND IsLatest = true
    //         AND FileType IN :validFileTypes
    //     ];
    //     //==========================================================================================

    //     // GETS ATTACHMENTS
    //     List<Attachment> attachments = [
    //         SELECT Id,
    //         Body,
    //         ParentId,
    //         ContentType
    //         FROM Attachment
    //         WHERE ParentId =:recordId
    //         AND ContentType IN :validAttachmentTypes
    //     ];

        
        
    //     // List<String> encodedBlobStrings = new List<String>();
    //     List<WorkOrderPhotoWrapper> workOrderPhotoWrappers = new List<WorkOrderPhotoWrapper>();
    //     Integer index = 0;
    //     for(Attachment a : attachments){
    //         WorkOrderPhotoWrapper wopw = new WorkOrderPhotoWrapper();
    //         String photoName = 'Photo_';
    //         String fileType = '';

    //         if(a.ContentType == 'image/jpeg'){
    //             fileType = 'jpeg';
    //         } else if(a.ContentType == 'image/png'){
    //             fileType = 'png';
    //         }

    //         // BLOB TO BASE64 STRING
    //         // wopw.photoDataString = EncodingUtil.base64Encode(a.Body);

    //         //ADD TITLE, CONTENTVERSIONID AND FILE TYPE
    //         wopw.title = photoName + String.valueOf(index);
    //         wopw.contentVersionId = a.Id;
    //         wopw.fileType = fileType;

    //         workOrderPhotoWrappers.add(wopw);

    //         index++;
    //     }

    //     // CREATE LIST OF WorkOrderPhotoWrapper
    //     for(ContentVersion cv : contentVersions){
    //         if(cv.VersionData != null){
    //             WorkOrderPhotoWrapper wopw = new WorkOrderPhotoWrapper();

    //             // BLOB TO BASE64 STRING
    //             // wopw.photoDataString = EncodingUtil.base64Encode(cv.VersionData);

    //             String dataFileType = cv.FileType.toLowerCase();
                
    //             //ADD TITLE, CONTENTVERSIONID AND FILE TYPE
    //             wopw.title = cv.Title;
    //             wopw.contentVersionId = cv.Id;
    //             wopw.fileType = dataFileType;

    //             workOrderPhotoWrappers.add(wopw);
    //         }
    //     }

    //     return workOrderPhotoWrappers;
    // }
}