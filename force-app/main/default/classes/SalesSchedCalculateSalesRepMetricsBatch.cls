/**
 * @File Name          : SalesSchedCalculateSalesRepMetricsBatch.cls
 * @Description        : 
 * @Author             : James Loghry (Demand Chain)
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 11-05-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/7/2019, 12:13:49 PM   James Loghry (Demand Chain)     Initial Version
**/
public class SalesSchedCalculateSalesRepMetricsBatch implements Database.Batchable<SObject> {
	/** @description Executes SalesSchedCalculateSalesRepMetrics for each Active Rep User (SS19-84 / SS19-97). 
	  * @author Demand Chain -  */
	
    public List<String> repProfileNames {get; set;}
	public string strQuery = '';
    public Date reportDate {get; set;}
    public Time storeCloseTime {get; set;}

	public SalesSchedCalculateSalesRepMetricsBatch(Date reportDate, Time storeCloseTime) {
        this.reportDate = reportDate;
        this.repProfileNames = new List<String>{'Partner RMS-Sales'};
        this.storeCloseTime = storeCloseTIme;
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
    public Database.QueryLocator start(Database.BatchableContext context){
        String query = 
            'SELECT ' +
            	'Id,'+
            	'Name ' +
            'FROM ' +
            	'User ' +
            'WHERE ' +
            	'IsActive = true ' +
            	'And Profile.Name in :repProfileNames ' +
				'And Contact.Account.Active_Store_Configuration__r.Call_Center_Close_Time__c = :storeCloseTime ' +
				'And Contact.Unavailable__c = false';

        if(Test.isRunningTest()){
            query += ' Limit 100';
        }
        System.debug('JWL: query: ' + query);
		return Database.getQueryLocator(query);
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	public void execute(Database.BatchableContext context, List<User> scope) {
        SalesSchedCalculateSalesRepMetrics.calculateMetrics(reportDate,scope);
	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	public void finish(Database.BatchableContext context) {
		Database.executeBatch(new SalesSchedAssignmentBatch(reportDate, storeCloseTIme),1);
	}
}