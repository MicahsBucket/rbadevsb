@isTest
public with sharing class RMS_createWorkOrderControllerTest {


    static testMethod void testingConstructor(){
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        id muniPermitRT = UtilityMethods.retrieveRecordTypeId('Permit', 'Municipality__c');
        id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        Account store = [SELECT id FROM Account WHERE Name ='77 - Twin Cities, MN'];
        Account newDwelling = new Account(Name='test1', Store_Location__c = store.id, RecordTypeId = dwellingRT, BillingPostalCode = '55429' , BillingCity = 'Blaine' );
        insert newDwelling;
        
        Municipality__c muni1 = new Municipality__c( name = 'Muni 1', recordTypeId=muniPermitRT, Application_Notes__c = 'Testing Notes', Permit_Fee_Notes__c = 'Permit fee Notes test', City_Township__c = 'Chicago', State__c = 'IL', Zip_Code__c = '60618', County__c = 'Cook', Phone__c = '555-555-5555', For_Retail_Location__c = store.Id);
        
            
        //CREATE AN ORDER RELATED TO OPPORTUNITY
        Order ord1 = new Order( AccountId = newDwelling.id ,
                                Status = 'In Progress', 
                                EffectiveDate = Date.Today(),
                                Pricebook2Id = Test.getStandardPricebookId()
                                );
        insert ord1;
        
        //CREATE OPPORTUNITY
        Opportunity opp1 = new Opportunity(Name='Opp1', AccountId=newDwelling.id, StageName = 'New', CloseDate = Date.today().addYears(1) );
        insert opp1;
        
        Id serviceOrderVisitRecordTypeId = UtilityMethods.retrieveRecordTypeId('Install', 'WorkOrder');
        WorkOrder workOrder = new WorkOrder(RecordTypeId = serviceOrderVisitRecordTypeId);
        
         /*Store_Configuration__c storeConfig = new Store_Configuration__c(
                Store__c = store.id,
                
                Location_Number__c = '777',
                Order_Number__c = 0000001
                );
                insert storeConfig;
    store.Active_Store_Configuration__c = storeConfig.Id;
            update store;*/
        
        String actionUrl = '/apex/RMS_createActionsWorkOrder?'+
        'type=actions'+
        '&orderId='+ord1.id+
        '&storeId='+store.id+        
        '&accountId='+newDwelling.id+
        '&billingStreet=300 Stanley Ave'+
        '&billingCity=Long Beach'+
        '&billingPostalCode=California'+
        '&billingCountry=United States'+
        '&relatedOpp='+opp1.id+
        '&permit='+
        '&hoa='+
        '&historical=';
        
        PageReference pageRef = new PageReference(actionUrl); 
        Test.setCurrentPage(pageRef);
            
        ApexPages.StandardController stdController = new ApexPages.StandardController(workOrder);
        RMS_createWorkOrderController customController  = new RMS_createWorkOrderController(stdController);
        
        System.assertEquals(ord1.id, customController.orderId);
        System.assertEquals('actions', customController.woType);
        System.assertEquals(newDwelling.Id, customController.accountId);
        
        String visitUrl = '/apex/RMS_createActionsWorkOrder?'+
        'type=visit'+
        '&orderId='+ord1.id+
        '&accountId='+newDwelling.id+
        '&billingStreet=300 Stanley Ave'+
        '&billingCity=Long Beach'+
        '&billingPostalCode=California'+
        '&billingCountry=United States'+
        '&relatedOpp='+opp1.id+
        '&permit='+
        '&hoa='+
        '&historical=';

        PageReference pageRefVisit = new PageReference(visitUrl); 
        Test.setCurrentPage(pageRefVisit);
            
        ApexPages.StandardController stdControllerVisit = new ApexPages.StandardController(workOrder);
        RMS_createWorkOrderController customControllerVisit  = new RMS_createWorkOrderController(stdControllerVisit);
        
        System.assertEquals(ord1.id, customControllerVisit.orderId);
        System.assertEquals('visit', customControllerVisit.woType);
        System.assertEquals(newDwelling.Id, customControllerVisit.accountId);
        
        String lswpUrl = '/apex/RMS_createActionsWorkOrder?'+
        'type=lswp'+
        '&orderId='+ord1.id+
        '&accountId='+newDwelling.id+
        '&billingStreet=300 Stanley Ave'+
        '&billingCity=Long Beach'+
        '&billingPostalCode=California'+
        '&billingCountry=United States'+
        '&relatedOpp='+opp1.id+
        '&permit='+
        '&hoa='+
        '&historical=';
        PageReference pageReflswp = new PageReference(lswpUrl); 
        Test.setCurrentPage(pageReflswp);
            
        ApexPages.StandardController stdControllerlswp = new ApexPages.StandardController(workOrder);
        RMS_createWorkOrderController customControllerlswp = new RMS_createWorkOrderController(stdControllerlswp);
        
        System.assertEquals(ord1.id, customControllerlswp.orderId);
        System.assertEquals('lswp', customControllerlswp.woType);
        System.assertEquals(newDwelling.Id, customControllerlswp.accountId);
        
        String paintStainUrl = '/apex/RMS_createActionsWorkOrder?'+
        'type=paintStain'+
        '&orderId='+ord1.id+
        '&accountId='+newDwelling.id+
        '&billingStreet=300 Stanley Ave'+
        '&billingCity=Long Beach'+
        '&billingPostalCode=California'+
        '&billingCountry=United States'+
        '&relatedOpp='+opp1.id+
        '&permit='+
        '&hoa='+
        '&historical=';
        PageReference pageRefpaintStain = new PageReference(paintStainUrl); 
        Test.setCurrentPage(pageRefpaintStain);
            
        ApexPages.StandardController stdControllerpaintStain = new ApexPages.StandardController(workOrder);
        RMS_createWorkOrderController customControllerpaintStain  = new RMS_createWorkOrderController(stdControllerpaintStain);
        
        System.assertEquals(ord1.id, customControllerpaintStain.orderId);
        System.assertEquals('paintStain', customControllerpaintStain.woType);
        System.assertEquals(newDwelling.Id, customControllerpaintStain.accountId);
        
        String permitUrl = '/apex/RMS_createActionsWorkOrder?'+
        'type=permit'+
        '&orderId='+ord1.id+
        '&accountId='+newDwelling.id+
        '&billingStreet=300 Stanley Ave'+
        '&billingCity=Long Beach'+
        '&billingPostalCode=California'+
        '&billingCountry=United States'+
        '&relatedOpp='+opp1.id+
        '&permit='+muni1.id+
        '&hoa='+
        '&historical=';
        PageReference pageRefpermit = new PageReference(permitUrl); 
        Test.setCurrentPage(pageRefpermit);
            
        ApexPages.StandardController stdControllerpermit = new ApexPages.StandardController(workOrder);
        RMS_createWorkOrderController customControllerpermit  = new RMS_createWorkOrderController(stdControllerpermit);
        
        System.assertEquals(ord1.id, customControllerpermit.orderId);
        System.assertEquals('permit', customControllerpermit.woType);
        System.assertEquals(newDwelling.Id, customControllerpermit.accountId);
        
         String ServiceUrl = '/apex/RMS_createActionsWorkOrder?'+
    'type=Service'+
    '&orderId='+ord1.id+
    '&accountId='+newDwelling.id+
    '&billingStreet=300 Stanley Ave'+
    '&billingCity=Long Beach'+
    '&billingPostalCode=California'+
    '&billingCountry=United States'+
    '&relatedOpp='+opp1.id+
    '&permit='+
    '&hoa='+
    '&historical=';
    PageReference pageRefService = new PageReference(ServiceUrl); 
    Test.setCurrentPage(pageRefService);
      
    ApexPages.StandardController stdControllerService = new ApexPages.StandardController(workOrder);
    RMS_createWorkOrderController customControllerService  = new RMS_createWorkOrderController(stdControllerService);
    
     String hoaUrl = '/apex/RMS_createActionsWorkOrder?'+
    'type=hoa'+
    '&orderId='+ord1.id+
    '&accountId='+newDwelling.id+
    '&billingStreet=300 Stanley Ave'+
    '&billingCity=Long Beach'+
    '&billingPostalCode=California'+
    '&billingCountry=United States'+
    '&relatedOpp='+opp1.id+
    '&permit='+
    '&hoa='+muni1.id+
    '&historical=';
    PageReference pageRefhoa = new PageReference(hoaUrl); 
    Test.setCurrentPage(pageRefhoa);
      
    ApexPages.StandardController stdControllerhoa = new ApexPages.StandardController(workOrder);
    RMS_createWorkOrderController customControllerhoa  = new RMS_createWorkOrderController(stdControllerhoa);
    
     String historicalUrl = '/apex/RMS_createActionsWorkOrder?'+
    'type=historical'+
    '&orderId='+ord1.id+
    '&accountId='+newDwelling.id+
    '&billingStreet=300 Stanley Ave'+
    '&billingCity=Long Beach'+
    '&billingPostalCode=California'+
    '&billingCountry=United States'+
    '&relatedOpp='+opp1.id+
    '&permit='+
    '&hoa='+
    '&historical='+muni1.id;
    PageReference pageRefhistorical = new PageReference(historicalUrl); 
    Test.setCurrentPage(pageRefhistorical);
      
    ApexPages.StandardController stdControllerhistorical = new ApexPages.StandardController(workOrder);
    RMS_createWorkOrderController customControllerhistorical  = new RMS_createWorkOrderController(stdControllerhistorical);
    
     String collectionsUrl = '/apex/RMS_createActionsWorkOrder?'+
    'type=collections'+
    '&orderId='+ord1.id+
    '&accountId='+newDwelling.id+
    '&billingStreet=300 Stanley Ave'+
    '&billingCity=Long Beach'+
    '&billingPostalCode=California'+
    '&billingCountry=United States'+
    '&relatedOpp='+opp1.id+
    '&permit='+
    '&hoa='+
    '&historical=';
    PageReference pageRefcollections = new PageReference(collectionsUrl); 
    Test.setCurrentPage(pageRefcollections);
      
    ApexPages.StandardController stdControllercollections = new ApexPages.StandardController(workOrder);
    RMS_createWorkOrderController customControllercollection  = new RMS_createWorkOrderController(stdControllercollections);
    
    customControllercollection.propPickValSelected = 'test';
    //customControllercollection.pickLstValue = 'test2';


        customControllerpermit.save();
    
    }

}