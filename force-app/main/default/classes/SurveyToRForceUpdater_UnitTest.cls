@isTest
private class SurveyToRForceUpdater_UnitTest {
	@isTest static void updateContactsTest() {
		TestDataFactoryStatic.setUpConfigs();
		Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
		Database.insert(storeAccount, true);
		Id storeAccountId = storeAccount.Id;
		Id storeConfigId = storeAccount.Active_Store_Configuration__c;
		Account account1 = TestDataFactory.createStoreAccount('Calvins Store');
		Database.insert(account1, true);
		Id accountId = account1.Id;
		Contact primaryContact = new Contact(
			AccountId = accountId,
			FirstName = 'PrimFirst',
			LastName = 'PrimLast',
			Email = 'prime@testing.com',
			Phone = '(763) 555-0000',
			HomePhone = '(763) 555-1111',
			MobilePhone = '(763) 555-2222',
			MailingCountry = 'United States',
			MailingState = 'Minnesota',
			MailingCity = 'Minneapolis',
			MailingPostalCode = '55514'

		);
		Database.insert(primaryContact, true);

		Contact secondaryContact = new Contact(
			AccountId = accountId,
			FirstName = 'SecFirst',
			LastName = 'SecLast',
			Email = 'second@testing.com',
			Phone = '(763) 123-0000',
			HomePhone = '(763) 123-1111',
			MobilePhone = '(763) 123-2222',
			MailingCountry = 'United States',
			MailingState = 'New York',
			MailingCity = 'New York',
			MailingPostalCode = '52214'
		);
		Database.insert(secondaryContact, true);

		Id recordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get('Post Install').getRecordTypeId();
		Survey__c newSurvey = new Survey__c(
                Source_System__c = 'rForce',
                RecordTypeId = recordTypeId,
                Store_Account__c = storeAccountId,
                Installation_Date__c = Date.Today(),
                
                Primary_Contact_First_Name__c = primaryContact.FirstName,
                Primary_Contact_Last_Name__c = primaryContact.LastName,
                Primary_Contact_Email__c = primaryContact.Email,
                Primary_Contact_Home_Phone__c = primaryContact.HomePhone,
                Primary_Contact_Work_Phone__c = primaryContact.Phone,
                Primary_Contact_Mobile_Phone__c = primaryContact.MobilePhone,
                State__c = primaryContact.MailingState,
                City__c = primaryContact.MailingCity,
                Country__c = primaryContact.MailingCountry,
                Zip__c = primaryContact.MailingPostalCode,
                Secondary_Contact_First_Name__c = secondaryContact.FirstName,
                Secondary_Contact_Last_Name__c = secondaryContact.LastName,
                Secondary_Contact_Email__c = secondaryContact.Email,
                Secondary_Contact_Home_Phone__c = secondaryContact.HomePhone,
                Secondary_Contact_Mobile_Phone__c = secondaryContact.MobilePhone,
                Secondary_Contact_Work_Phone__c = secondaryContact.Phone
            );
		Database.insert(newSurvey, true);

		Contact_Survey__c newJunc = new Contact_Survey__c(Contact_Id__c = primaryContact.Id, Survey_Id__c = newSurvey.Id, Type__c = 'Primary');
		Database.insert(newJunc);

		Contact_Survey__c newJunc2 = new Contact_Survey__c(Contact_Id__c = secondaryContact.Id, Survey_Id__c = newSurvey.Id, Type__c = 'Secondary');
		Database.insert(newJunc2);

		Test.startTest();
			newSurvey.Primary_Contact_First_Name__c = 'newName';
			newSurvey.Secondary_Contact_Email__c = 'newemail@email.com';
			newSurvey.Secondary_Contact_Mobile_Phone__c = '(123) 456-7890';
			Database.update(newSurvey);
		Test.stopTest();
		String primaryFirstName = [SELECT Id, FirstName FROM Contact WHERE Id = :primaryContact.Id].FirstName;
		system.assertEquals(primaryFirstName, newSurvey.Primary_Contact_First_Name__c);
		String secondaryEmail = [SELECT Id, Email FROM Contact WHERE Id = :secondaryContact.Id].Email;
		system.assertEquals(secondaryEmail, newSurvey.Secondary_Contact_email__c);
		String secondaryPhone = [SELECT Id, MobilePhone FROM Contact WHERE Id = :secondaryContact.Id].MobilePhone;
		system.assertNotEquals(secondaryPhone, '(763) 123-2222');
	}
	
}