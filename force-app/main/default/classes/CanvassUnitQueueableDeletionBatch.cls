global class CanvassUnitQueueableDeletionBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable 
{
    //database.executebatch(new CanvassUnitQueueableDeletionBatch(),1);
    
    //CanvassUnitQueueableDeletionBatch rsb = new CanvassUnitQueueableDeletionBatch();
    //String sch = '0 0 * * * ?';
    //system.schedule('Canvass Unit Queueable Deletion Batch 00', sch, rsb);
    
    //CanvassUnitQueueableDeletionBatch rsb = new CanvassUnitQueueableDeletionBatch();
    //String sch = '0 15 * * * ?';
    //system.schedule('Canvass Unit Queueable Deletion Batch 15', sch, rsb);
    
    //CanvassUnitQueueableDeletionBatch rsb = new CanvassUnitQueueableDeletionBatch();
    //String sch = '0 30 * * * ?';
    //system.schedule('Canvass Unit Queueable Deletion Batch 30', sch, rsb);
    
    //CanvassUnitQueueableDeletionBatch rsb = new CanvassUnitQueueableDeletionBatch();
    //String sch = '0 45 * * * ?';
    //system.schedule('Canvass Unit Queueable Deletion Batch 45', sch, rsb);

    global CanvassUnitQueueableDeletionBatch()
    {

    }
    
    global void execute(SchedulableContext SC)
    {
        database.executeBatch(new CanvassUnitQueueableDeletionBatch(),1);
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        // query to get holding jobs from queue
        return Database.getQueryLocator ([SELECT Id, Name, Field_Ids__c, Marker_Ids__c, Marker_Ids2__c, Market_Id__c, Status__c, ZipCode__c 
                                          FROM Custom_Data_Layer_Batch_Queue__c 
                                          WHERE Status__c = 'Queued']);
    }
    
    global void execute (Database.BatchableContext BC, List<Custom_Data_Layer_Batch_Queue__c> scope)
    {
        List<Canvass_Unit_Batch_Fields__c> apexBatchJobLimitList = [SELECT Id, Name, Apex_Batch_Job_Limit__c FROM Canvass_Unit_Batch_Fields__c];
        
        if(apexBatchJobLimitList.size() > 0)
        {
            // query to get currently running jobs
            list<AsyncApexJob> listOfRunningJobs = [SELECT Status, NumberOfErrors, ApexClassID, CompletedDate, ExtendedStatus, JobItemsProcessed, JobType, MethodName, TotalJobItems 
                                                    FROM AsyncApexJob 
                                                    WHERE Status != 'Completed' AND Status != 'Aborted' AND Status != 'Failed'];
            
            // if running jobs less than  50
            if(listOfRunningJobs.size() < apexBatchJobLimitList[0].Apex_Batch_Job_Limit__c || Test.isRunningTest() == true)
            {
                // iterate over scope
                for(Custom_Data_Layer_Batch_Queue__c queuedJob :scope)
                {                    
                    if (!Test.isRunningTest())
                    {
                        // start queueable job
                        System.enqueueJob(new CanvassUnitDeletionQueueable(queuedJob.ZipCode__c,queuedJob.Market_Id__c,queuedJob.Id));
                    }
                    
                    // update status so that job won't be queued again
                    queuedJob.Status__c = 'Deleting Records';
                    update queuedJob;
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {

    }
}