/**
 * @File Name          : MakabilityCheckFrameNotchConfig.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 02-24-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/1/2019, 1:09:00 PM   mark.rothermal@andersencorp.com     Initial Version
**/
public without sharing class MakabilityCheckFrameNotchConfig implements MakabilityService {

      /**
      * @description run makability on list of order items and return results
      * @author mark.rothermal@andersencorp.com | 5/1/2019
      * @param map<string, MakabilityRestResource.OrderItem> requests
      * @param set<id> productIds
      * @return List<MakabilityRestResource.MakabilityResult>
      */
      public static List<MakabilityRestResource.MakabilityResult>  checkCompatibility( map <string,MakabilityRestResource.OrderItem> requests, set<id> productIds){
        List<MakabilityRestResource.MakabilityResult> results = new List<MakabilityRestResource.MakabilityResult>();
        Map<id,Frame_Notch_Configuration__c> configMap = getConfigOptions(productIds);
        Map<String,Frame_Notch_Configuration__c> uniKeyToConfigMap = new Map<String,Frame_Notch_Configuration__c>();
        
        system.debug('requests in check size! '+requests);
        
        // handle no config record found.
        if(configMap.keyset().size() == 0){
            for(String r:requests.keyset()){
                MakabilityRestResource.OrderItem oi = requests.get(r);
                MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult(); 
                List<string> errMessages = new List<string>();
                string errMessage = 'Salesforce - No Frame Notch configurations found for this productId.';  
                errMessages.add(errMessage);
                result.errorMessages = errMessages;
                result.orderId = oi.orderId;
                result.orderItemId = oi.orderItemId;
                results.add(result);                
            }            
            return results;
        }
                
        // populate config map
        for(id fncId:configMap.keyset()) {
                Frame_Notch_Configuration__c fnc = configMap.get(fncId);
                String uniKey = fnc.ProductConfigurationLookup__r.Product__c + '-' + fnc.Frame_Type__c + fnc.Pocket_Notch__c;
                uniKeyToConfigMap.put(uniKey,fnc);

        }       
        ////////////////////////////// 
        // run makability check  
        // //////////////////////////        
        for(String r:requests.keyset()){
            MakabilityRestResource.OrderItem req = requests.get(r);                  
            List<string> errMessages = new List<string>();        
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
            result.orderItemId = req.orderItemId;
            result.orderId = req.orderId;
            ProductConfiguration p = req.ProductConfiguration;
            Boolean frameNotchPasses = true;
			Boolean runMakability  = req.makabilityCalculator.checkFrameNotch ;
            String requestKey = p.productId + '-' + p.frame + p.insertFrame;
            ///////////////////////////////////////////////////////////////////
            // compare request against matching config  
            ///////////////////////////////////////////////////////////////////
            if(runMakability){
                if(uniKeyToConfigMap.containsKey(requestKey)){
                 Frame_Notch_Configuration__c currentConfig = uniKeyToConfigMap.get(requestKey);
                 List<String> exteriorTrimList = currentConfig.Exterior_Trim__c.split(';');
                    if(!exteriorTrimList.contains(p.exteriorTrim)){
                    frameNotchPasses = false;
                    errMessages.add('Frame Notch Config - The Exterior Trim selected is not valid with the Frame Type & Insert Frame selections.');
                    //+ 
                    //'Available trim choices include:  ' + currentConfig.Exterior_Trim__c );
                    }
                } 
                // else{
                //     frameNotchPasses = false;
                //     errMessages.add('Frame Notch Config - Incorrect Frame Type');
                // }
                                                    
            }
            if(frameNotchPasses){
                errMessages.add('Frame Notch Config  - passed');  
                result.isMakable = true;                          
            }else{
                result.isMakable = false;  
            }   
            result.errorMessages = errMessages;                               
            results.add(result);    
        }
        system.debug('results check ' + results );        
        if(results.size() == 0){
            List<string> errMessages = new List<string>();                    
            errMessages.add('Something bad happened with the Frame Notch config');
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
            result.errorMessages = errMessages;
            result.isMakable = false;
            results.add(result);
        }         
        return results;
    }
    
    /**
    * @description get config options for makability testing
    * @author mark.rothermal@andersencorp.com | 5/1/2019
    * @param set<id> productIds
    * @return Map<id, Frame_Notch_Configuration__c>
    */
    public static Map<id,Frame_Notch_Configuration__c> getConfigOptions(set<id> productIds){
        Map<id,Frame_Notch_Configuration__c> configMap= new Map<id,Frame_Notch_Configuration__c>( [
            SELECT id,
            Pocket_Notch__c,
            Frame_Type__c,
            Exterior_Trim__c,            
            ProductConfigurationLookup__r.Product__c
            from Frame_Notch_Configuration__c 
            where ProductConfigurationLookup__r.Product__c in :productids] );  
        return configMap;
    }
    

    
}