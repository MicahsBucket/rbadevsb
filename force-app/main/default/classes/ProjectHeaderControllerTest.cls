@isTest
public class ProjectHeaderControllerTest {
    static ID selectedOrdId;
    @testSetup
    static void setup(){
        TestDataFactoryStatic.setUpConfigs();
        Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
        Database.insert(storeAccount, true);
        Id storeAccountId = storeAccount.Id;
        Id storeConfigId = storeAccount.Active_Store_Configuration__c;
        Id accountId = TestDataFactoryStatic.createAccount('Calvins', storeAccountId).Id;
        Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'CalvinPrime');
        Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('Calvins Opportunity', accountId, storeAccountId, 'Sold', date.today());
        System.debug('opportunity.Store_Location__c: ' + opportunity.Store_Location__c);
        OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
        Order ord = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        Order ord2 = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        Order ord3 = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        ord.EffectiveDate = date.today();
        ord2.EffectiveDate = date.today().addDays(-1);
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'Super Administrator' LIMIT 1].Id;
        User currentUser = TestDataFactoryStatic.createUser(profileID);
        currentUser.FirstName = 'testUser1';
        currentUser.Selected_Order__c=ord.id;
        selectedOrdId=ord2.Id;
        Database.insert(currentUser);
        ord.CustomerPortalUser__c = currentUser.Id;
        ord2.CustomerPortalUser__c = currentUser.Id;
        List<Order> ordList = new List<Order>{ord, ord2, ord3};
            Database.update(ordList);
    }
    
    @isTest
    private static void getSelectedOrderTest(){
		User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1' LIMIT 1 ];
        System.runAs(currentUser){
            Test.startTest();            
            LightningResponse lr = ProjectHeaderController.getSelectedOrder();
            Test.stopTest();            
            //system.debug('lr in getSelectedOrder ' + lr );
            string u = lr.jsonResponse;
            //System.assert(u.contains('test-initial-value'));
        }
    }
    
    @isTest
    private static void updateSelectedOrderTest(){
        
        Id updateId = UserInfo.getUserId();
       // update currentUser;
       Order StoreInfo = [SELECT Id, Store_Location__c, Store_Location__r.Legal_Name__c, Store_Location__r.Phone,
                               Store_Location__r.BillingStreet, Store_Location__r.BillingCity, Store_Location__r.BillingPostalCode, 
                               Store_Location__r.BillingState, Store_Location__r.BillingCountry
                               FROM Order 
                               ORDER BY createdDate DESC LIMIT 1];
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1' LIMIT 1 ];
        System.runAs(currentUser){
            Test.startTest();
            LightningResponse lr = ProjectHeaderController.updateSelectedOrder(StoreInfo.Id);
            Test.stopTest();
            User testResults = [SELECT Id, Selected_Order__c FROM User WHERE FirstName = 'testUser1'];
            system.assertEquals(StoreInfo.Id, testResults.Selected_Order__c);
            //system.debug('lr in getAllORders ' + lr );
        }
    }
    
    @isTest
    private static void getAllOrdersTest(){
       Id profileId = [SELECT Id FROM Profile WHERE Name = 'Super Administrator' LIMIT 1].Id;
        User currentUser = TestDataFactoryStatic.createUser(profileID);
        currentUser.FirstName = 'testUser1';
        Database.insert(currentUser);       
        System.runAs(currentUser){
            Test.startTest();                        
            LightningResponse lr = ProjectHeaderController.getAllOrders();
            Test.stopTest();            
            //system.debug('lr in getAllORders ' + lr.jsonResponse );
            System.assert(lr.state == 'SUCCESS');
        }
    }
    
        @isTest
    static void getOrderIdTest(){
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
            Id orderId = ProjectHeaderController.getOrderId();
            Order ord = [SELECT EffectiveDate FROM Order ORDER BY createdDate DESC LIMIT 1];
            // System.assertEquals(Date.today(), ord.EffectiveDate);
        }
    }
    
     @isTest
    static void getOrdersTest(){
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
            List<Order> orderList = ProjectHeaderController.getOrders();
            System.assertEquals(2, orderList.size());
        }
    }
    @isTest
    static void getNewOrderId(){
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
            Id ordActualId = ProjectHeaderController.getNewOrderId(String.valueOf(Date.today()));
            Id ordExpectedId = [SELECT Id FROM Order WHERE EffectiveDate = :Date.today() LIMIT 1].Id;
            System.assertEquals(ordExpectedId, ordActualId);
        }
    }
   
    @isTest
    static void getStoreInfoUpdatedTest() {
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
            Order StoreInfo = [SELECT Id, Store_Location__c, Store_Location__r.Legal_Name__c, Store_Location__r.Phone,
                               Store_Location__r.BillingStreet, Store_Location__r.BillingCity, Store_Location__r.BillingPostalCode, 
                               Store_Location__r.BillingState, Store_Location__r.BillingCountry
                               FROM Order 
                               ORDER BY createdDate DESC LIMIT 1];
            ProjectHeaderController.OrderStoreConfigWrap thisOrder =ProjectHeaderController.getStoreInfoUpdated(StoreInfo.Id);
        }
    }
        @isTest
    static void getStoreInfoTest() {
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
            ProjectHeaderController.OrderStoreConfigWrap thisOrder = ProjectHeaderController.getStoreInfo();
            Order StoreInfo = [SELECT Id, Store_Location__c, Store_Location__r.Legal_Name__c, Store_Location__r.Phone,
                               Store_Location__r.BillingStreet, Store_Location__r.BillingCity, Store_Location__r.BillingPostalCode, 
                               Store_Location__r.BillingState, Store_Location__r.BillingCountry
                               FROM Order 
                               ORDER BY createdDate DESC LIMIT 1];
        }
    }
    
    
}