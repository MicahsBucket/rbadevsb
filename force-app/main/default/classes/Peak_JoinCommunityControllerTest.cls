/**
 * Created by kentheberling on 3/13/17.
 */
@IsTest
private class Peak_JoinCommunityControllerTest {

   @isTest
    public static void testGetUsersWithPublicPhotos() {
        List<User> allUsers = [SELECT Id, UserPreferencesShowProfilePicToGuestUsers FROM User WHERE UserPreferencesShowProfilePicToGuestUsers = TRUE LIMIT 1000];
        // Get User list. This will initially find 0 because there are now no users with public photos. However, the function builds up a list of empty users for us!
        List<User> userList = Peak_JoinCommunityController.getUsersWithPublicPhotos(String.valueOf(allUsers.size() + 1));
       System.assertEquals(true ,userList.size() >= 1);
    }

}