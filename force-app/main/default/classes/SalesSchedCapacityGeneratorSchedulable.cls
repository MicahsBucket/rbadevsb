/**
 * @File Name          : SalesSchedCapacityGeneratorSchedulable.cls
 * @Description        : 
 * @Author             : James Loghry (Demand Chain)
 * @Group              : 
 * @Last Modified By   : James Loghry (Demand Chain)
 * @Last Modified On   : 4/2/2019, 9:24:36 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/2/2019, 9:24:10 AM   James Loghry (Demand Chain)     Initial Version
**/
global class SalesSchedCapacityGeneratorSchedulable implements Schedulable{
    global void execute(SchedulableContext sc){
        Database.executeBatch(new SalesSchedCapacityGeneratorBatch(null,null),200);
    }
}