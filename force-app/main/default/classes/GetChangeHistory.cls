/**
* @description  Getting ChangeHistory Records Related to orderitem
*/ 
public with sharing class GetChangeHistory {
    
    /**
* @description 
* @param orderItemId passed from Edit and Read pages 
*/ 
   @AuraEnabled(cacheable=true)
    public static List<Change_History__c> getChangeHistory(Id orderItemId) {
        return [SELECT  id ,LastModifiedby.Name, LastModifiedDate , Retail_Price__c,Responsible_Party__c,Retail_Price_Change__c,Changes__c from Change_History__c where order_Product__c =:orderItemId];
    }
}