public with sharing class TechMeasureWOController {

    private Id opportunityId {get; set;}
    private Opportunity relatedOpp {get; set;}
    private Order soldOrder {get; set;}
    private Id techMeasureTypeId {get; set;}

    public TechMeasureWOController(ApexPages.StandardController stdController) {
        this.opportunityId = ApexPages.currentPage().getParameters().get('id');
        this.techMeasureTypeId = UtilityMethods.RecordTypeFor('WorkOrder', 'Tech_Measure');
        this.relatedOpp = [ SELECT Id, Name, AccountId, Account.Name, Account.ShippingStreet,
                			Account.ShippingStateCode, Account.ShippingPostalCode, Account.ShippingCity
                            FROM Opportunity
                            WHERE Id = :this.opportunityId ];

        List<Order> orders = [SELECT Id, Name FROM Order WHERE OpportunityId = :this.opportunityId ];
		if (orders.size() > 0)
            this.soldOrder = orders.get(0);
    }

    public PageReference redirect() {
      
    	List<Contact_History__c> contactHistories = [SELECT Id, Contact__c, Dwelling__c, Contact__r.Full_Name__c
                                                        FROM Contact_History__c
                                                        WHERE Dwelling__c = :this.relatedOpp.AccountId
                                                        AND Primary_Contact__c = true];

        WorkOrder WORec = new WorkOrder();
		WORec.Opportunity__c = this.relatedOpp.id;
        WORec.AccountId =this.relatedOpp.Accountid;        
        WORec.RecordTypeid = this.techMeasureTypeId;
        WORec.Duration =2;
        if (this.soldOrder != null) {
            WORec.Sold_Order__c = this.soldOrder.Id;
            }
         if (contactHistories.size() > 0) {
             WORec.contactid = contactHistories.get(0).Contact__r.id ;
         }
        insert WORec;
        PageReference p = new PageReference('/' +WORec.id);
        return p;
    }   
}