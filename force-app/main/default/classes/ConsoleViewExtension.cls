public class ConsoleViewExtension {
    public Boolean showHeader {get; set;}
    public Boolean showSidebar {get; set;}
    
    public ConsoleViewExtension(ApexPages.StandardController controller) {
        Map<String, String> urlParam = ApexPages.currentPage().getParameters();
        /*for(String s : urlParam.keyset()) {
            System.debug(s);
            }
        for(String t : urlParam.values()) {
            system.debug(t);
            }
        */
        showHeader = urlParam.get('sfdcIFrameOrigin') == null ? true : false;
        showSidebar = urlParam.get('sfdcIFrameOrigin') == null ? true : false;
    }

}