@isTest
public class AddLegacyAssetControllerTest {

    @TestSetup
    static void setupData() {

        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Account dwelling = new Account(Type = 'Dwelling',
                                       Name = 'Dwelling 1',
                                       Dwelling_Style__c = 'House',
                                       Year_Built__c = '1997',
                                       Account_Balance__c = 1000000);
        insert dwelling;

        Contact contact = new Contact(FirstName = 'Test',
                                      LastName = 'Contact',
                                      HomePhone = '1111111111',
                                      MobilePhone = '2222222222',
                                      Primary_Contact__c = true,
                                      Primary_Dwelling_for_Contact__c = true,
                                      AccountId = dwelling.Id);
        insert contact;

        Id masterRecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Master Product').getRecordTypeId();
        Product2 product = new Product2(Name = 'Test Product',
                                        Number_Service_Products__c = 10,
                                        RecordTypeId = masterRecordTypeId);
        insert product;

    }

    static testMethod void testGetAccountInfo() {

        Id accountId = [SELECT Id FROM Account WHERE Year_Built__c = '1997'].Id;
        Account testAccount = AddLegacyAssetController.getAccountInfo(accountId);
        System.assertEquals('Dwelling 1', testAccount.Name);

    }

    static testMethod void testGetProducts() {

        List<Product2> products = AddLegacyAssetController.getProducts('Test Product');
        System.assertEquals(1, products.size());

    }

    static testMethod void testSaveAssets() {

        List<Product2> products = AddLegacyAssetController.getProducts('Test Product');
        Id accountId = [SELECT Id FROM Account WHERE Year_Built__c = '1997'].Id;

        List<Asset> assets = new List<Asset>();
        Asset asset = new Asset(Name = products[0].Name,
                                AccountId = accountId,
                                Legacy_Asset__c = true,
                                Product2Id = products[0].Id,
                                Variant_Number__c = '12345');
        assets.add(asset);
        AddLegacyAssetController.saveAssets(assets);

        assets = [SELECT Name, RecordTypeId FROM Asset];
        System.assertEquals(products[0].Name, assets[0].Name);

        assets[0].Name = 'New Name';
        AddLegacyAssetController.saveAssets(assets);
        assets = [SELECT Name FROM Asset];
        System.assertEquals('New Name', assets[0].Name);

    }
    
}