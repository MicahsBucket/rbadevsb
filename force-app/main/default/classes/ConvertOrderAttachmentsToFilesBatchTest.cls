/*
* @author Jason Flippen
* @date 02/28/2021
* @description Test Class for the following Classes:
*              - ConvertOrderAttachmentsToFilesBatch
*/ 
@isTest
public class ConvertOrderAttachmentsToFilesBatchTest {

    /*
    * @author Jason Flippen
    * @date 02/28/2021
    * @description: Method to create data used in the Test methods
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @TestSetup
    static void testSetup() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();
        
        Account testStoreAccount = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        testStoreAccount.Store_Number__c = '1234';
        update testStoreAccount;
        
        Account testVendorAccount = testUtility.createVendorAccount('Test Vendor Account');
        insert testVendorAccount;
        
        Account testDwellingAccount = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAccount.Store_Location__c = testStoreAccount.Id;
        insert testDwellingAccount;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAccount.Id, 'Closed - Won');
        insert testOpportunity;
        
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;
        
        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Legacy_Service').getRecordTypeId(),
                                            Vendor__c = testVendorAccount.Id,
                                            Product_PO__c = true,
                                            Cost_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c =  testFan.Id);
        insert testProduct;
        
        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;
        
        // Parent PricebookEntry
        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order',
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Record_Type').getRecordTypeId(),
                                     AccountId = testDwellingAccount.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAccount.Id,
                                     OpportunityId = testOpportunity.Id,           
                                     Status = 'Draft',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;
        
        OrderItem testOrderItem = new OrderItem(OrderId = testOrder.Id,
//                                                Has_PO__c = true,
//                                                Purchase_Order__c = testPurchaseOrder.Id,
                                                PricebookentryId = testPBEStandard.Id,
                                                Quantity = 2,
                                                Quanity_Ordered__c = 2,
                                                Unit_Wholesale_Cost__c = 50.00,
                                                UnitPrice = 100,
                                                Variant_Number__c = 'ABCD1234');
        insert testOrderItem;
        
        Blob attachmentBody = Blob.valueOf('Test Attachment Body');
        Attachment testAttachment = new Attachment(Name = 'Test Attachment',
                                                   Body = attachmentBody,
                                                   parentId = testOrder.Id);
        insert testAttachment;

    }

    /*
    * @author Jason Flippen
    * @date 02/28/2021
    * @description: Method to test the methods in the Batch Class.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testExecute() {

        Datetime earliestCreatedDateTime = Datetime.newInstance(Date.today().year(), Date.today().month(), 01, 0, 0, 0);

        Set<String> storeNumberSet = new Set<String>{'1234'};

        Id orderId = null;
        String queryString = 'SELECT Id ' +
                             'FROM   Order ' +
                             'WHERE  Store_Number__c IN :storeNumberSet ' +
                             'AND    CreatedDate >= :earliestCreatedDateTime ' +
                             'ORDER BY CreatedDate';

        List<String> resultEmailList = new List<String>{'jane.doe@andersencorp.com'};

        Test.startTest();

            ConvertOrderAttachmentsToFilesBatch convertBatch = new ConvertOrderAttachmentsToFilesBatch(queryString,
                                                                                                       earliestCreatedDateTime,
                                                                                                       storeNumberSet,
                                                                                                       orderId,
                                                                                                       resultEmailList);

            Id batchId = Database.executeBatch(convertBatch);

        Test.stopTest();

    }

}