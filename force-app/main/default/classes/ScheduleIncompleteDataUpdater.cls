global class ScheduleIncompleteDataUpdater implements Schedulable{
	global void execute(SchedulableContext sc) {
		//Variables
		Date todaysDate = Date.today();

		//finds recordIds for post install record types
        Id installRecordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get('Post Install').getRecordTypeId();

        //Creates a list of Surveys with a Send Date of today and have record type of 'Post Install'
       	List<Survey__c> todaysPostInstallSurveys = [SELECT Id, Survey_Auto_Sent__c, Survey_Status__c, Date_Sent__c, Send_Date__c, Primary_Contact_First_Name__c, 
       												Primary_Contact_Last_Name__c, City__c, State__c, Zip__c, Country__c, Street__c 
       												FROM Survey__c 
       												WHERE Send_Date__c = :todaysDate AND RecordTypeId = :installRecordTypeId AND On_Hold__c = FALSE];

       	//Finds required fields that are missing to send survey to homeowner, if any are null it sets the status to incompelete data
       	for(Survey__c installSurvey : todaysPostInstallSurveys){
       		if(installSurvey.Primary_Contact_First_Name__c == null || installSurvey.Primary_Contact_Last_Name__c == null || 
       			installSurvey.City__c == null || installSurvey.Street__c == null || installSurvey.State__c == null ||
       			installSurvey.Zip__c == null || installSurvey.Country__c == null){
   				installSurvey.Survey_Status__c = 'Incomplete Data';
   			} else {
   				installSurvey.Survey_Auto_Sent__c = TRUE;
   				installSurvey.Send_to_Medallia__c = TRUE;
   				installSurvey.On_Hold__c = TRUE;
   			}
       	}
       	
       	Database.update(todaysPostInstallSurveys, false);	
	}
}