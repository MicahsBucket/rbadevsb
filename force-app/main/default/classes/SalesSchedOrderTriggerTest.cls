/**
 * @File Name          : SalesSchedOrderTriggerTest.cls
 * @Description        : 
 * @Author             : James Loghry (Demand Chain)
 * @Group              : 
 * @Last Modified By   : James Loghry (Demand Chain)
 * @Last Modified On   : 5/8/2019, 7:18:48 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/7/2019, 3:28:26 PM   James Loghry (Demand Chain)     Initial Version
**/
@isTest
public class SalesSchedOrderTriggerTest {

    static testmethod void testSaleResulted(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);

        RecordType rt = [Select Id From RecordType Where DeveloperName='Finance_Company'];
        Account finance = new Account(Name='Finance',RecordTypeId=rt.Id);
        insert finance;

        RecordType rt2 = [Select Id From RecordType Where DeveloperName='Cash_Payment' And sObjectType = 'Store_Finance_Program__c'];
        Store_Finance_Program__c sfp = new Store_Finance_Program__c(
			RecordTypeId = rt2.Id,
			Store_Configuration__c = sstu.store.Active_Store_Configuration__c,
			Active__c = true,
			Name = 'TestStoreFinanceProgram',
            Finance_Company__c = finance.Id
		);
        insert sfp;
        
        insert new Store_Finance_Program__share(UserOrGroupId=sstu.reps.get(0).Id,ParentId=sfp.Id,AccessLevel='Read',RowCause=Store_Finance_Program__share.RowCause.Manual);
        
       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
        insert resources;

        Test.startTest();
        System.runAs(sstu.reps.get(0)){
            update new Sales_Order__c(
                Id = sstu.salesOrder.Id
                ,Time_Arrived__c = Datetime.now().time()
                ,Time_Left__c = Datetime.now().time()
                ,Key_Contact_Person__c = 'John Doe'
                ,Result__c = 'Sale'
                ,Payment_Type__c = 'Finance'
                //,Financing_Source__c = 'Source 1'
                ,Account_Number__c = '123'
                ,Approved__c = 1
                ,Deposit_Type__c = 'Cash'
                ,Deposit__c = 1
                ,Construction_Department_Notes__c = 'notes'
                ,Why_Customer_Purchased__c = 'why'
                ,Concerns__c = 'concerns'
                ,Address_Concerns__c = 'address'
                ,Partial_Sale__c = 'Yes'
                ,Historical_Area__c = 'Yes'
                ,Mail_and_Canvass_Neighborhood__c = 'Yes'
                ,Finance_Issues__c = 'No'
                ,Financeable_Details__c = 'Blah'
                ,Store_Finance_Program__c = sfp.Id
                ,Were_All_Owners_Present__c = 'Yes'
            );
        }
        System.assertEquals(false,[Select Resulted__c From Sales_Order__c Where Id = :sstu.salesOrder.Id].Resulted__c);
        //System.assertEquals('Resulted',[Select Status__c From Sales_Appointment_Resource__c Where Id = :resources.get(0).Id].Status__c);

    }

    static testmethod void testSaleNotResulted(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);

       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
        insert resources;

        Test.startTest();
        System.runAs(sstu.reps.get(0)){
            update new Sales_Order__c(
                Id = sstu.salesOrder.Id
                ,Time_Arrived__c = Datetime.now().time()
                ,Time_Left__c = Datetime.now().time()
                //Commented out to prevent a result.
                //,Key_Contact_Person__c = 'John Doe'
                ,Result__c = 'Sale'
                ,Payment_Type__c = 'Finance'
                ,Financing_Source__c = 'Source 1'
                ,Account_Number__c = '123'
                ,Approved__c = 1
                ,Deposit_Type__c = 'Cash'
                ,Deposit__c = 1
                ,Construction_Department_Notes__c = 'notes'
                ,Why_Customer_Purchased__c = 'why'
                ,Concerns__c = 'concerns'
                ,Address_Concerns__c = 'address'
                ,Partial_Sale__c = 'Yes'
                ,Historical_Area__c = 'Yes'
                ,Mail_and_Canvass_Neighborhood__c = 'Yes'
            );
            
            update new Sales_Order__c(
                Id = sstu.salesOrder.Id
                ,Time_Arrived__c = Datetime.now().time()
                ,Time_Left__c = Datetime.now().time()
                //Commented out to prevent a result.
                //,Key_Contact_Person__c = 'John Doe'
                ,Result__c = 'Sale'
                ,Clear_Resultant_Details__c=true
            );
        }
        System.assertEquals(false,[Select Resulted__c From Sales_Order__c Where Id = :sstu.salesOrder.Id].Resulted__c);
        System.assertNotEquals('Resulted',[Select Status__c From Sales_Appointment_Resource__c Where Id = :resources.get(0).Id].Status__c);
    }

    static testmethod void testDemoNoSale(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);

       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
        insert resources;

        Test.startTest();
        System.runAs(sstu.reps.get(0)){
            update new Sales_Order__c(
                Id = sstu.salesOrder.Id
                ,Time_Arrived__c = Datetime.now().time()
                ,Time_Left__c = Datetime.now().time()
                ,Key_Contact_Person__c = 'John Doe'
                ,Result__c = 'Demo No Sale'
                ,Project_Description__c = 'Blah'
                ,Key_Reason_Customer_Didnt_Proceed__c = 'Blah'
                ,Mail_and_Canvass_Neighborhood__c = 'Yes'
                ,Future_Purchase__c = '2'
                ,Rescheduled__c = 'Yes'
                ,Were_All_Owners_Present__c = 'Yes'
                ,Follow_Up_Appt_Date_Time__c = Datetime.now()
            );
        }
        System.assertEquals(false,[Select Resulted__c From Sales_Order__c Where Id = :sstu.salesOrder.Id].Resulted__c);
        //System.assertEquals('Resulted',[Select Status__c From Sales_Appointment_Resource__c Where Id = :resources.get(0).Id].Status__c);

    }
    static testmethod void testNotCovered(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);

       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
        insert resources;

        Test.startTest();
        System.runAs(sstu.reps.get(0)){
            update new Sales_Order__c(
                Id = sstu.salesOrder.Id
                ,Time_Arrived__c = Datetime.now().time()
                ,Time_Left__c = Datetime.now().time()
                ,Key_Contact_Person__c = 'John Doe'
                ,Result__c = 'Not Covered'
                ,Why_Not_Covered__c='Why Not Covered'
            );
        }
        System.assertEquals(true,[Select Resulted__c From Sales_Order__c Where Id = :sstu.salesOrder.Id].Resulted__c);
        System.assertEquals('Resulted',[Select Status__c From Sales_Appointment_Resource__c Where Id = :resources.get(0).Id].Status__c);

    } 

    static testmethod void testSaleNotResultedWithNoFinanceIssues(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);

       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
        insert resources;

        Test.startTest();
        System.runAs(sstu.reps.get(0)){
            update new Sales_Order__c(
                Id = sstu.salesOrder.Id
                ,Time_Arrived__c = Datetime.now().time()
                ,Time_Left__c = Datetime.now().time()
                ,Key_Contact_Person__c = 'John Doe'
                ,Result__c = 'Sale'
                ,Payment_Type__c = 'Finance'
                ,Financing_Source__c = 'Source 1'
                ,Account_Number__c = '123'
                ,Approved__c = 1
                ,Deposit_Type__c = 'Cash'
                ,Deposit__c = 1
                ,Construction_Department_Notes__c = 'notes'
                ,Why_Customer_Purchased__c = 'why'
                ,Concerns__c = 'concerns'
                ,Address_Concerns__c = 'address'
                ,Partial_Sale__c = 'Yes'
                ,Historical_Area__c = 'Yes'
                ,Mail_and_Canvass_Neighborhood__c = 'Yes'
                ,Finance_Issues__c = 'No'
            );
            
            update new Sales_Order__c(
                Id = sstu.salesOrder.Id
                ,Time_Arrived__c = Datetime.now().time()
                ,Time_Left__c = Datetime.now().time()
                ,Key_Contact_Person__c = 'John Doe'
                ,Result__c = 'Not Home'
                ,Payment_Type__c = 'Finance'
            );
        }
        System.assertEquals(false,[Select Resulted__c From Sales_Order__c Where Id = :sstu.salesOrder.Id].Resulted__c);
        System.assertNotEquals('Resulted',[Select Status__c From Sales_Appointment_Resource__c Where Id = :resources.get(0).Id].Status__c);
    }
}