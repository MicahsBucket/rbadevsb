/**
 * @File Name          : EditOrderPageLwcController.cls
 * @Description        : 
 * @Author             : Mark.Rothermal@AndersenCorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 11-13-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    3/8/2019, 10:21:16 AM   Mark.Rothermal@AndersenCorp.com     Initial Version
**/

public with sharing class EditOrderPageLwcController {


    /**
     * @description - return a product config, based on a product id chosen on the edit orderproduct page
     * @param productId 
     * @return 
     * TODO - update to be a try catch and to return a lighting response.
     */
    @AuraEnabled(cacheable = true)
    public static Product_Configuration__c getProductConfiguration(id productId) {
        return[SELECT
        Astragal__c,
        Brickmould_Location__c,
        Casing_Location__c,
        Casing_Profile__c,
        Casing_Species__c,
        Checkrail_Style__c,
        Door_Sill_Color__c,
        Drip_Cap_Pieces__c,
        Dwelling_Information__c,
        EJ_Color__c,
        EJ_Species__c,
        EJ_Thickness__c,
        EJ_Wall_Depth__c,
        Exterior_Color__c,
        Exterior_Frame_Depth__c,
        Exterior_Grille_Color__c,
        Exterior_Trim_Color__c,
        Exterior_Trim__c,
        Finger_Lifts__c,
        Frame_Type__c,
        Glass_Pattern__c,
        Glazing__c,
        Grille_Patterns__c,
        Grille_Style__c,
        Hardware_Color__c,
        Hardware_Finish_Color__c,
        Hardware_Finish__c,
        Hardware_Option__c,
        Hardware_Style__c,
        Hubs__c,
        Id,
        Install_Track_Location__c,
        Interior_Color__c,
        Interior_Grille_Color__c,
        Interior_Pre_Finish__c,
        Interior_Sash_Color__c,
        Interior_Trim__c,
        Jamb_Liner_Color__c,
        Lifts_Pulls__c,
        Lites_High__c,
        Lites_Wide__c,
        Locks_Sash__c,
        Measurement_Fractions__c,
        Mull_Sequence_Mullion__c,
        Mullion_Size__c,
        Name,
        Number_of_Sashes__c,
        Performance_Category__c,
        Product__c,
        RecordTypeId,
        Sash_Operation__c,
        Sash_Ratio__c,
        Screen_Color__c,
        Screen_Color_Default__c, 
        Screen_Size__c,
        Screen_Type__c,
        Screen_Veneer_Type__c,
        Sill_Angle__c,
        Specialty_Shape__c,
        Spokes__c,
        Storm_Window_Color__c,
        Storm_Window_Style__c,
        Storm_Window__c,
        (SELECT
         EJ_Color__c,
         EJ_Species__c,
         Id,
         Name
         FROM EJ_Color_Configurations__r),
        (SELECT
         Id,
         Name,
         OwnerId,
         Product_Configuration__c,
         Sash_Operation__c,
         Sash_Ratio__c
         FROM Sash_Configurations__r),
        (SELECT
         Grille_Pattern__c,
         Grille_Style__c,
         Hubs__c,
         Id,
         Lites_High__c,
         Lites_Wide__c,
         Name,
         Specialty_Shape__c,
         Spokes__c
         FROM Specialty_Grille_Configurations__r),
        (SELECT
         Field_Control_ID__c,
         Id,
         Name,
         Required__c
         FROM Product_Field_Controls__r),
        (SELECT
         Exterior_Trim__c,
         Frame_Type__c,
         Id,
         Name,
         Pocket_Notch__c
         FROM Frame_Notch_Configurations__r),
        (SELECT
         Id,
         Interior_Color__c,
         Maximum_Height_Fractions__c,
         Maximum_Height__c,
         Maximum_Width_Fractions__c,
         Maximum_Width__c,
         Name
         FROM Color_Size_Limits__r),
        (SELECT
         Id,
         Name,
         Screen_Type__c
         FROM Screen_Configurations__r),
        (SELECT
         Extended_Max_Height_Fraction__c,
         Extended_Max_Height_Inches__c,
         Extended_Max_Width_Fraction__c,
         Extended_Max_Width_Inches__c,
         Frame_Type__c,
         Glass_Square_Foot_Max__c,
         Hardware_Options__c,
         Height_Message_Threshold__c,
         Height_Message__c,
         Id,
         Lock_Max_Height_Fraction__c,
         Lock_Max_Height_Inches__c,
         Lock_Max_Height_Locks__c,
         Lock_Max_Width_Fraction__c,
         Lock_Max_Width_Inches__c,
         Lock_Max_Width_Locks__c,
         Lock_Min_Height_Fraction__c,
         Lock_Min_Height_Inches__c,
         Lock_Min_Height_Locks__c,
         Lock_Min_Width_Fraction__c,
         Lock_Min_Width_Inches__c,
         Lock_Min_Width_Locks__c,
         Max_Height_Fraction__c,
         Max_Height_Inches__c,
         Max_Leg_Height_Fraction__c,
         Max_Leg_Height_Inches__c,
         Max_Width_Fraction__c,
         Max_Width_Inches__c,
         Min_Height_Fraction__c,
         Min_Height_Inches__c,
         Min_Leg_Height_Fraction__c,
         Min_Leg_Height_Inches__c,
         Min_Width_Fraction__c,
         Min_Width_Inches__c,
         Name,
         Negative_Force__c,
         Peak_Height_Max__c,
         Peak_Height_Min__c,
         Performance_Category__c,
         Positive_Force__c,
         Sash_Operation__c,
         Sash_Ratio__c,
         Single_Leg__c,
         Specialty_Shape__c,
         United_Inch_Maximum__c,
         United_Inch_Minimum__c,
         Width_Message_Threshold__c,
         Width_Message__c,
         Width_to_Height_Maximum__c,
         Width_to_Height_Minimum__c
         FROM Size_Detail_Configurations__r),
        (SELECT
         Exterior_Color__c,
         Id,
         Interior_Color__c,
         Name
         FROM Color_Configurations__r),
        (SELECT
         Default_Lites_High__c,
         Default_Lites_Wide__c,
         S2_Default_Lites_High__c,
         S2_Default_Lites_Wide__c,
         Grille_Pattern__c,
         Grille_Style__c,
         Id,
         Name,
         Sash_Ratio__c
         FROM Grille_Pattern_Configurations__r),
        (SELECT
         Glass_Pattern__c,
         Glazing__c,
         Id,
         Name,
         Sash_Ratio__c,
         Tempered__c
         FROM Glazing_Configurations__r),
          ( SELECT id,
            Glazing_Type__c,
            Glazing__c,
            Product_Configuration__r.Product__c
            from Glazing_Type_Pattern_Configurations__r)
        FROM Product_Configuration__c
        WHERE Product__c = : productId];
    }
    
    /**
     * @description - return a pricebook entry id,
     * based on a product id chosen on the edit orderproduct page and the order id that the order item relates to.
     * @param productId , orderId
     * @return  
     */
    public static Id getPricebookEntryId(id productId,id orderId) {
        Id pbeId;
        Id pb2Id;
    //  Id storeConfigId;
    //  Order placeholder;  
             if(Test.isRunningTest()){
                pb2Id = Test.getStandardPricebookId();
            } else{
                pb2ID = [SELECT Opportunity.Pricebook2Id FROM Order WHERE Id = :orderId limit 1].Opportunity.Pricebook2Id;
                system.debug('pricebook2 id '+pb2ID);
                system.debug('order ID ' + orderId);
                system.debug('productId ' + productId);
            //  storeConfigId = placeholder.Store_Location__r.Active_Store_Configuration__c;
            //  pb2ID = [SELECT id from PRICEBOOK2 where Store_Configuration__c = :storeConfigId and (NOT Name LIKE '%Legacy%') limit 1].id; 
            }
        try {
         pbeId = [SELECT Id FROM PricebookEntry WHERE Product2Id = : productId AND Pricebook2Id = :pb2Id limit 1].Id;
        }
        catch(exception ex){
            system.debug('error in get pricebook entry id ' + ex.getMessage() );
        }
        system.debug('pbeId in pricebook entry ' + pbeId);
        return pbeId;
    }
                        

    /**
     * @description - insert or update an orderitem and return a success/error message. DOES NOT PERFORM makability checks.
     * @param OrderItem - form - the values from the create/edit form. 
     * @returns lightning response - either a success message or an error. 
     */
    @AuraEnabled
    public static LightningResponse saveOrderItem(OrderItem form) {
        system.debug(form);
        system.debug('Product2Id'+ form.Product2Id);
        system.debug('OrderId'+ form.OrderId);
        Boolean isUpdate = true;
        LightningResponse lr = new LightningResponse();
        saveResponse sr = new saveResponse();
        Savepoint sp = Database.setSavepoint();
        // check if Id - determines if edit or create new. cannot set pbeId if editing an order item
        if(String.isBlank(form.Id)){
        Id pbeId = getPricebookEntryId(form.Product2Id, form.OrderId);
            form.PricebookEntryId = pbeId; // twin cities casement test id '01u610000074OjQAAU'; standard pricebook casement01u610000074OjMAAU
            isUpdate = false;
        }
        try{
            // todo add if statement. if makability fails. rollback db. return makability errors 
            system.debug('form with pbeId '+form);
            system.debug('form  pbeId '+form.PricebookEntryId);
            if(isUpdate){
                Database.update(form);
                sr.message = 'Upsert Successful';
            } else{
                Database.insert(form);
                sr.message = 'Insert Successful';
                sr.recordId = form.Id;
            }
            lr.jsonResponse = JSON.serialize(sr);
            system.debug('makability response in save order item '+lr);
        }catch(exception ex) {
            lr = new LightningResponse(ex);
            Database.rollback(sp);
            system.debug('lightning response in catch block for save edit order page controller' + lr);
            system.debug('Exception get cause' + ex.getCause());
            system.debug('Exception line number' + ex.getLineNumber());
            system.debug('Exception stack trace' + ex.getStackTraceString());
            system.debug('Exception message' + ex.getMessage());

        }
        system.debug('lightning response in saveorderitem '+ lr);
        return lr;
    }

    // /**
    //  * @description - insert or update an orderitem and return a success/error message. PERFORMS makability checks.
    //  * @param OrderItem - form - the values from the create/edit form. 
    //  * @returns lightning response - either a success message or an error. 
    //  */
    // @AuraEnabled
    // public static LightningResponse saveOrderItemWithMakability(OrderItem form) {
    //  system.debug(form);
    //  LightningResponse lr = new LightningResponse();
    //  Savepoint sp = Database.setSavepoint();
    //  Boolean isUpdate = true;
    //  Boolean passedMakability = true;
    //  // check if Id - determines if edit or create new. cannot set pbeId if editing an order item
    //  if(String.isBlank(form.Id)){
    //  Id pbeId = getPricebookEntryId(form.Product2Id, form.OrderId);
    //      form.PricebookEntryId = pbeId; // twin cities casement test id '01u610000074OjQAAU'; standard pricebook casement01u610000074OjMAAU
    //      isUpdate = false;
    //  }
    //  try{
    //      system.debug('form with pbeId '+form);
    //      if(isUpdate){
    //          Database.update(form);
    //          sr.message = 'Upsert Successful';
    //      } else{
    //          Database.insert(form);
    //          sr.message = 'Insert Successful';
    //      }
    //      list<MakabilityRestResource.MakabilityResult> mr = performMakabilityCheck(form);
    //      lr.jsonResponse = JSON.serialize(mr);
    //      for(MakabilityRestResource.MakabilityResult m:mr){
    //          if(m.isMakable == false){
    //              passedMakability = false;
    //          }
    //      }
    //      if(!passedMakability){
    //          Database.rollback(sp);
    //      }
    //  }catch(exception ex) {
    //      lr = new LightningResponse(ex);
    //      Database.rollback(sp);
    //      system.debug('lightning response in catch block for save edit order page controller' + lr);
    //      system.debug('Exception get cause' + ex.getCause());
    //      system.debug('Exception line number' + ex.getLineNumber());
    //      system.debug('Exception stack trace' + ex.getStackTraceString());
    //      system.debug('Exception message' + ex.getMessage());

    //  }
    //  system.debug('lightning response in saveorderitem With Makability '+ lr);
    //  return lr;
    // }

    /**
     * @description - return a product config, based on a product id chosen on the edit orderproduct page
     * @param productId 
     * @return 
     */
    @AuraEnabled(cacheable = true)
    public static LightningResponse getProductFieldcontrols(id productId){
        LightningResponse lr = new LightningResponse();
        List<Product_Field_Control__c> pfc = new List<Product_Field_Control__c>();
            try{ 
                pfc = [SELECT Field_Control_ID__c, 
                Field_Control_ID__r.Target_Api_Name__c, 
                Field_Control_ID__r.Target_Object_Name__c, 
                Field_Control_ID__r.Name, 
                Id, 
                Product_Configuration_ID__c,
                Product_Configuration_ID__r.Name, 
                Required__c
                FROM Product_Field_Control__c 
                WHERE Field_Control_ID__r.Target_Object_Name__c = 'OrderItem' 
                AND (Product_Configuration_ID__r.Product__c = :productId OR Product_Configuration_ID__c = null )];
                lr.jsonResponse = JSON.serialize(pfc);
            }catch(Exception ex){
                system.debug('error in get product field controls '+ex.getMessage());
                lr = new LightningResponse(ex);
            }
            System.debug('list of pfcs  '+ lr.jsonResponse);
            return lr;
    }

    /**
     * @description - return a product config, based on a product id chosen on the edit orderproduct page
     * @param productId 
     * @return 
     */
    @AuraEnabled(cacheable = true)
    public static LightningResponse getProductFieldControlDependency(id productId){
        LightningResponse lr = new LightningResponse();
        List<Product_Field_Control_Dependency__c> pfcd = new List<Product_Field_Control_Dependency__c>();
        try{
             pfcd = [SELECT
                    Action_Taken__c,
                    Controlling_Field__c,
                    Controlling_Field__r.Field_Control_ID__r.Target_Api_Name__c,
                    Controlling_Field__r.Field_Control_ID__r.Target_Object_Name__c,
                    Controlling_Field__r.Field_Control_ID__r.Name,
                    Controlling_Value__c,
                    Dependent_Field__c,
                    Dependent_Field__r.Field_Control_ID__r.Name,
                    Dependent_Field__r.Field_Control_ID__r.Target_Api_Name__c,
                    Dependent_Field__r.Field_Control_ID__r.Target_Object_Name__c,
                    Id,
                    Make_Required__c,
                    Name
                    FROM Product_Field_Control_Dependency__c
                    WHERE (Controlling_Field__r.Product_Configuration_ID__r.Product__c =:productId OR Controlling_Field__r.Product_Configuration_ID__r.Product__c = null)];
        lr.jsonResponse = JSON.serialize(pfcd);
        }catch (Exception ex){
        lr = new LightningResponse(ex);     
        }
        return lr;

    }

    /**
     * @description - return an orderITem, based on an orderItem Id passed in through url parameters
     * @param orderItemId 
     * @return 
     */
     //TODO - add positive and negative force fields to this query.
    @AuraEnabled(cacheable = true)
    public static LightningResponse getOrderItemForEdit(id orderItemId){
        LightningResponse lr = new LightningResponse();
        OrderItem oi = new OrderItem();
        try{
             oi = [SELECT
                    Applied_Grilles__c,
                    Astragal__c,
                    Breather_Tubes__c,
                    Brickmould_Applied__c,
                    Brickmould_Location__c,
                    Casing_Location__c,
                    Casing_Profile__c,
                    Casing_Species__c,
                    Checkrail_Style__c,
                    Corrosion_Resistant_Hardware__c,
                    Corrosion_Resistant_Hinges__c,
                    Corrosion_Resistant_Lock__c,
                    Custom_Grille_Details_S1__c,
                    Custom_Grille_Details_S2__c,
                    Custom_Grille_Details_S3__c,
                    Description,
                    Drip_Cap_Pieces__c,
                    Drip_Cap__c,
                    EJ_Color__c,
                    EJ_Frame__c,
                    EJ_Species__c,
                    EJ_Thickness__c,
                    EJ_Wall_Depth__c,
                    Entered_Price__c,
                    Extended_Description__c,
                    Extended_Price__c,
                    Exterior_Color__c,
                    Exterior_Grille_Color__c,
                    Exterior_Trim_Color__c,
                    Exterior_Trim__c,
                    Frame_Type__c,
                    Glass_Pattern_S1__c,
                    Glass_Pattern_S2__c,
                    Glass_Pattern_S3__c,
                    Glazing_S1__c,
                    Glazing_S2__c,
                    Glazing_S3__c,
                    Grille_Alignment__c,
                    Grille_Pattern_S1__c,
                    Grille_Pattern_S2__c,
                    Grille_Pattern_S3__c,
                    Grille_Pattern__c,
                    Grille_Style__c,
                    Hardware_Color__c,
                    Hardware_Finish_Color__c,
                    Hardware_Finish__c,
                    Hardware_Option__c,
                    Hardware_Style__c,
                    Height_Fraction__c,
                    Height_Inches__c,
                    Hubs__c,
                    Id,
                    Install_Holes_Location__c,
                    Install_Track_Location__c,
                    Interior_Color__c,
                    Interior_Grille_Color__c,
                    Interior_Sash_Color__c,
                    Interior_Trim__c,
                    Jamb_Liner_Color__c,
                    Left_Leg_Fraction__c,
                    Left_Leg_Inches__c,
                    Lifts_Pulls__c,
                    Lites_High_S1__c,
                    Lites_High_S2__c,
                    Lites_High_S3__c,
                    Lites_Wide_S1__c,
                    Lites_Wide_S2__c,
                    Lites_Wide_S3__c,
                    Locks_Sash__c,
                    Mfg_Mull_Sequence__c,
                    Mull_Sequence_Mullion__c,
                    Negative_Force__c,
                    NSPR_Number__c,
                    NSPR__c,
                    Notes__c,
                    OrderId,
                    PG_50__c,
                    Performance_Category__c,
                    Part_of_Bay__c,
                    Part_of_Bow__c,
                    Pocket_Notch__c,
                    Positive_Force__c,
                    Product2Id,
                    Product2.Name,
                    Product2.ProductCode,
                    Purchase_Order__c,
                    Quantity,
                    Responsible_Party__c,
                    Right_Leg_Fraction__c,
                    Right_Leg_Inches__c,
                    Room__c,
                    Sales_Height_Inches__c,
                    Sales_Width_Inches__c,
                    Sash_Operation__c,
                    Sash_Ratio__c,
                    Screen_Color__c,
                    Screen_Size__c,
                    Screen_Type__c,
                    Sill_Angle__c,
                    Special_Options__c,
                    Specialty_Shape__c,
                    Spokes__c,
                    Status__c,
                    Tax_Credit_Amount__c,
                    Tax_Credit_Qualified__c,
                    Tax__c,
                    Tempered_S1__c,
                    Tempered_S2__c,
                    Tempered_S3__c,
                    Total_Retail_Price__c,
                    Total_Wholesale_Cost__c,
                    Type__c,
                    UnitPrice,
                    Unit_Id__c,
                    Unit_of_Measure__c,
                    Variant_Number__c,
                    Verify_Item_Configuration__c,
                    Wide_Bar__c,
                    Width_Fraction__c,
                    Width_Inches__c,
                    Purchase_Order__r.Released_Timestamp__c,
                    Retail_Purchase_Order__r.Released_Timestamp__c,
                    Mid_Point_Height_Inches__c, 
                    Mid_Point_Height_Fractions__c 
                    FROM OrderItem
                    WHERE id =:orderItemId limit 1];
        lr.jsonResponse = JSON.serialize(oi);
        }catch (Exception ex){
        lr = new LightningResponse(ex);     
        }
        return lr;
      
    }


    @AuraEnabled
    Public Static LightningResponse performMakabilityCheck(OrderItem form){
        system.debug('form in check makability ' + form);
        LightningResponse lr = new LightningResponse();
        /////// set up for calling makability ////////
        List<Product_Field_Control__c> pfcList = [SELECT Field_Control_ID__c, 
                                                    Field_Control_ID__r.Target_Api_Name__c, 
                                                    Id 
                                                    FROM Product_Field_Control__c 
                                                    WHERE Field_Control_ID__r.Target_Object_Name__c = 'OrderItem' 
                                                    AND (Product_Configuration_ID__r.Product__c = :form.Product2Id OR Product_Configuration_ID__c = null )];
        Set<String> pfcSet = new Set<String>();                                                 
        Set<id> productIds = new set<id>();
        string uniKey;
        Map<string,MakabilityRestResource.OrderItem> uniKeyToOrderItemMap = new map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderRequest ord = new MakabilityRestResource.OrderRequest();
        List<MakabilityRestResource.OrderRequest> ords = new list<MakabilityRestResource.OrderRequest>();
        ProductConfiguration pc = new ProductConfiguration();
        List<MakabilityRestResource.OrderItem> mOis = new List<MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem mOi = new MakabilityRestResource.OrderItem();
        List<MakabilityRestResource.MakabilityResult> allMakability = new List<MakabilityRestResource.MakabilityResult> ();
        List<ApexClass> mclasses = MakabilityUtility.fetchMakabilityClasses();

        // build callout

        productIds.add(form.Product2Id);
        // build pfc map
        for(Product_Field_Control__c pfc : pfcList){
            pfcSet.add(pfc.Field_Control_ID__r.Target_Api_Name__c);
        }
        System.debug('list of pfcs in check makability '+pfcSet);       

        /* optional fields*/
        if(form.Locks_Sash__c != null){
            pc.locks = decimal.valueOf(form.Locks_Sash__c);
        }
        if(form.EJ_Color__c != null) {
            pc.ejColor = form.EJ_Color__c;
        }       
        if(form.EJ_Species__c != null) {
            pc.ejSpecies = form.EJ_Species__c;
        }           
        if(form.Right_Leg_Inches__c != null) {
            pc.rightLegInches = form.Right_Leg_Inches__c;
        }       
        if(form.Right_Leg_Fraction__c != null) {
            pc.rightLegFraction = form.Right_Leg_Fraction__c;
        }       
        if(form.Left_Leg_Inches__c != null) {
            pc.leftLegInches = form.Left_Leg_Inches__c;
        }       
        if(form.Left_Leg_Fraction__c != null) {
            pc.leftLegFraction = form.Left_Leg_Fraction__c;
        }       
        if(form.Pocket_Notch__c != null) {
            pc.insertFrame = form.Pocket_Notch__c;
        }       
        if(form.Exterior_Trim__c != null) {
            pc.exteriorTrim = form.Exterior_Trim__c;
        }       
        if(form.Grille_Style__c != null) {
            pc.grilleStyle = form.Grille_Style__c;
        }        
        if(form.Lites_High_S1__c != null) {
            pc.litesHigh = form.Lites_High_S1__c;
        }    
        if(form.Lites_Wide_S1__c != null) {
            pc.litesWide = form.Lites_Wide_S1__c;
        }    
        if(form.Spokes__c != null && form.Spokes__c.isNumeric()) {
            pc.spokes = Decimal.valueOf(form.Spokes__c);
        }    
        if(form.Hubs__c != null && form.Hubs__c.isNumeric()) {
            pc.hubs = Decimal.valueOf(form.Hubs__c);
        }    
        if(form.Frame_Type__c != null) {
            pc.frame = form.Frame_Type__c;
        }
        if(form.Hardware_Option__c != null) {
            pc.hardwareOption = form.Hardware_Option__c;
        }
        if(form.Sash_Operation__c != null) {
            pc.sashOperation = form.Sash_Operation__c;
        }
        if(form.Sash_Ratio__c != null) {
            pc.sashRatio = form.Sash_Ratio__c;
        }
        if(form.Specialty_Shape__c != null) {
            pc.specialtyShape = form.Specialty_Shape__c;
        }
        if(form.Grille_Pattern__c != null) {
            pc.grillePattern = form.Grille_Pattern__c;
        }
        if(form.Tempered_S1__c) {
            pc.s1Tempering = form.Tempered_S1__c;
        } else {
            if(pfcSet.contains('Tempered_S1__c')){
                pc.s1Tempering = false;
            }
        }
        if(form.Tempered_S2__c) {
            pc.s2Tempering = form.Tempered_S2__c;
        } else{
            if(pfcSet.contains('Tempered_S2__c')){
                pc.s2Tempering = false;
            }        }
        if(form.Tempered_S3__c) {
            pc.s3Tempering = form.Tempered_S3__c;
        } else{
            if(pfcSet.contains('Tempered_S3__c')){
                pc.s3Tempering = false;
            }        }
        if(form.Glass_Pattern_S1__c != null) {
            pc.s1Pattern = form.Glass_Pattern_S1__c;
        }
        if(form.Glass_Pattern_S2__c != null) {
            pc.s2Pattern = form.Glass_Pattern_S2__c;
        }
        if(form.Glass_Pattern_S3__c != null) {
            pc.s3Pattern = form.Glass_Pattern_S3__c;
        }
         if(form.Glazing_S1__c!=null){
          pc.s1Type=form.Glazing_S1__c;
        }
        if(form.Glazing_S2__c!=null){
         pc.s2Type=form.Glazing_S2__c;  
        }
        if(form.Glazing_S3__c!=null){
         pc.s3Type=form.Glazing_S3__c;  
        }
        if(form.Exterior_Color__c != null) {
            pc.exteriorColor = form.Exterior_Color__c;
        }
        if(form.Interior_Color__c != null) {
            pc.interiorColor = form.Interior_Color__c;
        }
        if(form.Screen_Type__c != null) {
            pc.screenType = form.Screen_Type__c;
        }
        if(form.Screen_Size__c != null) {
            pc.screenSize = form.Screen_Size__c;
        }
        // mandatory fields
        // high performance doesnt exist on order product yet..
        // but needs to be set for makability. update to set from oi when field is created.
        // will also need to add external force and internal force fields.
        //pc.highPerformance = false;
        if(form.PG_50__c != null) {
            pc.highPerformance = form.PG_50__c;
        }
        if(form.Positive_Force__c != null) {
            pc.externalForce = form.Positive_Force__c;
        }
        if(form.Negative_Force__c != null) {
            pc.internalForce = form.Negative_Force__c;
        }
        ////////////////////////////////////
        if(form.Performance_Category__c!= null) {
            pc.performanceCategory = form.Performance_Category__c;
        }
        /////////////////////////////
        if(form.Width_Inches__c != null) {
            pc.widthInches = form.Width_Inches__c;
        }
        if(form.Width_Fraction__c != null) {
            pc.widthFractions = form.Width_Fraction__c;
        }
        if(form.Height_Inches__c != null) {
            pc.heightInches = form.Height_Inches__c;
        }
        if(form.Height_Fraction__c != null) {
            pc.heightFractions = form.Height_Fraction__c;
        }
        if(form.Product2Id != null) {
            pc.productId = form.Product2Id;
        }

        ////////////////////////////
        mOi.orderId = form.OrderId;
        mOi.orderItemId= form.Id;
        // ** for makability, orderItemId can be represented by any string, 
        // it is used to sort multiple items per order through a bulk makability check
        // in this instance - single item and single order, if no Id, any string should do.
        if(mOi.orderItemId == null || mOi.orderItemId == ''){
            mOi.orderItemId = 'New OrderItem';
        }
        mOi.productConfiguration = pc;
        mOi.makabilityCalculator = MakabilityUtility.validateFields(pc);
        mOis.add(mOi);
        ord.orderId = form.OrderId;
        ord.orderItems = mOis;
        ords.add(ord);
        uniKeyToOrderItemMap.put(mOi.orderId + mOi.orderItemId, mOi);


        /// run checks ///
        for(ApexClass m:mclasses) {
            try{
                Type t = Type.forName(m.Name);
                MakabilityService ms = (MakabilityService)t.newInstance();
                system.debug('makability class : ' + ms + m.Name);
                List<MakabilityRestResource.MakabilityResult> result = ms.checkCompatibility(uniKeyToOrderItemMap, productIds);
                allMakability.addAll(result);
            } catch (exception e) {
                system.debug('lightning response in catch block for makabilityOrderItem' + e.getMessage() + e.getStackTraceString());

            }
        }
        /// end checks ///
        system.debug('response in makabilityOrderItem '+allMakability);
        lr.jsonResponse = JSON.serialize(allMakability);
        return lr;
        
    }

    /*

    SELECT Family,IsActive,Master_Product__c,Name,RecordTypeId 
    FROM Product2 
    WHERE IsActive = True 
    AND (Family = 'Window' OR Family = 'Specialty' )
    AND RecordTypeId = '01261000000SG0z' 
    AND Master_Product__c = null 
    AND (NOT Name LIKE '%(%')
    AND (NOT name like '%:%')
    returns the 13 new products records atm 

    */
    public class saveResponse {
     string message {get;set;}  
     string recordId {get;set;}      
    }

}