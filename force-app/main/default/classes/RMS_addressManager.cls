public with sharing class RMS_addressManager {
    
    //When Account Address Changes, update Address on all child Records
  //When Account Address Changes, update Address on all child Contacts
    /*public void updaterealtedrecordsAddresses(List<Account> listOld, List<Account> listNew, Map<Id, Account> mapOld, Map<Id, Account> mapNew){        
        List<Id> account = new List<Id>();
        Id dwellingRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dwelling').getRecordTypeId();
        for(Account acc : listNew){
         if(mapOld.get(acc.Id).RecordTypeId == dwellingRecordTypeId){
         if((mapOld.get(acc.Id).ShippingCountry != mapNew.get(acc.Id).ShippingCountry ||
             mapOld.get(acc.Id).ShippingStreet != mapNew.get(acc.Id).ShippingStreet || 
              mapOld.get(acc.Id).ShippingCity != mapNew.get(acc.Id).ShippingCity || 
              mapOld.get(acc.Id).ShippingState != mapNew.get(acc.Id).ShippingState ||
              mapOld.get(acc.Id).ShippingPostalCode != mapNew.get(acc.Id).ShippingPostalCode)){
                account.add(acc.Id);
              }
           }
      }
        
        // If no account's address has changed, just return
        if (account.size() == 0) return;
        
        List<Contact> childContacts = [Select Id, AccountId, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode, OtherStreet, OtherCity, OtherCountry, OtherPostalCode, OtherState FROM Contact WHERE AccountId IN: account];
        List<Order> relatedorders = [Select Id, AccountId, ShippingStreet, ShippingCity, ShippingState, ShippingCountry, ShippingPostalCode, BillingStreet, BillingCity, BillingCountry, BillingPostalCode, BillingState FROM Order WHERE AccountId IN: account];
        List<WorkOrder> relatedworkorders = [Select Id,AccountId, Status, Country,City, PostalCode, State, Street FROM WorkOrder WHERE AccountId IN: account];
        Map<Id,Contact> contactMap = new Map<Id,Contact>([SELECT Id from Contact WHERE Id in:childContacts]);
        Map<Id, Order> orderMap = new Map<Id, Order>([Select Id from Order Where Id in:relatedorders]);
        Map<Id, WorkOrder> woMap = new Map<Id, WorkOrder>([Select Id from WorkOrder where Id in:relatedworkorders]);
       
       for(Account a: listNew){
            for(Contact child :childContacts){
                if(!contactMap.IsEmpty()){
                    if(mapNew.get(child.AccountId).ShippingStreet != mapOld.get(child.AccountId).ShippingStreet ){
                        child.MailingStreet = a.ShippingStreet;
                        child.MailingCity = a.ShippingCity;
                        child.MailingState = a.ShippingState;
                        child.MailingPostalCode = a.ShippingPostalCode;
                        child.MailingCountry = a.ShippingCountry;
                        child.OtherStreet = a.ShippingStreet;
                        child.OtherCity = a.ShippingCity;
                        child.OtherState = a.ShippingState;
                        child.OtherPostalCode = a.ShippingPostalCode;
                        child.OtherCountry = a.ShippingCountry;
                        }
                   }
            }
              for(Order ord :relatedorders){
              if(!orderMap.IsEmpty()){
              if(mapNew.get(ord.AccountId).ShippingStreet != mapOld.get(ord.AccountId).ShippingStreet){
                       ord.ShippingCountry = a.ShippingCountry;
                       ord.ShippingStreet = a.ShippingStreet;
                       ord.ShippingCity = a.ShippingCity;
                       ord.ShippingState = a.ShippingState;
                       ord.ShippingPostalCode = a.ShippingPostalCode;
                       ord.BillingCountry = a.BillingCountry;
                       ord.BillingStreet = a.BillingStreet;
                       ord.BillingCity = a.BillingCity;
                       ord.BillingState = a.BillingState;
                       ord.BillingPostalCode = a.BillingPostalCode;
                       }
                 }
            }
            
            for(WorkOrder wo :relatedworkorders){
            if(!woMap.IsEmpty()){
            if(mapNew.get(wo.AccountId).ShippingStreet != mapOld.get(wo.AccountId).ShippingStreet){
            If(!(wo.Status == 'Canceled' || wo.Status == 'Appt Complete / Closed')){
             system.debug('@@@'+wo.Status);
                 wo.Country = a.ShippingCountry;
                 wo.Street = a.ShippingStreet;
                 wo.City  = a.ShippingCity;
                 wo.State = a.ShippingState;
                 wo.PostalCode = a.ShippingPostalCode;
                            }
                      }
                  }
            }
        } 
        
        if(childContacts.size() > 0){
            update childContacts;
        }
        
        if(relatedorders.size() > 0){
            update relatedorders;
        }
        
        if(relatedworkorders.size() > 0){
            update relatedworkorders;
        }
        
                
    }
    
  //Update Address on Contact if the Account has changed on the Contact
    public void populateAccountAddressNewAccount(List<Contact> listOld, List<Contact> listNew, Map<Id, Contact> mapOld, Map<Id, Contact> mapNew){
        
        List<Id> account = new List<Id>(); 
        
        for(Contact c : listNew){
            if(mapNew.get(c.Id).AccountId != mapOld.get(c.Id).AccountId){            
                If(c.AccountId != null){
                    account.add(c.AccountId);   
                }   
            }
        }
        List <Account> accountList = [Select ID, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry from Account where Id in :account];
        
        for (Integer i = 0; i <listNew.size(); i++){
            if (accountList.size() > 0 && listNew[i].AccountId !=null){
                for (Account a:accountList){
                    if (listNew[i].AccountId != listOld[i].AccountId){
                        listNew[i].MailingStreet = a.ShippingStreet;
                        listNew[i].MailingCity = a.ShippingCity;
                        listNew[i].MailingState = a.ShippingState;
                        listNew[i].MailingPostalCode = a.ShippingPostalCode;
                        listNew[i].MailingCountry = a.ShippingCountry;
                        listNew[i].OtherStreet = a.BillingStreet;
                        listNew[i].OtherCity = a.BillingCity;
                        listNew[i].OtherState = a.BillingState;
                        listNew[i].OtherPostalCode = a.BillingPostalCode;
                        listNew[i].OtherCountry = a.BillingCountry;
                    }
                }
            }
            
        }        
    }
    
    
    //Set the Account address on the newly created Contact
    public void populateAccountAddressNewContact(List<Contact> listOld, List<Contact> listNew, Map<Id, Contact> mapOld, Map<Id, Contact> mapNew){
        
        List<Id> account = new List<Id>();
        
        for(Contact c : listNew){            
            If(c.AccountId != null){
                account.add(c.AccountId);   
            }            
        }
        
        List <Account> accountList = [SELECT ID, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry from Account where Id in :account];
        
        for(Integer i=0; i<listNew.size(); i++){
            if(accountList.size() > 0){
                for (Account a:accountList){                    
                    listNew[i].MailingStreet = a.ShippingStreet;
                    listNew[i].MailingCity = a.ShippingCity;
                    listNew[i].MailingState = a.ShippingState;
                    listNew[i].MailingPostalCode = a.ShippingPostalCode;
                    listNew[i].MailingCountry = a.ShippingCountry;
                    listNew[i].OtherStreet = a.BillingStreet;
                    listNew[i].OtherCity = a.BillingCity;
                    listNew[i].OtherState = a.BillingState;
                    listNew[i].OtherPostalCode = a.BillingPostalCode;
                    listNew[i].OtherCountry = a.BillingCountry;                    
                }
                
            }
        }
    }    
    
     // @1231: Update address on the Service Appointment, when address change is made on the Account : 11/15/2018
    
  public void updateServiceAppointmentAddresses(List<Account> listOld, List<Account> listNew, Map<Id, Account> mapOld, Map<Id, Account> mapNew){        
        List<Id> accountIds = new List<Id>();
        
        for(Account acc : listNew){
            if((mapOld.get(acc.Id).ShippingStreet != acc.ShippingStreet)|| (mapOld.get(acc.Id).ShippingCity != acc.ShippingCity)|| (mapOld.get(acc.Id).ShippingState != acc.ShippingState)||(mapOld.get(acc.Id).ShippingPostalCode != acc.ShippingPostalCode)){
                accountIds.add(acc.Id);
            }
        }
        
        // If no account's address has changed, just return
        if (accountIds.size() == 0) return;
    List<ServiceAppointment> relatedServiceAppointments = [Select Id,AccountId, Status, Country,City, PostalCode, State, Street FROM ServiceAppointment WHERE AccountId IN: accountIds and Status in ('None', 'Scheduled')];
    
    for(ServiceAppointment sa :relatedServiceAppointments){
        if(mapNew.containskey(sa.accountId)){
            account a = mapNew.get(sa.AccountId);
            {
                 sa.Country = a.ShippingCountry;
                 sa.Street = a.ShippingStreet;
                 sa.City  = a.ShippingCity;
                 sa.State = a.ShippingState;
                 sa.PostalCode = a.ShippingPostalCode;
            
             }
                    } 
                }
    if (!relatedServiceAppointments.isEmpty() &&relatedServiceAppointments.size()>0)
        update relatedServiceAppointments;
    }*/

}