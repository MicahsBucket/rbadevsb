/**
 * @File Name          : SalesSchedDateSchedule5PM.cls
 * @Description        : 
 * @Author             : James Loghry (Demand Chain)
 * @Group              : 
 * @Last Modified By   : James Loghry (Demand Chain)
 * @Last Modified On   : 5/7/2019, 2:14:02 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/7/2019, 12:49:51 PM   James Loghry (Demand Chain)     Initial Version
**/
public class SalesSchedDateSchedule7PM implements Schedulable{

    public void execute(SchedulableContext sc){
        Time storeCloseTime = Time.newInstance(19,0,0,0);
        Date nextDate = SalesSchedDateSchedule.setNextDate(storeCloseTime);
        SalesSchedDateSchedule.executeMetrics(nextDate, storeCloseTime);
    }
}