public class CanvassUnitBatchPageSingleZip 
{
    public CanvassUnitBatchPageSingleZip()
    {
        
    }
    
    @RemoteAction
    public static string runBatch(string zipcode)
    {
        if(zipcode != '')
        {
            if(zipcode != null)
            {
                if (!Test.isRunningTest())
                {
                    database.executebatch(new CanvassMarketBatch(null,zipcode),1);
                }
                return 'Batch Started';
            }
            else
            {
                return 'Error: Market Not Found';
            }
        }
        else
        {
            return 'Select Market';
        }
    }
}