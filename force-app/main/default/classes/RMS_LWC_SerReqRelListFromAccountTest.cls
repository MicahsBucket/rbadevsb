/*
* @author Jason Flippen
* @date 11/30/2020
* @description Test Class for the following classes:
*              - RMS_LWC_SerReqRelListFromAccount
*/
@isTest
public class RMS_LWC_SerReqRelListFromAccountTest {

    /*
    * @author Jason Flippen
    * @description Method to create data to be consumed by test methods.
    * @param N/A
    * @returns N/A
    */
    @testSetup static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        Account testVenderAccount = testUtility.createVendorAccount('Test Vendor Account');
        insert testVenderAccount;

        Account testStoreAccount = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAccount = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAccount.Store_Location__c = testStoreAccount.Id;
        insert testDwellingAccount;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAccount.Id, 'Closed - Won');
        insert testOpportunity;
    
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;

        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId(),
                                            Vendor__c = testVenderAccount.Id,
                                            Cost_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c = testFan.Id);
        insert testProduct;

        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        List<Order> testOrderList = new List<Order>();
        Order testOrder01 =  new Order(Name = 'Test Order 01',
                                       RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Record_Type').getRecordTypeId(),
                                       AccountId = testDwellingAccount.Id,
                                       EffectiveDate = Date.Today(),
                                       Store_Location__c = testStoreAccount.Id,
                                       OpportunityId = testOpportunity.Id,
                                       Status = 'Draft',
                                       Tech_Measure_Status__c = 'New',
                                       Pricebook2Id = Test.getStandardPricebookId());
        testOrderList.add(testOrder01);
        Order testOrder02 =  new Order(Name = 'Test Order 02',
                                       RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Service').getRecordTypeId(),
                                       AccountId = testDwellingAccount.Id,
                                       EffectiveDate = Date.Today(),
                                       Store_Location__c = testStoreAccount.Id,
                                       OpportunityId = testOpportunity.Id,
                                       Status = 'Draft',
                                       Tech_Measure_Status__c = 'New',
                                       Pricebook2Id = Test.getStandardPricebookId());
        testOrderList.add(testOrder02);
        insert testOrderList;
        
        Purchase_Order__c testPurchaseOrder = new Purchase_Order__c(Name = 'ABCD12345',
                                                                    Order__c = testOrder02.Id,
                                                                    RecordTypeId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Service_Purchase_Order').getRecordTypeId(),
                                                                    Status__c = 'In Process');
        insert testPurchaseOrder;

        OrderItem testOI = new OrderItem(OrderId = testOrder02.Id,
                                         Has_PO__c = true,
                                         Purchase_Order__c = testPurchaseOrder.Id,
                                         PricebookentryId = testPBEStandard.Id,
                                         Quantity = 2,
                                         UnitPrice = 100);
        insert testOI;

        Charge__c testCharge = new Charge__c(Service_Request__c = testOrder02.Id,
                                             Service_Product__c = testOI.Id);
        insert testCharge;

    }

    /*
    * @author Jason Flippen
    * @date 12/02/2020
    * @description Method to test the "assetsExist" method in the Controller.
    */
    public static testMethod void testAssetsExist() {

        Account testDwellingAccount = [SELECT Id FROM Account WHERE Name = '11111, Test Dwelling Account'];

        Test.startTest();

            Boolean assetExists = RMS_LWC_SerReqRelListFromAccount.assetExits(testDwellingAccount.Id);
            System.assertEquals(false, assetExists, 'Unexpected Existing Assets');

        Test.stopTest();

    }

    /*
    * @author Jason Flippen
    * @date 12/02/2020
    * @description Method to test the "fetchOrders" method in the Controller.
    */
    public static testMethod void testFetchOrders() {

        Account testDwellingAccount = [SELECT Id FROM Account WHERE Name = '11111, Test Dwelling Account'];
        Order testOrder = [SELECT OrderNumber FROM Order WHERE Name = 'Test Order 01'];

        Test.startTest();

            List<Order> orderList = RMS_LWC_SerReqRelListFromAccount.fetchOrders(testDwellingAccount.Id);
            System.assertEquals(testOrder.OrderNumber, orderList[0].OrderNumber, 'Unexpected Order Retrieved');

        Test.stopTest();

    }

    /*
    * @author Jason Flippen
    * @date 12/02/2020
    * @description Method to test the "getServiceReqs" method in the Controller.
    */
    public static testMethod void testGetServiceReqs() {

        Account testDwellingAccount = [SELECT Id FROM Account WHERE Name = '11111, Test Dwelling Account'];
        Order testOrder = [SELECT Id FROM Order WHERE Name = 'Test Order 02'];

        Test.startTest();

            List<RMS_LWC_SerReqRelListFromAccount.ServiceRequest> serviceRequestList = RMS_LWC_SerReqRelListFromAccount.getServiceReqs(testDwellingAccount.Id);
            System.assertEquals(testOrder.Id, serviceRequestList[0].RequestId, 'Unexpected Service Request Retrieved');

        Test.stopTest();

    }

    /*
    * @author Jason Flippen
    * @date 12/02/2020
    * @description Method to test the "allValidationsCheck" method in the Controller.
    */
    public static testMethod void testAllValidationsCheck() {

        Account testDwellingAccount = [SELECT Id FROM Account WHERE Name = '11111, Test Dwelling Account'];

        Test.startTest();

            String errorMessage = RMS_LWC_SerReqRelListFromAccount.allValidationsCheck(testDwellingAccount.Id);
            System.assertEquals(true, String.isBlank(errorMessage), 'Unexpected Error Message Returned');

        Test.stopTest();

    }

}