@isTest
private class BatchJobPostInstallationSurvey_UnitTest {

    @isTest 
    static void executeTest() {
    TestDataFactoryStatic.setUpConfigs();

        // Create the store
        Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
        Database.insert(storeAccount, true);
        Id storeAccountId = storeAccount.Id;

        // Get the security profile for field service
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'RMS-CORO Field Service' LIMIT 1].Id;

        // Create the test users for tech measure and install
        User serviceUserTechMeasure = TestDataFactoryStatic.createUser(profileId);
        User serviceUserInstaller = TestDataFactoryStatic.createUser(profileId);

        // Get the store configuration Id
        Id storeConfigId = [SELECT Id FROM Store_Configuration__c LIMIT 1].Id;

        // Create the test Account for the store
        Account testAccount = TestDataFactoryStatic.createAccount('Calvins', storeAccountId);
        Id accountId = testAccount.Id;

        // Create a contact for the store account
        Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'CalvinPrime');
        Contact SecondaryContact = TestDataFactoryStatic.createContact(accountId, 'CalvinPrime');

        // Create a test opportunity, opportunity primary contact and an order
        Opportunity opportunity = TestDataFactoryStatic.createFSLOpportunity('Calvins Opportunity', accountId, storeAccountId, 'Sold', date.today());
        OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
        Order order = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        Id orderId = order.Id;
        
         // Create a test opportunity, opportunity Secondary contact and an order
        Opportunity opportunity1 = TestDataFactoryStatic.createFSLOpportunity('Calvins Opportunity', accountId, storeAccountId, 'Sold', date.today());
        OpportunityContactRole oppContJunc1 = TestDataFactoryStatic.createOppCon('Decision Maker', true, SecondaryContact, opportunity);
        Order order1 = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        Id orderId1 = order.Id;
        
        //setup field service lightning permission sets for the service users
        //List<PermissionSet> PermissionSetList = [Select Label, Id from PermissionSet where Label = 'FSL Resource Permissions' or Label = 'FSL Resource License'];
        List<Id> PermissionSetIds = new List<Id>();
        for(PermissionSet ps : [Select Label, Id from PermissionSet where Label = 'FSL Resource Permissions' or Label = 'FSL Resource License']) {
            PermissionSetIds.add(ps.Id);
        }

        // Id permissionSetId = [SELECT Id FROM PermissionSet WHERE Label = 'FSL Resource Permissions' LIMIT 1].Id;
        // Id permissionSetId2 = [SELECT Id FROM PermissionSet WHERE Label = 'FSL Resource License'].Id;
        Database.insert(serviceUserTechMeasure);
        Database.insert(serviceUserInstaller);
        
        
        //assign permissions to tech measure
       /* System.runAs(serviceUserTechMeasure) {
            PermissionSetAssignment psa = new PermissionSetAssignment(
                PermissionSetId = PermissionSetIds[0], 
                AssigneeId = serviceUserTechMeasure.Id
            );
            PermissionSetAssignment psa2 = new PermissionSetAssignment(
                PermissionSetId = PermissionSetIds[1], 
                AssigneeId = serviceUserTechMeasure.Id
            );
            List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>{psa,psa2};
            //Database.insert(psaList);
        }
        
        // Assign permissions to installer
        System.runAs(serviceUserInstaller) {
            PermissionSetAssignment psa3 = new PermissionSetAssignment(
                PermissionSetId = PermissionSetIds[0], 
                AssigneeId = serviceUserInstaller.Id
            );
            PermissionSetAssignment psa4 = new PermissionSetAssignment(
                PermissionSetId = PermissionSetIds[1], 
                AssigneeId = serviceUserInstaller.Id
            );
            List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>{psa3,psa4};
            Database.insert(psaList);
        }*/

        // Creating Work Types for the Work Order
        Id measureTypeId = TestDataFactoryStatic.createFLSWorkType('Measure', 8.00, 'Hours').Id;
        Id installTypeId = TestDataFactoryStatic.createFLSWorkType('Install', 2.00, 'Hours').Id;



        //Creating FSL objects required to attach an assigned Tech Measure to a Work Order
 /*       WorkOrder workOrd = TestDataFactoryStatic.createFSLWorkOrder(measureTypeId, 'Tech Measure', orderId, testAccount, opportunity);
        Database.insert(workOrd); 
        ServiceResource servResource = TestDataFactoryStatic.createFSLServiceResource(serviceUserTechMeasure.Id, 'Calvin', 'test1', storeConfigId);
        Database.insert(servResource);
        TestDataFactoryStatic.createFSLServiceTerritory(servResource, 'test1');
        ServiceAppointment servAppt = TestDataFactoryStatic.createFSLServiceAppointment(workOrd.Id);
        Database.insert(servAppt);
        AssignedResource assignResource = TestDataFactoryStatic.createFSLAssignedResource(servResource, servAppt);
        Database.insert(assignResource);
                     
        System.debug('---------creating more FSL objects');
        //Creating FSL objects required to attach an assigned Tech Measure to a Work Order
        WorkOrder workOrd2 = TestDataFactoryStatic.createFSLWorkOrder(installTypeId, 'Install', orderId, testAccount, opportunity);
        Database.insert(workOrd2); 
        ServiceResource servResource2 = TestDataFactoryStatic.createFSLServiceResource(serviceUserInstaller.Id, 'Calvin','test2', storeConfigId);
        Database.insert(servResource2);
        TestDataFactoryStatic.createFSLServiceTerritory(servResource2, 'test2');
        ServiceAppointment servAppt2 = TestDataFactoryStatic.createFSLServiceAppointment(workOrd2.Id);
        System.debug('++++++++++++++++++++++++++++servAppt2: ' + Database.insert(servAppt2));
        AssignedResource assignResource2 = TestDataFactoryStatic.createFSLAssignedResource(servResource2, servAppt2);
 //       Database.insert(assignResource2);

*/

//*******************************************

        Test.startTest();

        List<WorkOrder> workOrdList = new List<WorkOrder>();
        workOrdList.add(TestDataFactoryStatic.createFSLWorkOrder(measureTypeId, 'Tech Measure', orderId, testAccount, opportunity));
        workOrdList.add(TestDataFactoryStatic.createFSLWorkOrder(installTypeId, 'Install', orderId, testAccount, opportunity));
        System.debug(workOrdList);
        Database.insert(workOrdList);
        
        List<ServiceResource> servResourceList = new List<ServiceResource>();
        servResourceList.add(TestDataFactoryStatic.createFSLServiceResource(serviceUserTechMeasure.Id, 'Calvin', 'test1', storeConfigId));
        servResourceList.add(TestDataFactoryStatic.createFSLServiceResource(serviceUserInstaller.Id, 'Calvin', 'test2', storeConfigId));
        
        Database.insert(servResourceList);
       Order ords = [SELECT Id,Primary_Tech_Measure_FSL__c,Primary_Tech_Measure_FSL__r.RelatedRecordId,Primary_Installer_FSL__c,Primary_Installer_FSL__r.RelatedRecordId 
                           From Order limit 1];
        ords.Primary_Tech_Measure_FSL__c = servResourceList[0].Id;
        ords.Primary_Installer_FSL__c = servResourceList[1].Id;
        update ords;
        
        TestDataFactoryStatic.createFSLServiceTerritory(servResourceList[0], 'test1');
        TestDataFactoryStatic.createFSLServiceTerritory(servResourceList[1], 'test2');
        
        List<ServiceAppointment> servApptList = new List<ServiceAppointment>();
        servApptList.add(TestDataFactoryStatic.createFSLServiceAppointment(workOrdList[0].Id));
        servApptList.add(TestDataFactoryStatic.createFSLServiceAppointment(workOrdList[1].Id));
        Database.insert(servApptList);
        
        List<AssignedResource> assignResourceList = new List<AssignedResource>();
        assignResourceList.add(TestDataFactoryStatic.createFSLAssignedResource(servResourceList[0], servApptList[0]));
        assignResourceList.add(TestDataFactoryStatic.createFSLAssignedResource(servResourceList[1], servApptList[1]));
       // Database.insert(assignResourceList);
        
        
        BatchJobPostInstallationSurvey obj = new BatchJobPostInstallationSurvey();
        System.debug(Database.executeBatch(obj));

        Test.stopTest();

//*******************************************

        // Verify that the survey was created
    /*   Survey__c newSurvey = [SELECT Id, Primary_Contact_First_Name__c, Primary_Contact_Last_Name__c, Opportunity__c, Installation_Date__c, Order_Name__c FROM Survey__c LIMIT 1];
        System.assertEquals('CalvinPrimeTest', newSurvey.Primary_Contact_First_Name__c);
        
        Worker__c tech;
        Worker__c installer;
        for(Worker__c w : [Select Id, First_Name__c, Last_Name__c, Role__c from Worker__c where Role__c = 'Tech Measurer' or Role__c = 'Installer']) {
            if(w.Role__c == 'Tech Measurer') {
                tech = w;
            }
            else {
                installer = w;
            }
        }
        
        // TODO - Delete: Worker__c tech = [SELECT Id, First_Name__c, Last_Name__c, Role__c FROM Worker__c WHERE Role__c = 'Tech Measurer' LIMIT 1];

        // Verify tech measurer worker
        String techRole = tech.Role__c;
        System.assertEquals('Tech Measurer', techRole);
        Integer workerSize = [SELECT Id, Role__c FROM Worker__c].size();
        

        // TODO - Delete: Worker__c installer = [SELECT Id, First_Name__c, Last_Name__c, Role__c FROM Worker__c WHERE Role__c = 'Installer' LIMIT 1];

        // Verify installer worker role
        String installerRole = installer.Role__c;
        System.assertEquals('Installer', installerRole);
        system.assertEquals(3, workerSize);
        
        Survey_Worker__c swJunc = [SELECT Id, Worker_Id__c, Survey_Id__c FROM Survey_Worker__c WHERE Worker_Id__c = :tech.Id LIMIT 1];
        System.assertEquals(swJunc.Worker_Id__c, tech.Id);
        System.assertEquals(swJunc.Survey_Id__c, newSurvey.Id);
        
    }
    */
    }
}