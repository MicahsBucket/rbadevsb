/**
* @author Jason Flippen
* @description Provides database functionality for the orderCancelledProductList LWC
*/ 
public with sharing class OrderCancelledProductListController {

    /**
    * @author Jason Flippen
    * @description Combines Cancelled Parent & Child OrderItems in a List of Wrapper objects.
    * @param Id of the Order the OrderItems are associated with.
    * @returns Wrapped List of Parent/Child OrderItem records.
    */ 
    @AuraEnabled(cacheable=true)
    public static List<OrderItemWrapper> getCancelledOrderItems(Id orderId) {

        List<OrderItemWrapper> oiWrapperList = new List<OrderItemWrapper>();

        // Group the OrderItem data in Parent and Parent-to-Child Maps.
        Map<Id,OrderItem> parentOIMap = new Map<Id,OrderItem>();
        Map<Id,Map<Id,OrderItem>> parentToChildOIMap = new Map<Id,Map<Id,OrderItem>>();
        for (OrderItem oi : [SELECT Id,
                                    Cancellation_Reason__c,
                                    Parent_Order_Item__c,
                                    Purchase_Order__c,
                                    Purchase_Order_Name__c,
                                    Product2Id,
                                    Product2.Name,
                                    Product2.Open_In_Visualforce__c,
                                    Quantity,
                                    Status__c,
                                    Total_Retail_Price__c,
                                    Total_Wholesale_Cost__c,
                                    UnitPrice,
                                    Unit_Id__c,
                                    Unit_Wholesale_Cost__c,
                                    Variant_Number__c
                             FROM   OrderItem
                             WHERE  OrderId = :orderId
                             AND    Status__c = 'Cancelled'
                             ORDER BY Unit_Id__c]) {
            
            // If a Parent_Order_Item__c is null this is a Parent record.
            if (oi.Parent_Order_Item__c == null) {
                parentOIMap.put(oi.Id, oi);
            }
            else {
                if (!parentToChildOIMap.containsKey(oi.Parent_Order_Item__c)) {
                    parentToChildOIMap.put(oi.Parent_Order_Item__c, new Map<Id,OrderItem>());
                }
                parentToChildOIMap.get(oi.Parent_Order_Item__c).put(oi.Id,oi);
            }

        }
System.debug('*** parentToChildOIMap: ' + parentToChildOIMap);

        // Iterate through the Maps and build the return "wrapper" List.
        for (OrderItem parentOI : parentOIMap.values()) {
            OrderItemWrapper oiWrapper = new OrderItemWrapper();
            oiWrapper.id = parentOI.Id;
            oiWrapper.status = parentOI.Status__c;
            oiWrapper.unitId = parentOI.Unit_Id__c;
            oiWrapper.productId = parentOI.Product2Id;
            oiWrapper.productName = parentOI.Product2.Name;
            oiWrapper.productOpenInVisualforce = parentOI.Product2.Open_In_Visualforce__c;
            oiWrapper.purchaseOrder = parentOI.Purchase_Order__c;
            oiWrapper.purchaseOrderName = parentOI.Purchase_Order_Name__c;
            oiWrapper.quantity = parentOI.Quantity;
            oiWrapper.unitPrice = parentOI.UnitPrice;
            oiWrapper.totalRetailPrice = parentOI.Total_Retail_Price__c;
            oiWrapper.unitWholesaleCost = parentOI.Unit_Wholesale_Cost__c;
            oiWrapper.totalWholesaleCost = parentOI.Total_Wholesale_Cost__c;
            oiWrapper.variantNumber = parentOI.Variant_Number__c;
            oiWrapper.cancellationReason = parentOI.Cancellation_Reason__c;

            // Iterate through the child OI Map (if applicable)
            // and add those records to the Wrapper
            List<OrderItemWrapper> childOIWList = new List<OrderItemWrapper>();
            if (parentToChildOIMap.containsKey(parentOI.Id)) {
                for (OrderItem childOI : parenttoChildOIMap.get(parentOI.Id).values()) {
                    OrderItemWrapper childOIWrapper = new OrderItemWrapper();
                    childOIWrapper.id = childOI.Id;
                    childOIWrapper.status = childOI.Status__c;
                    childOIWrapper.unitId = childOI.Unit_Id__c;
                    childOIWrapper.productId = childOI.Product2Id;
                    childOIWrapper.productName = childOI.Product2.Name;
                    childOIWrapper.productOpenInVisualforce = childOI.Product2.Open_In_Visualforce__c;
                    childOIWrapper.purchaseOrder = childOI.Purchase_Order__c;
                    childOIWrapper.purchaseOrderName = childOI.Purchase_Order_Name__c;
                    childOIWrapper.quantity = childOI.Quantity;
                    childOIWrapper.unitPrice = childOI.UnitPrice;
                    childOIWrapper.totalRetailPrice = childOI.Total_Retail_Price__c;
                    childOIWrapper.unitWholesaleCost = childOI.Unit_Wholesale_Cost__c;
                    childOIWrapper.totalWholesaleCost = childOI.Total_Wholesale_Cost__c;
                    childOIWrapper.variantNumber = childOI.Variant_Number__c;
                    childOIWrapper.cancellationReason = childOI.Cancellation_Reason__c;
                    childOIWList.add(childOIWrapper);
                }
                oiWrapper.miscCharges = childOIWList;
            }
            oiWrapper.miscChargesCount = childOIWList.size();
            oiWrapperList.add(oiWrapper);
        }

        return oiWrapperList;

    } // End Method: getOrderItems()

    /**
    * @author Jason Flippen
    * @description Class to combine (wrap) Cancelled Parent and Child OrderItem records.
    */
    @TestVisible
    public class OrderItemWrapper {

        @TestVisible
        @AuraEnabled public String id {get;set;}
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public String unitId {get;set;}
        @AuraEnabled public String productId {get;set;}
        @AuraEnabled public String productName {get;set;}
        @AuraEnabled public Boolean productOpenInVisualforce {get;set;}
        @AuraEnabled public String purchaseOrder {get;set;}
        @AuraEnabled public String purchaseOrderName {get; set;}
        @AuraEnabled public Decimal quantity {get;set;}
        @AuraEnabled public Decimal unitPrice {get;set;}
        @AuraEnabled public Decimal totalRetailPrice {get;set;}
        @AuraEnabled public Decimal unitWholesaleCost {get;set;}
        @AuraEnabled public Decimal totalWholesaleCost {get;set;}
        @AuraEnabled public String variantNumber {get;set;}
        @AuraEnabled public String cancellationReason {get;set;}
        @AuraEnabled public Decimal miscChargesCount {get;set;}
        @AuraEnabled public List<OrderItemWrapper> miscCharges {get;set;}

    } // End (Wrapper) Class

}