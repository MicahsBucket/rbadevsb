// Code coverage provided by: AerisWeatherForecastHelperTest.cls

global without sharing class BatchUpdateServiceAppointmentForecasts implements Database.Batchable<sObject>, Database.AllowsCallouts{
	// FOR QA TESTING: use a lower value for FORECAST_WINDOW_DAYS (3 or 4) and MIN_REFRESH_INTERVAL_HOURS of 24 to stay within 750 call/day trial limit
	// FOR PROD: use FORECAST_WINDOW_DAYS of 5 or 7 and MIN_REFRESH_INTERVAL_HOURS of 12 or 24.  BATCH_SIZE of 50 + scheduling every 15 mins will limit calls to <5000 regardless.

    global static final Integer BATCH_SIZE = 50; // AerisWeather limit is 100 calls/min and 5000 calls/day with Basic subscription
    global static final Integer FORECAST_WINDOW_DAYS = 5; // Number of days in the future for which to retrieve weather forecast data.
    global static final Integer MIN_REFRESH_INTERVAL_HOURS = 24; // Minimum number of hours between a service appointment's weather refreshes
    global final String query;

    global static DateTime timestampCutoff = System.now().addHours(-1 * MIN_REFRESH_INTERVAL_HOURS);

    // Normal Constructor
    global BatchUpdateServiceAppointmentForecasts() {

        // Query for service appointments in the next N days which have not had their weather forecast updated for the longest period of time
        query = 'SELECT PostalCode,Id,Latitude,Longitude,SchedStartTime,SchedEndTime,Weather_Timestamp__c,FSL__InJeopardy__c,FSL__InJeopardyReason__c,'
        	+ ' ServiceTerritoryId,FSL__IsMultiDay__c,FSL__MDT_Operational_Time__c,Work_Order__r.RecordType.Name,ServiceTerritory.ParentTerritoryId, Weather_Forecast_Warnings__c' 
        	+ ' FROM ServiceAppointment WHERE SchedEndTime >= TODAY AND SchedStartTime <= NEXT_N_DAYS:' + FORECAST_WINDOW_DAYS
        	+ ' AND ((Latitude != null AND Longitude != null) OR (PostalCode != null)) AND Work_Order__c != null AND ServiceTerritoryId != null'
        	+ ' AND (Weather_Timestamp__c = null OR Weather_Timestamp__c < :timestampCutoff)'
        	+ ' ORDER BY Weather_Timestamp__c ASC NULLS FIRST, SchedStartTime LIMIT ' + BATCH_SIZE;
    }

    // Constructor for debugging
    /*global BatchUpdateServiceAppointmentForecasts(Id serviceAppointmentId) {

        query = 'SELECT PostalCode,Id,Latitude,Longitude,SchedStartTime,SchedEndTime,Weather_Timestamp__c,FSL__InJeopardy__c,FSL__InJeopardyReason__c,'
        	+ ' ServiceTerritoryId,FSL__IsMultiDay__c,FSL__MDT_Operational_Time__c,Work_Order__r.RecordType.Name,ServiceTerritory.ParentTerritoryId, Weather_Forecast_Warnings__c' 
        	+ ' FROM ServiceAppointment WHERE Id = \'' + serviceAppointmentId + '\'';
    }*/

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<ServiceAppointment> sasToUpdate = AerisWeatherForecastsHelper.updateForecasts(scope);

        Database.update(sasToUpdate, false); // Allow partial success in case some appointments are non-updatable
    }

    global void finish(Database.BatchableContext BC){
        
    }
}