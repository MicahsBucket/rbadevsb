/**
 * @File Name          : MakabilityCheckSpecialLegHeightCalcTest.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 5/20/2019, 3:15:47 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/20/2019, 3:15:47 PM   mark.rothermal@andersencorp.com     Initial Version
**/
@isTest
public class MakabilityCheckSpecialLegHeightCalcTest {

    @isTest
    public static void passingChordTest() {

        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(50,24,'Chord' ,'1');
        test.startTest();
 	    MakabilityCheckSpecialLegHeightCalc.legCalcResult result = MakabilityCheckSpecialLegHeightCalc.checkLegCalc(configsToTest.get('1'));
        test.stopTest();
        Boolean assertMsg = result.productMakable;
        system.assertEquals(assertMsg, true);
    }

    @isTest
    public static void passingCircleTopTest() {

        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(50,29,'Circle Top' ,'1');
        test.startTest();
 	    MakabilityCheckSpecialLegHeightCalc.legCalcResult result = MakabilityCheckSpecialLegHeightCalc.checkLegCalc(configsToTest.get('1'));
        test.stopTest();
        Boolean assertMsg = result.productMakable;
        system.assertEquals(assertMsg, true);
    }

    @isTest
    public static void passingSpringlineTest() {
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(50,50,'Springline' ,'1');
        test.startTest();
 	    MakabilityCheckSpecialLegHeightCalc.legCalcResult result = MakabilityCheckSpecialLegHeightCalc.checkLegCalc(configsToTest.get('1'));
        test.stopTest();
        Boolean assertMsg = result.productMakable;
        system.assertEquals(assertMsg, true);
    }

    @isTest
    public static void requireSpringLineTest() {
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(50,50,'Circle Top' ,'1');
        test.startTest();
 	    MakabilityCheckSpecialLegHeightCalc.legCalcResult result = MakabilityCheckSpecialLegHeightCalc.checkLegCalc(configsToTest.get('1'));
        test.stopTest();
        system.debug('results requireSpringLineTest ' + result);
        Boolean assertMsg = result.productMakable;
        system.assertEquals(assertMsg, false);          
    }  

    @isTest
    public static void requiresCirleTopTest() {
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(50,28,'Chord' ,'1');
        test.startTest();
 	    MakabilityCheckSpecialLegHeightCalc.legCalcResult result = MakabilityCheckSpecialLegHeightCalc.checkLegCalc(configsToTest.get('1'));
        test.stopTest();
        system.debug('results requiresCirleTopTest ' + result);
        Boolean assertMsg = result.productMakable;
        system.assertEquals(assertMsg, false);             
    }   

    @isTest
    public static void requiresChordTest() {
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(50,23, 'Springline' ,'1');
        test.startTest();
 	    MakabilityCheckSpecialLegHeightCalc.legCalcResult result = MakabilityCheckSpecialLegHeightCalc.checkLegCalc(configsToTest.get('1'));
        test.stopTest();
        system.debug('results requiresChordTest ' + result);
        Boolean assertMsg = result.productMakable;
        system.assertEquals(assertMsg, false);             
    }   

     
    
    private static Map<String,MakabilityRestResource.OrderItem>  createOrderItem(Decimal wInch, Decimal hInch,String shape, String oiId){
        Map<string,MakabilityRestResource.OrderItem> orderItems = new Map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
        MakabilityCalculator mc = new MakabilityCalculator();
		ProductConfiguration pc = new ProductConfiguration();
        pc.widthInches = wInch;
        pc.widthFractions = 'Even';
        pc.heightInches = hInch;
        pc.heightFractions = 'Even';        
        pc.specialtyShape = shape; 
        /////////////////////       
        oi.orderItemId = oiId;
        oi.makabilityCalculator = mc;
        oi.productConfiguration = pc;
        orderItems.put(oiId,oi);       
        return orderItems;
    }
}