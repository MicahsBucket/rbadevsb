@isTest
Public class WorkerRestAPI_UnitTest{

static testMethod void testDoPost(){
		TestDataFactory.setUpConfigs();
	    Account testStoreAccount = TestDataFactory.createStoreAccount('Test Store');
	    Database.insert(testStoreAccount, true);
		Id accountId = testStoreAccount.Id;
		Id surveyId = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0].Id;
		TestDataFactory.createWorkers('Sales Rep', 1, accountId, surveyId);
		Id workerId = [SELECT Id FROM Worker__c LIMIT 1].Id;
		Survey_Worker__c survWorkJunc = new Survey_Worker__c(Role__c = 'Sales Rep', Worker_Id__c = workerId, Survey_Id__c = surveyId);
		Database.insert(survWorkJunc);


        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = '/services/apexrest/postWorker';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
       
        String statusMedallia = 'Accepted';
        
        Test.startTest();
        	String results = WorkerRestAPI.getWorkers(surveyId, statusMedallia);
        	Boolean containsLastName = results.contains('lastname0');
        	System.assertEquals(true, containsLastName);
        Test.stopTest();
        
    }       
}