/*
* @author Jason Flippen
* @date 06/22/2020 
* @description Class to provide functionality for the newCostPurchaseOrderAction LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - NewCostPOActionControllerTest
*/
public with sharing class NewCostPOActionController {

    /*
    * @author Jason Flippen
    * @date 06/22/2020
    * @description Method to return a new instance of a Cost Purchase Order.
    */
    @AuraEnabled(cacheable=true)
    public static Purchase_Order__c getNewCostPOData() {

        Purchase_Order__c newCostPO = new Purchase_Order__c();

        User currentUser = [SELECT Id,
                                   Default_Store_Location__c
                            FROM   User
                            WHERE  Id = :UserInfo.getUserId()];
        
        String storeLocationName = currentUser.Default_Store_Location__c;

        Account storeLocation = null;
        if (String.isNotBlank(storeLocationName)) {
            storeLocation = [SELECT Id
                             FROM   Account
                             WHERE  Name = :storeLocationName];
        }

        newCostPO.Comments__c = null;
        newCostPO.Date__c = Date.today();
        newCostPO.Order__c = UtilityMethods.getDummyOrderId();
        newCostPO.RecordTypeId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Cost_Purchase_Order').getRecordTypeId();
        newCostPO.Reference__c = null;
        newCostPO.Requested_Ship_Date__c = null;
        newCostPO.Status__c = 'In Process';
        newCostPO.Store_Location__c = storeLocation.Id;
        newCostPO.Tax__c = 0;
        newCostPO.Vendor__c = null;

        return newCostPO;

    }

    /*
    * @author Jason Flippen
    * @date 06/22/2020
    * @description Method to create the new Cost Purchase Order.
    */
    @AuraEnabled()
    public static Map<String,String> createCostPO(Purchase_Order__c newPurchaseOrder) {

        Map<String,String> resultMap = new Map<String,String>();

        try {

            newPurchaseOrder.Name = 'PlaceholderPOName';
            newPurchaseOrder.Store_Abbreviation__c = getStoreAbbreviation(newPurchaseOrder);
            insert newPurchaseOrder;

            resultMap.put('New Cost PO Success', newPurchaseOrder.Id);

        }
        catch (Exception ex) {
            System.debug('***** The following exception occurred in the NewCostPOActionController in the createCostPO method inserting records:' + ex);
            resultMap.put(ex.getMessage(), '');
        }

        return resultMap;

    }

        /*
    * @author Jason Flippen
    * @date 03/02/2021
    * @description Method to retrieve a concatenated Store Abbreviation.
    */
    private static String getStoreAbbreviation(Purchase_Order__c newPurchaseOrder) {
        
        String returnValue = '';

        // Retrive the current user
        User currentUser = [SELECT User_Abbreviation__c FROM User WHERE Id =: UserInfo.getUserId()];
        
        // If a test is running just set the user's abbreviation, otherwise get it from the current user.
        String userAbbreviation = 'X';
        if (Test.isRunningTest() == true) {
            userAbbreviation = 'A';
        }
        else if (String.isNotBlank(currentUser.User_Abbreviation__c)) {
            userAbbreviation = currentUser.User_Abbreviation__c;
        }

        // Set the type of PO
        String typeAbbreviation = 'C';
        
        String storeAbbreviation = 'X';

        // Query the Store Location (Account) to retrieve the store abbreviation.
        Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id,
                                                                 Active_Store_Configuration__c,
                                                                 Active_Store_Configuration__r.Store_Abbreviation__c
                                                          FROM   Account
                                                          WHERE  Id = :newPurchaseOrder.Store_Location__c
                                                          AND    Active_Store_Configuration__c != null]);
        
        if (!accountMap.isEmpty()) {

            // Update the store abbreviation to the value from the Store Location (Account).
            storeAbbreviation = '';
            Account account = accountMap.values()[0];
            if (account.Active_Store_Configuration__r.Store_Abbreviation__c != null) {
                storeAbbreviation = account.Active_Store_Configuration__r.Store_Abbreviation__c;
            }

        }

        // Set the return value to the store abbreviation, user abbreviation, and type abbreviation (concatenated).
        returnValue = storeAbbreviation + userAbbreviation + typeAbbreviation;

        return returnValue;

    }

}