@isTest
public with sharing class RetailPurchaseOrderTriggerHandlerTest {
    
    @isTest
    public static void updateRelatedOrderItemsTest() {

        Id oId = createMockData();
         Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;
        
        Test.startTest();
        Retail_Purchase_Order__c rpo = new Retail_Purchase_Order__c(
            Order__c = oId ,Store_Location__c=store.id ,Store_Number__c='1234'        
        );
        insert rpo;
        Test.stopTest();
        
        List<OrderItem> assertionList = [Select id, Retail_Purchase_Order__c from OrderItem where OrderId = :oId];
        System.assertEquals(rpo.Id, assertionList[0].Retail_Purchase_Order__c);
    }
    
    private static Id createMockData(){
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
       /*RMS_Settings__c customSetting = new RMS_Settings__c(
            Value__c = '1234567890', 
            Name='Data Loading Profile ID'
        );
        insert customSetting;   
       */  
        
        Id pbId = Test.getStandardPricebookId();
        
        Account a = new Account(
            Name = 'Test Dwelling',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dwelling').getRecordTypeId()      
        );
        insert a;
        Account rba = new Account(
            Name = 'Renewal by Andersen',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId()      
        );
        insert rba;
        
        
        Product2 p = new Product2(
            Name = 'Test x',
            IsActive = true,
            Vendor__c = rba.Id
        );
        insert p;
        
        PriceBookEntry pbe = new PriceBookEntry(
            Pricebook2Id = pbId,
            Product2Id = p.Id,
            UnitPrice = 1.00,
            IsActive = true     
        );
        insert pbe;
        
        Order o = new Order(
            Pricebook2Id = pbId,
            AccountId = a.Id,
            Apex_Context__c = true,
            EffectiveDate = System.today(),
            Status = 'New'        
        ); 
        insert o;
        
        OrderItem oi = new OrderItem(
            OrderId = o.Id,
            Quantity = 1,
            UnitPrice = 0,
            PriceBookEntryId = pbe.Id,
            Verify_Item_Configuration__c = true
        ); 
        insert oi;
        return o.Id;        
    }
    
     static testmethod void updateOrderStatusTest() {

        Id oId = createMockData();
          Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;

        
        Test.startTest();
        Retail_Purchase_Order__c rpo = new Retail_Purchase_Order__c(
            Order__c = oId,status__c='confirmed');
         rpo.Store_Number__c='1234';
         rpo.Store_Location__c=store.id;
        insert rpo;
        update rpo;
        Test.stopTest();
        
       
    }
}