@isTest
private class CustomerPayment_UnitTest {
@testSetup
    static void setup(){

        	TestUtilityMethods testMethods = new TestUtilityMethods();
		testMethods.setUpConfigs();
		
		Account store1 = [SELECT id, Active_Store_Configuration__c FROM Account WHERE Name='77 - Twin Cities, MN'];
		Store_Configuration__c storeConfig1 = [SELECT id FROM Store_Configuration__c WHERE Store__c = :store1.id ];
		
		Account dwelling1 = testMethods.createDwellingAccount('dwelling1');
		dwelling1.Store_Location__c = store1.id;
		insert dwelling1;
		
		Contact contact1 = testMethods.createContact(dwelling1.id, 'contact1');
		insert contact1;
		
		Opportunity opp1 = testMethods.createOpportunity(dwelling1.id, 'Draft');
		insert opp1;
		
		Order order1 = new Order(	Name='Sold Order 1', 
									AccountId = dwelling1.id, 
									EffectiveDate= Date.Today(),
                                 
									Store_Location__c = store1.Id,
									Opportunity = opp1, 									
									Status ='Tech Measure Needed',
									BillToContactId = contact1.id, 
									Pricebook2Id = Test.getStandardPricebookId()
								);
		insert order1;

			
		
		Account vend1 = testMethods.createIntercompanyVendorAccount('Intercompany');
		Account vend2 = testMethods.createIntercompanyCCVendorAccount('IntercompanyCC');
		Account vend3 = testMethods.createExternalVendorAccount('External');
		List<Account> vendsToInsert = new List<Account>{vend1,vend2,vend3};
		
		insert vendsToInsert;
		
		Pricebook2 pricebook1 =  testMethods.createPricebook2Name('Standard Price Book');
		insert pricebook1;

		Product2 product1 = new Product2(
			Name='Test Product',
			Vendor__c = vend1.id
		);

		insert product1;
		
		PricebookEntry pricebookEntry1 = testMethods.createPricebookEntry(Test.getStandardPricebookId(), product1.id);
		insert pricebookEntry1;
		
		OrderItem orderItem1 = new OrderItem(OrderId = order1.id, PricebookentryId = pricebookEntry1.Id, Quantity = 2, UnitPrice = 10000 );
		insert orderItem1;	

		testMethods.createAllFinancialRecords(storeConfig1);
    }
    
    @isTest
    static void getOrderTest(){
        Order ord = [SELECT Id FROM Order LIMIT 1];
        Order testOrder = CustomerPaymentController.getOrder(ord.Id);
        System.assertEquals(ord.Id, testOrder.Id);
    }
    
    @isTest
    static void getOrderDates(){
        Order ord = [SELECT Id ,EffectiveDate FROM Order LIMIT 1];
        Order testOrder = CustomerPaymentController.getORderDates(ord.Id);
        System.assertEquals(Date.today(), testOrder.EffectiveDate);
    }
    
     @isTest
    static void getPaymentsTest(){
       Order soldOrder = [SELECT Id, AccountId, BillToContactId FROM Order where Name='Sold Order 1'];		
		Account store = [SELECT id FROM Account WHERE Name='77 - Twin Cities, MN'];				

		Id paymentDepositRecordTypeId = UtilityMethods.retrieveRecordTypeId('Deposit', 'Payment__c');
		Payment__c payment1 = new Payment__c(	Order__c = soldOrder.id,
												RecordTypeId = paymentDepositRecordTypeId,
												Payment_Type__c = 'Deposit',
												Store_Location__c = store.id,
												Payment_Amount__c = 100,
												Payment_Date__c = Date.Today()
												);
		insert payment1;
       List<Payment__c> pay =  CustomerPaymentController.getPayments(soldOrder.ID);
       system.assert(pay.size()>0);
    }
    
     @isTest
    static void getRefundsTest(){
		
		Test.startTest();
        Order soldOrder = [SELECT Id, AccountId, BillToContactId FROM Order where Name='Sold Order 1'];		
		Account store = [SELECT id FROM Account WHERE Name='77 - Twin Cities, MN'];		
		Id paymentDepositRecordTypeId = UtilityMethods.retrieveRecordTypeId('Deposit', 'Payment__c');
		Payment__c payment1 = new Payment__c(	Order__c = soldOrder.id,
												RecordTypeId = paymentDepositRecordTypeId,
												Payment_Type__c = 'Check',
												Store_Location__c = store.id,
												Payment_Amount__c = 100,
												Payment_Date__c = Date.Today()
												);
		insert payment1;
		soldOrder.Revenue_Recognized_Date__c = System.today();
		update soldOrder;
		
		Refund__c refund1 = new Refund__c(	Order__c = soldOrder.id, 
											Amount__c = 10, 
											Store_Location__c = store.id, 
											Refund_Method__c = 'Check'
											);
        insert refund1;
		 List<Refund__c>  refnd =  CustomerPaymentController.getRefunds(soldOrder.ID);
		Test.stopTest();
        
    }
    
    @isTest
    static void  getOrderIdTest(){
          Order soldOrder = [SELECT Id, AccountId,CustomerPortalUser__c, BillToContactId FROM Order where Name='Sold Order 1'];	
        soldOrder.CustomerPortalUser__c =  UserInfo.getUserId();
        update soldOrder;
     Id SelOrd =   CustomerPaymentController.getOrderId();
    }
    
}