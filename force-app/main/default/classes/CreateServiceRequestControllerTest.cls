@isTest 
public with sharing class CreateServiceRequestControllerTest {
    
    @testSetup
    static void setupData() {

        RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name = 'Turn Financial Transactions Off', Value__c = 'Yes');
        insert turnOffFinancialTrigger;
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        
        Account dwelling1 = utility.createDwellingAccount('Dwelling Account');       
        Account store1 = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
        Store_Configuration__c storeConfig1 = [SELECT id FROM Store_Configuration__c WHERE Store__c = :store1.id ];
        dwelling1.Store_Location__c = store1.Id;         
        insert dwelling1;       
        
        Contact contact1 = new contact ( FirstName='Contact',LastName='1',AccountId=dwelling1.id);
        insert contact1;
        
        Defect_codes__c testDefectCodes = new Defect_codes__c(Name = 'Test Defect Code',
                                                              Code__c = '12345');
        insert testDefectCodes;

        MTO_Source_code__c testMTOSourceCode = new MTO_Source_code__c(Name = 'Test MTO Source Code',
                                                                      SourceCode__c = '54321',
                                                                      Product_Type__c = 'Window');
        insert testMTOSourceCode;

        MTOSourceCode_DefectCode__c testMTOSourceDefectCode = new MTOSourceCode_DefectCode__c(Defect_Code__c = testDefectCodes.Id,
                                                                                              MTO_Source_Code__c = testMTOSourceCode.Id,
                                                                                              Responsibility__c = 'Manufacturing');
        insert testMTOSourceDefectCode;
        
        Product2 masterProduct = new Product2( name = 'master',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Master_Product'));
        Product2 servProduct = new Product2( name = 'service',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        Product2 servProduct2 = new Product2(name = 'service2',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        
        Id pricebookId = Test.getStandardPricebookId();
        system.debug(pricebookId);
        
        List<Product2> products = new List<Product2>{masterProduct,servProduct,servProduct2};
            insert products;
        PricebookEntry pricebookEntryMaster= utility.createPricebookEntry(pricebookId, masterProduct.id);     
        PricebookEntry pricebookEntryServ = utility.createPricebookEntry(pricebookId, servProduct.id);    
        PricebookEntry pricebookEntryServ2 = utility.createPricebookEntry(pricebookId, servProduct2.id);      
        List<PriceBookEntry> pbEntries = new List<PricebookEntry>{pricebookEntryMaster,pricebookEntryServ,pricebookEntryServ2};
            insert pbEntries;
        //utility.createOrderTestRecords();
        //
        
        Service_Product__c servProd1 = new Service_Product__c(Service_Product__c = servProduct.Id, Master_Product__c = masterProduct.Id);
        Service_Product__c servProd2 = new Service_Product__c(Service_Product__c = servProduct2.Id, Master_Product__c = masterProduct.Id);
        List<Service_Product__c> sprods = new List<Service_Product__c>{servProd1,servProd2};
            insert sprods;
        
        
        List<Order> ordersToInsert = new List<Order>();
        Order order =  new Order(   Name='Sold Order 1', 
                                 AccountId = dwelling1.id, 
                                 BilltoContactId = contact1.id,
                                 EffectiveDate= Date.Today(), 
                                 Store_Location__c = store1.Id,                           
                                 Status ='Draft', 
                                 Pricebook2Id = pricebookId
                                );
        insert order;
        //ordersToInsert.add(order);
        Order o =  new Order(Name='Service Order 1', 
                             AccountId = dwelling1.id, 
                             EffectiveDate= Date.Today(),
                             Install_Sched_End_Time__c = Date.today(),
                             Store_Location__c = store1.Id,                               
                             Status ='Draft', 
                             Sold_Order__c = order.Id,
                             BilltoContactId = contact1.id,
                             Service_Type__c = 'Job in Progress',                                             
                             Pricebook2Id = pricebookId
                             
                            );
        ordersToInsert.add(o);
        insert ordersToInsert;
        
        
        OrderItem orderItemMaster = new OrderItem(OrderId = Order.id, PricebookentryId = pricebookEntryMaster.Id, Quantity = 2, UnitPrice = 100, MTO_Source_Code__c = 'D - GBG',
                             Defect_Code__c = 'B - Broken');
        insert orderItemMaster;
        
        Asset asset = new Asset (     Name='Asset1',
                                 Original_Order_Product__c = orderItemMaster.Id,
                                 Product2Id= masterProduct.Id,
                                 AccountId = dwelling1.id,
                                 ContactId = contact1.id,
                                 Variant_Number__c = '1234ABC',
                                 Unit_Wholesale_Cost__c = 200,
                                 Store_Location__c = store1.id,
                                 Quantity = 1,
                                 Price = 100,
                                 Status = 'Installed',
                                 Sold_Order__c = order.Id,
                                 PurchaseDate = Date.Today()
                                );
        
        insert asset;

    }
  
    
    static testmethod void OrderWrapper(){

        Order order = [SELECT Id, AccountId, Sold_Order__c, Install_Sched_End_Time__c, Service_Type__c FROM Order WHERE Name = 'Service Order 1'];
        Asset at = [SELECT id FROM Asset LIMIT 1];

        Test.startTest();

        CreateServiceRequestController.getServiceTypes(order);
        CreateServiceRequestController.getMTOSourceCode();
        CreateServiceRequestController.getDefectCode('Manufacturing','54321');
        CreateServiceRequestController.fetchDefectCodes('Manufacturing', '54321');
        CreateServiceRequestController.getAssetsWithProducts1(order.Id);
//        CreateServiceRequestController.deleteLineItems(order.Id);
        List<Charge__c>charges = [SELECT id, Category__c FROM Charge__c];

        try{

            List<CreateServiceRequestController.ServiceProductWrapper> listOfWrapper = new List<CreateServiceRequestController.ServiceProductWrapper>();
            for (Service_Product__c serviceProd : [SELECT id,Service_Product__c FROM Service_Product__c] ) {
                CreateServiceRequestController.ServiceProductWrapper sv = new CreateServiceRequestController.ServiceProductWrapper(serviceProd,'Manufacturing','',true,at.id);
                sv.isSelected = true;
                listOfWrapper.add(sv);
            }
            CreateServiceRequestController.AssetProductWrapper aw = new CreateServiceRequestController.AssetProductWrapper();
            aw.serviceProducts = listOfWrapper;
            aw.isChecked =true;
            List<CreateServiceRequestController.AssetProductWrapper> awList = new List<CreateServiceRequestController.AssetProductWrapper>();
            awList.add(aw);
            CreateServiceRequestController.saveAll(order.Id,JSON.serialize(awList));
            CreateServiceRequestController.saveCharges(JSON.serialize(charges),new List<SObject>());
            CreateServiceRequestController.saveOrderRecord(JSON.serialize(order),order.AccountId);
            CreateServiceRequestController.fetchGroupDetails(order.Id,JSON.serialize(listOfWrapper));
            CreateServiceRequestController.getPrimaryContact(order.AccountId);
            CreateServiceRequestController.getChargeCategories('Manufacturing');
            CreateServiceRequestController.getWhatWhere('Manufacturing');
        }
        catch(Exception exc) {

        }
        Test.stopTest();
    }
    
}