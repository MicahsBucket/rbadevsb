/*
* @author Jason Flippen
* @date 02/28/2021
* @description Test Class for the following Classes:
*              - ConvertWOAttachmentsToFilesBatch
*/ 
@isTest
public class ConvertWOAttachmentsToFilesBatchTest {

    /*
    * @author Jason Flippen
    * @date 02/28/2021
    * @description: Method to create data used in the Test methods
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @TestSetup
    static void testSetup() {

        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Account storeAccount = TestDataFactory.createStoreAccount('Test Store Account');
        insert storeAccount;

        Store_Configuration__c storeConfig = new Store_Configuration__c(Store__c = storeAccount.Id,
                                                                        Order_Number__c = 1);
        insert storeConfig;

/*
        User user1 = new User (FirstName = 'Tester',
                               LastName = 'TesterTestUser',
                               Email = 'testertestuser@example.com',
                               UserName = 'testertestuser@example.com' + (Math.random() * 1000000).intValue(),
                               CompanyName = 'ABC Company',
                               Alias = 'tuser',
                               ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
                               TimeZoneSidKey = 'America/Chicago',
                               EmailEncodingKey = 'UTF-8',
                               LanguageLocaleKey = 'en_US',
                               LocaleSidKey = 'en_US');
        insert user1;

        String pricebookId = Test.getStandardPricebookId();

        Product2 prod = new Product2();
        prod.Name = 'Test Product 1';
        prod.IsActive = true;
        insert prod;

        List<Skill> mySkills = [SELECT Id FROM Skill];

        PricebookEntry pe = new PricebookEntry();
        pe.IsActive = true;
        pe.Pricebook2Id = pricebookId;
        pe.Product2Id = prod.Id;
        pe.UnitPrice = 45;
        insert pe;
*/
        OperatingHours oh = new OperatingHours();
        oh.Name = 'Test Operating Hours';
        insert oh;

        ServiceTerritory st1 = new ServiceTerritory();
        st1.Name = 'Test Service Territory';
        st1.OperatingHoursId = oh.Id;
        st1.IsActive = true;
        insert st1;

/*
        ServiceResource resource1 = new ServiceResource(Name = user1.LastName,
                                                        RelatedRecordId = user1.Id,
                                                        Retail_Location__c = storeConfig.Id,
                                                        IsActive = true);
        insert resource1;

        ServiceTerritoryMember stMember = new ServiceTerritoryMember (
            OperatingHoursId = oh.Id,
            ServiceResourceId = resource1.Id,
            ServiceTerritoryId = st1.Id,
            EffectiveStartDate = Date.today().addDays(-1)
        );
        insert stMember;

        Service_Territory_Zip_Code__c serviceZipCode1 = new Service_Territory_Zip_Code__c();
        serviceZipCode1.Name = '53211';
        serviceZipCode1.Service_Territory__c = st1.Id;
        serviceZipCode1.WorkType__c = 'Other';

        Service_Territory_Zip_Code__c serviceZipCode2 = new Service_Territory_Zip_Code__c();
        serviceZipCode2.Name = '53132';
        serviceZipCode2.Service_Territory__c = st1.Id;
        serviceZipCode2.WorkType__c = 'Service';

        Service_Territory_Zip_Code__c serviceZipCode3 = new Service_Territory_Zip_Code__c();
        serviceZipCode3.Name = 'L5L5Y7';
        serviceZipCode3.Service_Territory__c = st1.Id;
        serviceZipCode3.WorkType__c = 'Other';
        insert new List<Service_Territory_Zip_Code__c>{serviceZipCode1,serviceZipCode2,serviceZipCode3};

        WorkType wtInstall = new WorkType();
        wtInstall.Name = 'Install';
        wtInstall.EstimatedDuration = 8.0;
        wtInstall.DurationType = 'Hours';

        WorkType wtMeasure = new WorkType();
        wtMeasure.Name = 'Measure';
        wtMeasure.EstimatedDuration = 2.0;
        wtMeasure.DurationType = 'Hours';

        WorkType wtService = new WorkType();
        wtService.Name = 'Service';
        wtService.EstimatedDuration = 1.0;
        wtService.DurationType = 'Hours';

        WorkType wtJobSiteVisit = new WorkType();
        wtJobSiteVisit.Name = 'Job Site Visit';
        wtJobSiteVisit.EstimatedDuration = 1.0;
        wtJobSiteVisit.DurationType = 'Hours';
        insert new List<WorkType>{wtInstall,wtMeasure,wtService,wtJobSiteVisit};

        Order testOrder =  new Order();
        testOrder.Name ='Sold Order 1';
        testOrder.AccountId = storeAccount.Id;
        testOrder.EffectiveDate = Date.Today();
        testOrder.Status = 'Install Complete';
        testOrder.Pricebook2Id = pricebookId;
        testOrder.Customer_Pickup_All__c = FALSE;
        testOrder.Installation_Date__c = Date.today().addDays(-1);
        insert testOrder;

        OrderItem item = new OrderItem();
        item.PricebookEntryId = pe.Id;
        item.OrderId = testOrder.Id;
        item.Product2Id = prod.Id;
        item.Quantity = 2;
        item.UnitPrice = 45;
        insert item;

        RbA_Skills__c mySkill = new RbA_Skills__c();
        myskill.Install_Duration__c = 5;
        mySkill.Tech_Measure_Duration__c = 5;
        insert myskill;        

        Product_Skill__c ps = new Product_Skill__c();
        ps.Product__c = prod.Id;
        ps.FSL_Skill_ID__c = mySkills[0].Id;
        ps.RBA_Skill__c = mySkill.id;
        insert ps;

        Municipality__c m1 = new Municipality__c();
        m1.Name = 'Test Municipality';
        m1.Application_Notes__c = 'Test Notes';
        insert m1;

        Municipality_Contact__c mc1 = new Municipality_Contact__c();
        mc1.Name = 'Test Municipality Contact';
        mc1.Active__c = true;
        mc1.Municipality__c = m1.Id;
        insert mc1;
*/
        WorkOrder installWorkOrder = new WorkOrder();
//        installWorkOrder.Sold_Order__c = testOrder.Id;
//        installWorkOrder.AccountId = testOrder.AccountId;
        installWorkOrder.AccountId = storeAccount.Id;
        installWorkOrder.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Install').getRecordTypeId();
        installWorkOrder.ServiceTerritoryId = st1.Id;
        installWorkOrder.Duration = 1.0;
        installWorkOrder.DurationType ='Hours';
        installWorkOrder.Status = 'To be Scheduled';
        insert installWorkOrder;

        Blob attachmentBody = Blob.valueOf('Test Attachment Body');
        Attachment testAttachment = new Attachment(Name = 'Test Attachment',
                                                   Body = attachmentBody,
                                                   parentId = installWorkOrder.Id);
        insert testAttachment;

    }

    /*
    * @author Jason Flippen
    * @date 02/28/2021
    * @description: Method to test the methods in the Batch Class.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testExecute() {

        Datetime earliestCreatedDateTime = Datetime.newInstance(Date.today().year(), Date.today().month(), 01, 0, 0, 0);

        Id serviceTerritoryId = [SELECT Id FROM ServiceTerritory LIMIT 1].Id;
        Set<Id> serviceTerritoryIdSet = new Set<Id>{serviceTerritoryId};

        Id workOrderId = null;
        String queryString = 'SELECT Id ' +
                             'FROM   WorkOrder ' +
                             'WHERE  ServiceTerritoryId IN :serviceTerritoryIdSet ' +
                             'AND    CreatedDate >= :earliestCreatedDateTime ' +
                             'ORDER BY CreatedDate';

        List<String> resultEmailList = new List<String>{'jane.doe@andersencorp.com'};

        Test.startTest();

            ConvertWOAttachmentsToFilesBatch convertBatch = new ConvertWOAttachmentsToFilesBatch(queryString,
                                                                                                 earliestCreatedDateTime,
                                                                                                 serviceTerritoryIdSet,
                                                                                                 workOrderId,
                                                                                                 resultEmailList);

            Id batchId = Database.executeBatch(convertBatch);

        Test.stopTest();

    }

}