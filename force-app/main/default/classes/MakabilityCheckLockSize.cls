/**
 * @description       : 
 * @author            : mark.rothermal@andersencorp.com
 * @group             : 
 * @last modified on  : 02-24-2021
 * @last modified by  : mark.rothermal@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                            Modification
 * 1.0   12-24-2019   mark.rothermal@andersencorp.com   Initial Version
**/
public without sharing class MakabilityCheckLockSize {    
    
    public static lockSizeResult checkLockSize( MakabilityRestResource.OrderItem request,  Size_Detail_Configuration__c currentConfig, map<string,Product_Field_Control__c> uniKeyToPfcMap ){
        lockSizeResult result = new lockSizeResult();
        boolean makabilityPasses = true;
		result.errMessages = new List<string>();
        ProductConfiguration p = request.ProductConfiguration;
        Boolean configRequiresLocks = false;

        // determine if lock values are required
        if(uniKeyToPfcMap.containsKey(p.productId + '-' + 'Locks/Sash')){
            Product_Field_Control__c pfc = uniKeyToPfcMap.get(p.productId + '-' + 'Locks/Sash');
            if(pfc.required__c){
                configRequiresLocks = true;
            }
        }

        //**********start makability flow*************\\

        if(configRequiresLocks){
            // Lock Makability specific variables
            Boolean checkMaxWidthLocks = false;
            Boolean checkMaxHeightLocks = false;
            Boolean checkMinWidthLocks = false;
            Boolean checkMinHeightLocks = false;
            Boolean continueHeightMakability = true;
            Boolean continueWidthMakability = true;
            Double convertedLockMaxWidth; // = currentConfig.Lock_Max_Width_Inches__c + Constants.fractionConversionMap.get(currentConfig.Lock_Max_Width_Fraction__c);
            Double convertedLockMinWidth; // = currentConfig.Lock_Min_Width_Inches__c + Constants.fractionConversionMap.get(currentConfig.Lock_Min_Width_Fraction__c);
            Double convertedLockMaxHeight; // = currentConfig.Lock_Max_Height_Inches__c + Constants.fractionConversionMap.get(currentConfig.Lock_Max_Height_Fraction__c);
            Double convertedLockMinHeight; // = currentConfig.Lock_Min_Height_Inches__c + Constants.fractionConversionMap.get(currentConfig.Lock_Min_Height_Fraction__c);
            Double convertedHeight = p.heightInches + Constants.fractionConversionMap.get(p.heightFractions);
            Double convertedWidth = p.widthInches + Constants.fractionConversionMap.get(p.widthFractions);  
            
            // check which config values to test with. a product will only have either a height value, or a width value for locks. not both. 
            // half of the fields below will be null.
            if(currentConfig.Lock_Max_Width_Inches__c != null && currentConfig.Lock_Max_Width_Fraction__c != null ){
                convertedLockMaxWidth = currentConfig.Lock_Max_Width_Inches__c + Constants.fractionConversionMap.get(currentConfig.Lock_Max_Width_Fraction__c);
            }
            if(currentConfig.Lock_Min_Width_Inches__c != null && currentConfig.Lock_Min_Width_Fraction__c != null ){
                convertedLockMinWidth = currentConfig.Lock_Min_Width_Inches__c + Constants.fractionConversionMap.get(currentConfig.Lock_Min_Width_Fraction__c);
            }
            if(currentConfig.Lock_Max_Height_Inches__c != null && currentConfig.Lock_Max_Height_Fraction__c != null){
                convertedLockMaxHeight = currentConfig.Lock_Max_Height_Inches__c + Constants.fractionConversionMap.get(currentConfig.Lock_Max_Height_Fraction__c);
            }
            if(currentConfig.Lock_Min_Height_Inches__c != null && currentConfig.Lock_Min_Height_Fraction__c != null ){
                convertedLockMinHeight = currentConfig.Lock_Min_Height_Inches__c + Constants.fractionConversionMap.get(currentConfig.Lock_Min_Height_Fraction__c);
            }
            // initial lock checks.
            if(convertedWidth <= convertedLockMaxWidth ){
                checkMaxWidthLocks = true;
            }
            if(convertedHeight <= convertedLockMaxHeight  ){
                checkMaxHeightLocks = true;
            } 
            if(convertedWidth >= convertedLockMinWidth ){
                checkMinWidthLocks = true;
            }
            if(convertedHeight >= convertedLockMinHeight ){
                checkMinHeightLocks = true;
            }
            // Lock_Max_Width_Locks__c Lock_Min_Height_Locks__c Lock_Min_Width_Locks__c Lock_Max_Height_Locks__c
            if(checkMaxWidthLocks){
                if(p.locks > decimal.valueOf(currentConfig.Lock_Max_Width_Locks__c)){
                    makabilityPasses = false;
                    continueWidthMakability = false;
                    result.errMessages.add('Check Lock Size - You may only have '+currentConfig.Lock_Max_Width_Locks__c+' lock(s) on a unit of this width.');
                }
            }
            if(checkMaxHeightLocks){
                if(p.locks > decimal.valueOf(currentConfig.Lock_Max_Height_Locks__c)){
                    makabilityPasses = false;
                    continueHeightMakability = false;
                    result.errMessages.add('Check Lock Size - You may only have '+currentConfig.Lock_Max_Height_Locks__c+' lock(s) on a unit of this height.');
                }
            }            
            if(checkMinWidthLocks && continueWidthMakability){
                if(p.locks < decimal.valueOf(currentConfig.Lock_Min_Width_Locks__c)){
                    makabilityPasses = false;
                    result.errMessages.add('Check Lock Size - You must have at least '+currentConfig.Lock_Min_Width_Locks__c+' lock(s) for a unit of this width.');
                }
            }
            if(checkMinHeightLocks && continueHeightMakability){
                if(p.locks < decimal.valueOf(currentConfig.Lock_Min_Height_Locks__c)){
                    makabilityPasses = false;
                    result.errMessages.add('Check Lock Size - You must have at least '+currentConfig.Lock_Min_Height_Locks__c+' lock(s) for a unit of this height.');
                }
            }            
        }
       //************end makability flow***************\\
       
        if(makabilityPasses){
            result.productMakable = true;
        //    result.errMessages.add('Check Lock Size - passed');
            
        } else{
            result.productMakable = false;
        }
        return result;
    }


    public class lockSizeResult {
      public  Boolean productMakable {get;set;}
      public  List<String>  errMessages {get;set;}
    }

        

}