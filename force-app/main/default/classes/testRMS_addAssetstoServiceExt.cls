@isTest
private class testRMS_addAssetstoServiceExt 
{
        private static Account rwo;
    private static Asset myAsset1;
    private static Asset myAsset2;
    
    private static void setupData()
    {
         
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        List<Account> dwellingsToInsert = new List<Account>();  
        Account dwelling1 = utility.createDwellingAccount('Dwelling Account');
        Account dwelling2 = utility.createDwellingAccount('Dwelling Account 2');
        Account store1 = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
        Store_Configuration__c storeConfig1 = [SELECT id FROM Store_Configuration__c WHERE Store__c = :store1.id ];
        dwelling1.Store_Location__c = store1.Id;
        dwelling2.Store_Location__c = store1.Id; 
        dwellingsToInsert.add(dwelling1);
        dwellingsToInsert.add(dwelling2);
        insert dwellingsToInsert; 
        
        List<Contact> contactsToInsert = new List<Contact>();       
        Contact contact1 = new contact ( FirstName='Contact',LastName='1',AccountId=dwelling1.id);
        contactsToInsert.add(contact1);
        insert contactsToInsert;
        
        Financial_Account_Number__c finacialAccountNumber1 = new Financial_Account_Number__c (  Store_Configuration__c = storeConfig1.id, Name = '1');
        insert finacialAccountNumber1;
        Financial_Account_Number__c finacialAccountNumber2 = new Financial_Account_Number__c (  Store_Configuration__c = storeConfig1.id, Name = '2');
        insert finacialAccountNumber2;
        
        Financial_Transaction__c finacialTransaction1 = new Financial_Transaction__c(  Store_Configuration__c = storeConfig1.id,
                                                                                     Transaction_Type__c = 'Inventory Received - External Vendor',
                                                                                     Debit_Account_Number__c = finacialAccountNumber1.id,
                                                                                     Credit_Account_Number__c = finacialAccountNumber2.id);
        insert finacialTransaction1;
        
        Product2 masterProduct = new Product2( name = 'master',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Master_Product'), Number_Service_Products__c = 2);
        Product2 servProduct = new Product2( name = 'service',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        Product2 servProduct2 = new Product2(name = 'service2',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        
        Id pricebookId = Test.getStandardPricebookId();
        system.debug(pricebookId);
        
        List<Product2> products = new List<Product2>{masterProduct,servProduct,servProduct2};
            insert products;
        PricebookEntry pricebookEntryMaster= utility.createPricebookEntry(pricebookId, masterProduct.id);     
        PricebookEntry pricebookEntryServ = utility.createPricebookEntry(pricebookId, servProduct.id);    
        PricebookEntry pricebookEntryServ2 = utility.createPricebookEntry(pricebookId, servProduct2.id);      
        List<PriceBookEntry> pbEntries = new List<PricebookEntry>{pricebookEntryMaster,pricebookEntryServ,pricebookEntryServ2};
            insert pbEntries;
        
        Service_Product__c servProd1 = new Service_Product__c(Service_Product__c = servProduct.Id, Master_Product__c = masterProduct.Id);
        Service_Product__c servProd2 = new Service_Product__c(Service_Product__c = servProduct2.Id, Master_Product__c = masterProduct.Id);
        List<Service_Product__c> sprods = new List<Service_Product__c>{servProd1,servProd2};
            insert sprods;
        
        
        
        rwo = new Account();
        rwo.recordTypeId=UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        rwo.Name = 'New Dwelling';
        rwo.Store_Location__c = store1.id;
        insert rwo;
        
        myAsset1 = new Asset();
        myAsset1.Description = 'test';
        myAsset1.Name='Test Asset';
        myAsset1.AccountId = rwo.Id;
        myAsset1.Status = 'Installed';
        myAsset1.recordTypeId = UtilityMethods.retrieveRecordTypeId('Installed_Products', 'Asset');
        myAsset1.Product2Id = masterProduct.Id;
        myAsset1.Quantity = 1;
        myAsset1.Legacy_Asset__c = TRUE;
        insert myAsset1;
        
        myAsset2 = new Asset();
        myAsset2.Description = 'test';
        myAsset2.Name='Test Asset';
        myAsset2.AccountId = rwo.Id;
        myAsset2.Status = 'Installed';
        myAsset2.recordTypeId = UtilityMethods.retrieveRecordTypeId('Installed_Products', 'Asset');
        myAsset2.Product2Id = masterProduct.Id;
        myAsset2.Quantity = 1;
        myAsset2.Legacy_Asset__c = TRUE;
        insert myAsset2;
    }
    
    @isTest static void testRMS_addAssetstoServiceExt()
    {
        setupData();
        
        Test.startTest();
        
        ApexPages.StandardController std = new ApexPages.StandardController(rwo);
        RMS_addAssetstoServiceExt ext = new RMS_addAssetstoServiceExt(std);
        
        Test.stopTest();
        
        System.assertEquals(0, ext.ZERO);
        System.assertEquals(0, ext.removeChildList.size());
        System.assertEquals(2, ext.childList.size());
    }
    
    @isTest static void test_getChildren()
    {
        setupData();
        
        ApexPages.StandardController std = new ApexPages.StandardController(rwo);
        RMS_addAssetstoServiceExt ext = new RMS_addAssetstoServiceExt(std);
        
        Test.startTest();
        
        System.assert(ext.getChildren() instanceof List<Asset>);
        
        Test.stopTest();    
    }
    
    @isTest static void test_initChildRecord()
    {
        setupData();
        
        ApexPages.StandardController std = new ApexPages.StandardController(rwo);
        RMS_addAssetstoServiceExt ext = new RMS_addAssetstoServiceExt(std);
        
        Test.startTest();
        
        System.assert(ext.initChildRecord() instanceof Asset);
        Asset myAsset = (Asset)ext.initChildRecord();
        
        Test.stopTest();
        
        System.assertEquals(rwo.Id, myAsset.AccountId);
    }
}