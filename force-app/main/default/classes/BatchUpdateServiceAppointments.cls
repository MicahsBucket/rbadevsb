/**************************************************************************************
Name: Geetha Katta
Created Date: 5/8/2018
Purpose: To update the EarliestStartTime to 1 day on service appointments
Related Test Class Name/Method: ServiceAppointmentTriggerTest/testBatchUpdateServiceAppointments
**************************************************************************************** */

global class BatchUpdateServiceAppointments implements Database.Batchable<sObject>{

          string status = 'Canceled';

        global BatchUpdateServiceAppointments(){
                   // Batch Constructor
        }
       
        // Start Method
         global  Database.QueryLocator start(Database.BatchableContext BC){
		  string query= 'select Id, Status, EarliestStartTime, Duedate, SchedStartTime from ServiceAppointment where Status != :status';
        return Database.getQueryLocator(query);
        }
      
       // Execute Logic
       global void execute(Database.BatchableContext BC, List<ServiceAppointment> scope){
               List<ServiceAppointment> sas = new List<ServiceAppointment>();

                // string status = 'Canceled';
                 string completed = 'Completed';
                 system.debug('@@@'+completed);
                
              // update EarliestStartTime and Duedate on SA
               for(ServiceAppointment sa : scope){               
                  if(sa.SchedStartTime == null && sa.Status != status ){
                     sa.EarliestStartTime= DateTime.newInstance(system.today()-7, Time.newInstance(0, 0, 0, 0));
                     sa.Duedate= DateTime.newInstance(system.today()+7, Time.newInstance(23, 59, 0, 0));
                     sas.add(sa);
                    }
                 else If (!(sa.Status == completed || sa.Status == status)&& sa.SchedStartTime != null ){
                     DateTime schdate = sa.SchedStartTime;
                     Date myschDate = date.newinstance(schdate.year(), schdate.month(), schdate.day());
                     system.debug('!!!!'+myschDate);
                     sa.EarliestStartTime= Datetime.newInstance(myschDate-7, Time.newInstance(0, 0, 0, 0));
                     sa.Duedate= Datetime.newInstance(myschDate+7, Time.newInstance(23, 59, 0, 0));
                     sas.add(sa);
                    }
               
               }
           update sas;
    }
     
       global void finish(Database.BatchableContext BC){
       // Logic to be Executed at finish,send email after job is executed
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, 
                           JobItemsProcessed,
                           TotalJobItems
                           FROM AsyncApexJob
                           WHERE Id = :BC.getJobId()];
                           
      }
    }