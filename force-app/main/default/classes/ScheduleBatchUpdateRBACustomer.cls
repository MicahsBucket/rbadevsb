/**
  * @author Ramya Bangalore
  * @Description This SchedulerBatch class is setting time for the "batchUpdateRBACustomer" -GroundForce App
  * 
  * 
  */
global class ScheduleBatchUpdateRBACustomer implements Schedulable{

    // Fetch the below string from Custom Settings
    public static String sched = '0 00 00 * * ?';  //Every Day at Midnight 

    public static String scheduleMe() {
        ScheduleBatchUpdateRBACustomer SC = new ScheduleBatchUpdateRBACustomer(); 
        return System.schedule('Update prevRBACustomer', sched, SC);
    }

    global void execute(SchedulableContext sc) {

        batchUpdateRBACustomer b1 = new batchUpdateRBACustomer();
        ID batchprocessid = Database.executeBatch(b1,20);           
    }
}