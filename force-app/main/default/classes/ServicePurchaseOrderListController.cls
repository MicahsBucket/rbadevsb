/*
* @author Jason Flippen
* @date 02/06/2020 
* @description Class to provide functionality for the servicePurchaseOrderList LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - ServicePurchaseOrderListControllerTest
*/
public with sharing class ServicePurchaseOrderListController {

    private static final String PURCHASE_ORDER_TYPE_COST = 'Cost';
    private static final String PURCHASE_ORDER_TYPE_SERVICE = 'Service';

    /*
    * @author Jason Flippen
    * @date 02/06/2020
    * @description Method to return a wrapped Purchase Order data
    */
    @AuraEnabled(cacheable=true)
    public static List<PurchaseOrderWrapper> getPurchaseOrderData(Id orderId, String recordPageUrl) {

        List<PurchaseOrderWrapper> wrapperList = new List<PurchaseOrderWrapper>();

        // Are we in a Community?
        Boolean sourceIsCommunity = true;
        String networkId = Network.getNetworkId();
        if (networkId == null) {
            sourceIsCommunity = false;
        }

        for (Purchase_Order__c po : [SELECT Id,
                                            Name,
                                            RecordTypeId,
                                            RecordType.Name,
                                            Status__c,
                                            Estimated_Ship_Date__c,
                                            Total__c
                                     FROM   Purchase_Order__c
                                     WHERE  Order__c = :orderId]) {
            
            PurchaseOrderWrapper wrapper = new PurchaseOrderWrapper();
            wrapper.id = po.Id;
            wrapper.name = po.Name;
            String recordUrl = recordPageUrl + po.Id;
            if (sourceIsCommunity == false) {
                recordUrl += '/view';
            }
            wrapper.purchaseOrderUrl = recordUrl;
            wrapper.recordTypeId = po.RecordTypeId;
            wrapper.recordTypeName = po.RecordType.Name;
            wrapper.status = po.Status__c;
            wrapper.estimatedShipDate = po.Estimated_Ship_Date__c;
            wrapper.amount = po.Total__c;
            wrapperList.add(wrapper);
        }
        
        return wrapperList;

    }

    /*
    * @author Jason Flippen
    * @date 02/14/2020
    * @description Method to return (wrapped) List of Vendors.
    */
    @AuraEnabled
    public static List<VendorWrapper> getVendorList(Id orderId) {
        
        List<VendorWrapper> wrapperList = new List<VendorWrapper>();

        // Grab some detail from the Order.
        Order order = [SELECT Id,
                              Store_Location__c,
                              (
                                  SELECT Id,
                                         Product2Id
                                  FROM   OrderItems
                                  WHERE  Product2.IsActive = true
                                  AND    Purchase_Order__c = null
                                  AND    Status__c != 'Cancelled'
                              )
                       FROM   Order
                       WHERE  Id = :orderId];

        if (!order.OrderItems.isEmpty() && String.isNotBlank(order.Store_Location__c)) { 
        
            // Grab a Set of Product Ids related to the Order.
            Set<Id> productIdSet = new Set<Id>();
            for (OrderItem oi : order.OrderItems) {
                productIdSet.add(oi.Product2Id);
            }

            // Build Maps and Sets to create Vendor Wrapper records.
            Map<Id,Map<String,List<Charge__c>>> productResponsibilityChargeListMap = getProductResponsibilityChargeListMap(orderId, productIdSet);
            Map<Id,String> eligibleVendorMap = getEligibleVendorMap(order.Store_Location__c);
            Map<Id,Set<Id>> vendorProductSetMap = getVendorProductSetMap(productIdSet, eligibleVendorMap.keySet());

            if (!vendorProductSetMap.isEmpty()) {
                
                for (Id vendorId : vendorProductSetMap.keySet()) {

                    // Make sure the Vendor has Products associated before adding it to our Wrapper List.
                    if (!vendorProductSetMap.get(vendorId).isEmpty()) {

                        String vendorName = eligibleVendorMap.get(vendorId);

                        VendorWrapper vendorWrapper = new VendorWrapper();
                        vendorWrapper.label = vendorName;
                        vendorWrapper.value = vendorId;
                        wrapperList.add(vendorWrapper);

                        // Iterate through the Products related to this Vendor and add
                        // them to the "orderItemList" property of the VendorWrapper.
                        for (Id productId : vendorProductSetMap.get(vendorId)) {

                            // Add Responsibilities to the VendorWrapper List.
                            for (String responsibility : productResponsibilityChargeListMap.get(productId).keySet()) {

                                // Grab existing VendorWrapper record if one exists and set it to our "vendorWrapper" variable,
                                // otherwise create a new VendorWrapper record and set its values.
                                ResponsibilityWrapper responsibilityWrapper;
                                ResponsibilityWrapper existingResponsibilityWrapper = getExistingResponsiblityWrapper(vendorWrapper.responsibilityList, responsibility);
                                if (existingResponsibilityWrapper != null) {
                                    responsibilityWrapper = existingResponsibilityWrapper;
                                }
                                else {
                                    responsibilityWrapper = new ResponsibilityWrapper();
                                    responsibilityWrapper.label = responsibility;
                                    responsibilityWrapper.value = responsibility;
                                    vendorWrapper.responsibilityList.add(responsibilityWrapper);
                                }

                                // Add the Charges to the ResponsibilityWrapperList.
                                for (Charge__c charge : productResponsibilityChargeListMap.get(productId).get(responsibility)) {
                                    
                                    ChargeWrapper chargeWrapper = new ChargeWrapper();
                                    chargeWrapper.id = charge.Id;
                                    chargeWrapper.category = charge.Category__c; // aka Source of Defect
                                    chargeWrapper.chargeCostTo = charge.Charge_Cost_To__c; // aka Responsibility
                                    chargeWrapper.installedProductId = charge.Installed_Product_Id__c;
                                    chargeWrapper.installedProductName = charge.Installed_Product__c;
                                    chargeWrapper.serviceRequestId = charge.Service_Request__c;
                                    chargeWrapper.serviceProductId = charge.Service_Product__c;
                                    chargeWrapper.serviceProductName = charge.Service_Product__r.Product_Name__c;
                                    chargeWrapper.serviceProductAssetId = charge.Service_Product__r.Sold_Order_Product_Asset__c;
                                    chargeWrapper.serviceProductAssetOriginalOrderProductId = charge.Service_Product__r.Sold_Order_Product_Asset__r.Original_Order_Product__c;
                                    chargeWrapper.serviceProductPurchaseOrderId = charge.Service_Product__r.Purchase_Order__c;
                                    chargeWrapper.serviceProductStoreLocationId = charge.Service_Product__r.Order.Store_Location__c;
                                    chargeWrapper.variantNumber = charge.Variant_Number__c;
                                    chargeWrapper.vendorId = charge.Service_Product__r.PricebookEntry.Product2.Vendor__c;
                                    chargeWrapper.whatWhere = charge.What_Where__c;

                                    responsibilityWrapper.chargeList.add(chargeWrapper);

                                }

                            }
                            
                        }

                    }

                }

            }

        }
        System.debug('***** wrapperList: ' + wrapperList);

        return wrapperList;

    }

    /*
    * @author Jason Flippen
    * @date 03/05/2021
    * @description Method to return a Product-to-Responsibility-to-Charge (List) Map.
    */
    private static Map<Id,Map<String,List<Charge__c>>> getProductResponsibilityChargeListMap(Id orderId, Set<Id> productIdSet) {

        Map<Id,Map<String,List<Charge__c>>> productResponsibilityChargeListMap = new Map<Id,Map<String,List<Charge__c>>>();

        // Build the Product-to-Responsibility-to-Charge (List) Map.
        for (Charge__c c : [SELECT Id,
                                   Category__c,
                                   Charge_Cost_To__c,
                                   Installed_Product__c,
                                   Installed_Product_Id__c,
                                   Service_Request__c,
                                   Service_Product__c,
                                   Service_Product__r.Order.Store_Location__c,
                                   Service_Product__r.PricebookEntry.Product2Id,
                                   Service_Product__r.PricebookEntry.Product2.Vendor__c,
                                   Service_Product__r.Product_Name__c,
                                   Service_Product__r.Purchase_Order__c,
                                   Service_Product__r.Purchase_Order__r.Charge_Cost_To__c,
                                   Service_Product__r.Sold_Order_Product_Asset__c,
                                   Service_Product__r.Sold_Order_Product_Asset__r.Original_Order_Product__c,
                                   Service_Product__r.Status__c,
                                   Variant_Number__c,
                                   What_Where__c
                            FROM   Charge__c
                            WHERE  Service_Request__c = :orderId
                            AND    Service_Product__c != null
                            AND    Service_Product__r.PricebookEntry.Product2Id IN :productIdSet
                            AND    Service_Product__r.Purchase_Order__c = null
                            AND    Service_Product__r.Status__c != 'Cancelled']) {
            
            if (!productResponsibilityChargeListMap.containsKey(c.Service_Product__r.PricebookEntry.Product2Id)) {
                productResponsibilityChargeListMap.put(c.Service_Product__r.PricebookEntry.Product2Id, new Map<String,List<Charge__c>>());
            }

            if (!productResponsibilityChargeListMap.get(c.Service_Product__r.PricebookEntry.Product2Id).containsKey(c.Charge_Cost_To__c)) {
                productResponsibilityChargeListMap.get(c.Service_Product__r.PricebookEntry.Product2Id).put(c.Charge_Cost_To__c, new List<Charge__c>());
            }

/*
            if (c.Service_Product__r.Purchase_Order__c == null || 
                (c.Service_Product__r.Purchase_Order__c != null && c.Charge_Cost_To__c != c.Service_Product__r.Purchase_Order__r.Charge_Cost_To__c)) {
                productResponsibilityChargeListMap.get(c.Service_Product__r.PricebookEntry.Product2Id).get(c.Charge_Cost_To__c).add(c);
            }
*/
            productResponsibilityChargeListMap.get(c.Service_Product__r.PricebookEntry.Product2Id).get(c.Charge_Cost_To__c).add(c);

        }

        return productResponsibilityChargeListMap;

    }

    /*
    * @author Jason Flippen
    * @date 03/05/2021
    * @description Method to return a Map of Vendors eligible for Service POs.
    */
    private static Map<Id,String> getEligibleVendorMap(Id storeLocationId) {

        Map<Id,String> eligibleVendorMap = new Map<Id,String>();

        // Add Vendors to our Eligible Vendor Map if
        // available the Store the Order is connected to.
        for (Store_Vendor__c storeVendor : [SELECT Store__c,
                                                   Vendor__c,
                                                   Vendor__r.Name
                                            FROM   Store_Vendor__c
                                            WHERE  Store__c = :storeLocationId
                                            AND    Vendor__c != null
                                            AND    Vendor__r.Active__c = true]) {
            eligibleVendorMap.put(storeVendor.Vendor__c, storeVendor.Vendor__r.Name);
        }

        return eligibleVendorMap;

    }

    /*
    * @author Jason Flippen
    * @date 03/05/2021
    * @description Method to return a Vendor-to-Product Map.
    */
    private static Map<Id,Set<Id>> getVendorProductSetMap(Set<Id> productIdSet, Set<Id> eligibleVendorIdSet) {

        Map<Id,Set<Id>> vendorProductSetMap = new Map<Id,Set<Id>>();

        // Build a Map of unique Vendors related to the Products.
        for (Vendor_Product__c vendorProduct : [SELECT Product__c,
                                                       Vendor__c
                                                FROM   Vendor_Product__c
                                                WHERE  Vendor__c IN :eligibleVendorIdSet
                                                AND    Product__c IN :productIdSet
                                                AND    Product__r.Service_PO__c = true]) {
            
            if (!vendorProductSetMap.containsKey(vendorProduct.Vendor__c)) {
                vendorProductSetMap.put(vendorProduct.Vendor__c, new Set<Id>());
            }
            vendorProductSetMap.get(vendorProduct.Vendor__c).add(vendorProduct.Product__c);

        }
        
        return vendorProductSetMap;

    }

    /*
    * @author Jason Flippen
    * @date 03/08/2021
    * @description Method to return an existing ResponsibilityWrapper record (if found).
    */
    private static ResponsibilityWrapper getExistingResponsiblityWrapper(List<ResponsibilityWrapper> responsibilityWrapperList, String responsibilityValue) {

        ResponsibilityWrapper existingResponsiblityWrapper = null;

        // If we don't have any records in the ResponsibilityWrapper List we'll be
        // creating a new ResponsibilityWrapper record, so skip over and return NULL.
        if (!responsibilityWrapperList.isEmpty()) {

            // Loop through the ResponsibilityWrapper List and determine if
            // this Responsibility already exists or a new one is needed.
            for (ResponsibilityWrapper responsibility : responsibilityWrapperList) {
                if (responsibility.value == responsibilityValue) {
                    existingResponsiblityWrapper = responsibility;
                    break;
                }
            }

        }

        return existingResponsiblityWrapper;

    }

    /*
    * @author Jason Flippen
    * @date 06/22/2020
    * @description Method to return a new instance of a Cost Purchase Order.
    */
    @AuraEnabled(cacheable=true)
    public static Purchase_Order__c getNewCostPOData(Id orderId) {

        Purchase_Order__c newCostPO = new Purchase_Order__c();

        User currentUser = [SELECT Id,
                                   Default_Store_Location__c
                            FROM   User
                            WHERE  Id = :UserInfo.getUserId()];
        
        String storeLocationName = currentUser.Default_Store_Location__c;

        Account storeLocation = null;
        if (String.isNotBlank(storeLocationName)) {
            storeLocation = [SELECT Id
                             FROM   Account
                             WHERE  Name = :storeLocationName];
        }

        newCostPO.Comments__c = null;
        newCostPO.Date__c = Date.today();
        newCostPO.Order__c = orderId;
        newCostPO.RecordTypeId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Cost_Purchase_Order').getRecordTypeId();
        newCostPO.Reference__c = null;
        newCostPO.Requested_Ship_Date__c = null;
        newCostPO.Status__c = 'In Process';
        newCostPO.Store_Location__c = storeLocation.Id;
        newCostPO.Tax__c = 0;
        newCostPO.Vendor__c = null;

        return newCostPO;

    }

    /*
    * @author Jason Flippen
    * @date 06/22/2020
    * @description Method to create the new Cost Purchase Order.
    */
    @AuraEnabled
    public static Map<String,String> createCostPO(Purchase_Order__c newPurchaseOrder) {

        Map<String,String> resultMap = new Map<String,String>();

        try {

            newPurchaseOrder.Name = 'PlaceholderPOName';
            newPurchaseOrder.Store_Abbreviation__c = getStoreAbbreviation(newPurchaseOrder, PURCHASE_ORDER_TYPE_COST);
            insert newPurchaseOrder;

            resultMap.put('New Cost PO Success', newPurchaseOrder.Id);

        }
        catch (Exception ex) {
            System.debug('***** The following exception occurred in the ServicePurchaseOrderListController in the createCostPO method inserting records:' + ex);
            resultMap.put(ex.getMessage(), '');
        }

        return resultMap;

    }

    /*
    * @author Jason Flippen
    * @date 02/12/2020
    * @description Method to create the new Service Purchase Order.
    */
    @AuraEnabled
    public static Map<String,String> createServicePO(List<ChargeWrapper> chargeWrapperList, Id selectedVendorId, Id orderId) {

        Map<String,String> resultMap = new Map<String,String>();

        // Create a savepoint prior to making any changes so
        // we can roll them back if we encounter an error.
//        Savepoint savePoint = Database.setSavepoint();

        Map<String,Id> existingChargePOMap = getExistingChargePOMap(orderId);

        // Get a Map of Existing POs using the PO Ids from the existingChargePOMap.
        Map<Id,Purchase_Order__c> existingPOMap = new Map<Id,Purchase_Order__c>([SELECT Id,
                                                                                        Vendor__c,
                                                                                        Order__c,
                                                                                        Store_Location__c,
                                                                                        Status__c,
                                                                                        Charge_Cost_To__c,
                                                                                        Store_Abbreviation__c,
                                                                                        RecordTypeId
                                                                                 FROM   Purchase_Order__c
                                                                                 WHERE  Id IN : existingChargePOMap.values()]);

        // Build our Maps prior to looping through the selected Charge records.
        Map<Id,OrderItem> serviceProductMap = getServiceProductMap(chargeWrapperList);
        Map<Id,Id> productToVendorMap = getProductToVendorMap(chargeWrapperList);

        Map<String,Purchase_Order__c> chargeVendorNewPOMap = new Map<String,Purchase_Order__c>();
        Map<OrderItem,String> orderItemChargeVendorMap = new Map<OrderItem,String>();

        // Initialize variables.
        Id purchaseOrderId = null;
        Set<Id> selectedServiceProdIdSet = new Set<Id>();
        Set<Id> selectedAssetIdSet = new Set<Id>();
        List<OrderItem> updateOrderItemList = new List<OrderItem>();

        // Loop through all of the selected Charge records.
        for (ChargeWrapper cw : chargeWrapperList) {

            // Has this charge already been selected for this product?  If so, return an error and exit the loop.
            if (selectedServiceProdIdSet.contains(cw.serviceProductId)) {
                resultMap.put(RMS_ErrorMessages.DUPLICATE_SERVICE_PRODUCTS + cw.serviceProductName, '');
                break;
            }
            else {
                selectedServiceProdIdSet.add(cw.serviceProductId);
            }

            // Check to see if there are duplicate assets being used for selected
            // product and if so, throw an error and exit the loop.
            OrderItem currentServiceProd = serviceProductMap.get(cw.serviceProductId);
            if (currentServiceProd.Product_Name__c == 'Complete Unit') {
                if (selectedAssetIdSet.contains(currentServiceProd.Sold_Order_Product_Asset__c)) {
                    resultMap.put(RMS_ErrorMessages.DUPLICATE_ASSETS, '');
                    break;
                }
                else {
                    selectedAssetIdSet.add(currentServiceProd.Sold_Order_Product_Asset__c);
                }
            }

            // Attempt to retrieve the existing Purchase Order (Id) from the Charge-to-Vendor map.
/*
            String vendorId = '';
            if (!productToVendorMap.isEmpty() && productToVendorMap.containsKey(cw.installedProductId)) {
                vendorId = productToVendorMap.get(cw.installedProductId);
            }
System.debug('***** vendorId: ' + vendorId);
*/

            // If we found an existing Purchase Order then add the Service Product
            // to the Update List to be updated with the Purchase Order.
            if (!existingChargePOMap.isEmpty() &&
                existingChargePOMap.containsKey(cw.chargeCostTo + selectedVendorId)) {

                purchaseOrderId = existingChargePOMap.get(cw.chargeCostTo + selectedVendorId);

                currentServiceProd.Purchase_Order__c = purchaseOrderId;
                currentServiceProd.Has_PO__c = true;
                
                // Set the remake flag on the order item if the charge cost to is local.
                Boolean chargeToCost = false;
                Purchase_Order__c existingPO = existingPOMap.get(purchaseOrderId);
                if (existingPO.Charge_Cost_To__c == 'Retailer') {
                    chargeToCost = true;
                }
                currentServiceProd.Remake__c = chargeToCost;
                
                // Set the order item description.
                String itemDescription = '';
                if (currentServiceProd.Sold_Order_Product_Asset__r.Product_Name__c != null) {
                    itemDescription = currentServiceProd.Sold_Order_Product_Asset__r.Product_Name__c + ' - ';
                }
                if (currentServiceProd.Sold_Order_Product_Asset__r.Variant_Number__c != null) {
                    itemDescription += currentServiceProd.Sold_Order_Product_Asset__r.Variant_Number__c;
                }
                currentServiceProd.Description = itemDescription;
                
                updateOrderItemList.add(currentServiceProd);

            }
            else if (!chargeVendorNewPOMap.isEmpty() &&
                     chargeVendorNewPOMap.containsKey(cw.chargeCostTo + selectedVendorId)) {

                // Check if there is a new PO already pending for this charge cost to and vendor.

                // Put the order item in a map to the charge cost to and vendor
//                orderItemChargeVendorMap.put(currentServiceProd, cw.chargeCostTo + productToVendorMap.get(cw.installedProductId));
                orderItemChargeVendorMap.put(currentServiceProd, cw.chargeCostTo + selectedVendorId);
            }
            else {

                // There was not a pending PO already so now create one and put it in the newPO map.
                Purchase_Order__c newPurchaseOrder = new Purchase_Order__c();
//                newPurchaseOrder.Vendor__c = ProductToVendorMap.get(cw.installedProductId);
                newPurchaseOrder.Name = 'PlaceholderPOName';
                newPurchaseOrder.Vendor__c = selectedVendorId;
                newPurchaseOrder.Order__c = orderId;
                newPurchaseOrder.Store_Location__c = cw.serviceProductStoreLocationId;
                newPurchaseOrder.Status__c = 'In Process';
                newPurchaseOrder.Charge_Cost_To__c = cw.chargeCostTo;
                newPurchaseOrder.Store_Abbreviation__c = getStoreAbbreviation(newPurchaseOrder, PURCHASE_ORDER_TYPE_SERVICE);
                newPurchaseOrder.Tax__c = 0.00;
                newPurchaseOrder.RecordTypeId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Service_Purchase_Order').getRecordTypeId();

                // Now put the new purchase order in the charge vendor map.
//                chargeVendorNewPOMap.put(cw.chargeCostTo + productToVendorMap.get(cw.installedProductId), newPO);
                chargeVendorNewPOMap.put(cw.chargeCostTo + selectedVendorId, newPurchaseOrder);

                // Pull the order item from the serviceproduct map and put it in a different map with charge vendor.
//                OrderItem updateOrderItem = serviceProductMap.get(cw.serviceProductId);
//                orderItemChargeVendorMap.put(currentServiceProd, cw.chargeCostTo + productToVendorMap.get(cw.installedProductId));
                orderItemChargeVendorMap.put(currentServiceProd, cw.chargeCostTo + selectedVendorId);
            }

        }

        // If we have a value in the Map that means we encountered
        // an issue, so there is no need to continue processing.
        if (resultMap.isEmpty()) {

            try {

                Id newPurchaseOrderId = null;

                // If we have new POs to insert, insert them.
                if (!chargeVendorNewPOMap.isEmpty()) {
                    insert chargeVendorNewPOMap.values();
                }

                // Then loop through the order items for  a new po
                for (OrderItem oi : orderItemChargeVendorMap.keyset()) {
                    // Retrieve the charge vendor for the order item and then use that to 
                    // retrieve the new PO
                    String chargeVendor = orderItemChargeVendorMap.get(oi);
                    Purchase_Order__c purchaseOrder = chargeVendorNewPOMap.get(chargeVendor);

                    // Set the order item purchase order lookup to the po id
                    oi.Purchase_Order__c = purchaseOrder.Id;
                    oi.Has_PO__c = true;
                    
                    // Set the remake flag on the order item if the charge cost to is local
                    Boolean remake = false;
                    if (purchaseOrder.Charge_Cost_To__c == 'Retailer') {
                        remake = true;
                    }
                    oi.Remake__c = remake;

                    // Set the order item description
                    String orderItemDescription = '';
                    if (oi.Sold_Order_Product_Asset__r.Product_Name__c != null) {
                        orderItemDescription = oi.Sold_Order_Product_Asset__r.Product_Name__c + ' - ';
                    }
                    if (oi.Sold_Order_Product_Asset__r.Variant_Number__c != null) {
                        orderItemDescription +=  oi.Sold_Order_Product_Asset__r.Variant_Number__c;
                    }
                    oi.Description = orderItemDescription;

                    newPurchaseOrderId = oi.Purchase_Order__c;
                    updateOrderItemList.add(oi);
                }

                if (newPurchaseOrderId == null) {
                    newPurchaseOrderId = purchaseOrderId;
                }

                if (!updateOrderItemList.isEmpty()) {
                    upsert updateOrderItemList;
                }

                resultMap.put('New Service PO Success', newPurchaseOrderId);

            }
            catch (Exception ex) {

                System.debug('***** The following exception occurred in the NewServicePurchaseOrderController in the createServicePO method inserting records:' + ex);

//                // Rollback the changes and return the error message.
//                Database.rollback(savePoint);
//                resultMap.put(RMS_ErrorMessages.SAVE_PO_EXCEPTION, '');
                resultMap.put(ex.getMessage(), '');
            }

        }

        return resultMap;

    }

    /*
    * @author Jason Flippen
    * @date 02/12/2020
    * @description Method to build an Existing Charge-to-Purchase Order Map.
    */
    private static Map<String,Id> getExistingChargePOMap(Id orderId) {

        Map<String,Id> returnMap = new Map<String,Id>();

        // Retrieve data from the Service Request record.
        for (Charge__c c : [SELECT Id,
                                   Service_Product__c,
                                   Service_Product__r.Purchase_Order__c,
                                   Service_Product__r.Purchase_Order__r.Charge_Cost_To__c,
                                   Service_Product__r.Purchase_Order__r.Status__c,
                                   Service_Product__r.Purchase_Order__r.Vendor__c
                            FROM   Charge__c
                            WHERE  Service_Request__c = :orderId
                            AND    Service_Product__c != null
                            AND    Service_Product__r.Purchase_Order__c != null
                            AND    Service_Product__r.Status__c != 'Cancelled'
                            AND    Service_Product__r.Purchase_Order__r.Status__c = 'In Process']) {

            String mapKey = c.Service_Product__r.Purchase_Order__r.Charge_Cost_To__c + c.Service_Product__r.Purchase_Order__r.Vendor__c;
            returnMap.put(mapKey, c.Service_Product__r.Purchase_Order__c);

        }

        return returnMap;

    }

    /*
    * @author Jason Flippen
    * @date 02/12/2020
    * @description Method to get a Service Product Map.
    */
    private static Map<Id,OrderItem> getServiceProductMap(List<ChargeWrapper> chargeWrapperList) {

        Map<Id,OrderItem> returnMap = new Map<Id,OrderItem>();

        Set<Id> serviceProductIdSet = new Set<Id>();
        for (chargeWrapper cw : chargeWrapperList) {
            serviceProductIdSet.add(cw.serviceProductId);
        }

        if (!serviceProductIdSet.isEmpty()) {

            for (OrderItem oi : [SELECT Id,
                                        Purchase_Order__c,
                                        Sold_Order_Product_Asset__c,
                                        Sold_Order_Product_Asset__r.Variant_Number__c,
                                        Sold_Order_Product_Asset__r.Product_Name__c,
                                        Product_Name__c 
                                 FROM   OrderItem
                                 WHERE  Id IN :serviceProductIdSet])
            {
                returnMap.put(oi.Id,oi);
            }

        }

        return returnMap;

    }

    /*
    * @author Jason Flippen
    * @date 02/12/2020
    * @description Method to build a Product-to-Vendor Map from a List of Charge records.
    */
    private static Map<Id,Id> getProductToVendorMap(List<ChargeWrapper> chargeWrapperList) {

        Map<Id,Id> returnMap = new Map<Id,Id>();

        Set<Id> installedProductIdSet = new Set<Id>();
        for (chargeWrapper cw : chargeWrapperList) {
            installedProductIdSet.add(cw.serviceProductAssetOriginalOrderProductId);
        }
        System.debug('***** installedProductIdSet: ' + installedProductIdSet);

        if (!installedProductIdSet.isEmpty()) {

            for (OrderItem oi : [SELECT Id,
                                        PricebookEntry.Product2Id,
                                        Purchase_Order__r.Vendor__c,
                                        Product_Name__c
                                 FROM   OrderItem
                                 WHERE  Id IN :InstalledProductIdSet
                                 AND    PricebookEntry.Product2Id != null
                                 AND    Purchase_Order__r.Vendor__c != null]) {
                returnMap.put(oi.PricebookEntry.Product2Id,oi.Purchase_Order__r.Vendor__c);
            }

        }

        return returnMap;

    }

    /*
    * @author Jason Flippen
    * @date 02/12/2020
    * @description Method to retrieve a concatenated Store Abbreviation.
    */
    private static String getStoreAbbreviation(Purchase_Order__c newPurchaseOrder, String purchaseOrderType) {
        
        String returnValue = '';

        // Retrive the current user
        User currentUser = [SELECT User_Abbreviation__c FROM User WHERE Id =: UserInfo.getUserId()];
        
        // If a test is running just set the user's abbreviation, otherwise get it from the current user.
        String userAbbreviation = 'X';
        if (Test.isRunningTest() == true) {
            userAbbreviation = 'A';
        }
        else if (String.isNotBlank(currentUser.User_Abbreviation__c)) {
            userAbbreviation = currentUser.User_Abbreviation__c;
        }

        // Set the type of PO
//        String typeAbbreviation = (costPurchaseOrder) ? 'C' : (servicePurchaseOrder) ? 'S' : 'P';
        String typeAbbreviation = 'S';
        if (purchaseOrderType == PURCHASE_ORDER_TYPE_COST) {
            typeAbbreviation = 'C';
        }
        
        String storeAbbreviation = 'X';

        // Query the Store Location (Account) to retrieve the store abbreviation.
        Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id,
                                                                 Active_Store_Configuration__c,
                                                                 Active_Store_Configuration__r.Store_Abbreviation__c
                                                          FROM   Account
                                                          WHERE  Id = :newPurchaseOrder.Store_Location__c
                                                          AND    Active_Store_Configuration__c != null]);
        
        if (!accountMap.isEmpty()) {

            // Update the store abbreviation to the value from the Store Location (Account).
            storeAbbreviation = '';
            Account account = accountMap.values()[0];
            if (account.Active_Store_Configuration__r.Store_Abbreviation__c != null) {
                storeAbbreviation = account.Active_Store_Configuration__r.Store_Abbreviation__c;
            }

        }

        // Set the return value to the store abbreviation, user abbreviation, and type abbreviation (concatenated).
        returnValue = storeAbbreviation + userAbbreviation + typeAbbreviation;

        return returnValue;

    }


/** Wrapper Classes **/


    /*
    * @author Jason Flippen
    * @date 02/06/2020
    * @description Wrapper Class for a Purchase Order.
    */
    @TestVisible
    public class PurchaseOrderWrapper {

        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String name {get;set;}

        @AuraEnabled
        public String purchaseOrderUrl {get;set;}

        @AuraEnabled
        public String recordTypeId {get;set;}

        @AuraEnabled
        public String recordTypeName {get;set;}

        @AuraEnabled
        public Decimal amount {get;set;}

        @AuraEnabled
        public String status {get;set;}

        @AuraEnabled
        public Date estimatedShipDate {get;set;}

    }

    /*
    * @author Jason Flippen
    * @date 02/14/2020
    * @description Wrapper Class for Vendors to be used in a Combobox.
    */
    @TestVisible
    public class VendorWrapper {

        @AuraEnabled
        public String label {get;set;}

        @AuraEnabled
        public String value {get;set;}

        @AuraEnabled
        public List<ResponsibilityWrapper> responsibilityList {get;set;}

        public VendorWrapper() {
            this.responsibilityList = new List<ResponsibilityWrapper>();
        }

    }

    /*
    * @author Jason Flippen
    * @date 03/19/2020
    * @description Wrapper Class for Responsibilities to be used in a Combobox.
    */
    @TestVisible
    public class ResponsibilityWrapper {

        @AuraEnabled
        public String label {get;set;}

        @AuraEnabled
        public String value {get;set;}

        @AuraEnabled
        public List<ChargeWrapper> chargeList {get;set;}

        ResponsibilityWrapper () {
            this.chargeList = new List<ChargeWrapper>();
        }

    }

    /*
    * @author Jason Flippen
    * @date 02/11/2020
    * @description Wrapper Class for a Charge.
    */
    @TestVisible
    public class ChargeWrapper {

        @AuraEnabled
        public String id {get;set;}
        
        @AuraEnabled
        public String category {get;set;}
        
        @AuraEnabled
        public String chargeCostTo {get;set;}

        @AuraEnabled
        public String installedProductId {get;set;}

        @AuraEnabled
        public String installedProductName {get;set;}

        @AuraEnabled
        public String serviceRequestId {get;set;}

        @AuraEnabled
        public String serviceProductId {get;set;}

        @AuraEnabled
        public String serviceProductName {get;set;}

        @AuraEnabled
        public String serviceProductAssetId {get;set;}

        @AuraEnabled
        public String serviceProductAssetOriginalOrderProductId {get;set;}

        @AuraEnabled
        public String serviceProductPurchaseOrderId {get;set;}

        @AuraEnabled
        public String serviceProductStoreLocationId {get;set;}

        @AuraEnabled
        public String variantNumber {get;set;}

        @AuraEnabled
        public String vendorId {get;set;}

        @AuraEnabled
        public String whatWhere {get;set;}

    }

}