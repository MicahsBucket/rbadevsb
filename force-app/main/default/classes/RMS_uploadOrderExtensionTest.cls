@isTest
private class RMS_uploadOrderExtensionTest {

	@testSetup static void setup() {

		testDataFactory.setUpConfigs();

		Account myStore = testDataFactory.createStoreAccount('My Local Windows Store');
		insert myStore;

		Store_Configuration__c myStoreConfiguration = new Store_Configuration__c();
		myStoreConfiguration.Store__c = myStore.id;
		myStoreConfiguration.Order_Number__c = 12345;
		myStoreConfiguration.Tech_Measure_Work_Order_Queue_Id__c = [SELECT Id FROM Group WHERE  Type = 'Queue' AND NAME = 'Tech Measure WO - Twin Cities' LIMIT 1].id;
		insert myStoreConfiguration;

		Back_Office_Checklist_Configuration__c myChecklist = new Back_Office_Checklist_Configuration__c();
		myChecklist.Store_Configuration__c = myStoreConfiguration.id;
		myChecklist.Contract_Signed__c = true;
		myChecklist.Deposit_Received__c = true;
		myChecklist.Lien_Rights_Signed__c = true;
		myChecklist.Project_Preparation_Expectations__c = true;
		myChecklist.Renovate_Right_Booklet__c = true;
		myChecklist.Rights_of_Cancellation__c = true;
		myChecklist.State_Supplement__c = true;
		insert myChecklist;

		myStore.Active_Store_Configuration__c = myStoreConfiguration.id;
		update myStore;

		Account myDwelling = new Account();
		myDwelling.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dwelling').getRecordTypeId();
		myDwelling.BillingCity = 'Minneapolis';
		myDwelling.BillingState = 'Minnesota';
		myDwelling.BillingCountry = 'United States';
		myDwelling.BillingStreet = '1234 Main St';
		myDwelling.BillingStateCode ='MN';
		myDwelling.Name = 'My Dwelling';
		myDwelling.Store_Location__c = myStore.id;
		insert myDwelling;

		Opportunity myOpp = new Opportunity();
		myOpp.Pricebook2Id =  Test.getStandardPricebookId();
		myOpp.RecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('CORO Record Type').getRecordTypeId();
		myOpp.AccountID= myDwelling.id;
		myOpp.CloseDate =Date.today();
		myOpp.StageName = 'New';
		myOpp.Name = 'Main Street Renovation Deal';
		insert myOpp;

		List<Product2> myProducts = new List<Product2>();
		for(Integer i=0; i<100; i++){
			Product2 myproduct = new Product2();
			myProduct.Name='My Product '+ i;
			myProducts.add(myProduct);
		}
		insert myProducts;


		List<PriceBookEntry> myPBEs = new List<PriceBookEntry>();
		Id standardPriceBookId = test.getStandardPricebookId();
		for(Product2 product : myProducts){
			PriceBookEntry myPBE = new PriceBookEntry();
			myPBE.Product2Id = product.id;
			myPBE.Pricebook2Id=standardPriceBookId;
			myPBE.UnitPrice = 1;
			myPBEs.add(myPBE);
		}
		insert myPBEs;



		
	}
	
	
	@isTest static void test_Construction() {

		Test.setCurrentPageReference(new PageReference('Page.RMS_uploadOrder'));
		System.currentPageReference().getParameters().put('oppId', [SELECT Id FROM Opportunity LIMIT 1].id);
		ApexPages.StandardController stdOppController = new ApexPages.StandardController([SELECT Id FROM Opportunity LIMIT 1]);
        RMS_uploadOrderExtension controller = new RMS_uploadOrderExtension(stdOppController);

        System.assert(controller.pageHasOppId);
        System.assert( !controller.fileHasBeenUploaded );
        System.assert( !controller.orderHasBeenCreated );
        System.assert( !controller.pendingProductsExist );
        System.assert( controller.projFileOrderItems.IsEmpty());
        System.assert( controller.projFileOrderId == null );


	}

	@isTest static void test_SaveAttachment() {
		Test.startTest();

		Test.setCurrentPageReference(new PageReference('Page.RMS_uploadOrder'));
		System.currentPageReference().getParameters().put('oppId', [SELECT Id FROM Opportunity LIMIT 1].id);
		ApexPages.StandardController stdOppController = new ApexPages.StandardController([SELECT Id FROM Opportunity LIMIT 1]);
        RMS_uploadOrderExtension controller = new RMS_uploadOrderExtension(stdOppController);

        Blob body = [Select  Body From StaticResource where name='Test_RBAProj_File'].body;
    	
    	Attachment attachment = new Attachment();
    	attachment.ParentId = [SELECT Id FROM Opportunity LIMIT 1].id;
    	attachment.Name = 'TestProjectFile.txt';
    	attachment.Body = body;
    	
    	controller.projFile = attachment;

    	controller.saveAttachment();

    	List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains('You must select an attachment with file extension .rbaproj')) b = true;
		}
		system.assert(b);

		controller.projFile.Name ='TestProjectFile.rbaproj';

		controller.saveAttachment();
		Test.stopTest();
		System.assert(controller.fileHasBeenUploaded);



	}
	
	
	@isTest static void test_conversionFromProjFileToOrder() {
		Test.startTest();
		Test.setCurrentPageReference(new PageReference('Page.RMS_uploadOrder'));
		System.currentPageReference().getParameters().put('oppId', [SELECT Id FROM Opportunity LIMIT 1].id);
		ApexPages.StandardController stdOppController = new ApexPages.StandardController([SELECT Id FROM Opportunity LIMIT 1]);
        RMS_uploadOrderExtension controller = new RMS_uploadOrderExtension(stdOppController);

        Blob body = [Select Body From StaticResource where name='Test_RBAProj_File'].body;

    	
    	Attachment attachment = new Attachment();
    	attachment.ParentId = [SELECT Id FROM Opportunity LIMIT 1].id;
    	attachment.Name = 'TestProjectFile.rbaproj';
    	attachment.Body = body;    	
    	controller.projFile = attachment;

    	controller.saveAttachment();
		System.assert(controller.fileHasBeenUploaded);
		System.assert(!controller.projFileOrderItems.IsEmpty());

		controller.uploadFile();

		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg:msgs){
		    if (msg.getDetail().contains('Order Created Successfully:')) b = true;
		}
		system.assert(b);

		Test.stopTest();
		System.assert(controller.pendingProductsExist);
		System.assert(controller.projFileOrderId !=null);


	}

	@isTest static void test_viewCreatedOrder() {
		Test.startTest();
		Test.setCurrentPageReference(new PageReference('Page.RMS_uploadOrder'));
		System.currentPageReference().getParameters().put('oppId', [SELECT Id FROM Opportunity LIMIT 1].id);
		ApexPages.StandardController stdOppController = new ApexPages.StandardController([SELECT Id FROM Opportunity LIMIT 1]);
        RMS_uploadOrderExtension controller = new RMS_uploadOrderExtension(stdOppController);

        Blob body = [Select Body From StaticResource where name='Test_RBAProj_File'].body;

    	
    	Attachment attachment = new Attachment();
    	attachment.ParentId = [SELECT Id FROM Opportunity LIMIT 1].id;
    	attachment.Name = 'TestProjectFile.rbaproj';
    	attachment.Body = body;    	
    	controller.projFile = attachment;

    	controller.saveAttachment();
		controller.uploadFile();
		Id myOrderID = controller.projFileOrderId;
		Test.setCurrentPageReference(controller.viewOrder());


		Test.stopTest();
		System.assert(apexPages.Currentpage().getURL().Contains(Controller.projFileOrderId));


	}

	@isTest static void test_makeModificationsToPendingOrderItems() {
		Test.startTest();
		Test.setCurrentPageReference(new PageReference('Page.RMS_uploadOrder'));
		System.currentPageReference().getParameters().put('oppId', [SELECT Id FROM Opportunity LIMIT 1].id);
		ApexPages.StandardController stdOppController = new ApexPages.StandardController([SELECT Id FROM Opportunity LIMIT 1]);
        RMS_uploadOrderExtension controller = new RMS_uploadOrderExtension(stdOppController);

        Blob body = [Select Body From StaticResource where name='Test_RBAProj_File'].body;
    	Attachment attachment = new Attachment();
    	attachment.ParentId = [SELECT Id FROM Opportunity LIMIT 1].id;
    	attachment.Name = 'TestProjectFile.rbaproj';
    	attachment.Body = body;    	
    	controller.projFile = attachment;

    	controller.saveAttachment();
		controller.uploadFile();
		controller.makeModificationsToPendingOrderItems();
		System.assert(controller.pendingProductsExist);
		System.assert(!controller.pendingOrderItems.IsEmpty());
		System.assert(controller.editModePendingOrderItems);

		controller.CancelModificationsToPendingOrderItems();

		Test.stopTest();
		System.assert(controller.pendingOrderItems.IsEmpty());
		System.assert(!controller.editModePendingOrderItems);


	}
	

	@isTest static void test_savePendingOrderItems_Success() {
		Test.startTest();
		Test.setCurrentPageReference(new PageReference('Page.RMS_uploadOrder'));
		System.currentPageReference().getParameters().put('oppId', [SELECT Id FROM Opportunity LIMIT 1].id);
		ApexPages.StandardController stdOppController = new ApexPages.StandardController([SELECT Id FROM Opportunity LIMIT 1]);
        RMS_uploadOrderExtension controller = new RMS_uploadOrderExtension(stdOppController);

		//this SOQL call exceeds the limits.
        Blob body = [Select Body From StaticResource where name='Test_RBAProj_File'].body;
    	Attachment attachment = new Attachment();
    	attachment.ParentId = [SELECT Id FROM Opportunity LIMIT 1].id;
    	attachment.Name = 'TestProjectFile.rbaproj';
    	attachment.Body = body;    	
    	controller.projFile = attachment;

    	controller.saveAttachment();
		controller.uploadFile();
		controller.makeModificationsToPendingOrderItems();
		System.assert(controller.pendingProductsExist);
		System.assert(!controller.pendingOrderItems.IsEmpty());
		System.assert(controller.editModePendingOrderItems);


		Id pricebookId = [SELECT Pricebook2Id FROM Opportunity LIMIT 1].Pricebook2Id;

		List<PriceBookEntry> myPBEs = [SELECT Id, Product2Id FROM PriceBookEntry WHERE Pricebook2Id = :pricebookId];
		for(OrderItem oi : controller.pendingOrderItems){
			Integer index = (Math.random() * (myPBEs.size() - 1)).intValue();
			PriceBookEntry randomPBE = myPBEs[index];
			oi.PricebookEntryId = randomPBE.id;
			oi.Child_Product_Pricebook_Entry_Id__c = randomPBE.id;
			myPBEs.remove(index);
		}

		controller.savePendingOrderItems();        
		Test.stopTest();
		System.assert(!controller.pendingProductsExist);

	}
    

	@isTest static void test_savePendingOrderItems_Unable_To_Add() {
		Test.startTest();
		Test.setCurrentPageReference(new PageReference('Page.RMS_uploadOrder'));
		System.currentPageReference().getParameters().put('oppId', [SELECT Id FROM Opportunity LIMIT 1].id);
		ApexPages.StandardController stdOppController = new ApexPages.StandardController([SELECT Id FROM Opportunity LIMIT 1]);
        RMS_uploadOrderExtension controller = new RMS_uploadOrderExtension(stdOppController);

        Blob body = [Select Body From StaticResource where name='Test_RBAProj_File'].body;
    	Attachment attachment = new Attachment();
    	attachment.ParentId = [SELECT Id FROM Opportunity LIMIT 1].id;
    	attachment.Name = 'TestProjectFile.rbaproj';
    	attachment.Body = body;    	
    	controller.projFile = attachment;

    	controller.saveAttachment();
		controller.uploadFile();
		controller.makeModificationsToPendingOrderItems();
		System.assert(controller.pendingProductsExist);
		System.assert(!controller.pendingOrderItems.IsEmpty());
		System.assert(controller.editModePendingOrderItems);

		for(OrderItem oi : controller.pendingOrderItems){
			oi.PricebookEntryId = [SELECT Id FROM Opportunity LIMIT 1].id;
		}

		controller.savePendingOrderItems();

		List<Apexpages.Message> msgs1 = ApexPages.getMessages();
		boolean unableToAddOrderItems = false;
		for(Apexpages.Message msg:msgs1){
		    if (msg.getDetail().contains('Unable to add Order Items.')) unableToAddOrderItems = true;
		}
		Test.stopTest();
		system.assert(unableToAddOrderItems);

	}

	@isTest static void test_savePendingOrderItems_Invalid_Products() {
		Test.startTest();
		Test.setCurrentPageReference(new PageReference('Page.RMS_uploadOrder'));
		System.currentPageReference().getParameters().put('oppId', [SELECT Id FROM Opportunity LIMIT 1].id);
		ApexPages.StandardController stdOppController = new ApexPages.StandardController([SELECT Id FROM Opportunity LIMIT 1]);
        RMS_uploadOrderExtension controller = new RMS_uploadOrderExtension(stdOppController);

        Blob body = [Select Body From StaticResource where name='Test_RBAProj_File'].body;
    	Attachment attachment = new Attachment();
    	attachment.ParentId = [SELECT Id FROM Opportunity LIMIT 1].id;
    	attachment.Name = 'TestProjectFile.rbaproj';
    	attachment.Body = body;    	
    	controller.projFile = attachment;

    	controller.saveAttachment();
		controller.uploadFile();
		controller.makeModificationsToPendingOrderItems();
		System.assert(controller.pendingProductsExist);
		System.assert(!controller.pendingOrderItems.IsEmpty());
		System.assert(controller.editModePendingOrderItems);

		for(OrderItem oi : controller.pendingOrderItems){
			oi.PricebookEntryId = null;
		}

		controller.savePendingOrderItems();

		List<Apexpages.Message> msgs2 = ApexPages.getMessages();
		boolean invalidProducts = false;
		for(Apexpages.Message msg:msgs2){
		    if (msg.getDetail().contains('Please select a valid product for at least one pending order item before saving modification')) invalidProducts = true;
		}
		
		Test.stopTest();
		
		system.assert(invalidProducts);


	}
	
}