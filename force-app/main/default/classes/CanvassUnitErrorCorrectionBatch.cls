global class CanvassUnitErrorCorrectionBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable
{
    //database.executebatch(new CanvassUnitErrorCorrectionBatch(),1);
    
    //CanvassUnitErrorCorrectionBatch rsb = new CanvassUnitErrorCorrectionBatch();
    //String sch = '0 0 23 * * ?';
    //system.schedule('CanvassUnit Error Correction Batch', sch, rsb);
    
    global CanvassUnitErrorCorrectionBatch()
    {
        
    }
    
    global void execute(SchedulableContext SC)
    {
        database.executeBatch(new CanvassUnitErrorCorrectionBatch(),1);
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        // get error messages created in the last 7 days where a correction has not been done
        return Database.getQueryLocator ([SELECT Id, Correction_Attempted__c, ZipCode__c, Market__c 
                                          FROM Custom_Data_Layer_Batch_Tracking__c
                                          WHERE Correction_Attempted__c = false
                                          AND CreatedDate = LAST_N_DAYS:7]);
    }
    
    global void execute (Database.BatchableContext BC, List<Custom_Data_Layer_Batch_Tracking__c> scope)
    {
        // iterate over error messages
        for(Custom_Data_Layer_Batch_Tracking__c errors :scope)
        {
            // update the correction field so that it won't run again
            errors.Correction_Attempted__c = true;
            
            // query to get queueable records
            list<Custom_Data_Layer_Batch_Queue__c> queueList = [SELECT Id, Name, Market_Id__c, ZipCode__c
                                                                FROM Custom_Data_Layer_Batch_Queue__c
                                                                WHERE Market_Id__c = :errors.Market__c
                                                                AND ZipCode__c = :errors.ZipCode__c];
            
            // iterate over queueable records                                                    
            for(Custom_Data_Layer_Batch_Queue__c queue :queueList)
            {
                // update the status field to holding so that it will re-run the batch
                queue.Status__c = 'Holding';
            }
            
            // process the updated queueable record
            update queueList;
        }

        // process the updated error message
        update scope;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
}