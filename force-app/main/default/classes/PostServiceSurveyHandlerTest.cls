@istest
private class PostServiceSurveyHandlerTest {
    private static final String DWELLING_ZIP = '53202';
    private static final String OPPORTUNITY1_NAME = 'Opp1';
    private static final String OPPORTUNITY2_NAME = 'Opp2';
    
    @isTest 
    static void postServiceTest() {
        TestDataFactoryStatic.setUpConfigs();
        
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;
        
        Store_Configuration__c storeConfig = new Store_Configuration__c( Store__c = store.Id,  Order_Number__c = 1);
        insert storeConfig;
        
        //Account acc = TestDataFactory.createStoreAccount('Test Account');
        Account acc = new Account(
            Name = 'Dwelling Account',
            AccountNumber = 'Dwelling Account1234567890',
            Phone = '(763) 555-2000',
            recordTypeId = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account'),
            ShippingPostalCode = '11111',
            ShippingStreet = 'Dwelling Account',
            Store_Location__c =  store.Id
        );
        insert acc;
        Contact primaryContact = TestDataFactoryStatic.createContact(acc.Id, 'CalvinPrime');
        
        Opportunity opportunity = TestDataFactoryStatic.createFSLOpportunity('Calvins Opportunity', acc.id, store.Id, 'Sold', date.today());
        
        String pricebookId = Test.getStandardPricebookId();
        
        Product2 prod = new Product2();
        prod.Name = 'Test Product 1';
        prod.IsActive = true;
        insert prod;
        Id recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Service').getRecordTypeId();
        Date d = Date.today().addDays(-1);
        test.startTest();
        Order testOrder2 =  new Order();
        testOrder2.RecordTypeId = recordTypeId;
        testOrder2.Name ='Sold Order 1';
        testOrder2.AccountId = acc.Id;
        testOrder2.EffectiveDate = Date.Today();
        testOrder2.Store_Location__c = store.Id;
        testOrder2.BillToContactId = primaryContact.Id;
        testOrder2.Opportunity = opportunity;
        testOrder2.Status = 'Warranty Submitted';
        testOrder2.Pricebook2Id = pricebookId;
        testOrder2.Customer_Pickup_All__c = FALSE;
        testOrder2.Installation_Date__c = system.today()-1;
        testOrder2.Notification_Message_Pop_Up__c=true;
        testOrder2.Message_Pop_up__c='testing';
        testOrder2.Service_Type__c ='Legacy';
        testOrder2.Revenue_Recognized_Date__c = d;
        insert testOrder2;
        
        
        
        list<Order> o = [SELECT Id,Revenue_Recognized_Date__c,of_Visits__c  ,BillToContactid,AccountId,Sold_Order__c,Service_Type__c, OpportunityId,Survey_Created__c,Description,Warranty_Date_Submitted__c, EffectiveDate, Store_Location__c from Order where Service_Type__c ='Legacy'];
        System.assertEquals(1, o.size()) ;
        SchedulableContext sc = null;
        BatchSchedulePostServiceSurvey SCobj = new BatchSchedulePostServiceSurvey();
        SCobj.execute(sc);
        BatchJobPostServiceSurvey obj = new BatchJobPostServiceSurvey();
        Database.executeBatch(obj);
        PostServiceSurveyHandler.surveyServiceProcessCreation(o);
        list<Survey__c> sr = [select id,Service_Request_Number__c from Survey__c where Service_Request_Number__c =:testOrder2.Id ];
        system.assertEquals(sr[0].Service_Request_Number__c, testOrder2.Id);
        test.stopTest();
    }
    
    static testmethod void withBillToContactSoldOrderTest(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        Id municipalityPermitRT = UtilityMethods.retrieveRecordTypeId('Permit', 'Municipality__c');
        Id municipalityHoaRT = UtilityMethods.retrieveRecordTypeId('HOA', 'Municipality__c');
        Id municipalityHistoricalRT = UtilityMethods.retrieveRecordTypeId('Historical', 'Municipality__c');
        Municipality__c permitMunicipality = utility.createMunicipality(null, municipalityPermitRT, '55555', 'Blaine', 'Anoka', 'MN');
        Municipality__c hoaMunicipality = utility.createMunicipality(null, municipalityHoaRT, '55555', 'Blaine', 'Anoka', 'MN');
        Municipality__c historicalMunicipality = utility.createMunicipality(null, municipalityHistoricalRT, '55555', 'Blaine', 'Anoka', 'MN');
        
        Account store = [SELECT id FROM Account WHERE Name ='77 - Twin Cities, MN'];
        Id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        Account newDwelling = new Account(Name=DWELLING_ZIP,Store_Location__c = store.id,RecordTypeId = dwellingRT,ShippingStreet = '200 E Wells St 1',
                                          ShippingCity = 'Milwaukee',ShippingStateCode = 'WI',ShippingState = 'Wisconsin', ShippingCountryCode = 'US',
                                          ShippingCountry = 'United States', ShippingPostalCode =  '53202', HOA__c = hoaMunicipality.Id,Historical__c = historicalMunicipality.Id,
                                          Building_Permit__c = permitMunicipality.Id, Year_Built__c = '1972' // before 1978 requires LSWP work order
                                         );
        insert newDwelling;
        Opportunity oppn = new Opportunity(Name=OPPORTUNITY1_NAME, AccountId=newDwelling.id, StageName = 'New', CloseDate = Date.today().addYears(1) );
        Opportunity opps = new Opportunity(Name=OPPORTUNITY2_NAME, AccountId=newDwelling.id, StageName = 'New', CloseDate = Date.today().addMonths(6) );
        insert new List<Opportunity> { oppn, opps };
            Id orderAroRT = UtilityMethods.retrieveRecordTypeId('ARO_Record_Type', 'Order');
        Id orderCoroServiceRT = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
        Id orderCoroRT = UtilityMethods.retrieveRecordTypeId('CORO_Record_Type', 'Order');
        Id workOrderTechMeasureRT = UtilityMethods.retrieveRecordTypeId('Tech_Measure', 'WorkOrder');
        Account dwelling = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry FROM Account WHERE Name LIKE :(DWELLING_ZIP + '%')];
        
        Contact primaryContact = TestDataFactoryStatic.createContact(dwelling.Id, 'CalvinPrime');
        Opportunity opp1 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY1_NAME];
        Opportunity opp2 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY2_NAME];
        Date d = Date.today().addDays(-1);
        Order order1 = new Order(
            RecordTypeId = orderCoroRT,
            AccountId = dwelling.id,
            Status = 'Draft',
            Service_Type__c = 'Service',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            OpportunityId = opp1.Id
        );
        insert order1;
        
        Order order3 = new Order(
            RecordTypeId = orderCoroServiceRT,
            AccountId = dwelling.id,
            Status = 'Closed',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            Service_Type__c ='Legacy',
            OpportunityId = opp2.Id,
            Store_Location__c = store.Id,
            Sold_Order__c = order1.id,
            BillToContactId = primaryContact.Id,
            Revenue_Recognized_Date__c = d
            
        );
        insert order3;
        system.assert(order3.id!= null);
        test.startTest();
        list<Order> o = [SELECT Id,Revenue_Recognized_Date__c,of_Visits__c,BillToContactid,AccountId,Sold_Order__c,Service_Type__c, OpportunityId,Survey_Created__c,Description,Warranty_Date_Submitted__c, EffectiveDate, Store_Location__c from Order where id =: order3.id];
        System.assertEquals(1, o.size()) ;
        BatchJobPostServiceSurvey obj = new BatchJobPostServiceSurvey();
        Database.executeBatch(obj);
        PostServiceSurveyHandler.surveyServiceProcessCreation(o);        
        PostServiceSurveyHandler.LogExceptionSMethod('Dml Exception',' string mesg', 'string c');
        list<Error__c> er = [select id ,Class__c from Error__c where Class__c ='string c'];
        system.assertEquals(1, er.size());
        
        test.stopTest();
        
    }
    /* failing unit test. commented out mark rothermal 3-17-20
    static testmethod void noBillToContactSoldOrderTest(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;
        
        Store_Configuration__c storeConfig = new Store_Configuration__c( Store__c = store.Id,  Order_Number__c = 1);
        insert storeConfig;
        
        //Account acc = TestDataFactory.createStoreAccount('Test Account');
        Account acc = new Account(
            Name = 'Dwelling Account Test',
            AccountNumber = 'Dwelling Account5681234567890',
            Phone = '(763) 555-2000',
            recordTypeId = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account'),
            ShippingPostalCode = '11111',
            ShippingStreet = 'Dwelling Account',
            Store_Location__c =  store.Id
        );
        insert acc;
        System.debug('debug for acc' + acc);
        Contact primaryContact = TestDataFactoryStatic.createContact(acc.Id, 'CalvinPrimeTest');
        
        list<Contact_History__c> CHList = [select id from Contact_History__c WHERE Contact__c  = : primaryContact .id ];
        System.debug('No. Of contact history' + CHList.size() );
        
        Contact_History__c tempCH = CHList[0];
        tempCH.Primary_Contact__c = true ;
        update tempCH ;
        
        Opportunity opportunity = TestDataFactoryStatic.createFSLOpportunity('Calvins Opportunity', acc.id, store.Id, 'Sold', date.today());
        
        String pricebookId = Test.getStandardPricebookId();
        
        Product2 prod = new Product2();
        prod.Name = 'Test Product 1';
        prod.IsActive = true;
        insert prod;
        Id recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Service').getRecordTypeId();   
        
        Date d = Date.today().addDays(-1);
        Order testOrder2 =  new Order();
        testOrder2.RecordTypeId = recordTypeId;
        testOrder2.Name ='Sold Order 1';
        testOrder2.AccountId = acc.Id;
        testOrder2.EffectiveDate = Date.Today();
        testOrder2.Store_Location__c = store.Id;
        //testOrder2.Sold_Order__c = order1.id;
        // testOrder2.BillToContactId = primaryContact.Id;
        testOrder2.Opportunity = opportunity;
        testOrder2.Status = 'Warranty Submitted';
        testOrder2.Pricebook2Id = pricebookId;
        testOrder2.Customer_Pickup_All__c = FALSE;
        testOrder2.Installation_Date__c = system.today()-1;
        testOrder2.Notification_Message_Pop_Up__c=true;
        testOrder2.Message_Pop_up__c='testing';
        testOrder2.Service_Type__c ='Legacy';
        testOrder2.Revenue_Recognized_Date__c = d;
        insert testOrder2;
        test.startTest();
        list<Order> o = [SELECT Id,Revenue_Recognized_Date__c,of_Visits__c  ,BillToContactid,AccountId,Sold_Order__c,Service_Type__c, OpportunityId,Survey_Created__c,Description,Warranty_Date_Submitted__c, EffectiveDate, Store_Location__c from Order where Service_Type__c ='Legacy'];
        System.assertEquals(1, o.size()) ;
        BatchJobPostServiceSurvey obj = new BatchJobPostServiceSurvey();
        Database.executeBatch(obj);
        PostServiceSurveyHandler.surveyServiceProcessCreation(o);
        list<Survey__c> sr = [select id,Service_Request_Number__c from Survey__c where Service_Request_Number__c =:testOrder2.Id ];
        system.assertEquals(1, sr.size());
        test.stopTest();
        
        
    }
*/
    static testmethod void workorderTestmethod(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        // Create the store
        Account storeAccount = TestDataFactoryStatic.createStoreAccount('77 - Twin Cities, MN');
        Database.insert(storeAccount, true);
        Id storeAccountId = storeAccount.Id;
        
        // Get the security profile for field service
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'RMS-CORO Field Service' LIMIT 1].Id;
        
        // Create the test users for tech measure and install
        User serviceUserTechMeasure = TestDataFactoryStatic.createUser(profileId);
        User serviceUserInstaller = TestDataFactoryStatic.createUser(profileId);
        
        // Get the store configuration Id
        Id storeConfigId = [SELECT Id FROM Store_Configuration__c LIMIT 1].Id;
        
        // Create the test Account for the store
        Account testAccount = TestDataFactoryStatic.createAccount('Calvins', storeAccountId);
        Id accountId = testAccount.Id;
        
        // Create a contact for the store account
        Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'CalvinPrime');
        Contact SecondaryContact = TestDataFactoryStatic.createContact(accountId, 'CalvinPrime');
        
        Id orderCoroServiceRT = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
        Id orderCoroRT = UtilityMethods.retrieveRecordTypeId('CORO_Record_Type', 'Order');
        
        Id municipalityPermitRT = UtilityMethods.retrieveRecordTypeId('Permit', 'Municipality__c');
        Id municipalityHoaRT = UtilityMethods.retrieveRecordTypeId('HOA', 'Municipality__c');
        Id municipalityHistoricalRT = UtilityMethods.retrieveRecordTypeId('Historical', 'Municipality__c');
        Municipality__c permitMunicipality = utility.createMunicipality(null, municipalityPermitRT, '55555', 'Blaine', 'Anoka', 'MN');
        Municipality__c hoaMunicipality = utility.createMunicipality(null, municipalityHoaRT, '55555', 'Blaine', 'Anoka', 'MN');
        Municipality__c historicalMunicipality = utility.createMunicipality(null, municipalityHistoricalRT, '55555', 'Blaine', 'Anoka', 'MN');
        
        Account store = [SELECT id FROM Account WHERE Name ='77 - Twin Cities, MN' limit 1];
        Id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        Account newDwelling = new Account(Name=DWELLING_ZIP,Store_Location__c = store.id,RecordTypeId = dwellingRT,ShippingStreet = '200 E Wells St 1',
                                          ShippingCity = 'Milwaukee',ShippingStateCode = 'WI',ShippingState = 'Wisconsin', ShippingCountryCode = 'US',
                                          ShippingCountry = 'United States', ShippingPostalCode =  '53202', HOA__c = hoaMunicipality.Id,Historical__c = historicalMunicipality.Id,
                                          Building_Permit__c = permitMunicipality.Id, Year_Built__c = '1972' // before 1978 requires LSWP work order
                                         );
        insert newDwelling;
        Opportunity oppn = new Opportunity(Name=OPPORTUNITY1_NAME, AccountId=newDwelling.id, StageName = 'New', CloseDate = Date.today().addYears(1) );
        Opportunity opps = new Opportunity(Name=OPPORTUNITY2_NAME, AccountId=newDwelling.id, StageName = 'New', CloseDate = Date.today().addMonths(6) );
        insert new List<Opportunity> { oppn, opps };
            
            Id workOrderTechMeasureRT = UtilityMethods.retrieveRecordTypeId('Tech_Measure', 'WorkOrder');
        Account dwelling = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry FROM Account WHERE Name LIKE :(DWELLING_ZIP + '%')];
        
        
        Opportunity opp1 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY1_NAME];
        Opportunity opp2 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY2_NAME];
        Date d = Date.today().addDays(-1);
        Order order1 = new Order(
            RecordTypeId = orderCoroRT,
            AccountId = dwelling.id,
            Status = 'Draft',
            Service_Type__c = 'Service',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            OpportunityId = opp1.Id
        );
        insert order1;
        
        Order order3 = new Order(
            RecordTypeId = orderCoroServiceRT,
            AccountId = dwelling.id,
            Status = 'Closed',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            Service_Type__c ='Legacy',
            OpportunityId = opp2.Id,
            Store_Location__c = store.Id,
            Sold_Order__c = order1.id,
            BillToContactId = primaryContact.Id,
            Revenue_Recognized_Date__c = d
            
        );
        insert order3;
        
        system.assert(order3.id!= null);
        id orderId = order3.Id;
        
        //setup field service lightning permission sets for the service users
        //List<PermissionSet> PermissionSetList = [Select Label, Id from PermissionSet where Label = 'FSL Resource Permissions' or Label = 'FSL Resource License'];
        List<Id> PermissionSetIds = new List<Id>();
        for(PermissionSet ps : [Select Label, Id from PermissionSet where Label = 'FSL Resource Permissions' or Label = 'FSL Resource License']) {
            PermissionSetIds.add(ps.Id);
        }
        
        // Id permissionSetId = [SELECT Id FROM PermissionSet WHERE Label = 'FSL Resource Permissions' LIMIT 1].Id;
        // Id permissionSetId2 = [SELECT Id FROM PermissionSet WHERE Label = 'FSL Resource License'].Id;
        Database.insert(serviceUserTechMeasure);
        Database.insert(serviceUserInstaller);
        
        
        // Creating Work Types for the Work Order
        Id measureTypeId = TestDataFactoryStatic.createFLSWorkType('Service', 8.00, 'Hours').Id;
        
        
        Test.startTest();
        List<ServiceResource> servResourceList = new List<ServiceResource>();
        servResourceList.add(TestDataFactoryStatic.createFSLServiceResource(serviceUserTechMeasure.Id, 'Calvin', 'test1', storeConfigId));
        servResourceList.add(TestDataFactoryStatic.createFSLServiceResource(serviceUserInstaller.Id, 'Calvin', 'test2', storeConfigId));
        
        Database.insert(servResourceList);
        List<WorkOrder> workOrdList = new List<WorkOrder>();
        workOrdList.add(TestDataFactoryStatic.createFSLWorkOrder(measureTypeId, 'Service ', orderId, dwelling, opp2));
        workOrdList.add(TestDataFactoryStatic.createFSLWorkOrder(measureTypeId, 'Service', orderId, dwelling, opp2));
        
        for(integer i =0 ; i< workOrdList.size(); i++){
            workOrdList[i].Status = 'Appt Complete / Closed';
            workOrdList[i].Primary_Service_FSL__c = servResourceList[i].id;            
        }
        System.debug(workOrdList);
        Database.insert(workOrdList);
        
        
        list<WorkOrder> workList = [ Select Id,Sold_Order__c,Primary_Service_FSL__r.RelatedRecordId from WorkOrder ];
        
        TestDataFactoryStatic.createFSLServiceTerritory(servResourceList[0], 'test1');
        TestDataFactoryStatic.createFSLServiceTerritory(servResourceList[1], 'test2');
        
        List<ServiceAppointment> servApptList = new List<ServiceAppointment>();
        servApptList.add(TestDataFactoryStatic.createFSLServiceAppointment(workList[0].Id));
        servApptList.add(TestDataFactoryStatic.createFSLServiceAppointment(workList[1].Id));
        Database.insert(servApptList);
        
        List<AssignedResource> assignResourceList = new List<AssignedResource>();
        assignResourceList.add(TestDataFactoryStatic.createFSLAssignedResource(servResourceList[0], servApptList[0]));
        assignResourceList.add(TestDataFactoryStatic.createFSLAssignedResource(servResourceList[1], servApptList[1]));
        // Database.insert(assignResourceList);
        
        list<Order> o = [SELECT Id,Revenue_Recognized_Date__c,of_Visits__c  ,BillToContactid,AccountId,Sold_Order__c,Service_Type__c, OpportunityId,Survey_Created__c,Description,Warranty_Date_Submitted__c, EffectiveDate, Store_Location__c from Order where id =:orderId];
        System.assertEquals(1, o.size()) ;
        // BatchJobPostServiceSurvey obj = new BatchJobPostServiceSurvey();
        // Database.executeBatch(obj);
        PostServiceSurveyHandler.surveyServiceProcessCreation(o);
        //  BatchJobPostServiceSurvey obj = new BatchJobPostServiceSurvey();
        // System.debug(Database.executeBatch(obj));
        
        Test.stopTest();
        
        
    }
    
}