@isTest
private class RMS_QuoteTriggerHandlerTest{

	 @testSetup static void setupData() {
        TestUtilityMethods utility = new TestUtilityMethods();
        
        utility.setUpConfigs();
        
        Account account1 = utility.createVendorAccount('Vendor Account 1');
        insert account1;
          id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');

        Account account2 = new Account( Name = 'RbA',
                                        AccountNumber = '1234567890',
                                        Phone = '(763) 555-2000',
                                                  RecordTypeId = dwellingRT
                                        
                                    );
        insert account2;

        Account dwelling = utility.createDwellingAccount('Dwelling Account');

        Account store = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
        dwelling.Store_Location__c = store.Id;
        insert dwelling;

       
        Opportunity opp1 = utility.createOpportunity(dwelling.id, 'In Progress');
        opp1.Name = 'opp1';
        insert opp1;

        Opportunity opp2 = utility.createOpportunity(dwelling.id, 'In Progress');
        insert opp2;


           
        Financial_Account_Number__c fan = new Financial_Account_Number__c(Name ='Finan Acc',Account_Type__c='Cost PO');
        insert fan;
        Product2 product1 = new Product2(
            Name='Test Product',
            Vendor__c = account1.id,
            Cost_PO__c = true,
            isActive = true,
            Account_Number__c =  fan.Id
        );
        
        insert product1;

          Pricebook2 pricebook1 =  utility.createPricebook2Name('Standard Price Book');
          insert pricebook1;

          Quote quote1 = utility.createQuote(opp1.id, pricebook1.id );
          insert quote1;

          Quote quote2 = utility.createQuote(opp1.id, pricebook1.id );
          insert quote2;

          Quote quote3 = utility.createQuote(opp1.id, pricebook1.id );
          insert quote3;          

          List<PricebookEntry> pbsToEnter = new List<PricebookEntry>();
          PricebookEntry pricebookEntry1 = utility.createPricebookEntry(Test.getStandardPricebookId(), product1.id);
          pbsToEnter.add(pricebookEntry1);
        PricebookEntry pricebookEntry2 = utility.createPricebookEntry(pricebook1.Id, product1.id);
          pbsToEnter.add(pricebookEntry2);
        insert pbsToEnter;
        
        List<Order> orderList =  new List<Order>{ new Order(    Name='TMNeeded', 
                                    AccountId = dwelling.id, 
                                    EffectiveDate= Date.Today(), 
                                    Store_Location__c = store.Id,
                                    OpportunityId = opp1.Id,                                 
                                    Status ='Draft', 
                                    Tech_Measure_Status__c = 'New',
                                    Pricebook2Id = Test.getStandardPricebookId()
                                ),  new Order(  Name='TMScheduled', 
                                    AccountId = dwelling.id, 
                                    EffectiveDate= Date.Today(), 
                                    Store_Location__c = store.Id,
                                    OpportunityId = opp1.Id,                                     
                                    Status ='Draft',
                                    Tech_Measure_Status__c = 'New', 
                                    Pricebook2Id = Test.getStandardPricebookId()
                                ),  new Order(Name='InstallScheduled', 
                                    AccountId = dwelling.id, 
                                    EffectiveDate= Date.Today(), 
                                    Store_Location__c = store.Id,
                                    OpportunityId = opp1.Id,                                     
                                    Status ='Draft', 
                                    Install_Order_Status__c = 'New',
                                    Pricebook2Id = Test.getStandardPricebookId()
                                ),  new Order(Name='InstallNeeded', 
                                    AccountId = dwelling.id, 
                                    EffectiveDate= Date.Today(), 
                                    Store_Location__c = store.Id,
                                    OpportunityId = opp1.Id,                                     
                                    Status ='Draft', 
                                    Install_Order_Status__c = 'New',
                                    Pricebook2Id = Test.getStandardPricebookId()
                                )};     
        insert orderList;

        List<OrderItem> oiList = new List<OrderItem>();
        for(Order o: orderList){
            oiList.add(new OrderItem(OrderId = o.Id, PricebookentryId = pricebookEntry1.Id, Quantity = 2, UnitPrice = 100 ));
        }
        insert oiList;
        
        
    }

    @isTest
	private static void testOnDeleteQuoteWithOrder() {

        User testUser = [SELECT Id FROM User WHERE Profile.Name ='RMS-CORO RSR' AND isActive = TRUE AND Default_Store_Location__c = '0077 - Twin Cities'  LIMIT 1];

		Order ord = [Select Id from Order WHERE Name = 'TMNeeded'];
		ord.OwnerId = testUser.Id;
		update ord;

		Opportunity opp = [Select Id from Opportunity WHERE Name = 'opp1'];
		opp.OwnerId = testUser.id;
		update opp;

		List<Quote> qt = [Select Id, Name from Quote where Name = 'testQuote' ];

		Test.startTest();

			System.runAs(testUser){
				try{

					delete qt;
				}
				catch(Exception e){

					System.Assert(e.getMessage().contains('You cannot delete a Quote'));

				}
		
			}

		Test.stopTest();

	}

	 @isTest
	private static void testOnDeleteQuoteWithoutOrder() {

        User testUser = [SELECT Id FROM User WHERE Profile.Name ='RMS-CORO RSR' AND isActive = TRUE AND Default_Store_Location__c = '0077 - Twin Cities'  LIMIT 1];

		Order ord = [Select Id from Order WHERE Name = 'TMNeeded'];
		ord.OwnerId = testUser.Id;
		ord.OpportunityId = null;
		update ord;

		Opportunity opp = [Select Id from Opportunity WHERE Name = 'opp1'];
		opp.OwnerId = testUser.id;
		update opp;

		List<Quote> qt = [Select Id, Name from Quote where Name = 'testQuote' ];

		Test.startTest();

			System.runAs(testUser){
					delete qt;
			}

		Test.stopTest();

	}

}