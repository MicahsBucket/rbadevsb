/**
 * @File Name          : MakabilityCheckScreenConfig.cls
 * @Description        : 
 * @Author             : Mark.Rothermal@AndersenCorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 02-24-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    1/12/2019, 1:30:42 PM   Mark.Rothermal@AndersenCorp.com     Initial Version
 * 1.1    3/12/2019, 1:30:42 PM   Mark.Rothermal@AndersenCorp.com     Removed else statement for screentype error.
**/

public without sharing class MakabilityCheckScreenConfig implements MakabilityService {
    
    /**
    * @description
    * @author mark.rothermal@andersencorp.com | 5/3/2019
    * @param map<string, MakabilityRestResource.OrderItem> requests
    * @param set<id> productIds
    * @return List<MakabilityRestResource.MakabilityResult>
    */
    public static List<MakabilityRestResource.MakabilityResult>  checkCompatibility( map <string,MakabilityRestResource.OrderItem> requests, set<id> productIds){
        List<MakabilityRestResource.MakabilityResult> results = new List<MakabilityRestResource.MakabilityResult>(); 
        Map<Id,Map<string,Screen_Configuration__c>> pIdToScreentypeConfigMap = new Map <Id,Map<String,Screen_Configuration__c>>();
        Map<id,Screen_Configuration__c> screenConfigMap = getConfigOptions(productIds);
        
        // handle no config record found.
        if(screenConfigMap.keyset().size() == 0){
            for(String r:requests.keyset()){
                MakabilityRestResource.OrderItem oi = requests.get(r);
                MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult(); 
                List<string> errMessages = new List<string>();
                string errMessage = 'Salesforce - No Screen configurations found for this productId.';  
                errMessages.add(errMessage);
                result.errorMessages = errMessages;
                result.orderId = oi.orderId;
                result.orderItemId = oi.orderItemId;
                results.add(result);                
            }            
            return results;
        }
        
        // populate map
        for(id screenConfig:screenConfigMap.keyset()) {
            Screen_Configuration__c sc = screenConfigMap.get(screenConfig);
            if(pIdToScreentypeConfigMap.containskey(sc.Product_Configuration__r.Product__c)){
                Map<string,Screen_Configuration__c> placeholder = pIdToScreentypeConfigMap.get(sc.Product_Configuration__r.Product__c);
                if (!placeholder.containsKey(sc.Screen_Type__c)){
                    placeholder.put(sc.Screen_Type__c,sc);
                }
            } else {
                Map<string,Screen_Configuration__c> placeholder = new map<string,Screen_Configuration__c>();
                placeholder.put(sc.Screen_Type__c,sc);
                pIdToScreentypeConfigMap.put(sc.Product_Configuration__r.Product__c, placeholder);                                
            }            
        }
        
        ////////////////////////////// 
        // run makability check  
        // //////////////////////////
        for(String r:requests.keyset()){
            MakabilityRestResource.OrderItem req = requests.get(r);
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();  
            boolean widthPassing = true;
            boolean heightPassing = true;
            id pId = req.productConfiguration.productId;
            ProductConfiguration reqConfig = req.productConfiguration;
            Map<string,Screen_Configuration__c> currentConfigMap = pIdToScreentypeConfigMap.get(pId);                
            Screen_Configuration__c currentConfig;
            List<string> errMessages = new List<string>();
            if(currentConfigMap.containskey(reqConfig.screenType)){
                currentConfig = currentConfigMap.get(reqConfig.screenType);             
                result.isMakable = false;
                // compare width, height, check for half screen.
                if(!MakabilityUtility.compareMaxHeightOrWidth(reqConfig.widthInches,
                                                              reqConfig.widthFractions,
                                                              currentConfig.Max_Width_Inches__c, 
                                                              currentConfig.Max_Width_Fraction__c)){
                                                                  //  errMessages.add('Screen Config - incorrect width, must be less than or equal to ' +
                                                                  //                 currentConfig.Max_Width_Inches__c + ' ' + currentConfig.Max_Width_Fraction__c + ' inches.' );
                                                                  widthPassing = false;
                                                              }
                // only check height if width fails ( manufacturing technically has unlimited height option, and can turn screen sideways to make width match )
                if (!MakabilityUtility.compareMaxHeightOrWidth(reqConfig.heightInches,
                                                               reqConfig.heightFractions,
                                                               currentConfig.Max_Height_Inches__c, 
                                                               currentConfig.Max_Height_Fraction__c) && widthPassing == false){
                                                                   //    errMessages.add('Screen Config - incorrect height, must be less than or equal to ' +
                                                                   //                    currentConfig.Max_Height_Inches__c + ' ' + currentConfig.Max_Height_Fraction__c + ' inches.' );
                                                                   heightPassing = false;
                                                               }
                // Currently there is no half screen option that will fail makability 
                // It was suggested and declined to create a check for half screens.
                // If one were needed it would look something like - request.height/2 <= config.maxHeight. 
                // mtr 11/02/18
                if(String.isEmpty(reqConfig.screenSize) && widthPassing == false && heightPassing == false){
                    errMessages.add('Screen Config - Incorrect size combination for screen type and screen size selected.' );
                }  
                if(errMessages.size() == 0  || errMessages == null){
                    result.isMakable = true;
                    errMessages.add('Screen Config - passed');                    
                }                
            } 
      /*            commented out 3/12/19 mtr - per Beth - screen config passes unless a match is found for screen type above, can only fail 
       *              if there is a screen type match and one of the tests above fails.
       *
       *      else{
       *         errMessages.add('Screen Config Incorrect ScreenType, valid screen types are: ' +  currentConfigMap.keyset());
       *         result.isMakable = false;                                    
       *      }   
       */
            result.orderId = req.orderId;
            result.orderItemId = req.orderItemId;
            result.errorMessages = errMessages;
            results.add(result);            
            
        }
        
        return results;
    } 
    
    /**
    * @description
    * @author mark.rothermal@andersencorp.com | 5/3/2019
    * @param set<id> productIds
    * @return Map<id, Screen_Configuration__c>
    */
    public static Map<id,Screen_Configuration__c> getConfigOptions(set<id> productIds){
        Map<id,Screen_Configuration__c> configMap = new Map<id,Screen_Configuration__c>( [
            SELECT id,
            Screen_Type__c,
            Full_Screen_Max_Height_Fraction__c,
            Full_Screen_Max_Height_Inches__c,
            Max_Height_Fraction__c,
            Max_Height_Inches__c,
            Max_Width_Fraction__c,
            Max_Width_Inches__c,
            Product_Configuration__r.Product__c
            from Screen_Configuration__c 
            where Product_Configuration__r.Product__c in :productids] );  
        return configMap;
    }
}