@IsTest
private class Renewal_ContentObjectTest {
    @testSetup
    static void testSetup() {
        Contact testContact = Peak_TestUtils.createTestContact();
    }

    @isTest
    public static void testRenewalContentObject() {

        List<Contact> testContacts = [SELECT Id from Contact where Email = :Peak_TestConstants.STANDARD_EMAIL];

        User testUser = Peak_TestUtils.createStandardUserNoContact();
        try {
            testUser.ContactId = testContacts[0].Id;
            insert testUser;
        }catch(Exception e){
            testUser.ContactId = NULL;
            insert testUser;
        }

        Test.startTest();

        Renewal_ContentObject testContentObject = new Renewal_ContentObject();
        testContentObject.contentID = testUser.Id;
        testContentObject.title = Peak_TestConstants.FIRSTNAME;
        testContentObject.description = Peak_TestConstants.TEST_DESCRIPTION;
        testContentObject.fullDescription = Peak_TestConstants.TEST_DESCRIPTION;
        testContentObject.attachments = new List<Attachment>();
        testContentObject.url = Peak_TestConstants.TEST_URL;
        testContentObject.featured = false;
        testContentObject.bannerImage = '';
        testContentObject.avatar = '';
        testContentObject.commentCount = 1;
        testContentObject.commentUrl = '';
        testContentObject.dateTimeField = DateTime.newInstance(2011, 11, 18, 3, 3, 3);
        testContentObject.dateField = Peak_TestConstants.TODAY;
        testContentObject.dashboardAdmin = true;
        testContentObject.rforceAdmin = true;

        System.assertEquals(testContentObject.title,Peak_TestConstants.FIRSTNAME);
        System.assertEquals(testContentObject.dashboardAdmin,true);
        System.assertEquals(testContentObject.dashboardAdmin,true);
        Test.stopTest();
    }
}