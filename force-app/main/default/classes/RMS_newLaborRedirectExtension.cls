/*******************************************************//**

@class      RMS_newLaborRedirectExtension

@brief      Extension for redirecting the user when clicking the New labor button

    Redirects the user directly to a labor or misc reimbursement page depending on if it's linked
    to a work order, bypassing the record type selector

@author     Creston Kuenzi (Slalom.CDK)

@version    2015-2-2  Slalom.CDK,  Created.
            2018-2-7  Penrod - Gabe Rholl,  Edited.

@see        RMS_newLaborRedirectExtensionTest

@copyright  (c)2015 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/

public with sharing class RMS_newLaborRedirectExtension {

    /******* Set up Standard Controller for Labor__c  *****************/
    public boolean displayPopup {get; set;}
    private final Labor__c theLabor;
    
    public RMS_newLaborRedirectExtension(ApexPages.StandardController stdController) {
        this.theLabor = (Labor__c)stdController.getRecord();
        system.debug('@@@@@'+theLabor);
        if (this.theLabor == null)
            this.theLabor = new Labor__c(); 
    }

    public String testStoreLocation;

    /******* redirect method  *****************/
    public PageReference redirect() {

        // Retrieve the key prefix for the labor object and insert it into the page reference
        Schema.DescribeSObjectResult r = Labor__c.sObjectType.getDescribe();
        String keyPrefix = r.getKeyPrefix(); 
        system.debug('@@@@'+ keyPrefix);      
        PageReference p = new PageReference('/' +keyPrefix +'/e');


        // Retrieve the field Ids to use as url parameter keys
        String relatedFSLWorkOrderId = FieldIdUtils.getFormattedLookupIdFromName('Labor__c', 'Related_FSL_Work_Order__c');
        String relatedFSLWorkOrderLKID = FieldIdUtils.getFormattedLKIDFromName('Labor__c', 'Related_FSL_Work_Order__c');
        String relatedStoreLocationId = FieldIdUtils.getFormattedLookupIdFromName('Labor__c', 'Store_Location__c');
        String relatedStoreLocationLKID = FieldIdUtils.getFormattedLKIDFromName('Labor__c', 'Store_Location__c');

        // Get all of the url parameters from the current url and put them in the new url
        Map<String, String> m = p.getParameters();
        m.put('retURL', ApexPages.currentPage().getParameters().get('retURL'));
        m.put(relatedFSLWorkOrderId, ApexPages.currentPage().getParameters().get(relatedFSLWorkOrderId));
        m.put(relatedFSLWorkOrderLKID, ApexPages.currentPage().getParameters().get(relatedFSLWorkOrderLKID));

        // If there is a work order, redirect to labor, otherwise redirect to misc reimbursement
        if (theLabor.Related_FSL_Work_Order__c == null) {
            m.put('RecordType', UtilityMethods.RecordTypeFor('Labor__c', 'Misc_Reimbursement'));
            theLabor.RecordTypeId = UtilityMethods.RecordTypeFor('Labor__c', 'Misc_Reimbursement');

            // If a test is running just set the store name, otherwise get it from the current user
            String storeLocationName = (Test.isRunningTest()) ?     testStoreLocation :
                                                                    [SELECT Default_Store_Location__c FROM User WHERE Id =: UserInfo.getUserId()].Default_Store_Location__c;

            if (String.isBlank(storeLocationName)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, RMS_errorMessages.STORE_LOCATION_IS_BLANK_USER)); //
                return null;
            }

            for (Account thisStore : [SELECT Id, Name FROM Account WHERE Name =: storeLocationName]) {                     
                m.put(relatedStoreLocationId, thisStore.Name);    
                m.put(relatedStoreLocationLKID, thisStore.Id);    
            }
            
            if (m.get(relatedStoreLocationId) == null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, RMS_errorMessages.STORE_LOCATION_IS_BLANK_USER)); //
                return null;
            }

        } else {
            m.put('RecordType', UtilityMethods.RecordTypeFor('Labor__c', 'Labor'));
            theLabor.RecordTypeId = UtilityMethods.RecordTypeFor('Labor__c', 'Labor');            

            for (WorkOrder theWorkOrder : [SELECT Store_Location__c, Store_Location_Id__c, Store_Allowable_Percentage__c, Order_Retail_Amount__c FROM WorkOrder WHERE Id =: theLabor.Related_FSL_Work_Order__c]) {               
                if (theWorkOrder.Store_Location_Id__c == null) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, RMS_errorMessages.STORE_LOCATION_IS_BLANK_LABOR)); //
                    return null;
                } 
                else { 
                    m.put(relatedStoreLocationId, theWorkOrder.Store_Location__c);    
                    m.put(relatedStoreLocationLKID, theWorkOrder.Store_Location_Id__c);  
                }
                theLabor.Store_Location__c = theWorkOrder.Store_Location_Id__c;
            }        
        }
        m.put('nooverride', '1');
        if(theLabor.Related_FSL_Work_Order__c == null){
        return p;
        }
        if(theLabor.Related_FSL_Work_Order__c != null){
        PageReference pr=new PageReference ('/apex/RMS_LaborCustomEdit?StoreLocation='+theLabor.Store_Location__c+'&RecordType='+theLabor.RecordTypeId+'&RelatedWorkOrder='+theLabor.Related_FSL_Work_Order__c);
        return pr;
        }
        return null; 
    }
    
    }