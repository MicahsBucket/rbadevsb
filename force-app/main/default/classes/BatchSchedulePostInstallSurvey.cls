/**
 * @description       : 
 * @author            : steven.tremblay@andersencorp.com
 * @group             : 
 * @last modified on  : 03-26-2021
 * @last modified by  : steven.tremblay@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-26-2021   steven.tremblay@andersencorp.com   CHanged batch size to 1
**/
global class BatchSchedulePostInstallSurvey implements Schedulable {
    global void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
       
        // We now call the batch class to be scheduled
        BatchJobPostInstallationSurvey obj = new BatchJobPostInstallationSurvey ();
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(obj, 1);
    }
   
}