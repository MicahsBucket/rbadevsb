@isTest
private without sharing class FSLRollupRolldownControllerTest {
    
 // RBA - Mark R - commenting out class as part of Demand Chain - Rollup rolldown update 10/2020 - deprecated. do not delete save for reference.
    
 /*   
    private static final String ACCOUNT_NAME = 'RbA Test Dwelling';
    private static final String MUNICIPALITY_NAME = 'Unit Test Municipality';
    private static final String APPLICATION_NOTES = 'Test Notes';
    private static final String MUNICIPALITY_CONTACT_NAME = 'Unit Test Municipality Contact';
    private static final String RESOURCE1_FIRST_NAME = 'Kaylee';
    private static final String RESOURCE1_LAST_NAME = 'Torres';
    private static final String RESOURCE2_FIRST_NAME = 'Harry';
    private static final String RESOURCE2_LAST_NAME = 'Hanson';
    private static final String WORK_TYPE_NAME = 'Test Type 1';
    private static final String INSTALL_WORK_TYPE_NAME = 'Install';
    private static final String MEASURE_WORK_TYPE_NAME = 'Measure';
    private static final String SERVICE_WORK_TYPE_NAME = 'Service';
    private static final String JOB_SITE_VISIT_WORK_TYPE_NAME = 'Job Site Visit';
    private static final String ACCOUNT_DESCRIPTION = 'Test Account Description';

    private static Municipality_Contact__c mc1 = new Municipality_Contact__c();

// ****** NOTE: Test setup code based off code from ServiceAppointmentTriggerTest.cls ******* //
    @TestSetup
    static void testSetup() {
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Store_Configuration__c storeConfig = createStoreConfig();
        insert storeConfig;

        Back_Office_Checklist_Configuration__c backOfficeChecklist1 = new Back_Office_Checklist_Configuration__c(
            Store_Configuration__c = storeConfig.id,
            Contract_Signed__c = true,
            Lien_Rights_Signed__c = true
            );
        insert backOfficeChecklist1;

        WorkType workType = new WorkType(
            Name = WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 4,
            DurationType = 'Hours'
        );
        
        WorkType installWorkType = new WorkType(
            Name = INSTALL_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType measureWorkType = new WorkType(
            Name = MEASURE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType serviceWorkType = new WorkType(
            Name = SERVICE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );

        WorkType jobSiteVisitWorkType = new WorkType(
            Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 1,
            DurationType = 'Hours'
        );
        insert new List<WorkType>{worktype, installWorkType, measureWorkType, serviceWorkType, jobSiteVisitWorkType};

        Account store = [SELECT Id
                         FROM Account
                         WHERE Id =: storeConfig.Store__c];
        store.Active_Store_Configuration__c = storeConfig.Id;
        update store;

        Contact testContact1 = new Contact();
        testContact1.LastName = 'Test Last Name One';
        insert testContact1;

        User user1 = createUser(RESOURCE1_FIRST_NAME, RESOURCE1_LAST_NAME);
        User user2 = createUser(RESOURCE2_FIRST_NAME, RESOURCE2_LAST_NAME);
        insert new List<User> { user1, user2 };

        ServiceResource resource1 = createServiceResource(user1, storeConfig);
        ServiceResource resource2 = createServiceResource(user2, storeConfig);
        insert new List<ServiceResource> { resource1, resource2 };

        Account acc = new Account();
        id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        acc.Name = ACCOUNT_NAME;
        acc.RecordTypeId = dwellingRT;
        acc.ShippingStreet = '123 Fake street';
        acc.ShippingCity = 'Milwaukee';
        acc.ShippingState = 'Wisconsin';
        acc.ShippingPostalCode = '55555';
        acc.ShippingCountry = 'United States';
        acc.Store_Location__c = storeConfig.Store__c;
        acc.Description = ACCOUNT_DESCRIPTION;
        insert acc;

        String pricebookId = Test.getStandardPricebookId();

        Product2 prod = new Product2();
        prod.Name = 'Test Product 1';
        prod.IsActive = true;
        insert prod;

        Product2 pro2 = new Product2();
        pro2.Name = 'Test Product 2';
        pro2.IsActive = true;
        insert pro2;

        PricebookEntry pe = new PricebookEntry();
        pe.IsActive = true;
        pe.Pricebook2Id = pricebookId;
        pe.Product2Id = prod.Id;
        pe.UnitPrice = 45;
        insert pe;

        PricebookEntry pe2 = new PricebookEntry();
        pe2.IsActive = true;
        pe2.Pricebook2Id = pricebookId;
        pe2.Product2Id = pro2.Id;
        pe2.UnitPrice = 45;
        insert pe2;

        Order testOrder =  new Order();
        testOrder.Name ='Sold Order 1';
        testOrder.AccountId = acc.Id;
        testOrder.EffectiveDate = Date.Today();
        testOrder.Status = 'Draft';
        testOrder.Pricebook2Id = pricebookId;
        testOrder.Customer_Pickup_All__c = FALSE;
        testOrder.Installation_Date__c = system.today()-1;
        //testOrder.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Record Type').getRecordTypeId(); // Commented out 1-22-2019 due to test errors on all test methods while running in QA:
        /* STACK TRACE 
        Class.RMS_WorkOrderCreationManager.createWorkOrderOnOrderActivationFuture: line 438, column 1

        MESSAGE 
        System.DmlException: Insert failed. First exception on row 0; first error: INVALID_CROSS_REFERENCE_KEY, Owner ID: owner cannot be blank: [OwnerId] */
        //testOrder.TestOrderField__c = 'TestValue123';
        /*
        testOrder.Description = 'Test Order Description';
        insert testOrder;
        testOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Id = :testOrder.Id];

        OrderItem item = new OrderItem();
        item.PricebookEntryId = pe.Id;
        item.OrderId = testOrder.Id;
        item.Product2Id = prod.Id;
        item.Quantity = 2;
        item.UnitPrice = 45;
        insert item;

        OrderItem item2 = new OrderItem();
        item2.PricebookEntryId = pe2.Id;
        item2.OrderId = testOrder.Id;
        item2.Product2Id = pro2.Id;
        item2.Quantity = 2;
        item2.UnitPrice = 45;
        insert item2;

        RbA_Skills__c rs = new RbA_Skills__c();
        rs.Name='Test Window';
        insert rs;

        Product_Skill__c ps = new Product_Skill__c();
        ps.Product__c = prod.Id;
        ps.FSL_Skill_ID__c = prod.Id;
        ps.RbA_Skill__c = rs.Id;
        insert ps;

        Product_Skill__c ps2 = new Product_Skill__c();
        ps2.Product__c = pro2.Id;
        ps2.FSL_Skill_ID__c = pro2.Id;
        ps2.RbA_Skill__c = rs.Id;
        insert ps2;

        Product_Skill__c ps3 = new Product_Skill__c();
        ps3.Product__c = pro2.Id;
        ps3.FSL_Skill_ID__c = prod.Id;
        ps3.RbA_Skill__c = rs.Id;
        insert ps3;

        Municipality__c m1 = new Municipality__c();
        m1.Name = MUNICIPALITY_NAME;
        m1.Application_Notes__c = APPLICATION_NOTES;
        insert m1;

        mc1.Name = MUNICIPALITY_CONTACT_NAME;
        mc1.Active__c = true;
        mc1.Municipality__c = m1.Id;
        insert mc1;

        OperatingHours oh = new OperatingHours();
        oh.Name = 'Test Operating Hours';
        insert oh;

        ServiceTerritory st1 = new ServiceTerritory();
        st1.Name = 'Test Service Territory';
        st1.OperatingHoursId = oh.Id;
        st1.IsActive = true;
        insert st1;

        Service_Territory_Zip_Code__c serviceZipCode1 = new Service_Territory_Zip_Code__c();
        serviceZipCode1.Name = '53211';
        serviceZipCode1.Service_Territory__c = st1.Id;
        serviceZipCode1.WorkType__c = 'Other';
        insert serviceZipCode1;

        Service_Territory_Zip_Code__c serviceZipCode2 = new Service_Territory_Zip_Code__c();
        serviceZipCode2.Name = '53132';
        serviceZipCode2.Service_Territory__c = st1.Id;
        serviceZipCode2.WorkType__c = 'Service';
        insert serviceZipCode2;

        WorkOrder wo = UtilityMethods.buildWorkOrder(acc, testOrder, Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId(), testOrder.ownerId, 'Service', m1.Id);
        wo.Description = 'Test Work Order Description';
        wo.Number_of_Assigned_Service_Appointments__c = 0;
        wo.DurationType = 'Hours';
        wo.Duration = 1;
        insert wo;
    }



    @isTest
    static void test_triggerFromServiceAppointment() {
        FSLRollupRolldownController.addRolldown('Account', 'Order', 'Description', 'Description');
        FSLRollupRolldownController.addRolldown('Order', 'WorkOrder', 'Description', 'Description');
        FSLRollupRolldownController.addRolldown('WorkOrder', 'ServiceAppointment', 'Description', 'Description');

        Test.startTest();
            Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
            Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account WHERE Name = :ACCOUNT_NAME];
            WorkOrder wo = [SELECT Id, Sold_Order__c, Account.ShippingStreet, Account.ShippingState, Account.ShippingCity, Account.ShippingPostalCode, Account.ShippingCountry,Number_of_Assigned_Service_Appointments__c FROM WorkOrder WHERE AccountId = :acc.Id LIMIT 1];
            Contact c = [SELECT Id FROM Contact LIMIT 1];

            List<ServiceAppointment> sasToInsert = new List<ServiceAppointment>();

            WorkOrder result;
            ServiceAppointment appt1 = new ServiceAppointment();
            appt1.ContactId = c.Id;
            appt1.ParentRecordId = wo.Id;
            appt1.SchedStartTime = System.now().addDays(7);
            appt1.ArrivalWindowStartTime = System.now().addDays(7);
            appt1.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
            appt1.Status = 'Scheduled';
            //appt1.Description = 'Description 1';
            sasToInsert.add(appt1);

            ServiceAppointment appt2 = new ServiceAppointment();
            appt2.ContactId = c.Id;
            appt2.ParentRecordId = wo.Id;
            appt2.SchedStartTime = System.now().addDays(8);
            appt2.ArrivalWindowStartTime = System.now().addDays(8);
            appt2.ArrivalWindowEndTime = System.now().addDays(8).addHours(2);
            appt2.Status = 'Scheduled';
            //appt2.Description = 'Description 2';
            sasToInsert.add(appt2);

            FSLRollupRolldownController.resetRepetitionCounter();
            insert sasToInsert;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            System.assertEquals('Scheduled & Assigned', result.Status);

            appt1.Status = 'Dispatched';
            update appt1;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            System.assertEquals('Scheduled & Assigned', result.Status);

            appt1.Status = 'In Progress';
            update appt1;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            System.assertEquals('Scheduled & Assigned', result.Status);
        
            ServiceAppointment a1 = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c FROM ServiceAppointment WHERE Id = :appt1.Id];
            ServiceAppointment a2 = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c FROM ServiceAppointment WHERE Id = :appt2.Id];

            Map<Id, ServiceAppointment> newMap = new Map<Id, ServiceAppointment>();
            newMap.put(a1.Id, a1);
            newMap.put(a2.Id, a2);

            FSLRollupRolldownController.resetRepetitionCounter();
            FSLRollupRolldownController.triggerFromServiceAppointment(newMap, newMap, FSLRollupRolldownController.TriggerOperationEnum.IS_UPDATE);

        Test.stopTest();

        Order orderResult = [SELECT Id, Description FROM Order WHERE Id = :mainOrder.Id];
        WorkOrder woResult = [SELECT Id, Status, Scheduled_Start_Time__c, Scheduled_End_Time__c, Duration, Description FROM WorkOrder WHERE Id = :wo.Id];
        ServiceAppointment a1Result = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c, Description FROM ServiceAppointment WHERE Id = :appt1.Id];
        ServiceAppointment a2Result = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c, Description FROM ServiceAppointment WHERE Id = :appt2.Id];
        
        if (FSLRollupRolldownController.ENABLE_UPDATES_ON_ORDER) {System.assertEquals(ACCOUNT_DESCRIPTION, orderResult.Description);
        }
        //System.assertEquals(ACCOUNT_DESCRIPTION, woResult.Description);
        //System.assertEquals('Test Work Order Description', a1Result.Description);
        //System.assertEquals('Test Work Order Description', a2Result.Description);

        system.assertEquals(a1Result.SchedStartTime.addHours(1), a1Result.SchedEndTime); // check that appt start/end times ended up correct
        system.assertEquals(a2Result.SchedStartTime.addHours(1), a2Result.SchedEndTime);

        system.assertEquals(1, a1Result.Duration); // check that the SA durations ended up correct
        system.assertEquals(1, a2Result.Duration);

        system.assertEquals(a1Result.SchedStartTime,woResult.Scheduled_Start_Time__c); // check that the times were rolled up correctly to the work order
        system.assertEquals(a2Result.SchedEndTime,woResult.Scheduled_End_Time__c);
    }
    

    @isTest
    static void test_triggerFromWorkOrder() {
        FSLRollupRolldownController.addRolldown('Account', 'Order', 'Description', 'Description');
        FSLRollupRolldownController.addRolldown('Order', 'WorkOrder', 'Description', 'Description');
        FSLRollupRolldownController.addRolldown('WorkOrder', 'ServiceAppointment', 'Description', 'Description');

        Test.startTest();
            Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
            Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account WHERE Name = :ACCOUNT_NAME];
            WorkOrder wo = [SELECT Id, Sold_Order__c, Account.ShippingStreet, Account.ShippingState, Account.ShippingCity, Account.ShippingPostalCode, Account.ShippingCountry FROM WorkOrder WHERE AccountId = :acc.Id LIMIT 1];
            Contact c = [SELECT Id FROM Contact LIMIT 1];

            List<ServiceAppointment> sasToInsert = new List<ServiceAppointment>();

            WorkOrder result;
            ServiceAppointment appt1 = new ServiceAppointment();
            appt1.ContactId = c.Id;
            appt1.ParentRecordId = wo.Id;
            appt1.SchedStartTime = System.now().addDays(7);
            appt1.ArrivalWindowStartTime = System.now().addDays(7);
            appt1.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
            appt1.Status = 'Scheduled';
            //appt1.Description = 'Description 1';
            sasToInsert.add(appt1);

            ServiceAppointment appt2 = new ServiceAppointment();
            appt2.ContactId = c.Id;
            appt2.ParentRecordId = wo.Id;
            appt2.SchedStartTime = System.now().addDays(8);
            appt2.ArrivalWindowStartTime = System.now().addDays(8);
            appt2.ArrivalWindowEndTime = System.now().addDays(8).addHours(2);
            appt2.Status = 'Scheduled';
            //appt2.Description = 'Description 2';
            sasToInsert.add(appt2);

            FSLRollupRolldownController.resetRepetitionCounter();
            insert sasToInsert;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            System.assertEquals('Scheduled & Assigned', result.Status);

            appt1.Status = 'Dispatched';
            update appt1;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            System.assertEquals('Scheduled & Assigned', result.Status);

            appt1.Status = 'In Progress';
            update appt1;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            System.assertEquals('Scheduled & Assigned', result.Status);
        
            ServiceAppointment a1 = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c FROM ServiceAppointment WHERE Id = :appt1.Id];
            ServiceAppointment a2 = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c FROM ServiceAppointment WHERE Id = :appt2.Id];

            Map<Id, WorkOrder> newMap = new Map<Id, WorkOrder>();
            newMap.put(wo.Id, wo);

            FSLRollupRolldownController.resetRepetitionCounter();
            FSLRollupRolldownController.triggerFromWorkOrder(new Map<Id, WorkOrder>(), newMap, FSLRollupRolldownController.TriggerOperationEnum.IS_UPDATE);

        Test.stopTest();

        Order orderResult = [SELECT Id, Description FROM Order WHERE Id = :mainOrder.Id];
        WorkOrder woResult = [SELECT Id, Status, Scheduled_Start_Time__c, Scheduled_End_Time__c, Duration, Description FROM WorkOrder WHERE Id = :wo.Id];
        ServiceAppointment a1Result = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c, Description FROM ServiceAppointment WHERE Id = :appt1.Id];
        ServiceAppointment a2Result = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c, Description FROM ServiceAppointment WHERE Id = :appt2.Id];
        
        if (FSLRollupRolldownController.ENABLE_UPDATES_ON_ORDER) {System.assertEquals(ACCOUNT_DESCRIPTION, orderResult.Description);
        }
        System.assertEquals(ACCOUNT_DESCRIPTION, woResult.Description);
        System.assertEquals(ACCOUNT_DESCRIPTION, a1Result.Description);
        System.assertEquals(ACCOUNT_DESCRIPTION, a2Result.Description);
    }


    @isTest
    static void test_triggerFromOrder() {
        FSLRollupRolldownController.addRolldown('Account', 'Order', 'Description', 'Description');
        FSLRollupRolldownController.addRolldown('Order', 'WorkOrder', 'Description', 'Description');
        FSLRollupRolldownController.addRolldown('WorkOrder', 'ServiceAppointment', 'Description', 'Description');

        Test.startTest();
            Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
            Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account WHERE Name = :ACCOUNT_NAME];
            WorkOrder wo = [SELECT Id, Sold_Order__c, Account.ShippingStreet, Account.ShippingState, Account.ShippingCity, Account.ShippingPostalCode, Account.ShippingCountry FROM WorkOrder WHERE AccountId = :acc.Id LIMIT 1];
            Contact c = [SELECT Id FROM Contact LIMIT 1];

            List<ServiceAppointment> sasToInsert = new List<ServiceAppointment>();

            WorkOrder result;
            ServiceAppointment appt1 = new ServiceAppointment();
            appt1.ContactId = c.Id;
            appt1.ParentRecordId = wo.Id;
            appt1.SchedStartTime = System.now().addDays(7);
            appt1.ArrivalWindowStartTime = System.now().addDays(7);
            appt1.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
            appt1.Status = 'Scheduled';
            //appt1.Description = 'Description 1';
            sasToInsert.add(appt1);

            ServiceAppointment appt2 = new ServiceAppointment();
            appt2.ContactId = c.Id;
            appt2.ParentRecordId = wo.Id;
            appt2.SchedStartTime = System.now().addDays(8);
            appt2.ArrivalWindowStartTime = System.now().addDays(8);
            appt2.ArrivalWindowEndTime = System.now().addDays(8).addHours(2);
            appt2.Status = 'Scheduled';
            //appt2.Description = 'Description 2';
            sasToInsert.add(appt2);

            FSLRollupRolldownController.resetRepetitionCounter();
            insert sasToInsert;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            System.assertEquals('Scheduled & Assigned', result.Status);

            appt1.Status = 'Dispatched';
            update appt1;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            System.assertEquals('Scheduled & Assigned', result.Status);

            appt1.Status = 'In Progress';
            update appt1;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            System.assertEquals('Scheduled & Assigned', result.Status);
        
            ServiceAppointment a1 = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c FROM ServiceAppointment WHERE Id = :appt1.Id];
            ServiceAppointment a2 = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c FROM ServiceAppointment WHERE Id = :appt2.Id];

            Map<Id, Order> newMap = new Map<Id, Order>();
            newMap.put(mainOrder.Id, mainOrder);

            FSLRollupRolldownController.resetRepetitionCounter();
            FSLRollupRolldownController.triggerFromOrder(new Map<Id, Order>(), newMap, FSLRollupRolldownController.TriggerOperationEnum.IS_UPDATE);

        Test.stopTest();

        Order orderResult = [SELECT Id, Description FROM Order WHERE Id = :mainOrder.Id];
        WorkOrder woResult = [SELECT Id, Status, Scheduled_Start_Time__c, Scheduled_End_Time__c, Duration, Description FROM WorkOrder WHERE Id = :wo.Id];
        ServiceAppointment a1Result = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c, Description FROM ServiceAppointment WHERE Id = :appt1.Id];
        ServiceAppointment a2Result = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c, Description FROM ServiceAppointment WHERE Id = :appt2.Id];
        
        if (FSLRollupRolldownController.ENABLE_UPDATES_ON_ORDER) {System.assertEquals(ACCOUNT_DESCRIPTION, orderResult.Description);
        }
        System.assertEquals(ACCOUNT_DESCRIPTION, woResult.Description);
        //System.assertEquals('Test Work Order Description', a1Result.Description);
        //System.assertEquals('Test Work Order Description', a2Result.Description);
    }
    @isTest
    static void test_triggerFromAccount() {
        FSLRollupRolldownController.addRolldown('Account', 'Order', 'Description', 'Description');
        FSLRollupRolldownController.addRolldown('Order', 'WorkOrder', 'Description', 'Description');
        FSLRollupRolldownController.addRolldown('WorkOrder', 'ServiceAppointment', 'Description', 'Description');

        Test.startTest();
            Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
            Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, RecordTypeId From Account WHERE Name = :ACCOUNT_NAME];
            WorkOrder wo = [SELECT Id, Sold_Order__c, Account.ShippingStreet, Account.ShippingState, Account.ShippingCity, Account.ShippingPostalCode, Account.ShippingCountry FROM WorkOrder WHERE AccountId = :acc.Id LIMIT 1];
            Contact c = [SELECT Id FROM Contact LIMIT 1];

            List<ServiceAppointment> sasToInsert = new List<ServiceAppointment>();

            WorkOrder result;
            ServiceAppointment appt1 = new ServiceAppointment();
            appt1.ContactId = c.Id;
            appt1.ParentRecordId = wo.Id;
            appt1.SchedStartTime = System.now().addDays(7);
            appt1.ArrivalWindowStartTime = System.now().addDays(7);
            appt1.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
            appt1.Status = 'Scheduled';
            //appt1.Description = 'Description 1';
            sasToInsert.add(appt1);

            ServiceAppointment appt2 = new ServiceAppointment();
            appt2.ContactId = c.Id;
            appt2.ParentRecordId = wo.Id;
            appt2.SchedStartTime = System.now().addDays(8);
            appt2.ArrivalWindowStartTime = System.now().addDays(8);
            appt2.ArrivalWindowEndTime = System.now().addDays(8).addHours(2);
            appt2.Status = 'Scheduled';
            //appt2.Description = 'Description 2';
            sasToInsert.add(appt2);

            FSLRollupRolldownController.resetRepetitionCounter();
            insert sasToInsert;
            
            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            System.assertEquals('Scheduled & Assigned', result.Status);
            
            appt1.Status = 'Dispatched';
            update appt1;

            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            System.assertEquals('Scheduled & Assigned', result.Status);
        
            appt1.Status = 'In Progress';
            update appt1;
            
            result = [SELECT Id, Status FROM WorkOrder WHERE Id = :wo.Id];
            System.assertEquals('Scheduled & Assigned', result.Status);
        
            //ServiceAppointment a1 = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c FROM ServiceAppointment WHERE Id = :appt1.Id];
            //ServiceAppointment a2 = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c FROM ServiceAppointment WHERE Id = :appt2.Id];

            Map<Id, Account> newMap = new Map<Id, Account>();
            newMap.put(acc.Id, acc);

            FSLRollupRolldownController.resetRepetitionCounter();
            FSLRollupRolldownController.triggerFromAccount(new Map<Id, Account>(), newMap, FSLRollupRolldownController.TriggerOperationEnum.IS_UPDATE);
        Test.stopTest();

        Order orderResult = [SELECT Id, Description FROM Order WHERE Id = :mainOrder.Id];
        WorkOrder woResult = [SELECT Id, Status, Scheduled_Start_Time__c, Scheduled_End_Time__c, Duration, Description FROM WorkOrder WHERE Id = :wo.Id];
        //ServiceAppointment a1Result = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c, Description FROM ServiceAppointment WHERE Id = :appt1.Id];
        //ServiceAppointment a2Result = [SELECT Id, ParentRecordId, ParentRecordType, SchedStartTime, SchedEndTime, Duration, Work_Order__c, Description FROM ServiceAppointment WHERE Id = :appt2.Id];
        
        if (FSLRollupRolldownController.ENABLE_UPDATES_ON_ORDER) {System.assertEquals(ACCOUNT_DESCRIPTION, orderResult.Description);
        }
        System.assertEquals(ACCOUNT_DESCRIPTION, woResult.Description);
        //System.assertEquals('Test Work Order Description', a1Result.Description);
        //System.assertEquals('Test Work Order Description', a2Result.Description);
    }

    @isTest
    static void test_triggerFromAccount1() {
        FSLRollupRolldownController.addRolldown('Account', 'Order', 'Description', 'Description');
        FSLRollupRolldownController.addRolldown('Order', 'WorkOrder', 'Description', 'Description');
        FSLRollupRolldownController.addRolldown('WorkOrder', 'ServiceAppointment', 'Description', 'Description');

        Test.startTest();
            //Order mainOrder = [SELECT Id, AccountId, Ownerid, type, OpportunityId, BillToContactId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
            Account acc = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, RecordTypeId From Account WHERE Name = :ACCOUNT_NAME];
            WorkOrder wo = [SELECT Id, Sold_Order__c, Account.ShippingStreet, Account.ShippingState, Account.ShippingCity, Account.ShippingPostalCode, Account.ShippingCountry FROM WorkOrder WHERE AccountId = :acc.Id LIMIT 1];
            Contact c = [SELECT Id FROM Contact LIMIT 1];

            List<ServiceAppointment> sasToInsert = new List<ServiceAppointment>();

            WorkOrder result;
            ServiceAppointment appt1 = new ServiceAppointment();
            appt1.ContactId = c.Id;
            appt1.ParentRecordId = wo.Id;
            appt1.SchedStartTime = System.now().addDays(7);
            appt1.ArrivalWindowStartTime = System.now().addDays(7);
            appt1.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
            appt1.Status = 'Scheduled';
            //appt1.Description = 'Description 1';
            sasToInsert.add(appt1);

            ServiceAppointment appt2 = new ServiceAppointment();
            appt2.ContactId = c.Id;
            appt2.ParentRecordId = wo.Id;
            appt2.SchedStartTime = System.now().addDays(8);
            appt2.ArrivalWindowStartTime = System.now().addDays(8);
            appt2.ArrivalWindowEndTime = System.now().addDays(8).addHours(2);
            appt2.Status = 'Scheduled';
            //appt2.Description = 'Description 2';
            sasToInsert.add(appt2);

            FSLRollupRolldownController.resetRepetitionCounter();
            insert sasToInsert;
            
            acc.ShippingStreet = '456 street';
            acc.ShippingCity = 'Milwaukee';
            acc.ShippingState = 'Wisconsin';
            acc.ShippingPostalCode = '66566';
            FSLRollupRolldownController.resetRepetitionCounter();
            update acc;
        Test.stopTest();
    }

    @isTest
    static void test11(){
        FSLRollupRolldownCustomLogic.rollupOrderToAccount(new FSLRollupRolldownController.ObjectMapContainer(),new FSLRollupRolldownController.ObjectNodeContainer());
    }

    @isTest
    static void test_logException() {
  
        Account acc = [SELECT Id From Account WHERE Name = :ACCOUNT_NAME LIMIT 1];
        WorkOrder wo = [SELECT Id FROM WorkOrder WHERE AccountId = :acc.Id LIMIT 1];
        Contact c = [SELECT Id FROM Contact LIMIT 1];

        ServiceAppointment appt1 = new ServiceAppointment();
        appt1.ContactId = c.Id;
        appt1.ParentRecordId = wo.Id;
        appt1.SchedStartTime = System.now().addDays(7);
        appt1.ArrivalWindowStartTime = System.now().addDays(7);
        appt1.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
        appt1.Status = 'Scheduled';

        Test.startTest();
            delete [SELECT Id FROM Rollup_Rolldown_Controller_Log__c]; // clear the logs before testing
            FSLRollupRolldownController.forceUnhandledErrorForTesting = true;
            insert appt1;
        Test.stopTest();

        List<Rollup_Rolldown_Controller_Log__c> logEntries = [
            SELECT Log_Entry_Type__c, Controller_Run_Id__c, Object_Type__c, Object_JSON__c, Error_Message__c
            FROM Rollup_Rolldown_Controller_Log__c];

        System.assertEquals(1,logEntries.size());
        System.assertEquals('UNHANDLED ERROR',logEntries[0].Log_Entry_Type__c);
    }

// ******** Static methods based off methods copied from ServiceAppointmentTriggerTest.cls ******* //

    private static User createUser(String firstName, String lastName) {
        User u = new User(
            FirstName = firstName,
            LastName = lastName,
            Email = firstName + '.' + lastName + '@example.com',
            UserName = firstName + '.' + lastName + '@example.com' + (Math.random() * 1000000).intValue(),
            CompanyName = 'ABC Company',
            Alias = 'tuser',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            TimeZoneSidKey = 'America/Chicago',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        return u;
    }

    private static Store_Configuration__c createStoreConfig() {
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008

        );
        insert store;

        Store_Configuration__c storeConfig = new Store_Configuration__c(
            Store__c = store.Id,
            Order_Number__c = 1,
            Tech_Measure_Work_Order_Queue_Id__c = [SELECT Id FROM Group WHERE  Type = 'Queue' AND NAME = 'Tech Measure WO - Twin Cities' LIMIT 1].id
        );

        return storeConfig;
    }

    private static ServiceResource createServiceResource(User u, Store_Configuration__c storeConfig) {
        ServiceResource sr = new ServiceResource(
            Name = u.LastName,
            RelatedRecordId = u.Id,
            Retail_Location__c = storeConfig.Id,
            IsActive = true
        );
        return sr;
    }
    */
}