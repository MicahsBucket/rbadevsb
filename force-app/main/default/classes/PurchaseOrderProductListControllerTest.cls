/*
* @author Jason Flippen
* @date 02/10/2020
* @description Test Class for the following classes:
*              - PurchaseOrderProductListController
*/
@isTest
public class PurchaseOrderProductListControllerTest {

    /*
    * @author Jason Flippen
    * @date 02/10/2020
    * @description Method to create data to be consumed by test methods.
    */
    @testSetup
    static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        Account testVenderAccount = testUtility.createVendorAccount('Vendor');
        insert testVenderAccount;

        Account testStoreAccount = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAcct = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAcct.Store_Location__c = testStoreAccount.Id;
        insert testDwellingAcct;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAcct.Id, 'Closed - Won');
        insert testOpportunity;
    
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;

        List<Product2> testProductList = new List<Product2>();
        Product2 testProduct01 = new Product2(Name = 'Test Product 01',
                                              RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId(),
                                              Vendor__c = testVenderAccount.Id,
                                              Cost_PO__c = true,
                                              IsActive = true,
                                              Account_Number__c = testFan.Id);
        testProductList.add(testProduct01);
        Product2 testProduct02 = new Product2(Name = 'Test Product 02',
                                              RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId(),
                                              Vendor__c = testVenderAccount.Id,
                                              Cost_PO__c = true,
                                              IsActive = true,
                                              Account_Number__c = testFan.Id);
        testProductList.add(testProduct02);
        Product2 testProduct03 = new Product2(Name = 'Test Product 03',
                                              RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Door_Components').getRecordTypeId(),
                                              Family = 'Parts and Accessories',
                                              Vendor__c = testVenderAccount.Id,
                                              Cost_PO__c = true,
                                              IsActive = true,
                                              Account_Number__c = testFan.Id);
        testProductList.add(testProduct03);
        insert testProductList;

/*
        Pricebook2 testCustomPricebook = testUtility.createPricebook2Name('Custom Price Book');
        insert testCustomPricebook;
*/

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard01 = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct01.Id);
        testPBEList.add(testPBEStandard01);
//        PricebookEntry testPBE01 = testUtility.createPricebookEntry(testCustomPricebook.Id, testProduct01.Id);
//        testPBEList.add(testPBE01);
        PricebookEntry testPBEStandard02 = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct02.Id);
        testPBEList.add(testPBEStandard02);
//        PricebookEntry testPBE02 = testUtility.createPricebookEntry(testCustomPricebook.Id, testProduct02.Id);
//        testPBEList.add(testPBE02);
        PricebookEntry testPBEStandard03 = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct03.Id);
        testPBEList.add(testPBEStandard03);
//        PricebookEntry testPBE03 = testUtility.createPricebookEntry(testCustomPricebook.Id, testProduct03.Id);
//        testPBEList.add(testPBE03);
        insert testPBEList;
        
        List<Order> testOrderList = new List<Order>();
        Order testOrder01 = new Order(Name = 'Test Order 01',
                                      RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Service').getRecordTypeId(),
                                      AccountId = testDwellingAcct.Id,
                                      EffectiveDate = Date.Today(),
                                      Store_Location__c = testStoreAccount.Id,
                                      OpportunityId = testOpportunity.Id,
                                      Status = 'Draft',
                                      Tech_Measure_Status__c = 'New',
                                      Pricebook2Id = Test.getStandardPricebookId());
        testOrderList.add(testOrder01);
        Order testOrder02 = new Order(Name = 'Test Order 02',
                                      RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Service').getRecordTypeId(),
                                      AccountId = testDwellingAcct.Id,
                                      EffectiveDate = Date.Today(),
                                      Store_Location__c = testStoreAccount.Id,
                                      OpportunityId = testOpportunity.Id,
                                      Status = 'Draft',
                                      Tech_Measure_Status__c = 'New',
                                      Pricebook2Id = Test.getStandardPricebookId());
        testOrderList.add(testOrder02);
        insert testOrderList;
        
        List<Purchase_Order__c> testPurchaseOrderList = new List<Purchase_Order__c>();
        Purchase_Order__c testPurchaseOrder01 = new Purchase_Order__c(Name = 'ABCD12345',
                                                                      Order__c = testOrder01.Id,
                                                                      RecordTypeId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Service_Purchase_Order').getRecordTypeId(),
                                                                      Store_Location__c = testStoreAccount.Id,
                                                                      Vendor__c = testVenderAccount.Id,
                                                                      Charge_Cost_To__c = 'Manufacturing',
                                                                      Status__c = 'In Process');
        testPurchaseOrderList.add(testPurchaseOrder01);
        Purchase_Order__c testPurchaseOrder02 = new Purchase_Order__c(Name = 'ABCD12345',
                                                                      Order__c = testOrder01.Id,
                                                                      RecordTypeId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Cost_Purchase_Order').getRecordTypeId(),
                                                                      Store_Location__c = testStoreAccount.Id,
                                                                      Vendor__c = testVenderAccount.Id,
                                                                      Charge_Cost_To__c = 'Manufacturing',
                                                                      Status__c = 'In Process');
        testPurchaseOrderList.add(testPurchaseOrder02);
        insert testPurchaseOrderList;

        List<OrderItem> testOIList = new List<OrderItem>();
        OrderItem testOI01 = new OrderItem(OrderId = testOrder01.Id,
                                           Has_PO__c = true,
                                           Purchase_Order__c = testPurchaseOrder01.Id,
                                           PricebookentryId = testPBEStandard01.Id,
                                           Quantity = 2,
                                           UnitPrice = 100);
        testOIList.add(testOI01);
        OrderItem testOI02 = new OrderItem(OrderId = testOrder01.Id,
                                           PricebookentryId = testPBEStandard02.Id,
                                           Quantity = 2,
                                           UnitPrice = 100);
        testOIList.add(testOI02);
        OrderItem testOI03 = new OrderItem(OrderId = testOrder02.Id,
                                           PricebookentryId = testPBEStandard01.Id,
                                           Quantity = 2,
                                           UnitPrice = 100);
        testOIList.add(testOI03);
        insert testOIList;

        List<Charge__c> testChargeList = new List<Charge__c>();
        Charge__c testCharge01 = new Charge__c(Service_Request__c = testOrder01.Id,
                                               Service_Product__c = testOI01.Id,
                                               Charge_Cost_To__c = 'Manufacturing');
        testChargeList.add(testCharge01);
        Charge__c testCharge02 = new Charge__c(Service_Request__c = testOrder01.Id,
                                               Service_Product__c = testOI02.Id,
                                               Charge_Cost_To__c = 'Manufacturing');
        testChargeList.add(testCharge02);
        Charge__c testCharge03 = new Charge__c(Service_Request__c = testOrder02.Id,
                                               Service_Product__c = testOI03.Id,
                                               Charge_Cost_To__c = 'Manufacturing');
        testChargeList.add(testCharge03);
        insert testChargeList;

    }

    /*
    * @author Jason Flippen
    * @date 11/30/2020
    * @description Method to test the "getPurchaseOrderData" & "deleteProduct" methods in the Controller.
    */
    static testMethod void testController_GetPurchaseOrderDataDeleteProduct() {

        Order testOrder = [SELECT Id FROM Order WHERE Name = 'Test Order 01'];
        Id servicePORTId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Service_Purchase_Order').getRecordTypeId();
        Purchase_Order__c testPurchaseOrder = [SELECT Id FROM Purchase_Order__c WHERE Order__c = :testOrder.Id AND RecordTypeId = :servicePORTId];

        Test.startTest();

            // Retrieve the Purchase Order records.
            PurchaseOrderProductListController.PurchaseOrderWrapper purchaseOrder = PurchaseOrderProductListController.getPurchaseOrderData(testPurchaseOrder.Id);
            System.assertEquals(false, purchaseOrder == null);

            // Delete the Product from the Purchase Order.
            String deleteResult = PurchaseOrderProductListController.deleteProduct(purchaseOrder.productList[0].Id);
            System.assertEquals('Delete Product Success', deleteResult, 'Unexpected Delete Result');

        Test.stopTest();

    }

    /*
    * @author Jason Flippen
    * @date 11/30/2020
    * @description Method to test the "getCharges" method in the Controller.
    */
    static testMethod void testController_GetCharges() {

        Account testVendorAccount = [SELECT Id FROM Account WHERE Name = 'VendorTest Account'];
        Order testOrder = [SELECT Id FROM Order WHERE Name = 'Test Order 02'];

        Test.startTest();

            // Retrieve the Charge records related to the Order.
            List<PurchaseOrderProductListController.ChargeWrapper> chargeList = PurchaseOrderProductListController.getCharges(testOrder.Id, testVendorAccount.Id, 'Manufacturing');
            System.assertEquals(1, chargeList.size(), 'Unexpected Charge records retrieved');

        Test.stopTest();

    }

    /*
    * @author Jason Flippen
    * @date 11/30/2020
    * @description Method to test the "saveProducts" method in the Controller.
    */
    static testMethod void testController_SaveProducts() {

        Account testVendorAccount = [SELECT Id FROM Account WHERE Name = 'VendorTest Account'];
        Order testOrder = [SELECT Id FROM Order WHERE Name = 'Test Order 01'];
        Id servicePORTId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Service_Purchase_Order').getRecordTypeId();
        Purchase_Order__c testPurchaseOrder = [SELECT Id FROM Purchase_Order__c WHERE Order__c = :testOrder.Id AND RecordTypeId = :servicePORTId];

        Test.startTest();

            // Retrieve the Purchase Order records.
            PurchaseOrderProductListController.PurchaseOrderWrapper purchaseOrder = PurchaseOrderProductListController.getPurchaseOrderData(testPurchaseOrder.Id);
            System.assertEquals(false, purchaseOrder == null);

            // Retrieve the Charge records related to the Order).
            List<PurchaseOrderProductListController.ChargeWrapper> chargeList = PurchaseOrderProductListController.getCharges(testOrder.Id, testVendorAccount.Id, 'Manufacturing');
            System.assertEquals(1, chargeList.size(), 'Unexpected Charge records retrieved');

            // Save Product to an existing Purchase Order.
            String saveResult = PurchaseOrderProductListController.saveProducts(purchaseOrder, chargeList);
            System.assertEquals('Add Products Success', saveResult, 'Unexpected Save Products result');

        Test.stopTest();

    }

    /*
    * @author Jason Flippen
    * @date 11/30/2020
    * @description Method to test the "fetchPartDetail" & "saveParts" methods in the Controller.
    */
    static testMethod void testController_fetchPartDetailSaveParts() {

        Product2 testProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Product 03'];
        Order testOrder = [SELECT Id FROM Order WHERE Name = 'Test Order 01'];
        Id costPORTId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Cost_Purchase_Order').getRecordTypeId();
        Purchase_Order__c testPurchaseOrder = [SELECT Id FROM Purchase_Order__c WHERE Order__c = :testOrder.Id AND RecordTypeId = :costPORTId];

        Test.startTest();

            // Retrieve the details of a Part.
            PurchaseOrderProductListController.PartWrapper partWrapper = PurchaseOrderProductListController.fetchPartDetail(testProduct.Id, Test.getStandardPricebookId());
            System.assertEquals('Test Product 03', partWrapper.name, 'Unexpected Part Name');

            partWrapper.quantity = 1;
            List<PurchaseOrderProductListController.PartWrapper> partWrapperList = new List<PurchaseOrderProductListController.PartWrapper>();
            partWrapperList.add(partWrapper);

            // Retrieve the Purchase Order records.
            String saveResult = PurchaseOrderProductListController.saveParts(partWrapperList, testPurchaseOrder.Id, testOrder.Id);
            System.assertEquals('Add Parts Success', saveResult, 'Unexpected Save Parts result');

        Test.stopTest();

    }

    @isTest
    public static void updateRemakeProductsTEST(){
        Test.startTest();
            OrderItem oi = [SELECT Id, Multiple_Remakes__c FROM OrderItem LIMIT 1];
            String saveResult = PurchaseOrderProductListController.updateRemakeProducts(oi.Id, true);
            System.assertEquals('Success', saveResult, 'Success');
        Test.stopTest();
    } 

}