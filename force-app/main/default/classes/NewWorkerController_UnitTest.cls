@isTest
private class NewWorkerController_UnitTest {
	@testSetup
	static void setup(){
		TestDataFactory.setUpConfigs();
		Account testStoreAccount = TestDataFactory.createStoreAccount('Test Store');
		Database.insert(testStoreAccount, true);
		Contact testContact = TestDataFactory.createCustomerContact(testStoreAccount.Id);
		Database.insert(testContact, true);
		User partnerUser = TestDataFactory.createPartnerUser('Test', 'Partner RMS-RSR', testContact.Id);
		Database.insert(partnerUser, true);
	}
	
	@isTest static void canSaveWorker() {
		
	}

	@isTest static void canCheckForDuplicates(){
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		test.startTest();
			String surveyEmail = 'test@test.com';
			NewWorkerApexController.saveWorker('test', 'test', '6517469953', surveyEmail, 'Sales Rep');
			String salesRep = NewWorkerApexController.checkForDuplicate(surveyEmail);
			String inactiveEmail = 'inactive@test.com';
			Worker__c workerInactive = TestDataFactory.createWorkers('Inactive', 1, accountId)[0];
			workerInactive.Email__c = inactiveEmail;
			Database.update(workerInactive, true);
			String inactive = NewWorkerApexController.checkForDuplicate(inactiveEmail);
			system.assertEquals(true, inactive.contains('Inactive'));
			system.assertEquals(true, salesRep.contains('Sales Rep'));
		test.stopTest();
	}
	
}