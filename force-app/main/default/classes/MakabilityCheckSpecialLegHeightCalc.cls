/**
 * @File Name          : MakabilityCheckSpecialLegHeightCalc.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 02-24-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/20/2019, 5:32:05 PM   mark.rothermal@andersencorp.com     Initial Version
**/
public without sharing class MakabilityCheckSpecialLegHeightCalc {
    
    /**
    * @description
    * @author mark.rothermal@andersencorp.com | 5/20/2019
    * @param MakabilityRestResource.OrderItem request
    * @return legCalcResult
    */
    public static legCalcResult checkLegCalc(MakabilityRestResource.OrderItem request){
        legCalcResult result = new legCalcResult();
        boolean makabilityPasses = true;
		result.errMessages = new List<string>();
        ProductConfiguration p = request.ProductConfiguration;
        String shape = p.specialtyShape;

        Boolean checkIfChord = true;
        Boolean checkIfCircleTop = false;
        Boolean checkIfSpringline = false;
   
        Double convertedHeight = p.heightInches + Constants.fractionConversionMap.get(p.heightFractions);
        Double convertedWidth = p.widthInches + Constants.fractionConversionMap.get(p.widthFractions);

        system.debug('current request in check leg calc ' + request);

        //**********start makability flow*************\\

        if(checkIfChord){
            if(convertedHeight < convertedWidth / 2){
                if(shape != 'Chord'){
                    makabilityPasses = false;
                    result.errMessages.add('Leg Height Calc - You must select the shape "Chord" for a unit of this size.');
                } 
            } else {
                checkIfCircleTop = true;
            }
        }
        if(checkIfCircleTop){
            if(convertedHeight <= (convertedWidth / 2) + 4){
                if(shape != 'Circle Top'){
                    makabilityPasses = false;
                    result.errMessages.add('Leg Height Calc - You must select the shape "Circle Top" for a unit of this size.');
                } 
            } else {
                checkIfSpringline = true;
            }
        }
        if(checkIfSpringline){
            if(shape != 'SpringLine'){
                makabilityPasses = false;
                result.errMessages.add('Leg Height Calc - You must select the shape "Springline" for a unit of this size.');
            } 
        }


//******************************************************************************\\
        if(makabilityPasses){
            result.productMakable = true;
          //  result.errMessages.add('Leg Height Calc - passed');
            
        } else{
            result.productMakable = false;
        }
        system.debug('special shape result '+ result);
        return result;
    }
//*************************End Makability****************************\\

    public class legCalcResult {
      public  Boolean productMakable {get;set;}
      public  List<String>  errMessages {get;set;}
    }

        
}