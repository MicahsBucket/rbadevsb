/**
*   {Purpose} - Allows batch routine batchAvailabilityReminder to be scheduled
*
*   {Contact} - support@demandchainsystems.com
*				www.demandchainsystems.com
*				612-424-0032
*
*	CHANGE HISTORY
*	=============================================================================
*	Date    	Name             		Description
*	20190211	Eric Gronholz DCS		Created
*	=============================================================================
**/
global class SalesSchedAvailabilityReminderSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        Database.executebatch(new SalesSchedAvailabilityReminderBatch(),200);        
    }
}