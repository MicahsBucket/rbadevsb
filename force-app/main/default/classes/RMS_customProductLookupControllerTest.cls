@isTest
private class RMS_customProductLookupControllerTest {
  
  @isTest static void test_construction() {
    exception myException;
    try{
    Test.setCurrentPageReference(new PageReference('Page.RMS_customProductLookup'));
    
    RMS_customProductLookupController controller = new RMS_customProductLookupController();
    pagereference pr = controller.search();
    string formTag = controller.getFormTag();
    String textBox = controller.getTextBox();
    System.currentPageReference().getParameters().put('miscPriceBookId', Test.getStandardPricebookId());
    pr = controller.search();
    } catch (exception e){
      myException = e;
    }
    system.assertEquals(null, myException);
  }

  
}