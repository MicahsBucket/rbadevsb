@isTest
public with sharing class OrderTriggerHandlerTest {

   
    private static final String ACCOUNT_NAME = 'Test Account';
    private static final String ACCOUNT_NAME2 = 'Test Account2'; 
    private static final String MUNICIPALITY_NAME = 'Unit Test Municipality';
    private static final String APPLICATION_NOTES = 'Test Notes';
    private static final String MUNICIPALITY_CONTACT_NAME = 'Unit Test Municipality Contact';
    private static final String RESOURCE1_FIRST_NAME = 'Bert';
    private static final String RESOURCE1_LAST_NAME = 'Knerkerts';
    private static final Map<String, RecordType> woRecordTypeMap = createWorkOrderRecordTypeMap();
    private static Municipality_Contact__c mc1 = new Municipality_Contact__c();
    
    private static Store_Configuration__c createStoreConfig() {
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;

        Store_Configuration__c storeConfig = new Store_Configuration__c(
            Store__c = store.Id,
            Order_Number__c = 1
        );
        return storeConfig;
    }

    private static ServiceResource createServiceResource(User u, Store_Configuration__c storeConfig) {
        ServiceResource sr = new ServiceResource(
            Name = u.LastName,
            RelatedRecordId = u.Id,
            Retail_Location__c = storeConfig.Id,
            IsActive = true
        );
        return sr;
    }
     private static Map<String, RecordType> createWorkOrderRecordTypeMap() {
        List<RecordType> woRecordTypes = [select Id, DeveloperName from RecordType where sObjectType = 'WorkOrder'];
        Map<String, RecordType> woRecordTypeMap = new Map<String, RecordType>();
        for (RecordType rt : woRecordTypes) {
            woRecordTypeMap.put(rt.DeveloperName, rt);
        }
        return woRecordTypeMap;
    }
    private static User createUser(String firstName, String lastName) {
        User u = new User(
            FirstName = firstName,
            LastName = lastName,
            Email = firstName + '.' + lastName + '@example.com',
            UserName = firstName + '.' + lastName + '@example.com' + (Math.random() * 1000000).intValue(),
            CompanyName = 'ABC Company',
            Alias = 'tuser',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            TimeZoneSidKey = 'America/Chicago',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        return u;
    }
  
    @TestSetup
    static void testSetup() {


        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Store_Configuration__c storeConfig = createStoreConfig();
        insert storeConfig;

        User user1 = createUser(RESOURCE1_FIRST_NAME, RESOURCE1_LAST_NAME);
        insert user1;

        ServiceResource resource1 = createServiceResource(user1, storeConfig);
        insert resource1;

        Account acc = TestDataFactory.createStoreAccount(ACCOUNT_NAME);
        insert acc;
        
		Account acc2 = TestDataFactory.createStoreAccount(ACCOUNT_NAME2);
        insert acc2;
        
        String pricebookId = Test.getStandardPricebookId();

        Product2 prod = new Product2();
        prod.Name = 'Test Product 1';
        prod.IsActive = true;
        insert prod;

        Product2 pro2 = new Product2();
        pro2.Name = 'Test Product 2';
        pro2.IsActive = true;
        insert pro2;

        List<Skill> mySkills = [SELECT Id FROM Skill];


        PricebookEntry pe = new PricebookEntry();
        pe.IsActive = true;
        pe.Pricebook2Id = pricebookId;
        pe.Product2Id = prod.Id;
        pe.UnitPrice = 45;
        insert pe;

        PricebookEntry pe2 = new PricebookEntry();
        pe2.IsActive = true;
        pe2.Pricebook2Id = pricebookId;
        pe2.Product2Id = pro2.Id;
        pe2.UnitPrice = 45;
        insert pe2;

        Order testOrder =  new Order();
        testOrder.Name ='Sold Order 1';
        testOrder.AccountId = acc.Id;
        testOrder.EffectiveDate = Date.Today();
        testOrder.Status = 'Install Complete';
        testOrder.Pricebook2Id = pricebookId;
        testOrder.Customer_Pickup_All__c = FALSE;
        testOrder.Installation_Date__c = system.today()-1;
        testOrder.RecordTypeId = '01261000000SG0rAAG';
        insert testOrder;
        
        Order testOrder2 =  new Order();
        testOrder2.Name ='Sold Order 1';
        testOrder2.AccountId = acc2.Id;
        testOrder2.EffectiveDate = Date.Today();
        testOrder2.Status = 'Install Complete';
        testOrder2.Pricebook2Id = pricebookId;
        testOrder2.Customer_Pickup_All__c = FALSE;
        testOrder2.Installation_Date__c = system.today()-1;
        testOrder2.Notification_Message_Pop_Up__c=true;
        testOrder2.Message_Pop_up__c='testing';
        testOrder2.RecordTypeId = '01261000000SG0rAAG';
        insert testOrder2;

        OrderItem item = new OrderItem();
        item.PricebookEntryId = pe.Id;
        item.OrderId = testOrder.Id;
        item.Product2Id = prod.Id;
        item.Quantity = 2;
        item.UnitPrice = 45;
        insert item;

        OrderItem item2 = new OrderItem();
        item2.PricebookEntryId = pe2.Id;
        item2.OrderId = testOrder.Id;
        item2.Product2Id = pro2.Id;
        item2.Quantity = 2;
        item2.UnitPrice = 45;
        insert item2;
        
        RbA_Skills__c mySkill = new RbA_Skills__c();
        myskill.Install_Duration__c = 5;
        mySkill.Tech_Measure_Duration__c = 5;
        insert myskill;        

        Product_Skill__c ps = new Product_Skill__c();
        ps.Product__c = prod.Id;
        ps.FSL_Skill_ID__c = mySkills[0].Id;
        ps.RBA_Skill__c = mySkill.id;
        insert ps;

        Product_Skill__c ps2 = new Product_Skill__c();
        ps2.Product__c = pro2.Id;
        ps2.FSL_Skill_ID__c = mySkills[0].Id;
        ps2.RBA_Skill__c = mySkill.id;
        insert ps2;

        Product_Skill__c ps3 = new Product_Skill__c();
        ps3.Product__c = pro2.Id;
        ps3.FSL_Skill_ID__c = mySkills[0].Id;
        ps3.RBA_Skill__c = mySkill.id;
        insert ps3;

        Municipality__c m1 = new Municipality__c();
        m1.Name = MUNICIPALITY_NAME;
        m1.Application_Notes__c = APPLICATION_NOTES;
        insert m1;

        Municipality_Contact__c mc1 = new Municipality_Contact__c();
        mc1.Name = MUNICIPALITY_CONTACT_NAME;
        mc1.Active__c = true;
        mc1.Municipality__c = m1.Id;
        insert mc1;
    }

    
    
    
    static testmethod void test_UpdateOrderEstiShipDateToSerApp(){ 
        
   Test.startTest();
        Order mainOrder = [SELECT Id, AccountId, RecordTypeName__c, Arrival_Date__c FROM Order WHERE Account.Name = :ACCOUNT_NAME];
        system.debug('***Pav mainOrder RecType'+mainOrder.RecordTypeName__c);
        WorkOrder wo = new WorkOrder();
        wo.Sold_Order__c = mainOrder.Id;
        wo.AccountId = mainOrder.AccountId;
        wo.RecordTypeId = woRecordTypeMap.get('Install').Id;
		insert WO;
        system.debug('***Pav WO Inserted of Order');
        ServiceAppointment appt = new ServiceAppointment();
        appt.ParentRecordId = wo.Id;
        appt.DurationType = 'Minutes';
        appt.Duration = 120;
        appt.SchedStartTime = Datetime.now().addDays(7);
        appt.SchedEndTime = Datetime.now().addDays(8);
        appt.ArrivalWindowStartTime = Datetime.now().addDays(7);
        appt.ArrivalWindowEndTime = Datetime.now().addDays(7).addHours(2);
        insert appt;
    
//        system.debug('***Pav SoldOrder of SA'+appt.Sold_Order__c);
//        system.debug('***Pav OrderNum of Order'+mainOrder.OrderNumber);
//        system.debug('***Pav Order Sold Order Num of SA'+mainOrder.Sold_Order__r.OrderNumber);
        
        
        mainOrder.Estimated_Ship_Date__c = system.today() + 10;
//        mainOrder.Sold_Order__c = '';
        Update mainOrder;
        

        Test.stopTest();
        appt = [SELECT Id, SchedStartTime, SchedEndTime, First_Available_Date__c FROM ServiceAppointment WHERE Id =: appt.Id LIMIT 1];
        System.assertEquals(appt.First_Available_Date__c, mainOrder.Arrival_Date__c);
    
    }
    static testmethod void CPP_NotificationTestmethod()
    {
        Order mainOrder = [SELECT Id, AccountId, RecordTypeName__c, Notification_Message_Pop_Up__c,Message_Pop_up__c FROM Order WHERE Account.Name = :ACCOUNT_NAME];
        mainOrder.Notification_Message_Pop_Up__c=true;
        mainOrder.Message_Pop_up__c='testing';
        update mainOrder;
            
    }
    static testmethod void CPP_NotificationTestmethod2()
    {
        Order mainOrder = [SELECT Id, AccountId, RecordTypeName__c, Notification_Message_Pop_Up__c,Message_Pop_up__c FROM Order WHERE Account.Name = :ACCOUNT_NAME2];
        System.debug('___ratna____mainOrder__________'+mainOrder); 
        mainOrder.Notification_Message_Pop_Up__c=false;
        mainOrder.Message_Pop_up__c=null;
        update mainOrder;
            
    }
  static testmethod void test1(){ 
/*    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    TestUtilityMethods utilities = new TestUtilityMethods();
    utilities.setUpConfigs();
    Account store = [SELECT Id FROM account WHERE Name ='77 - Twin Cities, MN'];
    
    //TODO TestutilityMethods
    Municipality__c muniHOA = new Municipality__c(Name ='HOAMuni', RecordTypeId = UtilityMethods.retrieveRecordTypeId('HOA', 'Municipality__c'), For_Retail_Location__c = store.id );
    insert muniHOA;
    Municipality__c muniHistorical = new Municipality__c(Name ='HistoricalMuni', RecordTypeId = UtilityMethods.retrieveRecordTypeId('Historical', 'Municipality__c'), For_Retail_Location__c = store.id );
    insert muniHistorical;
    Municipality__c muniPermit = new Municipality__c(Name ='PermitMuni', RecordTypeId = UtilityMethods.retrieveRecordTypeId('Permit', 'Municipality__c'), For_Retail_Location__c = store.id );
    insert muniPermit;
    
    Municipality_Match__c muniMatchHOA = new Municipality_Match__c(City_Township__c= 'Sunshine1', State__c = 'Minnesota', Zip_Code__c = '55449', Agency_Name__c = muniHOA.id );
    insert muniMatchHOA;
    Municipality_Match__c muniMatchHistorical = new Municipality_Match__c(City_Township__c= 'Sunshine1', State__c = 'Minnesota', Zip_Code__c = '55449', Agency_Name__c = muniHistorical.id );
    insert muniMatchHistorical;
    Municipality_Match__c muniMatchPermit = new Municipality_Match__c(City_Township__c= 'Sunshine', State__c = 'Minnesota', Zip_Code__c = '55449', Agency_Name__c = muniPermit.id );
    insert muniMatchPermit;
    
    
    
    
    //Creating Account (Needed for RMS_Queue_Settings__c)
    Account account1 =  new Account(Name = 'Test Account1',
          AccountNumber = '1234567890',
          Phone = '(763) 555-2000',
       Store_Location__c = store.id, 
      HOA__c = muniHOA.id, 
      Historical__c = muniHistorical.id, 
      Building_Permit__c = muniPermit.id,
      BillingStreet = '123 happy Ln',
      BillingCity = 'Sunshine',
      BillingState = 'Minnesota',
      BillingStateCode = 'MN',
      BillingPostalCode = '55449', 
      BillingCountry = 'United States',
      RecordTypeId = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account') ); 
     insert account1;

    Order ord1 = new Order( AccountId = account1.id ,
                Status = 'Draft', 
                EffectiveDate = Date.Today(),
                Pricebook2Id = Test.getStandardPricebookId()
                );
    insert ord1;
    
    
    
    Account vend1 = utilities.createVendorAccount('Vendor Account 1');
    insert vend1;

    Pricebook2 pricebook1 =  utilities.createPricebook2Name('Standard Price Book');
    insert pricebook1;

    Product2 product1 = new Product2(
      Name='Test Product',
      Vendor__c = vend1.id
    );

    insert product1;
    
    PricebookEntry pricebookEntry1 = utilities.createPricebookEntry(Test.getStandardPricebookId(), product1.id);
    insert pricebookEntry1;
    
    OrderItem orderItem1 = new OrderItem(OrderId = ord1.id, PricebookentryId = pricebookEntry1.Id, Quantity = 2, UnitPrice = 100 );
    insert orderItem1;  

    
    ord1.Status = 'Activated';
    ord1.Apex_Context__c = true;
    update ord1;
    
    list<RbA_Work_Order__c> relatedWorkOrders = [SELECT id FROM RbA_Work_Order__c WHERE Sold_Order__c = :ord1.id];
    
    //System.assertEquals(relatedWorkOrders.size(),4);
    
    ord1.Status = 'Draft';
    ord1.Apex_Context__c = true;
    update ord1;
    
    delete ord1;*/
    
    //TODO:Assertions
  }
  

  
}