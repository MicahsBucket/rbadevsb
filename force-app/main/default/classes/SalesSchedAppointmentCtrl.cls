/**
 * @File Name          : SalesSchedAppointmentCtrl.cls
 * @Description        : Lightning Web Component controller for viewing appointment information.
 * @Author             : James Loghry
 * @Group              : Demand Chain
 * @Last Modified By   : Connor.Davis@andersencorp.com
 * @Last Modified On   : 03-25-2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/15/2019, 1:18:04 PM   Demand Chain (James Loghry)     Initial Version
**/
public without sharing class SalesSchedAppointmentCtrl {

    @AuraEnabled(cacheable=true)
    public static List<SalesSchedUtils.Option> getAvailableCapacities(Id appointmentId, String searchText){
        Sales_Appointment__c sa = [Select Slot__c,Appointment_Date__c,Sales_Order__r.Opportunity__r.Store_Location__c, Sales_Order__r.Opportunity__r.Store_Location__r.Sales_Gateway_Store__c From Sales_Appointment__c Where Id = :appointmentId];

        List<Sales_Capacity__c> capacities =
            [Select
                Id,
                User__r.Name,
             	User__r.Contact.New_Rep__c,
             	Status__c,
             	Store__c,
                (Select Id From Sales_Appointment_Resources__r Where Sales_Appointment__r.Cancelled__c = false)
             From
                Sales_Capacity__c
             Where
                (Store__c = :sa.Sales_Order__r.Opportunity__r.Store_Location__r.Sales_Gateway_Store__c OR Store__c = :sa.Sales_Order__r.Opportunity__r.Store_Location__c)
                And Date__c = :sa.Appointment_Date__c
                And Status__c != 'Unavailable'
             	//And Status__c != 'Deferred'--->this is commented out by Ratna as we need to fetch the Deferred capacities for ride along
                And Slot__c = :sa.Slot__c
             Order By
                User__r.Name Asc];

        List<SalesSchedUtils.Option> options = new List<SalesSchedUtils.Option>();
        List<SalesSchedUtils.Option> availableOptions = new List<SalesSchedUtils.Option>();		
        List<SalesSchedUtils.Option> allocatedOptions = new List<SalesSchedUtils.Option>(); 
        //Ratna created below variable to capture the deferred capacities
        List<SalesSchedUtils.Option> deferredOptions = new List<SalesSchedUtils.Option>(); 
        
        for(Sales_Capacity__c capacity : capacities){
            if(capacity.Sales_Appointment_Resources__r.isEmpty()){
                if(String.isEmpty(searchText) || capacity.User__r.Name.containsIgnoreCase(searchText)){
                   // availableOptions.add(new SalesSchedUtils.Option(capacity.Id,capacity.User__r.Name));
                   availableOptions.add(new SalesSchedUtils.Option(capacity));
                }
            }
            if(!capacity.Sales_Appointment_Resources__r.isEmpty())		
            {		
                if(String.isEmpty(searchText) || capacity.User__r.Name.containsIgnoreCase(searchText)){		
                    allocatedOptions.add(new SalesSchedUtils.Option(capacity.Id,capacity.User__r.Name,true));		
                }		
            }		
        }		
        if(!availableOptions.isEmpty())		
        {		
            options.addAll(availableOptions);		
        }		
        if(!allocatedOptions.isEmpty())		
        {		
            options.addAll(allocatedOptions);
        }
        return options;
    }

    @AuraEnabled(cacheable=true)
    public static  AppointmentWrapper getAppointment(String appointmentId){
        Sales_Appointment__c appt = null;
        Id appointmentIdCasted = (Id)appointmentId;
        try{
            appt =
                [Select
                    Id,
                    Appointment_Date__c,
                    Appointment_Date_Time__c,
                    Date__c,
                 	Sales_Order__r.Id,	
                 	Sales_Order__r.Opportunity__r.Owner.Name,
                 	Sales_Order__r.Opportunity__c,
                    Sales_Order__r.Opportunity__r.Id,
                    Sales_Order__r.Opportunity__r.AccountId,
                    Sales_Order__r.Opportunity__r.Account.Dwelling_Style__c,
                    Sales_Order__r.Opportunity__r.Approved_One_Party__c,
                    Sales_Order__r.Opportunity__r.Cancel_Save__c,
                    Sales_Order__r.Opportunity__r.Follow_Up__c,
                    Sales_Order__r.Opportunity__r.Important_Lead_Notes__c,
                    Sales_Order__r.Opportunity__r.Last_DNS_Date__c,
                    Sales_Order__r.Opportunity__r.Last_DNS_Rep__c,
                    Sales_Order__r.Opportunity__r.Last_Sale_Date__c,
                    Sales_Order__r.Opportunity__r.Last_Sale_Rep__c,
                    Sales_Order__r.Opportunity__r.Num_of_Doors__c,
                    Sales_Order__r.Opportunity__r.of_Windows__c,
                    Sales_Order__r.Opportunity__r.Repeat_Sale__c,
                    Sales_Order__r.Opportunity__r.Revisit__c,
                    Sales_Order__r.Opportunity__r.Same_Rep_Resit__c,
                    Sales_Order__r.Opportunity__r.Units__c,
                    Sales_Order__r.Opportunity__r.Appointment_Type__c,
                    Sales_Order__r.Opportunity__r.Store_Location__c,
                    Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__c,
                    Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_2__c,
                    Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_A__c,
                 	Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Entry_Doors__c,
                    Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Reps_can_schedule_follow_ups__c,
                    Sales_Order__r.Rescheduled__c,
                    Sales_Order__r.Payment_Type__c,
                    Sales_Order__r.Result__c,
                    Sales_Order__r.Resulted__c,
                 	Sales_Order__r.Key_Contact_Person__c,
                    Sales_Order__r.Finance_Issues__c,
                    Sales_Order__r.Time_Arrived__c,
                    Sales_Order__r.Time_Left__c,
                    Sales_Order__r.leftTime__c,
                    Sales_Order__r.InTime__c,
                    Sales_Order__r.Check_out_Time__c,
                    Sales_Order__r.Check_In_Time__c,
                    Sales_Order__r.Why_Not_Covered__c,
                    Sales_Order__r.Comments__c,
                    Sales_Order__r.Project_Description__c,
                    Sales_Order__r.Why_No_Demo__c,
                    Sales_Order__r.Follow_Up_Appt_Date_Time__c,
                    Sales_Order__r.Mail_and_Canvass_Neighborhood__c,             
                    Sales_Order__r.Series_One_Windows__c, 
                    Sales_Order__r.Series_Two_Windows__c,
                    Sales_Order__r.Patio_Doors__c,
                    Sales_Order__r.Entry_Doors__c,
                    Sales_Order__r.Amount_If_Sold__c,
                    Sales_Order__r.Series_One_Windows_Quoted__c,
                    Sales_Order__r.Series_Two_Windows_Quoted__c,
                    Sales_Order__r.Includes_Series_A_Window__c,
                    Sales_Order__r.Patio_Doors_Quoted__c,
                    Sales_Order__r.Entry_Doors_Quoted__c,
                    Sales_Order__r.Contract_Amount_Quoted__c,
                    Sales_Order__r.BAY_Windows_Quoted__c,
                    Sales_Order__r.BOW_Windows_Quoted__c,             
                    Sales_order__r.Were_All_Owners_Present__c,
                    Sales_order__r.Key_Reason_Customer_Didnt_Proceed__c,
                    Sales_order__r.Future_Purchase__c,
                    Sales_order__r.Deposit_Type__c,
                    Sales_order__r.Deposit__c,             
                    Sales_order__r.Construction_Department_Notes__c,
                    Sales_order__r.Why_Customer_Purchased__c,
                    Sales_order__r.Concerns__c,
                    Sales_order__r.Address_Concerns__c,
                    Sales_order__r.Partial_Sale__c,
                    Sales_order__r.Historical_Area__c,
                    Sales_order__r.Account_Number__c,
                    Sales_order__r.Approved__c,
                    Sales_order__r.Next_For_Renewal__c,
                 	Sales_Order__r.Store_Finance_Program__r.Id,
                    Sales_Order__r.Store_Finance_Program__r.Name,
                    Sales_Order__r.Store_Finance_Program__r.Finance_Company__r.Id,
                    Sales_Order__r.Store_Finance_Program__r.Finance_Company__r.Name,
                    Sales_Order__r.Turned_Down_By__r.Id,
                    Sales_Order__r.Turned_Down_By__r.Name,
                    Sales_Order__r.Turned_Down_By__c,
                    Slot__r.Start__c,
                    Slot__r.End__c,
                    (Select
                        Sales_Capacity__c,
                        Primary__c,
                     	Sales_Capacity__r.User__r.Contact.New_Rep__c,	
                     	Status__c
                    From
                        Sales_Appointment_Resources__r
                    Order By
                        Primary__c Desc
                    )
                From
                    Sales_Appointment__c
                Where
                    Id = :appointmentIdCasted
            ];

            Set<Id> capacityIds = new Set<Id>();
            for(Sales_Appointment_Resource__c sar : appt.Sales_Appointment_Resources__r){
                if(sar.Sales_Capacity__c != null){
                    capacityIds.add(sar.Sales_Capacity__c);
                }
            }

            Map<Id,Sales_Capacity__c> capacities =  SalesSchedUtilsWithoutSharing.getSalesCapacityMap(capacityIds);
            for(Sales_Appointment_Resource__c sar : appt.Sales_Appointment_Resources__r){
                if(sar.Sales_Capacity__c != null){
                    sar.Sales_Capacity__r = capacities.get(sar.Sales_Capacity__c);
                }
            }
        }catch(QueryException ex){
            System.debug('SalesSchedAppointmentCtrl: getAppointment: Exception: ' + ex.getMessage());
            System.debug('SalesSchedAppointmentCtrl: getAppointment: Exception: ' + ex.getStackTraceString());
            throw new AuraHandledException('No appointment found');
        }
		salesOrderWrap sw = new salesOrderWrap();
        list<AccountInterfaceWrap> allAcountSalOrders = new list<AccountInterfaceWrap>();
        for(Sales_Order__c  salOrde: [SELECT Id,Result__c,Resulted__c,Opportunity__r.Owner.Name, Opportunity__r.Last_DNS_Date__c,Opportunity__r.CloseDate,
                                      Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_2__c, Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_A__c,
                                      Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Entry_Doors__c,Payment_Type__c,
                                      Finance_Issues__c,Rescheduled__c,(Select Id, Appointment_Date_Time__c From Sales_Appointments__r ) from Sales_Order__c
                                      where  Opportunity__r.AccountId =: appt.sales_Order__r.Opportunity__r.AccountId AND Id !=:appt.sales_Order__r.Id AND Opportunity__r.StageName != 'Canceled']){
                                          system.debug('salOrde.Sales_Appointments__r---->'+salOrde.Sales_Appointments__r);
                                           system.debug('salOrde>'+salOrde);
                                          AccountInterfaceWrap acInWrap = new AccountInterfaceWrap();
                                          acInWrap.sl = salOrde;
										  //acInWrap.salUrl = '/rForceARO/'+salOrde.Id;
                                          acInWrap.opptyOwner = salOrde.Opportunity__r.Owner.Name;
                                          acInWrap.salesOrderResult = salOrde.Result__c;
                                          acInWrap.lastSalesDate  =  salOrde.Opportunity__r.CloseDate;                       
                                          acInWrap.salesOrderId = salOrde.Id;
                                          
                                          if(!salOrde.Sales_Appointments__r.isEmpty()){
                                              list<Sales_Appointment__c> appointment = salOrde.Sales_Appointments__r; 
                                              if(appointment[0].Appointment_Date_Time__c != null){
                                                  acInWrap.appointmentTime = appointment[0].Appointment_Date_Time__c.format('h:mm a');
                                              }
                                          }           
                                          allAcountSalOrders.add(acInWrap);
                                      }
        sw.asaccwrap = allAcountSalOrders;
        for(Contact cont : [Select Id, Name, Primary_Contact__c,Full_Name__c, Email,Phone,HomePhone,MobilePhone,
                            Preferred_Method_of_Contact__c,Preferred_Method_of_Contact_Notes__c from Contact 
                            where AccountId =: appt.sales_Order__r.Opportunity__r.AccountId AND Primary_Contact__c = true]){
                                sw.con = cont;
                            }
        List<OpportunityContactRole> ocrs =
                [Select
                    Contact.Name,
                    Contact.Email,
                    Contact.Phone,
                    Contact.MobilePhone,
                    Contact.HomePhone,
                    IsPrimary
                From
                    OpportunityContactRole
                Where
                    OpportunityId = :appt.Sales_Order__r.Opportunity__c
                    And Role = 'Decision Maker'];
        if(ocrs.isEmpty()){
            throw new AuraHandledException('No appointment contact found');
        }

Sales_Schedule_Setting__mdt sss = [Select Current_Sales_Date__c From Sales_Schedule_Setting__mdt Where QualifiedApiName='Default'];

        return new AppointmentWrapper(appt,ocrs,sss,sw);
    }

    @AuraEnabled
    public static List<Sales_Appointment__c> getPastAppointments(String appointmentId){
        List<Sales_Appointment__c> currentAppointments =
            [Select
                Sales_Order__r.Opportunity__c,
                Date__c
             From
                Sales_Appointment__c
             Where
                Id = :appointmentId];

        if(currentAppointments.isEmpty()){
            throw new AuraHandledException('Current appointment not found or invalid appointment id entered.');
        }

        Sales_Appointment__c currentAppointment = currentAppointments.get(0);

        return
            [Select
                Id,
                Date__c,
                (Select
                    Sales_Capacity__r.User__r.Name
                 From
                    Sales_Appointment_Resources__r
                 Where
                    Primary__c = True
                 Limit 1)
             From
                Sales_Appointment__c
             Where
                Sales_Order__r.Opportunity__c = :currentAppointment.Sales_Order__r.Opportunity__c
                And Date__c < :currentAppointment.Date__c
             Order By
                Date__c Asc];
    }
    
    @AuraEnabled
    public static void deleteRidealong(Id appointmentResourceId){
        delete [Select Id From Sales_Appointment_Resource__c Where Id = :appointmentResourceId And Primary__c = false];
    }

    @AuraEnabled
    public static void deleteAssignment(Id appointmentId){
        delete [Select Id From Sales_Appointment_Resource__c Where  Sales_Appointment__c = :appointmentId];
    }
    
    @AuraEnabled(cacheable=true)
    public static  List<AppointmentWrapper> getAppointmentAll(List<String> appointmentIdSet){
        List<Sales_Appointment__c> apptList = null;
        List<AppointmentWrapper> appointmentWrapperList = new List<AppointmentWrapper>();
        Map<Id,List<Sales_Appointment__c>> OpptIdToApptMap = new Map<Id,List<Sales_Appointment__c>>();

        try{
            apptList =
                [Select
                 Id,
                 Appointment_Date__c,
                 Appointment_Date_Time__c,
                 Date__c,
                 Sales_Order__r.Opportunity__c,
                 Sales_Order__r.Opportunity__r.Id,
                 Sales_Order__r.Opportunity__r.AccountId,
                 Sales_Order__r.Opportunity__r.Account.Dwelling_Style__c,
                 Sales_Order__r.Opportunity__r.Approved_One_Party__c,
                 Sales_Order__r.Opportunity__r.Cancel_Save__c,
                 Sales_Order__r.Opportunity__r.Follow_Up__c,
                 Sales_Order__r.Opportunity__r.Important_Lead_Notes__c,
                 Sales_Order__r.Opportunity__r.Last_DNS_Date__c,
                 Sales_Order__r.Opportunity__r.Last_DNS_Rep__c,
                 Sales_Order__r.Opportunity__r.Last_Sale_Date__c,
                 Sales_Order__r.Opportunity__r.Last_Sale_Rep__c,
                 Sales_Order__r.Opportunity__r.Num_of_Doors__c,
                 Sales_Order__r.Opportunity__r.of_Windows__c,
                 Sales_Order__r.Opportunity__r.Repeat_Sale__c,
                 Sales_Order__r.Opportunity__r.Revisit__c,
                 Sales_Order__r.Opportunity__r.Appointment_Type__c,
                 Sales_Order__r.Opportunity__r.Same_Rep_Resit__c,
                 Sales_Order__r.Opportunity__r.Units__c,
                 Sales_Order__r.Opportunity__r.Store_Location__c,
                 Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__c,
                 Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_2__c,
                 Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_A__c,
                 Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Entry_Doors__c,
                 Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Reps_can_schedule_follow_ups__c,
                 Sales_Order__r.Rescheduled__c,
                 Sales_Order__r.Payment_Type__c,
                 Sales_Order__r.Finance_Issues__c,
                 Sales_Order__r.Result__c,
                 Sales_Order__r.Resulted__c,
                 Slot__r.Start__c,
                 Slot__r.End__c,
                 (Select
                  Sales_Capacity__c,
                  Primary__c
                  From
                  Sales_Appointment_Resources__r
                  Order By
                  Primary__c Desc
                 )
                 From
                 Sales_Appointment__c
                 Where
                 Id IN :appointmentIdSet
                 Order By
                 Appointment_Date_Time__c
                ];

            Set<Id> capacityIds = new Set<Id>();

            for(Sales_Appointment__c appt : apptList){
                for(Sales_Appointment_Resource__c sar : appt.Sales_Appointment_Resources__r){
                    if(sar.Sales_Capacity__c != null){
                        capacityIds.add(sar.Sales_Capacity__c);
                    }
                }
                if(!OpptIdToApptMap.containsKey(appt.Sales_Order__r.Opportunity__c))
                    OpptIdToApptMap.put(appt.Sales_Order__r.Opportunity__c,new List<Sales_Appointment__c>());
                OpptIdToApptMap.get(appt.Sales_Order__r.Opportunity__c).add(appt);
            }

            Map<Id,Sales_Capacity__c> capacities =  SalesSchedUtilsWithoutSharing.getSalesCapacityMap(capacityIds);
            for(Sales_Appointment__c appt : apptList){
                for(Sales_Appointment_Resource__c sar : appt.Sales_Appointment_Resources__r){
                    if(sar.Sales_Capacity__c != null){
                        sar.Sales_Capacity__r = capacities.get(sar.Sales_Capacity__c);
                    }
                }
            }
        }catch(QueryException ex){
            System.debug('SalesSchedAppointmentCtrl: getAppointment: Exception: ' + ex.getMessage());
            System.debug('SalesSchedAppointmentCtrl: getAppointment: Exception: ' + ex.getStackTraceString());
            throw new AuraHandledException('No appointment found');
        }

        Set<Id> OpptIdSetToCheck = new Set<Id>();
        Map<Id,List<OpportunityContactRole>> MapApptIdToOpptunityRole = new Map<Id,List<OpportunityContactRole>>();
        //List<OpportunityContactRole> ocrs =
        for(OpportunityContactRole contactRoleRec : [Select
                                                     Contact.Name,
                                                     Contact.Email,
                                                     Contact.Phone,
                                                     Contact.MobilePhone,
                                                     Contact.HomePhone,
                                                     OpportunityId,
                                                     IsPrimary
                                                     From
                                                     OpportunityContactRole
                                                     Where
                                                     OpportunityId IN :OpptIdToApptMap.keySet()
                                                     And Role = 'Decision Maker']){
                                                         OpptIdSetToCheck.add(contactRoleRec.OpportunityId);
                                                         for(Sales_Appointment__c apptObj : OpptIdToApptMap.get(contactRoleRec.OpportunityId)){
                                                             if(!MapApptIdToOpptunityRole.containsKey(apptObj.Id))
                                                                 MapApptIdToOpptunityRole.put(apptObj.Id,new List<OpportunityContactRole>());
                                                             MapApptIdToOpptunityRole.get(apptObj.Id).add(contactRoleRec);
                                                         }
                                                     }
        if(OpptIdSetToCheck.size() != OpptIdToApptMap.size()){
            throw new AuraHandledException('No appointment contact found');
        }

        Sales_Schedule_Setting__mdt sss = [Select Current_Sales_Date__c From Sales_Schedule_Setting__mdt Where QualifiedApiName='Default'];
        for(Sales_Appointment__c salesAppt : apptList){
            appointmentWrapperList.add(new AppointmentWrapper(salesAppt,MapApptIdToOpptunityRole.get(salesAppt.Id),sss, new salesOrderWrap()));
        }



        return appointmentWrapperList;
    }

    @AuraEnabled
    public static void clearResultantDetails(ID salesorderId)
    {
        Sales_Order__c	sorder=[select id,Locked__c,Clear_Resultant_Details__c from Sales_Order__c where ID=:salesorderId];
        sorder.Locked__c=false;
        sorder.Clear_Resultant_Details__c=true;
        update sorder;
        
        Sales_Appointment_Resource__c primaryResource=[select id,Status__c,Primary__c from Sales_Appointment_Resource__c where Sales_Appointment__r.Sales_Order__c=:salesorderId and Primary__c=True];
        primaryResource.Status__c='Checked Out';
        update primaryResource;
    }
    @AuraEnabled 
    public static void confirmAppointments(String resourceListStr)
    {
        System.debug('______resourceList_____'+resourceListStr);
        Savepoint sp = Database.setSavePoint();
        try{
            Map<Id,User> usersToNotify = new Map<Id,User>();
            List<Sales_Appointment_Resource__c> resourcelist=(List<Sales_Appointment_Resource__c>)JSON.deserialize(resourceListStr, List<Sales_Appointment_Resource__c>.class);
            Set<String> assignedStatus=new Set<String>{'Manually Assigned','Manually Reassigned','Assigned'}; 
                Id currentUserId = UserInfo.getUserId();
            List<Sales_Appointment_Resource__c> updateAppntResource=new List<Sales_Appointment_Resource__c>();
            for(Sales_Appointment_Resource__c resource:resourcelist)
            {
                if(assignedStatus.contains(resource.Status__c))
                {
                    usersToNotify.put(resource.Sales_Capacity__r.User__c,resource.Sales_Capacity__r.User__r);
                    resource.Status__c='Confirmed';    
                    resource.Finalized_By__c = currentUserId;
                    updateAppntResource.add(resource); 
                }
            }
            if(!updateAppntResource.isEmpty())
            {  
                update updateAppntResource;     
            }
            System.debug('______usersToNotify_______'+JSON.serialize(usersToNotify));
            Set<Id> userIdsToNotify = usersToNotify.keyset();
            
            //Check to see if any reps have appointments they still need to result.
            Map<Id,User> usersNeedingResulting = new Map<Id,User>();
            for(Sales_Appointment_Resource__c resource :
                [Select
                 Sales_Capacity__r.User__c
                 ,Sales_Capacity__r.User__r.Contact.Email
                 ,Sales_Capacity__r.User__r.Contact.Secondary_Email__c
                 From
                 Sales_Appointment_Resource__c
                 Where
                 Sales_Appointment__r.Date__c < :Date.today()
                 And Sales_Capacity__r.User__c in :userIdsToNotify
                 And (Status__c = 'Accepted' Or Status__c = 'Confirmed')
                 And Sales_Appointment__r.Cancelled__c = false]){
                     Id userId = resource.Sales_Capacity__r.User__c;
                     usersNeedingResulting.put(resource.Sales_Capacity__r.User__c,resource.Sales_Capacity__r.User__r);
                     usersToNotify.remove(resource.Sales_Capacity__r.User__c);
                 }
            System.debug('______usersNeedingResulting_______'+JSON.serialize(usersNeedingResulting));
            Id resultingTemplateId = null;
            Id notificationTemplateId = null;
            for(EmailTemplate et :
                [Select
                 Id,
                 DeveloperName
                 From
                 EmailTemplate
                 Where
                 DeveloperName = 'Sales_Rep_Need_Resulting_Notification'
                 Or DeveloperName = 'Sales_Rep_Appointment_Notification']){
                     if(et.DeveloperName == 'Sales_Rep_Appointment_Notification'){
                         notificationTemplateId = et.Id;
                     }else{
                         resultingTemplateId = et.Id;
                     }
                 }
            
            //Send out appropriate emails.
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            for(User  u : usersNeedingResulting.values()){
                addEmailMessage(mails, u, resultingTemplateId);
            }
            System.debug('____usersToNotify___values_____'+usersToNotify);
            for(User u : usersToNotify.values()){
                System.debug('_____user details_______'+usersToNotify);
                addEmailMessage(mails, u, notificationTemplateId);
            }
            Messaging.sendEmail(mails);
        }catch(Exception ex){
            Database.rollback(sp);
            UtilityMethods.logException(ex,'SalesSchedStoreManagerCtrl');
            throw new AuraHandledException(ex.getMessage());
        }
        
    }
    private static void addEmailMessage(List<Messaging.SingleEmailMessage> mails, User u, Id templateId){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(u.ContactId);
        mail.setTemplateId(templateId);
        mail.setSenderDisplayName(UserInfo.getName());
        mail.setReplyTo(UserInfo.getUserEmail());
        mail.setSaveAsActivity(false);
        mail.setBccSender(false);

        if(!String.isEmpty(u.Contact.Secondary_Email__c)){
            mail.setCcAddresses(new List<String>{u.Contact.Secondary_Email__c});
        }
		System.debug('_______mail______'+mail);
        mails.add(mail);
    }
    @AuraEnabled(cacheable=true)
    public static List<SalesSchedUtils.Option> getFinanceCompanies(Id configId,String searchText){
        System.debug('JWL: configId: ' + configId);
        System.debug('JWL: searchText: [' + searchText + ']');

        if(configId == null){
            return new List<SalesSchedUtils.Option>();
        }

        List<SalesSchedUtils.Option> options = new List<SalesSchedUtils.Option>();
        for(AggregateResult ar :
            [Select
                Finance_Company__r.Name name,
                Finance_Company__c id
             From
                Store_Finance_Program__c
             Where
                Active__c = true
                And Finance_Company__c != null
                And Store_Configuration__c = :configId
             Group By
                Finance_Company__r.Name,
                Finance_Company__c
             Order By
                Finance_Company__r.Name Asc
            ]){
            String company = (String)ar.get('name');
            Id companyId = (Id)ar.get('id');
            if(String.isEmpty(searchText) || company.containsIgnoreCase(searchText)){
                options.add(new SalesSchedUtils.Option(companyId,company));
            }
        }
        return options;
    }

    @AuraEnabled(cacheable=true)
    public static List<SalesSchedUtils.Option> getFinancePlans(Id configId, Id companyId, String searchText){
        if(companyId == null || configId == null){
            return new List<SalesSchedUtils.Option>();
        }

        List<SalesSchedUtils.Option> options = new List<SalesSchedUtils.Option>();
        for(Store_Finance_Program__c sfp :
            [Select
                Id,
                Name
             From
                Store_Finance_Program__c
             Where
                Active__c = true
                And Finance_Company__c = :companyId
                And Store_Configuration__c = :configId
             Order By
                Name Asc
            ]){
            if(String.isEmpty(searchText) || sfp.Name.containsIgnoreCase(searchText)){
                options.add(new SalesSchedUtils.Option(sfp.Id,sfp.Name));
            }
        }
        return options;
    }
    public class AppointmentWrapper{        
        @AuraEnabled public Sales_Appointment__c appointment {get; private set;}
        @AuraEnabled public List<Contact> otherContacts {get; private set;}
        @AuraEnabled public Contact primaryContact {get; private set;}
        @AuraEnabled public Sales_Appointment_Resource__c primaryRep {get; set;}
        @AuraEnabled public boolean assignable {get; set;}
        @AuraEnabled public String formattedTime {get; set;}
        @AuraEnabled public boolean hasRideAlongs{get;set;}
        @AuraEnabled public salesOrderWrap actSalOrdHist{get;set;}
        @AuraEnabled public String headingName {get; set;}

        public AppointmentWrapper(Sales_Appointment__c appointment,List<OpportunityContactRole> contactRoles,Sales_Schedule_Setting__mdt sss, salesOrderWrap actSalOrdHist){
            this.appointment = appointment;
            this.otherContacts = new List<Contact>();
            this.actSalOrdHist = actSalOrdHist;
            
            this.assignable = appointment.Appointment_Date__c >= sss.Current_Sales_Date__c && !appointment.Sales_Order__r.Resulted__c;
            for(OpportunityContactRole ocr : contactRoles){
                if(ocr.IsPrimary){
                    this.primaryContact = ocr.Contact;
                }else{
                    this.otherContacts.add(ocr.Contact);
                }
            }
            this.hasRideAlongs=false;
            for(Sales_Appointment_Resource__c resource : appointment.Sales_Appointment_Resources__r){
                
                if(resource.Primary__c){
                    this.primaryRep = resource;
                }
                this.hasRideAlongs |= (!resource.Primary__c);
            }

            this.formattedTime = '';
            if(appointment.Appointment_Date_Time__c != null){
                // Make the name something useful if primary contact role is missing
                String name = 'Primary Contact Missing';
                if (this.primaryContact.Name != null){
                    name = this.primaryContact.Name;
                }
                this.formattedTime = appointment.Appointment_Date_Time__c.format('h:mm a');
                this.headingName = this.formattedTime + ' with ' + name;
            }
        }
    }
     public class AccountInterfaceWrap{		
        		
        @AuraEnabled public Sales_Order__c sl {get;set;}		
        @AuraEnabled public string appointmentTime{get;set;}		
        @AuraEnabled public string opptyOwner {get;set;}		
        @AuraEnabled public string salesOrderResult {get;set;}		
        @AuraEnabled public date lastSalesDate {get;set;}		
        @AuraEnabled public string salesOrderId {get;set;}		
        //@AuraEnabled public string salUrl{get;set;}		
        		
        		
    }		
    public class salesOrderWrap{		
        		
        @AuraEnabled public Contact con{get;set;}		
        @AuraEnabled public list<AccountInterfaceWrap> asaccwrap{get;set;}		
    }
}