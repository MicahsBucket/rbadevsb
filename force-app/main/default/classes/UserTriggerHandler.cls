/*******************************************************//**

@class    UserTriggerHandler

@brief   Handler for the user trigger

@author  Creston Kuenzi (Slalom.CDK)

@version  2016-5-25  Slalom.CDK
  Created

@see    UserTriggerHandlerTest

@copyright  (c)2016 Slalom.  All Rights Reserved.
      Unauthorized use is prohibited.
* edit by Steven Tremblay to refactor assignPermissions method. SS19-475
***********************************************************/

public with sharing class UserTriggerHandler {
  // This should be used in conjunction with the ApexTriggerComprehensive.trigger template
  // The origin of this pattern is http://www.embracingthecloud.com/2010/07/08/ASimpleTriggerTemplateForSalesforce.aspx
  private boolean m_isExecuting = false;
  private integer BatchSize = 0;

  public static boolean skipTrigger=false;
  public UserTriggerHandler(boolean isExecuting, integer size){
    m_isExecuting = isExecuting;
    BatchSize = size;
  }

  
  public void OnBeforeInsert(User[] newUsers){
    System.debug('running OnBeforeInsert from usertriggerhandler - do nothing for now');
    updateStoreLocationText(newUsers);
    updateAllUsersPermissionSetField(newUsers);
  }

 
  
  public void OnAfterInsert(User[] newUsers){  
    System.debug('running OnAfterInsert from UserTriggerHandler');
	System.debug('request createSharingRecords from OnAfterInsert User Trigger');
    createSharingRecords(new Map<Id, User>(newUsers).keySet()); 

  }

  

  // assignPermissions looks through the user's permissions from the Permission_List__c on the user, finds the permission set Ids and assigns those permissions
  
 public void assignPermissions(Map<ID, User> oldMap, Map<ID, User> newMap){
    System.debug('UserTriggerHandler:assignPermissions in User Trigger hanlder');
    List<PermissionSetAssignment> permissionsToCreate = getAssignmentPermissions(oldMap,newMap);

		if(!permissionsToCreate.isEmpty()){
		  createPermission(JSON.serialize(permissionsToCreate));
		}
  }

  public static List<PermissionSetAssignment> getAssignmentPermissions(Map<ID, User> oldMap, Map<ID, User> newMap){
    List<PermissionSetAssignment> permissionSetAssignmentList = new List<PermissionSetAssignment>();
    //Get Possible Permission Sets from User records
    Map<Id,User> usersWithPermissionSetLabels = new Map<Id,User>();
    Set<String> possiblePermissionSetLabels = new Set<String>();
    for(User user : newMap.values()){
      if(user.Permissions_List__c != null){
        usersWithPermissionSetLabels.put(user.Id,user);
        String[] psNames = user.Permissions_List__c.split(';');
        for(String psName : psNames){
          possiblePermissionSetLabels.add(psName);
        }
      }
    }

    //Only create Permission sets if Permissions_List__c field was populated
    if(!possiblePermissionSetLabels.isEmpty()){
      //Load PermissionSet Map
      Map<String,PermissionSet> permissionSetMap = new Map<String,PermissionSet>();
      List<PermissionSet> permissionSetList = [SELECT id, Label FROM PermissionSet WHERE Label IN :possiblePermissionSetLabels];
      for (PermissionSet ps : permissionSetList){
        permissionSetMap.put(ps.Label,ps);
      }
      
      //Load Existing Users PermissionSetAssignments
      Map<String,PermissionSetAssignment> usersPermissionSetAssignmentMap = new Map<String,PermissionSetAssignment>();
      if(oldMap != null){
        for (PermissionSetAssignment psa : [SELECT Id, AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId IN: usersWithPermissionSetLabels.keySet()]){
          String key = psa.AssigneeId + ':' + psa.PermissionSetId;
          usersPermissionSetAssignmentMap.put(key,psa);
        }
      }

      for(User user : usersWithPermissionSetLabels.values()){
        System.debug('UserTriggerHandler:assignPermissions looking for userId: ' + user.Id);
        String[] psNames = user.Permissions_List__c.split(';');
        for(String psName : psNames){
          PermissionSet ps = permissionSetMap.get(psName);
          if(ps != null){
            //Check for existing permission set assignment
            String key = user.Id + ':' + ps.Id;
            if(!usersPermissionSetAssignmentMap.containsKey(key)){
              PermissionSetAssignment new_psa = new PermissionSetAssignment (PermissionSetId = ps.Id, AssigneeId = user.Id);
              permissionSetAssignmentList.add(new_psa);
            } else{
              System.debug('UserTriggerHandler:already assigned permissionSet:'+ psName);
            }
          } else{
            System.debug('UserTriggerHandler:could not find permissionSet:'+ psName);
          }
        }
      }
    }

    return permissionSetAssignmentList;
  }

  public void OnBeforeUpdate( Map<ID, User> oldMap, Map<ID, User> newMap){
    System.debug('running OnBeforeUpdate from UserTriggerHandler');
    updateStoreLocationText(newMap);
  }
  
  
  public void OnAfterUpdate( Map<ID, User> oldMap, Map<ID, User> newMap){
    System.debug('running OnAfterUpdate from userTriggerHandler');
    createOrDeleteSharingRecordsHelper(oldMap, newMap);
  }


/*
  public void OnBeforeDelete(User[] UsersToDelete, Map<ID, User> UsersMap){

  }

  public void OnAfterDelete(User[] deletedUsers, Map<ID, User> UsersMap){

  }


  public void OnUndelete(User[] restoredUsers){

  }*/

  public void updateAllUsersPermissionSetField(List<User> usersToUpdate){
    String ps = UtilityMethods.getApplicationProperty('Permission_Set_For_All_Users');
    if(ps != null && !String.isEmpty(ps)){
      for(User u : usersToUpdate){
        if(u.Permissions_List__c != null){
          u.Permissions_List__c = ps + ';' + u.Permissions_List__c;
        } else {
          u.Permissions_List__c = ps;
        }
      }
    }
  }


  // This method puts store location multi-picklist values in a text box so we can use them in field filters
  // Note Salesforce multi-picklists are limited and cannot be used in field filters, formula fields or workflow easily
  public void updateStoreLocationText(List<User> usersToUpdate){
    for(User u : usersToUpdate){
	   System.debug('UserID = ' + u.Id);
	   System.debug('running updateStoreLocationText method');
      
      // Limit the text to 255 characters to avoid too many chars error. We can't use a long text area
      // because this field is being used in the Labor->Store Location field filter
      // to limit which stores can be selected, and long text areas are not usable in filters.
        if (u.Store_Locations__c != null)
        {
            u.Store_Location_Text__c = (u.Store_Locations__c).abbreviate(255);
        }        
        else 
        {
            if(u.Store_Location_Text__c==null)
            {
                u.Store_Location_Text__c = '';        
            }
        }
    }
  }

  // This method receives a map of store locations and simply calls the method above
  public void updateStoreLocationText(Map<Id, User> userMap){
    List<User> usersToUpdate = userMap.values();
	System.debug('running updateStoreLocationText in user trigger');
    updateStoreLocationText(usersToUpdate);
  }

  public void createOrDeleteSharingRecordsHelper(Map<Id, User> oldMap, Map<Id, User> newMap){

    //TODO: account for store location going from nonnull to null; Account for deleting the old share records
//    List<User> usersToCreateSharingFor = new List<User>();
    Set<Id> usersToCreateSharingFor = new Set<Id>();
//    List<User> usersToDeleteSharingRecordsFor = new List<User>();
    Set<Id> usersToDeleteSharingRecordsFor = new Set<Id>();
    for(Id uid: oldMap.keyset()){
	  System.debug('in createordeletesharingrecordshelper in user trigger');
      if( (newMap.get(uid).IsActive == true) && 
          ((oldMap.get(uid).Store_Locations__c != newMap.get(uid).Store_Locations__c) ||  
      (oldMap.get(uid).IsActive != newMap.get(uid).IsActive))) 
    {
        usersToDeleteSharingRecordsFor.add(uid);
        if(newMap.get(uid).Store_Locations__c != null){
          usersToCreateSharingFor.add(uid);       
        }
      }
    }
    if (usersToDeleteSharingRecordsFor.size() > 0) deleteSharingRecords(usersToDeleteSharingRecordsFor);
    //if (usersToCreateSharingFor.size() > 0) createSharingRecords(usersToCreateSharingFor);
    
  }
 
 //future method because we run into Contact Trigger and get a DML error if it's not future during SSO code Auto-Provisioning.
  @future
	 public static void createPermission(string JSONpermissions){
	    System.debug('running Create Permissions future method');
		//taking the Json list from assignPermissions method and running in future.
		
    List<PermissionSetAssignment> psaList = (List<PermissionSetAssignment>)System.JSON.deserialize(JSONpermissions, List<PermissionSetAssignment>.class);	
    //if insert is attempted a mixed dml operation error will occur   
    if (!Test.isRunningTest()) {
        insert psaList;
      }
    }

  @future
  public static void createSharingRecords(Set<Id> userIds){
    //Possibly unnecessary
    List<User> users = [Select Id, Store_Location_Text__c, Store_Locations__c from User where Id in: userIds and Store_Locations__c != null and Store_Location_Text__c != null AND IsActive = true];
    if(!users.isEmpty() ){
	  System.debug('hit createSharingRecords method in User Trigger');
      Set<UserShare> sharesToInsert = new Set<UserShare>();

      String userQuery = 'Select Id, Store_Locations__c from User where IsActive = true AND Store_Locations__c includes(';
      Set<String> locations = new Set<String>();
      for(User u : users){
          List<String> loc = u.Store_Locations__c.split(';');
          locations.addAll(loc);
      }
      for(String s: locations){
            userQuery +='\''+s+'\',';
      }
      userQuery = userQuery.removeEnd(',');
      userQuery += ')';

      System.debug('______userQuery________'+userQuery);
      List<User> usersWithMatchingLoc = Database.query(userQuery);
      for(User u1: usersWithMatchingLoc){
        for(User u : users){
          if(u1.Id != u.Id){
            List<String> loc = u.Store_Locations__c.split(';');
            for(String l : loc){
              if(u1.Store_Locations__c.containsIgnoreCase(l)){
                sharesToInsert.add(new UserShare(UserId = u.Id, UserOrGroupId = u1.Id, UserAccessLevel = 'read',
                                rowCause = 'Manual'));
                sharesToInsert.add(new UserShare(UserId = u1.Id, UserOrGroupId = u.Id, UserAccessLevel = 'read',
                                rowCause = 'Manual'));
                break;
              }
            }
          }
        }
      }

      system.debug(sharesToInsert);
      if( !Test.isRunningTest())
      insert new List<UserShare>(sharesToInsert);
    }
  }
  @future
  public static void deleteSharingRecords(Set<Id> userIds){
      List<UserShare> sharesToDelete = [Select Id, rowCause, UserId, UserOrGroupId from UserShare where ( UserId in: userIds or UserOrGroupId in: userIds) and rowCause = 'Manual'];
      delete sharesToDelete;
  }
}