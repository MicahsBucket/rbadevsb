global class SurveyWashingtonToBaltimore implements Schedulable {
	global void execute(SchedulableContext sc) {
		Id baltimoreId = [SELECT Id FROM Account WHERE Name = '0085 - Baltimore' LIMIT 1].Id;
    	Id capitalWashingtonId = [SELECT Id FROM Account WHERE Name = '0097 - Capital Washington, DC - Virginia' LIMIT 1].Id;
		List<Survey__c> washingtonSurveys = [SELECT Id, Store_Account__c FROM Survey__c WHERE Store_Account__c = :capitalWashingtonId];
		for(Survey__c s : washingtonSurveys){
		    s.Store_Account__c = baltimoreId;
		}
		Database.update(washingtonSurveys);
	}
}