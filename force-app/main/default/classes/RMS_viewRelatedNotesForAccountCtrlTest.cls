@isTest
public with sharing class RMS_viewRelatedNotesForAccountCtrlTest {

	public static Id serviceRecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
   	public static Id CORORecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Record_Type', 'Order');
   	public static Id changeRecordTypeId = UtilityMethods.retrieveRecordTypeId('Change_Order', 'Order');
   	public static Id contactRecordTypeId = UtilityMethods.retrieveRecordTypeId('Customer', 'Contact');
   	public static Id woTechMeasureRecordTypeId = UtilityMethods.retrieveRecordTypeId('Visit', 'RbA_Work_Order__c');
   	public static Id woPermitRecordTypeId = UtilityMethods.retrieveRecordTypeId('Permit', 'RbA_Work_Order__c');

	@testSetup static void setupData() {
		TestUtilityMethods utility = new TestUtilityMethods();
		utility.setUpConfigs();
		Account dwelling = utility.createDwellingAccount('Dwelling Account');
		Account store = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
		dwelling.Store_Location__c = store.Id;
		insert dwelling;

		Order serviceOrder =  new Order(	Name='Sold Order 1', 
									AccountId = dwelling.id, 
									EffectiveDate= Date.Today(), 
									Store_Location__c = store.Id,							
									Status ='Draft', 
									Pricebook2Id = Test.getStandardPricebookId(),
									RecordtypeId = serviceRecordTypeId
								);
		insert serviceOrder;

		Order  COROOrder =  new Order(	Name='Sold Order 2', 
									AccountId = dwelling.id, 
									EffectiveDate= Date.Today(), 
									Store_Location__c = store.Id,							
									Status ='Draft', 
									Pricebook2Id = Test.getStandardPricebookId(),
									RecordtypeId = CORORecordTypeId
								);
		insert COROOrder;

		Order  ChangeOrder =  new Order( Name='Sold Order 3', 
									AccountId = dwelling.id, 
									EffectiveDate= Date.Today(), 
									Store_Location__c = store.Id,							
									Status ='Draft', 
									Pricebook2Id = Test.getStandardPricebookId(),
									RecordtypeId = changeRecordTypeId
								);
		insert ChangeOrder;

		OperatingHours operatingHours = new OperatingHours(
							            Name = 'Test Hours 1'
							        );
        insert operatingHours;

		ServiceTerritory territory1 = new ServiceTerritory(
					            IsActive = true,
					            Name = 'Test Territory 1',
					            OperatingHoursId = operatingHours.Id
					        );
        insert territory1;

        WorkOrder wo = new WorkOrder(
            Recommended_Crew_Size__c = 2,
            Street = '137 5th St N',
            City = 'Bayport',
            State = 'Minnesota',
            PostalCode = '55003-1113',
            CountryCode = 'US',
            ServiceTerritoryId = territory1.Id
        );
        insert wo;

	}

	@isTest static void testControllerForVariousNotes(){
		Test.startTest();
		id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        Account acc = [Select Id,Store_Location__c from Account where Name != 'RbA' AND Name != 'Unassigned Account' AND RecordTypeId =: dwellingRT];
        WorkOrder wo = [Select Id from WorkOrder LIMIT 1];
        Contact contct = new Contact ( FirstName = 'Home',
        								LastName = 'Owner',
        								AccountId = acc.id,
        								Primary_Contact__c = TRUE,
        								RecordTypeId = contactRecordTypeId );
        insert contct;
        Note contactnt = new Note (Title = 'Contact Note',
        						Body = 'This is the body',
        						Parentid = contct.id);
        insert contactnt;
        Opportunity opp = new Opportunity (Name = 'Opp Name',
        						AccountId = acc.id,
        						StageName = 'New', 
        						CloseDate = System.today() );
        insert opp;
        Note nt = new Note (Title = 'Opp Note',
        						Body = 'This is the body',
        						Parentid = opp.id);
        insert nt;
        RbA_Work_Order__c techmeasure = new RbA_Work_Order__c( 
        									RecordtypeId = woTechMeasureRecordTypeId,
        									Work_Order_Type__c = 'Tech Measure',
        									Account__c = acc.id);
        insert techmeasure;
		Note techmeasurent = new Note (Title = 'Tech Meaure Note',
        						Body = 'This is the body',
        						Parentid = techmeasure.id);
        insert techmeasurent;
        RbA_Work_Order__c permit = new RbA_Work_Order__c(
        								RecordtypeId = woPermitRecordTypeId,
        								Work_Order_Type__c = 'Building Permit',
        								Account__c = acc.id);
        insert permit;
		Note permitnt = new Note (Title = 'Permit Note',
        						Body = 'This is the body',
        						Parentid = permit.id);
        insert permitnt;
        Note wont = new Note (Title = 'New Work Order Note',
        						Body = 'This is the body',
        						Parentid = wo.id);
        insert wont;

		Test.setCurrentPage(new PageReference('/apex/RMS_viewRelatedNotes?Id='+acc.Id));
		RMS_viewRelatedNotesForAccountController controller = new RMS_viewRelatedNotesForAccountController(new ApexPages.StandardController(acc));
		System.assertEquals(controller.attachmentsOpp.size(), 1);
		System.assertEquals(controller.attachmentsContact.size(), 1);
		System.assertEquals(controller.attachmentsActionWorkOrder.size(), 1);
		System.assertEquals(controller.attachmentsVisitWorkOrder.size(), 1);
// Removing from build 8.8.2018 due to failing assertion - Matt Azlin
//		System.assertEquals(controller.attachmentsFSLWorkOrder.size(), 1);
		Test.stopTest();
	}

	@isTest static void testControllerForServiceOrdersNotes(){
		Test.startTest();
		Order order = [select Id,Pricebook2Id,RecordTypeId from Order where Name ='Sold Order 1'];
		id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        Account acc = [Select Id,Store_Location__c from Account where Name != 'RbA' AND Name != 'Unassigned Account' AND RecordTypeId =: dwellingRT];
        Note nt = new Note( Title = 'ServiceOrder Note',
        						Body = 'This is the body',
        						Parentid = order.id);
        insert nt;
		Test.setCurrentPage(new PageReference('/apex/RMS_viewRelatedNotes?Id='+acc.Id));
		RMS_viewRelatedNotesForAccountController controller = new RMS_viewRelatedNotesForAccountController(new ApexPages.StandardController(acc));
		System.assertEquals(controller.attachmentsServiceOrder.size(), 1);
		Test.stopTest();
	}

	@isTest static void testControllerForOrdersNotes(){
		Test.startTest();
		Order order = [select Id,Pricebook2Id,RecordTypeId from Order where Name ='Sold Order 2'];
		id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        Account acc = [Select Id,Store_Location__c from Account where Name != 'RbA' AND Name != 'Unassigned Account' AND RecordTypeId =: dwellingRT];
        Note nt = new Note( Title = 'CoroOrder Note',
        						Body = 'This is the body',
        						Parentid = order.id);
        insert nt;
        Test.setCurrentPage(new PageReference('/apex/RMS_viewRelatedNotes?Id='+acc.Id));
		RMS_viewRelatedNotesForAccountController controller = new RMS_viewRelatedNotesForAccountController(new ApexPages.StandardController(acc));
		System.assertEquals(controller.attachmentsOrder.size(), 1);
		Test.stopTest();
	}

	@isTest static void testControllerForChangeOrdersNotes(){
		Test.startTest();
		Order order = [select Id,Pricebook2Id,RecordTypeId from Order where Name ='Sold Order 3'];
		id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        Account acc = [Select Id,Store_Location__c from Account where Name != 'RbA' AND Name != 'Unassigned Account' AND RecordTypeId =: dwellingRT];
        Note nt = new Note( Title = 'ChangeOrder Note',
        						Body = 'This is the body',
        						Parentid = order.id);
        insert nt;
        Test.setCurrentPage(new PageReference('/apex/RMS_viewRelatedNotes?Id='+acc.Id));
		RMS_viewRelatedNotesForAccountController controller = new RMS_viewRelatedNotesForAccountController(new ApexPages.StandardController(acc));
		System.assertEquals(controller.attachmentsChangeOrder.size(), 1);
		Test.stopTest();
	}

	
}