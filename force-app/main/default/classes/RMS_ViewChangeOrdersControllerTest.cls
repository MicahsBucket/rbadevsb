/*******************************************************//**

@class  RMS_ViewChangeOrdersControllerTest

@brief  Test Class for RMS_ViewChangeOrdersController

@author  Pavan Gunna


***********************************************************/
@isTest
private class RMS_ViewChangeOrdersControllerTest{
  public static Id serviceRecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
     public static Id CORORecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Record_Type', 'Order');
  @testSetup static void setupData() {
    TestUtilityMethods utility = new TestUtilityMethods();
    utility.setUpConfigs();
    Account dwelling = utility.createDwellingAccount('Dwelling Account');
    Account store = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
    dwelling.Store_Location__c = store.Id;
    insert dwelling;

    Order serviceOrder =  new Order(  Name='Sold Order 1', 
                  AccountId = dwelling.id, 
                  EffectiveDate= Date.Today(), 
                  Store_Location__c = store.Id,              
                  Status ='Draft', 
                  Pricebook2Id = Test.getStandardPricebookId(),
                  recordtypeId = serviceRecordTypeId
                );
    insert serviceOrder;

    Order  COROOrder =  new Order(  Name='Sold Order 2', 
                  AccountId = dwelling.id, 
                  EffectiveDate= Date.Today(), 
                  Store_Location__c = store.Id,              
                  Status ='Draft', 
                  Pricebook2Id = Test.getStandardPricebookId(),
                  recordtypeId = CORORecordTypeId
                );
    insert COROOrder;

        }
  
  

  @isTest static void testControllerForCOROOrders(){
    Order order = [select Id,Pricebook2Id,RecordTypeId from Order where Name ='Sold Order 2'];
    id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
    Account acc = [Select Id,Store_Location__c from Account where Name != 'RbA' AND Name != 'Unassigned Account' AND RecordTypeId =: dwellingRT];
    Test.setCurrentPage(new PageReference('/apex/RMS_ViewChangeOrders?Id='+order.Id));
    RMS_ViewChangeOrdersController controller = new RMS_ViewChangeOrdersController(new ApexPages.StandardController(order));
      
  }
  
}