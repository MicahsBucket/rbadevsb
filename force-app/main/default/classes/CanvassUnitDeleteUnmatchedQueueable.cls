global class CanvassUnitDeleteUnmatchedQueueable implements Queueable, Database.AllowsCallouts
{
    private string marketId;
    private string zipCode;
    private string queuedJobId;
    
    global CanvassUnitDeleteUnmatchedQueueable(string CUMarketId, string CUZipCode, string queuedJobIdString)
    {
        marketId = CUMarketId;
        zipCode = CUZipCode;
        queuedJobId = queuedJobIdString;
    }
    
    global void execute(QueueableContext context)  
    {
        try
        {
            list<CNVSS_Canvass_Unit__c> listOfOldRecordsNotUpdated = [SELECT Id
                                                                      FROM CNVSS_Canvass_Unit__c 
                                                                      WHERE CNVSS_Canvass_Market__c = :marketId 
                                                                      AND Auto_Created__c = false 
                                                                      AND Auto_Modified__c = false
                                                                      AND CNVSS_Zip_Code__c = :zipCode
                                                                      LIMIT 9999];
            
            if(listOfOldRecordsNotUpdated.size() == 9999)
            {
                // delete old CU that were not matched
                delete listOfOldRecordsNotUpdated;

                // make recursive call
                System.enqueueJob(new CanvassUnitDeleteUnmatchedQueueable(marketId,zipCode,queuedJobId));
            }
            else
            {
                // delete old CU that were not matched
                delete listOfOldRecordsNotUpdated;

                // Object to create / update with 
                list<Custom_Data_Layer_Batch_Queue__c> canvassUnitBatchJobQueue = [SELECT Id, Name, Canvass_Market_Batch_Job__c, Old_Records_Not_Updated_Count__c 
                                                                                   FROM Custom_Data_Layer_Batch_Queue__c
                                                                                   WHERE Id = :queuedJobId];
                
                boolean seeIfMarketJobIsDone = false;
                string canvassMarketJobId = null;

                for(Custom_Data_Layer_Batch_Queue__c canvassUnitBatchJob :canvassUnitBatchJobQueue)
                {
                    canvassUnitBatchJob.Status__c = 'Waiting for Summation';

                    if(canvassUnitBatchJob.Canvass_Market_Batch_Job__c != null)
                    {
                        seeIfMarketJobIsDone = true;
                        canvassMarketJobId = canvassUnitBatchJob.Canvass_Market_Batch_Job__c;
                    }
                }
                
                update canvassUnitBatchJobQueue;

                if(seeIfMarketJobIsDone == true && canvassMarketJobId != null)
                {
                    list<Custom_Data_Layer_Batch_Queue__c> canvassUnitJobQueueList = [SELECT Id
                                                                                      FROM Custom_Data_Layer_Batch_Queue__c
                                                                                      WHERE Canvass_Market_Batch_Job__c = :canvassMarketJobId
                                                                                      AND (Status__c = 'NA' OR
                                                                                           Status__c = 'Queued' OR 
                                                                                           Status__c = 'Deleting Records' OR
                                                                                           Status__c = 'Holding' OR 
                                                                                           Status__c = 'Fetching Data In Progress' OR 
                                                                                           Status__c = 'Waiting for Insertion And Update' OR 
                                                                                           Status__c = 'Insertion And Update In Progress' OR 
                                                                                           Status__c = 'Waiting For Record Count' OR
                                                                                           Status__c = 'Counting Records' OR
                                                                                           Status__c = 'Waiting For Delectation Of Unmatched Records' OR
                                                                                           Status__c = 'Deleting Unmatched Records' OR
                                                                                           Status__c = 'Failed')
                                                                                      AND Id != :queuedJobId];

                    // if there are no holding or in progress jobs then send email
                    if(canvassUnitJobQueueList.size() == 0)
                    {
                        list<Canvass_Market_Batch_Job__c> canvassMarketBatchJobList = [SELECT Id, Status__c, User__r.Email
                                                                                       FROM Canvass_Market_Batch_Job__c 
                                                                                       WHERE Id = :canvassMarketJobId];
                        if(canvassMarketBatchJobList.size() > 0)
                        {
                            database.executebatch(new CanvassUnitRecordCountSummationBatch(canvassMarketBatchJobList[0].Id),1);
                        }
                        /*boolean sendEmail = false;
                        string userEmail = null;
                        for(Canvass_Market_Batch_Job__c canvassMarketBatchJob :canvassMarketBatchJobList)
                        {
                            canvassMarketBatchJob.Status__c = 'Completed';

                            if(canvassMarketBatchJob.User__r.Email != null)
                            {
                                sendEmail = true;
                                userEmail = canvassMarketBatchJob.User__r.Email;
                            }
                        }

                        update canvassMarketBatchJobList;

                        if(sendEmail == true)
                        {
                            sendClientEmail(userEmail);
                        }*/
                    }
                }
            }
        }
        catch(Exception e)
        {
            system.debug('*****');
            system.debug(e);
            // Object to create / update with 
            list<Custom_Data_Layer_Batch_Queue__c> canvassUnitBatchJobQueue = [SELECT Id, Name, Canvass_Market_Batch_Job__c, Old_Records_Not_Updated_Count__c 
                                                                               FROM Custom_Data_Layer_Batch_Queue__c
                                                                               WHERE Id = :queuedJobId];

            // iterate over scope
            for(Custom_Data_Layer_Batch_Queue__c queuedJob :canvassUnitBatchJobQueue)
            {
                // update status so that job won't be queued again
                queuedJob.Status__c = 'Failed';
            }
            
            update canvassUnitBatchJobQueue;
        }
    }

    // used to send email to client
    /*global void sendClientEmail(string userEmail)
    {
        list<string> emailAddresses = new list<string>();
        emailAddresses.add(userEmail);

        string resBody = 'Canvass Unit Batch Job Has Finished Running';
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.ToAddresses = EmailAddresses;
        message.Subject = 'Canvass Unit Batch Job Has Finished Running';
        message.htmlBody = resBody;
            
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] rslts = Messaging.sendEmail(messages);
            
        if (rslts[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + rslts[0].errors[0].message);
        }
    }*/
}