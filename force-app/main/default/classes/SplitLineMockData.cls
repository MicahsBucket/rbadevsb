public with sharing class SplitLineMockData {
    @AuraEnabled
    public static List<JsonParser> MockData() {
        string mockdata = '[{"Id":88,"Attributes":[{"Attributes":[],"DataType":0,"Name":"PartNumber","Value":"Sidelite"},{"Attributes":[],"DataType":1,"Name":"Quantity","Value":"1"},{"Attributes":[{"Attributes":[],"DataType":0,"Name":"Color","Value":"R"},{"Attributes":[],"DataType":1,"Name":"Height","Value":"84"},{"Attributes":[],"DataType":1,"Name":"Width","Value":"11"}],"DataType":7,"Name":"RapidOptions","Value":null}],"IntegrationOutputId":"12b2c39c-db86-492d-a525-6f301456c75c","TemplateName":"ManufacturedConfigurable"},{"Id":89,"Attributes":[{"Attributes":[],"DataType":0,"Name":"PartNumber","Value":"Sidelite"},{"Attributes":[],"DataType":1,"Name":"Quantity","Value":"1"},{"Attributes":[{"Attributes":[],"DataType":0,"Name":"Color","Value":"R"},{"Attributes":[],"DataType":1,"Name":"Height","Value":"84"},{"Attributes":[],"DataType":1,"Name":"Width","Value":"11"}],"DataType":7,"Name":"RapidOptions","Value":null}],"IntegrationOutputId":"b86ff4bb-c89d-49de-9342-dcc4e95c0f71","TemplateName":"ManufacturedConfigurable"},{"Id":90,"Attributes":[{"Attributes":[],"DataType":0,"Name":"PartNumber","Value":"Door"},{"Attributes":[],"DataType":1,"Name":"Quantity","Value":"1"},{"Attributes":[{"Attributes":[],"DataType":0,"Name":"Color","Value":"R"},{"Attributes":[],"DataType":1,"Name":"Height","Value":"84"},{"Attributes":[],"DataType":1,"Name":"Width","Value":"30"}],"DataType":7,"Name":"RapidOptions","Value":null}],"IntegrationOutputId":"fd6dc881-1356-4003-8f7b-ee0f49451c40","TemplateName":"ManufacturedConfigurable"}]';
        List<JsonParser> deserializedData = (List<JsonParser>)JSON.deserialize(mockdata, List<JsonParser>.class);
        system.debug('deserializedData:'+ deserializedData);
        for(JsonParser j: deserializedData){
            system.debug('Id: '+ j.Id );
            HeaderData hd = new HeaderData();
            hd.Id = j.Id;
            if(j.Attributes != null){
                for(Attributes a: j.Attributes){
                    if(a.Name == 'PartNumber'){
                        system.debug('Name: '+ a.Name + ' Value: '+ a.Value);
                        if(a.Name != null){
                            hd.Name = 'Part Number';
                        }
                        if(a.Value != null){
                            hd.Value = a.Value;
                        }
                    }
                    if(a.Name == 'Quantity'){
                        system.debug('Name: '+ a.Name + ' Value: '+ a.Value);
                        if(a.Name != null){
                            hd.QuantityName = 'Quantity';
                        }
                        if(a.Value != null){
                            hd.QuantityValue = a.Value;
                        }
                    }
                }
            }
            j.HeaderData = hd;
        }
        return deserializedData;
    
    }

    public class JsonParser{
        @AuraEnabled
		public String IntegrationOutputId{get;set;}
        @AuraEnabled
		public list<Attributes> Attributes{get;set;}
        @AuraEnabled
		public String TemplateName{get;set;}
        @AuraEnabled
		public Integer Id{get;set;}
        @AuraEnabled
        public HeaderData HeaderData {get;set;}
    }
	public class Attributes{
        @AuraEnabled
		public String Value{get;set;}
        @AuraEnabled
		public Integer DataType{get;set;}
        @AuraEnabled
		public String Name{get;set;}
        @AuraEnabled
		public list<Attributes> Attributes{get;set;}
	}

    public class HeaderData{
        @AuraEnabled
        public Integer Id{get;set;}
        @AuraEnabled
        public String Name{get;set;}
        @AuraEnabled
        public String Value{get;set;}
        @AuraEnabled
        public String QuantityName{get;set;}
        @AuraEnabled
        public String QuantityValue{get;set;}
    }
}