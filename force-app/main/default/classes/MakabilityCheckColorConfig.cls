/**
 * @File Name          : MakabilityCheckColorConfig.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 02-24-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/30/2019, 1:20:02 PM   mark.rothermal@andersencorp.com     Initial Version
**/

public without sharing class MakabilityCheckColorConfig implements MakabilityService {
    
    
    /**
    * @description Check Makability function- compare orderItem Request against Product Config makability records
    * @author mark.rothermal@andersencorp.com | 4/30/2019
    * @param map<string, MakabilityRestResource.OrderItem> requests
    * @param set<id> productIds
    * @return List<MakabilityRestResource.MakabilityResult>
    */
    Public Static List<MakabilityRestResource.MakabilityResult> checkCompatibility( map <string,MakabilityRestResource.OrderItem> requests, set<id> productIds ){
        
        List<MakabilityRestResource.MakabilityResult> results = new List<MakabilityRestResource.MakabilityResult>(); 
        Map<id,Color_Configuration__c> colorConfigMap = getConfigOptions(productIds);
        Map<id,map<string,string>> prodIdToExteriorInteriorMap = createExteriorToInteriorMap(colorConfigMap);  
   //     system.debug('requests in check color ' + requests);        
  //      system.debug('color config map' + colorConfigMap);
 //       system.debug('prod id to color config map... '+ prodIdToExteriorInteriorMap);
 
        
         // handle no config record found.
        if(colorConfigMap.keyset().size() == 0){
            for(String r:requests.keyset()){
                MakabilityRestResource.OrderItem oi = requests.get(r);
                MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult(); 
                List<string> errMessages = new List<string>();
                string errMessage = 'Salesforce - No Color Configurations found for this productId.';  
                errMessages.add(errMessage);
                result.errorMessages = errMessages;
                result.orderId = oi.orderId;
                result.orderItemId = oi.orderItemId;
                results.add(result);                
            }            
            return results;
        }
        
        ////////////////////////////// 
        // run makability check  
        // //////////////////////////
        for(String r:requests.keyset()){
                MakabilityRestResource.OrderItem req = requests.get(r);                  
                List<string> errMessages = new List<string>();        
                MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
                result.orderItemId = req.orderItemId;
                result.orderId = req.orderId;
                Map<string,string> currentColorMap = prodIdToExteriorInteriorMap.get(req.productConfiguration.productId);
                String eColor = req.productConfiguration.exteriorColor;
                String iColor = req.productConfiguration.interiorColor;
                if(currentColorMap.containskey(eColor)){
                    String interiorColorOptions = currentColorMap.get(eColor);
                    if(checkInteriorColorConfig(interiorColorOptions,iColor)){
                        errMessages.add('Color Config - passed');  
                        result.isMakable = true;
                        result.errorMessages = errMessages;                
                        results.add(result);                       
                    }else {
                        errMessages.add ('Color Config. Incorrect Interior Color ' 
                                         + iColor + 
                                         ' is not an option for an exterior color of ' +  eColor +
                                         ' . available options are: ' + interiorColorOptions );                         
                        result.isMakable = false;
                        result.errorMessages = errMessages;                               
                        results.add(result);                        
                    }
                } else {
                    errMessages.add('Color Config. Incorrect Exterior Color, available options are: ' + currentColorMap.keyset());                     
                    result.isMakable = false;
                    result.errorMessages =  errMessages;                               
                    results.add(result);    
                }
            
        }
        system.debug('results check ' + results );        
        if(results.size() == 0){
            List<string> errMessages = new List<string>();                    
            errMessages.add('Something bad happened with the color config');
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
            result.errorMessages = errMessages;
            result.isMakable = false;
            results.add(result);
        }
        return results;
    }
    
    /**
    * @description compare 2 strings and return a boolean
    * @author mark.rothermal@andersencorp.com | 4/30/2019
    * @param String config
    * @param string request
    * @return boolean
    */
    public static boolean checkInteriorColorConfig(String config, string request ){
        boolean makable = false;
        if(config.contains(request)){
            makable = true;
        }
        return makable;
    }

    /**
    * @description builds a map of config records to be used in main method
    * @author mark.rothermal@andersencorp.com | 4/30/2019
    * @param set<id> productIds
    * @return Map<id, Color_Configuration__c>
    */
    public static Map<id,Color_Configuration__c> getConfigOptions(set<id> productIds){
          Map<id,Color_Configuration__c> colorConfigMap= new Map<id,Color_Configuration__c>( [
            SELECT id,
            Exterior_Color__c,
            Interior_Color__c,
            Product_Configuration__r.Product__c
            from Color_Configuration__c 
            where Product_Configuration__r.Product__c in :productids] );  
        return colorConfigMap;
    }
    /**
    * @description builds a map of exterior color to interior colors.
    * @author mark.rothermal@andersencorp.com | 4/30/2019
    * @param Map<id, Color_Configuration__c> colorConfigMap
    * @return Map<id, map<string, string>>
    */
    public static Map<id,map<string,string>> createExteriorToInteriorMap(Map<id,Color_Configuration__c> colorConfigMap){
        Map<id,map<string,string>> prodIdToExteriorInteriorMap = new Map<id,map<string,string>>(); 
        for(id cc :colorConfigMap.keyset()){
            Color_Configuration__c  currentConfig =  colorConfigMap.get(cc);          
            if(prodIdToExteriorInteriorMap.containskey(currentConfig.Product_Configuration__r.Product__c)){
                Map<string,string> exteriorInteriorMapPlaceholder = prodIdToExteriorInteriorMap.get(currentConfig.Product_Configuration__r.Product__c); 
                if(!exteriorInteriorMapPlaceholder.containsKey(currentConfig.Exterior_Color__c)){
                    exteriorInteriorMapPlaceholder.put(currentConfig.Exterior_Color__c,currentConfig.Interior_Color__c);
                }
            } else {
                Map<string,string> exteriorInteriorMapPlaceholder = new map<string,string>();
                exteriorInteriorMapPlaceholder.put(currentConfig.Exterior_Color__c,currentConfig.Interior_Color__c);
                prodIdToExteriorInteriorMap.put(currentConfig.Product_Configuration__r.Product__c,exteriorInteriorMapPlaceholder);
            }
        }
        return prodIdToExteriorInteriorMap;
    }
}