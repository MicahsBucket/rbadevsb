public class SalesSchedInsertBatch implements Database.Batchable<sObject>{

    public List<sObject> items {get; set;}
    
    public SalesSchedInsertBatch(List<sObject> items){
        this.items = items;
    }
    
    public List<sObject> start(Database.BatchableContext BC){
      return this.items;
   }
    
    public void execute(Database.BatchableContext bc, List<sObject> scope){
        Database.insert(scope,false);
    }
    
    public void finish(Database.BatchableContext bc){
        
    }
}