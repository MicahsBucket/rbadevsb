/**
 * @File Name          : MakabilityCheckSizeDetailConfigTest.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 5/3/2019, 6:32:58 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/3/2019, 6:32:58 PM   mark.rothermal@andersencorp.com     Initial Version
**/
@isTest
public with sharing class MakabilityCheckSizeDetailConfigTest {
    @isTest
    public static void passingMakabilityTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSizeDetailConfig(pc);
        createTestWohHeightConfig(pc);
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        createProductFieldControlDependencies('test',pfc,pfc);        
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,50,true,1 ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Size Detail Config  - passed');
    }
     @isTest
    public static void makabilityTestForCoastalPerfCategory() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        System.debug('The product configuration pc: '+pc);
        createTestSizeDetailConfig(pc);
        Size_Detail_Configuration__c sdc = [Select id, Performance_Category__c from Size_Detail_Configuration__c where Product_Configuration__c=:pc.id];
        sdc.Performance_Category__c='Coastal';
        update sdc;
        createTestWohHeightConfig(pc);
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        createProductFieldControlDependencies('test',pfc,pfc);        
        Map<string,MakabilityRestResource.OrderItem> configsToTest = customizeCreateOrderItem(pc.Product__c,50,50,50,false,1 ,'1','Coastal');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Size Detail Config  - passed');
    }
    @isTest
    public static void makabilityNegTestForCoastalPerfCategory() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        System.debug('The product configuration pc: '+pc);
        createTestSizeDetailConfig(pc);
        createTestWohHeightConfig(pc);
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        createProductFieldControlDependencies('test',pfc,pfc);        
        Map<string,MakabilityRestResource.OrderItem> configsToTest = customizeCreateOrderItem(pc.Product__c,50,50,50,false,1 ,'1','Coastal');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Size Detail Config - Unable to find correct size detail configuration. Please check that you have included all the correct information for your window.Frame type, Sash Operation, Sash Ratio, Specialty Shape, Hardware options and Positive / Negative Force values.');
    }
    @isTest
    public static void makabilityTestForStandardPerfCategory() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        System.debug('The product configuration pc: '+pc);
        createTestSizeDetailConfig(pc);
        Size_Detail_Configuration__c sdc = [Select id, Performance_Category__c from Size_Detail_Configuration__c where Product_Configuration__c=:pc.id];
        sdc.Performance_Category__c='Standard';
        update sdc;
        createTestWohHeightConfig(pc);
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        createProductFieldControlDependencies('test',pfc,pfc);        
        Map<string,MakabilityRestResource.OrderItem> configsToTest = customizeCreateOrderItem(pc.Product__c,50,50,50,false,1 ,'1','Standard');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Size Detail Config  - passed');
    }
    @isTest
    public static void unableToTestSpecialShapeTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSizeDetailConfig(pc);
        createTestWohHeightConfig(pc);
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,50,true,1 ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results unableToTestSpecialShapeTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Salesforce - Unable to check special shape makability, no Product Field Control Dependency Records Found for this Product Id');
    }
    @isTest
    public static void belowMinTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSizeDetailConfig(pc);
        createTestWohHeightConfig(pc);
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        createProductFieldControlDependencies('test',pfc,pfc);        
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,5,5,50,true,3 ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results belowMinTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        string assertMsg1 = results[0].errorMessages[1];
        string assertMsg2 = results[0].errorMessages[2];
        string assertMsg3 = results[0].errorMessages[3];
        string assertMsg4 = results[0].errorMessages[4];
        system.assertEquals(assertMsg, 'Size Detail Config - UI less than minimum: UI Minimum = 30 inches.');
        system.assertEquals(assertMsg1, 'Size Detail Config - Width less than minimum: Minimum Width = 40 Even inches.');
        system.assertEquals(assertMsg2, 'Size Detail Config - Height less than minimum: Minimum Height = 40 Even inches.');
        system.assertEquals(assertMsg3, 'Check Lock Size - You may only have 1 lock(s) on a unit of this width.');
        system.assertEquals(assertMsg4, 'Check Lock Size - You may only have 1 lock(s) on a unit of this height.');
    }
    @isTest
    public static void aboveMaxTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSizeDetailConfig(pc);
        createTestWohHeightConfig(pc);
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        createProductFieldControlDependencies('test',pfc,pfc);        
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,95,95,50,true,0 ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results aboveMaxTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        string assertMsg1 = results[0].errorMessages[1];
        string assertMsg2 = results[0].errorMessages[2];
        string assertMsg3 = results[0].errorMessages[3];
        string assertMsg4 = results[0].errorMessages[4];
        system.assertEquals(assertMsg, 'Size Detail Config - UI greater than maximum: UI Maximum = 120 inches.');
        system.assertEquals(assertMsg1, 'Size Detail Config - The unit width exceeds the extended maximums.');
        system.assertEquals(assertMsg2, 'Size Detail Config - The unit height exceeds the extended maximums.');
        system.assertEquals(assertMsg3, 'Check Lock Size - You must have at least 1 lock(s) for a unit of this width.');
        system.assertEquals(assertMsg4, 'Check Lock Size - You must have at least 1 lock(s) for a unit of this height.');
    }
    @isTest
    public static void noMatchingProductIdTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSizeDetailConfig(pc);
        createTestWohHeightConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,50,true,1 ,'1');
        set<id> productIds = new set<id>();
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingProductIdTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Salesforce - No Size Detail configurations found for provided productId(s).');
    }
    @isTest
    public static void noMatchingPfcTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSizeDetailConfig(pc);
        createTestWohHeightConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,50,true,1 ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingPfcTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Salesforce - No Product Field Control records found for provided productId(s).');
    }
    @isTest
    public static void doesNotMatchSdcTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSizeDetailConfig(pc);
        createTestWohHeightConfig(pc);
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        createProductFieldControlDependencies('test',pfc,pfc);        
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,50,false,1 ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results doesNotMatchsdcTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Size Detail Config - Unable to find correct size detail configuration. Please check that you have included all the correct information for your window. options may include: Frame type, Sash Operation, Sash Ratio, Specialty Shape and Hardware options.');        
       
    }  

    @isTest
    public static void doesNotMatchDpiTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSizeDetailConfig(pc);
        createTestWohHeightConfig(pc);
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        createProductFieldControlDependencies('test',pfc,pfc);        
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,45,true, 1 ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSizeDetailConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results doesNotMatchDpiTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Size Detail Config - Positve or Negative force values incorrect');             
    }   
         
    private static List<Product2> createTestProduct(Integer num){
        List<Product2> pros = new List<Product2>();
        for(Integer i = 0; i < num;i++){
            Product2 p = new Product2();
            p.name = 'test product ' + i;
            pros.add(p);            
        }        
        insert pros;
        return pros;
    }
    
    private static List<Product_Configuration__c> createTestProConfig(Integer num){
        List<Product2> pros = createTestProduct(num);
        List<Product_Configuration__c> pcs = new List<Product_Configuration__c>();
        for(Integer i = 0 ; i < num; i++){
            Product_Configuration__c pc = new Product_Configuration__c();
            pc.name = 'test config ' + i;
            pc.Tempered_Glass_Required_at_UI__c = 100;	
            pc.Product__c = pros[i].id;  
            pcs.add(pc);
        }
        insert pcs;
        return pcs;
    }

    private static Field_Control_List__c createFieldControlLists(String fieldName){
        Field_Control_List__c fcl = new Field_Control_List__c();
        fcl.Name = fieldName;
        fcl.rForce__c = true;
        fcl.Sales__c = true;
        fcl.Tech__c = true;
        insert fcl;
        return fcl;
    }    

    private static Product_Field_Control__c createProductFieldControl(Product_Configuration__c pc, Field_Control_List__c fcl ){
        Product_Field_Control__c pfc = new Product_Field_Control__c();
        pfc.Name = fcl.Name;
        pfc.Field_Control_ID__c = fcl.id;
        pfc.Product_Configuration_ID__c = pc.id;
        pfc.Required__c = true;
        insert pfc;
        return pfc;
    }

    // private static map<String,Product_Field_Control__c> createPfcMap(Id prodId,Product_Field_Control__c pfc){
    //     map<String,Product_Field_Control__c> result = new map<String,Product_Field_Control__c>();
    //     String key = prodId + '-' + pfc.Name;
    //     result.put(key,pfc);
    //     return result;
    // }
    private static void createProductFieldControlDependencies(String controllingValue, Product_Field_Control__c controllingField, Product_Field_Control__c dependentField ){
        Product_Field_Control_Dependency__c pfcd = new Product_Field_Control_Dependency__c();
        pfcd.Action_Taken__c = 'Disable';
        pfcd.Controlling_Field__c = controllingField.id;
        pfcd.Controlling_Value__c = controllingValue;
        pfcd.Dependent_Field__c = dependentField.id;
        insert pfcd;
        
    }       
    
    private static Size_Detail_Configuration__c createTestSizeDetailConfig(Product_Configuration__c pc){
        Size_Detail_Configuration__c sdc = new Size_Detail_Configuration__c();  
            sdc.Product_Configuration__c = pc.id;
            sdc.Performance_Category__c = 'DP Upgrade';
            sdc.Extended_Max_Height_Inches__c = 80;
            sdc.Extended_Max_Height_Fraction__c = 'Even';
            sdc.Extended_Max_Width_Inches__c = 80;
            sdc.Extended_Max_Width_Fraction__c = 'Even';
            sdc.Lock_Max_Width_Inches__c = 15;
            sdc.Lock_Max_Height_Inches__c = 15;
            sdc.Lock_Min_Width_Inches__c = 25;
            sdc.Lock_Min_Height_Inches__c = 25;
            sdc.Lock_Max_Width_Fraction__c = 'Even';
            sdc.Lock_Min_Width_Fraction__c = 'Even';
            sdc.Lock_Max_Height_Fraction__c = 'Even';
            sdc.Lock_Min_Height_Fraction__c = 'Even';
            sdc.Lock_Min_Width_Locks__c = '1';
            sdc.Lock_Max_Width_Locks__c = '1';
            sdc.Lock_Max_Height_Locks__c = '1';
            sdc.Lock_Min_Height_Locks__c = '1';
            sdc.Positive_Force__c = 50;
            sdc.Frame_Type__c = 'Flat Sill';
            sdc.Hardware_Options__c = 'Normal Hinge';
            sdc.Negative_Force__c = 50;
            sdc.Max_Height_Inches__c = 40;
            sdc.Max_Height_Fraction__c = 'Even';
            sdc.Max_Width_Inches__c = 40;
            sdc.Max_Width_Fraction__c = 'Even';
            sdc.Min_Height_Inches__c = 40;
            sdc.Min_Height_Fraction__c = 'Even';        
            sdc.Min_Width_Inches__c = 40;
            sdc.Min_Width_Fraction__c = 'Even';
            sdc.Sash_Operation__c = 'Right';
            sdc.Sash_Ratio__c = '1:1';
            sdc.Specialty_Shape__c = 'Circle';
            sdc.United_Inch_Maximum__c = 120;
            sdc.United_Inch_Minimum__c = 30;
            insert sdc;
            return sdc;
    }

    private static Map<String,MakabilityRestResource.OrderItem>  createOrderItem(Id prodId, Decimal wInch, Decimal hInch, Decimal posForce,Boolean dpUpgrade,Decimal locks,String oiId){
        Map<string,MakabilityRestResource.OrderItem> orderItems = new Map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
        MakabilityCalculator mc = new MakabilityCalculator();
		ProductConfiguration pc = new ProductConfiguration();
        pc.widthInches = wInch;
        pc.widthFractions = 'Even';
        pc.heightInches = hInch;
        pc.heightFractions = 'Even';        
        pc.frame = 'Flat Sill';
        pc.hardwareOption = 'Normal Hinge';
        pc.sashOperation = 'Right';
        pc.sashRatio = '1:1';
        pc.externalForce = posForce;
        pc.internalForce = 50;
        pc.highPerformance = dpUpgrade;
        pc.leftLegInches = null;
        pc.leftLegFraction = null;
        pc.rightLegInches = null;
        pc.rightLegFraction = null;
        pc.locks = locks; 
        pc.specialtyShape = 'Circle';
        mc.checkSpecialShape = true;
        mc.hasLeftLeg = false;
        mc.hasRightLeg = false;
        mc.checkLegCalc = false;
        /////////////////////       
        pc.productId = prodId; 
        oi.orderItemId = oiId;
        oi.makabilityCalculator = mc;
        oi.productConfiguration = pc;
        orderItems.put(oiId,oi);       
        return orderItems;
    }
    
    private static Map<String,MakabilityRestResource.OrderItem>  customizeCreateOrderItem(Id prodId, Decimal wInch, Decimal hInch, Decimal posForce,Boolean dpUpgrade,Decimal locks,String oiId,String perfCategory){
        Map<string,MakabilityRestResource.OrderItem> orderItems = new Map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
        MakabilityCalculator mc = new MakabilityCalculator();
		ProductConfiguration pc = new ProductConfiguration();
        pc.widthInches = wInch;
        pc.widthFractions = 'Even';
        pc.heightInches = hInch;
        pc.heightFractions = 'Even';        
        pc.frame = 'Flat Sill';
        pc.hardwareOption = 'Normal Hinge';
        pc.sashOperation = 'Right';
        pc.sashRatio = '1:1';
        pc.externalForce = posForce;
        pc.internalForce = 50;
        pc.highPerformance = dpUpgrade;
        pc.performanceCategory=perfCategory;
        pc.leftLegInches = null;
        pc.leftLegFraction = null;
        pc.rightLegInches = null;
        pc.rightLegFraction = null;
        pc.locks = locks; 
        pc.specialtyShape = 'Circle';
        mc.checkSpecialShape = true;
        mc.hasLeftLeg = false;
        mc.hasRightLeg = false;
        mc.checkLegCalc = false;
        /////////////////////       
        pc.productId = prodId; 
        oi.orderItemId = oiId;
        oi.makabilityCalculator = mc;
        oi.productConfiguration = pc;
        orderItems.put(oiId,oi);       
        return orderItems;
    }

    private static Wide_Open_Hinge_Height_Configuration__c createTestWohHeightConfig(Product_Configuration__c pc){
        Wide_Open_Hinge_Height_Configuration__c wohc = new Wide_Open_Hinge_Height_Configuration__c();
        
            wohc.Ending_Width_Fractions__c = 'Even';
            wohc.Ending_Width_Inches__c = 40;
            wohc.Max_Height_Inches__c = 40;
            wohc.Max_Height_Fractions__c ='Even';
            wohc.Product_Configuration__c = pc.id;
            wohc.Sash_Ratio__c = '1:1';
            wohc.Starting_Width_Fractions__c = 'Even';
            wohc.Starting_Width_Inches__c = 39;
            
            insert wohc;
            return wohc;
    }
}