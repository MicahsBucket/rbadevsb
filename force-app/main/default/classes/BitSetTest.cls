@isTest
public class BitSetTest {
	@isTest
    public static void testController() {
        BitSet b = new BitSet();
        System.assertEquals(b.alphaNumCharCodes.get('A'), 65);
    }
    
	@isTest
    public static void testBitTest() {
        BitSet b = new BitSet();
        List <Integer> iList = b.testBits('ABCDEFGHIJKLMN', new List<Integer>{65, 66, 75, 100, 102, 103, 110, 120, 118, 125});
        System.assertEquals(iList.size(), 4);
    }    
}