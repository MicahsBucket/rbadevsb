/*

  Allows the runPayments method to be schedulable.
  Checks for pay periods (Comm_Payment_Event's) that have ended

*/

public class PaymentEventSchedulable implements Schedulable {
  
  public static String sched = '0 00 01 * * ?';  //Every Day at 1:00

  public static String schedulePaymentEvents(String jobName) {
    PaymentEventSchedulable SC = new PaymentEventSchedulable();
    return System.schedule(jobName, sched, SC);
  }

  public void execute(SchedulableContext sc) {
    Map<String, RMS_Settings__c> RMS_Settings_map = RMS_Settings__c.getAll(); 
    RMS_Settings__c batchSizeSetting = RMS_Settings_map.get('Commission User Batch Size');
    Integer batchSize = batchSizeSetting != null ? Integer.ValueOf(batchSizeSetting.Value__c) : 100;

    //CommissionsManagement.runPayments();
    CalculateCommissionsBatchable obj = new CalculateCommissionsBatchable();
    Database.executeBatch(obj, batchSize);
  }

}