public class CanvassMapController {
    
    public list<Dashboard> DashboardList {get;set;}
    public string MapSession {get;set;}
    public boolean isCurrentUserLicensed {
        get
        {
            return UserInfo.isCurrentUserLicensed('sma');
        }
    }
    
    public String CUs {get;set;}
    
    public CanvassMapController()
    {
        DashboardList = new list<Dashboard>();
        
        
        //put tiles of dashboards in the order you want them displayed
        list<string> DashBoardListToSearch = new list<string>{'My Results: Canvassing Stats','My Ranking: Canvassing Leaderboards','Team Results','Cross Market Dashboard'};
        
        string DynamicDashboardQry = 'SELECT ID, Title FROM Dashboard Where Title IN (\'' + string.join(DashBoardListToSearch,'\',\'') + '\')';
        list<Dashboard> DashboardQryResults = database.query(DynamicDashboardQry);
        
        
        for (string DashboardTitle : DashBoardListToSearch)
        {
            for (Dashboard DashboardObj : DashboardQryResults)
            {
                if (DashboardObj.Title == DashboardTitle)
                {
                    DashboardList.add(DashboardObj);
                    break;
                }
            }
        }
        
        MapSession = '';
        
        //See if there is a settings record for the user already
        CanvassMapSettings__c SettingsRecord = CanvassMapSettings__c.getValues(UserInfo.getUserId());
        
        if (SettingsRecord != null)
        {
            //check to make sure the date is today
            if (SettingsRecord.Date__c == date.today())
            {
                MapSession = SettingsRecord.MapSession__c;
            }
        }

        List<CNVSS_Canvass_Unit__c> CUList = [SELECT FORMAT(Assessment_Value__c) AssessmentValue,Assessment_Value__c,CNVSS_DO_NOT_CONTACT__c,CNVSS_Year_Built__c,
            CNVSS_Homeowner_Full_Name__c,CNVSS_Last_Result__c,Id,Name,MALatitude__c,MALongitude__c,
            CNVSS_StreetAddress__c,CNVSS_Canvass_Market__r.MapAnything_Marker_Reset_in_Days__c,CNVSS_Last_Result_Datetime__c 
            FROM CNVSS_Canvass_Unit__c WHERE MALatitude__c!=null AND MALongitude__c!=null 
            AND DateAssigned__c=TODAY AND OwnerId !=:UserInfo.getUserID() AND DateAssigned__c=TODAY AND OwnerId !=:UserInfo.getUserID() and (Canvasser_2__c=:UserInfo.getUserID() OR
                                           	Canvasser_3__c=:UserInfo.getUserID() OR
                                           	Canvasser_4__c=:UserInfo.getUserId()) LIMIT 1000];
            
        Cus = JSON.serialize(CUList);
        
    }

    
    @RemoteAction
    public static string StoreMapSession(string MapSession)
    {
        try 
        {
            //See if there is a settings record for the user already
            CanvassMapSettings__c SettingsRecord = CanvassMapSettings__c.getValues(UserInfo.getUserId());
            
            //if there is a record, let's use that one, if not let's make a new one
            if (SettingsRecord == null)
            {
                SettingsRecord = new CanvassMapSettings__c();
                SettingsRecord.SetupOwnerId = UserInfo.getUserId(); //set the user so it doesn't get set as "company"
            }
            
            SettingsRecord.Date__c = Date.Today();
            SettingsRecord.MapSession__c = MapSession;
            
            upsert SettingsRecord;
            
            return 'Good!';

        } 
        catch(Exception e) 
        {
            return e.getStackTraceString() + ',' + e.getMessage();
        }
    
        
        
    }
    
    
}