public class RMS_viewServiceRequestCtrlr {

    public final Order o;

    public String baseURL{get;set;}
    public String serviceOrderInstallRecordTypeURL { get; set; }
    public String serviceOrderHOARecordTypeURL { get; set; }
    public String serviceOrderHistoricalRecordTypeURL { get; set; }
    public String serviceOrderLSWPRecordTypeURL { get; set; }
    public String serviceOrderPaintStainRecordTypeURL { get; set; }
    public String serviceOrderPermitRecordTypeURL { get; set; }
    public String serviceOrderCollectionsRecordTypeId { get; set; }
    public String serviceOrderCollectionsRecordTypeURL { get; set; }
    public String serviceOrderServiceRecordTypeURL { get; set; }
    public Integer countOfServiceOrderProducts { get; private set; }
    public String serviceOrderJobSiteVisitRecordTypeURL { get; set; }
    private Set<String> showRejectProfiles = new Set<String>{'HomeOwner Resolution',
        'Super Administrator', 'System Administrator'};

    public id ordRecId;

    public Boolean showRejectButton {
        get {
            String profileName = [select Profile.Name from User
                where Id = :Userinfo.getUserId()].Profile.Name;
            return showRejectProfiles.contains(profileName) && o.Status != 'Warranty Rejected';
        }
    }

    public RMS_viewServiceRequestCtrlr(ApexPages.StandardController stdController) {
        if(!test.isRunningTest()){
            stdController.addFields(new List<String>{'RecordType','Id','RecordType.Name','Lock_Service_Request__c'});
        }

        this.o = (Order)stdController.getRecord();

        baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        try {
          String netId = Network.getNetworkId();
          ConnectApi.Community comm = ConnectApi.Communities.getCommunity(netId);
          baseUrl = comm.siteUrl;
        } catch(Exception e) {}

        Schema.DescribeSObjectResult r = WorkOrder.sObjectType.getDescribe();
        String workOrderKeyPrefix = r.getKeyPrefix();

        String ordId = o.id;
        list<Order> theOrd = [SELECT Id, AccountId, Status, RecordType.Name, Store_Location__c, OrderNumber, OpportunityId, BillToContactId, RecordTypeId FROM Order WHERE Id =:ordId ];

        list<Account> relatedAccount = [SELECT  Name,
                                        ShippingStreet,
                                        ShippingCity,
                                        ShippingState,
                                        ShippingPostalCode,
                                        ShippingCountry,
                                        Historical__c,
                                        HOA__c,
                                        Building_Permit__c
                                        FROM Account WHERE Id = :theOrd[0].AccountId];
        Id servProd = UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'); 
        countOfServiceOrderProducts = [SELECT Count() from OrderItem  where OrderId =: o.Id and PricebookEntry.Product2.RecordTypeId =: servProd];

        String endURL =                     '&orderId='+o.Id+
            '&storeId='+theOrd[0].Store_Location__c+
            '&accountId='+relatedAccount[0].Id+
            '&shippingStreet='+relatedAccount[0].ShippingStreet+
            '&shippingCity='+relatedAccount[0].ShippingCity+
            '&shippingState='+relatedAccount[0].ShippingState+
            '&shippingPostalCode='+relatedAccount[0].ShippingPostalCode+
            '&shippingCountry='+relatedAccount[0].ShippingCountry+
            '&relatedOpp='+theOrd[0].OpportunityId+
            '&billToContactId='+theOrd[0].BillToContactId+
            '&permit='+relatedAccount[0].Building_Permit__c+
            '&hoa='+relatedAccount[0].HOA__c+
            '&historical='+relatedAccount[0].Historical__c;

        serviceOrderInstallRecordTypeURL = '/apex/RMS_createVisitWorkOrderService?type=visit'+endURL;
        serviceOrderServiceRecordTypeURL = '/apex/RMS_createVisitWorkOrderService?type=service'+endURL;
        //serviceOrderJobSiteVisitRecordTypeURL = '/apex/RMS_createVisitWorkOrderService?type=Job_Site_Visit'+endURL;
        serviceOrderJobSiteVisitRecordTypeURL = '/apex/RMS_newWorkOrderRedirect?type=jobSiteVisit'+endURL;
        serviceOrderHOARecordTypeURL =  '/apex/RMS_createActionsWorkOrder?type=hoa'+endURL;
        serviceOrderHistoricalRecordTypeURL = '/apex/RMS_createActionsWorkOrder?type=historical'+endURL;
        serviceOrderLSWPRecordTypeURL = '/apex/RMS_createActionsWorkOrder?type=lswp'+endURL;
        serviceOrderPaintStainRecordTypeURL = '/apex/RMS_createActionsWorkOrder?type=paintStain'+endURL;
        serviceOrderPermitRecordTypeURL = '/apex/RMS_createActionsWorkOrder?type=permit'+endURL;
        serviceOrderCollectionsRecordTypeURL = '/apex/RMS_createActionsWorkOrder?type=collections'+endURL;

    }

    public PageReference rejectOrder(){
        PageReference pageRef = null;
        try{
            o.Status = 'Warranty Rejected';
            upsert o;
            pageRef = new PageReference('/' + o.id);
            pageRef.setRedirect(true);
        }catch(DMLException ex){
            ApexPages.addMessages(ex);
        }

        return pageRef;
    }

    public PageReference cancelOrder(){
        PageReference pageRef = null;
        try{
            o.Status = 'Cancelled';
            o.Date_Cancelled__c = system.today();
            upsert o;
            pageRef = new PageReference('/' + o.id);
            pageRef.setRedirect(true);
        }catch(DMLException ex){
            ApexPages.addMessages(ex);
        }

        return pageRef;
    }

    public PageReference orderRedirect(){
        PageReference newPage = null;
        if (o.RecordType.Name == 'CORO Service'){
            If(o.Lock_Service_Request__c == TRUE){
                newPage = new PageReference('/apex/RMS_viewServiceRequestLocked?id='+ o.Id);
            } else {
                newPage = new PageReference('/apex/RMS_viewServiceRequest?id='+ o.Id);
            }
        } else {
            newPage = new PageReference('/'+ o.Id);
            newPage.getParameters().put('nooverride','1');
        }

        newPage.setRedirect(true);
        return newPage;
    }

    public PageReference editOrderRedirect(){
        PageReference newPage = null;
        if (o.RecordType.Name == 'CORO Service'){
                newPage = new PageReference('/apex/RMS_editServiceRequest?id='+ o.Id);
        } else {
            newPage = new PageReference('/'+o.Id+'/e?');
            newPage.getParameters().put('retURL',o.Id);
            newPage.getParameters().put('nooverride','1');
        }

        newPage.setRedirect(true);
        return newPage;
    }

    public PageReference updateServiceType(){
        PageReference newPage;
        newPage = new PageReference('/apex/RMS_updateServiceType?id='+o.id);
        newPage.setRedirect(true);
        return newPage;
    }

    public PageReference refreshPage(){
        PageReference newPage;
        newPage = new PageReference('/'+o.Id);
        newPage.setRedirect(true);
        return newPage;
    }

    public PageReference editServiceRequest(){
        PageReference newPage;
        newPage = new PageReference('/apex/RMS_editServiceRequest?id='+o.id);
        newPage.setRedirect(true);
        return newPage;
    }
}