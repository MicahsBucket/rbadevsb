@isTest
public class CanvassMapControllerTest 
{
    @isTest(SeeAllData=true)
    public static void testmethod1() 
    {
        // Create a Canvass Market
        CNVSS_Canvass_Market__c testCanvassMarket = CanvassTestDataUtils.createTestCanvassMarket();
        ID canvassMarketId = testCanvassMarket.Id;
            
        // Create Canvass Units
        CNVSS_Canvass_Unit__c canvassUnit = CanvassTestDataUtils.createTestCanvassUnit(canvassMarketId, false);
        
        Test.startTest();
        CanvassMapController cmap=new CanvassMapController();
        CanvassMapController.StoreMapSession('{"center":{"lat":42.288760978114404,"lng":-88.10612278027344},"zoom":12}');
        //createCanvassSessionId();
        //CanvassMapController.StoreMapSession('{"center":{"lat":42.288760978114404,"lng":-88.10612278027344},"zoom":12}');
        Test.stopTest();
    }
    private static void createCanvassSessionId()
    {
        CanvassMapSettings__c cmapst=new CanvassMapSettings__c();
        cmapst.SetupOwnerId=UserInfo.getUserId();
        cmapst.Date__c=System.today()-20;
        cmapst.MapSession__c='{"center":{"lat":42.288760978114404,"lng":-88.10612278027344},"zoom":11}';
        insert cmapst;
        
    }
}