@istest
public with sharing class ProductSelectorControllerTest {
    
    @testSetup
    static void setup(){
        
        // Setup products
        List<Product2> testProducts = new List<Product2>();
        Id masterRecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Master Product').getRecordTypeId();
 
        // Create some master products
        Product2 mProd1 = TestDataFactoryStatic.createProduct('MasterProdExample1',masterRecordTypeId);
        mProd1.isActive = true;
        mProd1.Inventoried_Item__c = true;
        testProducts.add(mProd1);

        Product2 mProd2 = TestDataFactoryStatic.createProduct('MasterProdExample2',masterRecordTypeId);
        mProd2.isActive = false;
        mProd2.Inventoried_Item__c = true;
        testProducts.add(mProd2);

        Product2 mProd3 = TestDataFactoryStatic.createProduct('MasterProdExample3',masterRecordTypeId);
        mProd3.isActive = true;
        mProd3.Inventoried_Item__c = false;
        testProducts.add(mProd3);

        // Save for later
        upsert testProducts;

    }


    @isTest
    private static void testSearch() {

        Test.startTest();

        List<Product2> results;

        // Test no search term, should have no results
        results = ProductSelectorController.findProducts('', false);
        System.assertEquals(results.size(), 0);

        // Test "Master" search term, should have one result because 
        // the two other test products should be filtered out by the base query
        results = ProductSelectorController.findProducts('Master',false);
        System.assertEquals(results.size(), 1);

        Test.stopTest();   

    }
}