/*
* @author Jason Flippen
* @date 01/08/2020 
* @description Test Class provides code coverage for the following classes:
*               - ReleasePOActionController
*/
@isTest
public class ReleasePOActionControllerTest {

    /*
    * @author Jason Flippen
    * @description Method to create data to be consumed by test methods.
    * @param N/A
    * @returns N/A
    */ 
    @testSetup static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        Account testVenderAcct = testUtility.createVendorAccount('Test Vendor Account');
        insert testVenderAcct;

        Account testStoreAcct = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAcct = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAcct.Store_Location__c = testStoreAcct.Id;
        insert testDwellingAcct;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAcct.Id, 'Closed - Won');
        insert testOpportunity;
    
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;

        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId(),
                                            Vendor__c = testVenderAcct.Id,
                                            Cost_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c =  testFan.Id);
        insert testProduct;

        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order',
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Service').getRecordTypeId(),
                                     AccountId = testDwellingAcct.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAcct.Id,
                                     OpportunityId = testOpportunity.Id,                                 
                                     Status = 'Draft',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;
        
        Purchase_Order__c testPurchaseOrder = new Purchase_Order__c(Name = 'ABCD12345',
                                                                    Order__c = testOrder.Id,
                                                                    RecordTypeId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Service_Purchase_Order').getRecordTypeId(),
                                                                    Status__c = 'In Process');
        insert testPurchaseOrder;

        OrderItem testOI = new OrderItem(OrderId = testOrder.Id,
                                         Has_PO__c = true,
                                         Purchase_Order__c = testPurchaseOrder.Id,
                                         PricebookentryId = testPBEStandard.Id,
                                         Quantity = 2,
                                         UnitPrice = 100);
        insert testOI;

        Charge__c testCharge = new Charge__c(Service_Request__c = testOrder.Id,
                                             Service_Product__c = testOI.Id);
        insert testCharge;

    }

    /*
    * @author Jason Flippen
    * @description Method to test the functionality in the Controller.
    * @param N/A
    * @returns N/A
    */ 
    private static testMethod void testController() {

        Order testOrder = [SELECT Id, Status FROM Order WHERE Name = 'Test Order'];
        Purchase_Order__c testPurchaseOrder = [SELECT Id FROM Purchase_Order__c WHERE Order__c = :testOrder.Id];

        Test.startTest();

            // Retrieve Purchase Order record. It should match the one we created for testing.
            ReleasePOActionController.PurchaseOrderWrapper purchaseOrder = ReleasePOActionController.getPurchaseOrderData(testPurchaseOrder.Id);
            System.assertEquals(testPurchaseOrder.Id, purchaseOrder.id);

            // The Status of the Purchase Order should now be set to "Released".
            ReleasePOActionController.releasePurchaseOrder(testPurchaseOrder.Id);
            Purchase_Order__c updatedPurchaseOrder = [SELECT Id, Status__c FROM Purchase_Order__c WHERE Id = :testPurchaseOrder.Id];
            System.assertEquals('Released', updatedPurchaseOrder.Status__c);

        Test.stopTest();

    }

}