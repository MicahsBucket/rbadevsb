@isTest
public with sharing class ChangeOrderListCtlrTest {

    @testSetup static void setupData() {
 
        TestUtilityMethods testUtility = new TestUtilityMethods();
                testUtility.setUpConfigs();

                Account testVenderAcct = testUtility.createVendorAccount('Test Vendor Account');
                insert testVenderAcct;

                Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                Account_Type__c='Cost PO');
                insert testFAN;
        
                Account testStoreAcct = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
                Account testDwellingAcct = testUtility.createDwellingAccount('Test Dwelling Account');
                testDwellingAcct.Store_Location__c = testStoreAcct.Id;
                Store_Configuration__c storeConfig1 = [SELECT id FROM Store_Configuration__c WHERE Store__c = :testStoreAcct.id ];
                insert testDwellingAcct;
        
                Contact contact1 = new contact ( FirstName='Contact',LastName='1',AccountId=testDwellingAcct.id);
                insert contact1;
                
                Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAcct.Id, 'Closed - Won');
                insert testOpportunity;

                List<Product2> testProductList = new List<Product2>();
                // Parent Product
                Product2 testParentProduct = new Product2(Name = 'Test Parent Product',
                                                          RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId(),
                                                          Vendor__c = testVenderAcct.Id,
                                                          Cost_PO__c = true,
                                                          Number_Service_Products__c = 1,
                                                          IsActive = true,
                                                          Account_Number__c =  testFan.Id);
                testProductList.add(testParentProduct);

                Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
                insert testPricebook;
        
                //Id pricebookId = Test.getStandardPricebookId();
        
                List<PricebookEntry> testPBEList = new List<PricebookEntry>();
                // PricebookEntry
                PricebookEntry testPB = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testParentProduct.Id);
                testPBEList.add(testPB);


                Id changeOrderId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Change_Order').getRecordTypeId();
        
        
                Order testOrder =  new Order(Name = 'TestOrder',
                                             Pricebook2Id = testPricebook.Id,
                                             AccountId = testDwellingAcct.Id,
                                             OpportunityId = testOpportunity.Id,
                                             Status = 'Job Closed',
                                             Tech_Measure_Status__c = 'New',  
                                             EffectiveDate = Date.Today(),
                                             Revenue_Recognized_Date__c = Date.Today());
                insert testOrder;

                Order testOrder2 =  new Order(RecordTypeId = changeOrderId,
                                              Pricebook2Id = testPricebook.Id,
                                              Sold_Order__c = testOrder.Id,
                                              AccountId = testDwellingAcct.Id,
                                              OpportunityId = testOpportunity.Id,
                                              Status = 'Job Closed',
                                              Tech_Measure_Status__c = 'New',  
                                              EffectiveDate = Date.Today());
                insert testOrder2;
                
            } // End Method: setupData()
        
            /**
            * @author Wallace Wylie
            * @description Method to test the functionality in the Controller.
            * @param N/A
            * @returns N/A
            */ 
            private static testMethod void testController() {
        
                Test.startTest();
        
                    List<Order> testOrderList = [SELECT Id FROM Order WHERE Name = 'TestOrder'];
            
                    List<ChangeOrderListCtlr.ChangeOrderWrapper> coList = ChangeOrderListCtlr.getChangeOrderList(testOrderList[0].id);
                    System.assertEquals(1,coList.size());

                    List<ChangeOrderListCtlr.ChangeOrderRevWrapper> corList = ChangeOrderListCtlr.getOrderRevRec(testOrderList[0].id);
                    System.assertEquals(1,corList.size());

                    Map<String, String> ccoList = ChangeOrderListCtlr.createChangeOrder(testOrderList[0].id);
                    System.assertEquals(1,ccoList.size());
        
        
                Test.stopTest();
        
            } // End Method: testController()
        }