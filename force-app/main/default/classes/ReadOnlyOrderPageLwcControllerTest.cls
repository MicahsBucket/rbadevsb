/**
 * @File Name          : ReadOnlyOrderPageLwcControllerTest.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 5/2/2019, 2:30:49 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/2/2019, 2:30:49 PM   mark.rothermal@andersencorp.com     Initial Version
**/
@isTest
public  class ReadOnlyOrderPageLwcControllerTest {

    @isTest
    private static void getRelatedOrderItemsTest(){
        List<Product_Configuration__c> pcs = createTestProConfig(1);
        Product_Configuration__c pc = pcs[0];
        Order o = mockUpOrderForTests();
        OrderItem oi = mockupOrderItemForTests(pc.Product__c);
        oi.OrderId = o.Id;
        insert oi;
        test.startTest();
        LightningResponse response = ReadOnlyOrderPageLwcController.getRelatedOrderItems(o.Id);
        test.stopTest(); 
        system.debug('response in getRelatedOrderItemsTest '+response);    
    }


       private static List<Product2> createTestProduct(Integer num){
        List<Product2> pros = new List<Product2>();
        for(Integer i = 0; i < num;i++){
            Product2 p = new Product2();
            p.name = 'test product ' + i;
            pros.add(p);            
        }        
        insert pros;
        return pros;
    }
    
    private static List<Product_Configuration__c> createTestProConfig(Integer num){
        List<Product2> pros = createTestProduct(num);
        List<Product_Configuration__c> pcs = new List<Product_Configuration__c>();
        for(Integer i = 0 ; i < num; i++){
            Product_Configuration__c pc = new Product_Configuration__c();
            pc.name = 'test config ' + i;
            pc.Tempered_Glass_Required_at_UI__c = 100;	
            pc.Product__c = pros[i].id;  
            pcs.add(pc);
        }
        insert pcs;
        return pcs;
    }


    private static Order mockUpOrderForTests (){
                RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name = 'Turn Financial Transactions Off', Value__c = 'Yes');
        insert turnOffFinancialTrigger;
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        
        Account dwelling1 = utility.createDwellingAccount('Dwelling Account');       
        Account store1 = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
        Store_Configuration__c storeConfig1 = [SELECT id FROM Store_Configuration__c WHERE Store__c = :store1.id ];
        dwelling1.Store_Location__c = store1.Id;         
        insert dwelling1;       
        
        Contact contact1 = new contact ( FirstName='Contact',LastName='1',AccountId=dwelling1.id);
        insert contact1;
        
        Financial_Account_Number__c finacialAccountNumber1 = new Financial_Account_Number__c (  Store_Configuration__c = storeConfig1.id, Name = '1');
        insert finacialAccountNumber1;
        Financial_Account_Number__c finacialAccountNumber2 = new Financial_Account_Number__c (  Store_Configuration__c = storeConfig1.id, Name = '2');
        insert finacialAccountNumber2;
        
        Financial_Transaction__c finacialTransaction1 = new Financial_Transaction__c(  Store_Configuration__c = storeConfig1.id,
                                                                                     Transaction_Type__c = 'Inventory Received - External Vendor',
                                                                                     Debit_Account_Number__c = finacialAccountNumber1.id,
                                                                                     Credit_Account_Number__c = finacialAccountNumber2.id);
        insert finacialTransaction1;
        
        Product2 masterProduct = new Product2( name = 'master',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Master_Product'));
        
        Id pricebookId = Test.getStandardPricebookId();
        system.debug(pricebookId);
        
        List<Product2> products = new List<Product2>{masterProduct};
            insert products;
        PricebookEntry pricebookEntryMaster= utility.createPricebookEntry(pricebookId, masterProduct.id);     
        List<PriceBookEntry> pbEntries = new List<PricebookEntry>{pricebookEntryMaster};
            insert pbEntries;
        //utility.createOrderTestRecords();
        //
        
        Order order =  new Order(   Name='Sold Order 1', 
                                 AccountId = dwelling1.id, 
                                 BilltoContactId = contact1.id,
                                 EffectiveDate= Date.Today(), 
                                 Store_Location__c = store1.Id,                           
                                 Status ='Draft', 
                                 Pricebook2Id = pricebookId
                                );
        insert order;
        return order;
    }

    private static OrderItem mockupOrderItemForTests(Id productId){
        PricebookEntry Pbe = new PricebookEntry(Product2Id = productId, UnitPrice = 200, Pricebook2Id = Test.getStandardPricebookId(), isActive = True);
        insert Pbe;
        OrderItem oi = new OrderItem();
        oi.Locks_Sash__c = '1';
        oi.EJ_Color__c = 'White';
        oi.EJ_Species__c = 'Oak';
        oi.Right_Leg_Inches__c = 25;
        oi.Right_Leg_Fraction__c = 'Even';
        oi.Left_Leg_Inches__c = 25;
        oi.Left_Leg_Fraction__c = 'Even';
        oi.Pocket_Notch__c = false;
        oi.Exterior_Trim__c = 'None';
        oi.Grille_Style__c  = 'grille style';
        oi.Lites_High_S1__c = 0;
        oi.Lites_Wide_S1__c = 0;
        oi.Spokes__c = '0';
        oi.Hubs__c = '0';
        oi.Frame_Type__c = 'Full Frame';
        oi.Hardware_Option__c = 'option';
        oi.Sash_Operation__c  = 'sash op';
        oi.Sash_Ratio__c = '1:1:1';
        oi.Specialty_Shape__c = 'Circle';
        oi.Grille_Pattern__c = 'G pattern';
        oi.Tempered_S1__c = true;
        oi.Tempered_S2__c = true;
        oi.Glass_Pattern_S1__c = 'glazing';
        oi.Glass_Pattern_S2__c = 'glazing';
        oi.Exterior_Color__c = 'White';
        oi.Interior_Color__c = 'White';
        oi.Screen_Type__c = 'screen type';
        oi.Screen_Size__c = 'screen size?';
        oi.PG_50__c = True;
        oi.Positive_Force__c = 25;
        oi.Negative_Force__c = 25;
        oi.Width_Inches__c = 25;
        oi.Width_Fraction__c = 'Even';
        oi.Height_Inches__c = 25;
        oi.Height_Fraction__c = 'Even';
        oi.Product2Id = productId;
        oi.UnitPrice = 200;
        oi.Quantity = 1;
        oi.PricebookEntryId = pbe.Id;
        return oi;
    }
}