/**
 * @author Micah Johnson - Demand Chain 2020
 * @description The job of this "Utility" class is to find out how many Parent Objects require rollup data, based on the passed in child object. 
 *              Each Parent/Child relationship is individually passed to the DynamicRollUpHelper Class for processing/mergin of data, before it is 
 *              passed back to this class for update (if in a Que) or return to the original process that called it (if the class is called directly)
 * @param The parameter passed in is the list of child objects and a string defining whether or not this process was initiated from the Que or called directly
 * @returns This class will always return a list of parent object records to update; 
 *              however, if the Utility is initiated by a Queueable, the parent records will be updated here.
 **/
public without sharing class DynamicRollUpUtility {

    /*
    * @author Micah Johnson - Demand Chain
    * @date 2020
    * @description: Method to retrieve a List of records to be rolled up.
    * @param objList - List of records that have been inserted/updated
    * @param isQue - Flag indicating if the execution is processing in a Que or not
    * @return List<SObject> - List of Parent records that were updated (if applicable)
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * 20210401      Jason Flippen           Refactored to include bulkification and resolve SOQL 101 errors.
    * ====================================================================================================
    */
    public static List<SObject> DynamicRollUp(List<sObject> objList, boolean isQue) {

        UtilityMethods.disableAllFslTriggers();

        Map<Id,SObject> recordUpdateMap = new Map<Id,SObject>();

        if (!objList.isEmpty()) {

            map<String, List<SObject>> ObjectTypeStringToObjectMap = new map<String, List<SObject>>();
            map<String, List<Custom_Rollup_Rolldown__mdt>> ObjectTypeStringToCustomMetadataMap = new map<String, List<Custom_Rollup_Rolldown__mdt>>();
            map<String, List<Custom_Rollup_Rolldown__mdt>> ParentObjectStringToCustomMetadataMap = new map<String, List<Custom_Rollup_Rolldown__mdt>>();
            for(SObject sobj: objList){
                String objectName = String.valueOf(sobj.getSObjectType());
                if (!ObjectTypeStringToObjectMap.containsKey(objectName)) {
                    ObjectTypeStringToObjectMap.put(objectName, new List<SObject>());
                }
                ObjectTypeStringToObjectMap.get(objectName).add(sobj);

                if (!ObjectTypeStringToCustomMetadataMap.containsKey(objectName)) {
                    ObjectTypeStringToCustomMetadataMap.put(objectName, new List<Custom_Rollup_Rolldown__mdt>());
                }

            }
            
            List<Custom_Rollup_Rolldown__mdt> CRRList = new List<Custom_Rollup_Rolldown__mdt>();
            for (Custom_Rollup_Rolldown__mdt customMDT : Custom_Rollup_Rolldown__mdt.getAll().values()) {
                String childObjectName = customMDT.Child_Object__c;
                if (ObjectTypeStringToObjectMap.keySet().contains(childObjectName) && customMDT.Active__c == true) {
                    CRRList.add(customMDT);
                    if (ObjectTypeStringToCustomMetadataMap.containsKey(childObjectName)) {
                        ObjectTypeStringToCustomMetadataMap.get(childObjectName).add(customMDT);
                    }
                }
            }

            for (String objectName: ObjectTypeStringToObjectMap.keySet()) {

                List<SObject> recordList = ObjectTypeStringToObjectMap.get(objectName);

                Map<String,List<Custom_Rollup_Rolldown__mdt>> parentObjectCustomMDTMap = new Map<String,List<Custom_Rollup_Rolldown__mdt>>();
                for (Custom_Rollup_Rolldown__mdt customMDT : ObjectTypeStringToCustomMetadataMap.get(objectName)) {
                    if (!parentObjectCustomMDTMap.containsKey(customMDT.Parent_Object__c)) {
                        parentObjectCustomMDTMap.put(customMDT.Parent_Object__c, new List<Custom_Rollup_Rolldown__mdt>());
                    }
                    parentObjectCustomMDTMap.get(customMDT.Parent_Object__c).add(customMDT);
                }

                for (String parentObject : parentObjectCustomMDTMap.keySet()) {

                    List<Custom_Rollup_Rolldown__mdt> customMDTList = parentObjectCustomMDTMap.get(parentObject);

                    List<SObject> recordUpdateList = DynamicRollupHelper.RollUpHelper(objectName,
                                                                                      recordList,
                                                                                      parentObject,
                                                                                      customMDTList);
                    for (SObject recordUpdate : recordUpdateList) {
                        if (!recordUpdateMap.containsKey(recordUpdate.Id)) {
                            recordUpdateMap.put(recordUpdate.Id,recordUpdate);
                        }
                    }

                }

            }

            if (!recordUpdateMap.isEmpty()) {
                if (isQue == true) {
                    update recordUpdateMap.values();
                }
            }

        }

        return recordUpdateMap.values();

    }

}