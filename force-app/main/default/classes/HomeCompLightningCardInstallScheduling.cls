/*
* @author Jason Flippen
* @date 11/17/2020
* @description Class to provide support for the homeCompLightningCardInstallScheduling LWC.
*
*			   Code Coverage provided by the following Test Class:
*              - HomeCompLightningCardInstallTest
*/ 
public with sharing class HomeCompLightningCardInstallScheduling {

    @AuraEnabled(cacheable=true)
    public static List<InstallNeededOrderWrapper> getInstallNeededOrderList(string searchKey) {

        List<InstallNeededOrderWrapper> wrapperList = new List<InstallNeededOrderWrapper>();

		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
		Set<String> eligibleStatusSet = new Set<String>{'Install Needed'};
		String queryString = 'SELECT Id,' +
							 '		 OrderNumber,' +
							 '		 BillToContact.Name,' +
							 '		 BillToContactId,' +
							 '		 Estimated_Ship_Date__c,' +
							 '		 Total_Finance_Units__c ' +
							 'FROM   Order ' +
							 'WHERE  Status IN :eligibleStatusSet ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'AND (OrderNumber LIKE :tempSearchKey OR BillToContact.Name LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY OrderNumber ' +
					   'LIMIT 20';

		for (Order o : Database.query(queryString)) {
			InstallNeededOrderWrapper wrapper = new InstallNeededOrderWrapper(o.Id,
																			  o.OrderNumber,
																			  o.BillToContact.Name,
																			  o.BillToContactId,
																			  o.Estimated_Ship_Date__c,
																			  o.Total_Finance_Units__c);
			wrapperList.add(wrapper);
		}

		return wrapperList;

	}
	
	@AuraEnabled(cacheable=true)
	public static List<InstallScheduledOrderWrapper> getInstallScheduledOrderList(string searchKey) {

		List<InstallScheduledOrderWrapper> wrapperList = new List<InstallScheduledOrderWrapper>();

		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
		Set<String> eligibleStatusSet = new Set<String>{'Install Scheduled'};
		String queryString = 'SELECT Id,' +
							 '		 OrderNumber,' +
							 '		 BillToContact.Name,' +
							 '		 BillToContactId,' +
							 '		 Estimated_Ship_Date__c,' +
							 '		 Installation_Date__c ' +
							 'FROM   Order ' +
							 'WHERE  Status IN :eligibleStatusSet ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'AND (OrderNumber LIKE :tempSearchKey OR BillToContact.Name LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY OrderNumber ' +
					   'LIMIT 20';

		for (Order o : Database.query(queryString)) {
			InstallScheduledOrderWrapper wrapper = new InstallScheduledOrderWrapper(o.Id,
																					o.OrderNumber,
																					o.BillToContact.Name,
																					o.BillToContactId,
																					o.Estimated_Ship_Date__c,
																					o.Installation_Date__c);
			wrapperList.add(wrapper);
		}

		return wrapperList;

	}

    @AuraEnabled(cacheable=true)
    public static List<InstallConflictOrderWrapper> getInstallConflictOrderList(string searchKey) {
  
		List<InstallConflictOrderWrapper> wrapperList = new List<InstallConflictOrderWrapper>();

		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
		String queryString = 'SELECT Id,' +
							 '		 OrderNumber,' +
							 '		 BillToContact.Name,' +
							 '		 BillToContactId,' +
							 '		 Estimated_Ship_Date__c,' +
							 '		 Installation_Date__c ' +
							 'FROM   Order ' +
							 'WHERE  Install_Ship_Date_Conflict__c = true ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'AND (OrderNumber LIKE :tempSearchKey OR BillToContact.Name LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY OrderNumber ' +
					   'LIMIT 20';

		for (Order o : Database.query(queryString)) {
			InstallConflictOrderWrapper wrapper = new InstallConflictOrderWrapper(o.Id,
																				  o.OrderNumber,
																				  o.BillToContact.Name,
																				  o.BillToContactId,
																				  o.Estimated_Ship_Date__c,
																				  o.Installation_Date__c);
			wrapperList.add(wrapper);
		}
		
		return wrapperList;

	}
  
    @AuraEnabled(cacheable=true)
    public static List<UnconfirmedWorkOrderWrapper> getUnconfirmedWorkOrderList(string searchKey) {
		
		List<UnconfirmedWorkOrderWrapper> wrapperList = new List<UnconfirmedWorkOrderWrapper>();
		
		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
		String queryString = 'SELECT Id,' +
							 '		 WorkOrderNumber,' +
							 '		 Contact_Name_Text__c,' +
							 '		 ContactId,' +
							 '		 Primary_Resource_Name__c,' +
							 '		 Scheduled_Start_Time__c ' +
							 'FROM   WorkOrder ' +
							 'WHERE  Confirmed_Appt_w_Customer__c = null ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'AND (WorkOrderNumber LIKE :tempSearchKey OR Contact_Name_Text__c LIKE :tempSearchKey OR Primary_Resource_Name__c LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY WorkOrderNumber ' +
						'LIMIT 20';

		for (WorkOrder wo : Database.query(queryString)) {
			UnconfirmedWorkOrderWrapper wrapper = new UnconfirmedWorkOrderWrapper(wo.Id,
																				  wo.WorkOrderNumber,
																				  wo.Contact_Name_Text__c,
																				  wo.ContactId,
																				  wo.Primary_Resource_Name__c,
																				  wo.Scheduled_Start_Time__c);
			wrapperList.add(wrapper);
		}

		return wrapperList;

	}


/** Wrapper Classes **/


	public class InstallNeededOrderWrapper {
		
		@AuraEnabled
		public String INOrderNumber{get;set;}
		
		@AuraEnabled
		public String INOrderNumberURL{get;set;}
		
		@AuraEnabled
		public String INOrderRBillToContactName{get;set;}
			
		@AuraEnabled
		public String INOrderRBillToContactURL{get;set;}

		@AuraEnabled
		public Date INOEstimatedShipDate{get;set;}

		@AuraEnabled
		public Decimal TotalUnits{get;set;}
		
		public InstallNeededOrderWrapper(Id INOid,
										 String INOrderNum,
										 String INObtcname,
										 Id INObtcId,
										 Date INOestShipDate,
										 Decimal TotUnits) {
			
			INOrderNumber = INOrderNum;
			INOrderNumberURL = '/'+ INOid;
			INOrderRBillToContactName = INObtcname;
			If(INOrderRBillToContactName == null) {
				INOrderRBillToContactURL = '';
			}
			else {
				INOrderRBillToContactURL = '/' + INObtcId;
			}
			INOEstimatedShipDate = INOestShipDate;
			TotalUnits = TotUnits;
		}

	}
	
	public class InstallScheduledOrderWrapper {

		@AuraEnabled
		public String SOrderNumber{get;set;}

		@AuraEnabled
		public String SOrderNumberURL{get;set;}

		@AuraEnabled
		public String SOrderRBillToContactName{get;set;}

		@AuraEnabled
		public String SOrderRBillToContactURL{get;set;}

		@AuraEnabled
		public Date SOEstimatedShipDate{get;set;}

		@AuraEnabled
		public Date SOInstallationDate{get;set;}

		public InstallScheduledOrderWrapper(Id SOid,
											String SOrderNum,
											String SObtcname,
											Id SObtcId,
											Date SOestShipDate,
											Date SOInsDate) {
			
			SOrderNumber = SOrderNum;
			SOrderNumberURL = '/'+ SOid;
			SOrderRBillToContactName = SObtcname;
			If (SOrderRBillToContactName == null) {
				SOrderRBillToContactURL = '';
			}
			else {
				SOrderRBillToContactURL = '/' + SObtcId;
			}
			SOEstimatedShipDate = SOestShipDate;
			SOInstallationDate = SOInsDate;

		}

	}  

	public class InstallConflictOrderWrapper {
		
		@AuraEnabled
		public String COrderNumber{get;set;}
		
		@AuraEnabled
		public String COrderNumberURL{get;set;}
		
		@AuraEnabled
		public String COrderBillToContactName{get;set;}
			
		@AuraEnabled
		public String COrderBillToContactURL{get;set;}

		@AuraEnabled
		public Date COEstimatedShipDate{get;set;}

		@AuraEnabled
		public Date COInstallationDate{get;set;}
		
		public InstallConflictOrderWrapper(Id COid,
										   String COrderNum,
										   String CObtcname,
										   Id CObtcId,
										   Date COestShipDate,
										   Date COInsDate) {
		
			COrderNumber = COrderNum;
			COrderNumberURL = '/'+ COid;
			COrderBillToContactName = CObtcname;
			If (COrderBillToContactName == null) {
				COrderBillToContactURL = '';
			}
			else {
				COrderBillToContactURL = '/' + CObtcId;
			}
			COEstimatedShipDate = COestShipDate;
			COInstallationDate = COInsDate;

		}

	}
	
	public class UnconfirmedWorkOrderWrapper {
		
		@AuraEnabled
		public String NCWorkOrderNumber{get;set;}
		
		@AuraEnabled
		public String NCWorkOrderNumberURL{get;set;}
		
		@AuraEnabled
		public String NCContactName{get;set;}
		
		@AuraEnabled
		public String NCContactURL{get;set;}

		@AuraEnabled
		public String NCPrimaryResourse{get;set;}

		@AuraEnabled
		public DateTime NCScheduledStartTime{get;set;}
		
		public UnconfirmedWorkOrderWrapper(Id NCid,
										   String NCWorkOrderNum,
										   String NCcname,
										   Id NCcId,
										   String NCprimRes,
										   Datetime NCschedStart) {
		
			NCWorkOrderNumber = NCWorkOrderNum;
			NCWorkOrderNumberURL = '/'+ NCid;
			NCContactName = NCcname;
			If (NCContactName == null) {
				NCContactURL = '';
			}
			else {
				NCContactURL = '/' + NCcId;
			}
			NCPrimaryResourse = NCprimRes;
			NCScheduledStartTime = NCschedStart;

		}

	}

}