/**
* @author Jason Flippen
* @date 01/25/2021
* @description Class to provide functionality for the newChangeOrderAction LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - NewChangeOrderActionControllerTest
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/
public with sharing class NewChangeOrderActionController {

    /**
    * @author Jason Flippen
    * @date 01/25/2021
    * @description: Method to retrieve Order data.
    * @param orderId
    * @return OrderWrapper
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @AuraEnabled(cacheable=true)
    public static Order getOrderData(String orderId) {

        // Retrieve Order data.
        Order order = [SELECT Id,
                              AccountId,
                              BillingStreet,
                              BillToContactId,
                              BillToContact.Name,
                              OpportunityId,
                              OrderNumber,
                              Revenue_Recognized_Date__c,
                              Store_Location__c,
                              Unique_Identifier__c
                       FROM   Order
                       WHERE  Id = :orderId];        
        
        System.debug('***** order: ' + order);
                                                       
        return order;

    }

    /**
    * @author Jason Flippen
    * @date 01/25/2021
    * @description: Method to create a new Change Order.
    * @param order
    * @return saveResultMap
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @AuraEnabled
    public static Map<String,String> createChangeOrder(Order order) {

        Map<String,String> saveResultMap = new Map<String,String>();

        try {

            String billingStreet =  Order.BillingStreet;
            billingStreet = billingStreet.replace('\r\n',' ');
            Date bookingDate = Date.today().addMonths(1);
            String formattedBookingDate = bookingDate.month() + '/' + bookingDate.day() + '/' + bookingDate.year();
            String orderName = order.BillToContact.Name + ' - ' + billingStreet + ' - ' + formattedBookingDate + ' - CO';
            
            Order newChangeOrder = new Order(RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Change_Order').getRecordTypeId(),
                                             AccountId = order.AccountId,
                                             BillToContactId = order.BillToContactId,
                                             EffectiveDate = bookingDate,
                                             Name = orderName,
                                             OpportunityId = order.OpportunityId,
                                             Sold_Order__c = order.Id,
                                             Status = 'In Progress',
                                             Store_Location__c = order.Store_Location__c);

            // 2021-01-25 - Jason Flippen - Could not determine where this value was being used.
            //var uniqueId = "{!Order.Unique_Identifier__c}";
            //location.href=sitePrefix+'801/e?retURL='+sitePrefix+soldOrderId +'&RecordType='+changeOrderRecordTypeId+'&ent=Order&Status='+status+'&opptyid_lkid='+opp+'&accid_lkid='+acc+'&BillToContact='+billToContact+'&CF00N6100000Br7Us='+soldOrderName+'&00NV0000000oxy3='+uniqueId+'&CF00N6100000Br7Uu='+storeLoc+'&EffectiveDate='+bookingDate+'&Name='+name;

            insert newChangeOrder;
            saveResultMap.put('New Change Order Success', newChangeOrder.Id);

        }
        catch (Exception ex) {
            saveResultMap.put(ex.getMessage(), '');
        }

        return saveResultMap;

    }

}