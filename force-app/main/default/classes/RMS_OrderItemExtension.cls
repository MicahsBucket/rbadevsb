/*******************************************************//**

@class  RMS_OrderItemExtension

@author  Brianne Wilson (Slalom.BLW)

@version    2016-3-9  Slalom.BLW
Created.

@copyright  (c)2016 Slalom.  All Rights Reserved. 
Unauthorized use is prohibited.

***********************************************************/
public class RMS_OrderItemExtension {
    
    public final OrderItem o;
    public String baseURL{get;set;}
    public String editURL{get;set;}
    public String addSymptomURL {get;set;}
    //map<String, Service_Field_Ids__c> Service_Field_Ids_map = Service_Field_Ids__c.getAll();
    public String ssSP {get;set;}
    public String oiLabel {get;set;}
    //public List<Order> parent {get;set;}
	
	public String sourcecode {get;set;}
    public String defectcode {get;set;}
	
    public String getSourcecode() { return this.sourcecode; }
    public void setSourcecode(String s) { this.sourcecode = s; }
    //public String getDefectcode() { return this.defectcode; } 
    //public void setDefectcode(String s) { this.defectcode = s; }
    
    public RMS_OrderItemExtension(ApexPages.StandardController stdController) {
        if(!test.isRunningTest()){
            stdController.addFields(new List<String>{'OrderId','Service__c', 'Installed_Product_Name__c', 'Product_Name__c','MTO_Source_Code__c','Defect_Code__c'});           
        }
        
        
        this.o = (OrderItem)stdController.getRecord();
        defectcode = o.Defect_Code__c;
        baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        try {
          String netId = Network.getNetworkId();
          ConnectApi.Community comm = ConnectApi.Communities.getCommunity(netId);
          baseUrl = comm.siteUrl;
        } catch(Exception e) {
          
        }

        editURL ='/apex/RMS_editServiceProduct?id='+o.Id;
        addSymptomURL = '/apex/RMS_addServiceSymptom?ssSP'+'='+o.id;
        
        
        
    }
	
	public List<SelectOption> getsourcecodes() {
      if (o.MTO_Source_Code__c != NULL){ 
	  sourcecode = o.MTO_Source_Code__c;
        } 
      List<SelectOption> optionList = new List<SelectOption>();
      optionList.add(new SelectOption('','- Select source code -'));

       for(Service_Product_Source_Codes__c sc : [select Product_Name__c, Code_Name__c, Name from Service_Product_Source_Codes__c where Service_Product__c IN (Select product2Id from PricebookEntry where Id=: o.pricebookentryid)]){
        optionList.add(new SelectOption(sc.Code_Name__c,sc.Code_Name__c));
      }
      return optionList; 
    }
    
    public List<SelectOption> getdefectcodes() {
	 if(o.Defect_Code__c != NULL){
      defectcode = o.Defect_Code__c;
	  }
      List<SelectOption> optionList = new List<SelectOption>();
      optionList.add(new SelectOption('', '- Select Defect Code -')); 

      system.debug('@@@@'+ sourcecode);
      if(sourcecode != NULL) {
         for (MTOSourceCode_DefectCode__c df : [select Name, Defect_Code_Name__c, MTO_Source_Code_Name__c from MTOSourceCode_DefectCode__c df where df.MTO_Source_Code_Name__c =: sourcecode]){
          optionList.add(new SelectOption(df.Defect_Code_Name__c,df.Defect_Code_Name__c));
       }
     }
      return optionList;
    } 
    
    public PageReference saveCloseTab(){
	o.MTO_Source_Code__c = sourcecode;
           o.Defect_Code__c = defectcode;
        try{                             
            upsert o;
            
            Order parent = [SELECT id FROM Order WHERE id = :o.OrderId];   
            
            
            PageReference savePage = new PageReference('/'+o.id);
            savePage.setRedirect(true);
            return savePage;
        }catch(DMLException ex){
            ApexPages.addMessages(ex);
        }
        return null;
    } 
    
    public PageReference orderItemRedirect(){
        PageReference newPage = null;
        
        
        
        If (o.Service__c == TRUE){
            newPage = new PageReference('/apex/RMS_viewServiceOrderItem?id='+ o.Id);                               
        } else {
            newPage = new PageReference('/'+ o.Id);
            newPage.getParameters().put('nooverride','1');
        }
        
        newPage.setRedirect(true);
        return newPage;
        
        
    }
    
    public PageReference editOrderItem(){
        PageReference newPage;
        newPage = new PageReference('/apex/RMS_editServiceProduct?id='+o.id);
        newPage.setRedirect(true); 
        return newPage;
    } 
    
    public PageReference cancelOrderItem(){
        PageReference newPage;
        newPage = new PageReference('/'+o.id);
        newPage.setRedirect(true); 
        return newPage;
    } 
    
    public PageReference addResponsibility(){
        PageReference newPage;
        newPage = new PageReference('/apex/RMS_addResponsibilitytoSP?id='+o.id+'&mode=e');
        newPage.setRedirect(true); 
        return newPage;
    } 

    public PageReference addWarranty(){
        PageReference newPage;
        newPage = new PageReference('/apex/RMS_addWarrantytoSP?id='+o.id+'&mode=e');
        newPage.setRedirect(true); 
        return newPage;
    } 
    
    public PageReference addServiceSymptom(){
        PageReference newPage;
        newPage = new PageReference('/apex/RMS_addServiceSymptom?ssSP'+'='+o.id+'&oiLabel='+o.Installed_Product_Name__c+': '+o.Product_Name__c);
        newPage.setRedirect(true); 
        return newPage;
    }     
}