/*
 *@class    RMS_salesOrderManager
 *@brief    class used to create orders from quotes
 *@author  Anthony Strafaccia (Slalom.ADS)
 *@author  Mark Wochnick (Slalom.MAW)
 *@version  2015/12/21  Slalom.MAW
 *@copyright  (c)2015 Slalom.  All Rights Reserved. Unauthorized use is prohibited.
 * test class = RMS_salesOrderManagerTest
 */

public with sharing class RMS_salesOrderManager {

    /* @method: createSalesOrderOnOpportunityClosedWon(List<Opportunity> listOld, List<Opportunity> listNew, Map<Id, Opportunity> mapOld, Map<Id, Opportunity> mapNew)
     * @param: List<Opportunity> listOld
     * @param: List<Opportunity> listNew
     * @param: Map<Id, Opportunity> mapOld
     * @param: Map<Id, Opportunity> mapNew
     * @return: void
     */
     public void createSalesOrderOnOpportunityClosedWon(List<Opportunity> listOld, List<Opportunity> listNew, Map<Id, Opportunity> mapOld, Map<Id, Opportunity> mapNew) {
        if (UtilityMethods.hasOpportunityTriggerRan()) 
        {
         if(UtilityMethods.hasOpportunityTriggerRanop())  
          {
              
          }
            else
            {
                return;
            }
        
        }
        System.debug('Total Number of SOQL Queries beginning at createSalesOrderOnOpportunityClosedWon: ' +  Limits.getQueries());
        System.debug('listOld: '+ System.JSON.Serialize(listOld));
        System.debug('listNew: '+ System.JSON.Serialize(listNew));
        //CREATE AND POPULATE LIST OF ONLY OPPORTUNITIES THAT HAVE BEEN CLOSED/WON AND HAVE A SYNCED QUOTE TO PREFORM LOGIC ON
        List<Opportunity> closedWonOpportunities = new List<Opportunity>();
        List<Id> closedWonOpportunitiesIds = new List<Id>();
        List<Order> newOrders = new List<Order>();
        List<Quote> soldQuoteList = new List<Quote>();
        List<Quote> quoteList = new List<Quote>();
        List<OpportunityContactRole> primaryContacts = new List<OpportunityContactRole>();

        for(Opportunity opp : listNew){
            if (opp.StageName == Constants.OPP_STAGE_DONT_GENERATE_ORDER) {
                // this opp is sold but don't generate an order its already been generated in the mobile api
                // this is only done when the opportunity -> New or In Progress or Quoted to Canceled and Sold
                opp.StageName = Constants.OPP_STAGE_SOLD;
            } else if(  opp.IsWon == true  &&  !mapOld.get(opp.id).IsWon){
                closedWonOpportunitiesIds.add(opp.id);
            }
        }
        // only do the following if we have closed won opportunities
        System.debug('closedWonOpportunitiesIds.size(): '+closedWonOpportunitiesIds.size());
        if (closedWonOpportunitiesIds.size() > 0) {
            // get the quotes, quote line items, quote discounts, quote financing of the sold quote and the primary contact for each close won opportunity
            soldQuoteList = [select Id, OpportunityId, Pricebook2Id, isSold__c, Name, rSuite_Id__c,
                Opportunity.AccountId, Opportunity.Store_Location__c, Opportunity.OwnerId, Opportunity.Account.BillingStreet,
                Opportunity.Account.BillingCity, Opportunity.Account.BillingStateCode, Opportunity.Account.BillingPostalCode,
                Opportunity.Account.BillingCountryCode, Opportunity.Account.ShippingStreet,
                Opportunity.Account.ShippingCity, Opportunity.Account.ShippingStateCode, Opportunity.Account.ShippingPostalCode,
                Opportunity.Account.ShippingCountryCode, Opportunity.Description, Opportunity.CloseDate, Opportunity.Pricebook2Id,
                Opportunity.Store_Number__c,Opportunity.ROP_Opportunity__c
                from Quote where isSold__c = true and OpportunityId in :closedWonOpportunitiesIds];

            List<Quote_Discount__c> qdList = [select Id, Name, Discount_Amount__c, Per_Unit_Discount__c, rSuite_Id__c, Store_Discount__c, Discount_Description__c, Status__c, Quote__c,
                Quote__r.OpportunityId
                from Quote_Discount__c where Quote__c in :soldQuoteList];

            // get the quote financing
            List<Quote_Financing__c> qfList = [SELECT Id, Name, Amount_Financed__c, Store_Finance_Program__c, Related_Quote__c,
                                                Authorization_Code__c, Payment_Display__c, Related_Quote__r.OpportunityId,
                                                Expiration_Date__c, Program_Fee__c, Program_Rate__c, rSuite_Id__c, Related_Quote__r.Opportunity.Store_Location__c
                                                from Quote_Financing__c where Related_Quote__c in :soldQuoteList];

            //Dynamicaly query for all QuoteLineItem fields
            SObjectType qliType = Schema.getGlobalDescribe().get('QuoteLineItem');
            Map<String,Schema.SObjectField> qliFieldsMap = qliType.getDescribe().fields.getMap();

            // build the query string
            String qliQuery = 'SELECT ';
            // add the qli fields
            for(String qliField : qliFieldsMap.KeySet()){
                qliQuery = qliQuery + qliField + ', ';
            }
            // add other needed fields
            qliQuery = qliQuery + 'Quote.OpportunityId';
            // add the from and where clause
            qliQuery = qliQuery + ' FROM QuoteLineItem WHERE QuoteId in :soldQuoteList AND isDeleted = false';
            // run the query
            List<QuoteLineItem> qliList= Database.query(qliQuery);

            primaryContacts = [select Id, ContactId, OpportunityId, Contact.FirstName, Contact.LastName, isPrimary
                from OpportunityContactRole where OpportunityId in :closedWonOpportunitiesIds and isPrimary = true];
            System.debug('About to create a new order');
            // create the orders from the quotes
            newOrders = createOrders(soldQuoteList, primaryContacts, 'Draft');
            System.debug('newOrders.size(): '+newOrders.size());
            if (newOrders.size() > 0) {
                System.debug('Total Number of SOQL Queries before newOrders insert: ' +  Limits.getQueries());
                insert newOrders;

                // create the order line items from the quote line items
                List<OrderItem> oiList = createOrderItems(qliList, newOrders, qliFieldsMap);
                if (oiList.size() > 0) {
                    System.debug('Total Number of SOQL Queries before oiList insert: ' +  Limits.getQueries());
                    insert oiList;
                    // now update the link order sub items to order items
                    // find orderitems that refer to parent order items
                    List<OrderItem> coiList = new List<OrderItem>();
                    for(OrderItem oi : oiList) {
                        if(oi.Parent_Unit_rSuite_Id__c != null && !oi.Parent_Unit_rSuite_Id__c.equals('')) {
                            coiList.add(oi);
                        }
                    }
                    if (coiList.size() > 0) {
                        // link the children to the parents
                        for(OrderItem coi : coiList) {
                            for(OrderItem oi : oiList) {
                                if(coi.Parent_Unit_rSuite_Id__c.equals(oi.rSuite_Id__c)) {
                                    coi.Parent_Order_Item__c = oi.Id;
                                    break;
                                }
                            }
                        }
                        update coiList;
                    }
                }

                // create the order discounts from the quote discounts
                if (qdList.size() > 0) {
                    List<Order_Discount__c> odList = createOrderDiscounts(qdList, newOrders);
                    if (odList.size() > 0) {
                        insert odList;
                    }
                }

                // create the order finanicing from the quote financing
                if (qfList.size() > 0) {
                    List<Order_Financing__c> ofList = createOrderFinancing(qfList, newOrders);
                    if (ofList.size() > 0) {
                        insert ofList;
                    }
                }
            }
            // move the attachments for all opportunities
        reparentAttachments(listNew);
        } // end processing for closed won opportunities


        
     }// END OF createSalesOrderOnOpportunityClosedWon METHOD

    /* @method: createOrders(List<Quote> quoteList)
     * @param: List<Quote> quoteList - list of Quote used as a source for the Orders
     * @param: List<OpportunityContactRole> primaryContacts - list of Primary Contacts used for populate order fields
     * @param: String orderStatus - the status to set on the order being created
     * @return: List<Order> a list of Order that are ready to be inserted
     */
    public List<Order> createOrders(List<Quote> quoteList, List<OpportunityContactRole> primaryContacts, String orderStatus){
        System.debug('quoteList: '+quoteList);
        set<Id> oppIDset = new set<Id>();
        for(Quote q: quoteList){
            oppIDset.add(q.OpportunityId);
        }
        List<WorkOrder> woList = [SELECT Id, Opportunity__c, Primary_Tech_Measure_FSL__c FROM WorkOrder WHERE Opportunity__c IN: oppIDset];

        List<Order> newOrders = new List<Order> ();
        Id coroRecordTypeId = UtilityMethods.RecordTypeFor('Order', 'CORO_Record_Type');
        Id ropRecordTypeId = UtilityMethods.RecordTypeFor('Order', 'Ordering_Portal'); //ROP_Opportunity__c
        Id currentUserId = UserInfo.getUserId();
        for (Quote qt : quoteList) {
            Order ord = new Order();
            ord.OpportunityId = qt.OpportunityId;
            ord.AccountId = qt.Opportunity.AccountId;
            ord.Store_Location__c = qt.Opportunity.Store_Location__c;
            ord.Store_Number__c = qt.Opportunity.Store_Number__c;
            ord.OwnerId = currentUserId;
            //ord.OwnerId = qt.Opportunity.OwnerId;
            ord.Description = qt.Opportunity.Description;
            ord.QuoteId = qt.Id;
            ord.Pricebook2Id = qt.Pricebook2Id;
            ord.BillingStreet = qt.Opportunity.Account.BillingStreet;
            ord.BillingCity = qt.Opportunity.Account.BillingCity;
            ord.BillingStateCode = qt.Opportunity.Account.BillingStateCode;
            ord.BillingPostalCode = qt.Opportunity.Account.BillingPostalCode;
            ord.BillingCountryCode = qt.Opportunity.Account.BillingCountryCode;
            ord.ShippingStreet = qt.Opportunity.Account.ShippingStreet;
            ord.ShippingCity = qt.Opportunity.Account.ShippingCity;
            ord.ShippingStateCode = qt.Opportunity.Account.ShippingStateCode;
            ord.ShippingPostalCode = qt.Opportunity.Account.ShippingPostalCode;
            ord.ShippingCountryCode = qt.Opportunity.Account.ShippingCountryCode;
            ord.Status = orderStatus;
            ord.EffectiveDate = qt.Opportunity.CloseDate;
            ord.rSuite_Id__c = qt.rSuite_Id__c;
            //adding support for Rop record type. - default to coro record type but switch as needed to other record types. 
            //Currently based on a formula field that looks at pricebooks.
            ord.RecordTypeId = coroRecordTypeId;             
            if(qt.Opportunity.ROP_Opportunity__c == true){
                ord.RecordTypeId = ropRecordTypeId;
            }
            // spin through the primary contacts and assign the name and primary contact
            for (OpportunityContactRole ocr : primaryContacts) {
                if (ocr.OpportunityId == qt.OpportunityId) {
                    // we have a match
                    ord.BillToContactId = ocr.ContactId;
                    ord.Name = ocr.Contact.LastName + ' - ' + ord.BillingStreet + ' - ' + ord.EffectiveDate.format();
                    break;
                }
            }
            // set the name if the primary contact is not found for some reason
            if (ord.Name == null) {
                ord.Name = 'No Primary Contact - ' + ord.BillingStreet + ' - ' + ord.EffectiveDate.format();
            }
            
            // add the order to the list
            newOrders.add(ord);

            if(woList.size() > 0){
                System.enqueueJob(new DynamicRollUpQue(woList));
            }
        }
        return newOrders;
    }
    /* @method: createOrderItems(List<QuoteLineItem> qliList, List<Order> theOrders)
     * @param: List<QuoteLineItem> qliList - list of QuoteLineItems used as a source for the OrderItems
     * @param: List<Order> theOrders - orders to link the Order Discounts to
     * @param: Map<String,Schema.SObjectField> qliFieldsMapRO
     * @return: List<OrderItem> a list of OrderItems that are ready to be inserted
     */
    public List<OrderItem> createOrderItems(List<QuoteLineItem> qliList, List<Order> theOrders, Map<String,Schema.SObjectField> qliFieldsMapRO) {
        // since the field map is read only we need to clone it so we can remove the standard fields that can't be copied by name
        Map<String,Schema.SObjectField> qliFieldsMap = qliFieldsMapRO.clone();
        // remove the standard and custom fields from the qli field mapping that can't be copied
        qliFieldsMap.remove('Id');
        qliFieldsMap.remove('isDeleted');
        qliFieldsMap.remove('ListPrice');
        qliFieldsMap.remove('CreateDate');
        qliFieldsMap.remove('Parent_Quote_Line_Item__c');

        // lets get the OrderLineItem fields for dynamic assignment
        SObjectType oiType = Schema.getGlobalDescribe().get('OrderItem');
        Map<String,Schema.SObjectField> oiFieldsMap = oiType.getDescribe().fields.getMap();

        List<OrderItem> oiList = new List<OrderItem> ();
        // generate the OrderItems from the QuoteLineItems
        for (QuoteLineItem qli : qliList) {
            OrderItem oi = new OrderItem();
            // populate fields not include in dynamic setup
            oi.QuoteLineItemId = qli.Id;
            oi.PricebookEntryId = qli.PricebookEntryId;

            //populate the dyanmic fields
            for(String qliField : qliFieldsMap.keySet()) {
                SObjectField oiField = oiFieldsMap.get(qliField);
                if (oiField != null) {
                    // we found the field can we update?
                    if (oiField.getDescribe().isUpdateable()) {
                        // yes check if the value is null
                        if (qli.get(qliField) != null) {
                            // its not null set the value
                            oi.put(oiField, qli.get(qliField));
                        }
                    }
                }
            }
            //assign the correct order to the order item
            for (Order ord : theOrders) {
                if (ord.QuoteId == qli.QuoteId) {
                    oi.OrderId = ord.Id;
                    break;
                }
            }
            // add the new order item to the list to be returned
            oiList.add(oi);
        }
        return oiList;
    }

    /* @method: createOrderDiscounts(List<Quote_Discount__c> qdList, List<Order> theOrders)
     * @param: List<Quote_Discount__c> qdList - list of Quote Discounts used as a source for the Order Discounts
     * @param: List<Order> theOrders - orders to link the Order Discounts to
     * @return: List<Order_Discount__c> a list of Order Discounts that are ready to be inserted
     */
    public List<Order_Discount__c> createOrderDiscounts(List<Quote_Discount__c> qdList, List<Order> theOrders) {
        List<Order_Discount__c> odList = new List<Order_Discount__c> ();
        for (Quote_Discount__c qd : qdList) {
            Order_Discount__c od = new Order_Discount__c();
            od.Store_Discount__c = qd.Store_Discount__c;
            od.Discount_Amount__c = qd.Discount_Amount__c;
            od.Per_Unit_Discount__c = qd.Per_Unit_Discount__c;
            od.rSuite_Id__c = qd.rSuite_Id__c;
            od.Status__c = qd.Status__c;
            //assign the correct order to the order discount
            for (Order ord : theOrders) {
                if (ord.QuoteId == qd.Quote__c) {
                    od.Order__c = ord.Id;
                    break;
                }
            }
            // add the new order discount to the list to be returned
            odList.add(od);
        }
        return odList;
    }

    /* @method: createOrderDiscounts(List<Quote_Financing__c> qfList, List<Order> theOrders)
     * @param: List<Quote_Financing__c> qdList - list of Quote Financing used as a source for the Order Financing
     * @param: List<Order> theOrders - orders to link the Order Financing to
     * @return: List<Order_Financing__c> a list of Order Financing that are ready to be inserted
     */
    public List<Order_Financing__c> createOrderFinancing(List<Quote_Financing__c> qfList, List<Order> theOrders) {
        List<Order_Financing__c> ofList = new List<Order_Financing__c> ();
        for (Quote_Financing__c qf : qfList) {
            Order_Financing__c ordf = new Order_Financing__c();
            ordf.Amount_Financed__c = qf.Amount_Financed__c;
            ordf.Store_Finance_Program__c = qf.Store_Finance_Program__c;
            ordf.Authorization_Code__c = qf.Authorization_Code__c;
            ordf.Payment_Display__c = qf.Payment_Display__c;
            ordf.Expiration_Date__c = qf.Expiration_Date__c;
            ordf.Program_Rate__c = qf.Program_Rate__c;
            ordf.Program_Fee__c = qf.Program_Fee__c;
            ordf.rSuite_Id__c = qf.rSuite_Id__c;
            ordf.Store_Location__c = qf.Related_Quote__r.Opportunity.Store_Location__c;
            //assign the correct order to the order discount
            for (Order ord : theOrders) {
                if (ord.QuoteId == qf.Related_Quote__c) {
                    ordf.Related_Order__c = ord.Id;
                    break;
                }
            }
            // add the new order financing to the list to be returned
            ofList.add(ordf);
        }
        return ofList;
    }

    /* @method: reparentAttachments(List<Order> orderList, List<Attachment> attachmentList)
     * @param: List<Attachment> attachmentList - list of Attachments for the closed Opportunities
     * @return: List<Attachment> a list of Attachments to insert
     */
    public void reparentAttachments(List<Opportunity> opps){
        //Get the attachments for the all opportunities
        Map<Id,Attachment> attachmentMap = new Map<Id,Attachment>([Select Id, Name, Body, ParentId,
                                        OwnerId, Description, IsPrivate
                                        from Attachment
                                        Where ParentId in: opps]);

        List<Attachment> attachmentsToInsert = new List<Attachment>();
        for(Id attachmentId: attachmentMap.keySet()){
            Attachment a = attachmentMap.get(attachmentId);
            if (RMS_AttachmentHandler.getrSuiteId(a.Name) != null) {
                //the name matches the format with an rSuite Id so it can be moved
                // make a copy of the attachment - can't actually do a move
                // need to do a copy - insert - delete
                attachmentsToInsert.add(new Attachment(Name = a.Name,
                    Body = a.Body,
                    OwnerId = a.OwnerId,
                    ParentId = a.ParentId,
                    IsPrivate = a.IsPrivate,
                    Description = a.Description ));
            } else {
                // remove it from the Map so we don't delete it
                attachmentMap.remove(attachmentId);
            }
        }

        // do the insert and delete - attachments will move via the Attachments trigger
        if(attachmentsToInsert.size() > 0){
            insert attachmentsToInsert;
            delete attachmentMap.values();
        }
    }
}