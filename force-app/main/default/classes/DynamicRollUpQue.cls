/**
 * @author Micah Johnson - Demand Chain 2020 
 * @description This is a Queueable that delays the processing of certain rollup fields
 * @param The parameter passed in is a list of sObjects (Most likely will be just 1) to update
 * @returns The Que will not return any data, but rather update the records at the end of the Queueable process
 **/

public class DynamicRollUpQue implements Queueable{
    public List<sObject> objcts;

    public DynamicRollUpQue(List<sObject> objList) {
        this.objcts = objList;
    }

    public void execute(QueueableContext context) {
            DynamicRollUpUtility.DynamicRollUp(objcts, true);
    }
}