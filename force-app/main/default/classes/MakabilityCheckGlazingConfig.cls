/**
 * @File Name          : MakabilityCheckGlazingConfig.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 02-24-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/1/2019, 1:35:02 PM   mark.rothermal@andersencorp.com     Initial Version
**/
global without sharing class MakabilityCheckGlazingConfig implements MakabilityService {
        
    /**
    * @description run makability on orderItem list and return results
    * @author mark.rothermal@andersencorp.com | 5/1/2019
    * @param map<string, MakabilityRestResource.OrderItem> requests
    * @param set<id> productIds
    * @return List<MakabilityRestResource.MakabilityResult>
    */
    public static List<MakabilityRestResource.MakabilityResult>  checkCompatibility( map <string,MakabilityRestResource.OrderItem> requests, set<id> productIds){
        List<MakabilityRestResource.MakabilityResult> results = new List<MakabilityRestResource.MakabilityResult>();
        Map<id,Glazing_Configuration__c> glazingConfigMap = getConfigOptions(productIds);
        Map<string,Glazing_Configuration__c> uniKeyToConfigMap = new Map<string,Glazing_Configuration__c>();
        Map<string,Map<Id,Glazing_Configuration__c>> uniKeyToConfigListMap = new Map<string,Map<Id,Glazing_Configuration__c>>();
        Map<id,Decimal> prodIdToTempReqUi = new map<id,decimal>();
        
        // handle no config record found.
        if(glazingConfigMap.keyset().size() == 0){
            for(String r:requests.keyset()){
                MakabilityRestResource.OrderItem oi = requests.get(r);
                MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult(); 
                List<string> errMessages = new List<string>();
                string errMessage = 'Salesforce - No Glazing Configurations found for this product Id.';  
                errMessages.add(errMessage);
                result.errorMessages = errMessages;
                result.orderId = oi.orderId;
                result.orderItemId = oi.orderItemId;
                results.add(result);                
            }            
            return results;
        }
        // build pattern to Glazing configuration map and productid to tempered glass required map

        for(id gId:glazingConfigMap.keyset()){
            Glazing_Configuration__c gc  = glazingConfigMap.get(gId);
            string uniKey = gc.Product_Configuration__r.Product__c +'-'+ gc.Frame_Type__c +'-'+gc.Glass_Pattern__c +'-'+ gc.Glazing__c +'-'+ gc.Sash_Ratio__c +'-'+ gc.Sash_Operation__c +'-'+ gc.Tempered__c;
            if(!uniKeyToConfigListMap.containsKey(uniKey)){
                Map<Id,Glazing_Configuration__c> placeholder = new Map<Id,Glazing_Configuration__c>();
                placeholder.put(gc.Id,gc);
                uniKeyToConfigListMap.put(uniKey,placeholder);
            } else{
                Map<Id,Glazing_Configuration__c> placeholder = uniKeyToConfigListMap.get(uniKey);
                placeholder.put(gc.Id,gc);
            }
            if(gc.Product_Configuration__r.Tempered_Glass_Required_at_UI__c != null){
                prodIdToTempReqUi.put(gc.Product_Configuration__r.Product__c, gc.Product_Configuration__r.Tempered_Glass_Required_at_UI__c);                
            }
        }
        System.debug('IN GLAZING MAKABILITY, sorted config map.'+ JSON.serializePretty(uniKeyToConfigListMap));
        
        ////////////////////////////// 
        // run makability check  
        // //////////////////////////        
        for(String r:requests.keyset()){
            MakabilityRestResource.OrderItem req = requests.get(r);                  
            List<string> errMessages = new List<string>();        
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
            result.orderItemId = req.orderItemId;
            result.orderId = req.orderId;
            Boolean doesntNeedTempering = true;
            Boolean patternPasses = true;
            Boolean checkThirdSash = req.makabilityCalculator.checkS3;
            String requestKeyS1 = req.productConfiguration.productId +'-'+ req.productConfiguration.frame +'-'+ req.productConfiguration.s1Pattern  +'-'+ req.productConfiguration.s1Type  +'-'+ req.productConfiguration.sashRatio  +'-'+ req.productConfiguration.sashOperation  +'-'+ req.productConfiguration.s1Tempering;
            String requestKeyS2 = req.productConfiguration.productId +'-'+ req.productConfiguration.frame +'-'+ req.productConfiguration.s2Pattern  +'-'+ req.productConfiguration.s2Type  +'-'+ req.productConfiguration.sashRatio  +'-'+ req.productConfiguration.sashOperation  +'-'+ req.productConfiguration.s2Tempering;
            String requestKeyS3 = req.productConfiguration.productId +'-'+ req.productConfiguration.frame +'-'+ req.productConfiguration.s3Pattern  +'-'+ req.productConfiguration.s3Type  +'-'+ req.productConfiguration.sashRatio  +'-'+ req.productConfiguration.sashOperation  +'-'+ req.productConfiguration.s3Tempering;
            // is there a config to check. 
            // ****************** need to handle some sort of glazingcheck below *******************
            if(req.productConfiguration != null){
                // if not tempered check if requires tempering
                if(req.productConfiguration.s1Tempering == false || req.productConfiguration.s2Tempering == false){
                    Decimal temperedUiConfig = prodIdToTempReqUi.get(req.productConfiguration.productId);
                    Decimal reqUnitedInches = MakabilityUtility.calculateUnitedInches (req.productConfiguration.heightInches,req.productConfiguration.heightFractions, 
                                                                                       req.productConfiguration.widthInches, req.productConfiguration.widthFractions);                  
                    //check Ui against config UI; 
                    if(reqUnitedInches > temperedUiConfig ){
                        doesntNeedTempering = false;
                        errMessages.add ('Glazing Config - Requires Tempered Glass due to size. Tempered glass required over ' + temperedUiConfig + ' united inches.' );                         
                        result.isMakable = false;                        
                    }
                }
                if(checkThirdSash && req.productConfiguration.s3Tempering == false ){
                    Decimal temperedUiConfig = prodIdToTempReqUi.get(req.productConfiguration.productId);
                    Decimal reqUnitedInches = MakabilityUtility.calculateUnitedInches (req.productConfiguration.heightInches,req.productConfiguration.heightFractions, 
                                                                                       req.productConfiguration.widthInches, req.productConfiguration.widthFractions);                  
                    //check Ui against config UI; 
                    if(reqUnitedInches > temperedUiConfig ){
                        doesntNeedTempering = false;
                        errMessages.add ('Glazing Config - S3 Requires Tempered Glass due to size. Tempered glass required over ' + temperedUiConfig + ' united inches.' );                         
                        result.isMakable = false;                        
                    }
                }

                // check pattern
                if(req.productConfiguration.s1Pattern != 'No Pattern' || req.productConfiguration.s1Type == 'High Performance Enhanced Triple Glass' || req.productConfiguration.s1Type == 'High Performance SmartSun Enhanced Triple Glass'){
                    if(uniKeyToConfigListMap.containsKey(requestKeyS1) ){
                        Map<Id,Glazing_Configuration__c>  currentConfigMap = uniKeyToConfigListMap.get(requestKeyS1) ;                      
                        Boolean onePassAllPass = false;
                        for(id key: currentConfigMap.keySet()){
                            Glazing_Configuration__c currentConfig = currentConfigMap.get(key);
                            Boolean widthPassing = false;
                            Boolean heightPassing = false;
                            // check width    
                            if(compareHeightOrWidth(req.productConfiguration.widthInches,req.productConfiguration.widthFractions, currentConfig.Max_Width_Inches__c, currentConfig.Max_Width_Fraction__c )){
                               widthPassing = true;                   
                            } 
                            // check height
                            if(compareHeightOrWidth(req.productConfiguration.heightInches,req.productConfiguration.heightFractions, currentConfig.Max_Height_Inches__c, currentConfig.Max_Height_Fraction__c )){
                                heightPassing = true;
                            }
                            if(widthPassing && heightPassing){
                                onePassAllPass = true;
                            }
                        }
                        if(!onePassAllPass){
                            patternPasses = false;
                            errMessages.add ('Glazing Config - s1Pattern Height or Width Greater than Allowed');   
                            result.isMakable = false;    
                        } 
                    } else{
                        // pattern doesnt match
                        if(req.productConfiguration.s1Pattern != 'No Pattern' && req.productConfiguration.s1Pattern != null || req.productConfiguration.s1Type == 'High Performance Enhanced Triple Glass' || req.productConfiguration.s1Type == 'High Performance SmartSun Enhanced Triple Glass'){
                            system.debug('s1 pattern no match');
                            patternPasses = false;
                        //    List<String> availablePatterns = MakabilityUtility.splitUniqueKey(req.productConfiguration.productId, uniKeyToConfigMap.keySet());
                            errMessages.add('Glazing Config - s1Pattern unable to match configuration record.');   
                            result.isMakable = false;                               
                        }
                        
                    }
                }
                if(req.productConfiguration.s2Pattern != 'No Pattern' || req.productConfiguration.s2Type == 'High Performance Enhanced Triple Glass' || req.productConfiguration.s2Type == 'High Performance SmartSun Enhanced Triple Glass'){
                    if(uniKeyToConfigListMap.containsKey(requestKeyS2) ){
                        Map<Id,Glazing_Configuration__c>  currentConfigMap = uniKeyToConfigListMap.get(requestKeyS2) ;                 
                        Boolean onePassAllPass = false;                        
                        for(id key: currentConfigMap.keySet()){
                            Glazing_Configuration__c currentConfig = currentConfigMap.get(key);
                            Boolean widthPassing = false;
                            Boolean heightPassing = false;
                            // check width    
                            if(compareHeightOrWidth(req.productConfiguration.widthInches,req.productConfiguration.widthFractions, currentConfig.Max_Width_Inches__c, currentConfig.Max_Width_Fraction__c )){
                               widthPassing = true;                   
                            } 
                            // check height
                            if(compareHeightOrWidth(req.productConfiguration.heightInches,req.productConfiguration.heightFractions, currentConfig.Max_Height_Inches__c, currentConfig.Max_Height_Fraction__c )){
                                heightPassing = true;
                            }
                            if(widthPassing && heightPassing){
                                onePassAllPass = true;
                            }
                        }
                        if(!onePassAllPass){
                            patternPasses = false;
                            errMessages.add('Glazing Config - s2Pattern Height or Width Greater than Allowed');   
                            result.isMakable = false;    
                        } 
                    } else{
                        // pattern doesnt match
                        if(req.productConfiguration.s2Pattern != 'No Pattern' && req.productConfiguration.s2Pattern != null || req.productConfiguration.s2Type == 'High Performance Enhanced Triple Glass' || req.productConfiguration.s2Type == 'High Performance SmartSun Enhanced Triple Glass'){
                            system.debug('s2 pattern no match');
                            patternPasses = false;
                        //    List<String> availablePatterns = MakabilityUtility.splitUniqueKey(req.productConfiguration.productId, uniKeyToConfigMap.keySet());
                            errMessages.add ('Glazing Config - s2Pattern unable to match configuration record.');   
                            result.isMakable = false;                               
                        }    
                    }
                }
                if(req.productConfiguration.s3Pattern != 'No Pattern' || req.productConfiguration.s3Type == 'High Performance Enhanced Triple Glass' || req.productConfiguration.s3Type == 'High Performance SmartSun Enhanced Triple Glass'){
                    if(uniKeyToConfigListMap.containsKey(requestKeyS3) ){
                        Map<Id,Glazing_Configuration__c>  currentConfigMap = uniKeyToConfigListMap.get(requestKeyS3) ;                                         
                        Boolean onePassAllPass = false;
                        for(id key: currentConfigMap.keySet()){
                            Glazing_Configuration__c currentConfig = currentConfigMap.get(key);
                            Boolean widthPassing = false;
                            Boolean heightPassing = false;
                            // check width    
                            if(compareHeightOrWidth(req.productConfiguration.widthInches,req.productConfiguration.widthFractions, currentConfig.Max_Width_Inches__c, currentConfig.Max_Width_Fraction__c )){
                               widthPassing = true;                   
                            } 
                            // check height
                            if(compareHeightOrWidth(req.productConfiguration.heightInches,req.productConfiguration.heightFractions, currentConfig.Max_Height_Inches__c, currentConfig.Max_Height_Fraction__c )){
                                heightPassing = true;
                            }
                            if(widthPassing && heightPassing){
                                onePassAllPass = true;
                            }
                        }
                        if(!onePassAllPass){
                            patternPasses = false;
                            errMessages.add ('Glazing Config - s3Pattern Height or Width Greater than Allowed');   
                            result.isMakable = false;    
                        } 
                    } else{
                        // pattern doesnt match
                        if(req.productConfiguration.s3Pattern != 'No Pattern' && req.productConfiguration.s3Pattern != null || req.productConfiguration.s3Type == 'High Performance Enhanced Triple Glass' || req.productConfiguration.s3Type == 'High Performance SmartSun Enhanced Triple Glass'){
                            system.debug('s3 pattern no match');
                            patternPasses = false;
                        //    List<String> availablePatterns = MakabilityUtility.splitUniqueKey(req.productConfiguration.productId, uniKeyToConfigMap.keySet());
                            errMessages.add ('Glazing Config - s3Pattern unable to match configuration record.');   
                            result.isMakable = false;                               
                        }
                    }
                }
                if(doesntneedTempering && patternPasses){
                    errMessages.add('Glazing Config  - passed');  
                    result.isMakable = true;
                    //                    result.errorMessages = errMessages;                
                    //                    results.add(result);                            
                }                 
            } else {
                errMessages.add ('Glazing Config - No configuration record found');   
                result.isMakable = true;                         
            }
            result.errorMessages = errMessages;                               
            results.add(result);    
        }
        system.debug('results check ' + results );        
        if(results.size() == 0){
            List<string> errMessages = new List<string>();                    
            errMessages.add('Something bad happened with the Glazing config');
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
            result.errorMessages = errMessages;
            result.isMakable = false;
            results.add(result);
        }         
        return results;
    }
    
    /**
    * @description get config options
    * @author mark.rothermal@andersencorp.com | 5/1/2019
    * @param set<id> productIds
    * @return Map<id, Glazing_Configuration__c>
    */
    public static Map<id,Glazing_Configuration__c> getConfigOptions(set<id> productIds){
        Map<id,Glazing_Configuration__c> glazingConfigMap= new Map<id,Glazing_Configuration__c>( [
            SELECT id,
            Max_Height_Fraction__c,
            Max_Height_Inches__c,
            Max_Width_Fraction__c,
            Max_Width_Inches__c,
            Frame_Type__c,
            Glazing__c,
            Glass_Pattern__c,
            Sash_Ratio__c,
            Sash_Operation__c,
            Tempered__c,
            Product_Configuration__r.Tempered_Glass_Required_at_UI__c,              
            Product_Configuration__r.Product__c
            from Glazing_Configuration__c 
            where Product_Configuration__r.Product__c in :productids] );  
        return glazingConfigMap;
    }
    
    /**
    * @description compare heights/widths for makability testing.
    * @author mark.rothermal@andersencorp.com | 5/1/2019
    * @param decimal requestInches
    * @param string requestFractions
    * @param decimal configInches
    * @param string configFractions
    * @return boolean
    */
    public static boolean compareHeightOrWidth(decimal requestInches,string requestFractions, decimal configInches, string configFractions  ){
        boolean makable = false;
    //    Decimal convertedRequestInches = decimal.valueOf(requestInches);
        Double requestConverted = requestInches + Constants.fractionConversionMap.get(requestFractions);
        Double configConverted = configInches + Constants.fractionConversionMap.get(configFractions); 
        if(requestConverted <= configConverted){
            makable = true;
        }
        return makable;        
    }

    
}