/**
 *@author Connor Davis
 *@description This class tests the functions of StoreGoalsController.cls
 *Unit Tests: StoreGoalsController.cls
 */

@isTest
private class StoreGoalsControllerTest {

    public StoreGoalsControllerTest() {


    }

    // Create the test data needed to test the methods of the StoreGoalsController class
    @TestSetup
    static void setupData() {

        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        // Create 3 test stores
        Account store1 = new Account(
            Type = 'South', 
            Name = 'Test Store 1'
        );

        insert store1;

        // String type = [SELECT Type FROM Account WHERE Name = 'Test Store 1'].Type;

        Account store2 = new Account(
            Type = 'CORO',
            Name = 'Test Store 2'
        );

        insert store2;

        Account store3 = new Account(
            Type = 'CORO',
            Name = 'Test Store 3'
        );

        insert store3;

        // Create store configurations for test stores 1 and 2
        Store_Configuration__c storeConfig1 = new Store_Configuration__c( Store__c = store1.Id, Order_Number__c = 1 );

        insert storeConfig1;

        Store_Configuration__c storeConfig2 = new Store_Configuration__c( Store__c = store2.Id, Order_Number__c = 1 );

        insert storeConfig2;

        // Create a user that we will use to test some methods that are user specific
        User test = new User(
            FirstName = 'TestStoreGoals',
            LastName = 'User',
            Email = 'test.user@email.com',
            UserName = 'test.user@email.com' + (Math.random() * 1000000).intValue(),
            Alias = 'testu',
            TimeZoneSidKey = 'America/Chicago',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LanguageLocaleKey = 'en_US',
            Default_Store_Location__c = 'Test Store 2',
            Store_Locations__c = 'Test Store 1;Test Store 2'
        );

        insert test;

        // Generate some goals for test store 2 in Current Year
        StoreGoalsController.generateGoals(System.today().year()-1, 'Test Store 2', 0);

    }

    // Test that we can accurately get the type of a store
    @isTest
    static void testGetStoreType() {

        String type = StoreGoalsController.getStoreType('Test Store 1');
        String type2 = StoreGoalsController.getStoreType('Test Store 2');


        System.assertNotEquals('CORO', type);

        System.assertEquals('CORO', type2);
    }

    // Test that we can accurately get the default store location of a user
    @isTest
    static void testGetUserDefaultLocation() {

        User testUser = [SELECT Id FROM User WHERE FirstName = 'TestStoreGoals'];

        System.runAs(testUser) {

            String loc = StoreGoalsController.getUserDefaultLocation();

            System.assertEquals('Test Store 2', loc);
        }
    }

    // Test that we can get the correct list of locations for a user
    @isTest
    static void testGetUserLocations() {

        User testUser = [SELECT Id FROM User WHERE FirstName = 'TestStoreGoals'];
        System.runAs(testUser) {
            String loc = StoreGoalsController.getUserLocations();

            System.assertEquals('Test Store 1;Test Store 2', loc);
        }
    }

    // Test that we can retrieve the goals of a given store
    @isTest
    static void testGetStoreGoalsWire() {

        List<Store_Goal__c> goals = StoreGoalsController.getStoreGoalsWire(String.valueOf(System.today().year()-1), 'Test Store 2');

        System.assertEquals(14, goals.size());
    }

    // Test that we can retrieve the goals of a given store, and if they don't exist, make them
    @isTest
    static void testGetStoreGoalsImperative() {


        // Testing that the function retrieves goals that exist
        List<Store_Goal__c> goals = StoreGoalsController.getStoreGoalsImperative(System.today().year()-1, 'Test Store 2');

        System.assertEquals(14, goals.size());

        // Testing that the function properly adds up the columns and stores it in the totals goal
        Decimal totals = 0;

        for (Integer i = 0; i < goals.size()-1; i++) {
            totals += goals[i].Sold_Units__c;
        }

        System.assertEquals(totals, goals[goals.size()-1].Sold_Units__c);

        // Checks that the function returns a list of goals even if none existed when the function was called, which also tests generateGoals
        delete goals;

        goals = StoreGoalsController.getStoreGoalsImperative(System.today().year()-1, 'Test Store 2');

        System.assertEquals(14, goals.size());
    }

    // This method doesn't really need to be tested since it is used in other methods that are being tested
    @isTest
    static void testGetStoreConfig() {

        Id storeConfig = StoreGoalsController.getStoreConfig('Test Store 2');
        System.debug('Config: ' + storeConfig);
    }

    // Test that we get an error if we try to generate goals for an invalid store
    @isTest
    static void testGenerateGoals() {
        try {
            List<Store_Goal__c> goals = StoreGoalsController.generateGoals(System.today().year()-1, '0060 - Rochester, MN', 0);
        } catch (Exception e) {
            System.assertEquals('Script-thrown exception', e.getMessage());
        }

    }

    // Test that we can check whether a store location has a store configuration.
    @isTest
    static void testCheckStoreConfig() {

        Boolean check1 = StoreGoalsController.checkStoreConfig('Test Store 2');
        Boolean check2 = StoreGoalsController.checkStoreConfig('Test Store 3');

        System.assertEquals(true, check1);
        System.assertEquals(false, check2);
    }
}