global class CanvassUnitBatch implements Database.Batchable<string>, Database.AllowsCallouts 
{
    //database.executebatch(new CanvassUnitBatch("List Of Zips Goes Here","Market Id Goes Here"),1);
    
    private set<string> zipCodeList;
    private string marketId;
    private string canvassMarketBatchJobId;
    private integer count;
    private string queueId;
    private boolean moveOntoNextZip;
    
    global CanvassUnitBatch(set<string> oldZipCodeList, string oldMarketId, string canvassMarketBatchJob)
    {
        zipCodeList = oldZipCodeList;
        marketId = oldMarketId;
        canvassMarketBatchJobId = canvassMarketBatchJob;
        count = 0;
        queueId = null;
        moveOntoNextZip = true;
    }
    
    global CanvassUnitBatch(set<string> oldZipCodeList, string oldMarketId, string canvassMarketBatchJob, integer countValue, string queueIdValue)
    {
        zipCodeList = oldZipCodeList;
        marketId = oldMarketId;
        canvassMarketBatchJobId = canvassMarketBatchJob;
        count = countValue;
        queueId = queueIdValue;
        moveOntoNextZip = true;
    }
    
    global list<string> start (Database.BatchableContext BC)
    {
        list<string> marketList = new list<string>();
        marketList.add(marketId);
        return marketList;
    }
    
    global void execute (Database.BatchableContext BC, list<string> scope)
    {
        
    }
    
    global void finish(Database.BatchableContext BC)
    {
        try
        {
            /*system.debug('zipCodeList.size()');
            system.debug(zipCodeList.size());
            
            list<string> testStringCodeList = new list<string>();
            testStringCodeList.addAll(zipCodeList);
            
            for(integer i = 0; i < testStringCodeList.size(); i++)
            {
                system.debug(testStringCodeList[i]);
            }*/
            
            if(zipCodeList.size() > 0)
            {
                // variable to hold field ids
                //list<object> fieldIds = parseMetadataIntoIds(HttpRequestMethod());
                list<object> fieldIds = new list<object>();
                fieldIds.add('areabuilding');
                fieldIds.add('areagross');
                fieldIds.add('assessorlastsaleamount');
                fieldIds.add('assessorlastsaledate');
                fieldIds.add('attom_id');
                fieldIds.add('bathcount');
                fieldIds.add('bathpartialcount');
                fieldIds.add('bedroomscount');
                fieldIds.add('censusblockgroup');
                fieldIds.add('censustract');
                fieldIds.add('contactownermailaddresscrrt');
                fieldIds.add('contactownermailaddressfull');
                fieldIds.add('contactownermailaddresshousenumber');
                fieldIds.add('contactownermailaddressunit');
                fieldIds.add('currentfirstpositionopenloaninterestrate');
                fieldIds.add('currentfirstpositionopenloaninterestratetype');
                fieldIds.add('currentfirstpositionopenloantype');
                fieldIds.add('currentopenloanamount');
                fieldIds.add('currentsecondpositionopenloanamount');
                fieldIds.add('currentthirdpositionopenloanamount');
                fieldIds.add('deedowner1namefull');
                fieldIds.add('exterior1code');
                fieldIds.add('flooringmaterialprimary');
                fieldIds.add('foundation');
                fieldIds.add('hvaccoolingdetail');
                fieldIds.add('hvacheatingdetail');
                fieldIds.add('lastownershiptransferdate');
                fieldIds.add('legaldescription');
                fieldIds.add('legalsubdivision');
                fieldIds.add('legaltownship');
                //fieldIds.add('Mortgage1TermDate');
                fieldIds.add('parcelaccountnumber');
                fieldIds.add('parcelnumberraw');
                fieldIds.add('partyowner1namefirst');
                fieldIds.add('partyowner1namefull');
                fieldIds.add('partyowner1namelast');
                fieldIds.add('partyowner2namefirst');
                fieldIds.add('partyowner2namefull');
                //fieldIds.add('propertyaddresscounty');
                fieldIds.add('partyowner1namefull');
                fieldIds.add('propertyaddresscity');
                fieldIds.add('propertyaddressfull');
                fieldIds.add('propertyaddresshousenumber');
                fieldIds.add('propertyaddressstate');
                fieldIds.add('propertyaddressstreetdirection');
                //fieldIds.add('propertyaddressstreetname');
                fieldIds.add('propertyaddressstreetsuffix');
                fieldIds.add('propertyaddressunitprefix');
                fieldIds.add('propertyaddresszip');
                fieldIds.add('propertyaddresszip4');
                fieldIds.add('propertylatitude');
                fieldIds.add('propertylongitude');
                fieldIds.add('propertyusegroup');
                fieldIds.add('propertyusestandardized');
                fieldIds.add('roofmaterial');
                fieldIds.add('roomscellarflag');
                fieldIds.add('situsstatecountyfips');
                fieldIds.add('statusowneroccupiedflag');
                fieldIds.add('roomscount');
                fieldIds.add('situscounty');
                fieldIds.add('storiescount');
                fieldIds.add('structurestyle');
                fieldIds.add('taxassessedvalueimprovements');
                fieldIds.add('taxassessedvalueland');
                fieldIds.add('taxassessedvaluetotal');
                fieldIds.add('taxassessedvaluetotal');
                fieldIds.add('taxfiscalyear');
                fieldIds.add('taxmarketvalueyear');
                fieldIds.add('taxyearassessed');
                fieldIds.add('yearbuilt');
                
                //assign string
                string zipCode = (new list<string>(zipCodeList))[0];
                
                // build request
                request newRequest = buildRequest(zipCode,fieldIds,count);
                
                // parse markers
                map<string,object> responseData = getMarkers(newRequest);
                system.debug('responseData**');
                system.debug(responseData);
                    
                map<string,object> dataMap = (map<string,object>)responseData.get('data');
                
                if(dataMap.get('ids') != null)
                {
                    list<object> markerIds = (list<object>)dataMap.get('ids');
                    
                    system.debug('markerIds**');
                    system.debug(markerIds.size());
                        
                    //system.debug('markerIds');    
                    //system.debug(markerIds[0]);
                    
                    //for(object market :markerIds)
                    //{
                    //    system.debug(market);
                    //}
                    
                    if(markerIds.size() > 0)
                    {
                        list<object> markerIdsTest = new list<object>();
                    
                        for(integer i = 0; i < markerIds.size(); i++)
                        {
                            markerIdsTest.add(markerIds[i]);
                        }
                        
                        string fieldIdString = '';
                        for(object element :fieldIds)
                        {
                            if(fieldIdString != '')
                            {
                                fieldIdString += ', ';
                            }
                            fieldIdString += string.valueOf(element);
                        }
                        
                        if(count == 0)
                        {
                            string markerIdsString = '';
                            for(object element :markerIdsTest)
                            {
                                if(markerIdsString != '')
                                {
                                    markerIdsString += ', ';
                                }
                                markerIdsString += string.valueOf(element);
                            }
                            
                            if(fieldIdString != '' && markerIdsString != '' && zipCode != null && marketId != null)
                            {
                                Custom_Data_Layer_Batch_Queue__c queueRecord = new Custom_Data_Layer_Batch_Queue__c();
                                queueRecord.Field_Ids__c = fieldIdString;
                                queueRecord.Marker_Ids__c = markerIdsString;
                                queueRecord.ZipCode__c = zipCode;
                                queueRecord.Market_Id__c = marketId;
                                
                                if(markerIds.size() != 8000)
                                {
                                    queueRecord.Status__c = 'Queued';
                                }
        
                                queueRecord.Canvass_Market_Batch_Job__c = canvassMarketBatchJobId;
                                
                                insert queueRecord;
                                queueId = queueRecord.Id;
                            }
                            else
                            {
                                Custom_Data_Layer_Batch_Queue__c queueRecord = new Custom_Data_Layer_Batch_Queue__c();
                                queueRecord.Field_Ids__c = fieldIdString;
                                queueRecord.Marker_Ids__c = markerIdsString;
                                queueRecord.ZipCode__c = zipCode;
                                queueRecord.Market_Id__c = marketId;
                                queueRecord.Status__c = 'NA';
                                queueRecord.Invalid_Job__c = true;
        
                                queueRecord.Canvass_Market_Batch_Job__c = canvassMarketBatchJobId;
                                
                                insert queueRecord;
                                queueId = queueRecord.Id;
                            }
                        }
                        else if(count > 0 && count < 6)
                        {
                            string markerIdsString = '';
                            for(object element :markerIdsTest)
                            {
                                if(markerIdsString != '')
                                {
                                    markerIdsString += ', ';
                                }
                                markerIdsString += string.valueOf(element);
                            }
                            
                            if(markerIdsString != '')
                            {
                                list<Custom_Data_Layer_Batch_Queue__c> existingQueueRecord = [SELECT Id, Marker_Ids3__c, Marker_Ids4__c, Status__c
                                                                                              FROM Custom_Data_Layer_Batch_Queue__c 
                                                                                              WHERE Id = :queueId];
                                for(Custom_Data_Layer_Batch_Queue__c record :existingQueueRecord)
                                {
                                    if(count == 1)
                                    {
                                        record.Marker_Ids2__c = markerIdsString;
                                    }
                                    else if(count == 2)
                                    {
                                        record.Marker_Ids3__c = markerIdsString;
                                    }
                                    else if(count == 3)
                                    {
                                        record.Marker_Ids4__c = markerIdsString;
                                    }
                                    else if(count == 4)
                                    {
                                        record.Marker_Ids5__c = markerIdsString;
                                    }
                                    else if(count == 5)
                                    {
                                        record.Marker_Ids6__c = markerIdsString;
                                    }
                    
                                    if(markerIds.size() != 8000)
                                    {
                                        record.Status__c = 'Queued';
                                    }
                                }
                                
                                update existingQueueRecord;
                            }
                        }
                        else if(count > 5)
                        {
                            list<string> emailAddresses = new list<string>();
                            emailAddresses.add('jhoneycutt@mapanything.com');
                            
                            if(emailAddresses.size() > 0)
                            {
                                string resBody = 'RBA Canvass Unit Zip Code Marker Error: count exceeded field availability, add more market id fields to the queue object. Count = '+count;
                                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                                message.ToAddresses = EmailAddresses;
                                message.Subject = 'RBA Canvass Unit Zip Code Marker Error';
                                message.htmlBody = resBody;
                                    
                                Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
                                Messaging.SendEmailResult[] rslts = Messaging.sendEmail(messages);
                                    
                                if (rslts[0].success) {
                                    System.debug('The email was sent successfully.');
                                } else {
                                    System.debug('The email failed to send: ' + rslts[0].errors[0].message);
                                }
                            }
                        }
                        
                        if(markerIds.size() == 8000)
                        {
                            moveOntoNextZip = false;
                            count++;
                            if (!Test.isRunningTest())
                            {
                                database.executebatch(new CanvassUnitBatch(zipCodeList, marketId, canvassMarketBatchJobId, count, queueId),1);
                            }
                        }
                    }
                    else
                    {
                        Custom_Data_Layer_Batch_Queue__c queueRecord = new Custom_Data_Layer_Batch_Queue__c();
                        queueRecord.ZipCode__c = zipCode;
                        queueRecord.Market_Id__c = marketId;
                        queueRecord.Status__c = 'NA';
                        queueRecord.Invalid_Job__c = true;

                        queueRecord.Canvass_Market_Batch_Job__c = canvassMarketBatchJobId;
                        
                        insert queueRecord;
                        queueId = queueRecord.Id;
                    }
                }
                /*else
                {
                    Custom_Data_Layer_Batch_Queue__c queueRecord = new Custom_Data_Layer_Batch_Queue__c();
                    queueRecord.ZipCode__c = zipCode;
                    queueRecord.Market_Id__c = marketId;
                    queueRecord.Status__c = 'NA';
                    queueRecord.Invalid_Job__c = true;

                    queueRecord.Canvass_Market_Batch_Job__c = canvassMarketBatchJobId;
                    
                    insert queueRecord;
                    queueId = queueRecord.Id;
                }*/
                
                if(moveOntoNextZip == true)
                {
                    set<string> newZipCodeSet = new set<string>();
                            
                    for(integer i = 1; i < zipCodeList.size(); i++)
                    {
                        newZipCodeSet.add((new list<string>(zipCodeList))[i]);
                    }

                    if(newZipCodeSet.size() > 0)
                    {
                        database.executebatch(new CanvassUnitBatch(newZipCodeSet,marketId, canvassMarketBatchJobId),1);
                    }
                }
            }
        }
        catch(Exception e)
        {
            if(moveOntoNextZip == true)
            {
                set<string> newZipCodeSet = new set<string>();
                        
                for(integer i = 1; i < zipCodeList.size(); i++)
                {
                    newZipCodeSet.add((new list<string>(zipCodeList))[i]);
                }
                
                if(newZipCodeSet.size() > 0)
                {
                    database.executebatch(new CanvassUnitBatch(newZipCodeSet,marketId, canvassMarketBatchJobId),1);
                }
            }
        }
    }
    
    // build JSON structure
    global request buildRequest(string zip, list<object> listOfFieldLists, integer count)
    {
        integer offset = count*8000;
        
        list<string> valueList = new list<string>();
        valueList.add(zip);
        
        filter newFilter = new filter();
        newFilter.operator = 'equals';
        newFilter.topic_id = 'propertyaddresszip';
        newFilter.values = valueList;
        
        list<filter> filterList = new list<filter>();
        filterList.add(newFilter);
        
        legend newLegend = new legend();
        newLegend.rows = new list<string>();
        newLegend.subTitle = '--';
        //newLegend.title = 'MapAnything Property + Loan Data';
        newLegend.title = 'Property Data (USA)';
        
        list<tabItem> tabItemList = new list<tabItem>();
        for(object fieldId :listOfFieldLists)
        {
            tabItem newTabItem = new tabItem();
            //newTabItem.file_id = 'attom_loanmodel';
            //newTabItem.file_id = 'attom_solar';
            newTabItem.file_id = 'usa_property_sfdc';
            newTabItem.topic_id = fieldId;
            tabItemList.add(newTabItem);
        }
        
        tab newTab = new tab();
        newTab.data = tabItemList;
        newTab.tab_id = '1559576830617';
        newTab.tab_label = 'Info';
        
        list<tab> newTabList = new list<tab>();
        newTabList.add(newTab);
        
        popup newPopup = new popup();
        newPopup.header = new list<string>();
        newPopup.tabs = newTabList;
        
        dataObj newData = new dataObj();
        newData.defaultMarkerColor = '93c47d:Circle';
        //newData.file_id = 'attom_loanmodel';
        //newData.file_id = 'attom_solar';
        newData.file_id = 'usa_property_sfdc';
        newData.filters = filterList;
        newData.legend = newLegend;
        newData.level_id = 'attom-1';
        newData.popup = newPopup;
        
        mapinfoObj newMapinfo = new mapinfoObj();
        /*newMapinfo.celat = 36.98500309285596;
        newMapinfo.celng = -97.8662109375;
        newMapinfo.limitToBeReplaced = 8000;
        newMapinfo.nelat = 47.24940695788845;
        newMapinfo.nelng = -61.3916015625;
        newMapinfo.offset = offset;
        newMapinfo.swlat = 25.12539261151203;
        newMapinfo.swlng = -134.3408203125;*/
        newMapinfo.celat = -87.99915054796863;
        newMapinfo.celng = -128.4078873680882;
        newMapinfo.limitToBeReplaced = 8000;
        newMapinfo.nelat = 89.96610869271207;
        newMapinfo.nelng = 180;
        newMapinfo.offset = offset;
        newMapinfo.swlat = -89.99998966524514;
        newMapinfo.swlng = -180;
        
        request newRequest = new request();
        newRequest.aggregates = false;
        newRequest.data = newData;
        newRequest.details = true;
        newRequest.ids = new list<integer>();
        newRequest.mapinfo = newMapinfo;
            
        return newRequest;
    }
    
    // get marker ids 
    global map<string,object> getMarkers(request newRequest)
    {
        string newRequestStr = JSON.serialize(newRequest);

        newRequestStr = newRequestStr.replace('limitToBeReplaced', 'limit');

        Map<String, String> newoptionsStr = new Map<String, String>();
        newoptionsStr.put('method','post');
        newoptionsStr.put('action','markers');
        newoptionsStr.put('subType','data');
        newoptionsStr.put('version','2');
        
        map<string,object> responseData = new map<string,object>();
        
        if (!Test.isRunningTest())
        {
            //responseData = sma.MARemoteFunctions.MapAnythingIORequestPOST(newoptionsStr, newRequestStr);
            responseData = maps.api.getHostedData(newRequestStr);
        }
        else
        {
            HttpRequest req = new HttpRequest();
            req.setEndpoint('blank');
            req.setMethod('GET');
            Http h = new Http();
            HttpResponse res = h.send(req);
            String jsonData = res.getBody();
                    
            responseData = (map<string, object>)JSON.deserializeUntyped(jsonData);
        }
        
        return responseData;
    }
    
    // request to get metadata
    /*global Map<String, object> HttpRequestMethod()
    {
        Map<String, object> returnData = new Map<String, object>();
        
        HttpRequest req = new HttpRequest();
        req.setTimeout(60000);
        req.setMethod('GET');
        req.setEndpoint('https://api.mapanything.io/data/sources/1?source=attom_loanmodel&fields=true&orgId=00D80000000ctx5EAA');
         
        Http routeHTTP = new Http();
        HttpResponse res = routeHTTP.send(req);
        returnData = (Map<String,object>) JSON.deserializeuntyped(res.getBody());
        
        return returnData;
    }*/
    
    // request to parse metadata and build list of field ids
    /*global list<object> parseMetadataIntoIds(Map<String, object> returnData)
    {
        list<object> fieldIds = new list<object>();

        list<object> sourcesList = (list<object>)returnData.get('sources');
        for(object value :sourcesList)
        {
            map<string, object> valueMap = (map<string, object>)value;
            
            list<object> fieldList = (list<object>)valueMap.get('fields');
            system.debug('labels');
            for(object field :fieldList)
            {
                map<string, object> fieldMap = (map<string, object>)field;
                fieldIds.add(fieldMap.get('id'));
                system.debug(fieldMap.get('label'));
            }
        }
        
        return fieldIds;
    }*/
    
    global class dataObj
    {
        global string file_id;
        global List<filter> filters;
        global legend legend;
        global string level_id;
        global popup popup;
        global string defaultMarkerColor;
    }
    
    global class filter
    {
        global string topic_id;
        global string operator;
        global List<string> values;
    }
    
    global class legend
    {
        global string title;
        global string subTitle;
        global list<string> rows; 
    }
    
    global class popup
    {
        global list<string> header;
        global list<tab> tabs;
    }
    
    global class tabItem
    {
        global string file_id;
        global object topic_id;
    }
    
    global class tab
    {
        global string tab_id;
        global string tab_label;
        global list<tabItem> data;
    }
    
    global class mapinfoObj
    {
        global decimal nelat;
        global decimal nelng;
        global decimal swlat;
        global decimal swlng;
        global decimal celat;
        global decimal celng;
        global integer limitToBeReplaced;
        global integer offset;
    }
    
    global class request
    {
        global boolean aggregates;
        global dataObj data;
        global list<object> ids;
        global mapinfoObj mapinfo;
        global boolean details;
    }
}