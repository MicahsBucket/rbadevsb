/**
 * @File Name          : SalesSchedTestUtils.cls
 * @Description        : 
 * @Author             : James Loghry (Demand Chain)
 * @Group              : 
 * @Last Modified By   : Connor.Davis@andersencorp.com
 * @Last Modified On   : 03-29-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/4/2019, 10:30:07 AM   James Loghry (Demand Chain)     Initial Version
**/
public class SalesSchedTestUtils {

    public List<User> reps {get; private set;}
    public List<User> managers {get; private set;}
    public Account store {get; private set;}
    public Account dwelling {get; private set;}
    public Slot__c slot {get; private set;}
    public Contact customer {get; private set;}
    public List<Sales_Capacity__c> capacities {get; private set;}
    public List<Sales_Capacity__c> pastCapacities {get; private set;}
    public Sales_Order__c salesOrder {get; set;}
    public List<Sales_Appointment__c> appointments {get; set;}
    public Map<String,List<Slot__c>>  slotMap {get; private set;}
    public Opportunity opportunity {get; private set;}
    private TestUtilityMethods tum {get; set;}
    private Datetime appointmentStartTime {get; set;}

    public SalesSchedTestUtils(){
        this.tum = new TestUtilityMethods();
    }

    public void createSalesManagersAndReps(Integer numSalesManagers, Integer numSalesReps){
        String storeName = '77 - Twin Cities, MN';

        //Creating RMS_Settings__c
        insert new List<RMS_Settings__c>{
            new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID'),
            new RMS_Settings__c(Value__c = '1234567890', Name='Ordering Portal Profile ID')
        };
    
        this.tum = new TestUtilityMethods();
        this.store = tum.createStoreAccount(storeName);
        this.store.Slot_Group__c = 'Three Slots';
        insert this.store;

        Store_Configuration__c storeConfig = new Store_Configuration__c(
        	Store__c = this.store.Id,
            Slot_Group__c = 'Three Slots',
            Location_Number__c = '777',
            Order_Number__c = 0000001,
            Distance_Warning__c = 50,
            Call_Center_Close_Time__c = Time.newInstance(18,0,0,0),
            Current_Sales_Date__c = Date.today()
        );
        insert storeConfig;
        this.store.Active_Store_Configuration__c = storeConfig.Id;
        update this.store;

        this.dwelling = this.tum.createDwellingAccount('TestDwelling');
		this.dwelling.Store_Location__c = this.store.id;
        this.dwelling.BillingStreet = '9900 Jamaica Ave S';
        this.dwelling.BillingCity = 'Cottage Grove';
        this.dwelling.BillingState = 'Minnesota';
        this.dwelling.BillingPostalCode = '55016';
        this.dwelling.BillingLatitude = 44.715075;
        this.dwelling.BillingLongitude = -93.271521;
        this.dwelling.ShippingStreet = '9900 Jamaica Ave S';
        this.dwelling.ShippingCity = 'Cottage Grove';
        this.dwelling.ShippingState = 'Minnesota';
        this.dwelling.ShippingPostalCode = '55016';
        this.dwelling.ShippingLatitude = 44.715075;
        this.dwelling.ShippingLongitude = -93.271521;
		insert this.dwelling;

        List<Contact> managerContacts = new List<Contact>();
        for(Integer i=0; i < numSalesManagers; i++){
            managerContacts.add(tum.createContact(this.store.Id,'test'+i));
        }

        List<Contact> repContacts = new List<Contact>();
        for(Integer i=0; i < numSalesReps; i++){
            Contact rep = tum.createContact(this.store.Id,'test'+(numSalesManagers+i));
            rep.MailingStreet = '9900 Jamaica Ave S';
            rep.MailingCity = 'Cottage Grove';
            rep.MailingState = 'Minnesota';
            rep.MailingPostalCode = '55016';
            rep.MailingLatitude = 44.715075;
            rep.MailingLongitude = -93.271521;
            repContacts.add(rep);
        }

        this.customer = tum.createContact(this.dwelling.Id,'test'+(numSalesManagers + numSalesReps));


        List<Contact> contacts = new List<Contact>();
        contacts.addAll(managerContacts);
        contacts.addAll(repContacts);
        contacts.add(customer);
        insert contacts;

        Id RMSSalesProfileId = [Select Id From Profile Where Name='Partner RMS-Sales'].Id;
        this.managers = new List<User>();

        for(Contact c : managerContacts){
            User u = tum.createUser(RMSSalesProfileId);
   			u.ContactId = c.Id;
            //u.UserRoleId = partnerUserRole.ParentRoleId;
            u.Store_Locations__c = storeName;
            u.Default_Store_Location__c = storeName;
            u.Enabled_User_Id__c = 'EnabledTest123'+Integer.valueOf(math.rint(math.random()*1000000));
            u.CommunityNickname = u.Enabled_User_Id__c;
            u.PortalRole = 'Manager';
            u.IsActive = true;
            managers.add(u);
        }

        this.reps = new List<User>();
        for(Contact c : repContacts){
            User u = tum.createUser(RMSSalesProfileId);
   			u.ContactId = c.Id;
            //u.UserRoleId = repRole.Id;
            u.Store_Locations__c = storeName;
            u.Default_Store_Location__c = storeName;
            u.Enabled_User_Id__c = 'EnabledTest234'+Integer.valueOf(math.rint(math.random()*1000000));
            u.CommunityNickname = u.Enabled_User_Id__c;
            u.IsActive = true;
            reps.add(u);
        }

        List<User> users = new List<User>();
        users.addAll(this.managers);
        users.addAll(this.reps);
        insert users;

        List<String> daysOfWeek = new List<String>{'Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'};
        List<Slot__c> slotsToInsert = new List<Slot__c>();
        slotMap = new Map<String,List<Slot__c>>();
        for(String str : daysOfWeek){
            List<Slot__c> slots = new List<Slot__c>();
        	slots.add(new Slot__c(Name='AM',Day__c = str,Start__c=Time.newInstance(8,0,0,0),End__c=Time.newInstance(12,0,0,0),Slot_Group__c='Three Slots'));
            slots.add(new Slot__c(Name='Mid',Day__c = str,Start__c=Time.newInstance(12,0,0,0),End__c=Time.newInstance(14,0,0,0),Slot_Group__c='Three Slots'));
            slots.add(new Slot__c(Name='PM',Day__c = str,Start__c=Time.newInstance(14,0,0,0),End__c=Time.newInstance(17,0,0,0),Slot_Group__c='Three Slots'));
            slots.add(new Slot__c(Name='Early AM',Day__c = str,Start__c=Time.newInstance(8,0,0,0),End__c=Time.newInstance(10,0,0,0),Slot_Group__c='Five Slots'));
            slots.add(new Slot__c(Name='Late AM',Day__c = str,Start__c=Time.newInstance(10,0,0,0),End__c=Time.newInstance(12,0,0,0),Slot_Group__c='Five Slots'));
            slots.add(new Slot__c(Name='Mid',Day__c = str,Start__c=Time.newInstance(12,0,0,0),End__c=Time.newInstance(14,0,0,0),Slot_Group__c='Five Slots'));
            slots.add(new Slot__c(Name='Early PM',Day__c = str,Start__c=Time.newInstance(14,0,0,0),End__c=Time.newInstance(17,0,0,0),Slot_Group__c='Five Slots'));
            slots.add(new Slot__c(Name='Late PM',Day__c = str,Start__c=Time.newInstance(17,0,0,0),End__c=Time.newInstance(20,0,0,0),Slot_Group__c='Five Slots'));
            slotsToInsert.addAll(slots);
            this.slotMap.put(str,slots);
        }
        insert slotsToInsert;


        this.capacities = new List<Sales_Capacity__c>();
        this.pastCapacities = new List<Sales_Capacity__c>();
        Date today = Date.today();
        Date yesterday = Date.today().addDays(-1);

        String todayStr = ((Datetime)today).formatGmt('yyyy-MM-dd');
  		String yesterdayStr = ((Datetime)yesterday).formatGmt('yyyy-MM-dd');

        this.slot = slotMap.get(((Datetime)today).formatGmt('EEEE')).get(0);
        Slot__c yesterdaySlot = slotMap.get(((Datetime)yesterday).formatGmt('EEEE')).get(0);


        for(User rep : this.reps){
        	this.pastCapacities.add(
                 new Sales_Capacity__c(
                    User__c=rep.Id,
                    OwnerId=rep.Id,
                    Store__c= this.store.Id,
                    Status__c='Available',
                    Date__c=yesterday,
                    Slot__c= yesterdaySlot.Id,
                    External_Id__c = rep.Id + '_' + yesterdayStr + '_' + slot.Id
                )
            );
            this.capacities.add(
                 new Sales_Capacity__c(
                    User__c=rep.Id,
                    OwnerId=rep.Id,
                    Store__c= this.store.Id,
                    Status__c='Available',
                    Date__c=today,
                    Slot__c=this.slot.Id,
                    External_Id__c = rep.Id + '_' + todayStr + '_' + slot.Id
                )
            );
        }
        List<Sales_Capacity__c> capacitiesToInsert = new List<Sales_Capacity__c>();
        capacitiesToInsert.addAll(this.pastCapacities);
        capacitiesToInsert.addAll(this.capacities);
        insert capacitiesToInsert;
    }

    public void createPastAppointments(Integer numAppointments){
        createAppointments(numAppointments,Date.today().addDays(-1));
    }

    public void createAppointments(Integer numAppointments){
        createAppointments(numAppointments,Date.today());
    }

    public void createAppointments(Integer numAppointments, Date day){
        String dayFormatted = ((Datetime)day).formatGmt('EEEE');
        this.opportunity =  this.tum.createOpportunity('TestOpp', this.dwelling.Id, 'Prospecting', this.reps.get(0).Id, day);
		this.opportunity.rSuite_Id__c = 'examplersuiteId0'+dayFormatted;
		this.opportunity.Store_Location__c = this.store.Id;
		insert this.opportunity;

        insert new OpportunityContactRole(OpportunityId = this.opportunity.Id,ContactId=this.reps.get(0).ContactId,Role='Decision Maker',isPrimary=true);

        this.salesOrder = new Sales_Order__c(Opportunity__c=this.opportunity.Id);
        insert this.salesOrder;

        Slot__c slot = this.slotMap.get(dayFormatted).get(0);

		this.appointments = new List<Sales_Appointment__c>();
        for(Integer i=0; i < numAppointments; i++){
            this.appointments.add(
                new Sales_Appointment__c(
        			Sales_Order__c = this.salesOrder.Id,
                    Appointment_Date_Time__c = Datetime.newInstance(day,slot.Start__c),
                    Date__c = day,
                    Slot__c = slot.Id
        		)
			);
        }
		insert this.appointments;
    }
}