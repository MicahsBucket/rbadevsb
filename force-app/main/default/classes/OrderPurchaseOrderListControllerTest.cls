/*
* @author Jason Flippen
* @date 09/15/2020 
* @description Test Class provides code coverage for the following classes:
*               - OrderPurchaseOrderListControllerTest
*/
@isTest
public class OrderPurchaseOrderListControllerTest {

    /*
    * @author Jason Flippen
    * @date 11/24/2020
    * @description Method to create data to be consumed by test methods.
    */
    @testSetup
    static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();
        
        Account testVendorAccount = testUtility.createVendorAccount('Test Vendor Account');
        insert testVendorAccount;
        
        Account testStoreAccount = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];

        Store_Vendor__c testStoreVendor = new Store_Vendor__c(Store__c = testStoreAccount.Id,
                                                              Vendor__c = testVendorAccount.Id);
        insert testStoreVendor;

        Account testDwellingAccount = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAccount.Store_Location__c = testStoreAccount.Id;
        insert testDwellingAccount;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAccount.Id, 'Closed - Won');
        insert testOpportunity;
        
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;
        
        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Legacy_Service').getRecordTypeId(),
                                            Vendor__c = testVendorAccount.Id,
                                            Product_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c = testFan.Id);
        insert testProduct;
        
        Vendor_Product__c testVendorProduct = new Vendor_Product__c(Vendor__c = testVendorAccount.Id,
                                                                    Product__c = testProduct.Id);
        insert testVendorProduct;
        
        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;
        
        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order',
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Record_Type').getRecordTypeId(),
                                     AccountId = testDwellingAccount.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAccount.Id,
                                     OpportunityId = testOpportunity.Id,           
                                     Status = 'Draft',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;
        
        OrderItem testOrderItem = new OrderItem(OrderId = testOrder.Id,
                                                PricebookentryId = testPBEStandard.Id,
                                                Quantity = 2,
                                                Quanity_Ordered__c = 2,
                                                Unit_Wholesale_Cost__c = 50.00,
                                                UnitPrice = 100,
                                                Variant_Number__c = 'ABCD1234');
        insert testOrderItem;
        
    }

    /*
    * @author Jason Flippen
    * @date 11/24/2020
    * @description Method to test the functionality in the Controller.
    */ 
    private static testMethod void testController() {

        Account testStoreAccount = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Order testOrder = [SELECT Id, Status FROM Order WHERE Name = 'Test Order'];

        Test.startTest();

            String defaultStoreLocationId = OrderPurchaseOrderListController.getDefaultStoreLocationId(testOrder.Id);

            List<OrderPurchaseOrderListController.VendorWrapper> vendorList = OrderPurchaseOrderListController.getVendorList(testOrder.Id);
            System.assertEquals(1, vendorList.size(), 'Unexpected Vendor Count');

            List<OrderPurchaseOrderListController.OrderItemWrapper> orderItemList = vendorList[0].orderItemList;
            System.assertEquals(1, orderItemList.size(), 'Unexpected Order Item Count');

            Purchase_Order__c testPurchaseOrder = new Purchase_Order__c(Name = 'ABCD12345',
                                                                        Estimated_Ship_Date__c = Date.today(),
                                                                        Order__c = testOrder.Id,
                                                                        Vendor__c = vendorList[0].value,
                                                                        Store_Location__c = testStoreAccount.Id);
            
            Map<String,String> purchaseOrderSaveResult = OrderPurchaseOrderListController.createPO(orderItemList, testOrder.Id, testPurchaseOrder, vendorList[0].label);
            System.assertEquals(true, purchaseOrderSaveResult.containsKey('New PO Success'), 'Unexpected Purchase Order Save Result');

            List<OrderPurchaseOrderListController.PurchaseOrderWrapper> purchaseOrderList = OrderPurchaseOrderListController.getPurchaseOrderData(testOrder.Id, '/lightning/r/Purchase_Order__c/');
            System.assertEquals(1, purchaseOrderSaveResult.size(), 'Unexpected Purchase Order Count');

        Test.stopTest();

    }

}