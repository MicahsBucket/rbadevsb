@RestResource(urlMapping='/postSurveyError')
global with sharing class SurveyErrorsRestAPI  {

    @HttpPost
    global static String create(String application, String details, String externalErrorId, Id salesforceId, String statusMedallia) {

        String returnMessage = '';
        Survey_Error__c surveyError = new Survey_Error__c(
            Details__c = details,
            External_Error_Id__c = externalErrorId,
            Survey__c = salesforceId,
            Application__c = application
        );
        
        Database.SaveResult sr = Database.insert(surveyError, true);
       
        // Iterate through each returned result
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                returnMessage = 'Successfully inserted Survey Error. Survey Error ID: ' + sr.getId();
            } else {
                for(Database.Error err : sr.getErrors()){
                    returnMessage += 'The following error has occurred - ' + err.getStatusCode() + ': ' + err.getMessage() + 
                    '\nSurvey Error fields that affected this error: ' + err.getFields() + 
                    '\n-----------------\n';                    
                }
            }
        String jsonReturnMessage = JSON.serialize(returnMessage);
        return jsonReturnMessage;
    }
}