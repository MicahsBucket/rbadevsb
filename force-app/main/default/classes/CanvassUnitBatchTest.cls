@isTest(SeeAllData=false)
global class CanvassUnitBatchTest {

    public static testMethod void test1()
    {        
        CNVSS_Canvass_Market__c canvassMarket = new CNVSS_Canvass_Market__c();
        canvassMarket.Name = 'Test';
        canvassMarket.CNVSS_Canvass_Market_Manager__c = userInfo.getUserId();
        canvassMarket.CNVSS_ISC_Phone_Number__c = '5555555555';
        insert canvassMarket;
        
        Canvass_Market_Batch_Job__c canvassMarketBatchJob = new Canvass_Market_Batch_Job__c();
        canvassMarketBatchJob.Canvass_Market__c = canvassMarket.Id;
        insert canvassMarketBatchJob;
        
        Canvass_Market_Zip_Code__c canvassMarketZip = new Canvass_Market_Zip_Code__c();
        canvassMarketZip.Name = '28217';
        canvassMarketZip.Zip_Code__c = '28217';
        canvassMarketZip.Canvass_Market__c = canvassMarket.id;
        insert canvassMarketZip;
        
        set<string> zipSet = new set<string>();
        zipSet.add(canvassMarketZip.Zip_Code__c);
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('MockPropertyData');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
        database.executebatch(new CanvassUnitBatch(zipSet,canvassMarket.Id, canvassMarketBatchJob.Id),1);
        database.executebatch(new CanvassUnitBatch(zipSet, canvassMarket.Id, canvassMarketBatchJob.Id, 1, '123'),1);
        database.executebatch(new CanvassUnitBatch(zipSet, canvassMarket.Id, canvassMarketBatchJob.Id, 2, '123'),1);
        Test.stopTest();
    }
}