public with sharing class StoreGoalController {
    @AuraEnabled
    public static List<Store_Goal__c> getGoals() {
		Id storeConfig = getUserStoreConfiguration();
        List<Store_Goal__c> goals =  [SELECT Id, Entry_Doors__c, Installed_Revenue__c, Month_Year__c, Patio_Doors__c, Series_1_Windows__c, Series_2_Windows__c, Store_Configuration__c 
                FROM Store_Goal__c 
                WHERE Store_Configuration__c = :storeConfig];
        System.debug('Goals: ' + goals);
        return goals;
    }
    
    @AuraEnabled
    public static String getStoreName() {
        String storeName = [Select Default_Store_Location__c from User where Id =:UserInfo.getUserId() limit 1].Default_Store_Location__c;
        Id storeId = [Select Id from Account where Name = :storeName limit 1].Id;
        System.debug('StoreId: ' + storeId);
        return [Select Store__r.Name from Store_Configuration__c where Store__r.Id =:storeId].Store__r.Name;
    }
    
    @AuraEnabled
    public static Id getUserStoreConfiguration() {
        String storeName = [Select Default_Store_Location__c from User where Id =:UserInfo.getUserId() limit 1].Default_Store_Location__c; //Default_Store_Location__c on user record
        System.debug('StoreName: ' + storeName);
        Id storeId = [Select Id from Account where Name = :storeName limit 1].Id;
        Id storeConfig = [select Id from Store_Configuration__c where Store__r.Id = :storeId].Id;
		return storeConfig;        
    }

}