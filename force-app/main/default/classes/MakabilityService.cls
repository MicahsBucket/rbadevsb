/**
 * @description       : Interface for running makability classes
 * @author            : mark.rothermal@andersencorp.com
 * @group             : 
 * @last modified on  : 02-24-2021
 * @last modified by  : mark.rothermal@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                            Modification
 * 1.0   02-24-2021   mark.rothermal@andersencorp.com   Initial Version
**/
Public Interface MakabilityService {

	List<MakabilityRestResource.MakabilityResult> checkCompatibility(map <string,MakabilityRestResource.OrderItem> requests, set<id> productIds);

}