@isTest
public class AerisWeatherForecastHelperTest {
    private static final String WORK_TYPE_NAME = 'Test Type 1';
    private static final String INSTALL_WORK_TYPE_NAME = 'Install';
    private static final String MEASURE_WORK_TYPE_NAME = 'Measure';
    private static final String SERVICE_WORK_TYPE_NAME = 'Service';
    private static final String JOB_SITE_VISIT_WORK_TYPE_NAME = 'Job Site Visit';

    @TestSetup
    static void setup(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        // FSLRollupRolldownController.disableFSLRollupRolldownController = true;

        OperatingHours oh1 = new OperatingHours();
        oh1.Name = 'Test Operating Hours One';
        oh1.TimeZone = 'America/Chicago';
        insert oh1;

        ServiceTerritory st1 = new ServiceTerritory();
        st1.Name = 'Test Service Territory One';
        st1.OperatingHoursId = oh1.Id;
        st1.IsActive = true;
        insert st1;
        
        WorkType installWorkType = new WorkType(
            Name = INSTALL_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType measureWorkType = new WorkType(
            Name = MEASURE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType serviceWorkType = new WorkType(
            Name = SERVICE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );

        WorkType jobSiteVisitWorkType = new WorkType(
            Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 1,
            DurationType = 'Hours'
        );
        insert new List<WorkType>{installWorkType, measureWorkType, serviceWorkType, jobSiteVisitWorkType};

        WorkOrder measureWO = new WorkOrder(
            Recommended_Crew_Size__c = 2,
            Street = '219 North Milwaukee Street',
            City = 'Milwaukee',
            State = 'Wisconsin',
            PostalCode = '53208',
            CountryCode = 'US',
            WorkTypeId = measureWorkType.Id,
            ServiceTerritoryId = st1.Id
        );
        insert measureWO;

        // FSLRollupRolldownController.disableFSLRollupRolldownController = false; 
    }

    @isTest 
    static void testPerformCalloutToAPI_ValidResponse_AllThresholdsViolated() {
    	Datetime now = Datetime.now();
        ServiceAppointment sa = createAndRetrieveServiceAppointment(false, now, now.addHours(4));

        Weather_Forecast_Threshold__c thresholdConfig = new Weather_Forecast_Threshold__c(
        	Service_Territory__c = sa.ServiceTerritoryId,
        	Work_Order_Record_Types__c = 'All',
        	Criteria_Logic__c = 'All Criteria (AND)',
        	Max_Temp_F__c = 0,
        	Wind_Speed_MPH__c = 0
        );
        insert thresholdConfig;

        String mockResponseBody = [SELECT Body FROM StaticResource WHERE Name = 'MockAerisResponse'].Body.toString();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator(mockResponseBody, 200));

        Test.startTest();
        List<ServiceAppointment> results = AerisWeatherForecastsHelper.updateForecasts(new List<ServiceAppointment>{sa});
        Test.stopTest();

        System.assertEquals(true,results[0].FSL__InJeopardy__c);
    }

    @isTest 
    static void testPerformCalloutToAPI_ValidResponse_ThresholdsNotViolated() {
    	Datetime now = Datetime.now();
        ServiceAppointment sa = createAndRetrieveServiceAppointment(false, now, now.addHours(4));

        Weather_Forecast_Threshold__c thresholdConfig = new Weather_Forecast_Threshold__c(
        	Service_Territory__c = sa.ServiceTerritoryId,
        	Work_Order_Record_Types__c = 'All',
        	Criteria_Logic__c = 'All Criteria (AND)',
        	Max_Temp_F__c = 0,
        	Wind_Speed_MPH__c = 99
        );
        insert thresholdConfig;

        String mockResponseBody = [SELECT Body FROM StaticResource WHERE Name = 'MockAerisResponse'].Body.toString();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator(mockResponseBody, 200));

        Test.startTest();
        List<ServiceAppointment> results = AerisWeatherForecastsHelper.updateForecasts(new List<ServiceAppointment>{sa});
        Test.stopTest();

        System.assertEquals(false,results[0].FSL__InJeopardy__c);
    }

    @isTest 
    static void testPerformCalloutToAPI_ValidResponse_OneThresholdViolated() {
    	Datetime now = Datetime.now();
        ServiceAppointment sa = createAndRetrieveServiceAppointment(false, now, now.addHours(4));

        Weather_Forecast_Threshold__c thresholdConfig = new Weather_Forecast_Threshold__c(
        	Service_Territory__c = sa.ServiceTerritoryId,
        	Work_Order_Record_Types__c = 'All',
        	Criteria_Logic__c = 'Any Criteria (OR)',
        	Max_Temp_F__c = 0,
        	Wind_Speed_MPH__c = 99
        );
        insert thresholdConfig;

        String mockResponseBody = [SELECT Body FROM StaticResource WHERE Name = 'MockAerisResponse'].Body.toString();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator(mockResponseBody, 200));

        Test.startTest();
        List<ServiceAppointment> results = AerisWeatherForecastsHelper.updateForecasts(new List<ServiceAppointment>{sa});
        Test.stopTest();

        System.assertEquals(true,results[0].FSL__InJeopardy__c);
    }


    @isTest 
    static void testBatchScheduleUpdateForecasts() {
    	Datetime now = Datetime.now();
        ServiceAppointment sa = createAndRetrieveServiceAppointment(false, now, now.addHours(4));

        Weather_Forecast_Threshold__c thresholdConfig = new Weather_Forecast_Threshold__c(
        	Service_Territory__c = sa.ServiceTerritoryId,
        	Work_Order_Record_Types__c = 'All',
        	Criteria_Logic__c = 'Any Criteria (OR)',
        	Max_Temp_F__c = 0,
        	Wind_Speed_MPH__c = 99
        );
        insert thresholdConfig;

        String mockResponseBody = [SELECT Body FROM StaticResource WHERE Name = 'MockAerisResponse'].Body.toString();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator(mockResponseBody, 200));

        Test.startTest();
       	(new BatchScheduleUpdateForecasts()).execute(null);
        Test.stopTest();

        ServiceAppointment result = [SELECT FSL__InJeopardy__c FROM ServiceAppointment WHERE Id = :sa.Id];

        //System.assertEquals(true,result.FSL__InJeopardy__c);
    }


    @isTest 
    static void testPerformCalloutToAPI_BadCallout() {
    	Datetime now = Datetime.now();
        ServiceAppointment sa = createAndRetrieveServiceAppointment(false, now, now.addHours(4));

        String mockResponseBody = [SELECT Body FROM StaticResource WHERE Name = 'MockAerisResponse'].Body.toString();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator(mockResponseBody, 404));

        Test.startTest();
        List<ServiceAppointment> results = AerisWeatherForecastsHelper.updateForecasts(new List<ServiceAppointment>{sa});
        Test.stopTest();

        system.assert(results[0].Weather_Forecast_JSON__c.contains('Error occurred during weather API callout attempt.'));
    }

    @isTest 
    static void testPerformCalloutToAPI_Response200WithoutResponseElement() {
    	Datetime now = Datetime.now();
        ServiceAppointment sa = createAndRetrieveServiceAppointment(false, now, now.addHours(4));

        AerisWeatherForecastResponse resp = AerisWeatherForecastResponse.parse([SELECT Body FROM StaticResource WHERE Name = 'MockAerisResponse'].Body.toString());
        resp.response = null;
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator(JSON.serialize(resp), 200));

        Test.startTest();
        List<ServiceAppointment> results = AerisWeatherForecastsHelper.updateForecasts(new List<ServiceAppointment>{sa});
        Test.stopTest();

        system.assert(results[0].Weather_Forecast_JSON__c.contains('Error occurred during weather API callout attempt - unable to process response.'));
    }

    @isTest 
    static void testPerformCalloutToAPI_Response200WithoutPeriods() {
    	Datetime now = Datetime.now();
        ServiceAppointment sa = createAndRetrieveServiceAppointment(false, now, now.addHours(4));

        AerisWeatherForecastResponse resp = AerisWeatherForecastResponse.parse([SELECT Body FROM StaticResource WHERE Name = 'MockAerisResponse'].Body.toString());
        resp.response[0].periods.clear();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator(JSON.serialize(resp), 200));

        Test.startTest();
        List<ServiceAppointment> results = AerisWeatherForecastsHelper.updateForecasts(new List<ServiceAppointment>{sa});
        Test.stopTest();

        system.assert(results[0].Weather_Forecast_JSON__c.contains('Error occurred during weather API callout attempt - no forecast data returned.'));
    }

    @isTest 
    static void testPerformCalloutToAPI_Response200WithError() {
    	Datetime now = Datetime.now();
        ServiceAppointment sa = createAndRetrieveServiceAppointment(false, now, now.addHours(4));

        String mockResponseBody = [SELECT Body FROM StaticResource WHERE Name = 'MockAerisFailedResponse'].Body.toString();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator(mockResponseBody, 200));

        Test.startTest();
        List<ServiceAppointment> results = AerisWeatherForecastsHelper.updateForecasts(new List<ServiceAppointment>{sa});
        Test.stopTest();

        system.assert(results[0].Weather_Forecast_JSON__c.contains('Error occurred during weather API callout attempt.  Response object'));
    }


    public static ServiceAppointment createAndRetrieveServiceAppointment(Boolean isMultiday, Datetime startTime, Datetime endTime) {
    	WorkOrder wo = [SELECT Id, ServiceTerritoryId, Street, City, State, PostalCode, Country, CountryCode, Latitude, Longitude FROM WorkOrder LIMIT 1];

    	ServiceAppointment sa = new ServiceAppointment(
                ParentRecordId = wo.Id,
                ServiceTerritoryId = wo.ServiceTerritoryId,
                Street = wo.Street,
                City = wo.City,
                State = wo.State,
                PostalCode = wo.PostalCode,
                Country = wo.Country,
                CountryCode = wo.CountryCode,
                Latitude = wo.Latitude,
                Longitude = wo.Longitude,
                FSL__IsMultiDay__c = isMultiday,
                SchedStartTime = startTime,
                SchedEndTime = endTime,
                EarliestStartTime = Datetime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0)),
                DueDate = Datetime.newInstance(Date.today().addDays(6), Time.newInstance(23, 59, 59, 0))
            );

    	insert sa;

    	ServiceAppointment result = [SELECT PostalCode,Id,Latitude,Longitude,SchedStartTime,SchedEndTime,Weather_Timestamp__c,FSL__InJeopardy__c,FSL__InJeopardyReason__c,
        	ServiceTerritoryId,FSL__IsMultiDay__c,FSL__MDT_Operational_Time__c,Work_Order__r.RecordType.Name,ServiceTerritory.ParentTerritoryId, Weather_Forecast_Warnings__c 
			FROM ServiceAppointment WHERE Id = :sa.Id];

		return result;
    }

	public class MockResponseGenerator implements HttpCalloutMock {
		private String responseBody;
		private Integer statusCode;

		public MockResponseGenerator(String responseBody, Integer statusCode) {
			this.responseBody = responseBody;
			this.statusCode = statusCode;
		}

	    public HTTPResponse respond(HTTPRequest req) {
	        
	        // Create a fake response
	        HttpResponse res = new HttpResponse();
	        res.setHeader('Content-Type', 'application/json');
	        res.setBody(this.responseBody);
	        res.setStatusCode(this.statusCode);
	        return res;
	    }
	}
}