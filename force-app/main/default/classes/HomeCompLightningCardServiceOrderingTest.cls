/*
* @author Jason Flippen
* @date 11/17/2020
* @description Test Class for the following Classes:
*              - HomeCompLightningCardServiceOrdering
*/ 
@isTest
public class HomeCompLightningCardServiceOrderingTest {

    /*
    * @author Jason Flippen
    * @date 11/17/2020
    * @description Method to create data for the Test Methods
    */ 
    @testSetup static void setupData() {
 
        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        Account testVenderAcct = testUtility.createVendorAccount('Test Vendor Account');
        insert testVenderAcct;

        Account testStoreAcct = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAcct = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAcct.Store_Location__c = testStoreAcct.Id;
        insert testDwellingAcct;
        
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name = 'Test FAN',
                                                                              Account_Type__c = 'Cost PO');
        insert testFAN;

        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId(),
                                            Vendor__c = testVenderAcct.Id,
                                            Cost_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c =  testFan.Id);
        insert testProduct;

        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        List<Order> testOrderList = new List<Order>();
        Order testOrder01 =  new Order(Name = 'Test Order1',
                                       AccountId = testDwellingAcct.Id,
                                       EffectiveDate = Date.Today(),
                                       Store_Location__c = testStoreAcct.Id,
                                       Status = 'Ready to Order',
                                       Tech_Measure_Status__c = 'New',
                                       Pricebook2Id = Test.getStandardPricebookId());
        testOrderList.add(testOrder01);
        Order testOrder02 =  new Order(Name = 'Test Order',
                                       AccountId = testDwellingAcct.Id,
                                       EffectiveDate = Date.Today(),
                                       Store_Location__c = testStoreAcct.Id,
                                       Status = 'Order Released',
                                       Tech_Measure_Status__c = 'New',
                                       Pricebook2Id = Test.getStandardPricebookId());
        testOrderList.add(testOrder02);
        insert testOrderList;

        Purchase_Order__c testPurchaseOrder = new Purchase_Order__c(Name = 'Test Purchase Order',
                                                                    Vendor__c = testVenderAcct.Id,
                                                                    Status__c = 'Rejected');
        insert testPurchaseOrder;
        
    }

    /*
    * @author Jason Flippen
    * @date 11/17/2020
    * @description Method to test the functionality in the getServiceRequests method.
    */
    private static testMethod void testGetServiceRequests() {

        Test.startTest();

            List<Order> testOrderList = [SELECT Id, OrderNumber FROM Order WHERE Status = 'Ready to Order'];

            if (!testOrderList.isEmpty()) {
                List<HomeCompLightningCardServiceOrdering.ServiceRequestWrapper> orderList = HomeCompLightningCardServiceOrdering.getServiceRequests(testOrderList[0].OrderNumber);
                System.assertEquals(1,orderList.size());
            }

        Test.stopTest();

    }

    /*
    * @author Jason Flippen
    * @date 11/17/2020
    * @description Method to test the functionality in the getPurchaseOrders method.
    */
    private static testMethod void testGetPurchaseOrders() {

        Test.startTest();

            List<Purchase_Order__c> testPurchaseOrderList = [SELECT Id, Name FROM Purchase_Order__c WHERE Status__c = 'Rejected'];

            if (!testPurchaseOrderList.isEmpty()) {
                List<HomeCompLightningCardServiceOrdering.PurchaseOrderWrapper> purchaseOrderList = HomeCompLightningCardServiceOrdering.getPurchaseOrders(testPurchaseOrderList[0].Name);
                System.assertEquals(1,purchaseOrderList.size());
            }

        Test.stopTest();

    }

}