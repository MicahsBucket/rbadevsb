global class MakabilityCalculator {
        public boolean checkColor {get;set;}
        public boolean checkScreen {get;set;}
        public boolean checkGlaze {get;set;}
        public boolean checkSize  {get;set;}
        public boolean checkGrille {get;set;}
        public boolean checkSpecialGrille {get;set;}
        public boolean checkFrameNotch {get;set;}
        public boolean checkSpecialShape {get;set;}
        public boolean hasLeftLeg {get;set;}
        public boolean hasRightLeg {get;set;}
        public boolean checkLegCalc {get;set;}
        public boolean checkS3 {get;set;}
    }