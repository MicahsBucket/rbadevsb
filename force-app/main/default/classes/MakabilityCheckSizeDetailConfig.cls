/**
 * @File Name          : MakabilityCheckSizeDetailConfig.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 02-24-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    5/3/2019, 6:33:10 PM   mark.rothermal@andersencorp.com     Initial Version
**/
public without sharing class MakabilityCheckSizeDetailConfig  implements MakabilityService {
    
    
    /**
    * @description
    * @author mark.rothermal@andersencorp.com | 5/3/2019
    * @param map<string, MakabilityRestResource.OrderItem> requests
    * @param set<id> productIds
    * @return List<MakabilityRestResource.MakabilityResult>
    */
    public static List<MakabilityRestResource.MakabilityResult>  checkCompatibility( map <string,MakabilityRestResource.OrderItem> requests, set<id> productIds){
        List<MakabilityRestResource.MakabilityResult> results = new List<MakabilityRestResource.MakabilityResult>();
        Map<id,Size_Detail_Configuration__c> sizeDetailConfigMap = getConfigOptions(productIds);
        Map<String,Product_Field_Control__c> uniKeyToPfc = getPfcs(productIds);
        Map<String,Product_Field_Control_Dependency__c> uniKeyToPfcdMap = getPfcds(productIds);
        //Get Wide Open Hinge Configuration
        Map<id,Wide_Open_Hinge_Height_Configuration__c> wohConfigMap = getWohcfs(productIds);
        // for high performance
        Map<String,List<Size_Detail_Configuration__c>> uniKeyToConfigListMap = new Map<String,List<Size_Detail_Configuration__c>>();
        // for standard
        Map<String,Size_Detail_Configuration__c> uniKeyToConfigMap = new Map<String,Size_Detail_Configuration__c>();
        //for woh
        Map<String,List<Wide_Open_Hinge_Height_Configuration__c>> uniKeyToWohConfigListMap = new Map<String,List<Wide_Open_Hinge_Height_Configuration__c>>();
        system.debug('requests in check size! '+requests);
        
        // for Coastal Perf Category -- Sandeep
        Map<String,List<Size_Detail_Configuration__c>> uniKeyToCoastalListMap = new Map<String,List<Size_Detail_Configuration__c>>();
        
        // handle no config record found.
        if(sizeDetailConfigMap.keyset().size() == 0){
           
            for(String r:requests.keyset()){
                MakabilityRestResource.OrderItem oi = requests.get(r);
                MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult(); 
                List<string> errMessages = new List<string>();
                string errMessage = 'Salesforce - No Size Detail configurations found for provided productId(s).';  
                errMessages.add(errMessage);
                result.errorMessages = errMessages;
                result.orderId = oi.orderId;
                result.orderItemId = oi.orderItemId;
                results.add(result);                
            }            
            return results;
        }
        
        // handle no product field control records found.
        if(uniKeyToPfc.keyset().size() == 0){
           
            for(String r:requests.keyset()){
                MakabilityRestResource.OrderItem oi = requests.get(r);
                MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult(); 
                List<string> errMessages = new List<string>();
                string errMessage = 'Salesforce - No Product Field Control records found for provided productId(s).';  
                errMessages.add(errMessage);
                result.errorMessages = errMessages;
                result.orderId = oi.orderId;
                result.orderItemId = oi.orderItemId;
                results.add(result);                
            }            
            return results;
        } 
       
        // handle no product field dependency records found.

        // build product id to Size Detail configuration map 
        // populate map
        for(id sizeConfig:sizeDetailConfigMap.keyset()) {
            
            String uniKey;
            Size_Detail_Configuration__c sc = sizeDetailConfigMap.get(sizeConfig);
            // possible need to null check here.
            uniKey = sc.Product_Configuration__r.Product__c + sc.Frame_Type__c + sc.Sash_Ratio__c + sc.Sash_Operation__c + sc.Specialty_Shape__c + sc.Hardware_Options__c;
            system.debug('unique key ' + uniKey);
            if(sc.Performance_Category__c.equalsIgnoreCase('DP Upgrade')){
                if(uniKeyToConfigListMap.containskey(uniKey)){
                    List<Size_Detail_Configuration__c> placeholder = uniKeyToConfigListMap.get(uniKey);
                    placeholder.add(sc);
                }
                else {
                    List<Size_Detail_Configuration__c> placeholder = new List<Size_Detail_Configuration__c>();
                    placeholder.add(sc);
                    uniKeyToConfigListMap.put(uniKey, placeholder);                                
                }                      
            }
            if(sc.Performance_Category__c == 'Standard'){
                if(!uniKeyToConfigMap.containskey(uniKey)){
                    uniKeyToConfigMap.put(uniKey, sc); 
                    System.debug('The uniKeyTOConfigMap for standard: '+uniKeyToConfigMap);                               
                }                 
            }
            //For Coastal Performance Category -- Sandeep
            if(sc.Performance_Category__c == System.Label.CoastalPerformanceCategory){
                if(uniKeyToCoastalListMap.containskey(uniKey)){
                    List<Size_Detail_Configuration__c> placeholder = uniKeyToCoastalListMap.get(uniKey);
                    placeholder.add(sc);
                }
                else {
                    List<Size_Detail_Configuration__c> placeholder = new List<Size_Detail_Configuration__c>();
                    placeholder.add(sc);
                    uniKeyToCoastalListMap.put(uniKey, placeholder);                                
                }                      
            }
            
        }       
        ////////////////////////////// 
        // run makability check  
        // //////////////////////////        
        for(String r:requests.keyset()){
           
            MakabilityRestResource.OrderItem req = requests.get(r);                  
            List<string> errMessages = new List<string>();        
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
            result.orderItemId = req.orderItemId;
            result.orderId = req.orderId;
            ProductConfiguration p = req.ProductConfiguration;
            Boolean sizeDetailPasses = true;
            Boolean currentConfigSet = false;
            Boolean checkSpecialShape = req.makabilityCalculator.checkSpecialShape;
            Boolean checkLegCalc = req.makabilityCalculator.checkLegCalc;
            Size_Detail_Configuration__c currentConfig = null;
            Wide_Open_Hinge_Height_Configuration__c currentWohConfig = null;
            Boolean currentWohConfigSet = false;            
            Double requestUnitedInches = MakabilityUtility.calculateUnitedInches(p.heightInches,p.heightFractions,p.widthInches,p.widthFractions);            
            String requestKey = p.productId + p.frame + p.sashRatio + p.sashOperation + p.specialtyShape + p.hardwareOption;
            system.debug('request unique key in size detail ' + requestKey);
            system.debug('Product Configuration P value ' + p);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////// Find the correct configuration record ////////////////////////////////////////////////////
            // is request High Performance or standard -- need to update this to be DpUpgrade or standard
            if(p.highPerformance || (String.isNotBlank(p.performanceCategory) && p.performanceCategory.equalsIgnoreCase('Dp Upgrade'))){
                System.debug('The value of p.highPerformance: '+p.highPerformance);
                if(uniKeyToConfigListMap.containsKey(requestKey)){
                    system.debug('uniKeyToConfigListMap in size detail ' + uniKeyToConfigListMap);
                    
                    List<Size_Detail_Configuration__c> sdcs = uniKeyToConfigListMap.get(requestKey);
                    system.debug('sdcs in size detail ' + sdcs);
                    system.debug('req in size detail ' + req);
                    dpiResult res = compareRequestAgainstConfigList(req,sdcs);
                    system.debug('res in size detail ' + res);
                    if(res.sdc != null){
                        currentConfig =  res.sdc; 
                        currentConfigSet = true;                        
                    } else {
                        sizeDetailPasses = false;
                        errMessages.add(res.errMessage);
                    }
                } else{
                    // no high performance config found
                    sizeDetailPasses = false;
                    errMessages.add ('Size Detail Config - Unable to find correct size detail configuration. Please check that you have included all the correct information for your window.' +
                                     'Frame type, Sash Operation, Sash Ratio, Specialty Shape, Hardware options and Positive / Negative Force values.' );                         
                }
            } //For Coastal Perf Category -- Sandeep
            else if(String.isNotBlank(p.performanceCategory) && p.performanceCategory.equals(System.Label.CoastalPerformanceCategory)) {
                if(uniKeyToCoastalListMap.containsKey(requestKey)){
                    system.debug('uniKeyToCoastalListMap in size detail ' + uniKeyToCoastalListMap);
                    
                    List<Size_Detail_Configuration__c> sdcs = uniKeyToCoastalListMap.get(requestKey);
                    system.debug('sdcs in size detail ' + sdcs);
                    system.debug('req in size detail ' + req);
                    dpiResult res = compareRequestAgainstConfigList(req,sdcs);
                    system.debug('res in size detail ' + res);
                    if(res.sdc != null){
                        currentConfig =  res.sdc; 
                        currentConfigSet = true;                        
                    } else {
                        sizeDetailPasses = false;
                        errMessages.add(res.errMessage);
                    }
                } else{
                    // no high performance config found
                    sizeDetailPasses = false;
                    errMessages.add ('Size Detail Config - Unable to find correct size detail configuration. Please check that you have included all the correct information for your window.' +
                                     'Frame type, Sash Operation, Sash Ratio, Specialty Shape, Hardware options and Positive / Negative Force values.' );                         
                }
            }
            else{
                if(uniKeyToConfigMap.containsKey(requestKey)){
                    currentConfig = uniKeyToConfigMap.get(requestKey); 
                    System.debug('The current Config value: '+currentConfig);
                    currentConfigSet = true;                    
                } else {
                    // no standard config found
                    sizeDetailPasses = false;
                    errMessages.add ('Size Detail Config - Unable to find correct size detail configuration. Please check that you have included all the correct information for your window. options may include: ' +
                                     'Frame type, Sash Operation, Sash Ratio, Specialty Shape and Hardware options.' );                         
                }
            }
            ///////////////////////////////////////////////////////////////////
            // compare request against matching config  
            ///////////////////////////////////////////////////////////////////
            if(currentConfigSet){
                // check united inches greater than minimum - 
                if(requestUnitedInches < currentConfig.United_Inch_Minimum__c){
                    sizeDetailPasses = false;
                    errMessages.add ('Size Detail Config - UI less than minimum: ' +
                                     'UI Minimum = ' + currentConfig.United_Inch_Minimum__c + ' inches.' );                         
                }
                // check united inches less than maximum
                if(requestUnitedInches > currentConfig.United_Inch_Maximum__c){
                    sizeDetailPasses = false;
                    errMessages.add ('Size Detail Config - UI greater than maximum: ' +
                                     'UI Maximum = ' + currentConfig.United_Inch_Maximum__c + ' inches.' );                         
                }
                // check width against minimum width
                if(!MakabilityUtility.compareMinHeightOrWidth(p.widthInches,p.widthFractions,currentConfig.Min_Width_Inches__c, currentConfig.Min_Width_Fraction__c)){
                    sizeDetailPasses = false;
                    errMessages.add ('Size Detail Config - Width less than minimum: ' +
                                     'Minimum Width = ' + currentConfig.Min_Width_Inches__c +' '+currentConfig.Min_Width_Fraction__c +' inches.' );                         
                }                            
                // check width against maximum - if width exceeds maximum, check extended values, if no extended values, throw max width error.
                if(!MakabilityUtility.compareMaxHeightOrWidth(p.widthInches,p.widthFractions,currentConfig.Max_Width_Inches__c, currentConfig.Max_Width_Fraction__c)){
                    //check against extended maximums if values present.
                    if(currentConfig.Extended_Max_Width_Inches__c != null && currentConfig.Extended_Max_Width_Fraction__c != null && 
                       currentConfig.Extended_Max_Height_Inches__c != null && currentConfig.Extended_Max_Height_Fraction__c != null ){
                           // max width exceeded - check extended max width and extended max height
                           if(!MakabilityUtility.compareMaxHeightOrWidth(p.widthInches,p.widthFractions,currentConfig.Extended_Max_Width_Inches__c, currentConfig.Extended_Max_Width_Fraction__c) 
                              ||!MakabilityUtility.compareMaxHeightOrWidth(p.heightInches,p.heightFractions,currentConfig.Extended_Max_Height_Inches__c, currentConfig.Extended_Max_Height_Fraction__c)){
                                  sizeDetailPasses = false;
                                  errMessages.add ('Size Detail Config - The unit width exceeds the extended maximums.');                         
                              }
                       } else {
                           // throw exceeded max width error
                           sizeDetailPasses = false;
                           errMessages.add ('Size Detail Config - Width greater than maximum: ' +
                                            'Maximum Width = ' + currentConfig.Max_Width_Inches__c +' '+currentConfig.Max_Width_Fraction__c +' inches.' );      
                       }
                }
                //check height against min height
                if(!MakabilityUtility.compareMinHeightOrWidth(p.heightInches,p.heightFractions,currentConfig.Min_Height_Inches__c, currentConfig.Min_Height_Fraction__c)){
                    sizeDetailPasses = false;
                    errMessages.add ('Size Detail Config - Height less than minimum: ' +
                                     'Minimum Height = ' + currentConfig.Min_Height_Inches__c +' '+currentConfig.Min_Height_Fraction__c +' inches.' );                         
                }                    
                // check height against maximum - check for extended max should be if/else with else block checking extended width and extended height  
                if(!MakabilityUtility.compareMaxHeightOrWidth(p.heightInches,p.heightFractions,currentConfig.Max_Height_Inches__c,currentConfig.Max_Height_Fraction__c)){                     
                    //check against extended maximums if values present.
                    if(currentConfig.Extended_Max_Width_Inches__c != null && currentConfig.Extended_Max_Width_Fraction__c != null && 
                       currentConfig.Extended_Max_Height_Inches__c != null && currentConfig.Extended_Max_Height_Fraction__c != null ){                    
                           // check extended max height and extended max width
                           if(!MakabilityUtility.compareMaxHeightOrWidth(p.heightInches,p.heightFractions,currentConfig.Extended_Max_Height_Inches__c, currentConfig.Extended_Max_Height_Fraction__c) 
                              || !MakabilityUtility.compareMaxHeightOrWidth(p.widthInches,p.widthFractions,currentConfig.Extended_Max_Width_Inches__c, currentConfig.Extended_Max_Width_Fraction__c)){
                                  sizeDetailPasses = false;
                                  errMessages.add ('Size Detail Config - The unit height exceeds the extended maximums.');                                               
                              }
                       } else {
                           // throw exceeded max height error
                           sizeDetailPasses = false;
                           errMessages.add ('Size Detail Config - Height greater than maximum: ' +
                                            'Maximum Height = ' + currentConfig.Max_Height_Inches__c +' '+currentConfig.Max_Height_Fraction__c +' inches.' );      
                       }
                }
            } else {
                // unable to match config. currentConfigSet = false
                //     sizeDetailPasses = false;
                //     errMessages.add ('Size Detail Config - Unable to find correct size detail configuration.');       
            }

            // size check product field dependancy map.
            if(uniKeyToPfcdMap.keyset().size() == 0 && checkSpecialShape == true){
                checkSpecialShape = false;
                sizeDetailPasses = false;
                errMessages.add('Salesforce - Unable to check special shape makability, no Product Field Control Dependency Records Found for this Product Id');     
            }                 
            ///////////////////////********************
            // Run special shape code below
            //////////////////////*********************
            if(checkSpecialShape && currentConfigSet){
                MakabilityCheckSpecialtyShapeConfig.specialShapeResult ssr = MakabilityCheckSpecialtyShapeConfig.checkShape(req,currentConfig,uniKeyToPfc,uniKeyToPfcdMap);
                if(!ssr.productMakable){
                    sizeDetailPasses = false;                    
                }
                if(ssr.errMessages.size() > 0){
                    errMessages.addAll(ssr.errMessages);
                    
                } 
            }
           ///////////////////////**********************
           // Run Locks Makability
           ///////////////////////**********************
           if(currentConfigSet){
               MakabilityCheckLockSize.lockSizeResult lsr = MakabilityCheckLockSize.checkLockSize(req,currentConfig,uniKeyToPfc);
                if(!lsr.productMakable){
                    sizeDetailPasses = false;                    
                }
                if(lsr.errMessages.size() > 0){
                    errMessages.addAll(lsr.errMessages);                    
                }                
           }
           ///////////////////////**********************
           // Run Leg calc makability
           ///////////////////////**********************
           if(checkSpecialShape && checkLegCalc){
               MakabilityCheckSpecialLegHeightCalc.legCalcResult lcr = MakabilityCheckSpecialLegHeightCalc.checkLegCalc(req);
                if(!lcr.productMakable){
                    sizeDetailPasses = false;                    
                }
                if(lcr.errMessages.size() > 0){
                    errMessages.addAll(lcr.errMessages);                    
                }                
           }
           
             ///////////////////////**********************
           // Run WideOpenhinge Makability
           ///////////////////////**********************
           if(currentConfigSet){
               MakabilityCheckOpenWideHinge.openWideHingeResult lsr = MakabilityCheckOpenWideHinge.checkOpenWideHinge(req,currentConfig,uniKeyToPfc);
               
               if(!lsr.productMakable){
                    sizeDetailPasses = false;                    
                }
                
                if(lsr.errMessages.size() > 0){
                    errMessages.addAll(lsr.errMessages);                    
                }
                // check WOH height makability
                if(lsr.productMakable && p.hardwareOption == 'Wide Opening Hinge'){
                    
                    String wohrequestKey = p.productId + p.sashRatio +  p.hardwareOption;
                   
                    if(wohConfigMap.keyset().size() == 0){                       
                        for(String wohr:requests.keyset()){
                            MakabilityRestResource.OrderItem oi = requests.get(wohr);
                            MakabilityRestResource.MakabilityResult wohResult = new MakabilityRestResource.MakabilityResult(); 
                            List<string> wohErrMessages = new List<string>();
                            string errMessage = 'Size Detail Config - No Wide Open Hinge Height Configurations found for provided productId(s).';
                            sizeDetailPasses = false;
                            errMessages.add(errMessage);                                 
                        }            
                    }else{
                            //added this logic to build prodcutid to wide open hinge gheight configuration map
                            List<Wide_Open_Hinge_Height_Configuration__c> wohholder = new List<Wide_Open_Hinge_Height_Configuration__c>();
                            for(id wohConfig:wohConfigMap.keyset()) {                                
                               // String uniKey;
                                Wide_Open_Hinge_Height_Configuration__c wohc = wohConfigMap.get(wohConfig);
                                // possible need to null check here.
                               // uniKey = wohc.Product_Configuration__r.Product__c + wohc.Sash_Ratio__c + p.hardwareOption;
                              
                                
                                //List<Wide_Open_Hinge_Height_Configuration__c> wohholder = new List<Wide_Open_Hinge_Height_Configuration__c>();
                                wohholder.add(wohc);
                               // uniKeyToWohConfigListMap.put(uniKey, wohholder);  
                            } 
                            // end 
                            
                           // List<Wide_Open_Hinge_Height_Configuration__c> wohcs = uniKeyToWohConfigListMap.get(wohrequestKey);
                           // system.debug('before compare'+wohcs.size());
                            Wide_Open_Hinge_Height_Configuration__c res = compareRequestAgainstWohConfigList(req,wohholder);
                           
                            if(res!=null){
                                
                                //currentWohConfig =  res; 
                                currentWohConfigSet = true;                        
                            }else {     
                                sizeDetailPasses = false;
                                errMessages.add('Wide Open Hinge Height Config - Unable to match a config record. please check sash ratio');
                            }
                            if(currentWohConfigSet){
                                
                                MakabilityCheckOpenWideHinge.openWideHingeResult olsr = MakabilityCheckOpenWideHinge.checkOpenWideHingeHeight(req,currentConfig,res);
                               
                                if(!olsr.productMakable){                                   
                                    sizeDetailPasses = false;                    
                                }
                                if(olsr.errMessages.size() > 0){                                  
                                    errMessages.addAll(olsr.errMessages);                    
                                }
                            }                           
                        }                 
                }           
           }

            //**********************************************************************
            // finish off makability results with a pass or fail and return the results
            //**********************************************************************
            if(sizeDetailPasses){
                errMessages.add('Size Detail Config  - passed');  
                result.isMakable = true;                                
            } else{
                result.isMakable = false;                                                
            }
            result.errorMessages = errMessages;                               
            results.add(result);    
        }
        system.debug('results check ' + results );        
        if(results.size() == 0){
            List<string> errMessages = new List<string>();                    
            errMessages.add('Something bad happened with the Size Detail config');
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
            result.errorMessages = errMessages;
            result.isMakable = false;
            results.add(result);
        }         
        return results;
    }
    
    /**
    * @description
    * @author mark.rothermal@andersencorp.com | 5/3/2019
    * @param set<id> productIds
    * @return Map<id, Size_Detail_Configuration__c>
    */
    public static Map<id,Size_Detail_Configuration__c> getConfigOptions(set<id> productIds){
        Map<id,Size_Detail_Configuration__c> configMap= new Map<id,Size_Detail_Configuration__c>( [
            SELECT id,
            Max_Height_Fraction__c,
            Max_Height_Inches__c,
            Max_Width_Fraction__c,
            Max_Width_Inches__c,
            Min_Height_Fraction__c,
            Min_Height_Inches__c,
            Min_Width_Fraction__c,
            Min_Width_Inches__c,
            Positive_Force__c,
            Negative_Force__c,
            Frame_Type__c,
            Hardware_Options__c,
            Sash_Operation__c,
            Sash_Ratio__c,
            Specialty_Shape__c,
            United_Inch_Maximum__c,
            United_Inch_Minimum__c,
            Performance_Category__c,
            Extended_Max_Height_Inches__c,
            Extended_Max_Height_Fraction__c,
            Extended_Max_Width_Inches__c,
            Extended_Max_Width_Fraction__c,
            Max_Leg_Height_Fraction__c,
            Max_Leg_Height_Inches__c,
            Min_Leg_Height_Fraction__c,
            Min_Leg_Height_Inches__c,
            Lock_Max_Height_Fraction__c,
            Lock_Max_Height_Inches__c,
            Lock_Max_Height_Locks__c,
            Lock_Max_Width_Fraction__c,
            Lock_Max_Width_Inches__c,
            Lock_Max_Width_Locks__c,
            Lock_Min_Height_Fraction__c,
            Lock_Min_Height_Inches__c,
            Lock_Min_Height_Locks__c,
            Lock_Min_Width_Fraction__c,
            Lock_Min_Width_Inches__c,
            Lock_Min_Width_Locks__c,
            Width_to_Height_Minimum__c,
            Width_to_Height_Maximum__c,
            Glass_Square_Foot_Max__c,
            Peak_Height_Min__c,
            Peak_Height_Max__c,
            Single_Leg__c,
            Match_Leg_Heights__c,
            WOH_Allowed_Below_Inches__c,
            WOH_Allowed_Below_Fractions__c,
            WOH_Required_Below_Inches__c,
            WOH_Required_Below_Fractions__c,
            WOH_Allowed_Above_Inches__c,
            WOH_Allowed_Above_Fractions__c,                                 
            Product_Configuration__r.Product__c
            from Size_Detail_Configuration__c 
            where Product_Configuration__r.Product__c in :productids] );  
        return configMap;
    }

    /**
    * @description
    * @author mark.rothermal@andersencorp.com | 5/3/2019
    * @param set<id> productIds
    * @return Map<string, Product_Field_Control__c>
    */
    public static Map<string,Product_Field_Control__c> getPfcs(set<id> productIds){
        Map<string,Product_Field_Control__c> unikeyToPfcMap = new Map<string,Product_Field_Control__c>();
        List<Product_Field_Control__c> pfcList= new List<Product_Field_Control__c>( [
            SELECT id,
            Field_Control_ID__r.Name,    
            Required__c,                              
            Product_Configuration_ID__r.Product__c
            from Product_Field_Control__c 
            where Product_Configuration_ID__r.Product__c in :productids] );  
       if(pfcList.size() > 0){
        for(Product_Field_Control__c pfc:pfcList){
            String uniKey = pfc.Product_Configuration_ID__r.Product__c + '-' + pfc.Field_Control_ID__r.Name;
            unikeyToPfcMap.put(uniKey,pfc);
        }
       }     


        return unikeyToPfcMap;
    }  

    /**
    * @description
    * @author mark.rothermal@andersencorp.com | 5/3/2019
    * @param set<id> productIds
    * @return Map<string, Product_Field_Control_Dependency__c>
    */
    public static Map<string,Product_Field_Control_Dependency__c> getPfcds(set<id> productIds){
        Map<string,Product_Field_Control_Dependency__c> uniKeyToPfcdMap = new Map<string,Product_Field_Control_Dependency__c>();
        List<Product_Field_Control_Dependency__c> pfcdList = new List<Product_Field_Control_Dependency__c>([
            SELECT id,
            Action_Taken__c,
            Controlling_Field__c,
            Controlling_Value__c,
            Dependent_Field__c,
            Dependent_Field__r.Product_Configuration_ID__r.Product__c,
            Dependent_Field__r.Field_Control_ID__r.Name,
            Make_Required__c
            FROM Product_Field_Control_Dependency__c
            WHERE Dependent_Field__r.Product_Configuration_ID__r.Product__c in :productids
        ]);
        for(Product_Field_Control_Dependency__c pfcd:pfcdList){
            String uniKey = pfcd.Dependent_Field__r.Product_Configuration_ID__r.Product__c + '-' + pfcd.Dependent_Field__r.Field_Control_ID__r.Name +'-'+ pfcd.Controlling_Value__c ;
            uniKeyToPfcdMap.put(uniKey,pfcd);
        }
        return uniKeyToPfcdMap;
    }

    /**
    * @description
    * @author mark.rothermal@andersencorp.com | 5/3/2019
    * @param MakabilityRestResource.OrderItem req
    * @param List<Size_Detail_Configuration__c> sdcs
    * @return dpiResult
    */
    private static dpiResult compareRequestAgainstConfigList(MakabilityRestResource.OrderItem req, List<Size_Detail_Configuration__c> sdcs){
        dpiResult result = new dpiResult();
        Size_Detail_Configuration__c sdc = new Size_Detail_Configuration__c();
        Decimal requestExternalForce = req.productConfiguration.externalForce;
        Decimal requestInternalForce = req.productConfiguration.InternalForce;
        for(Size_Detail_Configuration__c s :sdcs){
            Decimal configExternalForce = s.Positive_Force__c;
            Decimal configInternalForce = s.Negative_Force__c;  
            if (requestExternalForce == configExternalForce && requestInternalForce == configInternalForce){
                result.sdc = s;
            }
        } 
        if(result.sdc == null){
            result.errMessage = 'Size Detail Config - Positve or Negative force values incorrect';
        }
        return result;
    }

 /**
    * @description
    * @author mark.rothermal@andersencorp.com | 10/18/2020
    * @param set<id> productIds
    * @return Map<id, Size_Detail_Configuration__c>
    */
    public static Map<id,Wide_Open_Hinge_Height_Configuration__c> getWohcfs(set<id> productIds){
        Map<id,Wide_Open_Hinge_Height_Configuration__c> wohConfigMap= new Map<id,Wide_Open_Hinge_Height_Configuration__c>( [
            SELECT id,
            Starting_Width_Inches__c,
            Starting_Width_Fractions__c,
            Ending_Width_Inches__c,
            Ending_Width_Fractions__c,
            Max_Height_Inches__c,
            Max_Height_Fractions__c,
            Sash_Ratio__c,
            Product_Configuration__r.Product__c
            from Wide_Open_Hinge_Height_Configuration__c 
            where Product_Configuration__r.Product__c in :productids] );  
        return wohConfigMap;
    }

     /**
    * @description
    * @author Syed@andersencorp.com | 10/18/2020
    * @param MakabilityRestResource.OrderItem req
    * @param List<Wide_Open_Hinge_Height_Configuration__c> sdcs
    * @return dpiWohhResult
    */
    private static Wide_Open_Hinge_Height_Configuration__c compareRequestAgainstWohConfigList(MakabilityRestResource.OrderItem req, List<Wide_Open_Hinge_Height_Configuration__c> wohhcs){
       // dpiWohhResult result = new dpiWohhResult();
        Wide_Open_Hinge_Height_Configuration__c wohc = new Wide_Open_Hinge_Height_Configuration__c();
        String requestSashRatio = req.productConfiguration.sashRatio;
        String requestProdcutId = req.productConfiguration.productId;
        Double requestWidth = req.productConfiguration.widthInches + Constants.fractionConversionMap.get(req.productConfiguration.widthFractions);
        system.debug('requestSashRatio '+requestSashRatio);
        system.debug('requestProdcutId '+requestProdcutId);
        system.debug('wohhcs size:'+wohhcs.size());
        //Decimal requestInternalForce = req.productConfiguration.InternalForce;
        for(Wide_Open_Hinge_Height_Configuration__c s :wohhcs){
            
            String configSashRatio = s.Sash_Ratio__c;
            String productId = s.Product_Configuration__r.Product__c;
            Double convertedRequiredStartingWidth;

            if(String.isEmpty(s.Starting_Width_Fractions__c) || String.isBlank(s.Starting_Width_Fractions__c)){
                convertedRequiredStartingWidth   = s.Starting_Width_Inches__c;
            }else{
                convertedRequiredStartingWidth   = s.Starting_Width_Inches__c+ Constants.fractionConversionMap.get(s.Starting_Width_Fractions__c);
            }
            
            Double convertedRequiredEndingWidth;
            if(String.isEmpty(s.Ending_Width_Fractions__c) || String.isBlank(s.Ending_Width_Fractions__c)){
                 convertedRequiredEndingWidth   = s.Ending_Width_Inches__c;
            }else{
                 convertedRequiredEndingWidth   = s.Ending_Width_Inches__c+ Constants.fractionConversionMap.get(s.Ending_Width_Fractions__c);
            }

            system.debug('configSashRatio '+configSashRatio);
            system.debug('productId '+productId);
            system.debug('INSIDE convertedRequiredStartingWidth'+convertedRequiredStartingWidth +' '+requestWidth); 
            system.debug('INSIDE convertedRequiredEndingWidth'+convertedRequiredEndingWidth +' '+requestWidth); 
            //Decimal configInternalForce = s.Negative_Force__c; 
            system.debug('wide open hinge height config sashratio'+configSashRatio); 
            /// should only be looking at sash ratio on casement triple..
            if (requestSashRatio == configSashRatio || configSashRatio == null && productId == requestProdcutId){
               
                if(convertedRequiredStartingWidth <= requestWidth){
                    
                    if(requestWidth <= convertedRequiredEndingWidth){
                        system.debug('wide open hinge height config sashratio Matched with'+configSashRatio); 
                        wohc = s;
                    }

                }
               
            }
        } 
        if(wohc == null){
            system.debug('result.wohhsc == null');
           // result.errMessage = 'Wide Open Hinge Height Config - Sash Ratio values incorrect';
        }
        return wohc;
    }


    private class dpiResult{
        private Size_Detail_Configuration__c sdc {get;set;}
        private String errMessage {get;set;}
    }
    
}