/**
 * @File Name          : MakabilityCheckColorSizeLimit.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 02-24-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/30/2019, 2:10:58 PM   mark.rothermal@andersencorp.com     Initial Version
**/

global without sharing class MakabilityCheckColorSizeLimit implements MakabilityService {
    
    /**
    * @description - run makability check. takes in a list of orderitems and product ids. returns makability results.
    * @author mark.rothermal@andersencorp.com | 5/1/2019
    * @param map<string, MakabilityRestResource.OrderItem> requests
    * @param set<id> productIds
    * @return List<MakabilityRestResource.MakabilityResult>
    */
    public static List<MakabilityRestResource.MakabilityResult>  checkCompatibility( map <string,MakabilityRestResource.OrderItem> requests, set<id> productIds){
        List<MakabilityRestResource.MakabilityResult> results = new List<MakabilityRestResource.MakabilityResult>(); 
        Map<String,Color_Size_Limits__c> uniKeyToColorSizeLimitMap = new Map <String,Color_Size_Limits__c>();
        Map<id,Color_Size_Limits__c> colorSizeLimitMap = getColorSizeLimits(productIds);
        
        // handle no config record found. if no records found, makability passes.
        if(colorSizeLimitMap.keyset().size() == 0){
            for(String r:requests.keyset()){
                MakabilityRestResource.OrderItem oi = requests.get(r);
                MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult(); 
                List<string> errMessages = new List<string>();
                string errMessage = 'Color Size Limit - passed - No records found.';  
                errMessages.add(errMessage);
                result.errorMessages = errMessages;
                result.orderId = oi.orderId;
                result.orderItemId = oi.orderItemId;
                result.isMakable = true;                    
                results.add(result);                
            }            
            return results;
        }
        
        // populate map
        for(id colorSizeLimit:colorSizeLimitMap.keyset()) {
            Color_Size_Limits__c csl = colorSizeLimitMap.get(colorSizeLimit);
            String uniKey = csl.Product_Configuration__r.Product__c + '-' + csl.Interior_Color__c;
            if(!uniKeyToColorSizeLimitMap.containskey(uniKey)){     
               uniKeyToColorSizeLimitMap.put(uniKey,csl);                          
            }            
        }
        
        ////////////////////////////// 
        // run makability check  
        // //////////////////////////
        for(String r:requests.keyset()){
            MakabilityRestResource.OrderItem req = requests.get(r);
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();  
            Boolean makabilityPasses = true;
            id pId = req.productConfiguration.productId;
            ProductConfiguration reqConfig = req.productConfiguration;
            String uniKey = pId + '-' + reqConfig.interiorColor;
            Color_Size_Limits__c currentConfig = uniKeyToColorSizeLimitMap.get(uniKey); 
            List<string> errMessages = new List<string>();
            if(currentConfig != null){            
                if(!MakabilityUtility.compareMaxHeightOrWidth(reqConfig.widthInches,
                                                              reqConfig.widthFractions,
                                                              currentConfig.Maximum_Width__c, 
                                                              currentConfig.Maximum_Width_Fractions__c)){
                                                                    errMessages.add('Color Size Limit - The unit exceeds the Maximum Width of ' +
                                                                                   currentConfig.Maximum_Width__c + ' ' + currentConfig.Maximum_Width_Fractions__c + 
                                                                                   ' inches for the interior color you have selected.' );
                                                                makabilityPasses = false;  
                                                              }

                if (!MakabilityUtility.compareMaxHeightOrWidth(reqConfig.heightInches,
                                                               reqConfig.heightFractions,
                                                               currentConfig.Maximum_Height__c, 
                                                               currentConfig.Maximum_Height_Fractions__c) ){
                                                                       errMessages.add('Color Size Limit - The unit exceeds the Maximum Height of ' +
                                                                                       currentConfig.Maximum_Height__c + ' ' + currentConfig.Maximum_Height_Fractions__c + 
                                                                                       ' inches for the interior color you have selected.');
                                                                makabilityPasses = false;                        
                                                               }
            
            } else{
                // handle no record matched - no record = makability passing
                makabilityPasses = true;                           
            }
            if(makabilityPasses){
                errMessages.add('Color Size Limit - passed');
                result.isMakable = true;
            }else{
                result.isMakable = false;
            }           
            result.orderId = req.orderId;
            result.orderItemId = req.orderItemId;
            result.errorMessages = errMessages;
            results.add(result);            
            
        }
        
        return results;
    } 
    
    /**
    * @description gets color size limit records for makability testing
    * @author mark.rothermal@andersencorp.com | 5/1/2019
    * @param set<id> productIds
    * @return Map<id, Color_Size_Limits__c>
    */
    public static Map<id,Color_Size_Limits__c> getColorSizeLimits(set<id> productIds){
        Map<id,Color_Size_Limits__c> configMap = new Map<id,Color_Size_Limits__c>( [
            SELECT id,
            Interior_Color__c,
            Maximum_Height_Fractions__c,
            Maximum_Height__c,
            Maximum_Width_Fractions__c,
            Maximum_Width__c,
            Product_Configuration__r.Product__c
            from Color_Size_Limits__c 
            where Product_Configuration__r.Product__c in :productids] );  
        return configMap;
    }
}