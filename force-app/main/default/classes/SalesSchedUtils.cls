/**
 * @File Name          : SalesSchedUtils.cls
 * @Description        :
 * @Author             : Demand Chain (James Loghry)
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10-26-2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    1/23/2019, 12:14:56 PM   Demand Chain (James Loghry)     Initial Version
**/
public with sharing class SalesSchedUtils {

    /**
    * @description Returns the sales rep user.  If this current user is a sales rep, then that user is returned.
    * If the sales rep Id is under the role hierarchy of the current user, then that user is returned.
    * If no sales rep is found, then a SalesSchedException is thrown.
    * No capacities are returned in this method, since start and end dates are not specified.
    * @author James Loghry (Demand Chain) | 3/10/2019
    * @param Id  salesRepId - Id of the sales rep to retrieve.
    * @return User - the sales rep user record.
    */
    public static User getSalesRep(Id salesRepId){
        return getUser(UserInfo.getUserId(), null, Date.today(), Date.today().addDays(-1));
    }


    /**
    * @description Returns the sales rep user, with sales capacities between the given dates.
    * If this current user is a sales rep, then that user is returned.
    * If the sales rep Id is under the role hierarchy of the current user, then that user is returned.
    * If no sales rep is found, then a SalesSchedException is thrown.
    * @author James Loghry (Demand Chain) | 3/10/2019
    * @param Id  salesRepId - Id of the sales rep to retrieve.
    * @param startDate - The start date of the sales capacities to retrieve
    * @param endDate - The end date of the sales capacities to retrieve
    * @return User - the sales rep user record.
    */
    public static User getSalesRep(Id salesRepId, Date startDate, Date endDate){
        return String.isEmpty(salesRepId) ?
            getUser(UserInfo.getUserId(), null, startDate, endDate) :
            getUser(salesRepId, UserInfo.getUserRoleId(), startDate, endDate);
    }

    /**
    * @description Returns the sales rep user.
    * @author James Loghry (Demand Chain) | 3/10/2019
    * @param Id  salesRepId - Id of the sales rep to retrieve.
    * @param Id userRoleId - If specified, will throw an exception if the role is invalid.
    * @param startDate - The start date of the sales capacities to retrieve
    * @param endDate - The end date of the sales capacities to retrieve
    * @return User - the sales rep user record.
    */
    public static User getUser(Id userId, Id userRoleId, Date startDate, Date endDate){
        UserRole currentUserRole=[Select Name From UserRole Where Id = :userInfo.getUserRoleId()];
        System.debug('JWL: userRoleId Role: ' + [Select Name From UserRole Where Id = :userRoleId]);
        System.debug('JWL: in get user: ' + userId);
        try{
            User u =
                [Select
                    Id,
                    Name,
                    UserRoleId,
                    Default_Store_Location__c,
                    Store_Locations__c,
                    UserRole.ParentRoleId,
                    UserRole.Name,
                 	(Select
                        Date__c,
                        Store__c,
                        Slot__r.Name,
                        Slot__r.Day__c,
                        Slot__r.Slot_Group__c,
                        Status__c,
                        Store__r.Name
                    From
                        Sales_Capacities__r
                    Where
                        Date__c >= :startDate
                        And Date__c <= :endDate
                    Order By
                        Date__c Asc,
                        Slot__r.Start__c Asc
                    )
                From
                    User
                Where
                    Id = :userId];

            System.debug('JWL: u.ParentRoleId: ' +u.UserRole.ParentRoleId);
            System.debug('JWL: u.ParentRole.Name: ' +[select Name from UserRole where Id=:u.UserRole.ParentRoleId]);
            System.debug('JWL: u.UserRoleId: ' + u.UserRoleId);
            System.debug('JWL: userRoleId (param): ' + userRoleId);
            if(!currentUserRole.Name.endsWith('Manager')) {
                if(userRoleId != null && u.UserRoleId != userRoleId  && u.UserRole.ParentRoleId != userRoleId){
                    throw new AuraHandledException('Invalid Sales Rep or Sales Manager Specified.');
                }
                System.debug('JWL: userId: ' + userId);
                
                System.debug('JWL: current user id: ' + UserInfo.getUserId());
                if(!u.UserRole.Name.endsWith('Manager') && userId != UserInfo.getUserId() && u.UserRole.ParentRoleId != userRoleId){
                    throw new AuraHandledException('Only sales managers may view other rep calendars');
                }
            }
            return u;
        }catch(QueryException qe){
            throw new AuraHandledException('Invalid Sales Rep or Sales Manager Specified.');
        }
    }

    //Helper method to return a wrapper class, which contains a list of options (for a combobox or picklist),
    //Along with the user's default store as the initial selection of the store picklist.
    public static OptionWrapper getStoreOptions(User u, boolean selectDefault){
        OptionWrapper ow = new OptionWrapper();
        if(!String.isEmpty(u.Store_Locations__c)){
            List<String> locations = u.Store_Locations__c.split(';');
            for(Account store : [Select Id,Name From Account Where Name in :locations Order By Name Asc]){
                if(selectDefault && store.Name == u.Default_Store_Location__c){
                    ow.selectedOption = store.Id;
                }
                ow.addOption(store.Id,store.Name);
            }
        }
        return ow;
    }

    //Helper method to return a wrapper class, which contains a list of options (for a combobox or picklist),
    public static OptionWrapper getCapacityStatusOptions(){
        OptionWrapper ow = new OptionWrapper();

        Schema.DescribeFieldResult fieldResult = Sales_Capacity__c.Status__c.getDescribe();
        for(Schema.PicklistEntry ple : fieldResult.getPicklistValues()){
            ow.addOption(ple.getValue(), ple.getLabel());
        }
        return ow;
    }

    public static List<User> getMyTeam(Set<String> stores){
        String storeNamesToInclude = '';
        for(String store : stores){
            storeNamesToInclude += (String.isEmpty(storeNamesToInclude) ? '' : ',') + '\'' + store + '\'';
        }

        Id salesRepProfileId = [Select Sales_Rep_Profile__c From Sales_Schedule_Setting__mdt Limit 1].Sales_Rep_Profile__c;

        String query = 
            'Select ' +
                'Id,' +
                'Name,' +
                'Email,' +
                'Phone,' +
                'MobilePhone,' +
                'SmallPhotoUrl,' +
                'UserRoleId,' +
                'Default_Store_Location__c,' +
                'Store_Locations__c, ' +
            	'ContactID, '+
            	'Contact.Secondary_Email__c, '+
            	'Contact.Email, '+
            	'Contact.Phone, '+
            	'Contact.MobilePhone, '+
            	'Contact.MailingStreet, '+ 
            	'Contact.MailingCity, '+
            	'Contact.MailingState, '+
            	'Contact.MailingCountry, '+
            	'Contact.MailingPostalCode, '+
            	'Contact.Unavailable__c '+
             'From ' +
             	'User ' +
             'Where ' +
             	'IsActive = true ' +
            	'And Contact.Unavailable__c = false ' +
             	'And Store_Locations__c includes (' + storeNamesToInclude + ') ' +
                'And ProfileId = :salesRepProfileId';
        System.debug('JWL: query: ' + query);
        return Database.query(query);
    }

    public static TeamWrapper getMyTeamWithCapacities(List<Date> dates, Set<String> stores){
        TeamWrapper tw = new TeamWrapper();

        Map<Id,User> myTeamMap = new Map<Id,User>(getMyTeam(stores));

        List<Sales_Capacity__c> capacities =
            [Select
             	Id,
             	User__c,
             	User__r.SmallPhotoUrl,
             	User__r.Name,
             	Slot__r.Name,
             	Status__c,
                Store__c,
                Date__c,
             	(Select
                	Id
                 From
                 	Sales_Appointment_Resources__r
                 Limit 1
                )
             From
             	Sales_Capacity__c
             Where
             	User__c in :myTeamMap.keySet()
             	And Date__c >= :dates.get(0)
                And Date__c <= :dates.get(1)
             Order By
            	Date__c Asc,
            	Slot__r.Start__c Asc,
            	Slot__r.End__c Asc];

        System.debug('JWL: users: ' + myTeamMap.values());
        System.debug('JWL: capacities: ' + capacities);
        for(User u : myTeamMap.values()){
            TeamMemberWrapper member = new TeamMemberWrapper(u);
            tw.members.add(member);
            for(Sales_Capacity__c sc : capacities){
                if(u.Id == sc.User__c){
                    member.capacityWrappers.add(new CapacityWrapper(sc));

                    if(!tw.slots.contains(sc.Slot__r.Name)){
                        tw.slots.add(sc.Slot__r.Name);
                    }
                }
            }
        }
        return tw;
    }

    public static TeamWrapper getMyTeamWithCapacities(Date d,Set<String> stores){
        return getMyTeamWithCapacities(new List<Date>{d,d},stores);
    }

    /**
    * @description Gets the current sales date.  This may or may not be equivalent to today's date.
    * The sales date resides in a custom metadata object, and should be set by a nightly batch job that runs.
    * @author ChangeMeIn@UserSettingsUnder.SFDoc | 3/10/2019
    * @param String day - an  optional parameter that coincides with the date format ('YYYY-MM-DD').  If set, a Date object is returned matching that format.
    * @param boolean  throwDateRangeException - a boolean value that dictates if an exception is thrown when the date is outside of the specified range.
    * @return Date
    */
    public static Date getCurrentDate(String day, boolean throwDateRangeException){
        User u = [Select Contact.Account.Active_Store_Configuration__r.Current_Sales_Date__c From User Where Id = :UserInfo.getUserId()];
        Date currentSalesDate = u.Contact.Account.Active_Store_Configuration__r.Current_Sales_Date__c;
        Date d = currentSalesDate;
        if(!String.isEmpty(day)){
            d = Date.valueOf(day);
        }

        //When this method is called, it may throw an invalid range exception (e.g. from the sales manager page)
        //When the exception is thrown, it'll prevent the UI from displaying data from outside of the date range.
        //In other cases, like letting reps view their appointments, they can view any date range.  So then false is passed for the
        //second argument and no exception will be thrown if the date is in the past or several days in the future.
        if(throwDateRangeException){
            Integer dayDiff = currentSalesDate.daysBetween(d);
            if(dayDiff > 4 || dayDiff < 0){
                throw new AuraHandledException('Invalid date specified.');
            }
        }
        System.debug('SalesSchedUtils: getCurrentDate: returning date: ' + d);
        return d;
    }

    /**
    * @description Gets the current sales date.  This may or may not be equivalent to today's date.
    * The sales date resides in a custom metadata object, and should be set by a nightly batch job that runs.
    * This method will throw an exception if the date is before the current sales date or if the date is 5 days from the current sales date.
    * @author James Loghry (Demand Chain)
    * @param String day - an  optional parameter that coincides with the date format ('YYYY-MM-DD').  If set, a Date object is returned matching that format.
    * @return Date
    */
    public static Date getCurrentDate(String day){
        return getCurrentDate(day, false/*true*/);
    }

    public static List<Account> getMyStores(String store, User salesManager){
        System.debug('SalesSchedUtils: GetMyStores: ENTERING');
        System.debug('SalesSchedUtils: GetMyStores: ENTERING');
        System.debug('SalesSchedUtils: GetMyStores: store: ' + store);
        System.debug('SalesSchedUtils: GetMyStores: salesManager: ' + salesManager);
        List<String> storeNames = new List<String>();
        if(!String.isEmpty(salesManager.Store_Locations__c)){
            storeNames = salesManager.Store_Locations__c.split(';');
        }

        if(store != 'All' && storeNames.contains(store)){
            storeNames = new List<String>{store};
        }else if(store != 'All'){
            storeNames = new List<String>{};
        }

        return
            [Select
                Id,
                Name,
                Active_Store_Configuration__r.Distance_Warning__c,
                Active_Store_Configuration__r.Slot_Group__c
             From
                Account Where Name in :storeNames];
    }

    public static List<MyTeamCapacityWrapper> getMyTeamCapacity(String store, User salesManager, Date day){
        System.debug('SalesSchedUtils: GetMyTeamCapacity: ENTERING');
        System.debug('SalesSchedUtils: GetMyTeamCapacity: store: ' + store);
        System.debug('SalesSchedUtils: GetMyTeamCapacity: salesManager: ' + salesManager);
        System.debug('SalesSchedUtils: GetMyTeamCapacity: day: ' + day);
        List<Account> stores = getMyStores(store, salesManager);

        String storeNamesToInclude = '';
        for(Account currentStore : stores){
            storeNamesToInclude += (String.isEmpty(storeNamesToInclude) ? '' : ',') + '\'' + currentStore.Name + '\'';
        }
        System.debug('SalesSchedUtils: GetMyTeamCapacity: StoreNames: ' + storeNamesToInclude);

        Id salesRepProfileId = [Select Sales_Rep_Profile__c From Sales_Schedule_Setting__mdt Limit 1].Sales_Rep_Profile__c;

        String query =
            'Select' +
            ' 	Id,' +
            '   Contact.New_Rep__c,' +
            '   Contact.Unavailable__c,' +
            '   Contact.MailingLongitude,' +
            '   Contact.MailingLatitude,' +
            ' 	SmallPhotoUrl,' +
            ' 	Name, ' +
            '   (Select ' +
            '       CloseRate__c,' +
            '       Events__c,' +
            '       RPA6__c,' +
            '       Orders__c,' +
            '       Orders_Issued__c,' +
            '       Proxy__c,' +
            '       RPA28__c,' +
            '       Utilization__c ' +
            '    From ' +
            '       Sales_Rep_Reports__r ' +
            '    Where ' +
            '       Date__c = :day) ' +
            'From ' +
            ' 	User ' +
            'Where ' +
            '   IsActive = true ' +
            'And Contact.Unavailable__c = false' +
            '   And ProfileId = :salesRepProfileId ' +
            ' 	And Store_Locations__c includes (' + storeNamesToInclude + ')';

        Map<Id,User> userMap = new Map<Id,User>((List<User>)Database.query(query));
        List<MyTeamCapacityWrapper> teamMemberWrappers = new List<MyTeamCapacityWrapper>();
        Map<Id,MyTeamCapacityWrapper> memberMap = new Map<Id,MyTeamCapacityWrapper>();
        for(Sales_Rep_Report__c srr :
            [Select
                User__c
             From
                Sales_Rep_Report__c
             Where
                Date__c = :day
                And User__c in :userMap.keySet()
             Order By
                CloseRate__c Desc,
                RPA28__c Desc,
                RPA6__c Desc]){
            User u = userMap.get(srr.User__c);
            MyTeamCapacityWrapper mtcw = new MyTeamCapacityWrapper(userMap.get(srr.User__c));
            teamMemberWrappers.add(mtcw);
            memberMap.put(u.Id,mtcw);
            userMap.remove(srr.User__c);
        }

        for(User u : userMap.values()){
            MyTeamCapacityWrapper mtcw = new MyTeamCapacityWrapper(u);
            teamMemberWrappers.add(mtcw);
            memberMap.put(u.Id,mtcw);
        }

        System.debug('JWL: day in sales sched utils: ' + day);
        for(Sales_Capacity__c sc :
            [Select
              	Id,
                Slot__r.Name,
              	Status__c,
                Store__c,
                Store__r.Name,
                User__c,
                User__r.Name,
             	User__r.Contact.New_Rep__c,
             	User__r.Contact.Unavailable__c,
                Date__c,
              	(Select
              		Id
                    ,Assignment_Reason__c
                    ,Accepted__c
                    ,Primary__c //SS-168
                    ,Sales_Appointment__r.Sales_Order__r.Contract_Amount_Quoted__c //SS-168
                    ,Sales_Appointment__r.Sales_Order__r.Result__c
                    ,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Id
                    ,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Account.BillingLatitude
                    ,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Account.BillingLongitude
              	    ,Sales_Appointment__r.Sales_Order__r.Opportunity__r.of_Windows__c
                    ,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Units__c
                    ,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Same_Rep_Resit__c
                 	,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Account.ShippingCity	
                 	,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Account.ShippingStateCode
                 	,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Account.ShippingPostalCode
                    ,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Store_Location__r.Name
                 	,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Enabled_Marketing_Source__c
                 	,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Approved_One_Party__c
                 	,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Last_DNS_Rep__c
                 	,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Last_Sale_Rep__c
                 	,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Revisit__c
                 	,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Cancel_Save__c
                    ,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Account.HOA__c
                    ,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Account.Primary_Contact__c
                 	,Sales_Appointment__r.Sales_Order__r.Opportunity__r.Appointment_Type__c
                     ,Status__c
              	 From
              	 	Sales_Appointment_Resources__r
                 Where
                    Sales_Appointment__r.Cancelled__c = false
              	 //Limit 1
              	)
             From
              	Sales_Capacity__c
             Where
                User__c in :memberMap.keySet()
                And Date__c = :day
              Order By
             	Date__c Asc,
             	Slot__r.Order__c Asc]){
            MyTeamCapacityWrapper mtcw = memberMap.get(sc.User__c);
            System.debug('_____mtcw____ratna________'+JSON.serializePretty(mtcw));
            mtcw.addCapacity(sc);
        }
        return teamMemberWrappers;
    }

    public static List<Option> getMyTeamOptions(List<Sales_Capacity__c> capacities){
        List<Option> options = new List<Option>();
        Map<Id,Option> optionMap = new Map<Id,Option>();
        for(Sales_Capacity__c sc : capacities){
            if('Available' == sc.Status__c && !optionMap.containsKey(sc.User__c)){
                Option o = new Option(sc.User__c, sc.User__r.Name);
                options.add(o);
                optionMap.put(sc.User__c,o);
            }
        }
        return options;
    }

    public static String getFormattedDate(Date d){
        String ans = ((Datetime)d).formatGmt('EEEE, MMMM d');
        String suffix = 'th';
        switch on Math.mod(d.day(),10){
            when 1 {
                suffix = 'st';
            }
            when 2 {
                suffix = 'nd';
            }
            when 3 {
                suffix = 'rd';
            }
        }
        return ans + suffix;
    }

    public static String getCommunityUrl(){
        if(Test.isRunningTest()){
            return 'https://sometesturl';
        }
        Id networkId = Network.getNetworkId();
        ConnectApi.Community comm = ConnectApi.Communities.getCommunity(networkId);
        return comm.SiteUrl;
    }

    //Inner class used for obtaining combobox / picklist of store options.
    public class OptionWrapper{
        @AuraEnabled public String selectedOption {get; set;}
        @AuraEnabled public List<Option> options {get; set;}
        public OptionWrapper(){
            this.options = new List<Option>();
        }

        public void addOption(String value, String label){
            this.options.add(new Option(value,label));
        }
    }

    //Inner class used for obtaining combobox / picklist of store options.
    public class Option{
        @AuraEnabled public String value {get; set;}
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public Boolean isAvailable{get;set;}
        @AuraEnabled public Boolean newRep{get;set;}
        @AuraEnabled public Boolean isDeferred{get;set;}  //This variable created by ratna for flagging deferred capacity
        public Option(String value, String label){
            this.value = value;
            this.label = label;
            this.isAvailable=false; 
            this.newRep=false;
            this.isDeferred=false;
        }
        public Option(String value, String label, Boolean isAvailable)
        {
            this.value=value;
            this.label=label;
            this.isAvailable=isAvailable;
            this.newRep=false;
            this.isDeferred=false;
            System.debug('newRep ----->' + this.newRep);
            System.debug('newRep value ----->' + this.value);
        }
        public Option(Sales_Capacity__c sc){
            
            value = sc.Id;
            label = sc.User__r.Name;
            isAvailable=false; 
            newRep= sc.User__r.Contact.New_Rep__c;
            isDeferred = (sc.Status__c == 'Deferred' && sc.Store__c!=null)?true:false;
        }
    }
    

    public class TeamWrapper{
        @AuraEnabled public List<String> slots {get; set;}
        @AuraEnabled public List<TeamMemberWrapper> members {get; set;}

        public TeamWrapper(){
            this.slots = new List<String>();
            this.members = new List<TeamMemberWrapper>();
        }
    }

    public class TeamMemberWrapper{
        @AuraEnabled public User user {get; set;}
        @AuraEnabled public List<CapacityWrapper> capacityWrappers {get; set;}
        public TeamMemberWrapper(User user){
            this.user = user;
            this.capacityWrappers = new List<CapacityWrapper>();
        }
    }

    public class CapacityWrapper{
        @AuraEnabled public Sales_Capacity__c capacity {get; set;}
        @AuraEnabled public boolean assignable {get; set;}

        public CapacityWrapper(Sales_Capacity__c capacity){
            this.capacity = capacity;
            this.assignable = capacity.Sales_Appointment_Resources__r.isEmpty();
        }
    }

    public class MyTeamCapacityWrapper{
        @AuraEnabled public User user {get; set;}
        @AuraEnabled public List<Sales_Capacity__c> capacities  {get; private set;}

        MyTeamCapacityWrapper(User user){
            this.user = user;
            this.capacities = new List<Sales_Capacity__c>();
        }

        public void addCapacity(Sales_Capacity__c sc){
            boolean add = true;
            for(Sales_Capacity__c current : this.capacities){
                add &= (current.Slot__c != sc.Slot__c);
            }

            if(add){
                this.capacities.add(sc);
            }
        }
    }
}