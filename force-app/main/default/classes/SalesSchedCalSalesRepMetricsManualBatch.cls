/**
 * @File Name          : SalesSchedCalSalesRepMetricsManualBatch.cls
 * @Description        : 
 * @Author             : Sundeep Goddety (Three Bridge)
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 11-05-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/1/2019, 1:22:16 PM   Sundeep Goddety (Three Bridge)   Initial Version
**/


public class SalesSchedCalSalesRepMetricsManualBatch implements Database.Batchable<SObject>  {
    
    public List<String> repProfileNames {get; set;}
	public string strQuery = '';
    public Date reportDate {get; set;}
    public set<Id> storeIdsSet {get; set;}
public SalesSchedCalSalesRepMetricsManualBatch(Date reportDate, set<id> storeIds) {
        this.reportDate = reportDate;
        this.repProfileNames = new List<String>{'Partner RMS-Sales'};
        this.storeIdsSet = storeIds;
	}
    
   	/**
	 * @ getting the Users related to the Partner RMS-Sales along with the Store Configuration Call Center Close Time.
	 */
    
     public Database.QueryLocator start(Database.BatchableContext context){
        String query = 
            'SELECT ' +
            	'Id,'+
            	'Name ' +
            'FROM ' +
            	'User ' +
            'WHERE ' +
            	'IsActive = true ' +
            	'And Profile.Name in :repProfileNames ' +
				'And Contact.Account.Active_Store_Configuration__c IN :storeIdsSet '+
				'And Contact.Unavailable__c = false';

        if(Test.isRunningTest()){
            query += ' Limit 100';
        }
        System.debug('JWL: query: ' + query);
		return Database.getQueryLocator(query);
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	public void execute(Database.BatchableContext context, List<User> scope) {
        SalesSchedCalculateSalesRepMetrics.calculateMetrics(reportDate,scope);
	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	public void finish(Database.BatchableContext context) {
	}

}