/**
 * @File Name          : MakabilityCheckColorConfigTest.cls
 * @Description        : 
 * @Author             : Ramakrishna.Manchala@andersencorp.com
 * @Group              : 
 * @Last Modified By   : Ramakrishna.Manchala@andersencorp.com
 * @Last Modified On   : 2/01/2020, 2:12:45 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0      2/01/2020, 2:12:45 PM    Ramakrishna.Manchala@andersencorp.com  Initial Version
**/

@isTest
public class MakabilityCheckGlazingPatternTest {
    @isTest
    public static void passingMakabilityTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestColorConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,'High Performance','Fern','1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGlassPatternConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
            }
    @isTest
    public static void noMatchingProductIdTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestColorConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,'High Performance test','ern','1');
        set<id> productIds = new set<id>();
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGlassPatternConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        
    }
    @isTest
    public static void noMatchingInteriorColorTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestColorConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,'High Performance','rede','1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGlassPatternConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
             
    }   
    @isTest
    public static void noMatchingExteriorColorTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestColorConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,'PURPLE','PURPLE','1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGlassPatternConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
              
    }   
    @isTest
    public static void noMatchingAnythingTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestColorConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = new Map<string,MakabilityRestResource.OrderItem>();
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGlassPatternConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
            
    }        
    

    private static List<Product2> createTestProduct(Integer num){
        List<Product2> pros = new List<Product2>();
        for(Integer i = 0; i < num;i++){
            Product2 p = new Product2();
            p.name = 'test product ' + i;
            pros.add(p);            
        }        
        insert pros;
        return pros;
    }
    
    private static List<Product_Configuration__c> createTestProConfig(Integer num){
        List<Product2> pros = createTestProduct(num);
        List<Product_Configuration__c> pcs = new List<Product_Configuration__c>();
        for(Integer i = 0 ; i < num; i++){
            Product_Configuration__c pc = new Product_Configuration__c();
            pc.name = 'test config ' + i;
            pc.Tempered_Glass_Required_at_UI__c = 100;	
            pc.Product__c = pros[i].id;  
            pcs.add(pc);
        }
        insert pcs;
        return pcs;
    }
    
    private static void createTestColorConfig(Product_Configuration__c pc){
        Glazing_Type_Pattern_Configuration__c gpc = new Glazing_Type_Pattern_Configuration__c();
       gpc.Product_Configuration__c = pc.id;
        gpc.Glazing_Type__c='High Performance';
        gpc.Glazing__c = 'Fern;Reed';
        insert gpc;
    }

    private static Map<String,MakabilityRestResource.OrderItem>  createOrderItem(Id prodId, String exColor,String inColor, String oiId){
        Map<string,MakabilityRestResource.OrderItem> orderItems = new Map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
		ProductConfiguration pc = new ProductConfiguration();
        pc.s1Type = excolor;
        pc.s1Pattern = incolor;  
        pc.s2Type=excolor;
        pc.s2Pattern = incolor;   
        pc.productId = prodId;
        oi.orderItemId = oiId;
        oi.productConfiguration = pc;
        orderItems.put(oiId,oi);       
        return orderItems;
    }

}