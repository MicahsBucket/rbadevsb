/**
 */
/**
 * @class	RSuiteFileHandler
 * @brief	Class for handling RSuite project files that are uploaded to an opportunity
 * @author  Mark Wochnick (Slalom.MAW)
 * 
 * @version	2017/04/11  Slalom.MAW
 * 	Created.
 * @(c)2017 Slalom/Renewal by Andersen.  All Rights Reserved.
 *			Unauthorized use is prohibited.
 */
public with sharing class RSuiteFileHandler {
	public static String projfile = null;
	public static FileUploadResultDTO uploadProjectFile(Id opptyId) {
		System.debug('Opp StageName 1: '+ [SELECT StageName FROM Opportunity WHERE Id =: opptyId]);
		FileUploadResultDTO fr = new FileUploadResultDTO();
		fr.messages = new List<String>();
		List<Attachment> attachList = [select Id, Name, ParentId, Body from Attachment where ParentId = :opptyId];
		RSuiteDTO.SalesAppointment sa = null;
		try {
			
			String savedatafile = null;
	        for(Attachment a : attachList) {
	        	if(a.Name.toLowerCase().endsWith('.rbaproj')) {
	        		// we found the project file
	        		projfile = a.Body.toString();
//	        		System.debug('$$$$$$$$$$$$$$$$$$$: ' + projfile);
	        	} else if (a.Name.toLowerCase().equals('savedata')) {
	        		// we have the file with the close date
	        		savedatafile = a.Body.toString();
//	        		System.debug('$$$$$$$$$$$$$$$$$$$: ' + savedatafile);
	        	}
	        }
			// process the project file
			sa = new RSuiteDTO.SalesAppointment(projFile);
			
			System.debug('####: sa::: ' + sa);
			// clear the order stuff for now - it is not being processed
	        sa.opptyContainer.anOrderContainer = null;
	        // link the records to the needed pricebook and product information
	        sa.linkToSaleforceRecords(opptyId);
	        System.debug('Opp StageName before helper '+ [SELECT StageName FROM Opportunity WHERE Id =: opptyId]);
	        System.debug('##### - sa' + sa);
	        // finally save the appointment
	        RMS_mobileAppRequestHelper helper = new RMS_mobileAppRequestHelper();
	        sa = helper.resultSalesAppointments(UserInfo.getUserId(), sa);
	        System.debug('Opp StageName after helper: '+ [SELECT StageName FROM Opportunity WHERE Id =: opptyId]);
	        System.debug('##### - sa2' + sa);
	        System.debug('##### - sa2 errors: ' + sa.getErrors());
		} catch (Exception e) {
			// we've got an exception
			fr.saveSuccessful = false;
			String message = 'Exception processing file: ' + e.getMessage() + ' stack trace: ' + e.getStackTraceString();
			fr.messages.add(message);
		}
		// process the results
		if (fr.saveSuccessful) {
			if (sa.opptyContainer != null) {
				Id soldQuoteId = null;
				fr.opp = sa.opptyContainer.oppty;
				for (RSuiteDTO.QuoteContainer qc: sa.opptyContainer.quoteContainerList) {
					fr.quotes.add(qc.aQuote);
					if (qc.aQuote.isSold__c) {
						soldQuoteId = qc.aQuote.Id;
					}
				}
				if (soldQuoteId != null && sa.isSaveSuccessful) {
					List<Order> ords = [select Id, Name from Order where OpportunityId = :fr.opp.Id and QuoteId = :soldQuoteId limit 1];
					if (ords.size() > 0) {
					    fr.theOrder = ords[0];
					    fr.orderId = ords[0].Id;
					}
				}
				// get the xml errors
				// errors from opportunity
				fr.messages.addAll(sa.opptyContainer.opptyDTO.errors);
				// errors from Quotes
				for (RSuiteDTO.QuoteContainer qc : sa.opptyContainer.quoteContainerList) {
					fr.messages.addAll(qc.quoteDTO.errors);
					// errrors from Quote lines
					for (XMLFactory.DTO qliDTO : qc.qliDTOs) {
						fr.messages.addAll(qliDTO.errors);
					}
/* enable once discounts are enabled 
					for (XMLFactory.DTO qdDTO : qc.qdDTOs) {
						fr.messages.addAll(qdDTO.errors);
					}
*/
/* enable once financing are enabled 
					for (XMLFactory.DTO qfDTO : qc.qfDTOs) {
						fr.messages.addAll(qfDTO.errors);
					}
*/
				}
				// get the save errors
				if (sa.getErrors().size() > 0) {
					fr.saveSuccessful = false;
					fr.errors.addAll(sa.getErrors());
				}
			} else {
				// we have some other kind of error
				fr.saveSuccessful = false;
				String message = 'Error processing file: No opportunity found in file.';
				fr.messages.add(message);
			}
			
		}
		return fr;
	}

	public class FileUploadResultDTO {
		public Boolean saveSuccessful = true;
		public Id orderId = null;
		public String[] errors = new String[]{};
		public String[] messages = new String[]{};
		public List<Quote> quotes = new List<Quote>();
		public Order theOrder = null;
		public Opportunity opp = null;
	}
}