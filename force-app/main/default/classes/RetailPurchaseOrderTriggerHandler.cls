/**
* @File Name          : RetailPurchaseOrderTriggerHandler.cls
* @Description        : 
* @Author             : mark.rothermal@andersencorp.com
* @Group              : 
* @Last Modified By   : mark.rothermal@andersencorp.com
* @Last Modified On   : 09-08-2020
* @Modification Log   : 
*==============================================================================
* Ver         Date                     Author                    Modification
*==============================================================================
* 1.0    7/29/2019, 1:59:44 PM   mark.rothermal@andersencorp.com     Initial Version
**/
public class RetailPurchaseOrderTriggerHandler {
    
    public static void updateRelatedOrderItems(List<Retail_Purchase_Order__c> rpos){
        Set<Id> orderIds = new Set<Id>();
        List<OrderItem> ois = new List<OrderItem>();
        List<OrderItem> oisToUpdate = new List<OrderItem>();
        Map<String,OrderItem> orderIdToOrderItemListMap = new Map<String,OrderItem>();
        Map<Id, Retail_Purchase_Order__c> orderIdToRetailPurchaseOrderMap = new Map<Id,Retail_Purchase_Order__c>();
        for(Retail_Purchase_Order__c rpo : rpos){
            if(rpo.Order__c != null){
                orderIds.add(rpo.Order__c);
                orderIdToRetailPurchaseOrderMap.put(rpo.Order__c, rpo);
            }
        }   
        Map<Id,OrderItem> oiMap = new Map <ID,OrderItem>([Select Id, 
               OrderId,
               Retail_Purchase_Order__c, 
               Product2.Vendor__r.Name 
               From OrderItem Where OrderId in :orderIds 
               And Retail_Purchase_Order__c = null
               And Verify_Item_Configuration__c = true]);
        for(Id key :oiMap.keySet()){
            OrderItem oi = oiMap.get(key);
            Retail_Purchase_Order__c rpo = orderIdToRetailPurchaseOrderMap.get(oi.OrderId);
            if(oi.Product2.Vendor__r.Name == 'Renewal by Andersen'){
            oi.Retail_Purchase_Order__c = rpo.Id;
            oisToUpdate.add(oi);
            }
        }       
        system.debug('order items updated with retail purchase order ' + oisToUpdate);
        update oisToUpdate;
        
    }
    
    public static void updateOrderStatus(List<Retail_Purchase_Order__c> retailPurchaseOrders, Map<Id,Retail_Purchase_Order__c>oldMap ,Map<Id,Retail_Purchase_Order__c>newMap ){
        List<Order> updateOrders ;
        List<Order> updateOrdersRej;
        Set<Id> orderIdsconf = new Set<Id>();
        Set<Id> orderIds = new Set<Id>();
        
        for(Retail_Purchase_Order__c rop :retailPurchaseOrders){
            
            if(oldMap.get(rop.id).Confirmed_Timestamp__c!=newMap.get(rop.id).Confirmed_Timestamp__c && rop.Confirmed_Timestamp__c!=null)
            {
                
                orderIdsconf.add(rop.Order__c);
            }
            if(oldMap.get(rop.id).Rejection_Timestamp__c!=newMap.get(rop.id).Rejection_Timestamp__c && rop.Rejection_Timestamp__c!=null)
            {
                orderIds.add(rop.Order__c);
                
            }
            
        }
        
        updateOrders =[SELECT id ,Status FROM ORDER WHERE Id IN:orderIdsconf];
        updateOrdersRej=[SELECT id ,Status FROM ORDER WHERE Id IN:orderIds];
        for(Order o : updateOrders){
            o.Status = 'Product Ordered';
            o.Apex_Context__c = true;
        }
        
        for(Order o :updateOrdersRej){
            o.Status = 'In Progress';
            o.Apex_Context__c = true;
        }
        if(updateOrders.size()>0)
        {
        update updateOrders;
        
        }
        
        if(updateOrdersRej.size()>0)
        {
        update updateOrdersRej;
        }
        
        // update updatePurchaseOrders;
    }
    
}