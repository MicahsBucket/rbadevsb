/**
* @author Jason Flippen
* @description Provides test code coverage for OrderCancelledProductListController class.
*/ 
@isTest 
public class OrderCancelledProductListControllerTest {

    /**
    * @author Jason Flippen
    * @description Method to create data to be consumed by test methods.
    * @param N/A
    * @returns N/A
    */ 
    @testSetup static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        Account testVenderAcct = testUtility.createVendorAccount('Test Vendor Account');
        insert testVenderAcct;

        Account testStoreAcct = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAcct = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAcct.Store_Location__c = testStoreAcct.Id;
        insert testDwellingAcct;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAcct.Id, 'Closed - Won');
        insert testOpportunity;
    
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;

        List<Product2> testProductList = new List<Product2>();
        // Parent Product
        Product2 testParentProduct = new Product2(Name = 'Test Parent Product',
                                                  RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Master Product').getRecordTypeId(),
                                                  Vendor__c = testVenderAcct.Id,
                                                  Cost_PO__c = true,
                                                  IsActive = true,
                                                  Account_Number__c =  testFan.Id);
        testProductList.add(testParentProduct);
        // Child Product
        Product2 testChildProduct = new Product2(Name = 'Test Parent Product',
                                                 RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Misc Job and Unit Charges').getRecordTypeId(),
                                                 Vendor__c = testVenderAcct.Id,
                                                 Cost_PO__c = true,
                                                 IsActive = true,
                                                 Account_Number__c =  testFan.Id);
        testProductList.add(testChildProduct);
        insert testProductList;

        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        // Parent PricebookEntry
        PricebookEntry testParentPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testParentProduct.Id);
        testPBEList.add(testParentPBEStandard);
        PricebookEntry testParentPBE = testUtility.createPricebookEntry(testPricebook.Id, testParentProduct.Id);
        testPBEList.add(testParentPBE);
        // Child PricebookEntry
        PricebookEntry testChildPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testChildProduct.Id);
        testPBEList.add(testChildPBEStandard);
        PricebookEntry testChildPBE = testUtility.createPricebookEntry(testPricebook.Id, testChildProduct.Id);
        testPBEList.add(testChildPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order', 
                                     AccountId = testDwellingAcct.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAcct.Id,
                                     OpportunityId = testOpportunity.Id,                                 
                                     Status = 'Draft',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;
        
        OrderItem testOIParent = new OrderItem(OrderId = testOrder.Id,
                                               PricebookentryId = testParentPBEStandard.Id,
                                               Quantity = 2,
                                               UnitPrice = 100,
                                               Status__c = 'Cancelled');
        insert testOIParent;
        OrderItem testOIChild = new OrderItem(OrderId = testOrder.Id,
                                              Parent_Order_Item__c = testOIParent.Id,
                                              PricebookentryId = testChildPBEStandard.Id,
                                              Quantity = 1,
                                              UnitPrice = 200,
                                              Status__c = 'Cancelled');
        insert testOIChild;

    } // End Method: setupData()

    /**
    * @author Jason Flippen
    * @description Method to test the functionality in the Controller.
    * @param N/A
    * @returns N/A
    */ 
    private static testMethod void testController() {

        Test.startTest();

            List<Order> testOrderList = [SELECT Id FROM Order WHERE Name = 'Test Order'];

            // Retrieve the "wrapped" List of Cancelled OrderItem records. There should only be one record.
            List<OrderCancelledProductListController.OrderItemWrapper> orderItemList = OrderCancelledProductListController.getCancelledOrderItems(testOrderList[0].Id);
            System.assertEquals(1,orderItemList.size());

        Test.stopTest();

    } // End Method: testController()

}