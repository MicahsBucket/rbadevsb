global class CanvassLeadSheetUtils {
    // Empty constructor for Extension purposes. Needs to have the pages controller in the constructor arg for the remote action to work...
    public CanvassLeadSheetUtils(CanvassLeadSheetController clsc) {
        
    }
    public static CNVSS_Canvass_Lead_Sheet__c getCanvassLeadSheetById(string leadSheetId) {
        CNVSS_Canvass_Lead_Sheet__c leadSheet = new CNVSS_Canvass_Lead_Sheet__c();
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.CNVSS_Canvass_Lead_Sheet__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        
        String theQuery = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues)
        {
            String theName = s.getDescribe().getName();
            
            // Continue building your dynamic query string
            theQuery += theName + ',';
        }
        
        // Trim last comma
        theQuery = theQuery.subString(0, theQuery.length() - 1);
        
        // Finalize query string
        // also get any canvass unit fields
        theQuery += ', CNVSS_Canvass_Lead_Sheet__c.CNVSS_Canvass_Unit__r.CNVSS_DO_NOT_CONTACT__c FROM CNVSS_Canvass_Lead_Sheet__c WHERE Id = \''+ leadSheetId +'\'';
        
        // Make your dynamic call
        leadSheet = Database.query(theQuery);
        return leadSheet;
    }
    
    @RemoteAction
    global static String saveSignature(String signatureBody, String parentId) {
        try {
            system.debug('Record Id == ' + parentId);
            system.debug(signatureBody);
            Attachment a = new Attachment();
            a.ParentId = parentId;
            a.Body = EncodingUtil.base64Decode(signatureBody);
            a.ContentType = 'image/png';
            a.Name = 'SignatureCapture.png';
            insert a;
            return '{success:true, attachId:' + a.Id + '}';
        } catch(Exception e) {
            return JSON.serialize(e);
        }
        return null;
    }
}