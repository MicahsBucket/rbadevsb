public  class ReadOnlyOrderPageLwcController {

	@AuraEnabled(cacheable = true)
	public static LightningResponse getRelatedOrderItems(id orderId){
		LightningResponse lr = new LightningResponse();
		List<OrderItem> ois = new List<OrderItem>();
		    try{ 
				ois = [SELECT 
				Id 
				FROM OrderItem 
				WHERE OrderId = :orderId
				AND Status__c != 'Cancelled'
				ORDER BY Unit_Id__c];
				lr.jsonResponse = JSON.serialize(ois);
            }catch(Exception ex){
                system.debug('error in get order item ids '+ex.getMessage());
				lr = new LightningResponse(ex);
            }
			System.debug('list of ois  '+ lr.jsonResponse);
			return lr;
	}
}