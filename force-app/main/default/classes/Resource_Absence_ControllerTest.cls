@isTest
public class Resource_Absence_ControllerTest {
    private static final String RESOURCE1_FIRST_NAME = 'Kaylee';
    private static final String RESOURCE1_LAST_NAME = 'Torres';
    private static final String RESOURCE2_FIRST_NAME = 'Harry';
    private static final String RESOURCE2_LAST_NAME = 'Hanson';
    private static final String RESOURCE3_FIRST_NAME = 'Chuck';
    private static final String RESOURCE3_LAST_NAME = 'Johnson';
    private static final String RESOURCE4_FIRST_NAME = 'Mario';
    private static final String RESOURCE4_LAST_NAME = 'Lopez';
    private static final String RESOURCE5_FIRST_NAME = 'AC';
    private static final String RESOURCE5_LAST_NAME = 'Slater';
    private static final String RESOURCE6_FIRST_NAME = 'Richard';
    private static final String RESOURCE6_LAST_NAME = 'Johnson';
    private static final String RESOURCE7_FIRST_NAME = 'Doug';
    private static final String RESOURCE7_LAST_NAME = 'Moss';

    @TestSetup
    static void makeData(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Date newDate = System.today();
        Date endDate = newDate.addDays(5);
        Date startDate = newDate.addDays(-5);

        Store_Configuration__c storeConfig = createStoreConfig();
        insert storeConfig;

        User user1 = createUser(RESOURCE1_FIRST_NAME, RESOURCE1_LAST_NAME);
        User user2 = createUser(RESOURCE2_FIRST_NAME, RESOURCE2_LAST_NAME);
        User user3 = createUser(RESOURCE3_FIRST_NAME, RESOURCE3_LAST_NAME);
        User user4 = createUser(RESOURCE4_FIRST_NAME, RESOURCE4_LAST_NAME);
        User user5 = createUser(RESOURCE5_FIRST_NAME, RESOURCE5_LAST_NAME);
        User user6 = createUser(RESOURCE6_FIRST_NAME, RESOURCE6_LAST_NAME);
        User user7 = createUser(RESOURCE7_FIRST_NAME, RESOURCE7_LAST_NAME);
        insert new List<User> { user1, user2, user3, user4, user5, user6, user7 };

        ServiceResource resource1 = createServiceResource(user1, storeConfig);
        ServiceResource resource2 = createServiceResource(user2, storeConfig);
        ServiceResource resource3 = createServiceResource(user3, storeConfig);
        ServiceResource resource4 = createServiceResource(user4, storeConfig);
        ServiceResource resource5 = createServiceResource(user5, storeConfig);
        ServiceResource resource6 = createServiceResource(user6, storeConfig);
        ServiceResource resource7 = createServiceResource(user7, storeConfig);
        insert new List<ServiceResource> { resource1, resource2, resource3, resource4, resource5, resource6, resource7 };
        
        OperatingHours oh1 = new OperatingHours();
        oh1.Name = 'Test Operating Hours One';
        oh1.TimeZone = 'America/Chicago';

        OperatingHours oh2 = new OperatingHours();
        oh2.Name = 'Test Operating Hours Two';
        oh2.TimeZone = 'America/New_York';

        OperatingHours oh3 = new OperatingHours();
        oh3.Name = 'Test Operating Hours Three';
        oh3.TimeZone = 'America/Los_Angeles';

        OperatingHours oh4 = new OperatingHours();
        oh4.Name = 'Test Operating Hours Four';
        oh4.TimeZone = 'America/Indiana/Indianapolis';

        OperatingHours oh5 = new OperatingHours();
        oh5.Name = 'Test Operating Hours Five';
        oh5.TimeZone = 'America/Denver';
        insert new List<OperatingHours>{oh1, oh2, oh3, oh4, oh5};

        ServiceTerritory st1 = new ServiceTerritory();
        st1.Name = 'Test Service Territory One';
        st1.OperatingHoursId = oh1.Id;
        st1.IsActive = true;
        insert st1;

        ServiceTerritory st2 = new ServiceTerritory();
        st2.Name = 'Test Service Territory Two';
        st2.OperatingHoursId = oh2.Id;
        st2.IsActive = true;
        insert st2;

        ServiceTerritory st3 = new ServiceTerritory();
        st3.Name = 'Test Service Territory Three';
        st3.OperatingHoursId = oh3.Id;
        st3.IsActive = true;
        insert st3;

        ServiceTerritory st4 = new ServiceTerritory();
        st4.Name = 'Test Service Territory Four';
        st4.OperatingHoursId = oh4.Id;
        st4.IsActive = true;
        insert st4;

        ServiceTerritory st5 = new ServiceTerritory();
        st5.Name = 'Test Service Territory Five';
        st5.OperatingHoursId = oh5.Id;
        st5.IsActive = true;
        insert st5;

        ServiceTerritoryMember stm1 = new ServiceTerritoryMember();
        stm1.OperatingHoursId = oh1.Id;
        stm1.ServiceTerritoryId = st1.Id;
        stm1.ServiceResourceId = resource1.Id;
        stm1.EffectiveEndDate = endDate;
        stm1.EffectiveStartDate = startDate;
        

        ServiceTerritoryMember stm2 = new ServiceTerritoryMember();
        stm2.OperatingHoursId = oh1.Id;
        stm2.ServiceTerritoryId = st1.Id;
        stm2.ServiceResourceId = resource2.Id;
        stm2.EffectiveEndDate = endDate;
        stm2.EffectiveStartDate = startDate;

        ServiceTerritoryMember stm3 = new ServiceTerritoryMember();
        stm3.OperatingHoursId = oh2.Id;
        stm3.ServiceTerritoryId = st2.Id;
        stm3.ServiceResourceId = resource3.Id;
        stm3.EffectiveEndDate = endDate;
        stm3.EffectiveStartDate = startDate;

        ServiceTerritoryMember stm4 = new ServiceTerritoryMember();
        stm4.OperatingHoursId = oh2.Id;
        stm4.ServiceTerritoryId = st2.Id;
        stm4.ServiceResourceId = resource4.Id;
        stm4.EffectiveEndDate = endDate;
        stm4.EffectiveStartDate = startDate;

        ServiceTerritoryMember stm5 = new ServiceTerritoryMember();
        stm5.OperatingHoursId = oh3.Id;
        stm5.ServiceTerritoryId = st3.Id;
        stm5.ServiceResourceId = resource5.Id;
        stm5.EffectiveEndDate = endDate;
        stm5.EffectiveStartDate = startDate;

        ServiceTerritoryMember stm6 = new ServiceTerritoryMember();
        stm6.OperatingHoursId = oh4.Id;
        stm6.ServiceTerritoryId = st4.Id;
        stm6.ServiceResourceId = resource6.Id;
        stm6.EffectiveEndDate = endDate;
        stm6.EffectiveStartDate = startDate;

        ServiceTerritoryMember stm7 = new ServiceTerritoryMember();
        stm7.OperatingHoursId = oh5.Id;
        stm7.ServiceTerritoryId = st5.Id;
        stm7.ServiceResourceId = resource7.Id;
        stm7.EffectiveEndDate = endDate;
        stm7.EffectiveStartDate = startDate;
        insert new List<ServiceTerritoryMember>{stm1, stm2, stm3, stm4,stm5, stm6, stm7};       


    }

    @isTest 
    static void testCreateAbsenceRecords() {
        DateTime newDT = System.today();
        DateTime startDt = newDT.addDays(-1);
        DateTime endDt = newDT.addDays(1);

        List<ServiceResource> listOfSRs = [SELECT Id FROM ServiceResource];
        Id stmId = [SELECT Id FROM ServiceTerritoryMember WHERE ServiceResourceId = :listOfSRs[0].Id].Id;

        ResourceAbsence ra1 = new ResourceAbsence();
        ra1.Start = startDt;
        ra1.End = endDt;
        ra1.Type = 'Vacation';
        ra1.ResourceId = listOfSRs[0].Id;
        ra1.Description = 'Vacation RA';
        insert ra1;
        
        Test.startTest();
        Resource_Absence_Controller.getResourceAbsence(ra1.Id);
        System.assertEquals(UserInfo.getTimeZone().getID(),Resource_Absence_Controller.getCurrentLoggedInUserTimeZone());
        Test.stopTest();
    }

        private static User createUser(String firstName, String lastName) {
        User u = new User(
            FirstName = firstName,
            LastName = lastName,
            Email = firstName + '.' + lastName + '@example.com',
            UserName = firstName + '.' + lastName + '@example.com' + (Math.random() * 1000000).intValue(),
            CompanyName = 'ABC Company',
            Alias = 'tuser',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            TimeZoneSidKey = 'America/Chicago',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        return u;
    }

    private static Store_Configuration__c createStoreConfig() {
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008

        );
        insert store;

        Store_Configuration__c storeConfig = new Store_Configuration__c(
            Store__c = store.Id,
            Order_Number__c = 1,
            Tech_Measure_Work_Order_Queue_Id__c = [SELECT Id FROM Group WHERE  Type = 'Queue' AND NAME = 'Tech Measure WO - Twin Cities' LIMIT 1].id
        );

        return storeConfig;
    }

    private static ServiceResource createServiceResource(User u, Store_Configuration__c storeConfig) {
        ServiceResource sr = new ServiceResource(
            Name = u.LastName,
            RelatedRecordId = u.Id,
            Retail_Location__c = storeConfig.Id,
            IsActive = true
        );
        return sr;
    }



    
}