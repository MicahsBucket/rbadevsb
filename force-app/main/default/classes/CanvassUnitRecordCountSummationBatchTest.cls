@isTest(SeeAllData=false)
global class CanvassUnitRecordCountSummationBatchTest {

    public static testMethod void test1()
    {   
        CNVSS_Canvass_Market__c canvassMarket = new CNVSS_Canvass_Market__c();
        canvassMarket.Name = 'Test';
        canvassMarket.CNVSS_Canvass_Market_Manager__c = userInfo.getUserId();
        canvassMarket.CNVSS_ISC_Phone_Number__c = '5555555555';
        insert canvassMarket;
        
        CNVSS_Canvass_Unit__c canvassUnit = new CNVSS_Canvass_Unit__c();
        canvassUnit.CNVSS_Canvass_Market__c = canvassMarket.Id;
        canvassUnit.CNVSS_House_No__c = '5200';
        canvassUnit.CNVSS_Street__c = '77 Center';
        canvassUnit.Street_Type__c = 'Dr';
        canvassUnit.CNVSS_City__c = 'Charlotte';
        canvassUnit.CNVSS_State__c = 'NC';
        canvassUnit.CNVSS_Zip_Code__c = '12345';
        canvassUnit.Fullstreetaddress__c = '5200 77 Center Dr #400, Charlotte, NC 28217';
        canvassUnit.Auto_Created__c = false;
        canvassUnit.Auto_Modified__c = false;
        insert canvassUnit;
        
        Canvass_Market_Batch_Job__c canvassMarketBatchJob = new Canvass_Market_Batch_Job__c();
        canvassMarketBatchJob.User__c = UserInfo.getUserId();
        insert canvassMarketBatchJob;
        
        Custom_Data_Layer_Batch_Queue__c canvassDataLayerQueue = new Custom_Data_Layer_Batch_Queue__c();
        canvassDataLayerQueue.Status__c = 'Waiting For Record Count';
        canvassDataLayerQueue.Field_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids2__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids3__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids4__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids5__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids6__c = '123, 456, 789';
        canvassDataLayerQueue.ZipCode__c = '12345';
        canvassDataLayerQueue.Market_Id__c = canvassMarket.id;
        canvassDataLayerQueue.Canvass_Market_Batch_Job__c = canvassMarketBatchJob.Id;
        canvassDataLayerQueue.New_Record_Count__c = 1;
        canvassDataLayerQueue.Old_Record_Count__c = 1;
        canvassDataLayerQueue.Old_Records_Not_Updated_Count__c = 1;
        canvassDataLayerQueue.Old_Records_Updated_Count__c = 1;
        insert canvassDataLayerQueue;
        
        Test.startTest();
        database.executebatch(new CanvassUnitRecordCountSummationBatch(canvassMarketBatchJob.id),1);
        Test.stopTest();
    }
}