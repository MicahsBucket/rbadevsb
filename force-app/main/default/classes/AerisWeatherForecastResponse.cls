// Code coverage provided by: AerisWeatherForecastHelperTest

public class AerisWeatherForecastResponse {
	public enum ThresholdType {MIN,MAX}

	/************************
	*** CLASS DEFINITION ***
	************************/

	public Boolean success;
	public Error error;
	public List<Response> response;

	public class Response {
		public Loc loc;
		public String interval;
		public List<Periods> periods;
		public ProfileObj profileobj;
	}

	public class Loc {
		public Double longitude;
		public Double latitude;
	}

	// Note: the commented-out values should REMAIN in code, as they are returned by the API and may be enabled in the future.
	// Commenting them out reduces the size of the returned API request and also the size of the parsed JSON object.
	public class Periods {
		public Integer timestamp;
		//public String validTime;
		public String dateTimeISO;
		//public Integer maxTempC;
		public Integer maxTempF;
		//public Integer minTempC;
		public Integer minTempF;
		//public Integer avgTempC;
		public Integer avgTempF;
		//public Integer tempC;
		public Integer tempF;
		public Integer pop;
		//public Double precipMM;
		public Double precipIN;
		//public Integer iceaccum;
		//public Integer iceaccumMM;
		public Integer iceaccumIN;
		public Integer maxHumidity;
		public Integer minHumidity;
		public Integer humidity;
		public Integer uvi;
		//public Integer pressureMB;
		public Double pressureIN;
		public Integer sky;
		//public Integer snowCM;
		public Integer snowIN;
		//public Integer feelslikeC;
		//public Integer feelslikeF;
		//public Integer minFeelslikeC;
		public Integer minFeelslikeF;
		//public Integer maxFeelslikeC;
		public Integer maxFeelslikeF;
		//public Integer avgFeelslikeC;
		public Integer avgFeelslikeF;
		//public Integer dewpointC;
		public Integer dewpointF;
		//public Integer maxDewpointC;
		public Integer maxDewpointF;
		//public Integer minDewpointC;
		public Integer minDewpointF;
		//public Integer avgDewpointC;
		public Integer avgDewpointF;
		public Integer windDirDEG;
		public String windDir;
		//public Integer windDirMaxDEG;
		//public String windDirMax;
		//public Integer windDirMinDEG;
		//public String windDirMin;
		//public Integer windGustKTS;
		//public Integer windGustKPH;
		public Integer windGustMPH;
		//public Integer windSpeedKTS;
		//public Integer windSpeedKPH;
		public Integer windSpeedMPH;
		//public Integer windSpeedMaxKTS;
		//public Integer windSpeedMaxKPH;
		public Integer windSpeedMaxMPH;
		//public Integer windSpeedMinKTS;
		//public Integer windSpeedMinKPH;
		public Integer windSpeedMinMPH;
		//public Integer windDir80mDEG;
		//public String windDir80m;
		//public Integer windDirMax80mDEG;
		//public String windDirMax80m;
		//public Integer windDirMin80mDEG;
		//public String windDirMin80m;
		//public Integer windGust80mKTS;
		//public Integer windGust80mKPH;
		//public Integer windGust80mMPH;
		//public Integer windSpeed80mKTS;
		//public Integer windSpeed80mKPH;
		//public Integer windSpeed80mMPH;
		//public Integer windSpeedMax80mKTS;
		//public Integer windSpeedMax80mKPH;
		//public Integer windSpeedMax80mMPH;
		//public Integer windSpeedMin80mKTS;
		//public Integer windSpeedMin80mKPH;
		//public Integer windSpeedMin80mMPH;
		public String weather;
		public List<WeatherCoded> weatherCoded;
		public String weatherPrimary;
		public String weatherPrimaryCoded;
		public String cloudsCoded;
		public String icon;
		public Boolean isDay;

		// Retrieve values dynamically using variable names (not supported out-of-box by Apex, hence the serialization hack)
		public Object getPropertyByName(String propertyName) {
			String json_instance = JSON.serialize(this);
			Map<String, Object> untyped_instance = (Map<String, Object>)JSON.deserializeUntyped(json_instance);
			return untyped_instance.get(propertyName);
		}
	}

	public class Error {
		public String code;
		public String description;
	}

	public class ProfileObj {
		public String tz;
	}

	public class WeatherCoded {
		public Integer timestamp;
		public String wx;
		public String dateTimeISO;
	}

	/**********************
	*** STATIC METHODS ***
	**********************/

	// Parse and swap out troublesome property names
	public static AerisWeatherForecastResponse parse(String json) {
		json = json.Replace('"lat"','"latitude"');
		json = json.Replace('"long"','"longitude"');
		json = json.Replace('"profile"','"profileobj"');
		return (AerisWeatherForecastResponse) System.JSON.deserialize(json, AerisWeatherForecastResponse.class);
	}

	// Get a list of the non-commented properties in the Periods inner class, to be consumed by AerisWeather API "fields" url parameter (not supported out-of-box by Apex, hence the serialization hack)
	public static String getDelimitedForecastPropertyNames(String stringToPrepend, String delimiter) {
		AerisWeatherForecastResponse instance = new AerisWeatherForecastResponse();
		instance.response = new List<Response>{new Response()};
		instance.response[0].periods = new List<Periods>{new Periods()};
		String json_instance = JSON.serialize(instance.response[0].periods[0]);
		Map<String, Object> untyped_instance = (Map<String, Object>)JSON.deserializeUntyped(json_instance);
		
		String result = '';
		for (String propertyName : untyped_instance.keySet()) {
			result += stringToPrepend + propertyName + delimiter;
		}
		return result.removeEnd(delimiter);
	}

	public static Map<String,ThresholdMapping> getThresholdFieldMap() {
		// Use DescribeFieldResult rather than just the string field name to validate that the fields exist at compile time.
		return new Map<String,ThresholdMapping>{
			'Accumulated_Precipitation_in__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.Accumulated_Precipitation_in__c.getDescribe().getLabel(),'precipIN',ThresholdType.MAX),
			'Wind_Speed_MPH__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.Wind_Speed_MPH__c.getDescribe().getLabel(),'windSpeedMPH',ThresholdType.MAX),
			'Ice_Accumulation_in__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.Ice_Accumulation_in__c.getDescribe().getLabel(),'iceaccumIN',ThresholdType.MAX),
			'Max_Feels_Like_Temp_F__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.Max_Feels_Like_Temp_F__c.getDescribe().getLabel(),'maxFeelslikeF',ThresholdType.MAX),
			'Max_Humidity__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.Max_Humidity__c.getDescribe().getLabel(),'maxHumidity',ThresholdType.MAX),
			'Max_Temp_F__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.Max_Temp_F__c.getDescribe().getLabel(),'maxTempF',ThresholdType.MAX),
			'Max_Wind_Gust_Speed_MPH__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.Max_Wind_Gust_Speed_MPH__c.getDescribe().getLabel(),'windSpeedMaxMPH',ThresholdType.MAX),
			'Min_Feels_Like_Temp_F__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.Min_Feels_Like_Temp_F__c.getDescribe().getLabel(),'minFeelslikeF',ThresholdType.MIN),
			'Min_Humidity__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.Min_Humidity__c.getDescribe().getLabel(),'minHumidity',ThresholdType.MIN),
			'Min_Temp_F__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.Min_Temp_F__c.getDescribe().getLabel(),'minTempF',ThresholdType.MIN),
			'Probability_of_Precipitation__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.Probability_of_Precipitation__c.getDescribe().getLabel(),'pop',ThresholdType.MAX),
			'Snow_Accumulation_in__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.Snow_Accumulation_in__c.getDescribe().getLabel(),'snowIN',ThresholdType.MAX),
			'UV_Index__c' => new ThresholdMapping(Weather_Forecast_Threshold__c.UV_Index__c.getDescribe().getLabel(),'uvi',ThresholdType.MAX)
		};
	}

	/**********************
	*** HELPER CLASSES ***
	**********************/

	public class ThresholdMapping {
		public String fieldLabel;
		public String propertyName;
		public ThresholdType thresholdType;

		public ThresholdMapping(String fieldLabel, String propertyName,ThresholdType thresholdType) {
			this.fieldLabel = fieldLabel;
			this.propertyName = propertyName;
			this.thresholdType = thresholdType;
		}
	}
}