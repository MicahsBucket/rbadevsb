public class CanvassLeadSheetTriggerHandler {
	/*
	 * public static void SendSetAppointmentEmailToISC (List<CNVSS_Canvass_Lead_Sheet__c> leadSheets) {
        try {
            for(CNVSS_Canvass_Lead_Sheet__c leadSheet : leadSheets) {
                if(leadSheet.Canvass_Result__c == 'Set Appointment' || leadSheet.Canvass_Result__c == 'Interested: Call Back')
                sendLeadSheetEmailHTML(leadSheet);
        	}
        }
        catch (Exception e) {
            System.debug(e);
        }
        
    }
	*/
    
    public static void updateCanvassUnitDoNotContact (List<CNVSS_Canvass_Lead_Sheet__c> leadSheets) {
        try {
            // Update the Canvass Unit parent of this leadsheet to do not contact after approval process updates the status field
            // on the Lead Sheet object to 'Approved: DO NOT CONTACT'.
            List<CNVSS_Canvass_Unit__c> listCu = new List<CNVSS_Canvass_unit__c>();
            for(CNVSS_Canvass_Lead_Sheet__c leadSheet : leadSheets) {
                CNVSS_Canvass_Unit__c cu = new CNVSS_Canvass_unit__c();
            	cu = CanvassUnitUtils.getCanvassUnitById(leadSheet.CNVSS_Canvass_Unit__c);
                cu.CNVSS_DO_NOT_CONTACT__c = true;
                listCu.add(cu);
            }
            update listCu;
        }
        catch (Exception e) {
            System.debug(e);
        }
    }
    
    /*
     * private static void sendLeadSheetEmailHTML(CNVSS_Canvass_Lead_Sheet__c cls) {
        // Retrieve the Canvass Unit that this leadsheet is associated to.
        CNVSS_Canvass_Unit__c cu = CanvassUnitUtils.getCanvassUnitById(cls.CNVSS_Canvass_Unit__c);
		       
        // First, reserve email capacity for the current Apex transaction to ensure
        // that we won't exceed our daily email limits when sending email after
        // the current transaction is committed.
        Messaging.reserveSingleEmailCapacity(2);
        
        // Processes and actions involved in the Apex transaction occur next,
        // which conclude with sending a single email.
        
        // Now create a new single email message object
        // that will send out a single email to the addresses in the To, CC & BCC list.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // Strings to hold the email addresses to which you are sending the email.
        String[] toAddresses = new String[] {cu.CNVSS_Canvass_Market__r.CNVSS_ISC_Email__c}; 
        //String[] ccAddresses = new String[] {'smith@gmail.com'};
          
        
        // Assign the addresses for the To and CC lists to the mail object.
        mail.setToAddresses(toAddresses);
        //mail.setCcAddresses(ccAddresses);
        
        // Specify the address used when the recipients reply to the email. 
        mail.setReplyTo(cu.CNVSS_Canvass_Market__r.CNVSS_ISC_Email__c);
        
        // Specify the name used as the display name.
        mail.setSenderDisplayName('RbA Canvassing App');
        
        // Specify the subject line for your email address.
        if(cls.CNVSS_Plan_B__c) {
            mail.setSubject('New Canvassing Lead - Plan B Set Appointment - created on ' + cls.CreatedDate.format('MM/dd/yyyy') + ' by Agent: ' + UserInfo.getName());
        }
        else {
            mail.setSubject('New Canvassing Lead -' + cls.Canvass_Result__c + '- created on ' + cls.CreatedDate.format('MM/dd/yyyy') + ' by Agent: ' + UserInfo.getName());
        }
        
        // Set to True if you want to BCC yourself on the email.
        mail.setBccSender(false);
        
        // Optionally append the salesforce.com email signature to the email.
        // The email address of the user executing the Apex Code will be used.
        mail.setUseSignature(false);
        
        // Build email based on result
        if(cls.Canvass_Result__c == 'Interested: Call Back') {
            // Specify the text content of the email.
            mail.setPlainTextBody('Your LeadSheet: ' + cls.Id +' has been created.');
            
            mail.setHtmlBody(	
                			 'Lead Sheet Created Date:<b> ' + cls.CreatedDate.format('MM/dd/yyyy') + '</b><p>'
                             +
                			 'Address:<b> ' + cu.CNVSS_PK_Full_Address__c + '</b><p>'
                		     +
                             'Call Back Reason:<b> ' + cls.Reason__c + '</b><p>'
                             +
                             'Homeowner 1 Name:<b> ' + cu.CNVSS_Homeowner_First_Name__c + ' ' + cu.CNVSS_Homeowner_Last_Name__c + '</b><p>'
                             +
                             'Is sole Owner:<b> ' + cu.CNVSS_Is_Sole_Owner__c + '</b><p>'
                             +
                             'Homeowner 2 Name:<b> ' + cu.CNVSS_Homeowner_2_First_Name__c + ' ' + cu.CNVSS_Homeowner_2_Last_Name__c + '</b><p>'
                             +
                             'Call Back Date:<b> ' + cls.CNVSS_Call_Back_Date__c.format() + '</b><p>'
                             +
                             'Call Back Time:<b> ' + cls.CNVSS_Best_Time__c + '</b><p>'
                             +
                             'Call Back Phone:<b> ' + cls.CNVSS_Best_Phone__c + '</b><p>'
                            );
        }
        else if (cls.Canvass_Result__c == 'Set Appointment') {
            mail.setPlainTextBody('Your LeadSheet: ' + cls.Id +' has been created.');
        	if(cls.CNVSS_Plan_B__c) {
                mail.setHtmlBody(
                    		 '<b> Please confirm this manually scheduled appointment.</b><p>' 
                       	     +
                			 'Lead Sheet Created Date:<b> ' + cls.CreatedDate.format('MM/dd/yyyy') + '</b><p>' 
                             +
                             'Address:<b> ' + cu.CNVSS_PK_Full_Address__c + '</b><p>'
                             +
                             'Homeowner 1 Name:<b> ' + cu.CNVSS_Homeowner_First_Name__c + ' ' + cu.CNVSS_Homeowner_Last_Name__c + '</b><p>'
                             +
                             'Is sole Owner:<b> ' + cu.CNVSS_Is_Sole_Owner__c + '</b><p>'
                             +
                             'Homeowner 2 Name:<b> ' + cu.CNVSS_Homeowner_2_First_Name__c + ' ' + cu.CNVSS_Homeowner_2_Last_Name__c + '</b><p>'
                    		 +
                    		 'Appointment Date:<b> ' + cls.CNVSS_Appointment_Date__c + '</b><p>'
                    		 +
                    		 'Appointment Time:<b> ' + cls.CNVSS_Appointment_Time__c + '</b><p>'
                            );
            }
            else {
                mail.setHtmlBody(
                                 'Lead Sheet Created Date:<b> ' + cls.CreatedDate.format('MM/dd/yyyy') + '</b><p>' 
                                 +
                                 'Address:<b> ' + cu.CNVSS_PK_Full_Address__c + '</b><p>'
                                 +
                                 'Homeowner 1 Name:<b> ' + cu.CNVSS_Homeowner_First_Name__c + ' ' + cu.CNVSS_Homeowner_Last_Name__c + '</b><p>'
                                 +
                                 'Is sole Owner:<b> ' + cu.CNVSS_Is_Sole_Owner__c + '</b><p>'
                                 +
                                 'Homeowner 2 Name:<b> ' + cu.CNVSS_Homeowner_2_First_Name__c + ' ' + cu.CNVSS_Homeowner_2_Last_Name__c + '</b><p>'
                                );
            }
        }
        
        // Send the email you have created.
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    
    // Not in use atm.
    /*public static void sendLeadSheetEmail(string[] to, string[] cc, string templateApiName, ID targetObjId, ID relatedToId,
                                         ID orgWideEmailId, Boolean saveAsActivity, Attachment[] attachList) 
    {
        Messaging.SingleEmailMessage leadSheetEmail = new Messaging.SingleEmailMessage();
        ID templateId;
        try {
           templateId = [select id, name from EmailTemplate where developername =:templateApiName].id;
        }
        catch (Exception e) {
            ApexPages.addMessages(e);
        }
        
        leadSheetEmail.setToAddresses(to);
        leadSheetEmail.setCcAddresses(cc);
        leadSheetEmail.setTargetObjectId(targetObjId);
        leadSheetEmail.setWhatId(relatedToId);
        leadSheetEmail.setOrgWideEmailAddressId(orgWideEmailId);
        leadSheetEmail.setTemplateId(templateId);
        leadSheetEmail.setSaveAsActivity(saveAsActivity);
        
        System.debug(LoggingLevel.INFO,'** entered sendTemplatedEmail, to:' + to + ' cc:' + cc +  ' templateId:' + templateId + ' tagetObjId:' + targetObjId + 
                    ' whatId:' + relatedToId + ' orgWideEmailId: ' + orgWideEmailId);
    }*/
}