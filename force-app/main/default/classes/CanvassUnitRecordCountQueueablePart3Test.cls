@isTest(SeeAllData=false)
global class CanvassUnitRecordCountQueueablePart3Test {

    public static testMethod void test1()
    {   
        CNVSS_Canvass_Market__c canvassMarket = new CNVSS_Canvass_Market__c();
        canvassMarket.Name = 'Test';
        canvassMarket.CNVSS_Canvass_Market_Manager__c = userInfo.getUserId();
        canvassMarket.CNVSS_ISC_Phone_Number__c = '5555555555';
        insert canvassMarket;
        
        Custom_Data_Layer_Batch_Queue__c canvassDataLayerQueue = new Custom_Data_Layer_Batch_Queue__c();
        canvassDataLayerQueue.Status__c = 'Waiting For Record Count';
        canvassDataLayerQueue.Field_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids2__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids3__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids4__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids5__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids6__c = '123, 456, 789';
        canvassDataLayerQueue.ZipCode__c = '12345';
        canvassDataLayerQueue.Market_Id__c = canvassMarket.id;
        insert canvassDataLayerQueue;
        
        Test.startTest();
        System.enqueueJob(new CanvassUnitRecordCountQueueablePart3(canvassMarket.Id,canvassDataLayerQueue.ZipCode__c,canvassDataLayerQueue.Id));
        list<string> testString = new list<string>();
        testString.add('12345');
        System.enqueueJob(new CanvassUnitRecordCountQueueablePart3(canvassMarket.Id,canvassDataLayerQueue.ZipCode__c,canvassDataLayerQueue.Id,testString));
        Test.stopTest();
    }
}