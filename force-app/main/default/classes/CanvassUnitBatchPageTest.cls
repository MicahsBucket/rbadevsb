@isTest(SeeAllData=false)
global class CanvassUnitBatchPageTest {

    public static testMethod void test1()
    {
        CreateTestData();
        
        Test.startTest();
        CanvassUnitBatchPage.getPickListValues();
        CanvassUnitBatchPage.runBatch('123');
        CanvassUnitBatchPage.runBatch('Select Market');
        Test.stopTest();
    }
    
    private static void CreateTestData()
    {
        CNVSS_Canvass_Market__c canvassMarket = new CNVSS_Canvass_Market__c();
        canvassMarket.Name = 'Test';
        canvassMarket.CNVSS_Canvass_Market_Manager__c = userInfo.getUserId();
        canvassMarket.CNVSS_ISC_Phone_Number__c = '5555555555';
        insert canvassMarket;
    }
}