/**
 * @description       : 
 * @author            : mark.rothermal@andersencorp.com
 * @group             : 
 * @last modified on  : 02-24-2021
 * @last modified by  : mark.rothermal@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                            Modification
 * 1.0   02-24-2021   mark.rothermal@andersencorp.com   Initial Version
**/
@RestResource(urlMapping='/api/v1/makability/*')
global without sharing class MakabilityRestResource {
    
    @HttpPost
    Global static ResponseWrapper doPost(List<OrderRequest> orders ){
        ResponseWrapper res = new ResponseWrapper();     
        ///////////////////variables///////////////////////////////////
        Map<string,OrderItem> uniKeyToOrderItemMap = new map<String,OrderItem>();
        Map<string,MakabilityResult> uniKeyToMakabilityResults = new map<string, MakabilityResult>();
        Map<string,OrderResult> orderIdToOrderResults = new map<string, OrderResult>(); 
        Set<id> productIds = new set<id>();
        List<OrderResult> orderResponses = new List<OrderResult>();
        List<MakabilityResult> allMakability = new List<MakabilityResult> ();
        List<ApexClass> mclasses = MakabilityUtility.fetchMakabilityClasses();
        
        
        //// build maps and sets for makability params - 
        // adding orderId to each item for mapping the responses back later ///
        for(OrderRequest o:orders){
            for(OrderItem oi : o.orderItems){
                oi.orderId = o.orderId;
                oi.makabilityCalculator = MakabilityUtility.validateFields(oi.productConfiguration);
                uniKeyToOrderItemMap.put( oi.orderId + oi.orderItemId, oi);
                productids.add(oi.productConfiguration.productId);
            }
        }
        
        /// run checks /// 
        for(ApexClass m:mclasses){
            try{
              Type t = Type.forName(m.Name); 
              MakabilityService ms = (MakabilityService)t.newInstance();  
              List<MakabilityResult> result = ms.checkCompatibility(uniKeyToOrderItemMap, productIds); 
              allMakability.addAll(result);                
            } catch (exception ex){
                system.debug('exception in makability rest resource '+ex.getStackTraceString());                
                if(String.Isblank(res.errorMessage)){
                    res.errorMessage = 'Makability error in '+m.Name +': ' + ex.getMessage() + ' stack trace:  ' + ex.getStackTraceString();
                } else {
                    res.errorMessage = res.errorMessage + '. Additional Makability error in '+m.Name +': '+ ex.getMessage()  +
                    ' stack trace:  ' + ex.getStackTraceString();
                }            
            }
        }
        /// end checks ///
        
        // build results map
        for (MakabilityResult mr: allMakability){
            string uKey = mr.orderId + mr.orderItemId;
            if(uniKeyToMakabilityResults.containsKey(uKey)){ 
                MakabilityResult result = uniKeyToMakabilityResults.get(uKey);                
                List<String> listToConcat = result.errorMessages;
                listToConcat.addAll(mr.errorMessages);
                if(mr.isMakable == false){
                    result.isMakable = false;                    
                }
                 if(result.isMakable == null){
                    result.isMakable = true;
                }    
                
                result.errorMessages = listToConcat;                        
            } else {
                MakabilityResult result = new MakabilityResult();                
                List<string>errMessages = new list<string>();
                errMessages.addAll(mr.errorMessages);                
                result.orderItemId = mr.orderItemId;
                result.orderId = mr.orderId;
                 if(mr.isMakable == false){
                    result.isMakable = false;                    
                }  
                 if(result.isMakable == null){
                    result.isMakable = true;
                }                  
                
                //////////////////////////////////////////////////////////////////////////////////////           
                result.errorMessages = errMessages;                    
                uniKeyToMakabilityResults.put(uKey, result);
            }               
        } 
        
        
                
        /// verify all makability checks passed - sort results into responses ///               
        res.allpassed = true;
        map<string,List<MakabilityResult>> oIdtoResults = new map<string,List<MakabilityResult>>();
        for(string oi:uniKeyToMakabilityResults.keyset()){
            MakabilityResult result =  uniKeyToMakabilityResults.get(oi); 
            if(oIdtoResults.containsKey(result.orderId)){
                List<MakabilityResult> makabilityResponses = oIdtoResults.get(result.orderId);
                if(result.isMakable == false){
                     res.allPassed = false;                   
                }                              
                makabilityResponses.add(result);                 
            }else {
                List<MakabilityResult> makabilityResponses = new List<MakabilityResult>();    
                if(result.isMakable == false){
                     res.allPassed = false;                   
                }
                makabilityResponses.add(result); 
                oIdtoResults.put(result.orderId,makabilityResponses);
            }
        }
        
        // create response //
        for(string oi:oIdtoResults.keyset()){
            OrderResult ores = new OrderResult();
            List<MakabilityResult> makabilityResponses = oIdtoResults.get(oi);                               
            ores.orderId = oi;
            ores.orderItemResults = makabilityResponses;            
            orderResponses.add(ores);
        }      
        res.orderResults = orderResponses;
        res.resultsSize = orderResponses.size();
        
        system.debug('response in webservice ' + res);
        return res;
        /// end response /// 
    } 
    
    //////////////////////////////////
    // classes created for webservice
    //////////////////////////////////    
    
    
    global class ResponseWrapper{
        public List<OrderResult> orderResults{get;set;}
        public boolean allPassed {get;set;} 
        public integer resultsSize {get;set;}
        public string errorMessage {get;set;}
    }
    // returned object from individual makability checks used to build response.
    global class MakabilityResult {
        public String orderItemId{get;set;}  
        public String orderId{get;set;}
        public Boolean isMakable{get;set;}
        public List<String> errorMessages {get;set;}                
    }
    // used in response
    global class OrderResult {
        public String orderId {get;set;}
        public List<MakabilityResult> orderItemResults {get;set;}
    }
    // request from webservice
    global class OrderRequest{
        public  String orderId {get;set;}
        Public List<OrderItem> orderItems {get;set;}
    }
    // individual units in each order of the request.
    global class OrderItem{
        public String orderId {get;set;}        
        public String orderItemId {get;set;}
        public MakabilityCalculator makabilityCalculator  {get;set;}
        public ProductConfiguration productConfiguration {get;set;}
    }    
    
}