/*
* @author Jason Flippen
* @date 03/11/2020
* @description Test Class for the following Classes:
*              - ReimbursementController
*/ 
@isTest
private class ReimbursementControllerTest {

    //private static OrderItem rwo;
    private static Service_Symptom__c mySS1;    
    
    @testSetup
    public static void setupData() {

        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        List<Account> dwellingsToInsert = new List<Account>();  
        Account dwelling1 = utility.createDwellingAccount('Dwelling Account');
        Account dwelling2 = utility.createDwellingAccount('Dwelling Account 2');
        Account store1 = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
        Store_Configuration__c storeConfig1 = [SELECT id FROM Store_Configuration__c WHERE Store__c = :store1.id ];
        dwelling1.Store_Location__c = store1.Id;
        dwelling2.Store_Location__c = store1.Id; 
        dwellingsToInsert.add(dwelling1);
        dwellingsToInsert.add(dwelling2);
        insert dwellingsToInsert; 
        
        List<Contact> contactsToInsert = new List<Contact>();       
        Contact contact1 = new contact ( FirstName='Contact',LastName='1',AccountId=dwelling1.id);
        contactsToInsert.add(contact1);
        insert contactsToInsert;
        
        List<Financial_Account_Number__c> testFANList = new List<Financial_Account_Number__c>();
        Financial_Account_Number__c financialAccountNumber1 = new Financial_Account_Number__c (Store_Configuration__c = storeConfig1.id, Name = '1');
        testFANList.add(financialAccountNumber1);
        Financial_Account_Number__c financialAccountNumber2 = new Financial_Account_Number__c(Store_Configuration__c = storeConfig1.id, Name = '2');
        testFANList.add(financialAccountNumber2);
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        testFANList.add(testFan);
        insert testFANList;
        
        Financial_Transaction__c financialTransaction1 = new Financial_Transaction__c(Store_Configuration__c = storeConfig1.id,
                                                                                      Transaction_Type__c = 'Inventory Received - External Vendor',
                                                                                      Debit_Account_Number__c = financialAccountNumber1.id,
                                                                                      Credit_Account_Number__c = financialAccountNumber2.id);
        insert financialTransaction1;
        
        Product2 masterProduct = new Product2(name = 'master',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Master_Product'));
        Product2 servProduct = new Product2(name = 'service',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        Product2 servProduct2 = new Product2(name = 'service2',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        
        Product2 testPart = new Product2(Name = 'Test Part',
                                         RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Door_Components').getRecordTypeId(),
                                         Family = 'Parts and Accessories',
                                         Vendor__c = dwelling2.Id,
                                         Cost_PO__c = true,
                                         IsActive = true,
                                         Account_Number__c = testFan.Id,
                                         Reimbursable_Part__c = true);

        Id pricebookId = Test.getStandardPricebookId();
        system.debug(pricebookId);

        List<Product2> products = new List<Product2>{masterProduct,servProduct,servProduct2,testPart};
        insert products;

        PricebookEntry pricebookEntryMaster = utility.createPricebookEntry(pricebookId, masterProduct.id);     
        PricebookEntry pricebookEntryServ = utility.createPricebookEntry(pricebookId, servProduct.id);    
        PricebookEntry pricebookEntryServ2 = utility.createPricebookEntry(pricebookId, servProduct2.id);      
        PricebookEntry testPBEPart = utility.createPricebookEntry(pricebookId, testPart.Id);
        
        List<PriceBookEntry> pbEntries = new List<PricebookEntry>{pricebookEntryMaster,pricebookEntryServ,pricebookEntryServ2,testPBEPart};
        insert pbEntries;
        
        Service_Product__c servProd1 = new Service_Product__c(Service_Product__c = servProduct.Id, Master_Product__c = masterProduct.Id);
        Service_Product__c servProd2 = new Service_Product__c(Service_Product__c = servProduct2.Id, Master_Product__c = masterProduct.Id);
        List<Service_Product__c> sprods = new List<Service_Product__c>{servProd1,servProd2};
        insert sprods;
        
        Problem_Component_List__c probC1 = new Problem_Component_List__c(name='Prob1',Inactive__c=FALSE);
        Problem_Component_List__c probC2 = new Problem_Component_List__c(name='Prob2',Inactive__c=FALSE);
        Problem_Component_List__c probC3 = new Problem_Component_List__c(name='Prob3',Inactive__c=FALSE);
        Problem_Component_List__c probC4 = new Problem_Component_List__c(name='Prob4',Inactive__c=TRUE);
        List<Problem_Component_List__c> probCs = new List<Problem_Component_List__c>{probC1,probC2,probC3,probC4};
        insert probCs;
        
        Service_Symptom_List__c ssl1 = new Service_Symptom_List__c(name='SS1',Inactive__c=FALSE);
        Service_Symptom_List__c ssl2 = new Service_Symptom_List__c(name='SS2',Inactive__c=FALSE);
        Service_Symptom_List__c ssl3 = new Service_Symptom_List__c(name='SS3',Inactive__c=FALSE);
        Service_Symptom_List__c ssl4 = new Service_Symptom_List__c(name='SS4',Inactive__c=TRUE);
        List<Service_Symptom_List__c> ssl = new List<Service_Symptom_List__c>{ssl1,ssl2,ssl3,ssl4};
        insert ssl;
        
        Procedure_Code__c procC1 = new Procedure_Code__c(name='Pr1',Description__c='Description',Time_Minutes_Each__c=15, Inactive__c=FALSE);
        Procedure_Code__c procC2 = new Procedure_Code__c(name='Pr2',Description__c='Description',Time_Minutes_Each__c=15, Inactive__c=FALSE);
        Procedure_Code__c procC3 = new Procedure_Code__c(name='Pr3',Description__c='Description',Time_Minutes_Each__c=15, Inactive__c=FALSE);
        Procedure_Code__c procC4 = new Procedure_Code__c(name='Pr4',Description__c='Description',Time_Minutes_Each__c=15, Inactive__c=TRUE);
        List<Procedure_Code__c> procC = new List<Procedure_Code__c>{procC1,procC2,procC3,procC4};
        insert procC;
        
        Procedure_Code_Assignment__c pca1 = new Procedure_Code_Assignment__c(Service_Product__c=servProduct.Id, Procedure_Code__c = procC1.Id);
        Procedure_Code_Assignment__c pca2 = new Procedure_Code_Assignment__c(Service_Product__c=servProduct.Id, Procedure_Code__c = procC2.Id);
        Procedure_Code_Assignment__c pca3 = new Procedure_Code_Assignment__c(Service_Product__c=servProduct.Id, Procedure_Code__c = procC3.Id);
        Procedure_Code_Assignment__c pca4 = new Procedure_Code_Assignment__c(Service_Product__c=servProduct2.Id, Procedure_Code__c = procC1.Id); 
        Procedure_Code_Assignment__c pca5 = new Procedure_Code_Assignment__c(Service_Product__c=servProduct2.Id, Procedure_Code__c = procC2.Id);        
        Procedure_Code_Assignment__c pca6 = new Procedure_Code_Assignment__c(Service_Product__c=servProduct2.Id, Procedure_Code__c = procC3.Id);
        Procedure_Code_Assignment__c pca7 = new Procedure_Code_Assignment__c(Service_Product__c=servProduct2.Id, Procedure_Code__c = procC4.Id);
        List<Procedure_Code_Assignment__c> pca = new List<Procedure_Code_Assignment__c>{pca1,pca2,pca3,pca4,pca5,pca6,pca7};
        insert pca;
        
        Service_Symptom_Assignment__c ssa1 = new Service_Symptom_Assignment__c(Procedure_Code__c=procC1.Id,Service_Symptom__c=ssl1.Id);
        Service_Symptom_Assignment__c ssa2 = new Service_Symptom_Assignment__c(Procedure_Code__c=procC2.Id,Service_Symptom__c=ssl1.Id);
        Service_Symptom_Assignment__c ssa3 = new Service_Symptom_Assignment__c(Procedure_Code__c=procC3.Id,Service_Symptom__c=ssl1.Id);
        Service_Symptom_Assignment__c ssa4 = new Service_Symptom_Assignment__c(Procedure_Code__c=procC1.Id,Service_Symptom__c=ssl2.Id);
        Service_Symptom_Assignment__c ssa5 = new Service_Symptom_Assignment__c(Procedure_Code__c=procC2.Id,Service_Symptom__c=ssl2.Id);
        Service_Symptom_Assignment__c ssa6 = new Service_Symptom_Assignment__c(Procedure_Code__c=procC3.Id,Service_Symptom__c=ssl2.Id);
        Service_Symptom_Assignment__c ssa7 = new Service_Symptom_Assignment__c(Procedure_Code__c=procC1.Id,Service_Symptom__c=ssl3.Id);
        Service_Symptom_Assignment__c ssa8 = new Service_Symptom_Assignment__c(Procedure_Code__c=procC2.Id,Service_Symptom__c=ssl3.Id);
        Service_Symptom_Assignment__c ssa9 = new Service_Symptom_Assignment__c(Procedure_Code__c=procC3.Id,Service_Symptom__c=ssl3.Id);
        Service_Symptom_Assignment__c ssa10 = new Service_Symptom_Assignment__c(Procedure_Code__c=procC4.Id,Service_Symptom__c=ssl3.Id);
        Service_Symptom_Assignment__c ssa11 = new Service_Symptom_Assignment__c(Procedure_Code__c=procC3.Id,Service_Symptom__c=ssl4.Id);
        List<Service_Symptom_Assignment__c> ssa = new List<Service_Symptom_Assignment__c>{ssa1,ssa2,ssa3,ssa4,ssa5,ssa6,ssa7,ssa8,ssa9,ssa10,ssa11};
        insert ssa;
        
        Problem_Component_Config__c pcc1 = new Problem_Component_Config__c(Problem_Component__c=probC1.Id,Service_Symptom__c=ssl1.Id);
        Problem_Component_Config__c pcc2 = new Problem_Component_Config__c(Problem_Component__c=probC2.Id,Service_Symptom__c=ssl1.Id);
        Problem_Component_Config__c pcc3 = new Problem_Component_Config__c(Problem_Component__c=probC3.Id,Service_Symptom__c=ssl1.Id);
        Problem_Component_Config__c pcc4 = new Problem_Component_Config__c(Problem_Component__c=probC1.Id,Service_Symptom__c=ssl2.Id);
        Problem_Component_Config__c pcc5 = new Problem_Component_Config__c(Problem_Component__c=probC2.Id,Service_Symptom__c=ssl2.Id);
        Problem_Component_Config__c pcc6 = new Problem_Component_Config__c(Problem_Component__c=probC3.Id,Service_Symptom__c=ssl2.Id);
        Problem_Component_Config__c pcc7 = new Problem_Component_Config__c(Problem_Component__c=probC1.Id,Service_Symptom__c=ssl3.Id);
        Problem_Component_Config__c pcc8 = new Problem_Component_Config__c(Problem_Component__c=probC2.Id,Service_Symptom__c=ssl3.Id);
        Problem_Component_Config__c pcc9 = new Problem_Component_Config__c(Problem_Component__c=probC3.Id,Service_Symptom__c=ssl3.Id);
        Problem_Component_Config__c pcc10 = new Problem_Component_Config__c(Problem_Component__c=probC3.Id,Service_Symptom__c=ssl4.Id);
        Problem_Component_Config__c pcc11 = new Problem_Component_Config__c(Problem_Component__c=probC4.Id,Service_Symptom__c=ssl3.Id);
        List<Problem_Component_Config__c> pcc = new List<Problem_Component_Config__c>{pcc1,pcc2,pcc3,pcc4,pcc5,pcc6,pcc7,pcc8,pcc9,pcc10,pcc11};
        insert pcc;
        
        List<Order> testOrderList = new List<Order>();
        Order testOrder01 = new Order(Name='Sold Order 1',
                                      AccountId = dwelling1.id,
                                      BilltoContactId = contact1.id,
                                      EffectiveDate= Date.Today(),
                                      Store_Location__c = store1.Id,
                                      Status ='Draft',
                                      Pricebook2Id = pricebookId);
        testOrderList.add(testOrder01);
        Order testOrder02 = new Order(Name='Sold Order 2',
                                      AccountId = dwelling2.id,
                                      BilltoContactId = contact1.id,
                                      EffectiveDate= Date.Today(),
                                      Store_Location__c = store1.Id,
                                      Status ='Draft',
                                      Sold_Order__c = testOrder01.id,
                                      Pricebook2Id = pricebookId,
                                      recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Service').getRecordTypeId());
        testOrderList.add(testOrder02);
        insert testOrderList;
        
        OrderItem rwo = new OrderItem();
        rwo.OrderId = testOrder02.id;
        rwo.PricebookentryId = pricebookEntryServ.Id;
        rwo.Quantity = 1;
        rwo.UnitPrice = 100;
        insert rwo;
        
        Charge__c charge = new Charge__c(Charge_Cost_To__c = 'Manufacturing',
                                         Category__c = 'Renewal by Andersen',
                                         What_Where__c='Customer Satisfaction',
                                         Service_Product__c=rwo.Id,
                                         Service_Request__c = testOrder02.id);
        insert charge;
        
        Charge__c charge2 = new Charge__c(Charge_Cost_To__c = 'Manufacturing',
                                          Category__c = 'Renewal by Andersen',
                                          Service_Product__c=rwo.Id);
        insert charge2;
        
        mySS1 = new Service_Symptom__c(Service_Product__c=rwo.Id,Service_Symptom__c=ssl1.Id);
        insert mySS1;
        
        Reimbursable_Part__c testReimbursablePart = new Reimbursable_Part__c(Order__c = testOrder02.Id,
                                                                             Order_Product__c = rwo.Id,
                                                                             Part__c = testPart.Id,
                                                                             Quantity__c = 1,
                                                                             Reason__c = 'Broken',
                                                                             Unit_Price__c = 100.00);
        insert testReimbursablePart;
        
    }
    
    public static testmethod void testOptionList() {

        Order testOrder = [SELECT Id FROM Order WHERE Name='Sold Order 2' LIMIT 1];
        Reimbursable_Part__c testReimbursablePart = [SELECT Id,
                                                            Order_Product__c,
                                                            Part__c
                                                     FROM   Reimbursable_Part__c
                                                     WHERE  Order__c = :testOrder.Id];
        Id testOrderProductId = testReimbursablePart.Order_Product__c;
        Id testPartId = testReimbursablePart.Part__c;

        delete testReimbursablePart;

        Test.startTest();

            List<ReimbursementController.ChargeWrapper>wrap =  ReimbursementController.getCharges(testOrder.Id);
            List<String>symptomsIds = new List<String>();
            for(Service_Symptom_List__c sym : ReimbursementController.getAllSymptoms()){
                symptomsIds.add(sym.Id);
                wrap[0].selectedSymptom = sym.Id;
            }
            ReimbursementController.getSymptoms(symptomsIds);
            ReimbursementController.saveServiceSymptoms(JSON.serialize(wrap),symptomsIds);

            List<ReimbursementController.ReimbursablePartWrapper> newReimbursablePartList = new List<ReimbursementController.ReimbursablePartWrapper>();
            ReimbursementController.ReimbursablePartWrapper newReimbursablePart = new ReimbursementController.ReimbursablePartWrapper();
            newReimbursablePart.id = testPartId;
            newReimbursablePart.isNew = true;
            newReimbursablePart.orderProductId = testOrderProductId;
            newReimbursablePart.quantity = 1;
            newReimbursablePart.reason = 'Broken';
            newReimbursablePart.unitPrice = 100.00;
            newReimbursablePartList.add(newReimbursablePart);
            ReimbursementController.saveAll(JSON.serialize(wrap), newReimbursablePartList, testOrder.Id);
            List<Reimbursable_Part__c> reimbursablePartList = [SELECT Id FROM Reimbursable_Part__c WHERE Order__c = :testOrder.Id];
            System.assertEquals(1, reimbursablePartList.size(), 'Unexpected Reimbursable Part List');

            ReimbursementController.getProcedureList(JSON.serialize(wrap));

            ReimbursementController.deleteAllRecords(testOrder.Id, testOrderProductId);
            
        Test.stopTest();
        
    }
    
    /*
    * @author Jason Flippen
    * @date 01/20/2021
    * @description: Method to test the "deleteReimbursablePart" functionality.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testDeleteReimbursablePart() {

        Order testOrder = [SELECT Id FROM Order WHERE Name='Sold Order 2'];
        Reimbursable_Part__c testReimbursablePart = [SELECT Id FROM Reimbursable_Part__c WHERE Order__c = :testOrder.Id];

        Test.startTest();

            ReimbursementController.deleteReimbursablePart(testReimbursablePart.Id);

        Test.stopTest();

        List<Reimbursable_Part__c> reimbursablePartList = [SELECT Id FROM Reimbursable_Part__c WHERE Order__c = :testOrder.Id];
        System.assertEquals(0, reimbursablePartList.size(), 'Unexpected Reimbursable Part List');
    }
    
    /*
    * @author Jason Flippen
    * @date 01/20/2021
    * @description: Method to test the "getReimbursablePartDetail" functionality.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testGetReimbursablePartDetail() {

        Product2 testProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Part'];

        Test.startTest();

            ReimbursementController.ReimbursablePartWrapper reimbursablePart = ReimbursementController.getReimbursablePartDetail(testProduct.Id);
            System.assertEquals('Test Part', reimbursablePart.name, 'Unexpected Reimbursable Part Name');

        Test.stopTest();

    }
    
    /*
    * @author Jason Flippen
    * @date 01/21/2021
    * @description: Method to test the "getReimbursableParts" functionality.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testGetReimbursableParts() {

        Order testOrder = [SELECT Id FROM Order WHERE Name='Sold Order 2'];

        Test.startTest();

            List<ReimbursementController.ReimbursablePartWrapper> reimbursablePartList = ReimbursementController.getReimbursableParts(testOrder.Id);
            System.assertEquals(1, reimbursablePartList.size(), 'Unexpected Reimbursable Part List');

        Test.stopTest();

    }
    
}