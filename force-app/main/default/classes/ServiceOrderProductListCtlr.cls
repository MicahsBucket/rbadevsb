public class ServiceOrderProductListCtlr {

    @AuraEnabled(cacheable=true)
    public static List<OrderWrapper> getOrder(Id orderId) {
        List<OrderWrapper> oWrapperList = new List<OrderWrapper>();
        for (Order o : [SELECT Id,
                               Accountid,
                               EffectiveDate,
                               Installation_Date__c,
                               Job_Close_Date__c,
                               OrderNumber,
                               Service_Type__c,
                               Status,
                               Store_Location__c,
                               Store_Location__r.Name
                        FROM   Order
                        WHERE  Id = :orderId]) {
            
            OrderWrapper oWrapper = new OrderWrapper();
            oWrapper.mainOrderId = o.Id;
            oWrapper.mainAccountId = o.AccountId;
            oWrapper.mainStoreId = o.Store_Location__c;
            oWrapper.mainOrderStatus = o.Status;
            oWrapperList.add(oWrapper);

        }
        System.debug('Order Wrapper List: ' + oWrapperList);
        
        return oWrapperList;
        
    }

    @AuraEnabled(cacheable=true)
    public static List<AssetProductWrapper> getAssetsWithProducts(String orderId) {
        List<AssetProductWrapper> assetsWithProducts = new List<AssetProductWrapper>();
        Order currentOrder = CreateServiceRequestController.getOrder(orderId);
        System.debug('Current Order: ' + currentOrder);
        Map<String,Asset>productIdWithAsset = new Map<String,Asset>();
        Map<Asset,String>assetWithProductId = new Map<Asset,String>();
        List<Asset>allAssets = [SELECT Id, Name, Product_Name__c, Install_Date__c,
                                  Product2Id, Variant_Number__c, Product2.Name, Original_Order_Variant_Number__c, 
                                  Location_Details__c, Original_Order_Product__c, Status, Sold_Order__c,Unit_ID__c
                                  FROM Asset 
                                  WHERE accountId = :currentOrder.accountId
                                  AND Status != 'Replaced'
                                  AND CancelledServiceProducts__c = FALSE
                                  AND Product2.Has_Service_Products__c = TRUE
                                  ORDER BY Name];
        System.debug('All Assets: ' + allAssets);
        for (Asset currentAsset : allAssets) {
            productIdWithAsset.put(currentAsset.Product2Id,currentAsset);
        }
        Map<String,Set<Service_Product__c>>productWithServiceProducts = new Map<String,Set<Service_Product__c>>();
        for(Service_Product__c serviceProd : [SELECT id,Service_Product__c,Service_Product__r.Name,
                                              Master_Product__c FROM Service_Product__c  
                                              WHERE Master_Product__c IN : productIdWithAsset.keyset()]) {
            if (!productWithServiceProducts.containsKey(serviceProd.Master_Product__c)) {
                productWithServiceProducts.put(serviceProd.Master_Product__c,new Set<Service_Product__c>{serviceProd});
            }
            productWithServiceProducts.get(serviceProd.Master_Product__c).add(serviceProd);    
            
        }
        for(Asset childAsset : allAssets) {
            AssetProductWrapper  assetWrap = new AssetProductWrapper();
            assetWrap.assetId =  childAsset.Id;
            assetWrap.assetName =  childAsset.Name;
            assetWrap.unitId = childAsset.Unit_ID__c;
            assetWrap.displayName = childAsset.Name + (String.isNotEmpty(childAsset.Unit_ID__c) ? ' '+'|'+' Unit Id : '+childAsset.Unit_ID__c : '');
            List<ServiceProductWrapper>serviceWrapper = new List<ServiceProductWrapper>(); 
            if (productWithServiceProducts.containsKey(childAsset.Product2Id)) {
                for (Service_Product__c serviceProd : productWithServiceProducts.get(childAsset.Product2Id)) {
                    ServiceProductWrapper serviceProdWrap = new 
                                        ServiceProductWrapper(serviceProd,'','',false,childAsset.Id);
                    serviceWrapper.add(serviceProdWrap);
                }
                assetWrap.serviceProducts =  serviceWrapper;   

            }
            assetsWithProducts.add(assetWrap);
        }
        System.debug('Assets With Products: ' + assetsWithProducts);
        return assetsWithProducts;
    }


    @AuraEnabled(cacheable=true)
    public static List<OrderItemWrapper> getServiceOrderProductList(Id orderId) {
 
        //Integer num_assets;
        //Integer recordCount = 0;
        List<OrderItemWrapper> oiWrapperList = new List<OrderItemWrapper>();

        Id servProdId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId();
        System.debug('Service Product Id: ' + servProdId);
        System.debug('Order Id: ' + orderId);
        System.debug('New System Debug');
        Set<Id> accountIdSet= new Set<Id>();

        for (OrderItem oi : [SELECT Id,
                                    Billable_Amount__c,
                                    Charge_Cost_To__c,
                                    Customer_Pickup__c,
                                    Do_Not_Print_Service_Product_on_Invoice__c,
                                    Do_Not_Print_Service_Product_on_Quote__c,
                                    Installed_Product_Asset__c,
                                    Installed_Product_Asset__r.Name,
                                    Notes__c,
                                    Order.Lock_Service_Request__c,
                                    Order.AccountId,
                                    Order.Store_Number__c,
                                    Order.Store_Location__c,
                                    OrderItemNumber,
                                    Purchase_Order__c,
                                    Purchase_Order__r.Name,
                                    Purchase_Order__r.Status__c,
                                    Product2Id,
                                    Product2.Name,
                                    Product2.Open_In_Visualforce__c,
                                    Quote_Accepted__c,
                                    Quote_Amount__c,
                                    Quanity_Ordered__c,
                                    Reimbursement_Completed__c, 
                                    Sold_Order_Product_Asset__c,
                                    Sold_Order_Product_Asset__r.Name,
                                    Sold_Order_Product_Asset__r.Variant_Number__c,
                                    Status__c,
                                    MTO_Source_Code__c,
                                    Defect_Code__c
                             FROM   OrderItem
                             WHERE  OrderId = :orderId
                             AND    Product2.RecordTypeId =: servProdId
                             AND    Status__c != 'Cancelled'
                             ORDER BY Sold_Order_Product_Asset__r.Name , Product2.Name]) {
            
            OrderItemWrapper oiWrapper = new OrderItemWrapper();
            oiWrapper.id = oi.Id;
            oiWrapper.billableAmount =  oi.Billable_Amount__c;
            oiWrapper.status = oi.Status__c;
            oiWrapper.chargeCostTo = oi.Charge_Cost_To__c;
            oiWrapper.customerPickup = oi.Customer_Pickup__c;
            oiWrapper.doNotPrintServiceProductOnInvoice = oi.Do_Not_Print_Service_Product_on_Invoice__c;
            oiWrapper.doNotPrintServiceProductOnQuote = oi.Do_Not_Print_Service_Product_on_Quote__c;
            oiWrapper.soldOrderProductAssetId = oi.Sold_Order_Product_Asset__c;
            oiWrapper.soldOrderProductAssetName = oi.Sold_Order_Product_Asset__r.Name;
            oiWrapper.lockedServiceRequest = oi.Order.Lock_Service_Request__c;
            oiWrapper.notes = oi.Notes__c;
            oiWrapper.orderId = oi.OrderId;
            oiWrapper.accountId = oi.Order.AccountId;
            oiWrapper.productId = oi.Product2Id;
            oiWrapper.productName = oi.Product2.Name;
            oiWrapper.productOpenInVisualforce = oi.Product2.Open_In_Visualforce__c;
            oiWrapper.purchaseOrder = oi.Purchase_Order__c;
            oiWrapper.purchaseOrderName = oi.Purchase_Order__r.Name;
            oiWrapper.quantityOrdered = oi.Quanity_Ordered__c;
            oiWrapper.quoteAccepted = oi.Quote_Accepted__c;
            oiWrapper.quoteAmount = oi.Quote_Amount__c;
            oiWrapper.reimbursementCompleted = oi.Reimbursement_Completed__c;
            oiWrapper.serviceProductNumber = oi.OrderItemNumber;
            oiWrapper.storeID = oi.Order.Store_Location__c;
            oiWrapper.storeNumber = oi.Order.Store_Number__c;
            oiWrapper.variantNumber = oi.Sold_Order_Product_Asset__r.Variant_Number__c;
            oiWrapper.defectCode = oi.Defect_Code__c;
            oiWrapper.mtoCode = oi.MTO_Source_Code__c;
            if (oiWrapper.id == null) {
                oiWrapper.serviceProductUrl = '';
                oiWrapper.componentUrl = '';
            } else {
                oiWrapper.serviceProductUrl = '/' + oiWrapper.id;
                oiWrapper.componentUrl = '/' + oiWrapper.id;
            }

            if (oiWrapper.purchaseOrder == null) {
                oiWrapper.purchaseOrderUrl = '';
            } else {
                oiWrapper.purchaseOrderUrl = '/' + oiWrapper.purchaseOrder;
            }

            if (oiWrapper.soldOrderProductAssetId == null) {
                oiWrapper.assetUrl = '';
            } else {
            oiWrapper.assetUrl = '/' + oiWrapper.soldOrderProductAssetId;
            }

            /*List<AggregateResult> results2=[select COUNT (Id) 
                                        FROM Asset 
                                        WHERE AccountId = :oiWrapper.accountId];
            oiWrapper.num_assets = (Integer) results2[0].get('expr0');*/

            accountIdSet.add(oi.Order.AccountId);

            oiWrapperList.add(oiWrapper);
            System.debug('Oi Wrapper :' + oiWrapper);
           

        }

        Map<Id, Integer> accountAssetMap= new Map<Id, Integer>();
        List<AggregateResult> results2=[select AccountId, COUNT (Id) 
                                        FROM Asset 
                                        WHERE AccountId IN :accountIdSet
                                        GROUP BY AccountId];
        for(AggregateResult ar: results2){
            String accountId = (String)ar.get('AccountId');
            Integer assetCount = (Integer)ar.get('expr0');
            accountAssetMap.put(accountId, assetCount);
        }

        for(OrderItemWrapper wrapper: oiWrapperList){
            wrapper.num_assets=accountAssetMap.get(wrapper.accountId);
        }


        System.debug('Wrapper List: ' + oiWrapperList);
        return oiWrapperList;

    }

    @AuraEnabled
    public static List<OrderItemWrapper> refreshData(Id orderId) {
        return getServiceOrderProductList(orderId);
    }

    /*
    * @author Jason Flippen
    * @date 10/01/2020
    * @description Method to update the Notes fields on an Order Product (aka OrderItem) record.
    */
    @AuraEnabled
    public static String updateNotes(Id orderItemId, String notes) {

        String result = '';

        try {

            OrderItem updateOI = new OrderItem(Id = orderItemId,
                                               Notes__c = notes);
            update updateOI;

            result = 'Update Notes Success';

        }
        catch (Exception ex) {
            System.debug('***** The following exception occurred in the ServiceOrderProductListCtlr in the updateNotes method:' + ex);
            result = ex.getMessage();
        }

        return result;

    }

    /*
    * @author Jason Flippen
    * @date 09/28/2020
    * @description Method to update the Variant Number on the Asset record.
    */
/*
    @AuraEnabled
    public static String updateVariantNumber(Id assetId, String variantNumber) {

        String result = '';

        try {

            Asset updateAsset = new Asset(Id = assetId,
                                          Variant_Number__c = variantNumber);
            update updateAsset;

            result = 'Update Variant Number Success';

        }
        catch (Exception ex) {
            System.debug('***** The following exception occurred in the ServiceOrderProductListCtlr in the updateVariantNumber method:' + ex);
            result = ex.getMessage();
        }

        return result;

    }
*/

    /*
    * @author Jason Flippen
    * @date 02/24/2021
    * @description Method to update the Order Product (aka OrderItem) record.
    */
    @AuraEnabled
    public static String updateOrderProduct(OrderItem orderProduct) {

        String result = '';

        try {

            update orderProduct;

            result = 'Update Product Success';

        }
        catch (Exception ex) {
            System.debug('***** The following exception occurred in the ServiceOrderProductListCtlr in the updateOrderProduct method:' + ex);
            result = ex.getMessage();
        }

        return result;

    }

    /*
    * @author Jason Flippen
    * @date 10/05/2020
    * @description Method to Cancel an Order Product (aka OrderItem).
    */
    @AuraEnabled
    public static String cancelProduct(OrderItemWrapper orderItem) {

        String result = '';

        try {

            // Delete the List of Warrnty records related to this Order Product.
            List<Warranty__c> deleteWarrantyList = [SELECT Id FROM Warranty__c WHERE Service_Product__c = :orderItem.id];
            System.debug('***** deleteWarrantyList: ' + deleteWarrantyList);
            if (!deleteWarrantyList.isEmpty()) {
                delete deleteWarrantyList;
            }

            // Make sure this Order Product isn't already Cancelled.
            if (orderItem.status != 'Cancelled') {

                // Grab a List of Order Products to be updated (cancelled);
                List<OrderItem> updateOIList = new List<OrderItem>();
                for (OrderItem oi : [SELECT Id,
                                            Cancellation_Reason__c,
                                            DeleteWarranty__c,
                                            Status__c
                                     FROM   OrderItem
                                     WHERE  OrderId = :orderItem.orderId
//                                     AND    Parent_Order_Item__c = :orderItem.id
                                     AND    Id = :orderItem.id
                                     AND    Status__c != 'Cancelled']) {
                    
//                    oi.Cancellation_Reason__c = oi.Cancellation_Reason__c;
                    oi.Status__c = 'Cancelled';
                    oi.DeleteWarranty__c = false;
                    updateOIList.add(oi);
                }
                System.debug('***** updateOIList: ' + updateOIList);

                // If we have Order Products to update, update them.
                if (!updateOIList.isEmpty()) {
                    update updateOIList;
                }
                
            }

            result = 'Cancel Product Success';

        }
        catch (Exception ex) {
            System.debug('***** The following exception occurred in the ServiceOrderProductListCtlr in the cancelProduct method:' + ex);
            result = ex.getMessage();
        }

        return result;

    }

    @AuraEnabled
    public static List<SelectOptionWrapper> getMTOCodes(Id ProductId){
        System.debug('getMTOCodes ProductId: '+ ProductId);
        List<OrderItem> oiList = [SELECT Id, Defect_Code__c, MTO_Source_Code__c, Product_Family__c,  
                        (SELECT Id, What_Where__c, Charge_Cost_To__c, Category__c FROM Charges__r) 
                        FROM OrderItem 
                        WHERE Id =: ProductId];

        System.debug('oiList: '+oiList);
        Set<String> DoorProductFamilySet = new Set<String>();
        for(DoorProductFamilyList__mdt dpf: ([SELECT Id, MasterLabel FROM DoorProductFamilyList__mdt])){
            DoorProductFamilySet.add(dpf.MasterLabel);
        }

        Set<string> productFamilySet = new Set<string>();
        for(OrderItem oi: oiList){
            if(DoorProductFamilySet.contains(oi.Product_Family__c)){
                productFamilySet.add(oi.Product_Family__c);
            }
        }
        System.debug('productFamilySet: '+productFamilySet);
        List<SelectOptionWrapper> MTOoptions = new List<SelectOptionWrapper>();
        if(productFamilySet.size() > 0){
            for(MTO_Source_code__c mto: ([SELECT id,Name,Product_Type__c FROM MTO_Source_code__c WHERE Product_Type__c IN: productFamilySet])){
                SelectOptionWrapper opt = new SelectOptionWrapper(mto.Name,mto.id);
                MTOoptions.add(opt);
            }
        }
        else{
            List<MTO_Source_code__c> mtoSourceCodeList = [SELECT id,Name,Product_Type__c FROM MTO_Source_code__c ];
            System.debug('mtoSourceCodeList: '+mtoSourceCodeList);
            for(MTO_Source_code__c mto: mtoSourceCodeList){
                SelectOptionWrapper opt = new SelectOptionWrapper(mto.Name,mto.id);
                MTOoptions.add(opt);
            }
        }
        return MTOoptions;
        // try {
            
        // } catch (Exception e) {
        //     throw new AuraHandledException(e.getMessage());
        // }
    }

    @AuraEnabled
    public static List<SelectOptionWrapper> getDefectCodes(Id newMTOCode, String newResponsibility){
        List<SelectOptionWrapper> defectCodeOptionsList = new List<SelectOptionWrapper>();
        for(MTOSourceCode_DefectCode__c code: ([SELECT Id, Name, Defect_Code__c, MTO_Source_Code__c, Defect_Code_Name__c, 
                                                    MTO_Source_Code_Name__c, Responsibility__c 
                                                    FROM MTOSourceCode_DefectCode__c 
                                                    WHERE Responsibility__c =: newResponsibility
                                                    AND MTO_Source_Code__c =: newMTOCode])){
            SelectOptionWrapper opt = new SelectOptionWrapper(code.Defect_Code_Name__c,code.Defect_Code__c);
            defectCodeOptionsList.add(opt);
        }
    
        return defectCodeOptionsList;

    }

    @AuraEnabled
    public static string saveEditProduct(String RowId, string Responsibility, Id DefectCode, Id MTOCode){
        System.debug('RowId: '+ RowId);
        System.debug('Responsibility: '+ Responsibility);
        System.debug('DefectCode: '+ DefectCode);
        System.debug('MTOCode: '+ MTOCode);

        Defect_codes__c dc = [SELECT Id, Name FROM Defect_codes__c WHERE Id =: DefectCode];
        MTO_Source_Code__c mtosc = [SELECT Id, Name FROM MTO_Source_Code__c WHERE Id =: MTOCode];

        Id RowIdToCheck = (Id)RowId.remove('"');
        List<SObject> objectsToUpdate = new List<SObject>();
        OrderItem oi = [SELECT Id, OrderId, Defect_Code__c, MTO_Source_Code__c, Product_Family__c, Purchase_Order__c, 
                            (SELECT Id, What_Where__c, Charge_Cost_To__c, Category__c FROM Charges__r) 
                            FROM OrderItem 
                            WHERE Id =: RowIdToCheck ];
                            System.debug('oi: '+ oi);

        oi.Defect_Code__c = dc.Name;
        oi.MTO_Source_Code__c = mtosc.Name;
        System.debug('oi: '+ oi);
        objectsToUpdate.add(oi);
        for(Charge__c c: oi.Charges__r){
            c.Charge_Cost_To__c = Responsibility;
            objectsToUpdate.add(c);
        }

        for(Purchase_Order__c po: ([SELECT Id, Charge_Cost_To__c, (SELECT Id FROM Order_Products__r) FROM Purchase_Order__c WHERE Id =: oi.Purchase_Order__c])){
            po.Charge_Cost_To__c = Responsibility;
            objectsToUpdate.add(po);
        }

        System.debug('objectsToUpdate: '+objectsToUpdate);
        update objectsToUpdate;
        return 'Hi';
    }

    @TestVisible
    public class OrderItemWrapper {

        @AuraEnabled public String id {get;set;}
        @AuraEnabled public String accountId {get;set;}
        @AuraEnabled public Decimal billableAmount {get;set;}
        @AuraEnabled public String chargeCostTo {get;set;}
        @AuraEnabled public String customerPickup {get;set;}
        @AuraEnabled public Boolean doNotPrintServiceProductOnInvoice {get;set;}
        @AuraEnabled public Boolean doNotPrintServiceProductOnQuote {get;set;}
        @AuraEnabled public String soldOrderProductAssetId {get; set;}
        @AuraEnabled public String soldOrderProductAssetName {get; set;}
        @AuraEnabled public Boolean lockedServiceRequest {get;set;}
        @AuraEnabled public String notes {get;set;}
        @AuraEnabled public Decimal num_assets {get;set;}
        @AuraEnabled public String orderId {get;set;}
        @AuraEnabled public String productId {get;set;}
        @AuraEnabled public String productName {get;set;}
        @AuraEnabled public Boolean productOpenInVisualforce {get;set;}
        @AuraEnabled public String purchaseOrder {get;set;}
        @AuraEnabled public String purchaseOrderName {get;set;}
        @AuraEnabled public Decimal quantityOrdered {get;set;}
        @AuraEnabled public Boolean quoteAccepted {get;set;}
        @AuraEnabled public Decimal quoteAmount {get;set;}
        @AuraEnabled public Boolean reimbursementCompleted {get;set;}
        @AuraEnabled public String serviceProductNumber {get;set;}
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public String storeId {get;set;}
        @AuraEnabled public String storeNumber {get;set;}
        @AuraEnabled public String variantNumber {get;set;}

        @AuraEnabled public String serviceProductUrl {get;set;}
        @AuraEnabled public String componentUrl {get;set;}
        @AuraEnabled public String purchaseOrderUrl {get;set;}
        @AuraEnabled public String assetUrl {get;set;}

        @AuraEnabled public String mtoCode {get;set;}
        @AuraEnabled public String defectCode {get;set;}

    }

    @TestVisible
    public class OrderWrapper {

        @AuraEnabled public String mainAccountId {get;set;}
        @AuraEnabled public String mainOrderId {get;set;}
        @AuraEnabled public String mainOrderStatus {get;set;}
        @AuraEnabled public String mainStoreId {get;set;}
        
    }

    public class AssetProductWrapper {

        @TestVisible
        @AuraEnabled public String assetId {get;set;}
        @AuraEnabled public String assetName {get;set;}
        @AuraEnabled public String unitId {get;set;}
        @AuraEnabled public String displayName {get;set;}
        @AuraEnabled public Boolean isChecked {get;set;}
        @AuraEnabled public List<ServiceProductWrapper> serviceProducts {get;set;}
        public AssetProductWrapper(){
            this.serviceProducts = new List<ServiceProductWrapper>();
            this.isChecked = true;
        }
    }


    public class ServiceProductWrapper {
        @AuraEnabled public Service_Product__c product{get;set;}
        @AuraEnabled public String responsibilty {get;set;}
        @AuraEnabled public String description {get;set;}
        @AuraEnabled public String rowIdentifier {get;set;}
        @AuraEnabled public Boolean isSelected {get;set;}

        public ServiceProductWrapper(Service_Product__c product,String responsibilty,String description,Boolean isSelected,String assetId){
            this.product = product; 
            this.isSelected = true;
            this.responsibilty = responsibilty;
            this.description = description;
            this.rowIdentifier = product.Id +'#'+ assetId;
        }
    }

    
    public class SelectOptionWrapper {
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public String value {get; set;}
        public SelectOptionWrapper(String label, String value) {
            this.value = value;
            this.label = label;
        }
       
    }

}