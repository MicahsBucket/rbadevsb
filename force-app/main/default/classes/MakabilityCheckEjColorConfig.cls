/**
 * @File Name          : MakabilityCheckEjColorConfig.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 02-24-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/1/2019, 12:13:37 PM   mark.rothermal@andersencorp.com     Initial Version
**/


Global without sharing class MakabilityCheckEjColorConfig implements MakabilityService {

    /**
    * @description Run makability checks on orderitems. return results.
    * @author mark.rothermal@andersencorp.com | 5/1/2019
    * @param map<string, MakabilityRestResource.OrderItem> requests
    * @param set<id> productIds
    * @return List<MakabilityRestResource.MakabilityResult>
    */
    public static List<MakabilityRestResource.MakabilityResult>  checkCompatibility( map <string,MakabilityRestResource.OrderItem> requests, set<id> productIds){
        List<MakabilityRestResource.MakabilityResult> results = new List<MakabilityRestResource.MakabilityResult>();
        Map<id,EJ_Color_Configuration__c> configMap = getConfigOptions(productIds);
        Map<String,EJ_Color_Configuration__c> uniKeyToConfigMap = new Map<String,EJ_Color_Configuration__c>();
        
        
        // handle no config record found.
        if(configMap.keyset().size() == 0){
            for(String r:requests.keyset()){
                MakabilityRestResource.OrderItem oi = requests.get(r);
                MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult(); 
                List<string> errMessages = new List<string>();
                string errMessage = 'Ej Color Config - Passed - No config records found.';  
                errMessages.add(errMessage);
                result.errorMessages = errMessages;
                result.orderId = oi.orderId;
                result.orderItemId = oi.orderItemId;
                results.add(result);                
            }            
            return results;
        }
                
        // populate config map
        for(id ejcId:configMap.keyset()) {
                EJ_Color_Configuration__c ejc = configMap.get(ejcId);
                String uniKey = ejc.Product_Configuration__r.Product__c + '-' + ejc.EJ_Species__c ;
                uniKeyToConfigMap.put(uniKey,ejc);

        }       
        ////////////////////////////// 
        // run makability check  
        // //////////////////////////        
        for(String r:requests.keyset()){
            MakabilityRestResource.OrderItem req = requests.get(r);                  
            List<string> errMessages = new List<string>();        
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
            result.orderItemId = req.orderItemId;
            result.orderId = req.orderId;
            ProductConfiguration p = req.ProductConfiguration;
            Boolean makabilityPasses = true;
			Boolean runMakability  = req.makabilityCalculator.checkSpecialShape;
            String requestKey = p.productId + '-' + p.ejSpecies ;
            ///////////////////////////////////////////////////////////////////
            // compare request against matching config  
            ///////////////////////////////////////////////////////////////////
            if(runMakability){
                if(uniKeyToConfigMap.containsKey(requestKey)){
                 EJ_Color_Configuration__c currentConfig = uniKeyToConfigMap.get(requestKey);
                 List<String> ejColorList = currentConfig.EJ_Color__c.split(';');
                    if(!ejColorList.contains(p.ejColor)){
                    makabilityPasses = false;
                    errMessages.add('Ej color Config - The EJ color you have selected is not valid for the selected EJ species.');
                    //+ 
                    //'Available colors for this species include:  ' + currentConfig.EJ_Color__c );
                    }
                }                                                     
            }
            if(makabilityPasses){
                errMessages.add('Ej Color Config  - passed');  
                result.isMakable = true;                          
            }else{
                result.isMakable = false;  
            }   
            result.errorMessages = errMessages;                               
            results.add(result);    
        }
        system.debug('results check ' + results );        
        if(results.size() == 0){
            List<string> errMessages = new List<string>();                    
            errMessages.add('Something bad happened with the Ej Color Config');
            MakabilityRestResource.MakabilityResult result = new MakabilityRestResource.MakabilityResult();
            result.errorMessages = errMessages;
            result.isMakable = false;
            results.add(result);
        }         
        return results;
    }
    
    /**
    * @description get ej config records for makability testing
    * @author mark.rothermal@andersencorp.com | 5/1/2019
    * @param set<id> productIds
    * @return Map<id, EJ_Color_Configuration__c>
    */
    public static Map<id,EJ_Color_Configuration__c> getConfigOptions(set<id> productIds){
        Map<id,EJ_Color_Configuration__c> configMap= new Map<id,EJ_Color_Configuration__c>( [
            SELECT id,
            EJ_Color__c,
            EJ_Species__c,
            Product_Configuration__r.Product__c
            from EJ_Color_Configuration__c 
            where Product_Configuration__r.Product__c in :productids] );  
        return configMap;
    }
    

}