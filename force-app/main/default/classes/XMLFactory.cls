/**
 * @class	XMLFactory
 * @brief	Factory class with method to parse rSuite project file XML Node and populate an sObject
 * @author  Mark Wochnick (Slalom.MAW)
 * 
 * @version	2017/04/11  Slalom.MAW
 * 	Created.
 * @(c)2017 Slalom/Renewal by Andersen.  All Rights Reserved.
 *			Unauthorized use is prohibited.
 */
public with sharing class XMLFactory {

	public static Map<String, List<RSuite_Translation__c>> fieldNameToRT = getFieldNameToRT();

	// Check rSuite translations to see if values should be changed before insert
	public static Map<String, List<RSuite_Translation__c>> getFieldNameToRT(){
		Map<String, List<RSuite_Translation__c>> fieldNameToRT = new Map<String, List<RSuite_Translation__c>>();
		for(RSuite_Translation__c rt : [Select RSuite_Value__c, Salesforce_Value__c, 
			Field_API_Name__c, Dependent_Field_API_Name__c
			from RSuite_Translation__c]){

				List<String> fieldnames = new List<String>{rt.Field_API_Name__c};
				if(!String.isEmpty(rt.Dependent_Field_API_Name__c))
					fieldnames.add(rt.Dependent_Field_API_Name__c);
				for(String f : fieldnames){
					if(!fieldNameToRT.containsKey(f))
						fieldNameToRT.put(f, new List<RSuite_Translation__c>());
					fieldNameToRT.get(f).add(rt);
				}

		}
		return fieldNameToRT;
	}
	
	public static Dom.XmlNode[] removeNullNodes(Dom.XmlNode[] nodes) {
		for(Integer j = 0; j < nodes.size(); j++) {
			// check for null nodes - caused by xml files having pretty format instead of being minified
			if (nodes[j].getName() == null && (nodes[j].getText() == null || nodes[j].getText().trim() == '')) {
				// delete the null node
				//System.debug('#########: node should be deleted');
				nodes.remove(j);
				//decrement the counter
				j--;				
			}
		}
		return nodes;
	}

	public static void addNote(sObject sObj, String note){
  	String noteField = 'Notes__c';
  	String currentNote = (String)sObj.get(noteField);
  	if(currentNote == null){
  		sObj.put(noteField, note);
  	} else {
  		sObj.put(noteField, currentNote + ' ' + note);
  	}
  }

	public static DTO populateSObject(Dom.XmlNode node, String sObjectName) {
		System.debug('#####: XMLFactory.populateSObject: start - processing sObject: ' + sObjectName);
		//System.debug('#####: XMLFactory.populateSObject: processing Node: ' + node);
		// get the rSuite field Name translation map
		Map<String, String> fieldNameTranMap = Constants.rSuiteObjectFieldNameMap.get(sObjectName);
		// get the rSuite field Type translation map
		Map<String, String> fieldTypeTranMap = Constants.rSuiteObjectFieldTypeMap.get(sObjectName);
		//setup the DTO and get the sObject and fields dynamically
		DTO aDTO = new DTO();
		SObjectType soType = Schema.getGlobalDescribe().get(sObjectName);
		aDTO.sObj = soType.newSObject();
		Map<String,Schema.SObjectField> soFieldsMap = soType.getDescribe().fields.getMap();
		Dom.XmlNode[] nodeChildren = node.getChildren();
		nodeChildren = XMLFactory.removeNullNodes(nodeChildren);
		
		List<RSuite_Translation__c> fieldRTList;
		String fieldName;

		for (Integer j = 0; j < nodeChildren.size(); j++) {
			if (nodeChildren[j].getName() == Constants.NODE_NAME_KEY) {
				String varName = nodeChildren[j].getText();
				if (varName == 'attributes') {
					// skip the node and the next node
					j++;
				} else {
					// translate the variable name
					String tmpVarName = fieldNameTranMap.get(varName);
					if (tmpVarName != null) {
						varName = tmpVarName;
					}
					SObjectField aField = soFieldsMap.get(varName);
					if(aField != null) {
						// we found the field can we create?
						if (aField.getDescribe().isCreateable()) {
							// set the value
							// get the type - use the custom mapping first if null then use the standard mapping
							String nodeName = fieldTypeTranMap.get(varName);
							if (nodeName == null) {
								nodeName = nodeChildren[j+1].getName();
							}

							try {
								Boolean addError = false;
								if(nodeName == Constants.NODE_NAME_STRING) {
									String valueString = nodeChildren[j+1].getText();
									if(valueString == Constants.NODE_NAME_FALSE)
										aDTO.sObj.put(aField, false);
									else if(valueString == Constants.NODE_NAME_TRUE)
										aDTO.sObj.put(aField, true);
									else
										aDTO.sObj.put(aField, valueString);
								} else if(nodeName == Constants.NODE_NAME_FALSE) {
									aDTO.sObj.put(aField, false);
								} else if(nodeName == Constants.NODE_NAME_TRUE) {
									aDTO.sObj.put(aField, true);
								} else if(nodeName == Constants.NODE_NAME_REAL) {
									Double d = Double.valueOf(nodeChildren[j+1].getText());
									aDTO.sObj.put(aField, d);
								} else if(nodeName == Constants.NODE_NAME_INTEGER) {
									Integer i = Integer.valueOf(nodeChildren[j+1].getText());
									aDTO.sObj.put(aField, i);
								} else {
									aDTO.errors.add(sObjectName + ' field name: ' + varName + ' Unknown XML Type-Field Type=' + nodeName + '|value=' +nodeChildren[j+1].getText());
									addError = true;
									//System.debug('$$$$$: Unknown Node type: ' + nodeName);
								}

								if(!addError){
									fieldName = aField.getDescribe().getName();
									fieldRTList = fieldNameToRT.containsKey(fieldName) ? fieldNameToRT.get(fieldName) : null;
									if(fieldRTList != null && !fieldRTList.isEmpty()){
										for(RSuite_Translation__c rt : fieldRTList){
											Boolean doUpdate = false;
											// Check if there is a dependency for this field to translate
											if(!String.isEmpty(rt.Dependent_Field_API_Name__c)){

												// IF Depedent field is found AFTER the field we are checking was set
												if(rt.Dependent_Field_API_Name__c == fieldName
													&& aDTO.sObj.get(rt.Field_API_Name__c) != null){
													if(aDTO.sObj.get(rt.Dependent_Field_API_Name__c) == 
														rt.RSuite_Value__c){
														SObjectField theField = soFieldsMap.get(rt.Field_API_Name__c);
														aDTO.sObj.put(theField, rt.Salesforce_Value__c);
													}
												} else if(rt.Dependent_Field_API_Name__c != fieldName 
													&& aDTO.sObj.get(rt.Dependent_Field_API_Name__c) != null) {
													// The dependent field has been set, and it
													// matches the rSuite value, make this translation
													doUpdate = aDTO.sObj.get(rt.Dependent_Field_API_Name__c) == 
														rt.RSuite_Value__c;
												}
											} else {
												doUpdate = rt.RSuite_Value__c == String.valueOf(aDTO.sObj.get(aField));
											}
											if(doUpdate){
												aDTO.sObj.put(aField, rt.Salesforce_Value__c);
											}
										}
									}
								}

							} catch (Exception e) {
								aDTO.errors.add(sObjectName + ' field name: ' + varName + ' Assignment Exception-Field Type=' + nodeName + '|value=' +nodeChildren[j+1].getText() + '|exception=' + e);
								//System.debug('$$$$$: Exception assigning value to QLI: type::' + nodeName + ' ::value::' + nodeChildren[j+1].getText() + ' ::exception '+ e);
							}


						} else {
							if (varName == 'isDeleted') {
								if (nodeChildren[j+1].getName() == Constants.NODE_NAME_TRUE) {
									// this object is in a deleted state stop processing
									aDTO.sObj = null;
									aDTO = null;
									System.debug('#####: XMLFactory.populateSObject: record is set to delete end processing sObject(' + sObjectName + ')');
									return null;
								}
							} else {
								aDTO.readOnlyFieldMap.put(varName, sObjectName + ' field is read only-Field Type=' + nodeChildren[j+1].getName() + '|value=' +nodeChildren[j+1].getText());
								//System.debug('$$$$$: variable not updateable ---Name = ' + varName + '::nextNodeName = ' + nodeChildren[j+1].getName() + '::' + nodeChildren[j+1].getText());
							}						
						}
					} else {
						aDTO.errors.add('Field with Name: '+ varName +' is present within in the .rpaProj file but no such field exists on '+sObjectName);
						//System.debug('$$$$$: variable not found ---Name = ' + varName);
					}
				}
				j++;
			}
		}
		System.debug('#####: XMLFactory.populateSObject: created sObject(' + sObjectName + ') = ' + aDTO.sObj);
		return aDTO;
	}
	
	public class DTO {
		public DTO() {
			readOnlyFieldMap = new Map<String, String>();
			errors = new List<String>();
			sObj = null;
		}
		public sObject sObj;
		public Map<String, String> readOnlyFieldMap;
		public List<String> errors;
	}

    
}