/*
* @author Jason Flippen
* @date 11/13/2020
* @description Test Class for the following Classes:
*              - HomeComponentTechScheduleController
*/ 
@isTest
public class HomeComponentTechScheduleControllerTest {

    private static Account storeAccount;
    private static Order ord;
    private static WorkOrder measureWO;
    private static final String INSTALL_WORK_TYPE_NAME = 'Install';
    private static final String MEASURE_WORK_TYPE_NAME = 'Measure';
    private static final String SERVICE_WORK_TYPE_NAME = 'Service';
    private static final String JOB_SITE_VISIT_WORK_TYPE_NAME = 'Job Site Visit';
    
    @TestSetup static void setup(){

        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        //FSLRollupRolldownController.disableFSLRollupRolldownController = true;
        
        storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
        Database.insert(storeAccount, true);
        Id storeAccountId = storeAccount.Id;
        Id storeConfigId = storeAccount.Active_Store_Configuration__c;
        Id accountId = TestDataFactoryStatic.createAccount('Calvins', storeAccountId).Id;
        Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'CalvinPrime');
        Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('Calvins Opportunity', accountId, storeAccountId, 'Sold', date.today());
        OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
        ord = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        
        OperatingHours oh1 = new OperatingHours();
        oh1.Name = 'Test Operating Hours One';
        oh1.TimeZone = 'America/Chicago';
        insert oh1;
        
        ServiceTerritory st1 = new ServiceTerritory();
        st1.Name = 'Test Service Territory One';
        st1.OperatingHoursId = oh1.Id;
        st1.IsActive = true;
        insert st1;
        
        WorkType installWorkType = new WorkType(
            Name = INSTALL_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType measureWorkType = new WorkType(
            Name = MEASURE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType serviceWorkType = new WorkType(
            Name = SERVICE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType jobSiteVisitWorkType = new WorkType(
            Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 1,
            DurationType = 'Hours'
        );
        insert new List<WorkType>{installWorkType, measureWorkType, serviceWorkType, jobSiteVisitWorkType};

        measureWO = new WorkOrder(Recommended_Crew_Size__c = 2,
                                  Street = '219 North Milwaukee Street',
                                  City = 'Milwaukee',
                                  State = 'Wisconsin',
                                  PostalCode = '53208',
                                  CountryCode = 'US',
                                  WorkTypeId = measureWorkType.Id,
                                  ServiceTerritoryId = st1.Id,
                                  Status = 'Scheduled & Assigned',
                                  RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Tech_Measure').getRecordTypeId());

        WorkOrder measureWO1 = new WorkOrder(Recommended_Crew_Size__c = 2,
                                             Street = '219 North Milwaukee Street',
                                             City = 'Milwaukee',
                                             State = 'Wisconsin',
                                             PostalCode = '53208',
                                             CountryCode = 'US',
                                             WorkTypeId = serviceWorkType.Id,
                                             ServiceTerritoryId = st1.Id,
                                             Status = 'To be Scheduled' ,
                                             RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Tech_Measure').getRecordTypeId());
        insert new List<WorkOrder>{measureWO,measureWO1};
        
        WorkOrderLineItem woli = new WorkOrderLineItem(WorkOrderId = measureWO.Id);
        insert woli;
        
    }
    
    static testMethod void testGetWorkOrderLineItems() {

        Set<String> eligibleStatusSet = new Set<String>{'Scheduled & Assigned','To be Scheduled'};
        Map<String,String> workOrderStatusMap = new Map<String,String>();
        for (WorkOrder wo : [SELECT Status, WorkOrderNumber FROM WorkOrder WHERE Status IN :eligibleStatusSet]) {
            workOrderStatusMap.put(wo.Status,wo.WorkOrderNumber);
        }

        Test.startTest();

            HomeComponentTechScheduleController.getOpportunities('Calvins');
            HomeComponentTechScheduleController.getAssignedWorkOrders(workOrderStatusMap.get('Scheduled & Assigned'));
            HomeComponentTechScheduleController.getToBeScheduledWorkOrders(workOrderStatusMap.get('To be Scheduled'));

        Test.stopTest();

    }

}