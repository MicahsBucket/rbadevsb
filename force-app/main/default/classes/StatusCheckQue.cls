/**
 * @author Micah Johnson - Demand Chain 2020 
 * @description This is a Queueable that delays the processing of the StatusCheckUtility (as applicable)
 * @param The parameters passed in are Old and New Maps of the specific sObject to check, as well as a string refence to the Type of sObject
 * @returns The Que will not return any data, but rather update the record's Staus at the end of the Queueable process
 **/


public class StatusCheckQue implements Queueable{
    public map<Id, sObject> nMap;
    public map<Id, sObject> oMap;
    public string oType;

    public StatusCheckQue(map<Id, sObject> newMap, map<Id, sObject> oldMap, string type) {
        this.nMap = newMap;
        this.oMap = oldMap;
        this.oType = type;
        System.debug('nMap: ' + nMap);
        System.debug('oMap: ' + oMap);
        System.debug('oType: ' + oType);
    }

    public void execute(QueueableContext context) {
        // StatusCheckUtility.StatusCheckUtility(objcts);
        if(oType == 'ServiceAppointment'){
            StatusCheckUtility.ServiceAppointmentToWorkOrderStatusCheck((map<Id, ServiceAppointment>)nMap, (map<Id, ServiceAppointment>)oMap);
            // DynamicRollUpUtility.DynamicRollUp(nMap.values(), true);
        }
        if(oType == 'TechMeasureWO'){
            StatusCheckUtility.techMeasureWorkOrderToOrderStatusCheck((Map<Id, WorkOrder>)nMap, (Map<Id, WorkOrder>)oMap);
        }
        if(oType == 'PurchaseOrder'){
            StatusCheckUtility.PurchaseOrderToOrderStatusCheck((Map<Id, Purchase_Order__c>)nMap, (Map<Id, Purchase_Order__c>)oMap);
        }
        if(oType == 'InstallAppointment'){
            StatusCheckUtility.InstallAppointmentToWorkOrderStatusCheck((map<Id, ServiceAppointment>)nMap, (map<Id, ServiceAppointment>)oMap);
            // DynamicRollUpUtility.DynamicRollUp(nMap.values(), true);
        }
        if(oType == 'InstallWO'){
            StatusCheckUtility.installWorkOrderToOrderStatusCheck((Map<Id, WorkOrder>)nMap, (Map<Id, WorkOrder>)oMap);
        }
        if(oType == 'TechMeasureOrderUpdate'){
            StatusCheckUtility.techMeasureStatusOnOrderUpdate((Map<Id, Order>)nMap, (Map<Id, Order>)oMap);
        }
        if(oType == 'ServiceTypeAppointment'){
            StatusCheckUtility.ServiceTypeAppointmentToWorkOrderStatusCheck((map<Id, ServiceAppointment>)nMap, (map<Id, ServiceAppointment>)oMap);
        }
        if(oType == 'JobSiteAppointment'){
            StatusCheckUtility.JobSiteTypeAppointmentToWorkOrderStatusCheck((map<Id, ServiceAppointment>)nMap, (map<Id, ServiceAppointment>)oMap);
        }
        if(oType == 'ServiceTypeWO'){
            StatusCheckUtility.updateServiceAppointmentfromWorkOrder((List<WorkOrder>)nMap.values());
        }
        if(oType == 'JobSiteWO'){
            StatusCheckUtility.updateServiceAppointmentfromWorkOrder((List<WorkOrder>)nMap.values());
        }
    }
}