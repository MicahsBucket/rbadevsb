@isTest
public class MakabilityValidatorControllerTest {
    // need to create product, order, orderItem , product config, color config, screen config, glaze config and some how link them all together. 
    // borrowed heavily and modified from collapable content unit test class. note, 
    // unable to do asserts due to system debugs from triggers consuming entire log.
    @isTest
    public static void getAllOrderItemsApexTest(){
        TestDataFactoryStatic.setUpConfigs();
        Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
        Database.insert(storeAccount, true);
        Id storeAccountId = storeAccount.Id;
        Id storeConfigId = storeAccount.Active_Store_Configuration__c;
        Id accountId = TestDataFactoryStatic.createAccount('RbaTest', storeAccountId).Id;
        Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'RbaTestContact');
        Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('RbaTest Opportunity', accountId, storeAccountId, 'Sold', date.today());
        System.debug('opportunity.Store_Location__c: ' + opportunity.Store_Location__c);
        OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
        Order ord = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        ord.Order_Processed_Date__c = date.today();
        List<Order> ordList = new List<Order>{ord};
        Database.update(ordList);
        
        test.startTest();
        LightningResponse results = MakabilityValidatorController.getAllOrderItemsApex(ordList[0].Id);
        test.stopTest();
        
        system.debug(results);
    }
    
    
    @isTest
    public static void makabilityOrderItemTest(){
        TestDataFactoryStatic.setUpConfigs();
        Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
        Database.insert(storeAccount, true);
        Id storeAccountId = storeAccount.Id;
        Id storeConfigId = storeAccount.Active_Store_Configuration__c;
        Id accountId = TestDataFactoryStatic.createAccount('RbaTest', storeAccountId).Id;
        Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'RbaTestContact');
        Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('RbaTest Opportunity', accountId, storeAccountId, 'Sold', date.today());
        System.debug('opportunity.Store_Location__c: ' + opportunity.Store_Location__c);
        OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
        Order ord = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        ord.Order_Processed_Date__c = date.today();
        List<Order> ordList = new List<Order>{ord};
        Database.update(ordList);        
        List<OrderItem> ois = createOrderItems(1,ordList[0]);
        List<Product_Configuration__c> pcs = updateTestProConfig();
 		createTestColorConfig(pcs[0]);       
 		createTestScreenConfig(pcs[0]);       
 		createTestGlazingConfig(pcs[0]);       
		createTestGrilleConfig(pcs[0]);  
        createTestSpecialtyShapeGrilleConfig(pcs[0]);
        createTestSizeDetailConfig(pcs[0]);
        createTestFrameNotchConfig(pcs[0]);
        
        test.startTest();
        LightningResponse results = MakabilityValidatorController.makabilityOrderItem(ois[0].id);
        test.stopTest();
        
        system.debug(results);        
    }
    
    
    
    
    
    private static List<Product_Configuration__c> updateTestProConfig(){
        List<Product_Configuration__c> pcs = [Select id FROM Product_Configuration__c Where Name='Product Config'];
        for(Integer i = 0 ; i < pcs.size(); i++){
            Product_Configuration__c pc = pcs[i];
            pc.Tempered_Glass_Required_at_UI__c = 100;	
        }
        update pcs;
        return pcs;
    }
    
    private static void createTestColorConfig(Product_Configuration__c pc){
        Color_Configuration__c cc = new Color_Configuration__c();
        cc.Product_Configuration__c = pc.id;
        cc.Exterior_Color__c = 'blue';
        cc.Interior_Color__c = 'blue;white;red';
        insert cc;
    }
    
    private static void createTestScreenConfig(Product_Configuration__c pc){
        Screen_Configuration__c sc= new Screen_Configuration__c();
        sc.Screen_Type__c = 'TruScene';
        sc.Full_Screen_Max_Height_Fraction__c = 'Even';
        sc.Full_Screen_Max_Height_Inches__c = 50;
        sc.Max_Height_Fraction__c = 'Even';
        sc.Max_Height_Inches__c = 96;
        sc.Max_Width_Fraction__c = 'Even';
        sc.Max_Width_Inches__c = 50;
        sc.Product_Configuration__c = pc.id;
        insert sc;
    }
    private static void createTestGlazingConfig(Product_Configuration__c pc){
        Glazing_Configuration__c gc = new Glazing_Configuration__c();
        gc.Glass_Pattern__c = 'Fern';
        gc.Glazing__c = 'High Performance';
        gc.Max_Height_Fraction__c = 'Even';
        gc.Max_Height_Inches__c = 96;
        gc.Max_Width_Inches__c = 50;
        gc.Max_Width_Fraction__c = 'Even';
        gc.Product_Configuration__c = pc.id;
        insert gc;
    }
    
    private static void createTestGrilleConfig(Product_Configuration__c pc){
        Grille_Pattern_Configuration__c gc = new Grille_Pattern_Configuration__c();
        gc.Grille_Pattern__c = 'Colonial';
        gc.Min_Height_Fraction__c = 'Even';
        gc.Min_Height_Inches__c = 50;
        gc.Min_Width_Inches__c = 25;
        gc.Min_Width_Fraction__c = 'Even';
        gc.Product_Configuration__c = pc.id;
        insert gc;
    }   

    private static void createTestSpecialtyShapeGrilleConfig(Product_Configuration__c pc){
       Specialty_Grille_Configuration__c sgc = new Specialty_Grille_Configuration__c(); 
        sgc.Product_Configuration__c = pc.id;
        sgc.Grille_Pattern__c = 'Gothic';
		sgc.Grille_Style__c = 'Full Divided Light (FDL with spacer)';
        sgc.Hubs__c = 0;
        sgc.Lites_High__c = 0;
        sgc.Lites_Wide__c = 0;
        sgc.Specialty_Shape__c ='Circle Top';
        sgc.Spokes__c = 0;
        insert sgc;											  
    }

    private static void createTestSizeDetailConfig(Product_Configuration__c pc){
       Size_Detail_Configuration__c sdc = new Size_Detail_Configuration__c(); 
        sdc.Product_Configuration__c = pc.id;
        sdc.Performance_Category__c = 'Standard';
        sdc.Positive_Force__c = null;
        sdc.Negative_Force__c = null;
        sdc.Frame_Type__c = 'Flat Sill';
        sdc.Hardware_Options__c = 'Normal Hinge';
        sdc.Max_Height_Inches__c = 40;
        sdc.Max_Width_Inches__c = 40;
        sdc.Max_Height_Fraction__c = 'Even';
        sdc.Max_Width_Fraction__c = 'Even';
        sdc.Min_Height_Inches__c = 40;
        sdc.Min_Width_Inches__c = 40;
        sdc.Min_Height_Fraction__c = 'Even';        
        sdc.Min_Width_Fraction__c = 'Even';
        sdc.Extended_Max_Height_Inches__c = 80;
        sdc.Extended_Max_Width_Inches__c = 80;
        sdc.Extended_Max_Height_Fraction__c = 'Even';
        sdc.Extended_Max_Width_Fraction__c = 'Even';
        sdc.Sash_Operation__c = 'Right';
        sdc.Sash_Ratio__c = '1:1';
        sdc.Specialty_Shape__c = null;
        sdc.United_Inch_Maximum__c = 120;
        sdc.United_Inch_Minimum__c = 30;
        insert sdc;	
    } 

    private static void createTestFrameNotchConfig(Product_Configuration__c pc){
       Frame_Notch_Configuration__c fnc = new Frame_Notch_Configuration__c(); 
        fnc.ProductConfigurationLookup__c = pc.id;
        fnc.Pocket_Notch__c = true;
		fnc.Frame_Type__c = 'Flat Sill';
        fnc.Exterior_Trim__c = 'Chamfered Edge Overfit Flange';
        insert fnc;		
    }

    private static List<OrderItem> createOrderItems(Integer num, Order o){
        List<OrderItem> ois = [Select id, OrderId, Product2Id from OrderItem Where OrderId = :o.Id];
        for(integer i = 0; i<ois.size(); i++){
            OrderItem oi = ois[i];
            oi.Height_Inches__c = 50;
            oi.Height_Fraction__c = 'Even';
            oi.Width_Inches__c = 50;
            oi.Width_Fraction__c = 'Even';
            oi.Interior_Color__c = 'blue';
            oi.Exterior_Color__c = 'blue';
            oi.Screen_Size__c = '';
            oi.Screen_Type__c ='TruScene';
            oi.Glass_Pattern_S1__c ='fern';
            oi.Glass_Pattern_S2__c = 'fern';
            oi.Tempered_S1__c = true;
            oi.Tempered_S2__c = true;
            oi.Grille_Pattern__c = 'Colonial';
            oi.Pocket_Notch__c = false;
            oi.Exterior_Trim__c = 'Chamfered Edge Overfit Flange';
            oi.Grille_Style__c = 'Gothic';
            oi.Lites_High_S1__c = 0;
            oi.Lites_Wide_S1__c  = 0;
            oi.Spokes__c = '0';
            oi.Hubs__c = '0';
            oi.Frame_Type__c = 'Flat Sill';
            oi.Hardware_Option__c = 'Normal Hinge';
            oi.Sash_Operation__c = 'Right';
            oi.Sash_Ratio__c  = '1:1';
            oi.Specialty_Shape__c = null;           
        }
        update ois;
        return ois;
    }

}