/**
 * @author Micah Johnson - Demand Chain 2020
 * @description Trigger Framework for the Purchase_Order__c and Cost_Purchase_Order objects (Note: Micah J updated logic for StatusCheckUtility and RollBackStatusQue)
**/

public with sharing class PurchaseOrderTriggerHandler {
  
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	Map<Id,Order> updateOrderMap =new Map<Id,Order>(); 
	Set<Id> orderIds = new Set<Id>();
	private static Id costPoRecordTypeId = UtilityMethods.retrieveRecordTypeId('Cost_Purchase_Order', 'Purchase_Order__c');

	public PurchaseOrderTriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}

	public void OnAfterInsert(Purchase_Order__c[] newPurchaseOrders) {
		updateHasPOCheckBox(newPurchaseOrders);
		Map<Id,Purchase_Order__c> newMap = new Map<Id,Purchase_Order__c>(newPurchaseOrders);
		// System.enqueueJob(new StatusCheckQue(newMap, null, 'PurchaseOrder'));
		Id RtId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByName().get('RbA Purchase Order').getRecordTypeId();
		Map<Id,Purchase_Order__c> eligiblePOMap = new Map<Id,Purchase_Order__c>();
		for (Purchase_Order__c po : newMap.values()) {
			if (po.RecordTypeId == RtId) {
				eligiblePOMap.put(po.Id,po);
			}
		}

		if (!eligiblePOMap.isEmpty()) {
			StatusCheckUtility.PurchaseOrderToOrderStatusCheck(eligiblePOMap, null);
		}

	}
		
	public void OnBeforeInsert( List<Purchase_Order__c> newList){
		//updateOrderEstimatedShipDate( newList);
	}
	public void OnAfterDelete( Map<ID, Purchase_Order__c> oldMap){
		//updateOrderEstimatedShipDate(oldMap);
	}

	public void OnBeforeUpdate( Map<ID, Purchase_Order__c> oldMap, Map<ID, Purchase_Order__c> newMap){
		updateOrderStatusIfNecessary(oldMap, newMap);  
		//updateOrderEstimatedShipDate(oldMap, newMap);
	}

	public void OnAfterUpdate(Map<Id,Purchase_Order__c> oldMap, Map<Id,Purchase_Order__c> newMap){

		updateHasPOCheckbox(oldMap, newMap);
		Id RtId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByName().get('RbA Purchase Order').getRecordTypeId();
		Id RtProdId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByName().get('Product Purchase Order').getRecordTypeId();
		Map<Id,Purchase_Order__c> eligibleNewPOMap = new Map<Id,Purchase_Order__c>();
		Map<Id,Purchase_Order__c> eligibleOldPOMap = new Map<Id,Purchase_Order__c>();
		for (Purchase_Order__c po : newMap.values()) {
			if (po.RecordTypeId == RtId || po.RecordTypeId == RtProdId) {
				eligibleNewPOMap.put(po.Id,po);
				Purchase_Order__c oldPurchaseOrder = oldMap.get(po.Id);
				eligibleOldPOMap.put(oldPurchaseOrder.Id,oldPurchaseOrder);
			}
		}

		if (!eligibleNewPOMap.isEmpty()) {
			StatusCheckUtility.PurchaseOrderToOrderStatusCheck(eligibleNewPOMap, eligibleOldPOMap);
		}

	}

	public void updateOrderStatusToInstallNeeded(Map<Id, Purchase_Order__c> oldMap, Map<Id,Purchase_Order__c> newMap){
		Map<Id, Order> parentOrders = new Map<Id,Order>();
		Set<Id> orderIds = new Set<Id>();
		Map<Id, Boolean> mapOrderIDtoValidForUpdate = new Map<Id, Boolean>();
		List<Order> ordersToUpdate = new List<Order>();
		Set<Id> poIdsToExclude = new Set<Id>();
		for(Purchase_Order__c po : newMap.values()){
			if(po.Status__c == 'Confirmed' && oldMap.get(po.Id).Status__c != po.Status__c
				&& po.RecordTypeId != costPoRecordTypeId){
				orderIds.add(po.Order__c);
				poIdsToExclude.add(po.Id);
			}
		}

		//If there are no order ids to update, just return
		if (orderIds.size() == 0) return;
			
		parentOrders = new Map<Id,Order>([SELECT Id, Status, Apex_Context__c FROM Order WHERE Id in: orderIds AND (Status = 'Order Released' OR  Status = 'Ready to Order')]);
		System.debug('@pavG....'+parentOrders);
		// Return if there were no orders in Order Released status.
		if (parentOrders.size() == 0) return;

		List<Purchase_Order__c> allRelatedPOs = [Select Id, Order__c, Status__c from Purchase_Order__c where Order__c in: parentOrders.keySet() AND Id not in : poIdsToExclude];
		for(Id orderId: parentOrders.keySet()){
			System.debug('@pavG.... orderId'+orderId);
			mapOrderIDtoValidForUpdate.put(orderId, true);
		}
		for(Purchase_Order__c po: allRelatedPOs){
			System.debug('@pavG.... po Outside'+po.Status__c);
			if(po.Status__c != 'Confirmed' && po.Status__c != 'Cancelled'){
			System.debug('@pavG.... po Inside'+po.Status__c);
				mapOrderIDtoValidForUpdate.put(po.Order__c,false);
			}
		}

		for(Id orderId: mapOrderIDtoValidForUpdate.keySet()){
			if(mapOrderIDtoValidForUpdate.get(orderId)){
				ordersToUpdate.add(parentOrders.get(orderId));
			}
		}

		if (!ordersToUpdate.isEmpty()) {
			for(Order o : ordersToUpdate){
				o.Status = 'Install Needed';
				o.Apex_Context__c = true;
			}
			update ordersToUpdate;
		}
		
	}

	public void updateHasPOCheckbox(Map<Id, Purchase_Order__c> oldMap, Map<Id,Purchase_Order__c> newMap){
		Map<Id, Order> parentOrders = new Map<Id,Order>();
		Set<Id> orderIds = new Set<Id>();
		Map<Id, Boolean> mapOrderIDtoValidForUpdate = new Map<Id, Boolean>();
		List<Order> ordersToUpdate = new List<Order>();
		Set<Id> poIdsToExclude = new Set<Id>();
		for(Purchase_Order__c po : newMap.values()){
			if(po.Status__c == 'Cancelled' && oldMap.get(po.Id).Status__c != po.Status__c
				&& po.RecordTypeId != costPoRecordTypeId){
				orderIds.add(po.Order__c);
				poIdsToExclude.add(po.Id);
			}
		}
		
		//If there are no order ids to update, just return
		if (orderIds.size() == 0) return;

		parentOrders = new Map<Id,Order>([SELECT Id, Status, hasPO__c FROM Order WHERE Id in: orderIds]);
		List<Purchase_Order__c> allRelatedPOs = [Select Id, Order__c, Status__c 
		from Purchase_Order__c 
		where Order__c in: orderIds AND Status__c != 'Cancelled' AND  Id not in : poIdsToExclude];
		for(Id orderId: parentOrders.keySet()){
			mapOrderIDtoValidForUpdate.put(orderId, true);
		}
		for(Purchase_Order__c po: allRelatedPOs){
			mapOrderIDtoValidForUpdate.put(po.Order__c,false);
		}

		for(Id orderId: mapOrderIDtoValidForUpdate.keySet()){
			if(mapOrderIDtoValidForUpdate.get(orderId)){
				ordersToUpdate.add(parentOrders.get(orderId));
			}
		}

		for(Order o : ordersToUpdate){
			o.hasPO__c = false;
		}
		update ordersToUpdate;

	}

	public void updateHasPOCheckbox(List<Purchase_Order__c> newPOs){
		List<Id> orderIds = new List<Id>();
		for(Purchase_Order__c po: newPOs){
			orderIds.add(po.Order__c);
		}

		List<Order> ordersToUpdate = [Select Id from Order where Id in: orderIds];
		for(Order o: ordersToUpdate){
			o.hasPO__c = true; 
		}
		update ordersToUpdate;
	}

	public static void updateOrderStatusIfNecessary(Map<Id, Purchase_Order__c> oldMap, Map<Id,Purchase_Order__c> newMap){
		Set<Id> orderIds = new Set<Id>();
		Set<Id> poIdsStatusReleased = new Set<Id>();
		list<Order> ordersToUpdate = new list<Order>();
		for (Purchase_Order__c po: newMap.values()){
			//Existing Functionality
			if(po.Status__c=='Released' 
				&& oldMap.get(po.Id).Confirmed_Timestamp__c == null 
				&& po.Confirmed_Timestamp__c != null){
				po.Status__c='Confirmed';
			}

			//RFORCE-416 Update the Order Status when all POs have been recieved for an Order:
			if((po.Status__c=='Released' || po.Status__c=='Confirmed' || po.Status__c=='Cancelled')
				&& oldMap.get(po.Id).Status__c != newMap.get(po.Id).Status__c) {
				if(!orderIds.contains(po.Order__c) && po.RecordTypeId != costPoRecordTypeId){
					orderIds.add(po.Order__c);
				}
				poIdsStatusReleased.add(po.Id);
			}
		}

		//we can use this method now instead getOrdersToListPurchaseOrderMap()
		map<Id, list<Purchase_Order__c>> orderIdToListofPO = getOrdersToListPurchaseOrderMap(orderIds, newMap);

		//now query for the Order Ids that you got in the Released Loop:
		map<Id,Order> orders = new map<Id,Order>([Select Id, Status, RecordTypeId from Order where id in: orderIds]);
		//now we have everything we need to loop over the orders
		Id productPurchaseOrderId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByName().get('Product Purchase Order').getRecordTypeId();
		for (Order o: orders.values()){
			boolean updateOrderStatus = false;
			list<Purchase_Order__c> poOnOrder = orderIdToListofPO.get(o.Id);
			//if Order
			if(o.RecordTypeId == Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Record Type').getRecordTypeId()) {
				for (Purchase_Order__c po3: poOnOrder){
				if(po3.Status__c =='Released' || po3.Status__c =='Confirmed' || po3.Status__c =='Cancelled'){
					//these are all acceptable but we need to make sure at lease one is in  Releases or Confirmed Status.
					if((po3.Status__c =='Released' || po3.Status__c =='Confirmed') && po3.RecordTypeId == productPurchaseOrderId){
					updateOrderStatus = true;
					}
				} else {
					updateOrderStatus = false;
					break;
				}
				}
			}
			//else if Service order
			else if(o.RecordTypeId == Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Service').getRecordTypeId()) {
				for(Purchase_Order__c service : poOnOrder) {
					/*if the status has been set to released*/ 
					/*And the related order is of type CORO_Service*/ 
					/*And the related Order status is "To be Ordered"*/ 
					if(service.Status__c == 'Released' && o.Status == 'To be Ordered') { 
						updateOrderStatus = true;
					}
				}
			}

			if(updateOrderStatus){
				if(o.RecordTypeId == Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Record Type').getRecordTypeId()) {
					o.Status = 'Order Released';
				} 
				//Commented out the following code as users want to manually change the status (RFORCE-1754)
				//else if(o.RecordTypeId == Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Service').getRecordTypeId()) { 
				//  o.Status = 'Product Ordered';
				//}
				o.Apex_Context__c = true;
				ordersToUpdate.add(o);
			}
		}
		if (ordersToUpdate.size()>0){
			update ordersToUpdate;
		}
	}
	
	// Geetha Katta Update Estimated Ship date
	
	public static void updateEstimatedShipDateonOrder(set<Id> orderIds, Map<Id,Order> updateOrderMap){
		
		List<AggregateResult> AggregateResultList = [select Order__c, MAX(Estimated_Ship_Date__c) maxDate from Purchase_Order__c where Order__c IN : orderIds AND Status__c IN ('Confirmed','Received','Partially Received') group by Order__c]; 
		if(AggregateResultList != null && AggregateResultList.size() > 0){ 
			for(AggregateResult aggr:AggregateResultList){             
				Order od = new Order(); 
				od.Estimated_Ship_Date__c = (date)aggr.get('maxDate'); 
				od.Id = (id)aggr.get('Order__c'); 
				updateOrderMap.put(od.Id, od); 
			} 
		} 
		update updateOrderMap.values();      
	}

	//this method is for when the Insert happens, map is null so we need to take in a list
	public static map<Id, list<Purchase_Order__c>> getOrdersToListPurchaseOrderMap(set<id> ids){
		map<Id,Purchase_Order__c> purchaseOrders = new map<Id,Purchase_Order__c>(
		[Select Id, Order__c, Status__c, Estimated_Ship_Date__c, RecordTypeId
		from Purchase_Order__c 
		where Order__c in: ids]);

		map<Id, list<Purchase_Order__c>> orderIdToListofPO = new map<Id, list<Purchase_Order__c>>();

		for (Purchase_Order__c po2: purchaseOrders.values()){

			if (!orderIdToListofPO.containsKey(po2.Order__c)){
				list<Purchase_Order__c> pos = new list<Purchase_Order__c>();
				pos.add(po2);
				orderIdToListofPO.put(po2.Order__c,pos);
			} else {
				list<Purchase_Order__c> pos = orderIdToListofPO.get(po2.Order__c);
				pos.add(po2);
				orderIdToListofPO.put(po2.Order__c, pos);
			}
		}

		return orderIdToListofPO;

	}

	public static map<Id, list<Purchase_Order__c>> getOrdersToListPurchaseOrderMap(set<id> ids, Map<Id,Purchase_Order__c> newMap){
		map<Id,Purchase_Order__c> purchaseOrders = new map<Id,Purchase_Order__c>(
		[Select Id, Order__c, Status__c, Estimated_Ship_Date__c, RecordTypeId
		from Purchase_Order__c 
		where Order__c in: ids]);

		map<Id, list<Purchase_Order__c>> orderIdToListofPO = new map<Id, list<Purchase_Order__c>>();

		for (Purchase_Order__c po2: purchaseOrders.values()){
			if (!orderIdToListofPO.containsKey(po2.Order__c)){
				list<Purchase_Order__c> pos = new list<Purchase_Order__c>();
				if (newMap.containsKey(po2.Id)){
				pos.add(newMap.get(po2.Id));
				} else {
				pos.add(po2);
				}
				orderIdToListofPO.put(po2.Order__c,pos);
			} else {
				list<Purchase_Order__c> pos = orderIdToListofPO.get(po2.Order__c);
				if (newMap.containsKey(po2.Id)){
				pos.add(newMap.get(po2.Id));
				} else {
				pos.add(po2);
				}
				orderIdToListofPO.put(po2.Order__c, pos);
			}
		}

		return orderIdToListofPO;
		
	}

}