@isTest
public class RMS_StoreConfigurationTriggerHandlerTest {

    @testSetup 
    static void setup() {
        TestUtilityMethods utility = new TestUtilityMethods();
		
		utility.setUpConfigs();
		
		Account myStore = utility.createStoreAccount('mystore');
		insert myStore;
		
	
		
        List<User> userList = new list<User>();

        List<Profile> ProfileList = [select id from Profile where name = 'RMS-CORO RSR'];
        User cc = createUser(ProfileList[0].Id);
        cc.CommunityNickname = 'ccNick';
        User notCC = createUser(ProfileList[0].Id);
        notCC.CommunityNickname = 'notCCNick';

        userList.add(cc);
        userList.add(notCC);

        ProfileList = [select id from Profile where name = 'Super Administrator'];
        User admin = createUser(ProfileList[0].Id);
        admin.CommunityNickname = 'adminNick';

        userList.add(admin);

        ProfileList = [select id from Profile where name = 'System Administrator'];
        User sysadmin = createUser(ProfileList[0].Id);
        userList.add(sysadmin);
        sysadmin.CommunityNickname = 'sysadminNick';

        

        insert userList;
        
        //        System.debug(userList);
    }

    @isTest
    public static void testIsBeforeUpdateComplianceUser() {
        System.debug('beginning test: testIsBeforeUpdateComplianceUser');

        Map<Id,Profile> profileMap = new Map<Id,Profile>([SELECT Id, Name FROM Profile WHERE Name IN ('Super Administrator','RMS-CORO RSR')]);

        User isCC, notCC, admin;
        for (User u : [SELECT Id, ProfileId
                       FROM   User
                       WHERE  IsActive = true
                       AND    ProfileId IN :profileMap.keySet() LIMIT 50000]) {
            
            Profile profile = profileMap.get(u.ProfileId);
            if (profile.Name == 'RMS-CORO RSR' && isCC == null) {
                isCC = u;
                System.debug('isCC assigned: ' + isCC);
            }
            else if (profile.Name == 'RMS-CORO RSR' && isCC != null && notCC == null) {
                notCC = u;
                System.debug('notCC assigned: ' + notCC);
            }
            else  if (profile.Name == 'Super Administrator' && admin == null) {
                admin = u;
                System.debug('admin assigned: ' + admin);
            }

        }

        List<Account> myStoreList = [select Name, Id from Account where name = 'myStore'];
        Account myStore = myStoreList[0];
        System.debug(myStore);

        Store_Configuration__c myStoreConfig = new Store_Configuration__c(Store__c = myStore.Id, Order_Number__c = 1, Contractor_License_1__c = '12345', Contractor_License_2__c = '23456', Contractor_License_1_Expiration__c = Date.newInstance(2018,1,2), Contractor_License_2_Expiration__c = Date.newInstance(2018,3,4), Compliance_Coordinator__c = isCC.Id);
        Test.startTest();        
        System.debug('myStoreConfig: ' + myStoreConfig);
		insert myStoreConfig;
        Test.stopTest();  //force the permission set assignment to apply
        PermissionSet scce = [select Id, Name from PermissionSet where Name = 'Store_Configuration_Contract_Editing'];
        System.debug(scce);
        System.debug([select Id, AssigneeId, Assignee.Name, PermissionSet.Name from PermissionSetAssignment where assigneeId = :isCC.Id]);


        System.debug(myStoreConfig);

        System.debug('myStoreConfig CC: ' + myStoreConfig.Compliance_Coordinator__c);
        System.debug('isCC: ' + isCC);
        

        try {
            UserRecordAccess ura = [select RecordId, HasReadAccess, HasEditAccess from UserRecordAccess where userId = :isCC.id and RecordId = :myStoreConfig.Id];
            System.debug(ura);
            if(ura.HasEditAccess) {     
            myStoreConfig.Contractor_License_1__c = '98765';
            myStoreConfig.Contractor_License_2__c = '87654';
            myStoreConfig.Contractor_License_1_Expiration__c = Date.newInstance(2019,1,1);
            myStoreConfig.Contractor_License_2_Expiration__c = Date.newInstance(2020,1,1);
            Database.update(myStoreConfig, true);
            }
        }
        catch (Exception e) {
                myStoreConfig.addError('You cannot update the Contractor License.' + e);
            }
        System.debug('testing notCC');
        //test Contractor_License_1__c
        System.assertEquals('98765', myStoreConfig.Contractor_License_1__c);
        //test Contractor_License_2__c
        System.assertEquals('87654', myStoreConfig.Contractor_License_2__c);
        //test Contractor_License_1_Expiration__c
        System.assertEquals(Date.newInstance(2019,1,1), myStoreConfig.Contractor_License_1_Expiration__c);
        //test Contractor_License_2_Expiration__c
        System.assertEquals(Date.newInstance(2020,1,1), myStoreConfig.Contractor_License_2_Expiration__c);

               
    }

    @isTest
    public static void testIsBeforeUpdateNotComplianceUser() {
        System.debug('beginning test: testIsBeforeUpdateNotComplianceUser');

        Map<Id,Profile> profileMap = new Map<Id,Profile>([SELECT Id, Name FROM Profile WHERE Name IN ('Super Administrator','RMS-CORO RSR')]);

        User isCC, notCC, admin;
        for (User u : [SELECT Id, ProfileId
                       FROM   User
                       WHERE  IsActive = true
                       AND    ProfileId IN :profileMap.keySet() LIMIT 50000]) {
            
            Profile profile = profileMap.get(u.ProfileId);
            if (profile.Name == 'RMS-CORO RSR' && isCC == null) {
                isCC = u;
                System.debug('isCC assigned: ' + isCC);
            }
            else if (profile.Name == 'RMS-CORO RSR' && isCC != null && notCC == null) {
                notCC = u;
                System.debug('notCC assigned: ' + notCC);
            }
            else  if (profile.Name == 'Super Administrator' && admin == null) {
                admin = u;
                System.debug('admin assigned: ' + admin);
            }

        }

        List<Account> myStoreList = [select Name, Id from Account where name = 'myStore'];
        Account myStore = myStoreList[0];
        System.debug(myStore);

        Store_Configuration__c myStoreConfig = new Store_Configuration__c(Store__c = myStore.Id, Order_Number__c = 1, Contractor_License_1__c = '12345', Contractor_License_2__c = '23456', Contractor_License_1_Expiration__c = Date.newInstance(2018,1,2), Contractor_License_2_Expiration__c = Date.newInstance(2018,3,4), Compliance_Coordinator__c = isCC.Id);
        
        System.debug('myStoreConfig: ' + myStoreConfig);
		insert myStoreConfig;

        PermissionSet scce = [select Id, Name from PermissionSet where Name = 'Store_Configuration_Contract_Editing'];
        System.debug(scce);
        System.debug([select Id, AssigneeId, Assignee.Name, PermissionSet.Name from PermissionSetAssignment where assigneeId = :isCC.Id]);


        System.debug(myStoreConfig);

        System.debug('myStoreConfig CC: ' + myStoreConfig.Compliance_Coordinator__c);
        System.debug('isCC: ' + isCC);
        Test.startTest();
        try {
            UserRecordAccess ura = [select RecordId, HasReadAccess, HasEditAccess from UserRecordAccess where userId = :notCC.id and RecordId = :myStoreConfig.Id];
            if(ura.HasEditAccess) {
                System.debug(ura);

            myStoreConfig.Contractor_License_1__c = '98765';
            myStoreConfig.Contractor_License_2__c = '87654';
            myStoreConfig.Contractor_License_1_Expiration__c = Date.newInstance(2019,1,1);
            myStoreConfig.Contractor_License_2_Expiration__c = Date.newInstance(2020,1,1);
            Database.update(myStoreConfig, true);
        }
        
        }
        catch (Exception e) {
            myStoreConfig.addError('You cannot update the Contractor License.' + e);
        }
        
        //test Contractor_License_1__c
        System.assertNotEquals('98765', [SELECT Contractor_License_1__c FROM Store_Configuration__c WHERE Store__c = :myStore.Id].Contractor_License_1__c);
        //test Contractor_License_2__c
        System.assertNotEquals('87654', [SELECT Contractor_License_2__c FROM Store_Configuration__c WHERE Store__c = :myStore.Id].Contractor_License_2__c);
        //test Contractor_License_1_Expiration__c
        System.assertNotEquals(Date.newInstance(2019,1,1), [SELECT Contractor_License_1_Expiration__c FROM Store_Configuration__c WHERE Store__c = :myStore.Id].Contractor_License_1_Expiration__c);
        //test Contractor_License_2_Expiration__c
        System.assertNotEquals(Date.newInstance(2020,1,1), [SELECT Contractor_License_2_Expiration__c FROM Store_Configuration__c WHERE Store__c = :myStore.Id].Contractor_License_2_Expiration__c);
        Test.stopTest();
    }

    private static Integer alias = 1;
    public static User createUser(Id profileID){

		String orgId = UserInfo.getOrganizationId();
    	String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    	Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    	String uniqueName = orgId + dateString + randomInt;  

		User testUser = new User(
			FirstName = 'TestFirst',
			LastName = 'TestLast',
			email = uniqueName + '@test' + orgId + '.org',
            Username = uniqueName + '@test' + orgId + '.org',			
            Alias = 'test' + String.valueOf(alias), 
			CommunityNickname = 'test' + String.valueOf(++alias),
			TimeZoneSidKey = 'America/Mexico_City', 
			LocaleSidKey = 'en_US', 
			EmailEncodingKey= 'UTF-8', 
			ProfileId = profileID, 
			LanguageLocaleKey = 'en_US'
			
		);
        alias++;
		return testUser;
	}

}