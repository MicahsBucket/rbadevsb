/**
* @author Jason Flippen
* @date 01/23/2021
* @description Test Class for the following Classes:
*              - OrderRecRevActionController
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/ 
@isTest
public class OrderRecRevActionControllerTest {

    /**
    * @author Jason Flippen
    * @date 01/23/2021 
    * @description Method to create data to be consumed by test methods.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @testSetup
    static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();
        
        Account testVendorAccount = testUtility.createVendorAccount('Test Vendor Account');
        insert testVendorAccount;
        
        Account testStoreAccount = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAccount = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAccount.Store_Location__c = testStoreAccount.Id;
        insert testDwellingAccount;

        Store_Configuration__c testStoreConfiguration = [SELECT Id FROM Store_Configuration__c WHERE Store__c = :testStoreAccount.Id];
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAccount.Id, 'Closed - Won');
        insert testOpportunity;
        
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO',
                                                                              Store_Configuration__c = testStoreConfiguration.Id);
        insert testFAN;

        List<Financial_Transaction__c> testFinancialTransactionList = new List<Financial_Transaction__c>();
        Financial_Transaction__c testFinancialTransaction01 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - Deposit');
        testFinancialTransactionList.add(testFinancialTransaction01);
        Financial_Transaction__c testFinancialTransaction02 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - Discount');
        testFinancialTransactionList.add(testFinancialTransaction02);
        Financial_Transaction__c testFinancialTransaction03 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - AR');
        testFinancialTransactionList.add(testFinancialTransaction03);
        Financial_Transaction__c testFinancialTransaction04 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - Refund Deposit');
        testFinancialTransactionList.add(testFinancialTransaction04);
        Financial_Transaction__c testFinancialTransaction05 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - External Labor');
        testFinancialTransactionList.add(testFinancialTransaction05);
        Financial_Transaction__c testFinancialTransaction06 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - COGS');
        testFinancialTransactionList.add(testFinancialTransaction06);
        insert testFinancialTransactionList;
        
        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Legacy_Service').getRecordTypeId(),
                                            Vendor__c = testVendorAccount.Id,
                                            Product_PO__c = true,
                                            Cost_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c =  testFan.Id);
        insert testProduct;
        
        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;
        
        // Parent PricebookEntry
        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order',
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Record_Type').getRecordTypeId(),
                                     AccountId = testDwellingAccount.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAccount.Id,
                                     OpportunityId = testOpportunity.Id,           
                                     Status = 'Draft',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;
        
        Purchase_Order__c testPurchaseOrder = new Purchase_Order__c(Name = 'ABCD12345',
                                                                    Estimated_Ship_Date__c = Date.today(),
                                                                    Order__c = testOrder.Id,
                                                                    Vendor__c = testVendorAccount.Id,
                                                                    Status__c = 'Cancelled',
                                                                    Store_Location__c = testStoreAccount.Id);
        insert testPurchaseOrder;
        
        OrderItem testOrderItem = new OrderItem(OrderId = testOrder.Id,
                                                Has_PO__c = true,
                                                Purchase_Order__c = testPurchaseOrder.Id,
                                                PricebookentryId = testPBEStandard.Id,
                                                Quantity = 2,
                                                Quanity_Ordered__c = 2,
                                                Unit_Wholesale_Cost__c = 50.00,
                                                UnitPrice = 100,
                                                Variant_Number__c = 'ABCD1234');
        insert testOrderItem;

    }

    /**
    * @author Jason Flippen
    * @date 01/23/2021
    * @description: Method to test the methods in the Controller.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testController() {

        Order testOrder = [SELECT Id FROM Order WHERE Name = 'Test Order'];

        Test.startTest();

            OrderRecRevActionController.OrderWrapper orderWrapper = OrderRecRevActionController.getOrderData(testOrder.Id);
            System.assertEquals(orderWrapper.id, testOrder.Id, 'Unexpected Order returned');

            String updateResult = OrderRecRevActionController.updateOrder(orderWrapper);
            System.assertEquals('Update Order Success', updateResult, 'Unexpected Order Update Result');

        Test.stopTest();

    }

}