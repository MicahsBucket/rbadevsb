global class CanvassUnitQueueableBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable 
{
    //database.executebatch(new CanvassUnitQueueableBatch(),1);
    
    //CanvassUnitQueueableBatch rsb = new CanvassUnitQueueableBatch();
    //String sch = '0 0 * * * ?';
    //system.schedule('Canvass Unit Queueable Batch 00', sch, rsb);
    
    //CanvassUnitQueueableBatch rsb = new CanvassUnitQueueableBatch();
    //String sch = '0 15 * * * ?';
    //system.schedule('Canvass Unit Queueable Batch 15', sch, rsb);
    
    //CanvassUnitQueueableBatch rsb = new CanvassUnitQueueableBatch();
    //String sch = '0 30 * * * ?';
    //system.schedule('Canvass Unit Queueable Batch 30', sch, rsb);
    
    //CanvassUnitQueueableBatch rsb = new CanvassUnitQueueableBatch();
    //String sch = '0 45 * * * ?';
    //system.schedule('Canvass Unit Queueable Batch 45', sch, rsb);

    global CanvassUnitQueueableBatch()
    {

    }
    
    global void execute(SchedulableContext SC)
    {
        database.executeBatch(new CanvassUnitQueueableBatch(),1);
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        // query to get holding jobs from queue
        return Database.getQueryLocator ([SELECT Id, Name, Field_Ids__c, Marker_Ids__c, Marker_Ids2__c, Marker_Ids3__c, Marker_Ids4__c, Marker_Ids5__c, Marker_Ids6__c, Market_Id__c, Status__c, ZipCode__c 
                                          FROM Custom_Data_Layer_Batch_Queue__c 
                                          WHERE Status__c = 'Holding']);
    }
    
    global void execute (Database.BatchableContext BC, List<Custom_Data_Layer_Batch_Queue__c> scope)
    {
        try
        {
            List<Canvass_Unit_Batch_Fields__c> apexBatchJobLimitList = [SELECT Id, Name, Apex_Batch_Job_Limit__c FROM Canvass_Unit_Batch_Fields__c];
            
            if(apexBatchJobLimitList.size() > 0)
			{
                // query to get currently running jobs
                list<AsyncApexJob> listOfRunningJobs = [SELECT Status, NumberOfErrors, ApexClassID, CompletedDate, ExtendedStatus, JobItemsProcessed, JobType, MethodName, TotalJobItems 
                                                        FROM AsyncApexJob 
                                                        WHERE Status != 'Completed' AND Status != 'Aborted' AND Status != 'Failed'];
                
                // if running jobs less than  50
                if(listOfRunningJobs.size() < apexBatchJobLimitList[0].Apex_Batch_Job_Limit__c || Test.isRunningTest() == true)
                {
                    // iterate over scope
                    for(Custom_Data_Layer_Batch_Queue__c queuedJob :scope)
                    {
                        list<string> fieldIdList = queuedJob.Field_Ids__c.split(', ');
                        list<string> markerIdList = queuedJob.Marker_Ids__c.split(', ');
                        list<string> markerIdList2 = new list<string>();
                        
                        list<string> markerIdList3 = new list<string>();
                        list<string> markerIdList4 = new list<string>();
                        list<string> markerIdList5 = new list<string>();
                        list<string> markerIdList6 = new list<string>();
                        
                        if(queuedJob.Marker_Ids2__c != null)
                        {
                            markerIdList2 = queuedJob.Marker_Ids2__c.split(', ');
                        }
                        
                        if(queuedJob.Marker_Ids3__c != null)
                        {
                            markerIdList3 = queuedJob.Marker_Ids3__c.split(', ');
                        }
                        if(queuedJob.Marker_Ids4__c != null)
                        {
                            markerIdList4 = queuedJob.Marker_Ids4__c.split(', ');
                        }
                        if(queuedJob.Marker_Ids5__c != null)
                        {
                            markerIdList5 = queuedJob.Marker_Ids5__c.split(', ');
                        }
                        if(queuedJob.Marker_Ids6__c != null)
                        {
                            markerIdList6 = queuedJob.Marker_Ids6__c.split(', ');
                        }
                        
                        //set<string> zipCodeSet = new set<string>();
                        //zipCodeSet.add(queuedJob.ZipCode__c);
                        
                        list<object> fieldIdListObj = new list<object>();
                        for(string field :fieldIdList)
                        {
                            fieldIdListObj.add(field);
                        }
                        
                        list<object> markerIdListObj = new list<object>();
                        for(string marker :markerIdList)
                        {
                            markerIdListObj.add(integer.valueOf(marker));
                        }
                        
                        for(string marker :markerIdList2)
                        {
                            markerIdListObj.add(integer.valueOf(marker));
                        }
                        
                        for(string marker :markerIdList3)
                        {
                            markerIdListObj.add(integer.valueOf(marker));
                        }
                        for(string marker :markerIdList4)
                        {
                            markerIdListObj.add(integer.valueOf(marker));
                        }
                        for(string marker :markerIdList5)
                        {
                            markerIdListObj.add(integer.valueOf(marker));
                        }
                        for(string marker :markerIdList6)
                        {
                            markerIdListObj.add(integer.valueOf(marker));
                        }
                        
                        if (!Test.isRunningTest())
                        {
                            // start queueable job
                            System.enqueueJob(new CanvassUnitQueueable(fieldIdListObj,markerIdListObj,queuedJob.ZipCode__c,queuedJob.Market_Id__c, queuedJob.Id));
                        }
                        
                        // update status so that job won't be queued again
                        queuedJob.Status__c = 'Fetching Data In Progress';
                        update queuedJob;
                    }
                }
            }
        }
        catch(Exception e)
        {
            // iterate over scope
            for(Custom_Data_Layer_Batch_Queue__c queuedJob :scope)
            {
                // update status so that job won't be queued again
                queuedJob.Status__c = 'Failed';
                update queuedJob;
            }
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {

    }
}