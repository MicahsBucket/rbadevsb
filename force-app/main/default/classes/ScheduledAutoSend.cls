global class ScheduledAutoSend implements Schedulable {
	global void execute(SchedulableContext sc) {
		Date yesterday = Date.today().addDays(-1);
		List<Survey__c> surveysToSend = [SELECT Id, Survey_Status__c, Survey_Auto_Sent__c FROM Survey__c WHERE Send_Date__c = :yesterday AND Sent_To_Medallia__c != TRUE];
		for(Survey__c s : surveysToSend){
			s.Survey_Status__c = 'Processing';
		}
		Database.update(surveysToSend);
	}
}