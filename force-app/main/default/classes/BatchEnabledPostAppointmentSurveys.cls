/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Signature Service Community
* @date 8/17
* @description Connects surveys with related rForce opportunities and contacts together
**/
global class BatchEnabledPostAppointmentSurveys implements Database.Batchable<sObject> {
	
	String query;
	Date tenDaysAgo;
	
	global BatchEnabledPostAppointmentSurveys() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		tenDaysAgo = Date.today().addDays(-10);
        Id recordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get('Post Appointment').getRecordTypeId();
        String source = 'EnabledPlus';
        String queryString = 'SELECT Id, Name, Store_Account__c, Enabled_Appointment_Id__c, Enabled_Lead_Id__c, Opportunity__c, RecordTypeId, Source_System__c FROM Survey__c WHERE Source_System__c = :source AND RecordTypeId = :recordTypeId AND CreatedDate >= ';
		String todayString = String.valueOf(tenDaysAgo);
		String dateVal = todayString.substring(0, 10);
		dateVal += 'T00:00:00Z';
		queryString += dateVal;
		return Database.getQueryLocator(queryString);
	}

   	global void execute(Database.BatchableContext BC, List<Survey__c> surveys) {
		Map<Id,String> surveysToEnabledApptIds = new Map<Id, String>();
		Map<String,Id> opportunitiesToEnabledApptIds = new Map<String,Id>();
		Map<Id,Survey__c> oppIdToSurveyMap = new Map<Id,Survey__c>();
		Map<Id,Id> surveyToPrimaryContactMap = new Map<Id,Id>();
		Map<Id,Id> surveyToSecondaryContactMap = new Map<Id,Id>();
		List<String> enabledAppointmentIdList = new List<String>();
		List<Contact_Survey__c> junctionsToCreate = new List<Contact_Survey__c>();
		List<Id> allSurveyIds = new List<Id>();
		Set<Id> surveysWithCSIdSet = new Set<Id>();
		List<Survey__c> surveysThatNeedCS = new List<Survey__c>();

		for(Survey__c s : surveys){
			allSurveyIds.add(s.Id);
		}
		List<Contact_Survey__c> oldCSList = [SELECT Survey_Id__c FROM Contact_Survey__c WHERE Survey_Id__c IN :allSurveyIds AND CreatedDate >= :tenDaysAgo];
		for(Contact_Survey__c cs : oldCSList){
			surveysWithCSIdSet.add(cs.Survey_Id__c);
		}
		for(Survey__c s : surveys){
			if(!surveysWithCSIdSet.contains(s.Id)){
				surveysThatNeedCS.add(s);
			}
		}

		for(Survey__c s : surveysThatNeedCS){
			surveysToEnabledApptIds.put(s.Id, s.Enabled_Appointment_Id__c);
			enabledAppointmentIdList.add(s.Enabled_Appointment_Id__c);
		}
		List<Opportunity> opportunities = [SELECT Id, Enabled_Appointment_Id__c FROM Opportunity WHERE Enabled_Appointment_Id__c IN :enabledAppointmentIdList];
		for(Opportunity opp : opportunities){
			opportunitiesToEnabledApptIds.put(opp.Enabled_Appointment_Id__c, opp.Id);
		}
		for(Survey__c s : surveysThatNeedCS){
			String enabledId = surveysToEnabledApptIds.get(s.Id);
			Id oppId = opportunitiesToEnabledApptIds.get(enabledId);
			s.Opportunity__c = oppId;
			oppIdToSurveyMap.put(oppId, s);
		}
		Database.update(surveysThatNeedCS, true);
		junctionsToCreate.addAll(createSurveyToContactJuncList(true, oppIdToSurveyMap));
		junctionsToCreate.addAll(createSurveyToContactJuncList(false, oppIdToSurveyMap));
		Database.upsert(junctionsToCreate, true);
		
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
	public static List<Contact_Survey__c> createSurveyToContactJuncList(Boolean primaryBool, Map<Id, Survey__c> oppIdToSurveyMap){
        Map<Id,Id> mapOpportunityIdToContactId = new Map<Id,Id>();
        Map<Id,Id> surveyToContactMap = new Map<Id,Id>();
		List<Contact_Survey__c> junctionsToCreate = new List<Contact_Survey__c>();
        String typeString = 'Secondary';
        if(primaryBool){
        	typeString = 'Primary';
        }
        //finds opportunity to contact junctions
        List<OpportunityContactRole> listOpportunityContactRoles = [SELECT Id, OpportunityId, ContactId 
                                    FROM OpportunityContactRole 
                                    WHERE OpportunityId IN :oppIdToSurveyMap.keySet() AND isPrimary = :primaryBool];
        //joins opportunity Id to a list of associated contact Ids
        for (OpportunityContactRole oppConRole: listOpportunityContactRoles){
            mapOpportunityIdToContactId.put(oppConRole.OpportunityId, oppConRole.ContactId);       
        }
        for(Id oppId : mapOpportunityIdToContactId.keySet()){
			Id surveyId = oppIdToSurveyMap.get(oppId).Id;
			surveyToContactMap.put(surveyId, mapOpportunityIdToContactId.get(oppId));
		}
		for(Id surveyId : surveyToContactMap.keySet()){
			Contact_Survey__c newJunc = new Contact_Survey__c(
				Contact_Id__c = surveyToContactMap.get(surveyId),
				Survey_Id__c = surveyId,
				Type__c = typeString
			);
			junctionsToCreate.add(newJunc);
		}
        return junctionsToCreate;
    }
}