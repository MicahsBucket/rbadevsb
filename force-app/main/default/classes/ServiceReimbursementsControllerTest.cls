/*
* @author Jason Flippen
* @date 10/21/2020 
* @description Class to provide Test Code coverage for the following Classes:
*			   - ServiceReimbursementsController
*/
@isTest
public with sharing class ServiceReimbursementsControllerTest {

    /*
    * @author Jason Flippen
    * @date 11/24/2020 
    * @description Method to create data to be consumed by test methods.
    */ 
    @testSetup
    static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        List<Account> testAccountList = new List<Account>();

        Account testStoreAccount = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];

        Account testAccount01 = testUtility.createVendorAccount('Test Vendor Account');
        testAccount01.Name = 'Test Vendor Account';
        testAccount01.Store_Location__c = testStoreAccount.Id;
        testAccountList.add(testAccount01);
        Account testDwellingAccount = testUtility.createDwellingAccount('Dwelling Account');
        testDwellingAccount.Store_Location__c = testStoreAccount.Id;
        testAccountList.add(testDwellingAccount);
        insert testAccountList;

        Contact testContact = testUtility.createContact(testAccount01.Id, 'BillToTest');
        insert testContact;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAccount.Id, 'Closed - Won');
        insert testOpportunity;
    
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;

        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Door_Components').getRecordTypeId(),
                                            Family = 'Parts and Accessories',
                                            Vendor__c = testAccount01.Id,
                                            Cost_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c = testFan.Id,
                                            Reimbursable_Part__c = true);
        insert testProduct;

        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order',
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Service').getRecordTypeId(),
                                     AccountId = testDwellingAccount.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAccount.Id,
                                     OpportunityId = testOpportunity.Id,                                 
                                     Status = 'Draft',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;

        Reimbursable_Part__c testReimbursablePart = new Reimbursable_Part__c(Order__c = testOrder.Id,
                                                                             Part__c = testProduct.Id,
                                                                             Unit_Price__c = 100.00);
        insert testReimbursablePart;
        
    }

    /*
    * @author Jason Flippen
    * @date 11/24/2020 
    * @description Method to test the functionality in the Controller.
    */ 
    private static testMethod void testController() {

        Order testOrder = [SELECT Id, Status FROM Order WHERE Name = 'Test Order'];

        Test.startTest();

            // Retrieve Purchase Order record. It should match the one we created for testing.
            ServiceReimbursementsController.ReimbursementsWrapper reimbursementsWrapper = ServiceReimbursementsController.getReimbursements(testOrder.Id);
            System.assertEquals(testOrder.Id, reimbursementsWrapper.orderId, 'Unexpected Reimbursement Order');

        Test.stopTest();

    }

}