@isTest
public without sharing class NewBusinessAdjustmentRedirectTEST {
    @isTest
    public static void NewBusinessAdjustmentTEST() {
        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        Account testVendorAcct = testUtility.createVendorAccount('Test Vendor Account');
        insert testVendorAcct;

        Account testStoreAcct = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAcct = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAcct.Store_Location__c = testStoreAcct.Id;
        insert testDwellingAcct;
        
        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        Order testOrder =  new Order(Name = 'Dummy Cost PO Order',
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Record_Type').getRecordTypeId(),
                                     AccountId = testDwellingAcct.Id,
                                     Cancellation_Reason__c = 'Unknown',
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAcct.Id,
                                     Status = 'Cancelled',
                                     Store_Number__c = '0077',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;


        NewBusinessAdjustmentRedirectController.getRecordData(testOrder.Id, 'Order');
    }
}