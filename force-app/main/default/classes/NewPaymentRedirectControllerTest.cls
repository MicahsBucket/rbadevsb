/**
* @author Jason Flippen
* @date 01/28/2021
* @description Test Class for the following Classes:
*              - NewPaymentRedirectController
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/ 
@isTest
public class NewPaymentRedirectControllerTest {

    /**
    * @author Jason Flippen
    * @date 01/28/2021 
    * @description Method to create data to be consumed by test methods.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @testSetup
    static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();
        
        Account testVendorAccount = testUtility.createVendorAccount('Test Vendor Account');
        insert testVendorAccount;
        
        Account testStoreAccount = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAccount = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAccount.Store_Location__c = testStoreAccount.Id;
        insert testDwellingAccount;

        Contact testContact = new Contact(FirstName = 'Test',
                                          LastName = 'Contact',
                                          HomePhone = '1111111111',
                                          MobilePhone = '2222222222',
                                          Primary_Contact__c = true,
                                          Primary_Dwelling_for_Contact__c = true,
                                          AccountId = testDwellingAccount.Id);
        insert testContact;

        Store_Configuration__c testStoreConfiguration = [SELECT Id FROM Store_Configuration__c WHERE Store__c = :testStoreAccount.Id];
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAccount.Id, 'Closed - Won');
        insert testOpportunity;
        
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO',
                                                                              Store_Configuration__c = testStoreConfiguration.Id);
        insert testFAN;

        List<Financial_Transaction__c> testFinancialTransactionList = new List<Financial_Transaction__c>();
        Financial_Transaction__c testFinancialTransaction01 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - Deposit');
        testFinancialTransactionList.add(testFinancialTransaction01);
        Financial_Transaction__c testFinancialTransaction02 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - Discount');
        testFinancialTransactionList.add(testFinancialTransaction02);
        Financial_Transaction__c testFinancialTransaction03 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - AR');
        testFinancialTransactionList.add(testFinancialTransaction03);
        Financial_Transaction__c testFinancialTransaction04 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - Refund Deposit');
        testFinancialTransactionList.add(testFinancialTransaction04);
        Financial_Transaction__c testFinancialTransaction05 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - External Labor');
        testFinancialTransactionList.add(testFinancialTransaction05);
        Financial_Transaction__c testFinancialTransaction06 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - COGS');
        testFinancialTransactionList.add(testFinancialTransaction06);
        insert testFinancialTransactionList;
        
        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Legacy_Service').getRecordTypeId(),
                                            Vendor__c = testVendorAccount.Id,
                                            Product_PO__c = true,
                                            Cost_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c =  testFan.Id);
        insert testProduct;
        
        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;
        
        // Parent PricebookEntry
        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order',
                                     AccountId = testDwellingAccount.Id,
                                     BillingStreet = '123 Somewhere Street',
                                     BillToContactId = testContact.Id,
                                     EffectiveDate = Date.Today(),
                                     OpportunityId = testOpportunity.Id,
                                     Pricebook2Id = Test.getStandardPricebookId(),
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Record_Type').getRecordTypeId(),
                                     Revenue_Recognized_Date__c = Date.today(),
                                     Status = 'Install Scheduled',
                                     Store_Location__c = testStoreAccount.Id,
                                     Tech_Measure_Status__c = 'New');
        insert testOrder;

    }

    /**
    * @author Jason Flippen
    * @date 01/28/2021
    * @description: Method to test the "getRecordData" method in the Controller.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testGetRecordData() {

        Test.startTest();

            NewPaymentRedirectController.RecordWrapper record = NewPaymentRedirectController.getRecordData('','');

            Id paymentMiscCashReceiptRTId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Misc_Cash_Receipt').getRecordTypeId();
            System.assertEquals(record.recordTypeId, paymentMiscCashReceiptRTId, 'Unexpected Payment Record Type returned');

        Test.stopTest();

    }

    /**
    * @author Jason Flippen
    * @date 01/28/2021
    * @description: Method to test the "getRecordData" method in the Controller from an Order.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testGetRecordDataOrder() {

        Order testOrder = [SELECT Id, BillToContactId, Store_Location__c FROM Order WHERE Name = 'Test Order'];

        Test.startTest();

            NewPaymentRedirectController.RecordWrapper record = NewPaymentRedirectController.getRecordData(testOrder.Id, 'Order');
            System.assertEquals(record.id, testOrder.Id, 'Unexpected record returned');

        Test.stopTest();

    }

}