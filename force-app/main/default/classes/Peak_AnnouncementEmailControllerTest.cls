/**
 * Created by melindagrad on 3/15/18.
 */

@IsTest
public with sharing class Peak_AnnouncementEmailControllerTest {

    @IsTest
    public static void testSendAnnouncementEmail(){
		Test.startTest();
        //Create Test Community_Announcement List
        List<Id> testAnnouncementIDList = new List<Id>();

        //Test Community_Announcement
        Community_Announcement__c testAnnouncement =
                new Community_Announcement__c(Start_Date_Time__c = Date.newInstance(1990, 1, 1),
                        End_Date_Time__c = Date.newInstance(2090, 1, 1), Name = 'Test Announcement',
                        Announcement_Body__c = 'This is a test.', Announcement_Type__c = 'info',
                        Announcement_Channel__c = 'Default');

        insert testAnnouncement;

        System.assert(testAnnouncement.Id != null);

        // create a test account and contact
        insert new RMS_Settings__c(Name = 'Data Loading Profile ID', Value__c = UserInfo.getProfileId());
        //UtilityMethods.disableAccountTrigger();
        Account account = new Account(name=Peak_TestConstants.ACCOUNT_NAME);
        insert account;

        System.assert(account.Id != null);

        //UtilityMethods.disableAccountTrigger();
        Contact contact = new Contact(firstName=Peak_TestConstants.FIRSTNAME, lastName=Peak_TestConstants.LASTNAME, email=Peak_TestConstants.STANDARD_EMAIL);
        contact.accountId = account.id;
        insert contact;
        System.assert(contact.Id != null);

        List<Profile> theProfileList = [SELECT Id FROM Profile WHERE Name = 'Partner RMS-Sales' LIMIT 1];
        System.debug('profile List size:'+theProfileList.size());
        User testUser;
        if (!Peak_Utils.isNullOrEmpty(theProfileList)) {
            Profile theProfile = theProfileList[0];
            testUser = new User(Alias = Peak_TestConstants.STANDARD_ALIAS, Email=Peak_TestConstants.STANDARD_EMAIL, EmailEncodingKey=Peak_TestConstants.ENCODING, FirstName=Peak_TestConstants.FIRSTNAME, LastName=Peak_TestConstants.LASTNAME, LanguageLocaleKey=Peak_TestConstants.LOCALE,LocaleSidKey=Peak_TestConstants.LOCALE, ProfileId = theProfile.Id,TimeZoneSidKey=Peak_TestConstants.TIMEZONE, UserName=Peak_TestConstants.STANDARD_USERNAME);
            testUser.ContactId = contact.id;
            testUser.Enabled_User_Id__c='Test';
            insert testUser;
        }

        System.assert(testUser.Id != null);


        testAnnouncementIDList.add(testAnnouncement.Id);

        Peak_AnnouncementEmailController.sendAnnouncementEmail(testAnnouncementIDList);
		Test.stopTest();
    }
}