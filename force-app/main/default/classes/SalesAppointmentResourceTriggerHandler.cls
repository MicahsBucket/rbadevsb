/* 
* @author: Ramya Bangalore
* @Created Date : 08/10/2019 
* @Description: SalesAppointmentResourceTriggerHandler Method to send the mail on removal of a Sales Assignmenent 
* Date Modified           Modified By             Description of the update
* *******************************************************************************/
public class SalesAppointmentResourceTriggerHandler {
    
    //This Method is called on After Update of Trigger
        public static void AfterUpdate(List<Sales_Appointment_Resource__c> newList,Map<Id,Sales_Appointment_Resource__c> oldMap){
        sendEmail(newList,oldMap);
    }
    
    // This Method is called on After Delete of Trigger
       public static void AfterDelete(Map<Id,Sales_Appointment_Resource__c> oldMap){
        sendEmail(null,oldMap);
    }
    
    // Description  : This Method is called to send Email when the Appointment is Update/Deleted for particular Status
      public static void sendEmail(List<Sales_Appointment_Resource__c> newList,Map<Id,Sales_Appointment_Resource__c> oldMap){
        try{
            //Declaring Variables
            //Changed below line added Manually Reassigned Status by Ramya Bangalore 'Manually Assigned','Manually Reassigned'
            Set<String> assignedStatuses = new Set<String>{'Confirmed', 'Accepted', 'Checked Out', 'Checked In','Manually Assigned','Manually Reassigned'};
                Map<Id,Id> MapRepCapacityIdSetToSalesAppointment = new Map<Id,Id>();
            Map<Id,List<Contact>> MapAppointmentIdToUsers = new Map<Id,List<Contact>>();
            Set<Id> userIDSet = new Set<Id>();
            
            if(newList != null){
                //Update Scenario
                for(Sales_Appointment_Resource__c appointmentResource  : newList){
                    if(appointmentResource.isApptConfirmedOrAccepted__c && appointmentResource.Sales_Capacity__c != oldMap.get(appointmentResource.Id).Sales_Capacity__c && appointmentResource.Primary__c && assignedStatuses.contains(appointmentResource.Status__c)){
                        //Changed below line by Ramya Bangalore
                        MapRepCapacityIdSetToSalesAppointment.put((oldMap.get(appointmentResource.Id)).Sales_Capacity__c,appointmentResource.Sales_Appointment__c);
                    }
                }
            }else{
                //Deletion Scenario
              /*  for(Id appointmentResourceId  : oldMap.keySet()){
                    if(assignedStatuses.contains((oldMap.get(appointmentResourceId)).Status__c)){
                        MapRepCapacityIdSetToSalesAppointment.put((oldMap.get(appointmentResourceId)).Sales_Capacity__c,(oldMap.get(appointmentResourceId)).Sales_Appointment__c);
                    }
                }
            }*/
                for(Id appointmentResourceId  : oldMap.keySet()){
                    if(oldMap.get(appointmentResourceId).isApptConfirmedOrAccepted__c && assignedStatuses.contains((oldMap.get(appointmentResourceId)).Status__c)){
                        MapRepCapacityIdSetToSalesAppointment.put((oldMap.get(appointmentResourceId)).Sales_Capacity__c,(oldMap.get(appointmentResourceId)).Sales_Appointment__c);
                    }
                }
            }
            // Getting the Users
            if(MapRepCapacityIdSetToSalesAppointment != null && !MapRepCapacityIdSetToSalesAppointment.isEmpty()){
                for(Sales_Capacity__c RepCapacityRec : [SELECT Id,User__r.ContactId,User__r.Contact.Secondary_Email__c
                                                        FROM Sales_Capacity__c
                                                        WHERE Id IN :MapRepCapacityIdSetToSalesAppointment.keySet()])
                    if(MapRepCapacityIdSetToSalesAppointment.containsKey(RepCapacityRec.Id)){
                        if(!MapAppointmentIdToUsers.containsKey(MapRepCapacityIdSetToSalesAppointment.get(RepCapacityRec.Id)))
                            MapAppointmentIdToUsers.put(MapRepCapacityIdSetToSalesAppointment.get(RepCapacityRec.Id),new List<Contact>());
                        MapAppointmentIdToUsers.get(MapRepCapacityIdSetToSalesAppointment.get(RepCapacityRec.Id)).add(new Contact(id = RepCapacityRec.User__r.ContactId,Secondary_Email__c = RepCapacityRec.User__r.Contact.Secondary_Email__c));
                     }
            }
            sendEmailtoUsers(MapAppointmentIdToUsers,'Appointment_Cancelled');
        }catch(Exception err){
            //Handler Error
            UtilityMethods.logException(err,'SalesAppointmentResourceTriggerHandler');
        }
    }
    
    //This Method is called to send Email
       public static void sendEmailtoUsers(Map<Id,List<Contact>> toAddMap, String emailTemplate ) {
        if(!toAddMap.isEmpty() && !String.isBlank(emailTemplate)) {
            Id emailTemplateId = [select Id, Name from EmailTemplate where DeveloperName =: emailTemplate].Id;
            List<Messaging.SingleEmailMessage> lstMails = new List<Messaging.SingleEmailMessage>();
            if( !toAddMap.isEmpty() && toAddMap != null ) {
                for(String recordId : toAddMap.keySet()) {
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    email.setWhatId(recordId);
                    email.setTargetObjectId(toAddMap.get(recordId).get(0).Id);
                    if(String.isNotBlank(toAddMap.get(recordId).get(0).Secondary_Email__c))
                    email.setToAddresses(new List<String>{toAddMap.get(recordId).get(0).Secondary_Email__c});
                    email.setTemplateId(emailTemplateId);
                    email.setSaveAsActivity(false);
                    lstMails.add(email);
                }
                
                try {
                    Messaging.sendEmail(lstMails);
                }
                catch (EmailException err) {
                    UtilityMethods.logException(err,'SalesAppointmentResourceTriggerHandler');
                }
            }
        }
    }
}