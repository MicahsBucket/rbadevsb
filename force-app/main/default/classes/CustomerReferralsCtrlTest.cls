@IsTest
public class CustomerReferralsCtrlTest {
	
    @IsTest
    private static void sendRefTest(){
        String message;
      CustomerReferralsCtrl.Referral r = new CustomerReferralsCtrl.Referral();
        r.firstName = 'test';
        r.lastName = 'lasttest';
        r.email = 'test@testtest.com';
        r.phone = '333-444-5555';
        
        test.startTest();
       	message = CustomerReferralsCtrl.sendReferral(r.firstName, r.lastName, r.phone, r.email);
        test.stopTest();
        system.assertEquals('success', message);
    }
}