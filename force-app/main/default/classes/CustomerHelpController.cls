/**
 * @description       : 
 * @author            :
 * @group             : 
 * @last modified on  : 03-17-2021
 * @last modified by  : Connor.Davis@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                          Modification
 * 1.0   03-16-2021   Connor.Davis@andersencorp.com   Initial Version
**/
public without sharing class CustomerHelpController {

    @AuraEnabled
    public static String submitContact(String notess, String stopic, String selOrder, String stopicLabel){

        String[] ccAddresses;
        contact c = getContactInfo(true,selOrder);
        Store_Configuration__c s = getStoreConfigInfo(selOrder);
        if (c.email!=null && s.EmailToCustomers__c){
          ccAddresses = new String[] {c.email};
        }
        if (selOrder!=null && notess!=null){
          saveTheFile(selOrder, selOrder,notess,stopicLabel);
        }
        if (stopic.trim().length()!=0){
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { stopic };
            message.setCCAddresses(ccAddresses);
            message.optOutPolicy = 'FILTER';
            message.subject = 'Customer Portal Email Received';
            message.plainTextBody = notess;
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
                System.debug('The email failed to send: ' + results[0].errors[0].message);
            }
        }
        return 'Success';
    }
   public static Id saveTheFile(Id parentId, String topic, String tbody, String stopicLabel) {
       // base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

       String mesgbody='Topic: '+  stopicLabel +'\n' + 'message:' + tbody;
       Note addedntes = new Note();
       addedntes.ParentId = parentId;
       addedntes.Body = mesgbody;
       addedntes.Title = 'Customer Portal Email Received';
       insert addedntes;
       return null;
    }
    @AuraEnabled
    public static FAQWrap getAllOrders() {
        FAQWrap lr = new FAQWrap();
        Id userId = UserInfo.getUserId();
        User userRecord=[select id,ContactId from user where id=:userId];
        try{
            List<Order> orders = [SELECT Id,OrderNumber,OpportunityCloseDate__c,Order_Processed_Date__c,Store_Location__c, Store_Location__r.Legal_Name__c, Store_Location__r.Phone,
                                  Store_Location__r.BillingStreet, Store_Location__r.BillingCity, Store_Location__r.BillingPostalCode,EffectiveDate,
                                  Store_Location__r.BillingState, Store_Location__r.BillingCountry, Sales_Rep__c, Primary_Installer__c,
                                  Store_Location__r.Active_Store_Configuration__r.Scheduling__c,Store_Location__r.Active_Store_Configuration__r.Service__c,
                                  Store_Location__r.Active_Store_Configuration__r.Website_Feedback__c,Store_Location__r.Active_Store_Configuration__r.Other__c,
                                  Store_Location__r.Active_Store_Configuration__r.Installation__c,Store_Location__r.Active_Store_Configuration__r.Billing__c,
                                  Store_Location__r.Active_Store_Configuration__r.Customer_Service_Number__c,Store_Location__r.Active_Store_Configuration__r.Customer_Service_Number2__c,
                                  Store_Location__r.Active_Store_Configuration__r.Contact_Us_Variable_Topic__c,
                                  Store_Location__r.Active_Store_Configuration__r.Contact_Us_Variable_Topic_Email__c,
                                  Primary_Tech_Measure__c FROM Order WHERE (CustomerPortalUser__c = :userId or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId)))) ORDER BY createdDate DESC];
            lr.selectedOrdersList = orders;
            Store_Configuration__c sc;
            Map<String,List<PickWrap>> emailOptions = new Map<String,List<PickWrap>>();
            Map<String,Order> orderMap = new Map<String,Order>();
            for(order Ord: orders){
                List<PickWrap> pw = new List<PickWrap>();
                if(Ord.Store_Location__r.Active_Store_Configuration__r.Website_Feedback__c!=Null){
                    pickWrap p = new pickWrap();
                    p.strLabel= 'Website Feedback';
                    p.strValue= Ord.Store_Location__r.Active_Store_Configuration__r.Website_Feedback__c;
                    pw.add(p);
                }
                if(Ord.Store_Location__r.Active_Store_Configuration__r.Scheduling__c!=Null){
                    pickWrap p = new pickWrap();
                    p.strLabel= 'Scheduling';
                    p.strValue= Ord.Store_Location__r.Active_Store_Configuration__r.Scheduling__c;
                    pw.add(p);
                }
                if(Ord.Store_Location__r.Active_Store_Configuration__r.Service__c!=Null){
                    pickWrap p = new pickWrap();
                    p.strLabel= 'Service';
                    p.strValue= Ord.Store_Location__r.Active_Store_Configuration__r.Service__c;
                    pw.add(p);
                }
                if(Ord.Store_Location__r.Active_Store_Configuration__r.Installation__c!=Null){
                    pickWrap p = new pickWrap();
                    p.strLabel= 'Installation';
                    p.strValue= Ord.Store_Location__r.Active_Store_Configuration__r.Installation__c;
                    pw.add(p);
                }
                if(Ord.Store_Location__r.Active_Store_Configuration__r.Other__c!=Null){
                    pickWrap p = new pickWrap();
                    p.strLabel= 'Other';
                    p.strValue= Ord.Store_Location__r.Active_Store_Configuration__r.Other__c;
                    pw.add(p);
                }
                if(Ord.Store_Location__r.Active_Store_Configuration__r.Billing__c!=Null){
                    pickWrap p = new pickWrap();
                    p.strLabel= 'Billing';
                    p.strValue= Ord.Store_Location__r.Active_Store_Configuration__r.Billing__c;
                    pw.add(p);
                }
                if(Ord.Store_Location__r.Active_Store_Configuration__r.Contact_Us_Variable_Topic__c!=Null &&
                   Ord.Store_Location__r.Active_Store_Configuration__r.Contact_Us_Variable_Topic_Email__c!= Null){
                    pickWrap p = new pickWrap();
                    //p.strLabel= 'Contact Us Topic';
                    p.strLabel= Ord.Store_Location__r.Active_Store_Configuration__r.Contact_Us_Variable_Topic__c;
                    p.strValue= Ord.Store_Location__r.Active_Store_Configuration__r.Contact_Us_Variable_Topic_Email__c;
                    pw.add(p);
                }
                emailOptions.put(String.valueof(Ord.EffectiveDate),pw);
                orderMap.put(String.valueof(Ord.EffectiveDate),Ord);
            }
            lr.emailCatOptions = emailOptions;
            lr.orderMap = orderMap;
        } catch(exception e){

        }
        return lr;
    }
    public class FAQWrap{
        @AuraEnabled Public Map <String,List<PickWrap>> emailCatOptions;
        @AuraEnabled Public List<Order> selectedOrdersList;
        @AuraEnabled Public Map <String,Order> orderMap;

    }
    Public class PickWrap{

        @AuraEnabled Public String strValue;
        @AuraEnabled Public String strLabel;
    }

    @AuraEnabled
	public static Contact getContactInfo(Boolean primaryBool, String OrderId) {
       // Id userId = UserInfo.getUserId();
        Order ord = [SELECT Id, OpportunityId, CustomerPortalUser__c FROM Order WHERE id=:OrderId LIMIT 1];
        try{
            OpportunityContactRole oppCon = [SELECT Id, OpportunityId, ContactId
                                    FROM OpportunityContactRole
                                    WHERE OpportunityId = :ord.OpportunityId AND isPrimary = :primaryBool];
            Contact cont = [SELECT FirstName, LastName, MailingCity, MailingState, MailingPostalCode, MailingCountry, MailingStreet, MobilePhone, Email, Preferred_Method_of_Contact__c FROM Contact WHERE Id = :oppCon.ContactId];
            return cont;
        } catch(Exception npe){
            return null;
        }
	}
        
// For the CC:Email Check Box in the Store Configuration
	@AuraEnabled
    public static Store_Configuration__c getStoreConfigInfo(String OrderId) {
        if(String.isNotBlank(OrderId)){
        Order ord = [SELECT Id, Store_Location__c FROM Order WHERE Id=:OrderId LIMIT 1];
        //finding the Active Store configuration From Order
        Store_Configuration__c store = [Select Id,EmailToCustomers__c,Store__c,Customer_Service_Number__c FROM Store_Configuration__c WHERE Store__c =: ord.Store_Location__c LIMIT 1 ];
        return store;
        }
        else{
            return new Store_Configuration__c();
        }
       
    }
}