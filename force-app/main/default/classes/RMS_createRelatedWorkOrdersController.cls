public with sharing class RMS_createRelatedWorkOrdersController {

    public String woRecordTypeName { get; set; }

    public String strurl;
    public String baseUrl{get;set;}

    public string serviceOrderVisitRecordTypeId{get;set;}
    public string serviceOrderActionsRecordTypeId{get;set;}
    public string serviceOrderPermitRecordTypeId{get;set;}
    public string serviceOrderLSWPRecordTypeId{get;set;}
    public string serviceOrderPaintStainRecordTypeId{get;set;}
    public string serviceOrderCollectionsRecordTypeId{get;set;}

    public string serviceOrderVisitRecordTypeURL{get;set;}
    public string serviceOrderTechMeasureRecordTypeURL{get;set;}
	public string serviceOrderJobSiteVisitRecordTypeURL{get;set;}
    public string serviceOrderInstallRecordTypeURL{get;set;}
    public string serviceOrderActionsRecordTypeURL{get;set;}
    public string serviceOrderHistoricalRecordTypeURL{get;set;}
    public string serviceOrderHOARecordTypeURL{get;set;}
    public string serviceOrderLSWPRecordTypeURL{get;set;}
    public string serviceOrderPaintStainRecordTypeURL{get;set;}
    public string serviceOrderPermitRecordTypeURL{get;set;}
    public string serviceOrderCollectionsRecordTypeURL{get;set;}

    public boolean displayButtonsOnPage{get;set;}
    public boolean displayPaintStainOnly{get;set;}

    public String reportURL{get;set;}

    /******* Set up Standard Controller for Purchase_Order__c  *****************/
    private Apexpages.StandardController standardController;
    private final Order ord;


    //Constructor
    public RMS_createRelatedWorkOrdersController(ApexPages.StandardController stdController) {
        this.ord = (Order)stdController.getRecord();

        strurl = ApexPages.currentPage().getUrl();
        strurl = strurl.split('apex/')[1];
        baseUrl = URL.getSalesforceBaseUrl().toExternalForm();

        try {
            String netId = Network.getNetworkId();
            ConnectApi.Community comm = ConnectApi.Communities.getCommunity(netId);
            baseUrl = comm.siteUrl;
        } catch(Exception e) {

        }
        //TODO: use utility methods
        //list<RecordType> serviceOrderVisitRecordType = [SELECT id FROM RecordType WHERE SobjectType = 'RbA_Work_Order__c' AND Name = 'Visit'];
        //list<RecordType> serviceOrderActionsRecordType = [SELECT id FROM RecordType WHERE SobjectType = 'RbA_Work_Order__c' AND Name = 'Actions'];

        serviceOrderVisitRecordTypeId = UtilityMethods.retrieveRecordTypeId('Visit', 'WorkOrder');
        serviceOrderActionsRecordTypeId = UtilityMethods.retrieveRecordTypeId('Actions', 'WorkOrder');
        serviceOrderPermitRecordTypeId = UtilityMethods.retrieveRecordTypeId('Permit', 'WorkOrder');
        serviceOrderLSWPRecordTypeId = UtilityMethods.retrieveRecordTypeId('LSWP', 'WorkOrder');
        serviceOrderPaintStainRecordTypeId = UtilityMethods.retrieveRecordTypeId('Paint_Stain', 'WorkOrder');
        serviceOrderCollectionsRecordTypeId = UtilityMethods.retrieveRecordTypeId('Collections', 'WorkOrder');


        String ordId = ord.id;

        List<Report> installMatReport = [SELECT Id
                                         FROM Report
                                         WHERE DeveloperName = 'Install_Materials_Summary2'
                                         LIMIT 1];
        String reportId = '';
        if(installMatReport.size() > 0) {
            reportId = installMatReport[0].Id;
        }
        reportURL = '/'+reportId+'?pv0='+String.ValueOf(ord.Id).substring(0,15)+'&pv1=Construction_Materials';


        //building strings to use as URL for create new Workorder
        Schema.DescribeSObjectResult r = WorkOrder.sObjectType.getDescribe();
        String workOrderKeyPrefix = r.getKeyPrefix();

        list<Order> theOrd = [SELECT Id,
                                     AccountId,
                                     Status,
                                     Store_Location__c,
                                     OrderNumber,
                                     OpportunityId,
                                     BillToContactId,
                                     RecordTypeId
                              FROM Order
                              WHERE Id =:ordId ];
        Id orderServiceRecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
        Id changeOrderRecordTypeId = UtilityMethods.retrieveRecordTypeId('Change_Order', 'Order');

        // only display the paint stain button for change orders
        displayPaintStainOnly = (theOrd[0].RecordTypeId == changeOrderRecordTypeId) ? true : false;
        if(theOrd[0].Status == 'Draft' && theOrd[0].RecordTypeId != orderServiceRecordTypeId) {
            displayButtonsOnPage = false;
        } else if(theOrd[0].Status != 'Draft' || theOrd[0].RecordTypeId == orderServiceRecordTypeId) {
            displayButtonsOnPage = true;
        }

        List<Account> relatedAccount = [SELECT Name,
                                               ShippingStreet,
                                               ShippingCity,
                                               ShippingState,
                                               ShippingPostalCode,
                                               ShippingCountry,
                                               Historical__c,
                                               HOA__c,
                                               Building_Permit__c
                                        FROM Account
                                        WHERE Id = :theOrd[0].AccountId];


        Map<String, Field_IDs__c> fieldIdsMap = Field_IDs__c.getAll();
        Field_IDs__c workOrderFieldIds = fieldIdsMap.get('Work Order');

        String endURL = '&orderId='+Ord.Id +
                        '&storeId='+theOrd[0].Store_Location__c +
                        '&accountId='+relatedAccount[0].Id +
                        '&shippingStreet='+removeLineBreaks(relatedAccount[0].ShippingStreet) +
                        '&shippingCity='+relatedAccount[0].ShippingCity +
                        '&shippingState='+relatedAccount[0].ShippingState +
                        '&shippingPostalCode='+relatedAccount[0].ShippingPostalCode +
                        '&shippingCountry='+relatedAccount[0].ShippingCountry +
                        '&relatedOpp='+theOrd[0].OpportunityId +
                        '&billToContactId='+theOrd[0].BillToContactId +
                        '&permit='+relatedAccount[0].Building_Permit__c +
                        '&hoa='+relatedAccount[0].HOA__c +
                        '&historical='+relatedAccount[0].Historical__c;


        serviceOrderTechMeasureRecordTypeURL =    '/apex/RMS_newWorkOrderRedirect?type=techMeasure'+endURL;
		
		serviceOrderJobSiteVisitRecordTypeURL =   '/apex/RMS_newWorkOrderRedirect?type=jobSiteVisit'+endURL;

        serviceOrderInstallRecordTypeURL =    '/apex/RMS_newWorkOrderRedirect?type=install'+endURL;

        serviceOrderActionsRecordTypeURL =  '/apex/RMS_newWorkOrderRedirect?type=actions'+endURL;

        serviceOrderHistoricalRecordTypeURL = '/apex/RMS_newWorkOrderRedirect?type=historical'+endURL;

        serviceOrderHOARecordTypeURL = '/apex/RMS_newWorkOrderRedirect?type=hoa'+endURL;

        serviceOrderLSWPRecordTypeURL = '/apex/RMS_newWorkOrderRedirect?type=lswp'+endURL;

        serviceOrderPaintStainRecordTypeURL = '/apex/RMS_newWorkOrderRedirect?type=paintStain'+endURL;

        serviceOrderPermitRecordTypeURL = '/apex/RMS_newWorkOrderRedirect?type=permit'+endURL;

        serviceOrderCollectionsRecordTypeURL = '/apex/RMS_newWorkOrderRedirect?type=Collections'+endURL;

    }

    public PageReference createWorkOrder() {
        List<RecordType> recordTypes = [SELECT Id FROM RecordType WHERE SObjectType = 'WorkOrder' AND DeveloperName = :woRecordTypeName];

        return null;
    }

    public String removeLineBreaks(String s) {
        if (s == null){
            return s;
        }
        String noBreaks = s.replace('\r\n', ' ');
        noBreaks = noBreaks.replace('\n', ' ');
        noBreaks = noBreaks.replace('\r', ' ');
        return noBreaks;
    }

}