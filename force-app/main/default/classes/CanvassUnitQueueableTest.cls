@isTest(SeeAllData=false)
global class CanvassUnitQueueableTest {

    public static testMethod void test1()
    {
        CNVSS_Canvass_Market__c canvassMarket = new CNVSS_Canvass_Market__c();
        canvassMarket.Name = 'Test';
        canvassMarket.CNVSS_Canvass_Market_Manager__c = userInfo.getUserId();
        canvassMarket.CNVSS_ISC_Phone_Number__c = '5555555555';
        insert canvassMarket;
        
        Canvass_Market_Zip_Code__c canvassMarketZip = new Canvass_Market_Zip_Code__c();
        canvassMarketZip.Name = '28217';
        canvassMarketZip.Zip_Code__c = '28217';
        canvassMarketZip.Canvass_Market__c = canvassMarket.id;
        insert canvassMarketZip;
        
        Custom_Data_Layer_Batch_Queue__c canvassDataLayerQueue = new Custom_Data_Layer_Batch_Queue__c();
        canvassDataLayerQueue.Status__c = 'Holding';
        canvassDataLayerQueue.Field_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.ZipCode__c = '60609';
        canvassDataLayerQueue.Market_Id__c = canvassMarket.Id;
        insert canvassDataLayerQueue;
        
        set<string> zipSet = new set<string>();
        zipSet.add(canvassMarketZip.Zip_Code__c);
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('MockPropertyDataFields');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
                
        list<Custom_Data_Layer_Batch_Queue__c> scope = [SELECT Id, Name, Field_Ids__c, Marker_Ids__c, Marker_Ids2__c, Market_Id__c, Status__c, ZipCode__c 
                                                        FROM Custom_Data_Layer_Batch_Queue__c 
                                                        WHERE Status__c = 'Holding'];
        // iterate over scope
        for(Custom_Data_Layer_Batch_Queue__c queuedJob :scope)
        {
            list<string> fieldIdList = queuedJob.Field_Ids__c.split(', ');
            list<string> markerIdList = queuedJob.Marker_Ids__c.split(', ');
            list<string> markerIdList2 = new list<string>();
            
            if(queuedJob.Marker_Ids2__c != null)
            {
                markerIdList2 = queuedJob.Marker_Ids2__c.split(', ');
            }
            
            //set<string> zipCodeSet = new set<string>();
            //zipCodeSet.add(queuedJob.ZipCode__c);
            
            list<object> fieldIdListObj = new list<object>();
            for(string field :fieldIdList)
            {
                fieldIdListObj.add(field);
            }
            
            list<object> markerIdListObj = new list<object>();
            for(string marker :markerIdList)
            {
                markerIdListObj.add(integer.valueOf(marker));
            }
            
            for(string marker :markerIdList2)
            {
                markerIdListObj.add(integer.valueOf(marker));
            }
            
            // start queueable job
            System.enqueueJob(new CanvassUnitQueueable(fieldIdListObj,markerIdListObj,queuedJob.ZipCode__c,queuedJob.Market_Id__c, queuedJob.Id));
            System.enqueueJob(new CanvassUnitQueueable(fieldIdListObj,markerIdListObj,queuedJob.ZipCode__c,queuedJob.Market_Id__c, queuedJob.Id, 15000));
            
            // update status so that job won't be queued again
            queuedJob.Status__c = 'Waiting for Insertion And Update';
            update queuedJob;
        }
        
        Test.stopTest();
    }
}