/**
 * @File Name          : SalesSchedCalSalesRepMetricsManualSched.cls
 * @Description        : 
 * @Author             : Sundeep Goddety (Three Bridge)
 * @Group              : 
 * @Last Modified By   : Sundeep Goddety (Three Bridge)
 * @Last Modified On   : 8/1/2019, 1:22:16 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    8/1/2019, 1:22:16 PM   Sundeep Goddety (Three Bridge)   Initial Version
**/


public class SalesSchedCalSalesRepMetricsManualSched implements Schedulable{
 public static date nextdate1;
    public void execute(SchedulableContext sc){
		/*Integer storeClose = Integer.valueof(system.label.StoreCloseTime);
        Time storeCloseTime = Time.newInstance(storeClose,0,0,0);
        system.debug('storeCloseTime------>'+storeCloseTime);
        Date nextDate = setNextDateManual(storeCloseTime);
       // SalesSchedDateSchedule.executeMetrics(nextDate, storeCloseTime);*/
        set<Id> activeConglist =  getActiveStoreConfig();
         system.debug('nextDate1---------------->'+nextDate1);
        system.debug('activeConglist---------------->'+activeConglist);
        Database.executeBatch(new SalesSchedCalSalesRepMetricsManualBatch(nextDate1 ,activeConglist),100);

    }
    
    /**
     * @ Method is used for getting the Active Store Configuration Call Center Close Time and Current Sales Date.
     */
    
    /* public static Date setNextDateManual(Time storeCloseTime){
        List<Store_Configuration__c> configs = [Select Current_Sales_Date__c From Store_Configuration__c Where Call_Center_Close_Time__c = :storeCloseTime];
        system.debug(configs);
         Date currentSalesDate = null;
        for(Store_Configuration__c config : configs){
            if(currentSalesDate == null || config.Current_Sales_Date__c > currentSalesDate){
                currentSalesDate = config.Current_Sales_Date__c.addDays(1);
            }
        }
    
        return currentSalesDate;
    } */
    
    public static set<Id> getActiveStoreConfig(){
        Set<Id> storeIds = new set<Id>();
        Date currentSalesDate = null;
        for(Store_Configuration__c sc:[select Id,MaualBatchApex__c,Current_Sales_Date__c from Store_Configuration__c where MaualBatchApex__c =true ]){
	          if(currentSalesDate == null || sc.Current_Sales_Date__c > currentSalesDate){
                currentSalesDate = sc.Current_Sales_Date__c.addDays(1);
            }
            storeIds.add(sc.Id);
        }
        nextdate1 = currentSalesDate;
        return storeIds;
    }
}