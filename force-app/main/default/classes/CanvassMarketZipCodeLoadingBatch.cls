global class CanvassMarketZipCodeLoadingBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful
{
    //database.executebatch(new CanvassMarketZipCodeLoadingBatch(),1);
    
    private set<string> zipCodeSet;
    private boolean doAnotherBatchOfZipCodes;
    private boolean doAnotherBatchOfMarket;
    private string marketId;
    private string marketName;
    private set<string> marketIdSet;
    
    global CanvassMarketZipCodeLoadingBatch()
    {
        zipCodeSet = new set<string>();
        doAnotherBatchOfZipCodes = false;
        doAnotherBatchOfMarket = false;
        marketId = null;
        marketIdSet = new set<string>();
    }
    
    global CanvassMarketZipCodeLoadingBatch(string oldMarketId, set<string> oldzipCodeSet, set<string> oldMarketIdSet)
    {
        marketId = oldMarketId;
        zipCodeSet = oldzipCodeSet;
        doAnotherBatchOfZipCodes = false;
        doAnotherBatchOfMarket = false;
        marketIdSet = oldMarketIdSet;
    }
    
    global CanvassMarketZipCodeLoadingBatch(set<string> oldMarketIdSet)
    {
        zipCodeSet = new set<string>();
        doAnotherBatchOfZipCodes = false;
        doAnotherBatchOfMarket = false;
        marketId = null;
        marketIdSet = oldMarketIdSet;
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        if(marketId == null)
        {
            return Database.getQueryLocator ([SELECT Id, Name
                                              FROM CNVSS_Canvass_Market__c
                                              WHERE Id NOT IN :marketIdSet
                                              LIMIT 1]);
        }
        else
        {
            return Database.getQueryLocator ([SELECT Id, Name
                                              FROM CNVSS_Canvass_Market__c
                                              WHERE Id = :marketId
                                              LIMIT 1]);
        }
    }
    
    global void execute (Database.BatchableContext BC, List<CNVSS_Canvass_Market__c> scope)
    {
        try
        {
            for(CNVSS_Canvass_Market__c market :scope)
            {
                marketId = market.Id;
                marketName = market.Name;
                
                List<CNVSS_Canvass_Unit__c> unitZips = [SELECT CNVSS_Zip_Code__c 
                                        			    FROM CNVSS_Canvass_Unit__c 
                            				            WHERE CNVSS_Canvass_Market__c = :marketId
                                                        AND CNVSS_Zip_Code__c NOT IN :zipCodeSet
                                                        LIMIT 49999];
                                                        
                //check to see if we need to do another batch
                if(unitZips.size() == 49999)
                {
                    doAnotherBatchOfZipCodes = true;
                }
                else
                {
                    system.debug('Less than 49999');
                }
                                                         
                for(CNVSS_Canvass_Unit__c zip :unitZips)
                {
                	zipCodeSet.add(zip.CNVSS_Zip_Code__c);
                }
                
                doAnotherBatchOfMarket = true;
            }
        }
        catch(Exception e)
        {
            Custom_Data_Layer_Batch_Tracking__c customDataLayer = new Custom_Data_Layer_Batch_Tracking__c();
            customDataLayer.Error__c = string.valueOf(e);
            customDataLayer.Error_Line_Number__c = string.valueOf(e.getLineNumber());
            customDataLayer.Error_Message__c = string.valueOf(e.getMessage());
            customDataLayer.Job_Name__c = 'CanvassMarketZipCodeLoadingBatch';
            insert customDataLayer;
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        try
        {
            marketIdSet.add(marketId);
            
            if(doAnotherBatchOfZipCodes == true)
            {
                database.executebatch(new CanvassMarketZipCodeLoadingBatch(marketId, zipCodeSet,marketIdSet),1);
            }
            else
            {
                list<string> zipCodeList = new list<string>();
                zipCodeList.addAll(zipCodeSet);
                
                list<Canvass_Market_Zip_Code__c> canvasUnitZipCodeList = new list<Canvass_Market_Zip_Code__c>();
                for(integer i = 0; i < zipCodeList.size(); i++)
                {
                    Canvass_Market_Zip_Code__c canvasUnit = new Canvass_Market_Zip_Code__c();
                    canvasUnit.Name = zipCodeList[i]+' - '+marketName;
                    canvasUnit.Zip_Code__c =  zipCodeList[i];
                    canvasUnit.Canvass_Market__c = marketId;
                    canvasUnitZipCodeList.add(canvasUnit);
                }
                
                insert canvasUnitZipCodeList;
    
                if(doAnotherBatchOfMarket == true)
                {
                    database.executeBatch(new CanvassMarketZipCodeLoadingBatch(marketIdSet),1);
                }
            }
        }
        catch(Exception e)
        {
            Custom_Data_Layer_Batch_Tracking__c customDataLayer = new Custom_Data_Layer_Batch_Tracking__c();
            customDataLayer.Error__c = string.valueOf(e);
            customDataLayer.Error_Line_Number__c = string.valueOf(e.getLineNumber());
            customDataLayer.Error_Message__c = string.valueOf(e.getMessage());
            customDataLayer.Job_Name__c = 'CanvassMarketZipCodeLoadingBatch';
            insert customDataLayer;
        }
    }
}