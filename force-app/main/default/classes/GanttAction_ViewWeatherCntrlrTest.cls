@isTest
public class GanttAction_ViewWeatherCntrlrTest {
    private static final String WORK_TYPE_NAME = 'Test Type 1';
    private static final String INSTALL_WORK_TYPE_NAME = 'Install';
    private static final String MEASURE_WORK_TYPE_NAME = 'Measure';
    private static final String SERVICE_WORK_TYPE_NAME = 'Service';
    private static final String JOB_SITE_VISIT_WORK_TYPE_NAME = 'Job Site Visit';

    @TestSetup
    static void makeData(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

//        FSLRollupRolldownController.disableFSLRollupRolldownController = true;

        OperatingHours oh1 = new OperatingHours();
        oh1.Name = 'Test Operating Hours One';
        oh1.TimeZone = 'America/Chicago';
        insert oh1;

        // OperatingHours oh2 = new OperatingHours();
        // oh2.Name = 'Test Operating Hours Two';
        // oh2.TimeZone = 'America/New_York';
        // insert new List<OperatingHours>{oh1, oh2};

        ServiceTerritory st1 = new ServiceTerritory();
        st1.Name = 'Test Service Territory One';
        st1.OperatingHoursId = oh1.Id;
        st1.IsActive = true;
        insert st1;

        // ServiceTerritory st2 = new ServiceTerritory();
        // st2.Name = 'Test Service Territory Two';
        // st2.OperatingHoursId = oh2.Id;
        // st2.IsActive = true;
        // insert st2;

        // ServiceAppointment sa1 = new ServiceAppointment();
        // sa1.ServiceTerritoryId = st1.id;
        // insert sa1;

        WorkType workType = new WorkType(
            Name = WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 4,
            DurationType = 'Hours'
        );
        
        WorkType installWorkType = new WorkType(
            Name = INSTALL_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType measureWorkType = new WorkType(
            Name = MEASURE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType serviceWorkType = new WorkType(
            Name = SERVICE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );

        WorkType jobSiteVisitWorkType = new WorkType(
            Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 1,
            DurationType = 'Hours'
        );
        insert new List<WorkType>{worktype, installWorkType, measureWorkType, serviceWorkType, jobSiteVisitWorkType};

        WorkOrder measureWO = new WorkOrder(
            Recommended_Crew_Size__c = 2,
            Street = '219 North Milwaukee Street',
            City = 'Milwaukee',
            State = 'Wisconsin',
            PostalCode = '53208',
            CountryCode = 'US',
            WorkTypeId = measureWorkType.Id,
            ServiceTerritoryId = st1.Id
        );
        insert measureWO;

//        FSLRollupRolldownController.disableFSLRollupRolldownController = false; 
    }

    @isTest static void testConstructor() {
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        ServiceAppointment sa = TeamSchedulingController.getOrCreateServiceAppointment(workOrderId);
        sa.Street = '219 North Milwaukee Street';
        sa.City = 'Milwaukee';
        sa.State = 'Wisconsin';
        sa.Latitude = 43.033248;
        sa.Longitude = -87.906276;
        insert sa;

        Test.startTest();
        ApexPages.currentPage().getParameters().put('id', sa.Id);
        GanttAction_ViewWeatherController controller = new GanttAction_ViewWeatherController();
        Test.stopTest();

        system.assertEquals(sa.Id, controller.sa.Id);
        system.assertEquals(43.033248.intValue(), Double.valueOf(controller.lat).intValue());
        system.assertEquals(-87.906276.intValue(), Double.valueOf(controller.lng).intValue());
        system.assertEquals('219 North Milwaukee Street, Milwaukee, Wisconsin', controller.address);

        system.debug('****** ViewWeather Test');

    }

    @isTest static void testUpdateForecast() {
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        ServiceAppointment appointment = TeamSchedulingController.getOrCreateServiceAppointment(workOrderId);
        appointment.SchedStartTime = Datetime.now();
        appointment.SchedEndTime = Datetime.now().addHours(4);
        insert appointment;

		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('MockAerisResponse');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
            String result = GanttAction_ViewWeatherController.updateForecast(appointment.Id);
        Test.stopTest();

        System.assertEquals('Success', result);

    }
}