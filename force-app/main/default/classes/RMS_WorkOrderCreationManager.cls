public class RMS_WorkOrderCreationManager {
    public static void createWorkOrderOnOrderSoldOrderBeingAssigned(List<Order> listOld, List<Order> listNew, Map<Id, Order> mapOld, Map<Id, Order> mapNew) {
        Id serviceWORecordType = UtilityMethods.retrieveRecordTypeId('Service', 'WorkOrder');
        Id orderCoroServiceRecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
        List<Order> listOrdersBeingAssigned = new List<Order>();
        
        // Loop through the new order triggers and identify if any meet the criteria for creating a work order
        system.debug('### listNew.size: '+listNew.size());
        for(Order ord : listNew){
            if(ord.Service_Type__c == 'Save' || String.isBlank(ord.Service_Type__c)){
                continue;
            }
            if(ord.RecordTypeId == orderCoroServiceRecordTypeId){
                if(mapOld.get(ord.Id).Sold_Order__c == null && mapNew.get(ord.Id).Sold_Order__c != null ){
                    listOrdersBeingAssigned.add(ord);
                    system.debug('geet'+listOrdersBeingAssigned.size());
                }
            }
        }
        // If there are no orders being assigned, exit this method
        system.debug('### listOrdersBeingAssigned.size: '+listOrdersBeingAssigned.size());
        if (listOrdersBeingAssigned.size() == 0){
            return;
        }
        list<WorkOrder> workOrdersToAdd = new list<WorkOrder>();
        list<id> relatedAccountIds = new list<Id>();
        list<id> relatedOpportunityIds = new list<id>();
        list<id> relatedParentIds = new list<id>();

        for(Order o : listOrdersBeingAssigned){
            relatedAccountIds.add(o.AccountId);
            //IF OPPORTUNITY POPULATED THEN ADD TO LIST
            if(o.OpportunityId != null){
                relatedOpportunityIds.add(o.OpportunityId);
            }
            if(o.Sold_Order__c != null){
                relatedParentIds.add(o.Sold_Order__c);
            }
        }
        list<Account> relatedAccounts = [SELECT ShippingStreet,ShippingCity,ShippingState,ShippingStateCode,
                                                ShippingPostalCode,ShippingCountry,ShippingCountryCode,Store_Location__c,
                                                Name,Store_Location__r.Active_Store_Configuration__c
                                                FROM Account WHERE Id IN :relatedAccountIds];
        map<id, Account> accountIdToAccountMap = new map<id, Account>();
        map<id, id> storeIdToStoreConfigIdMap = new map<id, id>();

        for(Account a : relatedAccounts){
            accountIdToAccountMap.put(a.id, a);
            if (a.Store_Location__r.Active_Store_Configuration__c != null) {
                storeIdToStoreConfigIdMap.put(a.Store_Location__c, a.Store_Location__r.Active_Store_Configuration__c);
            }
        }
        map<Id, Store_Configuration__c> accountIdtoStoreConfigMap = UtilityMethods.mapStoreConfigs(storeIdToStoreConfigIdMap);
        map<id, Order> orderIdToOrderMap = new map<id, Order>([SELECT Id, BillToContactID, AccountId, OpportunityId FROM Order WHERE Id IN :relatedParentIds]);

        for(Order ord: listOrdersBeingAssigned){
            Account relatedDwelling = accountIdToAccountMap.get(ord.AccountId);
            Id serviceQueueId;
            
            if(accountIdtoStoreConfigMap.get(relatedDwelling.Store_Location__c).Service_Work_Order_Queue_ID__c != null){
                serviceQueueId = accountIdtoStoreConfigMap.get(relatedDwelling.Store_Location__c).Service_Work_Order_Queue_ID__c;
            }
            else{
                serviceQueueId = UserInfo.getUserId();
            }
            WorkOrder serviceWorkOrder = new WorkOrder(
                    Sold_Order__c = ord.id,
                    RecordTypeId = serviceWORecordType,
                    Work_Order_Type__c = 'Service',
                    AccountId = ord.AccountId,
                    ContactId = ord.BillToContactId,
                    Opportunity__c = ord.OpportunityId,
                    Street = relatedDwelling.ShippingStreet,
                    City = relatedDwelling.ShippingCity,
                    State = relatedDwelling.ShippingState,
                    PostalCode = relatedDwelling.ShippingPostalCode,
                    Country = relatedDwelling.ShippingCountry,
                    OwnerId = serviceQueueId
            );
            if(ord.Service_Type__c != 'Save'){
                workOrdersToAdd.add(serviceWorkOrder);
                system.debug('geet'+workOrdersToAdd.size());
            }
        }
        if (!workOrdersToAdd.isEmpty()){
            insert workOrdersToAdd;
           OrderTriggerHandler.isWorkOrdersCreated = true;
        }
    }
    public static void createWorkOrderOnOrderActivation(List<Order> listOld, List<Order> listNew, Map<Id, Order> mapOld, Map<Id, Order> mapNew) {
        List<Id> activatedOrderId = new List<Id>();
        List<Id> relatedDwellingIds = new List<Id>();
        //BUILDING MAPS OF ORDER ID TO LIST OF WORK ORDERS BASED ON WO TYPE TO USE WHILE LOOPING THROUGH ORDERS
        Map<Id, List<WorkOrder>> ordIdtoListOfLSWPWOMap = new Map<Id, List<WorkOrder>>();
        Map<Id, List<WorkOrder>> ordIdtoListOfHOAWOMap = new Map<Id, list<WorkOrder>>();
        Map<Id, List<WorkOrder>> ordIdtoListOfBuildingPermitWOMap = new Map<Id, List<WorkOrder>>();
        Map<Id, List<WorkOrder>> ordIdtoListOfHistoricalWOMap = new Map<Id, List<WorkOrder>>();
        Map<Id, List<WorkOrder>> ordIdtoListOfTechWOMap = new Map<Id, List<WorkOrder>>();
        Map<Id, List<WorkOrder>> ordIdtoListOfPaintStainWOMap = new Map<Id, List<WorkOrder>>();
        
        //RETREVING RECORD TYPES USED IN WORK ORDER CREATION
        Id permitWORecordType = UtilityMethods.retrieveRecordTypeId('Permit', 'WorkOrder');
        Id hoaWORecordType = UtilityMethods.retrieveRecordTypeId('HOA', 'WorkOrder');
        Id LSWPWORecordType = UtilityMethods.retrieveRecordTypeId('LSWP', 'WorkOrder');
        Id historicalRecordType = UtilityMethods.retrieveRecordTypeId('Historical', 'WorkOrder');
        Id techMeasureRecordType = UtilityMethods.retrieveRecordTypeId('Tech_Measure', 'WorkOrder');
        Id paintStainWORecordType = UtilityMethods.retrieveRecordTypeId('Paint_Stain', 'WorkOrder');
        
        //FILTERING OUT NON ACTIVATED ORDERS AS WELL AS BUILDING THE LIST OF DWELLINGS TO GET THEIR STORE CONFIGS
        for(Order ord : listNew){
           system.debug('### ord.Status: '+ord.Status);
            system.debug('### mapOld.containsKey(ord.Id): '+mapOld.containsKey(ord.Id));
           if((ord.Status == 'Tech Measure Scheduled' || ord.Status == 'Tech Measure Needed') && mapOld.containsKey(ord.Id) && mapOld.get(ord.Id).Status == 'Draft'){
                relatedDwellingIds.add(ord.AccountId);
                activatedOrderId.add(ord.id);
            }
        }
        //IF NO ORDERS HAVE BEEN ACTIVATED THEN SKIP REST OF LOGIC
        system.debug('### activatedOrderId.size: '+activatedOrderId.size());
        if(activatedOrderId.size() == 0){
            return;
        }
        List<Order> activatedOrders = [SELECT Id, AccountId, BillToContactID, OpportunityId from Order where Id in :activatedOrderId];
        
        Map<Id,Id> storeIdToStoreConfigIdMap = new Map<Id,Id>();
        //LOOPING THROUGH ALL DWELLINGS GETTING THEIR STORE LOCATIONS TO USE TO GET STORE CONFIGS
        for(Account dwelling : [SELECT id, Store_Location__c,Store_Location__r.Active_Store_Configuration__c FROM Account WHERE Id IN :relatedDwellingIds]){
            if(dwelling.Store_Location__c != null && dwelling.Store_Location__r.Active_Store_Configuration__c != null)
                storeIdToStoreConfigIdMap.put(dwelling.Store_Location__c, dwelling.Store_Location__r.Active_Store_Configuration__c);
        }
        //GETTING ALL STORE CONFIGS FOR RELATED DWELLINGS
        map<Id, Store_Configuration__c> accountIdtoStoreConfigMap = UtilityMethods.mapStoreConfigs(storeIdToStoreConfigIdMap);
        
        //QUERY FOR FIELDS ON DWELLING TO POPULATE WORK ORDER
        list<Account> relatedDwelling = [SELECT Id,Store_Location__c,HOA__c, Historical__c,Building_Permit__c,
                                                ShippingStreet,ShippingCity,ShippingState,ShippingStateCode,
                                                ShippingPostalCode,ShippingCountry,ShippingCountryCode,
                                                Year_Built__c,Name,Store_Location__r.Active_Store_Configuration__c
                                                FROM Account Where Id IN : relatedDwellingIds];
        system.debug('### relatedDwelling.size: '+relatedDwelling.size());
        List<WorkOrder> relatedWorkOrders = [SELECT id, Sold_Order__c, Work_Order_Type__c, RecordTypeId FROM WorkOrder WHERE Sold_Order__c IN :activatedOrderId];
        system.debug('### relatedWorkOrders.size: '+relatedWorkOrders.size());
        for(WorkOrder wo : relatedWorkOrders){
            if(wo.Work_Order_Type__c == 'LSWP'){
                if(ordIdtoListOfLSWPWOMap.containsKey(wo.Sold_Order__c)){
                    ordIdtoListOfLSWPWOMap.get(wo.Sold_Order__c).add(wo);
                }else{
                    ordIdtoListOfLSWPWOMap.put(wo.Sold_Order__c, new list<WorkOrder>{wo});
                }
            }else if(wo.Work_Order_Type__c == 'HOA'){
                if(ordIdtoListOfHOAWOMap.containsKey(wo.Sold_Order__c)){
                    ordIdtoListOfHOAWOMap.get(wo.Sold_Order__c).add(wo);
                }else{
                    ordIdtoListOfHOAWOMap.put(wo.Sold_Order__c, new list<WorkOrder>{wo});
                }
            }else if(wo.Work_Order_Type__c == 'Building Permit'){
                if(ordIdtoListOfBuildingPermitWOMap.containsKey(wo.Sold_Order__c)){
                    ordIdtoListOfBuildingPermitWOMap.get(wo.Sold_Order__c).add(wo);
                }else{
                    ordIdtoListOfBuildingPermitWOMap.put(wo.Sold_Order__c, new list<WorkOrder>{wo});
                }
            }else if(wo.Work_Order_Type__c == 'Historical'){
                if(ordIdtoListOfHistoricalWOMap.containsKey(wo.Sold_Order__c)){
                    ordIdtoListOfHistoricalWOMap.get(wo.Sold_Order__c).add(wo);
                }else{
                    ordIdtoListOfHistoricalWOMap.put(wo.Sold_Order__c, new list<WorkOrder>{wo});
                }
            }else if(wo.Work_Order_Type__c == 'Tech Measure'){
                if(ordIdtoListOfTechWOMap.containsKey(wo.Sold_Order__c)){
                    ordIdtoListOfTechWOMap.get(wo.Sold_Order__c).add(wo);
                }else{
                    ordIdtoListOfTechWOMap.put(wo.Sold_Order__c, new list<WorkOrder>{wo});
                }
            }else if(wo.Work_Order_Type__c == 'Paint/Stain'){
                if(ordIdtoListOfPaintStainWOMap.containsKey(wo.Sold_Order__c)){
                    ordIdtoListOfPaintStainWOMap.get(wo.Sold_Order__c).add(wo);
                }else{
                    ordIdtoListOfPaintStainWOMap.put(wo.Sold_Order__c, new list<WorkOrder>{wo});
                }
            }
        }
        //BUILDING MAP OF ID TO DWELLING TO USE WHILE LOOPING THROUGH ORDERS
        map<id, Account> dwellingIdtoDwellingMap = new map<id, Account>();
        for(Account dwel : relatedDwelling){
            dwellingIdtoDwellingMap.put(dwel.id, dwel);
        }
        
        //LIST OF ORDERS TO INSERT AFTER FOR LOOP
        list<WorkOrder> workOrdersToAdd = new list<WorkOrder>();
        //LOOP THROUGH ACTIVATED ORDERS AND CREATE WORK ORDERS
        Map<Id,List<OrderItem>> mapOrderIdToItems = new Map<Id,List<OrderItem>>();
        List<OrderItem> listOIs;
        for(OrderItem oi : [SELECT Id,Notes__c,Status__c,OrderId,Pricebookentry.Product2.Paint_Stain_Misc_Job_and_Unit_Charges__c
                                FROM OrderItem WHERE OrderId IN :activatedOrders AND Status__c != 'Cancelled' 
                                AND Pricebookentry.Product2.Paint_Stain_Misc_Job_and_Unit_Charges__c = TRUE]){
            listOIs = mapOrderIdToItems.containsKey(oi.OrderId)? mapOrderIdToItems.get(oi.OrderId) : new List<OrderItem>();
            listOIs.add(oi);
            mapOrderIdToItems.put(oi.OrderId,listOIs);
        }
        for(Order ord : activatedOrders){
            Account dwelling = dwellingIdtoDwellingMap.get(ord.AccountId);
            Store_Configuration__c storeConfig;
            
            //Loops through the Order Items to see if there are any Paint/Stain items that ares Misc Job and Unit Charge type, excludes Cancelled Order Items
            map<id, list<OrderItem>> relatedOrderItemsToOrder = new map<id, list<OrderItem>>();
            list<OrderItem> relatedOrderItems = new list<OrderItem>();
            
            if(mapOrderIdToItems.containsKey(ord.Id)){
                for(OrderItem oi : mapOrderIdToItems.get(ord.Id)){
                    relatedOrderItems.add(oi);
                    if(relatedOrderItemsToOrder.containsKey(oi.OrderId)){
                        relatedOrderItemsToOrder.remove(oi.OrderId);
                        relatedOrderItemsToOrder.put(oi.OrderId, relatedOrderItems);
                    }
                    else{
                        relatedOrderItemsToOrder.put(oi.OrderId, relatedOrderItems);
                    }
                }
            }
            system.debug('******related OrderItems for Paint and/or Stain WO creation--'+relatedOrderItemsToOrder.size());
            
            //NULL CHECKS AND ERRORS IF NO STORE LOCATION OR NO STORE CONFIGURATION
            system.debug('### dwelling.Store_Location__c: '+dwelling.Store_Location__c);
            system.debug('### accountIdtoStoreConfigMap.get(dwelling.Store_Location__c): '+accountIdtoStoreConfigMap.get(dwelling.Store_Location__c));
            if(dwelling.Store_Location__c == null){
                ord.addError('Store has not been set on dwelling.');
                system.debug('### Store has not been set on dwelling.');
            }
            else if(accountIdtoStoreConfigMap.get(dwelling.Store_Location__c) == null){
                ord.addError('Store Configuration   has not been created.');
                system.debug('### Store Configuration   has not been created.');
            }
            else{
                //GET STORE CONFIG FROM MAP
                storeConfig = accountIdtoStoreConfigMap.get(dwelling.Store_Location__c);
                //CREATE HOA WORK ORDER IF DWELLING HAS HOA POPULATED
                if(ordIdtoListOfHOAWOMap.get(ord.id) == null || ordIdtoListOfHOAWOMap.get(ord.id).size() == 0){
                    system.debug('### inside else.1: '+dwelling.HOA__c);
                    if(dwelling.HOA__c != null){
                        WorkOrder hoaWorkOrder = UtilityMethods.buildWorkOrder(dwelling, ord, hoaWORecordType, storeConfig.HOA_Work_Order_Queue_Id__c, 'HOA',  dwelling.HOA__c);
                        workOrdersToAdd.add(hoaWorkOrder);
                    }
                }
                //CREATE HISTORICAL WORK ORDER IF DWELLING HAS HISTORICAL POPULATED
                if(ordIdtoListOfHistoricalWOMap.get(ord.id) == null || ordIdtoListOfHistoricalWOMap.get(ord.id).size() == 0){
                    system.debug('### inside else.2: '+dwelling.Historical__c);
                    if(dwelling.Historical__c != null){
                        WorkOrder historicalWorkOrder = UtilityMethods.buildWorkOrder(dwelling, ord, historicalRecordType, storeConfig.Historical_Work_Order_Queue_Id__c, 'Historical', dwelling.Historical__c );
                        workOrdersToAdd.add(historicalWorkOrder);
                    }
                }
                //CREATE PERMIT WORK ORDER IF DWELLING HAS BUILDING PERMIT POPULATED
                if(ordIdtoListOfBuildingPermitWOMap.get(ord.id) == null || ordIdtoListOfBuildingPermitWOMap.get(ord.id).size() == 0){
                    system.debug('### inside else.3: '+dwelling.Building_Permit__c);
                    if(dwelling.Building_Permit__c != null){
                        WorkOrder buildingPermitWorkOrder = UtilityMethods.buildWorkOrder(dwelling, ord, permitWORecordType, storeConfig.Building_Permit_Work_Order_Queue_Id__c, 'Building Permit', dwelling.Building_Permit__c );
                        workOrdersToAdd.add(buildingPermitWorkOrder);
                    }
                }
                //CREATE LSWP WORK ORDER IF NO LSWP WORK ORDER BU DWELLING Year_Built__c IS NULL OR BEFORE  1978
                if(ordIdtoListOfLSWPWOMap.get(ord.id) == null || ordIdtoListOfLSWPWOMap.get(ord.id).size() == 0){
                    system.debug('### inside else.4-dwelling.Year_Built__c: '+dwelling.Year_Built__c);
                    if(dwelling.Year_Built__c == null || dwelling.Year_Built__c == ''  || Integer.valueof(Dwelling.Year_Built__c) < 1978){
                        WorkOrder LSWPWorkOrder = UtilityMethods.buildWorkOrder(dwelling, ord, LSWPWORecordType, storeConfig.LSWP_WO_Owner_ID__c, 'LSWP', null );
                        workOrdersToAdd.add(LSWPWorkOrder);
                    }
                    //CREATE TECH MEASURE WORK ORDER IF NO TECH MEASURE WORK ORDER
                    system.debug('### inside else.4.1: '+ordIdtoListOfTechWOMap.get(ord.id));
                    if(ordIdtoListOfTechWOMap.get(ord.id) == null || ordIdtoListOfTechWOMap.get(ord.id).size() == 0){
                        WorkOrder LSWPWorkOrder = UtilityMethods.buildWorkOrder(dwelling, ord, techMeasureRecordType, storeConfig.Tech_Measure_Work_Order_Queue_Id__c, 'Tech Measure', null );
                        workOrdersToAdd.add(LSWPWorkOrder);
                    }
                }
                //CREATE PAINT/STAIN WORK ORDER IF THERE ARE PAINT/STAIN ORDER ITEMS OF MISC JOB UNIT TYPE AND ARE NOT CANCELLED
                if(ordIdtoListOfPaintStainWOMap.get(ord.id) == null || ordIdtoListOfPaintStainWOMap.get(ord.id).size() == 0){
                    system.debug('### inside else.5: '+relatedOrderItemsToOrder.size());
                    system.debug('### inside else.5.1: '+relatedOrderItemsToOrder.containsKey(ord.id));
                    if(relatedOrderItemsToOrder.size() > 0  && relatedOrderItemsToOrder.containsKey(ord.id)){
                        WorkOrder paintStainWorkOrder = UtilityMethods.buildWorkOrder(dwelling, ord, paintStainWORecordType, storeConfig.Paint_Stain_Work_Order_Queue_ID__c, 'Paint/Stain', null );
                        workOrdersToAdd.add(paintStainWorkOrder);
                    }
                }
            }
        }
        //INSERT ALL WORK ORDERS CREATED
        system.debug('### workOrdersToAdd.size: '+workOrdersToAdd.size());
        if (!workOrdersToAdd.isEmpty()){
            insert workOrdersToAdd;
            OrderTriggerHandler.beforeUpdateCreatedWO = true;
 
        }
    }
    public static void createWorkOrderOnOrderCreation(List<Order> listNew, Map<Id, Order> mapNew) {
        List<Id> relatedAccountIds = new List<Id>();
        List<Id> relatedOpportunityIds = new List<Id>();
        List<Id> orderIds = new List<Id>();
        list<WorkOrder> workOrdersToAdd = new list<WorkOrder>();
        
        Id workOrderTechMeasureRecordTypeId = UtilityMethods.retrieveRecordTypeId('Tech_Measure', 'WorkOrder');
        Id orderCoroServiceRecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
        Id changeOrderRecordTypeId = UtilityMethods.retrieveRecordTypeId('Change_Order', 'Order');
        
        for(Order o : listNew){
            if (o.RecordTypeId == changeOrderRecordTypeId) continue;
            if (o.Id == UtilityMethods.getDummyOrderId()) continue;
            
            relatedAccountIds.add(o.AccountId);
            orderIds.add(o.Id);

            if(o.OpportunityId != null){
                relatedOpportunityIds.add(o.OpportunityId);
            }
        }
        // If there were no accounts, then just break out of this method
        if (relatedAccountIds.size() == 0) return;
        
        listNew = [SELECT ID, RecordTypeId, OpportunityId, Name, AccountId, BillToContactId from Order where Id in :orderIds ];
        list<Account> relatedAccounts = [SELECT ShippingStreet,ShippingCity,ShippingState,ShippingStateCode,
                                                ShippingPostalCode,ShippingCountry,ShippingCountryCode,Store_Location__c,
                                                Name,Store_Location__r.Active_Store_Configuration__c
                                                FROM Account WHERE Id IN :relatedAccountIds];
        map<id, Account> accountIdToAccountMap = new map<id, Account>();
        map<id,Id> storeIdToStoreConfigIdMap = new map<id,Id>();
        for(Account a : relatedAccounts){
            accountIdToAccountMap.put(a.id, a);
            if (a.Store_Location__c != null && a.Store_Location__r.Active_Store_Configuration__c != null)
                storeIdToStoreConfigIdMap.put(a.Store_Location__c, a.Store_Location__r.Active_Store_Configuration__c);
        }

        //IF ANY RELATED OPPS EXIST THEN QUERY FOR TECH MEASURE WORK ORDERS ON THEM
        map<id, list<WorkOrder>> oppIdtoListOfTechMeasureWOMap = new map<id, list<WorkOrder>>();
        //I don't like these large if {}s
        if(relatedOpportunityIds.size() > 0){
            list<WorkOrder> relatedTechMeasureWorkOrders = [SELECT Id, Opportunity__c FROM WorkOrder
                                                                    WHERE Opportunity__c IN : relatedOpportunityIds
                                                                    AND RecordTypeId = :workOrderTechMeasureRecordTypeId];
            //BUILDING MAP OF OPP ID TO LIST OF WORK ORDERS TO USE WHILE LOOPING THROUGH ORDERS
            for(WorkOrder wo : relatedTechMeasureWorkOrders){
                if(oppIdtoListOfTechMeasureWOMap.containsKey(wo.Opportunity__c)){
                    oppIdtoListOfTechMeasureWOMap.get(wo.Opportunity__c).add(wo);
                }else{
                    oppIdtoListOfTechMeasureWOMap.put(wo.Opportunity__c, new list<WorkOrder>{wo});
                }
            }
        }
        //GETTING ALL STORE CONFIGS FOR RELATED DWELLINGS
        map<Id, Store_Configuration__c> accountIdtoStoreConfigMap = UtilityMethods.mapStoreConfigs(storeIdToStoreConfigIdMap);
        for(Order ord : listNew){
            if (ord.RecordTypeId == changeOrderRecordTypeId) continue;
            if (ord.Id == UtilityMethods.getDummyOrderId()) continue;
            //CHECKING IF CURRENT ORDER'S OPPORTUNITY HAS ANY RELATED TECH MEASURES IF SO, SKIP LOGIC AND ASSIGN THOSE TO THIS WORK ORDER
            if(ord.OpportunityId != null && oppIdtoListOfTechMeasureWOMap.get(ord.OpportunityId) != null){
                list<WorkOrder> techMeasureWorkOrders = oppIdtoListOfTechMeasureWOMap.get(ord.OpportunityId);
                for(WorkOrder wo : techMeasureWorkOrders){
                    wo.Sold_Order__c = ord.id;
                    if (!workOrdersToAdd.contains(wo)) {
                        workOrdersToAdd.add(wo);
                    }
                }
            //CHECK THE RECORD TYPE IS CORO SERVICE, IF IT IS, SKIP LOGIC
            }else{
                Account relatedDwelling = accountIdToAccountMap.get(ord.AccountId);
                if (ord.Name == 'Dummy Cost PO Order') continue;
                if(accountIdtoStoreConfigMap.get(relatedDwelling.Store_Location__c) == null){
                    ord.addError('Store Configuration has not been created.');
                }
                if(accountIdtoStoreConfigMap.get(relatedDwelling.Store_Location__c).Tech_Measure_Work_Order_Queue_Id__c == null){
                    ord.addError('Tech Measure Work Order Queue Id has not been set on the related Store Configuation.');
                }
                Id techMeasureQueueId = accountIdtoStoreConfigMap.get(relatedDwelling.Store_Location__c).Tech_Measure_Work_Order_Queue_Id__c;

                if(ord.RecordTypeId == orderCoroServiceRecordTypeId){
                    continue;
                }else{
                    //CREATE WORK ORDER (TECH MEASURE)
                    WorkOrder techWO = new WorkOrder(
                        Sold_Order__c = ord.id,
                        RecordTypeId = workOrderTechMeasureRecordTypeId,
                        Work_Order_Type__c = 'Tech Measure',
                        AccountId = ord.AccountId,
                        ContactId = ord.BillToContactId,
                        Opportunity__c = ord.OpportunityId,
                        Street = relatedDwelling.ShippingStreet,
                        City = relatedDwelling.ShippingCity,
                        State = relatedDwelling.ShippingState,
                        PostalCode = relatedDwelling.ShippingPostalCode,
                        Country = relatedDwelling.ShippingCountry,
                        OwnerId = techMeasureQueueId
                    );
                    workOrdersToAdd.add(techWO);
                }
            }
        }
        //INSERT ALL WORK ORDERS
        if (workOrdersToAdd.size() > 0) {
            upsert workOrdersToAdd;
        }
    }
    public static void createInstallWorkOrderOnTechMeasureComplete(Map<Id, WorkOrder> mapOld, Map<Id, WorkOrder> mapNew){
        System.debug('mapNew:  ' + mapNew);
        // if (UtilityMethods.hasWOTriggerRan()) return;
        Id installWORecordType = UtilityMethods.retrieveRecordTypeId('Install', 'WorkOrder');
        list<WorkOrder> workOrdersToInsert = new list<WorkOrder>();
        list<WorkOrder> completeWorkOrders = new list<WorkOrder>();
        list<id> relatedDwellingIds = new list<id>();
        list<Id> relatedOrderIdList = new list<id>();
        map<id,Id> storeIdToStoreConfigIdMap = new map<id,Id>();
        list<WorkOrder> extraFieldsList = [SELECT   id,
                                                    Sold_Order__c,
                                                    Sold_Order__r.BillToContactId,
                                                    Sold_Order__r.OpportunityId,
                                                    RecordTypeId,
                                                    Work_Order_Type__c,
                                                    AccountId,
                                                    Account.Store_Location__c,
                                                    Street,
                                                    City,
                                                    State,
                                                    PostalCode,
                                                    Country,
                                                    OwnerId,
                                                    Status
                                                    FROM WorkOrder WHERE Id IN :mapNew.keyset()];

        for(WorkOrder wo : extraFieldsList){
            if(wo.Work_Order_Type__c != null && wo.Work_Order_Type__c == 'Tech Measure'){
                if(mapOld == null && mapNew.get(wo.id).Status == 'Appt Complete / Closed'){
                    completeWorkOrders.add(wo);
                    if (wo.Sold_Order__c != null) {
                        relatedOrderIdList.add(wo.Sold_Order__c);
                    }
                    relatedDwellingIds.add(wo.AccountId);
                }
            }
        }
        System.debug('completeWorkOrders:  ' + completeWorkOrders);
        System.debug('relatedDwellingIds:  ' + relatedDwellingIds);
        //GET RELATED DWELLINGS AND ASSIGN TO MAP
        for(Account dwelling : [SELECT id, Store_Location__c, Store_Location__r.Active_Store_Configuration__c FROM Account WHERE Id IN :relatedDwellingIds]){
            if (dwelling.Store_Location__c != null && dwelling.Store_Location__r.Active_Store_Configuration__c != null)
                storeIdToStoreConfigIdMap.put(dwelling.Store_Location__c, dwelling.Store_Location__r.Active_Store_Configuration__c);
        }
        map<Id, Store_Configuration__c> accountIdtoStoreConfigMap = UtilityMethods.mapStoreConfigs(storeIdToStoreConfigIdMap);
        System.debug('relatedOrderIdList:  ' + relatedOrderIdList);
        //GET RELATED WORK ORDERS ON SAME ORDER AND MAP THEM
        list<WorkOrder> relatedWorkOrders = [SELECT Id, Sold_Order__c, Work_Order_Type__c
                                                    FROM WorkOrder
                                                    Where Sold_Order__c
                                                    IN :relatedOrderIdList];
        System.debug('relatedWorkOrders:  ' + relatedWorkOrders);
        map<id, list<WorkOrder>> orderIdToListWorkOrdersMap = new map<id, list<WorkOrder>>();
        for(WorkOrder wo : relatedWorkOrders){
           if(orderIdToListWorkOrdersMap.containsKey(wo.Sold_Order__c)){
                orderIdToListWorkOrdersMap.get(wo.Sold_Order__c).add(wo);
            }else{
                orderIdToListWorkOrdersMap.put(wo.Sold_Order__c, new list<WorkOrder>{wo});
            }
        }

        for(WorkOrder wo : completeWorkOrders){
            boolean alreadyHaveInstall = false;
            if(orderIdToListWorkOrdersMap.get(wo.Sold_Order__c) != null && orderIdToListWorkOrdersMap.get(wo.Sold_Order__c).size() > 0 ){
                for(WorkOrder relatedWO : orderIdToListWorkOrdersMap.get(wo.Sold_Order__c)){
                    if(relatedWO.Work_Order_Type__c == 'Install'){
                        alreadyHaveInstall = true;
                        break;
                    }
                }
            }//end of check for null related
            if(alreadyHaveInstall == false){
                if(wo.AccountId == null){
                    mapNew.get(wo.id).addError('Dwelling must be set to complete Tech Measure.');
                }else if(wo.Account.Store_Location__c == null){
                    mapNew.get(wo.id).addError('Store Location must be set on related Dwelling');
                }else if(accountIdtoStoreConfigMap.get(wo.Account.Store_Location__c) == null){
                    mapNew.get(wo.id).addError('Store Configuration has not been created for the related Dwellings Store.');
                } else{
                    Id installQueueId = accountIdtoStoreConfigMap.get(wo.Account.Store_Location__c).Install_Work_Order_Queue_Id__c;
                    Id InstallId = [SELECT Name, Id FROM WorkType WHERE Name = 'Install' LIMIT 1].Id;
                    WorkOrder installWO = new WorkOrder(
                        Sold_Order__c = wo.Sold_Order__c,
                        RecordTypeId =  installWORecordType,
                        WorkTypeId = InstallId,
                        Work_Order_Type__c = 'Install',
                        AccountId = wo.AccountId,
                        Street = wo.Street,
                        City = wo.City,
                        State = wo.State,
                        PostalCode = wo.PostalCode,
                        Country = wo.Country,
                        ContactId = wo.Sold_Order__r.BillToContactId,
                        Opportunity__c = wo.Sold_Order__r.OpportunityId,
                        OwnerId = installQueueId
                    );
                    if(workOrdersToInsert.size() == 0){
                        workOrdersToInsert.add(installWO);
                    }
                }
            }// end of alreadyHaveInstall if
        }//end of loop of compleatedWorkOrders
        System.debug('workOrdersToInsert:  ' + workOrdersToInsert);
        if (workOrdersToInsert.size() > 0){
            // FSLRollupRolldownController.disableFSLRollupRolldownController = true;
            insert workOrdersToInsert;
            // FSLRollupRolldownController.disableFSLRollupRolldownController = false;
        }
    }//end of createInstallWorkOrderOnTechMeasureComplete method
}