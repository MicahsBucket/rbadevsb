public class CanvassUnitUtils {
	public static CNVSS_Canvass_Unit__c getCanvassUnitById(string canvassUnitId) {
    	CNVSS_Canvass_Unit__c canvassUnit = new CNVSS_Canvass_Unit__c();
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.CNVSS_Canvass_Unit__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        
        String theQuery = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues)
        {
            String theName = s.getDescribe().getName();
            
            // Continue building your dynamic query string
            theQuery += theName + ',';
        }

        // Trim last comma
        theQuery = theQuery.subString(0, theQuery.length() - 1);
        
        // Get Canvass Market field(s) for phone call functionality and email
        theQuery += ', CNVSS_Canvass_Unit__c.CNVSS_Canvass_Market__r.CNVSS_ISC_Phone_Number__c, CNVSS_Canvass_Unit__c.CNVSS_Canvass_Market__r.CNVSS_ISC_Email__c, CNVSS_Canvass_Unit__c.CNVSS_Canvass_Market__r.Name';
        
        // Finalize query string
        theQuery += ' FROM CNVSS_Canvass_Unit__c WHERE Id = \''+ canvassUnitId +'\'';
        
        // Make your dynamic call
        canvassUnit = Database.query(theQuery);
        return canvassUnit;
    }

    public static String formatCanvassUnitAddress(string houseNo, string streetName, string streetType, string unitPrefix, string unitNo, string city, string state, string zip) {
        String address = houseNo + ' ' + streetName + ' ' + (String.isNotBlank(streetType) ? streetType : '')  + (String.isNotBlank(unitPrefix) ? ' ' + unitPrefix : '')
        + (String.isNotBlank(unitNo) ? ' ' + unitNo : '') + ', ' + city + ', ' + state + ', ' + zip;
        return address;
    }
    
    public static string getCanvassUnitIdByFullAddress(string fullAddress) {
        string canvassUnitId = '';
        CNVSS_Canvass_Unit__c cu = null;
        List<CNVSS_Canvass_Unit__c> listCu = [Select Id, CNVSS_PK_Full_Address__c From CNVSS_Canvass_Unit__c Where CNVSS_PK_Full_Address__c =: fullAddress Limit 1];
        if(!listCu.isEmpty()) {
            cu = listCu[0];
            canvassUnitId = cu.Id;
        }
        return canvassUnitId;
    }

    public static string getCanvassUnitIdByAddress(string house_no, string street, string city,string state,string zip) {
        string canvassUnitId = '';
        CNVSS_Canvass_Unit__c cu = null;
        String streetSearchText = street.substringBefore(' ')+'%';
        List<CNVSS_Canvass_Unit__c> listCu = 
            [Select Id, CNVSS_PK_Full_Address__c From CNVSS_Canvass_Unit__c 
                Where CNVSS_House_No__c =: house_no 
                 and  CNVSS_Street__c like :streetSearchText 
                 and CNVSS_City__c =: city
                 and CNVSS_State__c =: state
                 and CNVSS_Zip_Code__c =: zip
                  
                 ];
        //don't return the CU if more than 1 match
        if (listCu.size() == 1){
            cu = listCu[0];
            canvassUnitId = cu.Id;
        } 
        return canvassUnitId;
    }
}