global class BatchJobServiceResource implements Database.Batchable<sObject> {
    //variables
    global final String Query = 'SELECT Name, Id, Date_To_Make_Inactive__c, IsActive, Inactive_Reason__c FROM ServiceResource WHERE Date_To_Make_Inactive__c <= TODAY AND IsActive = True AND User_Type__c = \'External\''; 
    
    global BatchJobServiceResource() {
        //constructor
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug(scope);
        List<ServiceResource> ServiceResourceList = new List<ServiceResource>();
        for(sObject s : scope) {
            ServiceResource r = (ServiceResource) s;
            r.Inactive_Reason__c = 'Insurance Expiration';
            r.IsActive = False;
            ServiceResourceList.add(r);
        }

        Database.SaveResult[] srList = Database.update(ServiceResourceList, false);
        Boolean errors = false;
        /*
 * from https://salesforce.stackexchange.com/questions/54737/database-saveresult-getid-returns-null
 * 
 *         for (Integer i = 0; i < accountsList.size(); i++) {
            Database.SaveResult s = res[i];
            Account origRecord = accountsList[i];
            if (!s.isSuccess()) {
                system.debug(s.getId()); // I get null here
                system.debug(origRecord.Id); //This should be the Id you're looking for
             } 
        }
*/
        for(Integer i = 0; i < ServiceResourceList.size(); i++) {
            Database.SaveResult s = srList[i];
            if(!s.isSuccess()) {
                errors = true;
                for(Database.Error err : s.getErrors()) {
                    System.debug('The following error has occurred on ' + serviceResourceList[i].Id + '.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('ServiceResource fields that affected this error: ' + err.getFields());
                }    
            }
        }
        System.debug(srList);
        // Iterate through each returned result
/*		for (Database.SaveResult sr : srList) {
    		if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully updated ServiceResource: ' + sr.getId());
            }
            else {
//                errors.add(sr);
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred on ' + sr.getId() + '.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('ServiceResource fields that affected this error: ' + err.getFields());
                }                
            }
        }
*/        
        if(errors) {
            sendEmail(ServiceResourceList, srList);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }

    private void sendEmail(List<ServiceResource> serviceResourceList, List<Database.SaveResult> dsr) {
        //get custom setting for support email
        Map<String, RMS_Settings__c> RMS_Settings_map = RMS_Settings__c.getAll(); 
	    RMS_Settings__c rSupportEmail = RMS_Settings_map.get('rSupport Email');

        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses = new String[] {rSupportEmail.Value__c};


        mail.setToAddresses(toAddresses);
        mail.setSubject('Batch Job Service Resource Error');
        String textBody = 'Hello rSupport! \n\n' +
                        'There are ' + dsr.size() + ' Service Resource(s) that have bad data that we need your help correcting: \n\n' +
                        'To resolve these errors, click on the link for the Service Resource(s) below and enter the missing information: \n\n';
        
/*
 * from https://salesforce.stackexchange.com/questions/54737/database-saveresult-getid-returns-null
 * 
 *         for (Integer i = 0; i < accountsList.size(); i++) {
            Database.SaveResult s = res[i];
            Account origRecord = accountsList[i];
            if (!s.isSuccess()) {
                system.debug(s.getId()); // I get null here
                system.debug(origRecord.Id); //This should be the Id you're looking for
             } 
        }
*/        
        for(Integer i = 0; i < serviceResourceList.size(); i++) {
			Database.SaveResult sr = dsr[i];
            if(!sr.isSuccess()) {
            	textBody += 'Name of Service Resource: ' + serviceResourceList[i].Name + 
                        '\n\nId of Service Resource: ' + sfdcBaseURL + '/' + serviceResourceList[i].Id +
                        '\n\nError of this Service Resource: ' + sr.getErrors() + '\n\n ----- \n\n';
            }            
        }

        mail.setPlainTextBody(textBody);
        System.debug(textBody);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });


    }
}