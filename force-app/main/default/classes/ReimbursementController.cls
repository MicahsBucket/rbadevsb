/*
 * @author Jason Flippen
 * @date 03/11/2020
 * @description: Provides database functionality for the serviceRequestRecordPageHeader LWC.
 *
 *			   Code Coverage provided by the following Test Class:
 *              - ReimbursementControllerTest
 */
public with sharing class ReimbursementController {

	@AuraEnabled
	public static List<ChargeWrapper> getCharges(String orderId) {

		List<ChargeWrapper> charges = new List<ChargeWrapper>();

		Set<Id> serviceSymptomIds = new Set<Id>();
		Map<String, Service_Symptom__c> orderProductIdWithServiceSymptom = new Map<String, Service_Symptom__c>();
		Map<String, Problem_Component__c> serviceSymptomIdWithProblemComponent = new Map<String, Problem_Component__c>();
		Map<String, List<Warranty__c>> problemIdWithWarranty = new Map<String, List<Warranty__c>>();
		for (Service_Symptom__c serviceSymp : [SELECT Id,
													  Service_Symptom__c,
													  Service_Product__c,
													  Service_Symptom__r.Name
											   FROM   Service_Symptom__c
											   WHERE  Service_Product__r.OrderId = :orderId]) {
			serviceSymptomIds.add(serviceSymp.Id);
			orderProductIdWithServiceSymptom.put(serviceSymp.Service_Product__c,serviceSymp);
		}

		if (!serviceSymptomIds.isEmpty()) {

			for (Problem_Component__c problemComponent : [SELECT Id,
																 Problem_Component_Text__c,
																 Problem_Component_asgn__c,
																 Service_Symptom__c
														  FROM   Problem_Component__c
														  WHERE  Service_Symptom__c IN :serviceSymptomIds]) {
				serviceSymptomIdWithProblemComponent.put(problemComponent.Service_Symptom__c,problemComponent);
			}

			if (!serviceSymptomIdWithProblemComponent.isEmpty()) {

				for (Warranty__c warranty : [SELECT Id,
													Problem_Component_Code__c,
													Problem_Component__c,
													Quantity__c,
													Procedure_Code__c
											 FROM   Warranty__c
											 WHERE  Problem_Component__c IN :serviceSymptomIdWithProblemComponent.values()]) {
					
					if (!problemIdWithWarranty.containsKey(warranty.Problem_Component__c)) {
						problemIdWithWarranty.put(warranty.Problem_Component__c, new List<Warranty__c>());
						problemIdWithWarranty.get(warranty.Problem_Component__c).add(warranty);
					}
					else {
						problemIdWithWarranty.get(warranty.Problem_Component__c).add(warranty);
					}
				}

			}

		}

		for (Charge__c charge : [SELECT Id,
										Installed_Product__c,
										Charge_Cost_To__c,
										Category__c,
										Service_Product__r.Variant_Number__c,
										Service_Product__r.Installation_Date__c,
										Service_Product__r.PriceBookEntry.Name,
										Service_Product__c,
										Service_Product__r.Sold_Order_Product_Asset__r.Variant_Number__c,
										Service_Product__r.Reimbursement_Completed__c
								 FROM   Charge__c
								 WHERE  Service_Request__c = :orderId
								 AND    Charge_Cost_To__c = 'Manufacturing'
								 AND    Category__c = 'Renewal by Andersen']) {
			
			String selectedSymptom;
			String selectedSymptomName;
			String problemComponentName;
			String problemComponent;
			String selectedProcedure;
			String selectedProcedure2;
			String procedureName;
			String procedureName2;
			Decimal selectedQty;
			Decimal selectedQty2;
			String symId;
			String problemComponentId;
			String procedureId;
			String procedureId2;
			if (orderProductIdWithServiceSymptom.containsKey(charge.Service_Product__c)) {

				symId = orderProductIdWithServiceSymptom.get(charge.Service_Product__c).Id;
				selectedSymptom = orderProductIdWithServiceSymptom.get(charge.Service_Product__c).Service_Symptom__c;
				selectedSymptomName = orderProductIdWithServiceSymptom.get(charge.Service_Product__c).Service_Symptom__r.Name;

				if (serviceSymptomIdWithProblemComponent.containsKey(symId)) {

					problemComponentId = serviceSymptomIdWithProblemComponent.get(symId).Id;
					problemComponentName = serviceSymptomIdWithProblemComponent.get(symId).Problem_Component_Text__c;
					problemComponent = serviceSymptomIdWithProblemComponent.get(symId).Problem_Component_asgn__c;

					if (problemIdWithWarranty.containsKey(problemComponentId)) {

						System.debug('problemIdWithWarranty.get(problemComponentId): ' + problemIdWithWarranty.get(problemComponentId));
						System.debug('problemIdWithWarranty.get(problemComponentId): ' + problemIdWithWarranty.get(problemComponentId).size());
						selectedProcedure = problemIdWithWarranty.get(problemComponentId)[0].Procedure_Code__c;
						selectedQty = problemIdWithWarranty.get(problemComponentId)[0].Quantity__c;
						procedureName = problemIdWithWarranty.get(problemComponentId)[0].Problem_Component_Code__c;
						procedureId = problemIdWithWarranty.get(problemComponentId)[0].Id;
						
						if (problemIdWithWarranty.get(problemComponentId).size() > 1) {
							selectedProcedure2 = problemIdWithWarranty.get(problemComponentId)[1].Procedure_Code__c;
							selectedQty2 = problemIdWithWarranty.get(problemComponentId)[1].Quantity__c;
							procedureName2 = problemIdWithWarranty.get(problemComponentId)[1].Problem_Component_Code__c;
							procedureId2 = problemIdWithWarranty.get(problemComponentId)[1].Id;
						}

					}

				}

			}

			ChargeWrapper wrapper = new ChargeWrapper(charge.Id,
													  charge.Installed_Product__c,
													  charge.Charge_Cost_To__c,
													  charge.Category__c,
													  charge.Service_Product__r.Sold_Order_Product_Asset__r.Variant_Number__c,
													  charge.Service_Product__r.Installation_Date__c,
													  charge.Service_Product__r.PriceBookEntry.Name,
													  charge.Service_Product__c,
													  charge.Service_Product__r.Reimbursement_Completed__c,
													  selectedSymptom,
													  selectedSymptomName,
													  problemComponentName,
													  problemComponent,
													  selectedProcedure,
													  selectedQty,
													  selectedProcedure2,
													  selectedQty2,
													  procedureName,
													  procedureName2,
													  symId,
													  problemComponentId,
													  procedureId,
													  procedureId2);
			charges.add(wrapper);

		}

		return charges;
		
	}

	@AuraEnabled
	public static List<Service_Symptom_List__c> getSymptoms(List<String> symptomIds) {
		return [SELECT Id, Name FROM Service_Symptom_List__c WHERE Id IN :symptomIds];
	}

	@AuraEnabled
	public static List<Service_Symptom_List__c> getAllSymptoms() {
		return [SELECT Id, Name FROM Service_Symptom_List__c LIMIT 50000];
	}

  /*
   * @author Jason Flippen
   * @date 01/06/2021
   * @description Method to return List of Reimbursable Parts for an Order.
   */
	@AuraEnabled
	public static List<ReimbursablePartWrapper> getReimbursableParts(Id orderId) {

		List<ReimbursablePartWrapper> wrapperList = new List<ReimbursablePartWrapper>();

		for (Reimbursable_Part__c reimbursablePart : [SELECT Id,
															 Cost__c,
															 Description__c,
															 Order_Product__c,
															 Order_Product__r.Sold_Order_Product_Asset__r.Name,
															 Order_Product__r.Sold_Order_Product_Asset__r.Variant_Number__c,
															 Part__c,
															 Part__r.ProductCode,
															 Quantity__c,
															 Reason__c,
															 Unit_Price__c
													  FROM   Reimbursable_Part__c
													  WHERE  Order__c = :orderId
													  ORDER BY part__r.ProductCode]) {
			
			ReimbursablePartWrapper wrapper = new ReimbursablePartWrapper();
			wrapper.id = reimbursablePart.Id;
			wrapper.description = reimbursablePart.Description__c;
			wrapper.isNew = false;
			wrapper.name = reimbursablePart.Part__r.ProductCode;
			wrapper.orderProductId = reimbursablePart.Order_Product__c;

			String productName = reimbursablePart.Order_Product__r.Sold_Order_Product_Asset__r.Name + ' | ' + reimbursablePart.Order_Product__r.Sold_Order_Product_Asset__r.Variant_Number__c;
			wrapper.productName = productName;

			wrapper.quantity = reimbursablePart.Quantity__c;
			wrapper.reason = reimbursablePart.Reason__c;
			wrapper.unitPrice = 0;
			if (reimbursablePart.Unit_Price__c > 0) {
				wrapper.unitPrice = reimbursablePart.Unit_Price__c;
			}

			wrapperList.add(wrapper);

		}

		return wrapperList;

	}

	@AuraEnabled
	public static List<ChargeWrapper> saveServiceSymptoms(String symptomJSON, List<String> symptomIdList) {

		List<ChargeWrapper> wrapperList = (List<ChargeWrapper>) JSON.deserialize(symptomJSON,List<ChargeWrapper>.class);

		try {

			Map<Id, Service_Symptom_List__c> serviceSymptompListMap = new Map<Id, Service_Symptom_List__c>([SELECT Id, Name
																											FROM   Service_Symptom_List__c
																											WHERE  Id IN :symptomIdList]);

			Map<String, List<ComboboxWrapper>> serviceSymptomProblemComponentConfigListMap = new Map<String, List<ComboboxWrapper>>();
			for (Problem_Component_Config__c problemComponentConfig : [SELECT Id,
																			  Name,
																			  Problem_Component__c,
																			  Service_Symptom__c,
																			  Problem_Component__r.Inactive__c
																	   FROM   Problem_Component_Config__c
																	   WHERE  Service_Symptom__c IN :symptomIdList
																	   AND    Problem_Component__r.Inactive__c = false
																	   ORDER BY Name ASC]) {
				ComboboxWrapper comboboxWrapper = new ComboboxWrapper(problemComponentConfig.Name,problemComponentConfig.Id);
				if (!serviceSymptomProblemComponentConfigListMap.containsKey(problemComponentConfig.Service_Symptom__c)) {
					serviceSymptomProblemComponentConfigListMap.put(problemComponentConfig.Service_Symptom__c,new List<ComboboxWrapper>());
				}
				serviceSymptomProblemComponentConfigListMap.get(problemComponentConfig.Service_Symptom__c).add(comboboxWrapper);
			}

			List<Service_Symptom__c> newServiceSymptomList = new List<Service_Symptom__c>();
			for (ChargeWrapper wrapper : wrapperList) {

				if (serviceSymptompListMap.containsKey(wrapper.selectedSymptom)) {
					wrapper.selectedSymptomName = serviceSymptompListMap.get(wrapper.selectedSymptom).Name;
				}

				if (serviceSymptomProblemComponentConfigListMap.containsKey(wrapper.selectedSymptom)) {
					wrapper.problems = serviceSymptomProblemComponentConfigListMap.get(wrapper.selectedSymptom);
				}

				if (wrapper.isNew == true) {

					// Create new Service Symptom record and add it to the List to be inserted.
					Service_Symptom__c newServiceSymptom = new Service_Symptom__c(Service_Product__c = wrapper.orderProductId,Service_Symptom__c = wrapper.selectedSymptom);
					newServiceSymptomList.add(newServiceSymptom);
				}

			}

			// If we have Service Symptoms to insert, insert them.
			if (!newServiceSymptomList.isEmpty()) {

				insert newServiceSymptomList;

				// Iterate through the new Service Symptom records and add the Id to the appropriate Wrapper record.
				for (Service_Symptom__c serviceSymptom : newServiceSymptomList) {
					for (ChargeWrapper wrapper : wrapperList) {
						if (wrapper.isNew == true &&
							serviceSymptom.Service_Product__c == wrapper.orderProductId &&
							serviceSymptom.Service_Symptom__c == wrapper.selectedSymptom) {
							wrapper.symptomId = serviceSymptom.Id;
							break;
						}
					}
				}

			}

		}
		catch (Exception ex) {
			System.debug('***** ReimbursementController.saveServiceSymptoms() Error: ' + ex.getMessage());
		}

		return wrapperList;

	}

  /*
   * @author Jason Flippen
   * @date 03/18/2021
   * @description: Method to delete Service Symptom records from the database.
   * @param symptomJSON - JSON string of a List of ChargeWrapper records.
   * @return N/A
   *
   * CHANGE HISTORY
   * ====================================================================================================
   * DATE          NAME                    DESCRIPTION
   * N/A           N/A                     N/A
   * ====================================================================================================
   */
	@AuraEnabled
	public static List<ChargeWrapper> deleteServiceSymptoms(String symptomJSON) {

		List<ChargeWrapper> wrapperList = (List<ChargeWrapper>) JSON.deserialize(symptomJSON,List<ChargeWrapper>.class);

		// Iterate through the (Charge) Wrapper List and grab the List of Service Symptom records to be deleted.
		List<Service_Symptom__c> deleteServiceSymptomList = new List<Service_Symptom__c>();
		List<OrderItem> updateOrderItemList = new List<OrderItem>();
		for (ChargeWrapper wrapper : wrapperList) {

			if (wrapper.isNew == true && wrapper.symptomId != null) {

				// Add Service Symptom to Delete List.
				Service_Symptom__c deleteServiceSymptom = new Service_Symptom__c(Id = wrapper.symptomId	);
				deleteServiceSymptomList.add(deleteServiceSymptom);
				wrapper.isNew = false;
				wrapper.symptomId = null;

				// Add Order Product (aka OrderItem) to Update List.
				OrderItem updateOrderItem = new OrderItem(Id = wrapper.orderProductId,
														  Number_Service_Symptoms__c = 0,
														  Number_Warranties__c = 0);
				updateOrderItemList.add(updateOrderItem);

			}

		}

		// If we have Service Symptoms to delete, delete them.
		if (!deleteServiceSymptomList.isEmpty()) {

			try {
				delete deleteServiceSymptomList;
				update updateOrderItemList;
			}
			catch (Exception ex) {
				System.debug('***** ReimbursementController.deleteServiceSymptoms() Error: ' + ex.getMessage());
			}

		}

		return wrapperList;

	}

	@AuraEnabled
	public static List<ChargeWrapper> saveAll(String symptomJSON, List<ReimbursablePartWrapper> newPartList, Id orderId) {

		System.debug('symptomJSON' + symptomJSON);
		System.debug('newPartList' + newPartList);
		System.debug('orderId' + orderId);
		List<ChargeWrapper> symWrapper = new List<ChargeWrapper>();
		List<Service_Symptom__c> serviceSymptoms = new List<Service_Symptom__c>();
		List<Problem_Component__c> problems = new List<Problem_Component__c>();
		List<Warranty__c> warranties = new List<Warranty__c>();

		try {

			List<Problem_Component__c> problemsComponents = new List<Problem_Component__c>();
			Map<String, Service_Symptom__c> orderProductIdWithServiceSymptom = new Map<String, Service_Symptom__c>();
			Map<String, Problem_Component__c> orderProductIdWithProblemComponent = new Map<String, Problem_Component__c>();
			Map<String, Warranty__c> orderProductIdWithWarranty = new Map<String, Warranty__c>();
			Map<Id, List<ChargeWrapper>> mapWarrantiesToUpdate = new Map<Id, List<ChargeWrapper>>();

			symWrapper = (List<ChargeWrapper>) JSON.deserialize(symptomJSON,List<ChargeWrapper>.class);
			for (ChargeWrapper chargeWrap : symWrapper) {

				System.debug('chargeWrap selectedProcedure ' + chargeWrap.selectedProcedure);
				System.debug('chargeWrap selectedProcedure 2 ' + chargeWrap.selectedProcedure2);

				if (!mapWarrantiesToUpdate.containsKey(chargeWrap.selectedProcedure)) {
					mapWarrantiesToUpdate.put(chargeWrap.selectedProcedure, new List<ChargeWrapper>());
					mapWarrantiesToUpdate.get(chargeWrap.selectedProcedure).add(chargeWrap);
				}
				else {
					mapWarrantiesToUpdate.get(chargeWrap.selectedProcedure).add(chargeWrap);
				}

				if (!mapWarrantiesToUpdate.containsKey(chargeWrap.selectedProcedure2)) {
					mapWarrantiesToUpdate.put(chargeWrap.selectedProcedure2, new List<ChargeWrapper>());
					mapWarrantiesToUpdate.get(chargeWrap.selectedProcedure2).add(chargeWrap);
				}
				else {
					mapWarrantiesToUpdate.get(chargeWrap.selectedProcedure2).add(chargeWrap);
				}

				Service_Symptom__c sc = new Service_Symptom__c();
				sc.Service_Product__c = chargeWrap.orderProductId;
				sc.Service_Symptom__c = chargeWrap.selectedSymptom;
				if (String.isNotBlank(chargeWrap.symptomId)) {
					sc.Id = chargeWrap.symptomId;
				}
				serviceSymptoms.add(sc);
				orderProductIdWithServiceSymptom.put(chargeWrap.orderProductId, sc);

				Problem_Component__c problem = new Problem_Component__c();
				problem.Service_Symptom__c = sc.Id;
				problem.Service_Product_lkup__c = chargeWrap.orderProductId;
				problem.Problem_Component_asgn__c = chargeWrap.problemComponent;
				if (String.isNotBlank(chargeWrap.problemId)) {
					problem.Id = chargeWrap.problemId;
				}
				problems.add(problem);
				orderProductIdWithProblemComponent.put(chargeWrap.orderProductId,problem);

				System.debug('chargeWrap selectedProcedure' + chargeWrap.selectedProcedure);
				System.debug('chargeWrap selectedProcedure2' + chargeWrap.selectedProcedure2);

				Warranty__c wc = new Warranty__c();
				wc.Charge_Cost_To__c = chargeWrap.chargeId;
				wc.Procedure_Code__c = chargeWrap.selectedProcedure;
				wc.Quantity__c = chargeWrap.selectedQty;
				wc.Service_Product__c = chargeWrap.orderProductId;
				if (!mapWarrantiesToUpdate.containsKey(chargeWrap.procedureId)) {
					wc.Id = chargeWrap.procedureId;
				}
				warranties.add(wc);

				if (chargeWrap.selectedProcedure2 != null) {
					Warranty__c wc2 = new Warranty__c();
					wc2.Charge_Cost_To__c = chargeWrap.chargeId;
					wc2.Procedure_Code__c = chargeWrap.selectedProcedure2;
					wc2.Quantity__c = chargeWrap.selectedQty2;
					wc2.Service_Product__c = chargeWrap.orderProductId;
					if (!mapWarrantiesToUpdate.containsKey(chargeWrap.procedureId2)) {
						wc2.Id = chargeWrap.procedureId2;
					}
					warranties.add(wc2);
				}
				orderProductIdWithWarranty.put(chargeWrap.orderProductId, wc);

			}

			if (serviceSymptoms.size() > 0) {

				upsert serviceSymptoms;

				if (problems.size() > 0) {
					for (Problem_Component__c pb : problems) {
						System.debug('@@@ problem' + pb);
						if (orderProductIdWithServiceSymptom.containsKey(pb.Service_Product_lkup__c) && pb.Id == null) {
							pb.Service_Symptom__c = orderProductIdWithServiceSymptom.get(pb.Service_Product_lkup__c).Id;
						}
					}
					upsert problems;
				}

				if (warranties.size() > 0) {
					for (Warranty__c wc : warranties) {
						system.debug('Warranty__c==== '+ wc);
						if (orderProductIdWithProblemComponent.containsKey(wc.Service_Product__c)) {
							wc.Problem_Component__c = orderProductIdWithProblemComponent.get( wc.Service_Product__c).Id;
						}
					}
					upsert warranties;
				}

				saveReimbursableParts(newPartList, orderId);

			}

		}
		catch (Exception ex) {
			System.debug('@@@' + ex.getMessage());
		}

		return symWrapper;

	}

	@AuraEnabled
	public static void deleteAllRecords(Id orderId, Id orderProductId) {

		delete [SELECT Id FROM Service_Symptom__c WHERE Service_Product__c = :orderProductId];
		delete [SELECT Id FROM Problem_Component__c WHERE Service_Product_lkup__c = :orderProductId];
		delete [SELECT Id FROM Warranty__c WHERE Service_Product__c = :orderProductId];
		delete [SELECT Id FROM Reimbursable_Part__c WHERE Order__c = :orderId AND Order_Product__c = :orderProductId];

		////Added this MRJ Demand Chain 3.22.2021////
		OrderItem oi = [SELECT Id,
							   Number_Warranties__c,
							   Number_Service_Symptoms__c
						FROM   OrderItem
						WHERE  Id = :orderProductId];
		oi.Number_Warranties__c = 0;
		oi.Number_Service_Symptoms__c = 0;
		update oi;

	}

  /*
   * @author Jason Flippen
   * @date 01/12/2021
   * @description Method to delete a Reimbursable Part.
   */
	@AuraEnabled
	public static void deleteReimbursablePart(Id reimbursablePartId) {

		Reimbursable_Part__c deleteReimbursablePart = new Reimbursable_Part__c(Id = reimbursablePartId);
		delete deleteReimbursablePart;

	}

	@AuraEnabled
	public static List<ChargeWrapper> getProcedureList(String symptomJSON) {

		List<ChargeWrapper> symWrapper = new List<ChargeWrapper>();

		try {

			List<Service_Symptom__c> serviceSymptoms = new List<Service_Symptom__c>();
			Set<Id> serviceSymptomIds = new Set<Id>();
			Map<String, List<ComboboxWrapper>> mapWithProblems = new Map<String, List<ComboboxWrapper>>();
			Set<String> orderProductIds = new Set<String>();

			symWrapper = (List<ChargeWrapper>) JSON.deserialize(symptomJSON,List<ChargeWrapper>.class);
			System.debug('@@@symWrapper' + symWrapper);
			for (ChargeWrapper chargeWrap : symWrapper) {
				orderProductIds.add(chargeWrap.orderProductId);
				serviceSymptomIds.add(chargeWrap.selectedSymptom);
			}

			Set<String> productIds = new Set<String>();
			Map<String, String> orderIdWithProductId = new Map<String, String>();
			for (OrderItem orderItem : [SELECT Id, Product2Id
										FROM   OrderItem
										WHERE  Id IN :orderProductIds]) {
				productIds.add(orderItem.Product2Id);
				orderIdWithProductId.put(orderItem.Id, orderItem.Product2Id);
			}
			System.debug('@@@orderIdWithProductId' + orderIdWithProductId);
			System.debug('@@@serviceSymptomIds' + serviceSymptomIds);

			List<Id> ssIds = new List<Id>();
			for (Service_Symptom__c symptoms : [SELECT Id,
													   Service_Symptom__c,
													   Service_Product__c,
													   Service_Symptom__r.Inactive__c
												FROM   Service_Symptom__c
												WHERE  Service_Product__c IN :orderProductIds
												AND    Service_Symptom__c IN :serviceSymptomIds
												AND    Service_Symptom__r.Inactive__c = false]) {
				ssIds.add(symptoms.Service_Symptom__c);
			}
			System.debug('@@@ssIds' + ssIds);
			System.debug('@@@serviceSymptomIds' + orderIdWithProductId);

			List<Id> ssAssign = new List<Id>();
			for (Service_Symptom_Assignment__c assignments : [SELECT Id,
																	 Service_Symptom__c,
																	 Procedure_Code__c
															  FROM   Service_Symptom_Assignment__c
															  WHERE  Service_Symptom__c = :serviceSymptomIds]) {
				ssAssign.add(assignments.Procedure_Code__c);
			}
			System.debug('@@@ssAssign' + ssAssign);

			for (Procedure_Code_Assignment__c procedures : [SELECT Id,
																   Name,
																   Service_Product__c,
																   Procedure_Code__c,
																   Procedure_Code__r.Inactive__c
															FROM   Procedure_Code_Assignment__c
															WHERE  Service_Product__c IN :productIds
															AND    Procedure_Code__c = :ssAssign
															AND    Procedure_Code__r.Inactive__c = false
															ORDER BY Procedure_Code_Assignment__c.Name ASC]) {
				System.debug('@@@procedures' + procedures);
				ComboboxWrapper sl = new ComboboxWrapper(procedures.Name,procedures.Id);
				if (!mapWithProblems.containsKey(procedures.Service_Product__c)) {
					mapWithProblems.put(procedures.Service_Product__c,new List<ComboboxWrapper>());
				}
				mapWithProblems.get(procedures.Service_Product__c).add(sl);
			}
			System.debug('@@@ssAssign' + mapWithProblems);

			for (ChargeWrapper chargeWrap : symWrapper) {

				System.debug('@@@chargeWrap' + chargeWrap);

				String prodId;
				if (orderIdWithProductId.containsKey(chargeWrap.orderProductId)) {
					system.debug('mapWithProblems.get(prodId): ' + mapWithProblems.get(prodId));
					prodId = orderIdWithProductId.get(chargeWrap.orderProductId);
				}

				if (mapWithProblems.containsKey(prodId)) {
					system.debug('mapWithProblems.get(prodId): ' + mapWithProblems.get(prodId));
					chargeWrap.procedures = mapWithProblems.get(prodId);
				}

			}

		}
		catch (Exception ex) {
			System.debug('@@@' + ex.getMessage());
		}

		return symWrapper;

	}

	/*
	* @author Jason Flippen
	* @date 08/24/2020
	* @description Method to return details of a Reimbursable Part (aka Product).
	*/
	@AuraEnabled(cacheable=true)
	public static ReimbursablePartWrapper getReimbursablePartDetail(Id partId) {

		ReimbursablePartWrapper wrapper = new ReimbursablePartWrapper();

		// Grab the List of Parts related to the Part Id (should only be one).
		List<Product2> partList = [SELECT Id,
										  Description,
										  Name,
										  Reimbursable_Part_Cost__c
								   FROM Product2
								   WHERE Id = :partId];

		if (!partList.isEmpty()) {

			// Update the Wrapper properties.
			wrapper.id = partList[0].Id;
			wrapper.description = partList[0].Description;
			wrapper.name = partList[0].Name;
			wrapper.quantity = 0;
			wrapper.reason = '';
			wrapper.unitPrice = 0;
			if (partList[0].Reimbursable_Part_Cost__c > 0) {
				wrapper.unitPrice = partList[0].Reimbursable_Part_Cost__c;
			}

		}

		return wrapper;

	}

	/*
	* @author Jason Flippen
	* @date 01/05/2021
	* @description Method to return details of a Reimbursable Part (aka Product).
	*/
	private static void saveReimbursableParts(List<ReimbursablePartWrapper> newPartList, Id orderId) {

		// Iterate through the List of new Parts and grab the List
		// of new Reimbursable Part records to be inserted.
		List<Reimbursable_Part__c> newReimbursablePartList = new List<Reimbursable_Part__c>();
		for (ReimbursablePartWrapper part : newPartList) {
			// We only need to create newly added records.
			if (part.isNew == true) {
				Reimbursable_Part__c newReimbursablePart = new Reimbursable_Part__c(Order__c = orderId,
																					Order_Product__c = part.orderProductId,
																					Part__c = part.id,
																					Quantity__c = part.quantity,
																					Reason__c = part.reason,
																					Unit_Price__c = part.unitPrice);
				newReimbursablePartList.add(newReimbursablePart);
			}
		}

		try {

			// If we have new Reimbursable Parts to be inserted, insert them.
			if (!newReimbursablePartList.isEmpty()) {
				insert newReimbursablePartList;
			}

		}
		catch (Exception ex) {
			System.debug('***** saveReimbursableParts Error: ' + ex.getMessage());
		}

	}


/** Wrapper Classes **/


	public class ComboboxWrapper {

		@AuraEnabled
		public String label;

		@AuraEnabled
		public String value;

		public ComboboxWrapper(String label, String value) {
			this.label = label;
			this.value = value;
		}

	}

	public class ChargeWrapper {

		@AuraEnabled
		public String chargeId;

		@AuraEnabled
		public String productName;

		@AuraEnabled
		public String chargeCostTo;

		@AuraEnabled
		public String category;

		@AuraEnabled
		public String variantNumber;

		@AuraEnabled
		public Date installDate;

		@AuraEnabled
		public String component;

		@AuraEnabled
		public String iconPath;

		@AuraEnabled
		public String orderProductId;

		@AuraEnabled
		public String selectedSymptom;

		@AuraEnabled
		public String selectedSymptomName;

		@AuraEnabled
		public List<ComboboxWrapper> problems;

		@AuraEnabled
		public List<ComboboxWrapper> procedures;

		@AuraEnabled
		public String problemComponentName;

		@AuraEnabled
		public String problemComponent;

		@AuraEnabled
		public String selectedProcedure;

		@AuraEnabled
		public String procedureName;

		@AuraEnabled
		public Decimal selectedQty;

		@AuraEnabled
		public String selectedProcedure2;

		@AuraEnabled
		public String procedureName2;

		@AuraEnabled
		public Decimal selectedQty2;

		@AuraEnabled
		public String variant;

		@AuraEnabled
		public Boolean isReimbursementCompleted;

		@AuraEnabled
		public String symptomId;

		@AuraEnabled
		public String problemId;

		@AuraEnabled
		public String procedureId;

		@AuraEnabled
		public String procedureId2;

		@AuraEnabled
		public Boolean isNew;

		public ChargeWrapper(String chargeId,
							 String productName,
							 String chargeCostTo,
							 String category,
							 String variantNumber,
							 Date installDate,
							 String component,
							 String orderProductId,
							 Boolean isReimbursementCompleted,
							 String selectedSymptom,
							 String selectedSymptomName,
							 String problemComponentName,
							 String problemComponent,
							 String selectedProcedure,
							 Decimal selectedQty,
							 String selectedProcedure2,
							 Decimal selectedQty2,
							 String procedureName,
							 String procedureName2,
							 String symptomId,
							 String problemId,
							 String procedureId,
							 String procedureId2) {
			
			this.chargeId = chargeId;
			this.productName = String.isNotEmpty(productName) ? productName : '';
			this.chargeCostTo = String.isNotEmpty(chargeCostTo) ? chargeCostTo : '';
			this.category = String.isNotEmpty(category) ? category : '';
			this.variantNumber = String.isNotEmpty(variantNumber) ? variantNumber : '';
			this.installDate = installDate;
			this.component = component;
			this.iconPath = isReimbursementCompleted ? 'utility:check' : 'utility:add';
			this.variant = isReimbursementCompleted ? 'brand' : 'warning';
			this.isReimbursementCompleted = isReimbursementCompleted;
			this.selectedSymptomName = selectedSymptomName;
			this.orderProductId = orderProductId;
			this.selectedSymptom = selectedSymptom;
			this.problemComponentName = problemComponentName;
			this.problemComponent = problemComponent;
			this.selectedProcedure = selectedProcedure;
			this.selectedQty = selectedQty;
			this.selectedProcedure2 = selectedProcedure2;
			this.selectedQty2 = selectedQty2;
			this.procedureName = procedureName;
			this.procedureName2 = procedureName2;
			this.symptomId = symptomId;
			this.problemId = problemId;
			this.procedureId = procedureId;
			this.procedureId2 = procedureId2;
			this.isNew = false;

			this.problems = new List<ComboboxWrapper>();
			this.procedures = new List<ComboboxWrapper>();

		}
	}

  /*
   * @author Jason Flippen
   * @date 08/24/2020
   * @description Wrapper Class for a Reimbursable Part (aka Product).
   */
	@TestVisible
	public class ReimbursablePartWrapper {
			
		@AuraEnabled
		public String id {get;set;}

		@AuraEnabled
		public String description {get;set;}

		@AuraEnabled
		public Boolean isNew {get;set;}

		@AuraEnabled
		public String name {get;set;}

		@AuraEnabled
		public String orderProductId {get;set;}

		@AuraEnabled
		public String productName {get;set;}

		@AuraEnabled
		public Decimal quantity {get;set;}

		@AuraEnabled
		public String reason {get;set;}

		@AuraEnabled
		public Decimal unitPrice {get;set;}

	}

}