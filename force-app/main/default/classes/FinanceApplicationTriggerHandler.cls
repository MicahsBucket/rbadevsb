public with sharing class FinanceApplicationTriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public FinanceApplicationTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }

    public void OnBeforeInsert(Finance_Application__c[] newFAList){
        updateSSN(newFAList);
    }
    public void OnBeforeUpdate(Finance_Application__c[] oldFAList, Finance_Application__c[] updatedFAList, Map<ID, Finance_Application__c> oldFAMap, Map<ID, Finance_Application__c> newFAMap){
        updateSSN(updatedFAList);
    }
    private void updateSSN(List<Finance_Application__c> faList){
        Set<Id> contactIdSet = new Set<Id>();
        for(Finance_Application__c fa : faList){
            contactIdSet.add(fa.Customer__c);
        }
        Map<Id, Contact> contactMap = new Map<Id, Contact>(
            [SELECT Id, LASERCA__Social_Security_Number__c, LASERCA__Co_Applicant_Social_Security_Number__c
             FROM Contact
             WHERE Id IN :contactIdSet]
        );
        for(Finance_Application__c fa : faList){
            if(contactMap.containsKey(fa.Customer__c)){
                Contact contact = contactMap.get(fa.Customer__c);
                fa.Social_Security_Number__c = contact.LASERCA__Social_Security_Number__c;
                fa.Co_Applicant_Social_Security_Number__c = contact.LASERCA__Co_Applicant_Social_Security_Number__c;
            }
        }
    }
}