global class BatchScheduleOrderHistoryTracking implements Schedulable {
    global void execute(SchedulableContext sc)
    {
        BatchJobOrderHistoryTracking obj = new BatchJobOrderHistoryTracking ();
            
        database.executebatch(obj, 5);
    }   
}