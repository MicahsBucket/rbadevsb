@isTest
public class TestDataFactory {

	/*******************************************************************************************************
	* @description creates store account
	* @param storeName gives the store a name
	* @return Account
	* @example
	* TestDataFactory.createStoreAccount('Calvins Store'); creates a Store Account named Calvins Store
	*/
	public static Account createStoreAccount(String storeName){
		Account theAccount = new Account(
			Name = storeName,
			AccountNumber = '1234567890',
			Phone = '(763) 555-2000',
			recordTypeId = retrieveRecordTypeId('Store', 'Account'),
			Survey_Holds__c = 5
		);
		
		return theAccount;
	}

	/*******************************************************************************************************
	* @description creates a Customer Contact
	* @param accountId id of the account that the contact is associated to
	* @return Contact
	* @example
	* TestDataFactory.createCustomerContact(accountId); creates a contact associated to an account
	*/
	public static Contact createCustomerContact(Id accountId){
		 
		Contact testContact = new Contact(
			AccountId = accountId,
			FirstName = 'Test',
			LastName = 'Contact',
			Email = 'test@testing.com',
			Finance_Pending_Picklist__c = 'No',
			Has_Customer_Authorization_Picklist__c = 'Yes',
			recordTypeId = retrieveRecordTypeId('Customer', 'Contact')
		);
		
		return testContact;
	}

	/*******************************************************************************************************
	* @description creates a user
	* @param userName first name of the user used to find during SOQL queries
	* @param profileName name of the profile the user should have ie. 'Partner RMS-RSR'
	* @param contactId contact id for the contact that the user will be related to
	* @return User
	* @example
	* TestDataFactory.createPartnerUser('Calvin', 'Partner RMS-RSR', contactId); creates a user with the Partner RMS-RSR User profile
	*/
	public static User createPartnerUser(String userName, String profileName, Id contactId){
		Profile p = [SELECT Id from Profile WHERE Name = :profileName LIMIT 1];
		String orgId = UserInfo.getOrganizationId();
    	String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    	Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    	String uniqueName = orgId + dateString + randomInt;  

		User testUser = new User(
			FirstName = userName,
			LastName = 'TestLast',
			email = uniqueName + '@test' + orgId + '.org',
            Username = uniqueName + '@test' + orgId + '.org',			
           	CommunityNickname = 'test' + String.valueOf(System.currentTimeMillis()),
			TimeZoneSidKey = 'America/Mexico_City', 
			LocaleSidKey = 'en_US', 
			EmailEncodingKey= 'UTF-8', 
			ProfileId = p.Id, 
			LanguageLocaleKey = 'en_US',
			ContactId = contactId,
			Alias = 'alias'
			
		);
		return testUser;
	}

	/*******************************************************************************************************
	* @description Generates and inserts surveys
	* @param recordType record type of the survey
	* @param surveyNum number of surveys user wants
	* @param accountId account Id for the account the survey is associated to
	* @param status the survey status ie. 'Pending', 'Hold'
	* @return List<Survey__c>
	* @example
	* TestDataFactory.createSurveys('Post Install', 5, accountId, 'Pending'); creates 5 pending surveys
	*/
	public static List<Survey__c> createSurveys(String recordType, Integer surveyNum, Id accountId, String status){
		List<Survey__c> surveyList = new List<Survey__c>();
		Id recordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
		
		for(Integer i = 0; i < surveyNum; i++){
			Survey__c ns = new Survey__c(
				RecordTypeId = recordTypeId,
				Survey_Status__c = status,
				Primary_Contact_Email__c = 'test' + i + '@user.com',
				Primary_Contact_First_Name__c = 'first' + i,
				Primary_Contact_Last_Name__c = 'last' + i,
				Primary_Contact_Mobile_Phone__c = '1234567890',
				State__c = 'Minnesota',
				City__c = 'St. Paul',
				Street__c = '1111 Fairmount Ave.',
				Zip__c = '55105',
				Store_Account__c = accountId
			);
			surveyList.add(ns);
		}
		Database.insert(surveyList, true);
		return surveyList;
	}

	/*******************************************************************************************************
	* @description Generates and inserts surveys with source system and appointment date
	* @param recordType record type of the survey
	* @param surveyNum number of surveys user wants
	* @param accountId account Id for the account the survey is associated to
	* @param status the survey status ie. 'Pending', 'Hold'
	* @return List<Survey__c>
	* @example
	* TestDataFactory.createSurveys('Post Install', 5, accountId, 'Pending', date.today()); creates 5 pending surveys with appointment dates of today
	*/
	public static List<Survey__c> createSurveys(String recordType, Integer surveyNum, Id accountId, String status, Date dateVal){
		List<Survey__c> surveyList = new List<Survey__c>();
		Id recordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
		
		for(Integer i = 0; i < surveyNum; i++){
			Survey__c ns = new Survey__c(
				RecordTypeId = recordTypeId,
				Survey_Status__c = status,
				Primary_Contact_Email__c = 'test' + i + '@user.com',
				Primary_Contact_First_Name__c = 'first' + i,
				Primary_Contact_Last_Name__c = 'last' + i,
				Primary_Contact_Mobile_Phone__c = '1234567890',
				State__c = 'Minnesota',
				City__c = 'St. Paul',
				Street__c = '1111 Fairmount Ave.',
				Zip__c = '55105',
				Appointment_Date__c = dateVal,
				Appointment_Result__c = 'Sale',
				Source_System__c = 'EnabledPlus',
				Store_Account__c = accountId
			);
			surveyList.add(ns);
		}
		Database.insert(surveyList, true);
		return surveyList;
	}

	/*******************************************************************************************************
	* @description Generates and inserts surveys with source system and appointment date
	* @param recordType record type of the survey
	* @param surveyNum number of surveys user wants
	* @param accountId account Id for the account the survey is associated to
	* @param status the survey status ie. 'Pending', 'Hold'
	* @return List<Survey__c>
	* @example
	* TestDataFactory.createSurveys('Post Install', 5, accountId, 'Pending', date.today()); creates 5 pending surveys with appointment dates of today
	*/
	public static List<Survey__c> createSurveysCan(String recordType, Integer surveyNum, Id accountId, String status, Date dateVal){
		List<Survey__c> surveyList = new List<Survey__c>();
		Id recordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
		
		for(Integer i = 0; i < surveyNum; i++){
			Survey__c ns = new Survey__c(
				RecordTypeId = recordTypeId,
				Survey_Status__c = status,
				Primary_Contact_Email__c = 'test' + i + '@user.com',
				Primary_Contact_First_Name__c = 'first' + i,
				Primary_Contact_Last_Name__c = 'last' + i,
				Primary_Contact_Mobile_Phone__c = '1234567890',
				State__c = 'Minnesota',
				City__c = 'St. Paul',
				Street__c = '1111 Fairmount Ave.',
				Zip__c = '55105',
				Appointment_Date__c = dateVal,
				Appointment_Result__c = 'Sale',
				Source_System__c = 'EnabledPlus',
				Store_Account__c = accountId,
				Country__c = 'Canada'
			);
			surveyList.add(ns);
		}
		Database.insert(surveyList, true);
		return surveyList;
	}

	/*******************************************************************************************************
	* @description Generates and inserts workers
	* @param role workers role ie. 'Sales Rep', 'Tech Measurer', 'Installer'
	* @param workerNum number of workers user wants
	* @param accountId account Id for the account the worker is associated to
	* @return List<Worker__c>
	* @example
	* TestDataFactory.createWorkers('Sales Rep', 5, accountId); creates 5 workers with the role of sales rep
	*/
	public static List<Worker__c> createWorkers(String role, Integer workerNum, Id accountId){
		List<Worker__c> workerList = new List<Worker__c>();
		for(Integer i = 0; i < workerNum; i++){
			Worker__c w = new Worker__c(
				First_Name__c = 'worker' + i,
				Last_Name__c = 'lastname' + i,
				Role__c = role,
				Store_Account__c = accountId,
				Email__c = 'test@test.com'
			);
			workerList.add(w);
		}
		Database.insert(workerList, true);
		return workerList;
	}

	/*******************************************************************************************************
	* @description Generates and inserts workers and connect them to a survey via a junction object
	* @param role workers role ie. 'Sales Rep', 'Tech Measurer', 'Installer'
	* @param workerNum number of workers user wants
	* @param accountId account Id for the account the worker is associated to
	* @param surveyId survey Id for the survey the workers will be attached to
	* @return List<Worker__c>
	* @example
	* TestDataFactory.createWorkers('Sales Rep', 5, accountId, surveyId); creates 5 workers with the role of sales rep
	*/
	public static List<Worker__c> createWorkers(String role, Integer workerNum, Id accountId, Id surveyId){
		List<Worker__c> workerList = new List<Worker__c>();
		List<Survey_Worker__c> surveyWorkerJunc = new List<Survey_Worker__c>();
		for(Integer i = 0; i < workerNum; i++){
			Worker__c w = new Worker__c(
				First_Name__c = 'worker' + i,
				Last_Name__c = 'lastname' + i,
				Role__c = role,
				Store_Account__c = accountId
			);
			workerList.add(w);
		}
		Database.insert(workerList, true);
		for(Worker__c w : workerList){
			Survey_Worker__c sw = new Survey_Worker__c(
				Survey_Id__c = surveyId,
				Worker_Id__c = w.Id,
				Role__c = role
			);	
			surveyWorkerJunc.add(sw);
		}
		Database.insert(surveyWorkerJunc, true);
		system.debug('workerListSize: ' + workerList.size());
		return workerList;
	}
	


	//																\\
	//EVERYTHING BELOW IS REQUIRED IN ORDER TO SET UP STORE ACCOUNTS\\
	//																\\
	public static void setUpConfigs(){

		//Creating RMS_Settings__c
		RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
		insert customSetting1;
        
        RMS_Settings__c customSettingrp = new RMS_Settings__c(Value__c = '12345678902', Name='Ordering Portal Profile ID');
		insert customSettingrp;

		User u = [select Id from User WHERE Id =: UserInfo.getUserId()];
		u.Store_Locations__c = '77 - Twin Cities, MN';
		u.Default_Store_Location__c = '77 - Twin Cities, MN';
		update u;
		system.runAs(u){
			id dwellingRT = retrieveRecordTypeId('Dwelling', 'Account');
			id storeRT = retrieveRecordTypeId('Store', 'Account');
		
			Account store1 = createStoreAccount('77 - Twin Cities, MN');
			store1.Store_Number__c = '0123';
			upsert store1;

			Account masterAccount = new Account(name = 'Unassigned Account', RecordTypeId = dwellingRT);
			insert masterAccount;

			RMS_Settings__c customSetting2 = new RMS_Settings__c(Value__c = masterAccount.Id, Name='Unassigned Account Id');
			insert customSetting2;
		
		
			//Creating RMS_Queue_Settings__c
			List<SObject> list2 = new List<SObject>();
			List<SObject> list1 = new List<SObject>();
			Group Order_Queue = createQueue('Order_Queue');
			list1.add(Order_Queue);
			Group Install_Work_Order_Queue = createQueue('Install_Work_Order_Queue');
			list1.add(Install_Work_Order_Queue);
			Group Tech_Measure_Work_Order_Queue = createQueue('Tech_Measure_Work_Order');
			list1.add(Tech_Measure_Work_Order_Queue);
			Group Permit_Work_Order_Queue = createQueue('Permit_Work_Order');
			list1.add(Permit_Work_Order_Queue);
			Group Action_Work_Order_Queue = createQueue('Action_Work_Order');
			list1.add(Action_Work_Order_Queue);
			Group LSWP_Work_Order_Queue = createQueue('LSWP_Work_Order');
			list1.add(LSWP_Work_Order_Queue);
			insert list1;

			QueueSobject Order_Queue_Link = createQueueSobject(Order_Queue.id ,'Order');
			list2.add(Order_Queue_Link);
			QueueSobject Install_Work_Order_Link = createQueueSobject(Install_Work_Order_Queue.id ,'RbA_Work_Order__c');
			list2.add(Install_Work_Order_Link);
			QueueSobject Tech_Measure_Work_Order_Link = createQueueSobject(Tech_Measure_Work_Order_Queue.id ,'RbA_Work_Order__c');
			list2.add(Tech_Measure_Work_Order_Link);
			QueueSobject Permit_Work_Order_Link = createQueueSobject(Permit_Work_Order_Queue.id ,'RbA_Work_Order__c');
			list2.add(Permit_Work_Order_Link);
			QueueSobject Action_Work_Order_Link = createQueueSobject(Action_Work_Order_Queue.id ,'RbA_Work_Order__c');
			list2.add(Action_Work_Order_Link);
			QueueSobject LSWP_Work_Order_Link = createQueueSobject(LSWP_Work_Order_Queue.id ,'RbA_Work_Order__c');
			list2.add(LSWP_Work_Order_Link);
		
			Store_Configuration__c storeConfig = new Store_Configuration__c(
				Store__c = store1.id,
				Order_Queue_Id__c = Order_Queue.id,
				Install_Work_Order_Queue_Id__c = Install_Work_Order_Queue.id,
				Tech_Measure_Work_Order_Queue_Id__c = Tech_Measure_Work_Order_Queue.id,
				Standard_Hourly_Rate__c = 50,		   
				Building_Permit_Work_Order_Queue_Id__c = Permit_Work_Order_Queue.id,
				Historical_Work_Order_Queue_Id__c = Action_Work_Order_Queue.id,
				HOA_Work_Order_Queue_Id__c = Action_Work_Order_Queue.id,
				LSWP_WO_Owner_ID__c = LSWP_Work_Order_Queue.id,
				Location_Number__c = '777',
				Order_Number__c = 0000001
				);
			list2.add(storeConfig);
			insert list2;

			store1.Active_Store_Configuration__c = storeConfig.Id;
			update store1;
			
			rSuite_Configuration__c rc = new rSuite_Configuration__c();
			rc.Store_Configuration__c = storeConfig.Id;
			rc.Unsold_Age_In_Months__c = 4;
			insert rc;
			
			storeConfig.rSuite_Configuration__c = rc.Id;
			update storeConfig;
		
			Back_Office_Checklist_Configuration__c backOfficeChecklist1 = new Back_Office_Checklist_Configuration__c(
				Store_Configuration__c = storeConfig.id,
				Contract_Signed__c = true,
				Lien_Rights_Signed__c = true
				);
			insert backOfficeChecklist1;

			WorkType measureWT = new WorkType();
            measureWT.Name = 'Measure';
            measureWT.DurationType = 'Hours';
            measureWT.EstimatedDuration = 2;
            measureWT.ShouldAutoCreateSvcAppt = true;
            measureWT.FSL__Exact_Appointments__c = false;

            WorkType installWT = new WorkType();
            installWT.Name = 'Install';
            installWT.DurationType = 'Hours';
            installWT.EstimatedDuration = 8;
            installWT.ShouldAutoCreateSvcAppt = true;
            installWT.FSL__Exact_Appointments__c = false;

            WorkType serviceWT = new WorkType();
            serviceWT.Name = 'Service';
            serviceWT.DurationType = 'Hours';
            serviceWT.EstimatedDuration = 1;
            serviceWT.ShouldAutoCreateSvcAppt = true;
            serviceWT.FSL__Exact_Appointments__c = false;

            WorkType jobSiteVisitWT = new WorkType();
            jobSiteVisitWT.Name = 'Job Site Visit';
            jobSiteVisitWT.DurationType = 'Hours';
            jobSiteVisitWT.EstimatedDuration = 1;
            jobSiteVisitWT.ShouldAutoCreateSvcAppt = true;
            jobSiteVisitWT.FSL__Exact_Appointments__c = false;
            insert new List<WorkType>{measureWT,installWT,serviceWT,jobSiteVisitWT};
		}
			
	}
	public static Group createQueue(String groupName){
		Group theQueue = new Group(Name = groupName, Type = 'Queue');
		return theQueue;
	}
	
	public static QueueSobject createQueueSobject(Id groupId, String typeOfObject){
		QueueSobject theQueueSobject = new QueueSObject(QueueId = groupId, SobjectType = typeOfObject);
		return theQueueSobject;
	}
	public static Id retrieveRecordTypeId(String developerName, String sobjectType){
		return RecordTypeFor(sobjectType, developerName);
	}
	
	// Retrieves Id for recordtype using object name and dev name
	public static Id RecordTypeFor(String sObjectName, String devName) {

		String devString = sObjectName + devName;
		if (masterRTList == null || masterRTList.isEmpty()) {
			for (RecordType rt : [SELECT Id, developerName, sObjectType FROM RecordType WHERE IsActive = true]) {
				masterRTList.put(rt.sObjectType + rt.DeveloperName, rt.Id);
			}
		}
				
		return masterRTList.get(sObjectName + devName);
	}

	public static Map<String,Id> masterRTList = new Map<String,Id>();

	public static List<Custom_Rollup_Rolldown__mdt> getRollupMetadata(){

	
		List<Custom_Rollup_Rolldown__mdt> CRRList = [SELECT Id, DeveloperName, MasterLabel, Language, NamespacePrefix, Label, QualifiedApiName, 
														Rollup_Number__c, Rollup_Name__c, Parent_Object__c, Child_Object__c, 
														Relationship_Field__c, Relationship_Criteria__c, Relationship_Criteria_Fields__c, 
														Field_to_Aggregate__c, Aggregate_Operation__c, Aggregate_Result_Field__c, 
														Aggregate_All_Rows__c, Active__c
														FROM Custom_Rollup_Rolldown__mdt];
		return CRRList;
	}
}