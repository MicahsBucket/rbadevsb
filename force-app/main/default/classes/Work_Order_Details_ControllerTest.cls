@isTest
public class Work_Order_Details_ControllerTest {
    private static final String WORK_TYPE_NAME = 'Test Type 1';
    private static final String INSTALL_WORK_TYPE_NAME = 'Install';
    private static final String MEASURE_WORK_TYPE_NAME = 'Measure';
    private static final String SERVICE_WORK_TYPE_NAME = 'Service';
    private static final String JOB_SITE_VISIT_WORK_TYPE_NAME = 'Job Site Visit';
    private static WorkOrder measureWO;
    
    @TestSetup
    static void setup(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        // FSLRollupRolldownController.disableFSLRollupRolldownController = true;
        
        OperatingHours oh1 = new OperatingHours();
        oh1.Name = 'Test Operating Hours One';
        oh1.TimeZone = 'America/Chicago';
        insert oh1;
        
        ServiceTerritory st1 = new ServiceTerritory();
        st1.Name = 'Test Service Territory One';
        st1.OperatingHoursId = oh1.Id;
        st1.IsActive = true;
        insert st1;
        
        WorkType installWorkType = new WorkType(
            Name = INSTALL_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType measureWorkType = new WorkType(
            Name = MEASURE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType serviceWorkType = new WorkType(
            Name = SERVICE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType jobSiteVisitWorkType = new WorkType(
            Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 1,
            DurationType = 'Hours'
        );
        
        insert new List<WorkType>{installWorkType, measureWorkType, serviceWorkType, jobSiteVisitWorkType};
            
            measureWO = new WorkOrder(
                Recommended_Crew_Size__c = 2,
                Street = '219 North Milwaukee Street',
                City = 'Milwaukee',
                State = 'Wisconsin',
                PostalCode = '53208',
                CountryCode = 'US',
                WorkTypeId = measureWorkType.Id,
                ServiceTerritoryId = st1.Id
            );
        insert measureWO;
        
        Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
        Database.insert(storeAccount, true);
        Id storeAccountId = storeAccount.Id;
        
        Contact testContact = new Contact(
            AccountId = storeAccountId,
            FirstName = 'Test',
            LastName = 'Contact',
            Email = 'test@testing.com',
            Phone = '(763) 555-0000',
            HomePhone = '(763) 555-1111',
            MobilePhone = '(763) 555-2222',
            Salutation = 'Mr.',
            Comments__c = 'This is a comment.',
            Preferred_Method_of_Contact__c = 'Mobile Phone'
        );
        insert testContact;
        
        ServiceAppointment appt1 = new ServiceAppointment();
        appt1.ContactId = testContact.Id;
        appt1.ParentRecordId = measureWO.Id;
        appt1.SchedStartTime = System.now().addDays(7);
        appt1.SchedEndTime = System.now().addDays(8);
        appt1.ArrivalWindowStartTime = System.now().addDays(7);
        appt1.ArrivalWindowEndTime = System.now().addDays(7).addHours(2);
        appt1.Status = 'Scheduled';
        insert appt1;
        
        // FSLRollupRolldownController.disableFSLRollupRolldownController = false; 
    }
    
    @isTest 
    static void testgetWorkOrder() {
        WorkOrder wo = [SELECT id FROM WorkOrder LIMIT  1];
        ServiceAppointment sa = [SELECT id FROM ServiceAppointment LIMIT  1];
        Work_Order_Details_Controller.getWorkOrder(wo.Id);
        Work_Order_Details_Controller.getServiceAppointment(sa.Id);
        TimeZone tz = UserInfo.getTimeZone();
        System.assertEquals(tz.getID(),Work_Order_Details_Controller.getCurrentLoggedInUserTimeZone());
    }
    
}