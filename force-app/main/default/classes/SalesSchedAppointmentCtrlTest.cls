/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 02-16-2021
 * @last modified by  : Connor.Davis@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   10-14-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
private class SalesSchedAppointmentCtrlTest {
    static testmethod void testDeleteAssignment(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
        insert resources;
        
        SalesSchedAppointmentManagerCtrl.MyAppointmentWrapper maw = null;
        Test.startTest();
        System.runAs(sstu.reps.get(0)){
            SalesSchedAppointmentCtrl.deleteAssignment(resources.get(0).Sales_Appointment__c);
        }
        Test.stopTest();
        
        System.assert([Select Id From Sales_Appointment_Resource__c].isEmpty());
    }
     
    static testmethod void getAppointmentException(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,2);
        sstu.createAppointments(2);
        
        Id appointmentId = sstu.appointments.get(0).Id;
        delete sstu.appointments.get(0);
        
        Test.startTest();
        boolean exceptionThrown = false;
        try{
            SalesSchedAppointmentCtrl.getAppointment(appointmentId);            
        }catch(Exception e){
            exceptionThrown = true;
        }
        System.assert(exceptionThrown);
    }
    
	static testmethod void testGetPastAppointmentsException(){
    	SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,2);
        sstu.createAppointments(2);
        
        Id appointmentId = sstu.appointments.get(0).Id;
        delete sstu.appointments.get(0);
        
        Test.startTest();
        boolean exceptionThrown = false;
        try{
            SalesSchedAppointmentCtrl.getPastAppointments(appointmentId);
        }catch(Exception e){
            exceptionThrown = true;
        }
        System.assert(exceptionThrown);
	}
    
    static testmethod void testGetPastAppointments(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
        Id appointmentId = sstu.appointments.get(0).Id;
        
        
       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual'
            )
        );
        
        insert new Sales_Appointment__c(
        	Sales_Order__c = sstu.salesOrder.Id,
          	Appointment_Date_Time__c = Datetime.newInstance(Date.today().addDays(-1),sstu.slot.Start__c),
            Date__c = Date.today().addDays(-1)
        );

        
         
        //Create a past appointment that has not been resulted yet.
        sstu.createPastAppointments(1);
        sstu.salesOrder.Result__c = 'Sale';
        sstu.salesOrder.Time_Arrived__c = Datetime.now().time();
        sstu.salesOrder.Time_Left__c = Datetime.now().time();
        update sstu.salesOrder;        
		resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.pastCapacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Resulted',
                Assignment_Reason__c = 'Manual'
            )
        );
      	insert resources;
        
        Test.startTest();
        List<Sales_Appointment__c> appointments = new List<Sales_Appointment__c>();
        SalesSchedAppointmentCtrl.AppointmentWrapper wrapper;
        System.runAs(sstu.reps.get(0)){
            appointments = SalesSchedAppointmentCtrl.getPastAppointments(appointmentId);
            wrapper = SalesSchedAppointmentCtrl.getAppointment(appointmentId);
        }
        System.assert(!appointments.isEmpty());
        System.assert(!wrapper.actSalOrdHist.asaccwrap.isEmpty());
    }
    
    static testmethod void testGetAppointment(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
         List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
      	insert resources;
        Test.startTest();
        SalesSchedAppointmentCtrl.AppointmentWrapper aw = null;
        System.runAs(sstu.reps.get(0)){
            aw = SalesSchedAppointmentCtrl.getAppointment(sstu.appointments.get(0).Id);
            SalesSchedAppointmentCtrl.getAvailableCapacities(sstu.appointments.get(0).Id,'');
        }
        Test.stopTest();
        
        System.assertNotEquals(null,aw.appointment);
    }
    static testmethod void testGetCancelAppointment(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        Sales_Appointment__c sa = sstu.appointments.get(0);
        sa.Cancelled__c = true;
        test.startTest();
        update sa;
        List<Sales_Appointment__c> salesAppt = [Select Id,Cancelled__c From Sales_Appointment__c where Id=:sa.Id];
      	
        List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Accepted',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
      	insert resources;
        List<Sales_Appointment_Resource__c> resourcess = [Select 
                Id,
                Sales_Capacity__r.User__r.Contact.Email,
				Sales_Capacity__r.User__r.Contact.Secondary_Email__c,
                Sales_Appointment__r.Sales_Order__r.Opportunity__r.Name,
                Sales_Appointment__r.Appointment_Date_Time__c, 
                Primary__c, Finalized_By__r.Contact.Email
                From  Sales_Appointment_Resource__c WHERE Id =: resources[0].Id];
        SalesSchedApptCancellationUtil.sendApptCancellationEmails(salesAppt);
       	test.stopTest();  
        System.assertNotEquals(null,salesAppt[0].Id);
    }
    static testmethod void testGetAppointmentAll(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
        Test.startTest();
        List<SalesSchedAppointmentCtrl.AppointmentWrapper> aw = null;
        System.runAs(sstu.reps.get(0)){
            aw = SalesSchedAppointmentCtrl.getAppointmentAll(new List<String>{sstu.appointments.get(0).Id});
            SalesSchedAppointmentCtrl.getAvailableCapacities(sstu.appointments.get(0).Id,'');
        }
        Test.stopTest();
        
        System.assertNotEquals(null,aw[0].appointment);
    }
    static testmethod void testClearAppointment(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
        RecordType rt = [Select Id From RecordType Where DeveloperName='Finance_Company'];
        Account finance = new Account(Name='Finance',RecordTypeId=rt.Id);
        insert finance;

        RecordType rt2 = [Select Id From RecordType Where DeveloperName='Cash_Payment' And sObjectType = 'Store_Finance_Program__c'];
        Store_Finance_Program__c sfp = new Store_Finance_Program__c(
			RecordTypeId = rt2.Id,
			Store_Configuration__c = sstu.store.Active_Store_Configuration__c,
			Active__c = true,
			Name = 'TestStoreFinanceProgram',
            Finance_Company__c = finance.Id
		);
        insert sfp;
        
        insert new Store_Finance_Program__share(UserOrGroupId=sstu.reps.get(0).Id,ParentId=sfp.Id,AccessLevel='Read',RowCause=Store_Finance_Program__share.RowCause.Manual);
        
       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
        insert resources;
        
        Test.startTest();
        List<SalesSchedAppointmentCtrl.AppointmentWrapper> aw = null;
        System.runAs(sstu.reps.get(0)){
            Sales_Order__c sorder=new Sales_Order__c(Id=sstu.appointments[0].Sales_Order__c,Time_Arrived__c=DateTime.now().Time(),Time_Left__c=DateTime.now().Time() ,Result__c='Not Covered');
            update sorder;
            //createAppointmentResource(sstu.appointments[0],sstu.capacities[0],sstu.slot);
            SalesSchedAppointmentCtrl.clearResultantDetails(sstu.appointments.get(0).Sales_Order__c);
            //SalesSchedAppointmentCtrl.getAvailableCapacities(sstu.appointments.get(0).Id,'');
        }
        Test.stopTest();
        
        //System.assertNotEquals(null,aw[0].appointment);
    }
    static testmethod void testConfirmAppointment(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
        RecordType rt = [Select Id From RecordType Where DeveloperName='Finance_Company'];
        Account finance = new Account(Name='Finance',RecordTypeId=rt.Id);
        insert finance;

        RecordType rt2 = [Select Id From RecordType Where DeveloperName='Cash_Payment' And sObjectType = 'Store_Finance_Program__c'];
        Store_Finance_Program__c sfp = new Store_Finance_Program__c(
			RecordTypeId = rt2.Id,
			Store_Configuration__c = sstu.store.Active_Store_Configuration__c,
			Active__c = true,
			Name = 'TestStoreFinanceProgram',
            Finance_Company__c = finance.Id
		);
        insert sfp;
        
        insert new Store_Finance_Program__share(UserOrGroupId=sstu.reps.get(0).Id,ParentId=sfp.Id,AccessLevel='Read',RowCause=Store_Finance_Program__share.RowCause.Manual);
        
       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
        insert resources;
        
        Set<Id> capacityIds = new Set<Id>();
        for(Sales_Appointment_Resource__c sar : resources){
            if(sar.Sales_Capacity__c != null){
                capacityIds.add(sar.Sales_Capacity__c);
            }
        }
        
        Map<Id,Sales_Capacity__c> capacities =  SalesSchedUtilsWithoutSharing.getSalesCapacityMap(capacityIds);
        for(Sales_Appointment_Resource__c sar : resources){
            if(sar.Sales_Capacity__c != null){
                sar.Sales_Capacity__r = capacities.get(sar.Sales_Capacity__c);
            }
        }
        Test.startTest();
        List<SalesSchedAppointmentCtrl.AppointmentWrapper> aw = null;
        System.runAs(sstu.reps.get(0)){
            SalesSchedAppointmentCtrl.confirmAppointments(JSON.serialize(resources));
            //SalesSchedAppointmentCtrl.getAvailableCapacities(sstu.appointments.get(0).Id,'');
        }
        Test.stopTest();
        
        //System.assertNotEquals(null,aw[0].appointment);
    }
}