@isTest
public without sharing class StatusCHeckUtilityTEST2 {

    private static final String ACCOUNT_NAME = 'Test Account';
    private static final String MUNICIPALITY_NAME = 'Unit Test Municipality';
    private static final String APPLICATION_NOTES = 'Test Notes';
    private static final String MUNICIPALITY_CONTACT_NAME = 'Unit Test Municipality Contact';
    private static final String RESOURCE1_FIRST_NAME = 'Bert';
    private static final String RESOURCE1_LAST_NAME = 'Knerkerts';
    private static final String SERVICE_TERRITORY_NAME = 'Test Territory';
    private static final String OPP_NAME = 'Test Opportunity';
    private static final String SERVICE_APPT_SUBJECT = 'Test Service Appt';
    private static final Map<String, RecordType> woRecordTypeMap = createWorkOrderRecordTypeMap();

    @TestSetup
    static void testSetup() {
        UtilityMethods.disableAllFslTriggers();
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Store_Configuration__c storeConfig = createStoreConfig();
        insert storeConfig;

        User admin = new User(
            FirstName = 'Test',
            LastName = 'Admin',
            Email = 'test.admin@example.com',
            UserName = 'test.admin@example.com' + (Math.random() * 1000000).intValue(),
            CompanyName = 'ABC Company',
            Alias = 'tuser',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            TimeZoneSidKey = 'America/Chicago',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );

    System.runAs(admin) {
        List<User> userListToInsert = new List<User>();
        User user1 = createUser(RESOURCE1_FIRST_NAME + '1', RESOURCE1_LAST_NAME+ '1');
        userListToInsert.add(user1);

        User user2 = createUser(RESOURCE1_FIRST_NAME + '2', RESOURCE1_LAST_NAME+ '2');
        userListToInsert.add(user2);

        User user3 = createUser(RESOURCE1_FIRST_NAME + '3', RESOURCE1_LAST_NAME+ '3');
        userListToInsert.add(user3);

        User user4 = createUser(RESOURCE1_FIRST_NAME + '4', RESOURCE1_LAST_NAME+ '4');
        userListToInsert.add(user4);
        insert userListToInsert;

        Account acc = TestDataFactory.createStoreAccount(ACCOUNT_NAME);
        insert acc;

        String pricebookId = Test.getStandardPricebookId();

        List<Product2> proListToInsert = new List<Product2>();
        Product2 prod = new Product2();
        prod.Name = 'Test Product 1';
        prod.IsActive = true;
        proListToInsert.add(prod);

        Product2 pro2 = new Product2();
        pro2.Name = 'Test Product 2';
        pro2.IsActive = true;
        proListToInsert.add(pro2);
        insert proListToInsert;

        List<Skill> mySkills = [SELECT Id FROM Skill];

        List<PricebookEntry> peListToInsert = new List<PricebookEntry>();
        PricebookEntry pe = new PricebookEntry();
        pe.IsActive = true;
        pe.Pricebook2Id = pricebookId;
        pe.Product2Id = prod.Id;
        pe.UnitPrice = 45;
        peListToInsert.add(pe);

        PricebookEntry pe2 = new PricebookEntry();
        pe2.IsActive = true;
        pe2.Pricebook2Id = pricebookId;
        pe2.Product2Id = pro2.Id;
        pe2.UnitPrice = 45;
        peListToInsert.add(pe2);
        insert peListToInsert;

        OperatingHours oh = new OperatingHours();
        oh.Name = 'Test Operating Hours';
        insert oh;

        ServiceTerritory st1 = new ServiceTerritory();
        st1.Name = 'Test Service Territory';
        st1.OperatingHoursId = oh.Id;
        st1.IsActive = true;
        insert st1;

        List<ServiceResource> SRListToInsert = new List<ServiceResource>();
        ServiceResource resource1 = createServiceResource(user1, storeConfig);
        SRListToInsert.add(resource1);

        ServiceResource resource2 = createServiceResource(user2, storeConfig);
        SRListToInsert.add(resource2);

        ServiceResource resource3 = createServiceResource(user3, storeConfig);
        SRListToInsert.add(resource3);

        ServiceResource resource4 = createServiceResource(user4, storeConfig);
        SRListToInsert.add(resource4);
        insert SRListToInsert;

        ServiceTerritoryMember stMember = new ServiceTerritoryMember (
            OperatingHoursId = oh.Id,
            ServiceResourceId = resource1.Id,
            ServiceTerritoryId = st1.Id,
            EffectiveStartDate = Date.today().addDays(-1)
        );
        insert stMember;

        Service_Territory_Zip_Code__c serviceZipCode1 = new Service_Territory_Zip_Code__c();
        serviceZipCode1.Name = '53211';
        serviceZipCode1.Service_Territory__c = st1.Id;
        serviceZipCode1.WorkType__c = 'Other';

        Service_Territory_Zip_Code__c serviceZipCode2 = new Service_Territory_Zip_Code__c();
        serviceZipCode2.Name = '53132';
        serviceZipCode2.Service_Territory__c = st1.Id;
        serviceZipCode2.WorkType__c = 'Service';

        Service_Territory_Zip_Code__c serviceZipCode3 = new Service_Territory_Zip_Code__c();
        serviceZipCode3.Name = 'L5L5Y7';
        serviceZipCode3.Service_Territory__c = st1.Id;
        serviceZipCode3.WorkType__c = 'Other';
        insert new List<Service_Territory_Zip_Code__c>{serviceZipCode1,serviceZipCode2,serviceZipCode3};

        WorkType wtInstall = new WorkType();
        wtInstall.Name = 'Install';
        wtInstall.EstimatedDuration = 8.0;
        wtInstall.DurationType = 'Hours';

        WorkType wtMeasure = new WorkType();
        wtMeasure.Name = 'Measure';
        wtMeasure.EstimatedDuration = 2.0;
        wtMeasure.DurationType = 'Hours';

        WorkType wtService = new WorkType();
        wtService.Name = 'Service';
        wtService.EstimatedDuration = 1.0;
        wtService.DurationType = 'Hours';

        WorkType wtJobSiteVisit = new WorkType();
        wtJobSiteVisit.Name = 'Job Site Visit';
        wtJobSiteVisit.EstimatedDuration = 1.0;
        wtJobSiteVisit.DurationType = 'Hours';
        insert new List<WorkType>{wtInstall,wtMeasure,wtService,wtJobSiteVisit};

        Order testOrder =  new Order();
        testOrder.Name ='Sold Order 1';
        testOrder.AccountId = acc.Id;
        testOrder.EffectiveDate = Date.Today();
        testOrder.Status = 'Install Complete';
        testOrder.Pricebook2Id = pricebookId;
        testOrder.Customer_Pickup_All__c = FALSE;
        testOrder.Installation_Date__c = system.today()-1;
        insert testOrder;

        List<OrderItem> itemsToInsert = new List<OrderItem>();
        OrderItem item = new OrderItem();
        item.PricebookEntryId = pe.Id;
        item.OrderId = testOrder.Id;
        item.Product2Id = prod.Id;
        item.Quantity = 2;
        item.UnitPrice = 45;
        itemsToInsert.add(item);

        OrderItem item2 = new OrderItem();
        item2.PricebookEntryId = pe2.Id;
        item2.OrderId = testOrder.Id;
        item2.Product2Id = pro2.Id;
        item2.Quantity = 2;
        item2.UnitPrice = 45;
        itemsToInsert.add(item2);
        
        RbA_Skills__c mySkill = new RbA_Skills__c();
        myskill.Install_Duration__c = 5;
        mySkill.Tech_Measure_Duration__c = 5;
        insert myskill;        

        List<Product_Skill__c> PClistToInsert = new List<Product_Skill__c>();
        Product_Skill__c ps = new Product_Skill__c();
        ps.Product__c = prod.Id;
        ps.FSL_Skill_ID__c = mySkills[0].Id;
        ps.RBA_Skill__c = mySkill.id;
        PClistToInsert.add(ps);

        Product_Skill__c ps2 = new Product_Skill__c();
        ps2.Product__c = pro2.Id;
        ps2.FSL_Skill_ID__c = mySkills[0].Id;
        ps2.RBA_Skill__c = mySkill.id;
        PClistToInsert.add(ps2);

        Product_Skill__c ps3 = new Product_Skill__c();
        ps3.Product__c = pro2.Id;
        ps3.FSL_Skill_ID__c = mySkills[0].Id;
        ps3.RBA_Skill__c = mySkill.id;
        PClistToInsert.add(ps3);

        insert PClistToInsert;

        Municipality__c m1 = new Municipality__c();
        m1.Name = MUNICIPALITY_NAME;
        m1.Application_Notes__c = APPLICATION_NOTES;
        insert m1;

        Municipality_Contact__c mc1 = new Municipality_Contact__c();
        mc1.Name = MUNICIPALITY_CONTACT_NAME;
        mc1.Active__c = true;
        mc1.Municipality__c = m1.Id;
        insert mc1;

        List<WorkOrder> WorkOrdersToInsert = new List<WorkOrder>();
        WorkOrder installWorkOrder = new WorkOrder();
        installWorkOrder.Sold_Order__c = testOrder.Id;
        installWorkOrder.AccountId = testOrder.AccountId;
        installWorkOrder.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Install').getRecordTypeId();
        installWorkOrder.Duration = 1.0;
        installWorkOrder.DurationType ='Hours';
        installWorkOrder.Status = 'To be Scheduled';
        WorkOrdersToInsert.add(installWorkOrder);

        WorkOrder serviceWorkOrder = new WorkOrder();
        serviceWorkOrder.Sold_Order__c = testOrder.Id;
        serviceWorkOrder.AccountId = testOrder.AccountId;
        serviceWorkOrder.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Service').getRecordTypeId();
        serviceWorkOrder.Duration = 1.0;
        serviceWorkOrder.DurationType ='Hours';
        serviceWorkOrder.Status = 'To be Scheduled';
        WorkOrdersToInsert.add(serviceWorkOrder);

        WorkOrder techMeasureWorkOrder = new WorkOrder();
        techMeasureWorkOrder.Sold_Order__c = testOrder.Id;
        techMeasureWorkOrder.AccountId = testOrder.AccountId;
        techMeasureWorkOrder.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Tech_Measure').getRecordTypeId();
        techMeasureWorkOrder.Duration = 1.0;
        techMeasureWorkOrder.DurationType ='Hours';
        techMeasureWorkOrder.Status = 'To be Scheduled';
        WorkOrdersToInsert.add(techMeasureWorkOrder);

        WorkOrder jobSiteVisitWorkOrder = new WorkOrder();
        jobSiteVisitWorkOrder.Sold_Order__c = testOrder.Id;
        jobSiteVisitWorkOrder.AccountId = testOrder.AccountId;
        jobSiteVisitWorkOrder.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Job_Site_Visit').getRecordTypeId();
        jobSiteVisitWorkOrder.Duration = 1.0;
        jobSiteVisitWorkOrder.DurationType ='Hours';
        jobSiteVisitWorkOrder.Status = 'To be Scheduled';
        WorkOrdersToInsert.add(jobSiteVisitWorkOrder);

        insert WorkOrdersToInsert;

        List<ServiceAppointment> SAsToInsert = new List<ServiceAppointment>();
        ServiceAppointment sap = TestDataFactoryStatic.createFSLServiceAppointment(installWorkOrder.Id);
        SAsToInsert.add(sap);

        ServiceAppointment sap5 = TestDataFactoryStatic.createFSLServiceAppointment(installWorkOrder.Id);
        sap5.Status = 'Completed';
        SAsToInsert.add(sap5);

        ServiceAppointment sap3 = TestDataFactoryStatic.createFSLServiceAppointment(installWorkOrder.Id);
        sap3.Status = 'None';
        SAsToInsert.add(sap3);

        ServiceAppointment sap4 = TestDataFactoryStatic.createFSLServiceAppointment(installWorkOrder.Id);
        sap4.Status = 'Canceled';
        SAsToInsert.add(sap4);

        insert SAsToInsert;

        }
    }
    @isTest
    public static void InstallAppointmentToWorkOrderStatusCheckTEST1() {
        UtilityMethods.disableAllFslTriggers();
        
        WorkOrder wo = [SELECT Id, Sold_Order__c, Status, Work_Order_Type__c,Opportunity__c FROM WorkOrder WHERE Work_Order_Type__c = 'Install' LIMIT 1];
        ServiceAppointment sap2 = TestDataFactoryStatic.createFSLServiceAppointment(wo.Id);
        sap2.Status = 'Completed';
        insert sap2;
        ServiceAppointment sa = [SELECT Id, Status, Work_Order_Type__c, ParentRecordId, Primary_Service_Resource__c
                                    FROM ServiceAppointment 
                                    WHERE Status = 'Completed' 
                                    AND ParentRecordId != null 
                                    // AND Primary_Service_Resource__c != null
                                    LIMIT 1];
        System.debug(sa);
        map<Id, ServiceAppointment> saMap = new map<Id, ServiceAppointment>();
        saMap.put(sa.Id, sa);

        System.enqueueJob(new StatusCheckQue(saMap, null, 'InstallAppointment'));
        
        StatusCheckUtility.ServiceTypeAppointmentToWorkOrderStatusCheck(saMap, null);
        StatusCheckUtility.JobSiteTypeAppointmentToWorkOrderStatusCheck(saMap, null);
        // System.enqueueJob(new RollBackStatusQue(woMap, null, 'WorkOrder'));
 }

    @isTest
    public static void InstallAppointmentToWorkOrderStatusCheckTEST2() {
        UtilityMethods.disableAllFslTriggers();
        WorkOrder wo = [SELECT Id, Sold_Order__c, Status, Work_Order_Type__c,Opportunity__c FROM WorkOrder WHERE Work_Order_Type__c = 'Install' LIMIT 1];
        ServiceAppointment sap2 = TestDataFactoryStatic.createFSLServiceAppointment(wo.Id);
        sap2.Status = 'None';
        insert sap2;
        ServiceAppointment sa = [SELECT Id, Status, Work_Order_Type__c, ParentRecordId, Primary_Service_Resource__c
        FROM ServiceAppointment 
        WHERE Status = 'None' 
        AND ParentRecordId != null 
        // AND Primary_Service_Resource__c != null
        LIMIT 1];
        System.debug(sa);
        // sa.Status = 'None';
        
        // update sa;
        
        map<Id, ServiceAppointment> saMap = new map<Id, ServiceAppointment>();
        saMap.put(sa.Id, sa);
        System.enqueueJob(new StatusCheckQue(saMap, null, 'InstallAppointment'));
        StatusCheckUtility.ServiceTypeAppointmentToWorkOrderStatusCheck(saMap, null);
        StatusCheckUtility.JobSiteTypeAppointmentToWorkOrderStatusCheck(saMap, null);
    }

    @isTest
    public static void InstallAppointmentToWorkOrderStatusCheckTEST3() {
        UtilityMethods.disableAllFslTriggers();
        WorkOrder wo = [SELECT Id, Sold_Order__c, Status, Work_Order_Type__c,Opportunity__c FROM WorkOrder WHERE Work_Order_Type__c = 'Install' LIMIT 1];
        ServiceAppointment sap2 = TestDataFactoryStatic.createFSLServiceAppointment(wo.Id);
        sap2.Status = 'Scheduled';
        insert sap2;
        ServiceAppointment sa = [SELECT Id, Status, Work_Order_Type__c, ParentRecordId, Primary_Service_Resource__c
        FROM ServiceAppointment 
        WHERE Status = 'Scheduled' 
        AND ParentRecordId != null 
        // AND Primary_Service_Resource__c != null
        LIMIT 1];
        System.debug(sa);
        // sa.Status = 'Scheduled';
        
        // update sa;
        
        map<Id, ServiceAppointment> saMap = new map<Id, ServiceAppointment>();
        saMap.put(sa.Id, sa);
        System.enqueueJob(new StatusCheckQue(saMap, null, 'InstallAppointment'));
        StatusCheckUtility.ServiceTypeAppointmentToWorkOrderStatusCheck(saMap, null);
        StatusCheckUtility.JobSiteTypeAppointmentToWorkOrderStatusCheck(saMap, null);
    }

    @isTest
    public static void InstallAppointmentToWorkOrderStatusCheckTEST4() {
        UtilityMethods.disableAllFslTriggers();
        WorkOrder wo = [SELECT Id, Sold_Order__c, Status, Work_Order_Type__c,Opportunity__c FROM WorkOrder WHERE Work_Order_Type__c = 'Install' LIMIT 1];
        ServiceAppointment sap2 = TestDataFactoryStatic.createFSLServiceAppointment(wo.Id);
        sap2.Status = 'Canceled';
        insert sap2;
        ServiceAppointment sa = [SELECT Id, Status, Work_Order_Type__c, ParentRecordId, Primary_Service_Resource__c
        FROM ServiceAppointment 
        WHERE Status = 'Canceled' 
        AND ParentRecordId != null 
        // AND Primary_Service_Resource__c != null
        LIMIT 1];
        System.debug(sa);
        // sa.Status = 'Canceled';
        
        // update sa;
        
        map<Id, ServiceAppointment> saMap = new map<Id, ServiceAppointment>();
        saMap.put(sa.Id, sa);
        System.enqueueJob(new StatusCheckQue(saMap, null, 'InstallAppointment'));
        StatusCheckUtility.ServiceTypeAppointmentToWorkOrderStatusCheck(saMap, null);
        StatusCheckUtility.JobSiteTypeAppointmentToWorkOrderStatusCheck(saMap, null);
    }

    /**************************
    *      SETUP METHODS
    **************************/

    private static User createUser(String firstName, String lastName) {
        User u = new User(
            FirstName = firstName,
            LastName = lastName,
            Email = firstName + '.' + lastName + '@example.com',
            UserName = firstName + '.' + lastName + '@example.com' + (Math.random() * 1000000).intValue(),
            CompanyName = 'ABC Company',
            Alias = 'tuser',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            TimeZoneSidKey = 'America/Chicago',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        return u;
    }

    private static Store_Configuration__c createStoreConfig() {
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;

        Store_Configuration__c storeConfig = new Store_Configuration__c(
            Store__c = store.Id,
            Order_Number__c = 1
        );
        return storeConfig;
    }

    private static ServiceResource createServiceResource(User u, Store_Configuration__c storeConfig) {
        ServiceResource sr = new ServiceResource(
            Name = u.LastName,
            RelatedRecordId = u.Id,
            Retail_Location__c = storeConfig.Id,
            IsActive = true
        );
        return sr;
    }

    private static Map<String, RecordType> createWorkOrderRecordTypeMap() {
        List<RecordType> woRecordTypes = [select Id, DeveloperName from RecordType where sObjectType = 'WorkOrder'];
        Map<String, RecordType> woRecordTypeMap = new Map<String, RecordType>();
        for (RecordType rt : woRecordTypes) {
            woRecordTypeMap.put(rt.DeveloperName, rt);
        }
        return woRecordTypeMap;
    }
}