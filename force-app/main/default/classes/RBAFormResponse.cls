public class RBAFormResponse {
    @AuraEnabled public String status{get;set;}
	@AuraEnabled public String data{get;set;}
	@AuraEnabled public String message{get;set;} 
	
	public static RBAFormResponse parse(String json) {
		return (RBAFormResponse) System.JSON.deserialize(json, RBAFormResponse.class);
	}
}