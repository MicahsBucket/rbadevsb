@isTest(SeeAllData=true)
private class CanvassUnitTriggerTest {
	
	@isTest static void testCanvassUnitTrigger() {

		Test.startTest();

		CNVSS_Canvass_Market__c mm = new CNVSS_Canvass_Market__c(
			name = 'Hawaii Test Market',
			CNVSS_ISC_Phone_Number__c = '5555555555'
			);
		insert mm;
		CNVSS_Canvass_Market__c m2 = new CNVSS_Canvass_Market__c(
			name = 'Another Test Market',
			CNVSS_ISC_Phone_Number__c = '5555555555'
			);
		insert m2;
	
		List<CNVSS_Canvass_Market__c> market = [select ID,name from CNVSS_Canvass_Market__c];
		
		CNVSS_Canvass_Unit__c cnvu = new CNVSS_Canvass_Unit__c(
			CNVSS_Canvass_Market__c = market[0].id,
			Market__c = market[0].name,
			CNVSS_PK_Full_Address__c = '124 main street, Chicago, IL 60565',
			CNVSS_House_No__c = '123',
			CNVSS_Street__c ='Main',
			CNVSS_City__c ='Chicago',
			CNVSS_State__c = 'IL',
			CNVSS_Zip_Code__c = '60565'
			);
		insert cnvu;

		cnvu.CNVSS_Canvass_Market__c = market[1].id;
		cnvu.Market__c = market[1].name;
		update cnvu;

		cnvu.CNVSS_Canvass_Market__c = market[0].id;
		cnvu.Market__c = market[0].name;
		update cnvu;

		Test.stopTest();
	}
	
}