/**
 * @File Name          : SalesSchedAppointmentManagerCtrl.cls
 * @Description        :
 * @Author             : Demand Chain (James Loghry)
 * @Group              :
 * @Last Modified By   : Connor.Davis@andersencorp.com
 * @Last Modified On   : 03-08-2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      	     Modification
 *==============================================================================
 * 1.0    2/4/2019, 11:43:46 PM   Demand Chain (James Loghry)     Initial Version
**/
public with sharing class SalesSchedAppointmentManagerCtrl { 

    public static final Set<String> STATUSES_TO_DISPLAY = new Set<String>{'Accepted', 'Confirmed', 'Checked In', 'Checked Out', 'Resulted'};

    @AuraEnabled(cacheable=true)
    public static MyAppointmentWrapper getMyAppointments(String day){
        boolean defaultToNonresultedDate = String.isEmpty(day);

        MyAppointmentWrapper maw = new MyAppointmentWrapper();
        maw.currentDate = SalesSchedUtils.getCurrentDate(day, false);
        maw.day = ((Datetime)maw.currentDate).formatGmt('yyyy-MM-dd');
		List<String> rideAlongResourceStatus=new List<String>{'Accepted','Confirmed'};
        List<Sales_Appointment_Resource__c> nonResultedAppointments =
            [Select
                Id,
                Sales_Appointment__r.Appointment_Date__c,
                Status__c
             From
                Sales_Appointment_Resource__c
             Where
                Sales_Capacity__r.User__c = :UserInfo.getUserId()
                And Sales_Appointment__r.Appointment_Date__c < :maw.currentDate
             	And ((Primary__c = false AND Status__c in :rideAlongResourceStatus) OR (Primary__c = true AND Sales_Appointment__r.Sales_Order__r.Resulted__c = false))
                //And Sales_Appointment__r.Sales_Order__r.Resulted__c = false
                And Sales_Appointment__r.Cancelled__c = false
             Order By
                Sales_Appointment__r.Appointment_Date__c Asc
             Limit 1];

        maw.showResultedAppointments = nonResultedAppointments.isEmpty();
        Boolean isUserNotResultant=false;
        if(!nonResultedAppointments.isEmpty() && defaultToNonresultedDate){
            Sales_Appointment_Resource__c res = nonResultedAppointments.get(0);
            maw.currentDate = res.Sales_Appointment__r.Appointment_Date__c;
            maw.day = ((Datetime)maw.currentDate).formatGmt('yyyy-MM-dd');
            maw.displayNotResultedWarning = true;
            
            //Set this boolean to true to hide resulted details.
            maw.showResultedAppointments = true;

            Set<String> assignedStatuses = new Set<String>{'Manually Reassigned','Assigned','Manually Assigned'};
            if(assignedStatuses.contains(res.Status__c)){
                maw.displayNotConfirmedWarning = true;
            }
        }

        User me =
            [Select
                Manager.Name,
                Manager.Phone,
                Manager.MobilePhone,
                Manager.Email,
                Manager.SmallPhotoUrl
             From
                User
             Where
                Id = :UserInfo.getUserId()];
        maw.manager = me.Manager;

        Set<Id> salesAppointmentIds = new Set<Id>();
        for(Sales_Appointment_Resource__c resource :
            [Select
                Sales_Appointment__c
             From
                Sales_Appointment_Resource__c
             Where
                Sales_Appointment__r.Appointment_Date__c = :maw.currentDate
                And Sales_Capacity__r.User__c =:me.Id]){
            salesAppointmentIds.add(resource.Sales_Appointment__c);
        }

        //if(maw.showResultedAppointments){
            getAppointmentWrapperResulted(salesAppointmentIds, maw);
        //}else{
        //    getAppointmentWrapperNotResulted(salesAppointmentIds, maw);
        //}

        /*Map<Id,List<Store_Finance_Program__c>> configToProgramMap = new Map<Id,List<Store_Finance_Program__c>>();
        for(AppointmentWrapper aw : maw.appointments){
            configToProgramMap.put(
                aw.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__c,
                new List<Store_Finance_Program__c>()
            );
        }

        for(Store_Finance_Program__c sfp :
            [Select
                Id,
                Name,
                Store_Configuration__c
             From
                Store_Finance_Program__c
             Where
                Active__c = true
                And Store_Configuration__c in :configToProgramMap.keySet()
                And Finance_Company__c != null
             Order By
                Name Asc]){
            configToProgramMap.get(sfp.Store_Configuration__c).add(sfp);
        }

        for(AppointmentWrapper aw : maw.appointments){
            List<Store_Finance_Program__c> programs = configToProgramMap.get(aw.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__c);
            aw.programs = programs == null ? new List<Store_Finance_Program__c>() : programs;
        }*/

        List<Sales_Capacity__c> capacities =
            [Select
                Slot__r.Name,
                Slot__r.Start__c,
                Slot__r.End__c,
                Status__c,
                (Select Id From Sales_Appointment_Resources__r Limit 1)
             From
                Sales_Capacity__c
             Where
                User__c = :me.Id
                And Status__c = 'Available'
                And Date__c = :maw.currentDate];

        for(Sales_Capacity__c capacity : capacities){
            if(capacity.Sales_Appointment_Resources__r.isEmpty()){
                maw.unassigned.add(capacity);
            }
            maw.available.add(capacity);
        }

        //SS19-61
        //maw.showResultedAppointments = true;

        return maw;
    }

    private static void getAppointmentWrapperResulted(Set<Id> salesAppointmentIds, MyAppointmentWrapper maw){
        Map<Id,List<Sales_Appointment__c>> oppIdToAppointmentIds = new Map<Id,List<Sales_Appointment__c>>();
        Map<Id,AppointmentWrapper> wrapperMap = new Map<Id,AppointmentWrapper>();
        List<Sales_Appointment__c> salesAppointments =
            [Select
                Id,
                Appointment_Date_Time__c,
                Date__c,
                Sales_Order__c,
                Sales_Order__r.Opportunity__c,
                Sales_Order__r.Opportunity__r.Account.Name,
                Sales_Order__r.Opportunity__r.Account.BillingLongitude,
                Sales_Order__r.Opportunity__r.Account.BillingLatitude,
                Sales_Order__r.Opportunity__r.Name,
             	Sales_Order__r.Opportunity__r.Appointment_Type__c,
             	Sales_Order__r.Key_Contact_Person__c,
                Sales_Order__r.Result__c,
                Sales_Order__r.Resulted__c,
                Sales_Order__r.Payment_Type__c,
                Sales_Order__r.Finance_Issues__c,
                Sales_Order__r.Rescheduled__c,
                Sales_Order__r.Time_Arrived__c,
                Sales_Order__r.Time_Left__c,
             	Sales_Order__r.leftTime__c,
             	Sales_Order__r.InTime__c,
             	Sales_Order__r.Check_out_Time__c,
             	Sales_Order__r.Check_In_Time__c,
             	Sales_Order__r.Why_Not_Covered__c,
             	Sales_Order__r.Comments__c,
				Sales_Order__r.Project_Description__c,
             	Sales_Order__r.Why_No_Demo__c,
             	Sales_Order__r.Follow_Up_Appt_Date_Time__c,
             	Sales_Order__r.Mail_and_Canvass_Neighborhood__c,             
                Sales_Order__r.Series_One_Windows__c, 
                Sales_Order__r.Series_Two_Windows__c,
                Sales_Order__r.Patio_Doors__c,
             	Sales_Order__r.Entry_Doors__c,
             	Sales_Order__r.Amount_If_Sold__c,
             	Sales_Order__r.Series_One_Windows_Quoted__c,
             	Sales_Order__r.Series_Two_Windows_Quoted__c,
                Sales_Order__r.Includes_Series_A_Window__c,
             	Sales_Order__r.Patio_Doors_Quoted__c,
             	Sales_Order__r.Entry_Doors_Quoted__c,
             	Sales_Order__r.Contract_Amount_Quoted__c,
             	Sales_Order__r.BAY_Windows_Quoted__c,
             	Sales_Order__r.BOW_Windows_Quoted__c,             
             	Sales_order__r.Were_All_Owners_Present__c,
                Sales_order__r.Key_Reason_Customer_Didnt_Proceed__c,
                Sales_order__r.Future_Purchase__c,
             	Sales_order__r.Deposit_Type__c,
             	Sales_order__r.Deposit__c,             
             	Sales_order__r.Construction_Department_Notes__c,
             	Sales_order__r.Why_Customer_Purchased__c,
             	Sales_order__r.Concerns__c,
             	Sales_order__r.Address_Concerns__c,
             	Sales_order__r.Partial_Sale__c,
             	Sales_order__r.Historical_Area__c,
             	Sales_order__r.Account_Number__c,
             	Sales_order__r.Approved__c,
             	Sales_order__r.Next_For_Renewal__c,
                Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__c,
                Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_2__c,
                Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_A__c,
             	Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Entry_Doors__c,
                Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Reps_can_schedule_follow_ups__c,
                Sales_Order__r.Store_Finance_Program__r.Id,
                Sales_Order__r.Store_Finance_Program__r.Name,
                Sales_Order__r.Store_Finance_Program__r.Finance_Company__r.Id,
                Sales_Order__r.Store_Finance_Program__r.Finance_Company__r.Name,
                Sales_Order__r.Turned_Down_By__r.Id,
                Sales_Order__r.Turned_Down_By__r.Name,
                (Select Appointment_Date_Time__c From Follow_Ups__r Where Cancelled__c = false Order By Appointment_Date_Time__c Asc),
                (Select
                    Sales_Capacity__c,
                    Finalized_By__r.Name,
                    Primary__c,
                    Status__c
                 From
                    Sales_Appointment_Resources__r
                 Where
                    Sales_Capacity__c != null
                 Order By
                    Primary__c Desc)
            From
                Sales_Appointment__c
            Where
                Id in :salesAppointmentIds
                And Cancelled__c = false
            Order By
                Appointment_Date_Time__c Asc];

        Set<Id> capacityIds = new Set<Id>();
        for(Sales_Appointment__c sa : salesAppointments){
            for(Sales_Appointment_Resource__c sar : sa.Sales_Appointment_Resources__r){
                capacityIds.add(sar.Sales_Capacity__c);
            }
        }

        Map<Id,Sales_Capacity__c> capacityMap = SalesSchedUtilsWithoutSharing.getSalesCapacityMap(capacityIds);

        for(Sales_Appointment__c sa : salesAppointments){
            AppointmentWrapper aw = new AppointmentWrapper(sa);

            for(Sales_Appointment_Resource__c resource : sa.Sales_Appointment_Resources__r){
                Sales_Capacity__c capacity = capacityMap.get(resource.Sales_Capacity__c);
                resource.Sales_Capacity__r = capacity;

                aw.hasRideAlongs |= (!resource.Primary__c);

                if(capacity.User__c == UserInfo.getUserId() && STATUSES_TO_DISPLAY.contains(resource.Status__c)){
                    aw.isSecondary = !resource.Primary__c;

                    //The next few lines are for adding the primary contact info to the appointment detail card in the lightning component.
                    List<Sales_Appointment__c> appts = oppIdToAppointmentIds.get(sa.Sales_Order__r.Opportunity__r.Id);
                    if(appts == null){
                        appts = new List<Sales_Appointment__c>();
                        oppIdToAppointmentIds.put(sa.Sales_Order__r.Opportunity__r.Id, appts);
                    }
                    appts.add(sa);

                    aw.resource = resource;
                    wrapperMap.put(sa.Id,aw);
                    maw.appointments.add(aw);
                }
            }
        }

        for(Opportunity o :
            [Select
                Id,
                (Select
                    Contact.Name,
                 	Contact.LastName,
                    Contact.HomePhone,
                    Contact.MobilePhone,
                 	Contact.Email,
                    IsPrimary
                 From
                    OpportunityContactRoles
                 Order By
                    IsPrimary Desc,
                    Contact.Name Asc)
             From
                Opportunity
             Where
                Id in :oppIdToAppointmentIds.keySet()]){
            if(!o.OpportunityContactRoles.isEmpty()){
                for(Sales_Appointment__c appt : oppIdToAppointmentIds.get(o.Id)){
                    AppointmentWrapper wrapper = wrapperMap.get(appt.Id);
                    for(OpportunityContactRole ocr : o.OpportunityContactRoles){
                        if(ocr.IsPrimary){
                            wrapper.primaryContact = ocr.Contact;
                        }else{
                            wrapper.secondaryContacts.add(ocr.Contact);
                        }
                    }
                }
            }
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<SalesSchedUtils.Option> getFinanceCompanies(Id configId,String searchText){
        System.debug('JWL: configId: ' + configId);
        System.debug('JWL: searchText: [' + searchText + ']');

        if(configId == null){
            return new List<SalesSchedUtils.Option>();
        }

        List<SalesSchedUtils.Option> options = new List<SalesSchedUtils.Option>();
        for(AggregateResult ar :
            [Select
                Finance_Company__r.Name name,
                Finance_Company__c id
             From
                Store_Finance_Program__c
             Where
                Active__c = true
                And Finance_Company__c != null
                And Store_Configuration__c = :configId
             Group By
                Finance_Company__r.Name,
                Finance_Company__c
             Order By
                Finance_Company__r.Name Asc
            ]){
            String company = (String)ar.get('name');
            Id companyId = (Id)ar.get('id');
            if(String.isEmpty(searchText) || company.containsIgnoreCase(searchText)){
                options.add(new SalesSchedUtils.Option(companyId,company));
            }
        }
        return options;
    }

    @AuraEnabled(cacheable=true)
    public static List<SalesSchedUtils.Option> getFinancePlans(Id configId, Id companyId, String searchText){
        if(companyId == null || configId == null){
            return new List<SalesSchedUtils.Option>();
        }

        List<SalesSchedUtils.Option> options = new List<SalesSchedUtils.Option>();
        for(Store_Finance_Program__c sfp :
            [Select
                Id,
                Name
             From
                Store_Finance_Program__c
             Where
                Active__c = true
                And Finance_Company__c = :companyId
                And Store_Configuration__c = :configId
             Order By
                Name Asc
            ]){
            if(String.isEmpty(searchText) || sfp.Name.containsIgnoreCase(searchText)){
                options.add(new SalesSchedUtils.Option(sfp.Id,sfp.Name));
            }
        }
        return options;
    }
	@TestVisible
    private static void getAppointmentWrapperNotResulted(Set<Id> salesAppointmentIds, MyAppointmentWrapper maw){
        List<Sales_Appointment__c> salesAppointments =
            [Select
                Id,
                Appointment_Date_Time__c,
                (Select Appointment_Date_Time__c From Follow_Ups__r Where Cancelled__c = false),
                (Select
                    Sales_Capacity__c,
                    Finalized_By__r.Name,
                    Primary__c,
                    Status__c
                 From
                    Sales_Appointment_Resources__r
                 Order By
                    Primary__c Desc)
            From
                Sales_Appointment__c
            Where
                Id in :salesAppointmentIds
                And Cancelled__c = false
            Order By
                Appointment_Date_Time__c Asc];

        Set<Id> capacityIds = new Set<Id>();
        for(Sales_Appointment__c sa : salesAppointments){
            for(Sales_Appointment_Resource__c sar : sa.Sales_Appointment_Resources__r){
                capacityIds.add(sar.Sales_Capacity__c);
            }
        }

        Map<Id,Sales_Capacity__c> capacityMap = SalesSchedUtilsWithoutSharing.getSalesCapacityMap(capacityIds);

        for(Sales_Appointment__c sa : salesAppointments){
            AppointmentWrapper aw = new AppointmentWrapper(sa);

            for(Sales_Appointment_Resource__c resource : sa.Sales_Appointment_Resources__r){
                Sales_Capacity__c capacity = capacityMap.get(resource.Sales_Capacity__c);
                resource.Sales_Capacity__r = capacity;

                if(capacity.User__c == UserInfo.getUserId() && STATUSES_TO_DISPLAY.contains(resource.Status__c)){
                    aw.resource = resource;
                    maw.appointments.add(aw);
                }
            }
        }
    }

    public class MyAppointmentWrapper{
        @AuraEnabled public Date currentDate {get; set;}
        @AuraEnabled public String day {get; set;}
        @AuraEnabled public List<AppointmentWrapper> appointments {get; set;}
        @AuraEnabled public List<Sales_Capacity__c> unassigned {get; set;}
        @AuraEnabled public List<Sales_Capacity__c> available {get; set;}
        @AuraEnabled public User manager {get; set;}
        @AuraEnabled public boolean showResultedAppointments {get; set;}
        @AuraEnabled public boolean allowRepAppointments {get; set;}
        @AuraEnabled public boolean displayNotResultedWarning {get; set;}
        @AuraEnabled public boolean displayNotConfirmedWarning {get; set;}

        public MyAppointmentWrapper(){
            this.appointments = new List<AppointmentWrapper>();
            this.unassigned = new List<Sales_Capacity__c>();
            this.available = new List<Sales_Capacity__c>();
            this.showResultedAppointments = false;
            this.allowRepAppointments = false;
            this.displayNotResultedWarning = false;
            this.displayNotConfirmedWarning = false;
        }
    }

    public class AppointmentWrapper{
        @AuraEnabled public Sales_Appointment__c appointment {get; set;}
        @AuraEnabled public Sales_Appointment_Resource__c resource {get; set;}
        @AuraEnabled public Contact primaryContact {get; set;}
        @AuraEnabled public List<Contact> secondaryContacts {get; set;}
        @AuraEnabled public boolean isSecondary {get; set;}
        @AuraEnabled public boolean hasRideAlongs {get; set;}
        @AuraEnabled public String formattedTime {get; set;}
        
        public AppointmentWrapper(Sales_Appointment__c appointment){
            this.appointment = appointment;
            this.hasRideAlongs = false;
            this.isSecondary = false;
            this.secondaryContacts = new List<Contact>();

            this.formattedTime = '';
            if(appointment.Appointment_Date_Time__c != null){
                this.formattedTime = appointment.Appointment_Date_Time__c.format('h:mm a');
            }
        }
    }
}