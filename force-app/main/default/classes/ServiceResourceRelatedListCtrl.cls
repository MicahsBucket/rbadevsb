public class ServiceResourceRelatedListCtrl {
    
    @AuraEnabled(cacheable=true)
    public static List<ServiceResourceSkillWrapper> getServiceResourceSkill(String SerResourceId){
        List<ServiceResourceSkillWrapper> ServiceResourceSkillWrapList = new List<ServiceResourceSkillWrapper>();
        for(ServiceResourceSkill serResourceSkill: [SELECT id, SkillLevel, SkillNumber,
                                                    	   EffectiveStartDate, EffectiveEndDate, Skill.MasterLabel
                                                    FROM ServiceResourceSkill 
                                                    WHERE ServiceResourceId = :SerResourceId])
        {
            ServiceResourceSkillWrapper ServiceResourceSkillWrap = new ServiceResourceSkillWrapper(serResourceSkill);
            ServiceResourceSkillWrapList.add(ServiceResourceSkillWrap);
        }
        return ServiceResourceSkillWrapList;
    }

    @AuraEnabled(cacheable=true)
    public static List<ServiceTerritoryWrapper> getServiceTerritoryLists(String SerResourceId){
        List<ServiceTerritoryWrapper> ServiceTerritoryWrapList = new List<ServiceTerritoryWrapper>();
        for(ServiceTerritoryMember SerTerritoryMem: [SELECT id,MemberNumber,ServiceTerritory.Name,TerritoryType,
                                                            EffectiveStartDate,EffectiveEndDate,ServiceTerritoryId 
                                                     FROM ServiceTerritoryMember 
                                                     WHERE ServiceResourceId = :SerResourceId])
        {
            ServiceTerritoryWrapper ServiceTerritoryWrap = new ServiceTerritoryWrapper(SerTerritoryMem);
            ServiceTerritoryWrapList.add(ServiceTerritoryWrap);  
        }
        return ServiceTerritoryWrapList;

    }
    
    public class ServiceResourceSkillWrapper{
        @AuraEnabled
        public String name{get;set;}
        @AuraEnabled
        public Double level{get;set;}
        @AuraEnabled
        public Datetime startDate{get;set;}
        @AuraEnabled
        public Datetime endDate{get;set;}   
        
        public ServiceResourceSkillWrapper(ServiceResourceSkill serRes){
            name = serRes.Skill.MasterLabel;
            level= serRes.SkillLevel;
            startDate = serRes.EffectiveStartDate;
            endDate = serRes.EffectiveEndDate;
        }
    }

    public class ServiceTerritoryWrapper{
        @AuraEnabled
        public string memberNumber{get;set;}
        @AuraEnabled
        public String serviceTerritoryName{get;set;}
        @AuraEnabled
        public String territoryType{get;set;}
        @AuraEnabled
        public Datetime effectiveStartDate{get;set;}
        @AuraEnabled
        public Datetime effectiveEndDate{get;set;}
        @AuraEnabled
        public String serviceTerritoryUrl{get;set;}
        @AuraEnabled
        public String memberUrl{get;set;}   
        
        public ServiceTerritoryWrapper(ServiceTerritoryMember serTer){
            memberNumber = serTer.MemberNumber;
            memberUrl = '/'+serTer.Id;
            serviceTerritoryUrl = '/'+serTer.ServiceTerritoryId;
            serviceTerritoryName = serTer.ServiceTerritory.Name;
            territoryType = serTer.TerritoryType;
            if(serTer.TerritoryType == 'P'){
                territoryType = 'Primary';
            }
            else if(serTer.TerritoryType == 'S'){
                territoryType = 'Secondary'; 
            }
            else if(serTer.TerritoryType == 'R'){
                territoryType = 'Relocation';
            }
            effectiveStartDate = serTer.EffectiveStartDate;
            effectiveEndDate = serTer.EffectiveEndDate;
        }
    }

}