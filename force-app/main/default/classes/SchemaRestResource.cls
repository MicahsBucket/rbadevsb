/**
* @author Bill Hodena
* @description REST resource for delivering simplified information about the Salesforce API and object schema
*/ 
@RestResource(urlMapping='/api/v1/schema/*')
global class SchemaRestResource  {

    /**
    * @description method returns a list of DataObject each containing a description related to the given object list and its child piclist and multi-picklist fields and values.
    * @return List<DataObject>
    */ 
	@HttpGet
    Global static List<DataObject> getPicklists(){

    	RestRequest req = RestContext.request;
        List<String> types = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1).split(',');
    	List<DataObject> returnResults = new List<DataObject>();

        try{

            // If no objects were sent, then retrieve the list of default objects from custom metadata
            system.debug('*** Size = ' + types.size());

            if((types.size() == 0) || String.isEmpty(types[0].trim())){
                rSuite_Integration_Objects__mdt defaults  = [SELECT Object_List__c FROM rSuite_Integration_Objects__mdt WHERE DeveloperName = 'default' LIMIT 1];
                types = defaults.Object_List__c.split(',');
            } 

            // Get the details for each object passed into the service  
    		Schema.DescribeSobjectResult[] describeResults = Schema.describeSObjects(types); // Get the object descriptions
    		Integer count; // Adds an "order" index value to the output as the data is returned in the order that it's listed in SF

    		// Iterate through the object descriptions that we collected.
    		for(Schema.DescribeSobjectResult describeResult : describeResults) {
    			
    			DataObject objectResult = new DataObject();
    			objectResult.ObjectName = describeResult.getName();
                objectResult.DataFields = new List<DataField>();

    			Map<String, Schema.SObjectField> fieldMap = describeResult.fields.getMap(); // Map containing all fields within the object
    	
    			// Iterate through the field names
    			for (String fieldName : fieldMap.keySet()){

    				// Get a reference to the current field
    				SObjectField currentField = fieldMap.get(fieldName);
                   
    				// If the field is a picklist or multi-picklist, get all of it's name/value pairs and add a counter
                    if((currentField.getDescribe().getType() == Schema.DisplayType.PICKLIST) || (currentField.getDescribe().getType() == Schema.DisplayType.MULTIPICKLIST)){

                        count = 0; // reset the counter
                        DataField currentDataField = new DataField();
                        currentDataField.FieldName = currentField.getDescribe().getName();
                        currentDataField.ListValues = new List<ListValue>();

                        List<Schema.PicklistEntry> listEntries = currentField.getDescribe().getPicklistValues(); // get all picklist entries for the current field
                        
    					for(Schema.PicklistEntry listEntry : listEntries){
                            ListValue currentPicklistValue = new ListValue();
    						currentPicklistValue.Index = count;
    						currentPicklistValue.Label = listEntry.getLabel();
    						currentPicklistValue.Value = listEntry.getValue();
    						currentDataField.ListValues.add(currentPicklistValue);
    						count++;
    					}
    					objectResult.DataFields.add(currentDataField);
    				}
    			}

    			returnResults.add(objectResult);
    		}

        } catch(Exception e){
            System.debug(e.getMessage());
            DataObject error = new DataObject();
            error.ObjectName = e.getMessage();
            returnResults.add(error);
        }

        return returnResults;
    }


    // *** add new methods above and new proxy classes below ***


    /**
    * @author Bill Hodena
    * @description Proxy class to represent a picklist or milti-picklist value
    */ 
	global class ListValue{
		public Integer Index {get;set;}  
		public String Label {get;set;}  
		public String Value {get;set;}  
	}

    /**
    * @author Bill Hodena
    * @description Proxy class to represent a field on an object
    */ 
	global class DataField{
        public List<ListValue> ListValues {get;set;} 
        public String FieldName {get;set;} 
	}

    /**
    * @author Bill Hodena
    * @description Proxy class to represent an object
    */ 
	global class DataObject{
		public String ObjectName {get;set;} 
		public List<DataField> DataFields {get;set;} 
	}

}