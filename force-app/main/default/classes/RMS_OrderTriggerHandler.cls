public with sharing class RMS_OrderTriggerHandler {
/*
 * all trigger operations should be handled in OrderTriggerTwo. per Pavan Gunnas updates from 2019. 
 * 
 * 
  private static Id COROServiceId = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
  private static final String STATUS_CLOSED = 'Appt Complete / Closed';
  private static final String STATUS_CANCELLED = 'Cancelled';
  private static final String STATUS_JOB_IN_PROGRESS = 'Job in Progress';

  public static void OnBeforeInsert(Order[] newOrders){
    setPriceBookId(newOrders);
    setNewTimeStamps(newOrders);
  }

  public static void OnAfterInsert(Order[] newOrders){
    updateJIPCOROServiceOrders(newOrders);
    checkRouteMileageOnInsert(newOrders);
  }

  public static void OnBeforeUpdate(Map<ID, Order>  oldMap, Map<ID, Order> newMap){
    // updateOrderStatus(oldMap,newMap);
    // updateOrderTimeStamps(oldMap,newMap);
    StatusCheckUtility.techMeasureStatusOnOrderUpdate(newMap, oldMap);
    updateRevenueRecDate(oldMap,newMap);
    updateUniqueIdentifier(oldMap,newMap);
    updateChangeHistories(oldMap,newMap);
    CommissionsManagement.checkCancelOrderAmounts(oldMap,newMap);
    populateOrderStatus(oldMap, newMap);
    checkRouteMileage(oldMap, newMap);
  }

  // commented out this as this is being addressed in RMS_Cancelorderextension
   
  public static void OnAfterUpdate(Map<ID, Order>  oldMap, Map<ID, Order> newMap){
    if(!UtilityMethods.hasOnHoldTaskBeenCreated()) {
      createOnHoldTasks(oldMap, newMap);
    }
  //  updateWorkOrderStatus(oldMap, newMap);
  
  }

  public static void setNewTimeStamps(List<Order> newOrders){
    DateTime now = Datetime.now();
    for(Order o: newOrders){
      if(o.Time_New__c == null && o.Status == 'New'){
        o.Time_New__c = now;
      }
    }
  }

  public static void setPriceBookId(List<Order> newOrders){
    List<Id> soldOrders = new List<Id>();
    for(Order o: newOrders){
      if(o.Sold_Order__c != null){
        soldOrders.add(o.Sold_Order__c);
      }
    }

    Map<Id,Order> soldOrderMap = new Map<Id,Order>([Select Id, PriceBook2Id from Order where Id in: soldOrders and Pricebook2Id != null]);
    for(Order o: newOrders){
      if( o.PriceBook2Id == null){
        if(soldOrderMap.containsKey(o.Sold_Order__c)){
          o.Pricebook2Id = soldOrderMap.get(o.Sold_Order__c).Pricebook2Id;
        }else{
          o.addError('A valid pricebook could not be retrieved for this order. Please contact your Administrator for help.');
        }
      }
    }
  }

  public static void updateOrderStatus(Map<ID, Order>  oldMap, Map<ID, Order> newMap){
        System.debug('in updateOrderStatus');
    ID ChangeOrderRecordTypeId = UtilityMethods.retrieveRecordTypeId('Change_Order', 'Order');
    String STATUS_WARRANTY_SUBMITTED = 'Warranty Submitted';
    String STATUS_WARRANTY_ACCEPTED = 'Warranty Accepted';
    String STATUS_WARRANTY_REJECTED = 'Warranty Rejected';
    String STATUS_CLOSED = 'Closed';

    for(Order o: newMap.values()){
      // Set the apex context to true so the Prevent Order Status Change validation rule
      // will be ignored if the status needs to be changed
      if(o.Status == 'Tech Measure Needed' && (o.Tech_Measure_Status__c == 'Pending Assignment' || o.Tech_Measure_Status__c == 'Scheduled & Assigned') && oldMap.get(o.Id).Tech_Measure_Status__c != o.Tech_Measure_Status__c){
      //  o.Apex_Context__c = true;
                System.debug('bypassing Order TM Scheduled status update');
      //  o.status = 'Tech Measure Scheduled';
      }else if(o.Status == 'Tech Measure Scheduled' && o.Tech_Measure_Status__c == 'Appt Complete / Closed' && oldMap.get(o.Id).Tech_Measure_Status__c != o.Tech_Measure_Status__c){
      //  o.Apex_Context__c = true;
                System.debug('bypassing Order Ready to Order');
      //  o.status = 'Ready to Order';
      }else if(o.Status == 'Tech Measure Scheduled' && o.Tech_Measure_Status__c == 'To be Scheduled' && oldMap.get(o.Id).Tech_Measure_Status__c != o.Tech_Measure_Status__c){
      //  o.Apex_Context__c = true;
                System.debug('bypassing Order Tech Measure Needed');
      //  o.status = 'Tech Measure Needed'; // when they Postpone WO (for Tech Measure) and WO changes back to 'Tech Measure Scheduled' via the MA controller, this will change the status of the Order back to 'Tech Measuer Needed'

      }else if(o.Status == 'Install Needed' && (o.Install_Order_Status__c == 'Pending Assignment' || o.Install_Order_Status__c == 'Scheduled & Assigned') && oldMap.get(o.Id).Install_Order_Status__c != o.Install_Order_Status__c){
        //o.Apex_Context__c = true;
        //o.status = 'Install Scheduled';
        System.debug('bypassing order Install Scheduled');
      }else if(o.Status == 'Install Scheduled' && o.Install_Order_Status__c == 'To be Scheduled' && oldMap.get(o.Id).Install_Order_Status__c != o.Install_Order_Status__c){
      //  o.Apex_Context__c = true;
      //  o.status = 'Install Needed'; // when they Postpone WO (for Install) and WO changes back to 'To be Scheduled' via the MA controller, this will change the status of the Order back to 'Install Needed'
        System.debug('bypassing order Install Needed');
      }else if(o.Status == 'Service Scheduled' && o.Service_Status__c == 'To be Scheduled' && oldMap.get(o.Id).Service_Status__c != o.Service_Status__c){
        o.Apex_Context__c = true;
        o.status = 'Service To Be Scheduled'; // when they Postpone WO (for Service) and WO changes back to 'To be Scheduled' via the MA controller, this will change the status of the Service Order back to 'Service To Be Scheduled'

      }else if(O.Revenue_Recognized_Date__c != null && o.RecordtypeId == ChangeOrderRecordTypeId){
        o.Apex_Context__c = true;
        o.status = 'Job Closed';
      }

      // Check for status change of Service Request
      if(o.RecordTypeId == COROServiceId){

      String oldStatus = oldMap.containsKey(o.Id) ? oldMap.get(o.Id).Status : null;

      if(oldStatus == o.Status)
        continue;

      if(o.Status == STATUS_WARRANTY_REJECTED){
        o.Warranty_Date_Submitted__c = null;
        o.Warranty_Processed_Date__c = null;
        o.Warranty_Rejected_Date__c = Date.today();
      } else if (o.Status == STATUS_WARRANTY_ACCEPTED){
        o.Warranty_Date_Accepted__c = Date.today();
        o.Warranty_Rejected_Date__c = null;
      } else if(o.Status == STATUS_WARRANTY_SUBMITTED){
        o.Warranty_Date_Submitted__c = Date.today();
            } else if(o.Status == STATUS_CLOSED){
        if(o.Revenue_Recognized_Date__c == null)
        o.Revenue_Recognized_Date__c = Date.today();
      }

      }

    }
  }

  public static void updateOrderTimeStamps(Map<ID, Order>  oldMap, Map<ID, Order> newMap){
    DateTime now = Datetime.now();
    for(Order o: newMap.values()){
      if(o.Status != oldMap.get(o.Id).Status){
        if(o.Status == 'Draft'){
          o.Time_Draft__c = now;
        }else if(o.Status == 'On Hold'){
          o.Time_On_Hold__c = now;
        }else if(o.Status == 'New'){
          o.Time_New__c = now;
        }else if(o.Status == 'Product Ordered'){
          o.Time_Product_Ordered__c = now;
        }else if(o.Status == 'Service Scheduled'){
          o.Time_Service_Scheduled__c = now;
        }else if(o.Status == 'Warranty Submitted'){
          o.Time_Warranty_Submitted__c = now;
        }else if(o.Status == 'Warranty Rejected'){
          o.Time_Warranty_Rejected__c = now;
        }else if(o.Status == 'Service On Hold'){
          o.Time_Service_on_Hold__c = now;
        }else if(o.Status == 'Customer Call Back'){
          o.Time_Customer_Call_Back__c = now;
        }else if(o.Status == 'Service To Be Scheduled'){
          o.Time_Service_to_be_Scheduled__c = now;
        }else if(o.Status == 'Service Complete'){
          o.Time_Service_Complete__c = now;
        }else if(o.Status == 'To Be Ordered'){
          o.Time_To_Be_Ordered__c = now;
        }else if(o.Status == 'Seasonal Service'){
          o.Time_Seasonal_Service__c = now;
        }else if(o.Status == 'Quote'){
          o.Time_Quote__c = now;
        }else if(o.Status == 'Tech Measure Needed'){
          o.Time_Tech_Measure_Needed__c = now;
        }else if(o.Status == 'Tech Measure Scheduled' && o.Time_Tech_Measure_Scheduled__c == Null){
          o.Time_Tech_Measure_Scheduled__c = now;
        }else if(o.Status == 'Ready to Order' && o.Time_Ready_To_Order__c == NULL){
          o.Time_Ready_To_Order__c = now;
        }else if(o.Status == 'Order Released'){
          o.Time_Order_Released__c = now;
        }else if(o.Status == 'Install Needed'){
          o.Time_Install_Needed__c = now;
        }else if(o.Status == 'Install Scheduled' && o.Time_Install_Scheduled__c == Null ){
          o.Time_Install_Scheduled__c = now;
        }else if(o.Status == 'Install Complete'){
          o.Time_Install_Complete__c = now;
        }else if(o.Status == 'Job in Progress'){
          o.Time_Job_In_Progress__c = now;
        }else if(o.Status == 'Job Closed'){
          o.Time_Job_Close__c = now;
          o.Job_Close_Date__c = Date.today();
        }else if(o.Status == 'Pending Cancellation'){
          o.Time_Pending_Cancellation__c = now;
        }else if(o.Status == 'Cancelled'){
          o.Time_Cancelled__c = now;
        }
      }
    }
  }

  public static void updateJIPCOROServiceOrders(List<Order> newOrders){
    List<Order> ordersToUpdate = new List<Order>();
    for(Order o : [SELECT Id, Status, Apex_Context__c FROM Order
            WHERE RecordType.DeveloperName = 'CORO_Service'
            AND Service_Type__c = :STATUS_JOB_IN_PROGRESS
            AND Sold_Order__r.Status = 'Install Scheduled'
            AND Id in: newOrders]){

      if (o.Status != STATUS_JOB_IN_PROGRESS) {
        // Allows the order status to be changed
        o.Apex_Context__c = true;
        o.Status = STATUS_JOB_IN_PROGRESS;
        ordersToUpdate.add(o);
      }

    }
    if (ordersToUpdate.size() > 0) {
      update ordersToUpdate;
    }
  }

  public static void updateRevenueRecDate(Map<ID, Order>  oldMap, Map<ID, Order> newMap){
    String STATUS_WARRANTY_SUBMITTED = 'Warranty Submitted';
    String STATUS_WARRANTY_ACCEPTED = 'Warranty Accepted';
    String STATUS_WARRANTY_REJECTED = 'Warranty Rejected';
    String STATUS_CLOSED = 'Closed';

    for(Order o: newMap.values()){
      if(o.RecordTypeId == COROServiceId && o.Status != oldMap.get(o.Id).Status
      && o.Revenue_Recognized_Date__c == null
      && ((o.Status == 'Closed' && o.Service_Type__c != 'Save')
        ||(o.Status == STATUS_WARRANTY_SUBMITTED || o.Status == 'Warranty Rejected'))){
        o.Revenue_Recognized_Date__c = System.Today();
      }
      if(o.RecordTypeId == COROServiceId && o.Status != oldMap.get(o.Id).Status && (o.Status == STATUS_WARRANTY_SUBMITTED)){
        o.Warranty_Date_Submitted__c = System.Today();
      }
      if(o.RecordTypeId == COROServiceId && o.Status != oldMap.get(o.Id).Status && (o.Status == STATUS_WARRANTY_SUBMITTED)){
        o.Warranty_Date_Submitted__c = System.Today();
      }
    }
  }

  public static void createOnHoldTasks(Map<ID, Order>  oldMap, Map<ID, Order> newMap){
    Id onHoldTaskID = UtilityMethods.retrieveRecordTypeId('On_Hold', 'Task');
    List<Task> tasksToInsert = new List<Task>();
    Boolean communityUser = UserInfo.getUserType() == 'PowerPartner';

    for(Order o: newMap.values()){
      //adding new check to see if the current user has a Partner Community license.  If so, then set Public flag to True so that Order owner can see On Hold Task
      if( o.Status != oldMap.get(o.Id).Status && o.Status == 'On Hold' && communityUser == TRUE ){
        tasksToInsert.add(new Task(WhatId = o.Id,WhoId = o.BilltoContactId, Primary_Reason__c = '', Secondary_Reason__c = '', Subject = 'On Hold', RecordtypeId = onHoldTaskID, IsVisibleInSelfService = TRUE));
      }
      if( o.Status != oldMap.get(o.Id).Status && o.Status == 'On Hold' && communityUser != TRUE ){
        tasksToInsert.add(new Task(WhatId = o.Id,WhoId = o.BilltoContactId, Primary_Reason__c = '', Secondary_Reason__c = '', Subject = 'On Hold', RecordtypeId = onHoldTaskID));
      }

    }
    if(!tasksToInsert.isEmpty()){
      insert tasksToInsert;
      UtilityMethods.setOnHoldTaskCreated();
    }
  }
  public static void populateOrderStatus(Map<ID, Order>  oldMap, Map<ID, Order> newMap){
    for(Order o:newMap.values()){
      //we are looking for orders with a satus of Draft changing to anything else
      if(oldMap.get(o.Id).Status == 'Draft' && oldMap.get(o.Id).Status != o.Status){
        o.Order_Processed_Date__c = system.today();
      }
    }
  }

  public static void checkRouteMileageOnInsert(List<Order> newList){
    Map<ID, Order> newOrderMap = new Map<ID, Order>();
    for(Order o : newList){
      newOrderMap.put(o.Id, o);
    }
    checkRouteMileage(null, newOrderMap);
  }

  public static void checkRouteMileage(Map<ID, Order> oldMap, Map<ID, Order> newMap){

    // Can't (and shouldn't) call a future method within another future method!
    if(System.isFuture() || System.isBatch())
      return;

    // Check if Route_Mileage__c is blank OR if either account has changed
    for(Order o:newMap.values()){

      Id storeLocationId = o.Store_Location__c;
      Id dwellingAccountId = o.AccountId;

      Boolean noMileage = o.Route_Mileage__c == null && storeLocationId != null && dwellingAccountId != null;

      // Check if either account has changed (should be rare)
      Order oldOrder = (oldMap != null && oldMap.containsKey(o.Id)) ? oldMap.get(o.Id) : null;
      Boolean accountsChanged = false;
      if(oldOrder != null &&
      (oldOrder.Store_Location__c != storeLocationId || oldOrder.AccountId != dwellingAccountId)){
        accountsChanged = true;
      }

      if((noMileage || accountsChanged)
      && o.RecordTypeId == COROServiceId && !Test.isRunningTest()){
        GoogleMaps.setRouteDistance(o.Id);
      }
    }

  }

  public static void updateUniqueIdentifier(Map<ID, Order>  oldMap, Map<ID, Order> newMap){
    ID CORORecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Record_Type', 'Order');
    List<Order> ordersToProcess = new List<Order>();
    for(Order o: newMap.values()){
      if(o.RecordTypeId == CORORecordTypeId && o.Status != oldMap.get(o.Id).Status &&
        o.Status != 'Cancelled' && oldMap.get(o.Id).Status == 'Draft'){
        ordersToProcess.add(o);
      }
    }

    Set<Id> storeConfigIds = new Set<Id>();
    if(!ordersToProcess.isEmpty()){
      for(Order o: [SELECT Id, Store_Location__r.Store_Number__c, Store_Location__r.Active_Store_Configuration__r.Order_Number__c FROM Order  WHERE Id in: ordersToProcess]){
        if(o.Store_Location__r.Store_Number__c == null){
          // add the error to the original record not the selected record
          Order originalOrder = newMap.get(o.Id);
          originalOrder.addError('A valid store number could not be retrieved from the Store Location Account. Please contact your Administrator for help.');
        }
        else if(o.Store_Location__r.Active_Store_Configuration__r.Order_Number__c == null){
          // add the error to the original record not the selected record
          Order originalOrder = newMap.get(o.Id);
          originalOrder.addError('A valid order number could not be retrieved from the Store Configuration. Please contact your Administrator for help.');
        }else{
          storeConfigIds.add(o.Store_Location__r.Active_Store_Configuration__c);
        }
      }

    //Lock the Store Configuration records to prevent another transaction from altering the Order Number
    Map<Id,Store_Configuration__c> configsToLock = new Map<Id,Store_Configuration__c>([Select Id, Order_Number__c from Store_Configuration__c where Id in: storeConfigIds for update]);

    for(Order o: [SELECT Id, Store_Location__r.Store_Number__c, Store_Location__r.Active_Store_Configuration__c FROM Order  WHERE Id in: ordersToProcess
            AND Store_Location__r.Store_Number__c != null AND Store_Location__r.Active_Store_Configuration__c in: configsToLock.keyset()]){
      String storeNum = o.Store_Location__r.Store_Number__c;
      while(storeNum.length()<4){
        storeNum = '0' + storeNum;
      }
      String uniqueIDString = storeNum + configsToLock.get(o.Store_Location__r.Active_Store_Configuration__c).Order_Number__c.toPlainString();
      while(uniqueIdString.length() < 11){
        uniqueIdString = uniqueIdString.substring(0,4) + '0' + uniqueIdString.substring(4);
      }
      if(newMap.get(o.Id).Unique_Identifier__c == null){
        newMap.get(o.Id).Unique_Identifier__c = uniqueIdString;
        configsToLock.get(o.Store_Location__r.Active_Store_Configuration__c).Order_Number__c +=1;
      }
    }
    update configsToLock.values();
    }
  }

  public static void updateChangeHistories(Map<ID, Order>  oldMap, Map<ID, Order> newMap){
    Set<Id> orderIdsToUpdateChangeHistory = new Set<Id>();
    for(Order o: newMap.values()){
      // If the revenue recognized date or booking date has changed on an order,
      // for CORO Orders or Change Orders, add it to the list
      if( ((o.Revenue_Recognized_Date__c != oldMap.get(o.Id).Revenue_Recognized_Date__c) ||
        (o.EffectiveDate != oldMap.get(o.Id).EffectiveDate)) &&
        ((o.RecordTypeId == UtilityMethods.RecordTypeFor('Order', 'CORO_Record_Type') ||
        (o.RecordTypeId == UtilityMethods.RecordTypeFor('Order', 'Change_Order'))) )){
      orderIdsToUpdateChangeHistory.add(o.Id);
      }
    }
    // If none of the updated orders meet the criteria above, just return
    if(orderIdsToUpdateChangeHistory.isEmpty()){
      return;
    }

    List<Change_History__c> changeHistoriesToUpdate = new List<Change_History__c>();
    // Now loop through all of the change histories linked to the orders in the set and update their
    // Rev Recognized and Booking dates
    for (Change_History__c ch : [SELECT Id, Order_Revenue_Recognition_Date__c, Order_Booking_Date__c, Order_Product__r.OrderId, Order_Discount__r.Order__r.Id FROM Change_History__c
                    WHERE Order_Product__r.OrderId IN : orderIdsToUpdateChangeHistory OR Order_Discount__r.Order__r.Id IN : orderIdsToUpdateChangeHistory])
    {
      // If this is an Order Product change history
      if (orderIdsToUpdateChangeHistory.contains(ch.Order_Product__r.OrderId)) {
        ch.Order_Revenue_Recognition_Date__c = newMap.get(ch.Order_Product__r.OrderId).Revenue_Recognized_Date__c;
        ch.Order_Booking_Date__c = newMap.get(ch.Order_Product__r.OrderId).EffectiveDate;
        changeHistoriesToUpdate.add(ch);
      }

      // If this is an Order Discount change history
      if (orderIdsToUpdateChangeHistory.contains(ch.Order_Discount__r.Order__r.Id)) {
        ch.Order_Revenue_Recognition_Date__c = newMap.get(ch.Order_Discount__r.Order__r.Id).Revenue_Recognized_Date__c;
        ch.Order_Booking_Date__c = newMap.get(ch.Order_Discount__r.Order__r.Id).EffectiveDate;
        changeHistoriesToUpdate.add(ch);
      }
    }

    update changeHistoriesToUpdate;

  }

  // commented method as this is being addressed in RMS_Cancelorder extension
  
  public static void updateWorkOrderStatus(Map<Id, Order> oldMap, Map<Id, Order> newMap){
    List<Order> scope = new List<Order>();
    for(Order o : oldMap.values()) {
      if(o.Status != STATUS_CANCELLED && newMap.get(o.Id).Status == STATUS_CANCELLED) {
        scope.add(o);
      }
    }

    List<WorkOrder> workOrders = [SELECT Id, Status, RecordType.Name, OwnerId, Zip_5_Character__c, ServiceTerritoryId, Sold_Order__r.OwnerId FROM WorkOrder WHERE Sold_Order__c IN :scope AND STATUS != :STATUS_CLOSED];
    List<Task> existingTasks = [SELECT Id, WhatId FROM Task WHERE WhatId IN :workOrders AND Subject = 'Manually Cancel Work Order'];
    Set<String> finishedWorkOrders = new Set<String>();
    for(Task t : existingTasks) {
      finishedWorkOrders.add(t.WhatId);
    }
    List<WorkOrder> updatingOrders = new List<WorkOrder>();
    List<Task> newTasks = new List<Task>();

    for(WorkOrder wo : workOrders){
      System.debug(wo);
      if(wo.RecordType.Name == 'Tech Measure' || wo.RecordType.Name == 'Install') {
        if(wo.Status == 'To be Scheduled') {
          wo.Status = STATUS_CANCELLED;
          updatingOrders.add(wo);
        } else {
          if(!finishedWorkOrders.contains(wo.Id)){
            Task t = new Task();
            t.WhatId = wo.Id;
            t.OwnerId = wo.Sold_Order__r.OwnerId;
            t.Priority = 'Normal';
            t.Status = 'Not Started';
            t.Subject = 'Manually Cancel Work Order';
            t.Description = 'The Order for this Work Order record has been cancelled. Please manually cancel this Work Order.';
            newTasks.add(t);
          }
        }
      } else if(wo.RecordType.Name == 'Service' || wo.RecordType.Name == 'Permit') {
        if(!finishedWorkOrders.contains(wo.Id)) {
          Task t = new Task();
          t.WhatId = wo.Id;
          t.OwnerId = wo.Sold_Order__r.OwnerId;
          t.Priority = 'Normal';
          t.Status = 'Not Started';
          t.Subject = 'Manually Cancel Work Order';
          t.Description = 'The Order for this Work Order record has been cancelled. Please manually cancel this Work Order.';
          newTasks.add(t);
        }
      } else {
        wo.Status = STATUS_CANCELLED;
        updatingOrders.add(wo);
      }
    }

    if(updatingOrders.size() > 0) {
      update updatingOrders;
    }

    if(newTasks.size() > 0) {
      insert newTasks;
    }
  } 
  
      
    // @Author PavanGunna
    //  * Story 1143 
    //  * When Estimated Ship date is entered to the Order we need to default the value for Service appointments.
    //  * Trigger on Order to update related workorders Service Appointments. 
    
    public static void UpdateOrderEstiShipDateToSerApp(Map<Id,Order> newMap, Map<Id, Order> oldMap){
        
        id OrderCoroSerRecTypeid = UtilityMethods.retrieveRecordTypeId('CORO_Record_Type', 'Order');
        set<id> OrderNumberSet = new set<id>();
        List<ServiceAppointment> UpdateSAList = new List<ServiceAppointment>();
        Map<id, List<Workorder>> OrderToWorkOrder = new Map<id,List<Workorder>>();
        Map<id, List<ServiceAppointment>> WorkOrderToSA = new Map<id,List<ServiceAppointment>>();
                
        for(order o: newMap.values()){
            if( o.recordtypeid == OrderCoroSerRecTypeid &&
                (oldmap == Null || ( oldmap != Null && oldmap.get(o.id).Estimated_Ship_Date__c != o.Estimated_Ship_Date__c) )){
                OrderNumberSet.add(o.id);
            }
        }
        
        for(Workorder wo : [Select id,Sold_Order__c ,
                            (Select ID , Sold_Order__c,First_Available_Date__c from ServiceAppointments)
                            from Workorder WHERE Sold_Order__c IN: OrderNumberSet]){
                                
                             WorkOrderToSA.put(wo.id, wo.ServiceAppointments);
                             
                                if(OrderToWorkOrder.get(wo.Sold_Order__c) == Null){
                                    
                                    OrderToWorkOrder.put(wo.Sold_Order__c, new List<Workorder>());
                                }
                                  OrderToWorkOrder.get(wo.Sold_Order__c).add(wo);           
            
        }
        
        for(Order orderrec : newMap.values() ){
            
            if(OrderNumberSet.contains(orderrec.id) && OrderToWorkOrder.containsKey(orderrec.id) && !OrderToWorkOrder.get(orderrec.id).isEmpty()){
              
                for( WorkOrder wo: OrderToWorkOrder.get(orderrec.id)){
                    
                    if(WorkOrderToSA.containsKey(wo.id) && !WorkOrderToSA.get(wo.id).isEmpty()){
                        
                        for(ServiceAppointment SerAppRec : WorkOrderToSA.get(wo.id)){
                            
                            if(SerAppRec.First_Available_Date__c != orderrec.Arrival_Date__c){
                              SerAppRec.First_Available_Date__c = orderrec.Arrival_Date__c;
                              UpdateSAList.add(SerAppRec);
                            }
                        }                        
                    }
                }   
            }
        }
        if(!UpdateSAList.isEmpty()) 
            update UpdateSAList;
    }
*/
}