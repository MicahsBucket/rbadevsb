public without sharing class CreateServiceRequestController {
  public static Boolean runChargeHistoryTrigger = true;

  @AuraEnabled(cacheable=false)
  public static Order getOrder(String orderId) {
    Order order = new Order();

    if (String.isNotBlank(orderId) && orderId != 'undefined') {
      order = [
        SELECT
          AccountId,
          Description,
          EffectiveDate,
          Installation_Date__c,
          Install_Sched_End_Time__c,
          Job_Close_Date__c,
          OrderNumber,
          Status,
          Service_Type__c,
          Store_Location__r.Name
        FROM Order
        WHERE Id = :orderId
      ];
    }

    return order;
  }

  /*
   * @author Jason Flippen
   * @date 10/05/2020
   * @description Method to return (wrapped) List of Service Types.
   */
  @AuraEnabled
  public static List<SelectOptionWrapper> getServiceTypes(Order currentOrder) {
    List<SelectOptionWrapper> wrapperList = new List<SelectOptionWrapper>();

    SelectOptionWrapper defaultWrapper = new SelectOptionWrapper(
      '--None--',
      'NA'
    );
    wrapperList.add(defaultWrapper);

    // Add the Service Types to the Wrapper List.
    Schema.DescribeFieldResult serviceTypeFieldResult = Order.Service_Type__c.getDescribe();
    List<Schema.PicklistEntry> serviceTypePLEList = serviceTypeFieldResult.getPicklistValues();
    for (Schema.PicklistEntry serviceTypePLE : serviceTypePLEList) {
      String serviceTypeValue = serviceTypePLE.getLabel();

      String optionLabel = '';
      String optionValue = '';
      Boolean addToList = false;
      if (
        currentOrder.Install_Sched_End_Time__c == null &&
        serviceTypeValue != 'Job in Progress'
      ) {
        optionLabel = serviceTypeValue;
        optionValue = serviceTypeValue;
        addToList = true;
      } else if (currentOrder.Install_Sched_End_Time__c != null) {
        Date currentDate = Date.today();
        Datetime installScheduledEndTime = currentOrder.Install_Sched_End_Time__c;
        Date installScheduledDate = Date.newInstance(
          installScheduledEndTime.year(),
          installScheduledEndTime.month(),
          installScheduledEndTime.day()
        );
        Integer daysDifference = installScheduledDate.daysBetween(currentDate);

        if (daysDifference <= 30 && serviceTypeValue == 'Job in Progress') {
          optionLabel = serviceTypeValue;
          optionValue = serviceTypeValue;
          addToList = true;
        } else if (
          daysDifference > 30 &&
          serviceTypeValue != 'Job in Progress'
        ) {
          optionLabel = serviceTypeValue;
          optionValue = serviceTypeValue;
          addToList = true;
        }
      }

      if (addToList == true) {
        SelectOptionWrapper wrapper = new SelectOptionWrapper(
          optionLabel,
          optionValue
        );
        wrapperList.add(wrapper);
      }
    }

    return wrapperList;
  }

  @AuraEnabled(cacheable=true)
  public static List<SelectOptionWrapper> getDefectCode(
    String responsibilty,
    String mtoSourceCode
  ) {
    System.debug('UI THEME: ' + UserInfo.getUiThemeDisplayed());
    List<SelectOptionWrapper> selectOptions = new List<SelectOptionWrapper>();
    for (MTOSourceCode_DefectCode__c mdefect : [
      SELECT id, Responsibility__c, Defect_Code__c, Defect_Code__r.Name
      FROM MTOSourceCode_DefectCode__c
      WHERE
        Responsibility__c = :responsibilty
        AND MTO_Source_Code__c = :mtoSourceCode
    ]) {
      SelectOptionWrapper sw = new SelectOptionWrapper(
        mdefect.Defect_Code__c,
        mdefect.Defect_Code__r.Name
      );
      selectOptions.add(sw);
    }

    return selectOptions;
  }

  @AuraEnabled(cacheable=true)
  public static Map<String, List<SelectOptionWrapper>> getMTOSourceCode() {
    System.debug('UI THEME: ' + UserInfo.getUiThemeDisplayed());
    Set<String> productTypes = new Set<String>{ 'Door', 'Window' };
    Map<String, List<SelectOptionWrapper>> mtoMap = new Map<String, List<SelectOptionWrapper>>();

    for (MTO_Source_code__c msc : [
      SELECT id, Name, Product_Type__c
      FROM MTO_Source_code__c
      WHERE product_type__c IN :productTypes
      ORDER BY Name
    ]) {
      system.debug('msc: ' + msc);
      if (!mtoMap.containsKey(msc.product_type__c)) {
        mtoMap.put(msc.product_type__c, new List<SelectOptionWrapper>());
      }
      mtoMap.get(msc.product_type__c)
        .add(new SelectOptionWrapper(msc.Name, msc.id));
    }
    return mtoMap;
  }

  @AuraEnabled
  public static List<AssetProductWrapper> getAssetsWithProducts1(
    String orderId
  ) {
    List<AssetProductWrapper> assetsWithProducts = new List<AssetProductWrapper>();
    Order currentOrder = CreateServiceRequestController.getOrder(orderId);
    Map<String, Asset> productIdWithAsset = new Map<String, Asset>();
    Map<Asset, String> assetWithProductId = new Map<Asset, String>();
    List<Asset> allAssets = [
      SELECT
        Id,
        Name,
        Product_Name__c,
        Install_Date__c,
        Product2Id,
        Variant_Number__c,
        Product2.Name,
        Original_Order_Variant_Number__c,
        Location_Details__c,
        Original_Order_Product__c,
        Status,
        Sold_Order__c,
        Unit_ID__c,
        Product2.Family
      FROM Asset
      WHERE
        accountId = :currentOrder.accountId
        AND Status != 'Replaced'
        AND CancelledServiceProducts__c = FALSE
        AND Product2.Has_Service_Products__c = TRUE
      ORDER BY Name
    ];
    for (Asset currentAsset : allAssets) {
      productIdWithAsset.put(currentAsset.Product2Id, currentAsset);
    }
    Map<String, Set<Service_Product__c>> productWithServiceProducts = new Map<String, Set<Service_Product__c>>();
    for (Service_Product__c serviceProd : [
      SELECT id, Service_Product__c, Service_Product__r.Name, Master_Product__c
      FROM Service_Product__c
      WHERE Master_Product__c IN :productIdWithAsset.keyset()
    ]) {
      if (
        !productWithServiceProducts.containsKey(serviceProd.Master_Product__c)
      ) {
        productWithServiceProducts.put(
          serviceProd.Master_Product__c,
          new Set<Service_Product__c>{ serviceProd }
        );
      }
      productWithServiceProducts.get(serviceProd.Master_Product__c)
        .add(serviceProd);
    }
    for (Asset childAsset : allAssets) {
      AssetProductWrapper assetWrap = new AssetProductWrapper();
      assetWrap.assetId = childAsset.Id;
      assetWrap.assetName = childAsset.Name;
      assetWrap.unitId = childAsset.Unit_ID__c;
      assetWrap.assetUrl = '/' + childAsset.Id;
      system.debug(
        'childAsset.Product2.Family+++++++++++ ' + childAsset.Product2.Family
      );
      Set<String> DoorProductFamilySet = new Set<String>();
      for (
        DoorProductFamilyList__mdt dpf : ([
          SELECT Id, MasterLabel
          FROM DoorProductFamilyList__mdt
        ])
      ) {
        DoorProductFamilySet.add(dpf.MasterLabel);
      }
      System.debug('DoorProductFamilySet= ' + DoorProductFamilySet);
      System.debug(
        'DoorProductFamilySet.contains(childAsset.Product2.Family)= ' +
        DoorProductFamilySet.contains(childAsset.Product2.Family)
      );
      if (DoorProductFamilySet.contains(childAsset.Product2.Family)) {
        assetWrap.isDoor = true;
      }

      // Set the Display Name of this Asset.
      assetWrap.displayName = childAsset.Name;
      if (String.isNotEmpty(childAsset.Variant_Number__c)) {
        assetWrap.displayName += ' | ' + childAsset.Variant_Number__c;
      }
      if (String.isNotEmpty(childAsset.Unit_ID__c)) {
        assetWrap.displayName += ' | ' + childAsset.Unit_ID__c;
      }

      List<ServiceProductWrapper> serviceWrapper = new List<ServiceProductWrapper>();
      if (productWithServiceProducts.containsKey(childAsset.Product2Id)) {
        for (
          Service_Product__c serviceProd : productWithServiceProducts.get(
            childAsset.Product2Id
          )
        ) {
          ServiceProductWrapper serviceProdWrap = new ServiceProductWrapper(
            serviceProd,
            '',
            '',
            false,
            childAsset.Id
          );
          serviceWrapper.add(serviceProdWrap);
        }
        assetWrap.serviceProducts = serviceWrapper;
      }
      assetsWithProducts.add(assetWrap);
    }
    System.debug('@@@ assetsWithProducts' + assetsWithProducts);
    return assetsWithProducts;
  }

  /*
    @AuraEnabled
    public static void deleteLineItems(String orderId) {
        DELETE [SELECT id FROM OrderItem WHERE OrderId = : orderId];
    }
*/

  @AuraEnabled
  public static void saveCharges(
    String chargeJSON,
    List<SObject> orderItemToUpdate
  ) {
    if (String.isNotEmpty(chargeJSON)) {
      List<Charge__c> charges = (List<Charge__c>) JSON.deserialize(
        chargeJSON,
        List<Charge__c>.class
      );
      upsert charges;
    }
    if (orderItemToUpdate.size() > 0) {
      update orderItemToUpdate;
    }
  }

  @AuraEnabled
  public static Contact_History__c getPrimaryContact(String accountId) {
    Contact_History__c primaryContact;
    if (String.isNotEmpty(accountId)) {
      primaryContact = [
        SELECT Contact__c, Dwelling__c, Primary_Contact__c, Id
        FROM Contact_History__c
        WHERE Dwelling__c = :accountId AND Primary_Contact__c = TRUE
        LIMIT 1
      ];
    }
    return primaryContact;
  }

  @AuraEnabled
  public static void saveAll(String orderId, String rowIdentifier) {
    Order newOrder;
    List<AssetProductWrapper> rows = (List<AssetProductWrapper>) JSON.deserialize(rowIdentifier,List<AssetProductWrapper>.class);
    if (rows != null && rows.size() > 0) {
      Id standardPriceBookId;
      Map<Id, PricebookEntry> productWithPriceBookEntry = new Map<Id, PricebookEntry>();
      if (Test.isRunningTest()) {
        standardPriceBookId = Test.getStandardPricebookId();
      } else {
        standardPriceBookId = [SELECT Id FROM Pricebook2 WHERE isStandard = TRUE].Id;
      }
      Set<Id> serviceProductIds = new Set<Id>();
      Set<String> mtoSourceCodeIds = new Set<String>();
      Set<String> defectCodesIds = new Set<String>();
      List<ServiceProductWrapper> serviceWrappers = new List<ServiceProductWrapper>();
      List<ServiceProductWrapper> serviceWrappersRows = new List<ServiceProductWrapper>();
      for (AssetProductWrapper assetWrap : rows) {
        serviceWrappers.addAll(assetWrap.serviceProducts);
      }
      for (ServiceProductWrapper row : serviceWrappers) {
        if (row.isSelected) {
          serviceProductIds.add(row.product.Service_Product__c);
          serviceWrappersRows.add(row);
          mtoSourceCodeIds.add(row.mtoSourceCode);
          defectCodesIds.add(row.selectedDefectCode);
        }
      }
      Map<String, MTO_Source_code__c> mtoSourceCodes = new Map<String, MTO_Source_code__c>();
      Map<Id, String> defectCodes = new Map<Id, String>();
      if (mtoSourceCodeIds.size() > 0) {
        for (MTO_Source_code__c mtoSourceCode : [
          SELECT id, Name, Product_Type__c
          FROM MTO_Source_code__c
          WHERE Id IN :mtoSourceCodeIds
          ORDER BY Name
        ]) {
          mtoSourceCodes.put(mtoSourceCode.Id, mtoSourceCode);
        }
      }
      if (defectCodesIds.size() > 0) {
        for (MTOSourceCode_DefectCode__c mdefect : [
          SELECT id, Defect_Code__c, Defect_Code__r.Name
          FROM MTOSourceCode_DefectCode__c
          WHERE Defect_Code__c IN :defectCodesIds
        ]) {
          defectCodes.put(mdefect.Defect_Code__c, mdefect.Defect_Code__r.Name);
        }
      }

      if (serviceProductIds.size() > 0) {
        for (PricebookEntry pbe : [
          SELECT Id, Product2Id, Pricebook2Id, UnitPrice
          FROM PricebookEntry
          WHERE
            Pricebook2Id = :standardPriceBookId
            AND isActive = TRUE
            AND Product2Id IN :serviceProductIds
        ]) {
          productWithPriceBookEntry.put(pbe.Product2Id, pbe);
        }

        List<OrderItem> listOI = new List<OrderItem>();
        Map<String, List<ServiceProductWrapper>> serviceProdMap = new Map<String, List<ServiceProductWrapper>>();
        map<string, string> ServiceProductToResponsibility = new Map<string, string>();
        map<string, string> ServiceProductToCategory = new Map<string, string>();
        map<string, string> ServiceProductToDefectDetails = new Map<string, string>();
        for (ServiceProductWrapper row : serviceWrappersRows) {
          if (productWithPriceBookEntry.containsKey(row.product.Service_Product__c)) {
            ServiceProductToCategory.put(row.product.Service_Product__c, row.selectedSourceOfDefect);
            ServiceProductToDefectDetails.put(row.product.Service_Product__c, row.defectDetails);
            ServiceProductToResponsibility.put(row.product.Service_Product__c, row.responsibilty);
            OrderItem oi = new OrderItem();
            oi.UnitPrice = productWithPriceBookEntry.get(row.product.Service_Product__c).UnitPrice;
              oi.OrderId = orderId;
              oi.PriceBookEntryId = productWithPriceBookEntry.get(row.product.Service_Product__c).id;
            String assetId = row.rowIdentifier.split('#')[1];
            oi.Sold_Order_Product_Asset__c = assetId;
            if ('Manufacturing'.equalsIgnoreCase(row.responsibilty)) {
              oi.Manufacturer_Replace__c = true;
            } else if ('Retailer'.equalsIgnoreCase(row.responsibilty)) {
              oi.Local_Remake__c = true;
            } else if ('Customer'.equalsIgnoreCase(row.responsibilty)) {
              oi.Customer_Replace__c = true;
            }
            oi.Quanity_Ordered__c = 1;
            oi.Quantity = 1;
            oi.Service__c = true;
            oi.Notes__c = row.notes;
            if (mtoSourceCodes.containsKey(row.mtoSourceCode)) {
              oi.MTO_Source_Code__c = mtoSourceCodes.get(row.mtoSourceCode).Name;
            }
            if (defectCodes.containsKey(row.selectedDefectCode)) {
              oi.Defect_Code__c = defectCodes.get(row.selectedDefectCode);
            }

            listOI.add(oi);
            if (!serviceProdMap.containsKey(row.product.Service_Product__c)) {
              serviceProdMap.put(row.product.Service_Product__c, new List<ServiceProductWrapper>());
              serviceProdMap.get(row.product.Service_Product__c).add(row);
            } else {
              serviceProdMap.get(row.product.Service_Product__c).add(row);
            }
          }
        }
        if (listOI.size() > 0) {
          insert listOI;
        }
        
        Set<String> orderItemIds = new Set<String>();
        for (OrderItem newOrderItem : listOI) {
          orderItemIds.add(newOrderItem.Id);
        }
        
        List<Charge__c> newCharges = new List<Charge__c>();
        List<Charge__c> chargeUpdates = new List<Charge__c>();
        map<Id, Charge__c> chargesMap = new Map<Id, Charge__c>();
        for (Charge__c charge : [SELECT Charge_Cost_To__c,What_Where__c,Category__c,Service_Product__c,Service_Product__r.Sold_Order_Product_Asset__c, Service_Product__r.Product2Id FROM Charge__c WHERE Service_Product__c IN :orderItemIds]) {
          if (charge.Service_Product__c != null && ServiceProductToResponsibility.containsKey(charge.Service_Product__r.Product2Id) ) {
            charge.Charge_Cost_To__c = ServiceProductToResponsibility.get(charge.Service_Product__r.Product2Id);
          }
          if (charge.Service_Product__c != null && ServiceProductToDefectDetails.containsKey(charge.Service_Product__r.Product2Id) ) {
            charge.What_Where__c = ServiceProductToDefectDetails.get(charge.Service_Product__r.Product2Id);
          }
          if (charge.Service_Product__c != null && ServiceProductToCategory.containsKey(charge.Service_Product__r.Product2Id) ) {
            charge.Category__c = ServiceProductToCategory.get(charge.Service_Product__r.Product2Id);
          }
          chargeUpdates.add(charge);
        }
        if (chargeUpdates.size() > 0) {
          update chargeUpdates;
        }
      }
    }
  }

  @AuraEnabled
  public static String saveOrderRecord(String currentOrder, String accountId) {
    Order newOrder;
    try {
      Id standardPriceBookId;
      if (Test.isRunningTest()) {
        standardPriceBookId = Test.getStandardPricebookId();
      } else {
        standardPriceBookId = [
          SELECT Id
          FROM Pricebook2
          WHERE isStandard = TRUE
        ]
        .Id;
      }
      if (String.isNotEmpty(currentOrder)) {
        newOrder = (Order) JSON.deserialize(currentOrder, Order.class);
      } else {
        newOrder = new Order();
      }
      System.debug('@@@@' + newOrder);
      if (newOrder.RecordTypeId == null) {
        newOrder.RecordTypeId = UtilityMethods.retrieveRecordTypeId(
          'CORO_Service',
          'Order'
        );
      }
      Account sl = new Account();
      sl = [
        SELECT Store_Location__c, Store_Location__r.Sales_Tax__c
        FROM Account
        WHERE id = :accountId
      ];
      if (sl != null) {
        newOrder.Sales_Tax_Percent__c = sl.Store_Location__r.Sales_Tax__c;
        newOrder.Store_Location__c = sl.Store_Location__c;
      }
      if (newOrder.Sold_Order__c != null) {
        Order so = [
          SELECT
            ShippingStreet,
            ShippingCity,
            ShippingState,
            ShippingPostalCode,
            ShippingCountry,
            BillingStreet,
            BillingCity,
            BillingState,
            BillingPostalCode,
            BillingCountry
          FROM Order
          WHERE id = :newOrder.Sold_Order__c
          LIMIT 1
        ];
        if (so != null) {
          newOrder.BillingStreet = so.BillingStreet;
          newOrder.BillingCity = so.BillingCity;
          newOrder.BillingState = so.BillingState;
          newOrder.BillingPostalCode = so.BillingPostalCode;
          newOrder.BillingCountry = so.BillingCountry;
          newOrder.ShippingStreet = so.ShippingStreet;
          newOrder.ShippingCity = so.ShippingCity;
          newOrder.ShippingState = so.ShippingState;
          newOrder.ShippingPostalCode = so.ShippingPostalCode;
          newOrder.ShippingCountry = so.ShippingCountry;
        }
      }
      newOrder.Status = 'New';
      newOrder.Pricebook2Id = standardPriceBookId;
      newOrder.AccountId = accountId;
      newOrder.EffectiveDate = System.Today();
      if (newOrder.Service_Type__c == 'Save') {
        newOrder.Service_Status__c = 'Closed';
      }
      if (newOrder.Service_Type__c != null) {
        newOrder.Validate_Service_Order__c = true;
      }
      if (newOrder.Service_Type__c == 'Legacy') {
        newOrder.Sold_Order__c = null;
      }
      upsert newOrder;

      if(newOrder != null){
        Contact c;
        Account a = [SELECT Id, Name, Store_Location__c, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingCountry, ShippingState, Primary_Contact__c FROM Account WHERE Id = :newOrder.AccountId];
        String zip5;
        if (a.ShippingCountry == 'Canada') {
            if(a.ShippingPostalCode.length() == 7){
                zip5 = a.ShippingPostalCode.substring(0,7);
            }
            if(a.ShippingPostalCode.length() == 6){
                zip5 = a.ShippingPostalCode.substring(0,6);
            }
        } else {
            zip5 = a.ShippingPostalCode.substring(0,5);
        }
        system.debug('@pavan country '+a.ShippingCountry);
        system.debug('@pavan zip is  '+zip5);
        Service_Territory_Zip_Code__c code = [SELECT Id, Service_Territory__c FROM Service_Territory_Zip_Code__c WHERE Name = :zip5 LIMIT 1];
        
        try{
            c = [SELECT Id, Primary_Contact__c FROM Contact WHERE Primary_Contact__c = TRUE AND AccountId = :a.Id LIMIT 1];
        } catch(Exception ex){
            c = null;
        }

       // Rforce 1020-This is removed as  Service work Order being created two times on Service request.
       //This method is called on RMS_WorkOrderCreationManager
       System.debug('o.Service_Type__c+++++ '+newOrder.Service_Type__c);
       System.debug('CONTACT+++++ '+c);
       if(newOrder.Service_Type__c != 'Save'){
          System.debug('IN WO LEVEL');
            //create service Work Order
            WorkOrder wo = new WorkOrder();
            wo.Sold_Order__c = newOrder.Id;
            wo.AccountId = a.Id;
            wo.Street = a.ShippingStreet;
            wo.City = a.ShippingCity;
            wo.State = a.ShippingState;
            wo.PostalCode = a.ShippingPostalCode;
            wo.Country = a.ShippingCountry;
            wo.RecordTypeId = UtilityMethods.retrieveRecordTypeId('Service', 'WorkOrder');
            wo.Work_Order_Type__c = 'Service';
            wo.WorkTypeId = [SELECT Id, Name FROM WorkType WHERE Name = 'Service' LIMIT 1].Id;
            wo.ServiceTerritoryId = code.Service_Territory__c;
            if(c != null){
                wo.ContactId = c.Id;
            } 
            else{
                wo.ContactId = [SELECT Id, Contact__c FROM Contact_History__c WHERE Dwelling__c =: a.Id LIMIT 1].Contact__c;
            }
             System.debug('wo.ContactId++++++ '+ wo.ContactId);
            insert wo;
          }
        }
    } catch (Exception exc) {
      System.debug('@@@@errorMessage' + exc.getMessage());
      System.debug('@@@@errorMessage' + exc.getStackTraceString());
    }
    return newOrder.Id;
  }

  @AuraEnabled
  public static List<SelectOptionWrapper> fetchDefectCodes(
    String responsibility,
    String mtoSourceCode
  ) {
    List<SelectOptionWrapper> selectOptions = new List<SelectOptionWrapper>();
    SyStem.debug('@@@@' + responsibility);
    SyStem.debug('@@@@mtoSourceCode' + mtoSourceCode);
    for (MTOSourceCode_DefectCode__c mdefect : [
      SELECT
        id,
        Responsibility__c,
        Defect_Code__c,
        Defect_Code__r.Name,
        MTO_Source_Code__c
      FROM MTOSourceCode_DefectCode__c
      WHERE
        Responsibility__c = :responsibility
        AND MTO_Source_Code__c = :mtoSourceCode
      ORDER BY Defect_Code__r.Name
    ]) {
      selectOptions.add(
        new SelectOptionWrapper(
          mdefect.Defect_Code__r.Name,
          mdefect.Defect_Code__c
        )
      );
    }
    return selectOptions;
  }

  @AuraEnabled
  public static List<GroupWrapper> fetchGroupDetails(
    String orderId,
    String rowIdentifier
  ) {
    Order newOrder;
    Set<String> mtoSourceCode = new Set<String>();
    List<ServiceProductWrapper> rows = (List<ServiceProductWrapper>) JSON.deserialize(
      rowIdentifier,
      List<ServiceProductWrapper>.class
    );
    if (rows != null && rows.size() > 0) {
      Id standardPriceBookId;
      Map<Id, PricebookEntry> productWithPriceBookEntry = new Map<Id, PricebookEntry>();
      if (Test.isRunningTest()) {
        standardPriceBookId = Test.getStandardPricebookId();
      } else {
        standardPriceBookId = [
          SELECT Id
          FROM Pricebook2
          WHERE isStandard = TRUE
        ]
        .Id;
      }
      Set<Id> serviceProductIds = new Set<Id>();
      for (ServiceProductWrapper row : rows) {
        serviceProductIds.add(row.product.Service_Product__c);
      }
      if (serviceProductIds.size() > 0) {
        for (PricebookEntry pbe : [
          SELECT Id, Product2Id, Pricebook2Id, UnitPrice
          FROM PricebookEntry
          WHERE
            Pricebook2Id = :standardPriceBookId
            AND isActive = TRUE
            AND Product2Id IN :serviceProductIds
        ]) {
          productWithPriceBookEntry.put(pbe.Product2Id, pbe);
        }

        List<OrderItem> listOI = new List<OrderItem>();
        for (ServiceProductWrapper row : rows) {
          if (
            productWithPriceBookEntry.containsKey(
              row.product.Service_Product__c
            )
          ) {
            OrderItem oi = new OrderItem();
            oi.UnitPrice = productWithPriceBookEntry.get(
                row.product.Service_Product__c
              )
              .UnitPrice;
            oi.OrderId = orderId;
            oi.PriceBookEntryId = productWithPriceBookEntry.get(
                row.product.Service_Product__c
              )
              .id;
            String assetId = row.rowIdentifier.split('#')[1];
            oi.Sold_Order_Product_Asset__c = assetId;
            if ('Manufacturing'.equalsIgnoreCase(row.responsibilty)) {
              oi.Manufacturer_Replace__c = true;
            } else if ('Retailer'.equalsIgnoreCase(row.responsibilty)) {
              oi.Local_Remake__c = true;
            } else if ('Customer'.equalsIgnoreCase(row.responsibilty)) {
              oi.Customer_Replace__c = true;
            }
            oi.Quanity_Ordered__c = 1;
            oi.Quantity = 1;
            oi.Service__c = true;
            oi.Notes__c = row.notes;
            oi.MTO_Source_Code__c = row.mtoSourceCode;
            mtoSourceCode.add(oi.MTO_Source_Code__c);
            listOI.add(oi);
          }
        }
        if (listOI.size() > 0) {
          insert listOI;
        }
      }
    }
    Map<String, String> groupedValWithUnit = new Map<String, String>();
    Map<String, String> groupedValWithVariantNumber = new Map<String, String>();
    Map<String, String> groupedValWithProductName = new Map<String, String>();
    String groupFieldName = 'Installed_Product_Name__c';
    Set<String> responsibilty = new Set<String>{
      'Manufacturing',
      'Retailer',
      'Customer'
    };
    Map<String, List<SelectOptionWrapper>> selectOptionsMap = new Map<String, List<SelectOptionWrapper>>();
    for (MTOSourceCode_DefectCode__c mdefect : [
      SELECT
        id,
        Responsibility__c,
        Defect_Code__c,
        Defect_Code__r.Name,
        MTO_Source_Code__c
      FROM MTOSourceCode_DefectCode__c
      WHERE
        Responsibility__c IN :responsibilty
        AND MTO_Source_Code__c = :mtoSourceCode
    ]) {
      String keyDefect = mdefect.Responsibility__c + mdefect.MTO_Source_Code__c;
      if (!selectOptionsMap.containsKey(keyDefect)) {
        selectOptionsMap.put(keyDefect, new List<SelectOptionWrapper>());
      }
      SelectOptionWrapper sw = new SelectOptionWrapper(
        mdefect.Defect_Code__r.Name,
        mdefect.Defect_Code__c
      );
      selectOptionsMap.get(keyDefect).add(sw);
    }
    System.debug('@@@selectOptionsMap' + selectOptionsMap);
    List<OrderItem> allOrderItems = [
      SELECT
        Id,
        OrderId,
        Product_Name__c,
        Installed_Product_Name__c,
        MTO_Source_Code__c,
        Manufacturer_Replace__c,
        Local_Remake__c,
        Customer_Replace__c,
        Sold_Order_Product_Asset__r.Variant_Number__c,
        Sold_Order_Product_Asset__r.Unit_Id__c,
        Sold_Order_Product_Asset__r.Product_Name__c,
        (
          SELECT
            Id,
            Charge_Cost_To__c,
            Category__c,
            What_Where__c,
            Service_Product__c,
            Product_Vendor__c,
            Service_Request__c
          FROM Charges__r
        )
      FROM OrderItem
      WHERE OrderId = :orderId
    ];
    Map<String, List<OrderItemWrapper>> groupedMap = new Map<String, List<OrderItemWrapper>>();
    for (OrderItem orderItem : allOrderItems) {
      String key = String.valueof(orderItem.get(groupFieldName));
      groupedValWithUnit.put(
        key,
        orderItem.Sold_Order_Product_Asset__r.Unit_Id__c
      );
      groupedValWithVariantNumber.put(
        key,
        orderItem.Sold_Order_Product_Asset__r.Variant_Number__c
      );
      groupedValWithProductName.put(
        key,
        orderItem.Sold_Order_Product_Asset__r.Product_Name__c
      );
      if ((null == key) || (0 == key.length())) {
        key = 'Undefined';
      }
      List<OrderItemWrapper> groupedOrderItems = groupedMap.get(key);
      if (null == groupedOrderItems) {
        groupedOrderItems = new List<OrderItemWrapper>();
        groupedMap.put(key, groupedOrderItems);
      }
      OrderItemWrapper orderItemWrap = new OrderItemWrapper();
      orderItemWrap.orderItem = orderItem;
      groupedOrderItems.add(orderItemWrap);
      String defectKey = '';
      if (orderItem.Customer_Replace__c) {
        defectKey = 'Customer';
      } else if (orderItem.Local_Remake__c) {
        defectKey = 'Retailer';
      } else if (orderItem.Manufacturer_Replace__c) {
        defectKey = 'Manufacturing';
        orderItemWrap.sourceOfDefect = 'Vendor';
      }
      defectKey = defectKey + orderItem.MTO_Source_Code__c;
      System.debug('@@@defectKey' + defectKey);
      System.debug('@@@@' + selectOptionsMap.containsKey(defectKey));
      if (selectOptionsMap.containsKey(defectKey)) {
        orderItemWrap.defectCodes = selectOptionsMap.get(defectKey);
      }
      System.debug('@@@@ orderItemWrap' + orderItemWrap);
    }

    List<GroupWrapper> groups = new List<GroupWrapper>();
    for (String key : groupedMap.keySet()) {
      GroupWrapper gr = new GroupWrapper();
      groups.add(gr);
      gr.accs = groupedMap.get(key);
      if (groupedValWithProductName.containsKey(key)) {
        gr.productName = groupedValWithProductName.get(key);
      }
      if (groupedValWithUnit.containsKey(key)) {
        if (groupedValWithUnit.get(key) == null) {
          gr.unitId = '';
        } else {
          gr.unitId = groupedValWithUnit.get(key);
        }
      }
      if (groupedValWithVariantNumber.containsKey(key)) {
        if (groupedValWithVariantNumber.get(key) == null) {
          gr.variantNumber = '';
        } else {
          gr.variantNumber = groupedValWithVariantNumber.get(key);
        }
      }

      gr.groupedVal =
        gr.productName +
        ' | Unit Id : ' +
        gr.unitId +
        ' | Variant Number :' +
        gr.variantNumber;
    }
    return groups;
  }

  @AuraEnabled
  public static List<SelectOptionWrapper> getChargeCategories(
    string responsibility
  ) {
    List<SelectOptionWrapper> options = new List<SelectOptionWrapper>();
    Map<String, List<String>> dependentPicklistValuesMap = getDependentPicklistValues(
      Charge__c.Category__c
    );
    system.debug('dependentPicklistValuesMap: ' + dependentPicklistValuesMap);
    if (dependentPicklistValuesMap.containsKey(responsibility)) {
      for (string f : dependentPicklistValuesMap.get(responsibility)) {
        options.add(new SelectOptionWrapper(f, f));
      }
    }
    return options;
  }

  @AuraEnabled
  public static List<SelectOptionWrapper> getWhatWhere(string chargeTo) {
    Map<String, List<String>> dependentPicklistValuesMap = getDependentPicklistValues(
      Charge__c.What_Where__c
    );
    system.debug('dependentPicklistValuesMap: ' + dependentPicklistValuesMap);

    List<SelectOptionWrapper> options = new List<SelectOptionWrapper>();
    if (dependentPicklistValuesMap.containsKey(chargeTo)) {
      for (string f : dependentPicklistValuesMap.get(chargeTo)) {
        options.add(new SelectOptionWrapper(f, f));
      }
    }

    return options;
  }

  @TestVisible
  public class SelectOptionWrapper {
    @AuraEnabled
    public String label { get; set; }
    @AuraEnabled
    public String value { get; set; }
    public SelectOptionWrapper(String label, String value) {
      this.value = value;
      this.label = label;
    }
  }

  public class GroupWrapper {
    @AuraEnabled
    public List<OrderItemWrapper> accs { get; set; }
    @AuraEnabled
    public String groupedVal { get; set; }
    @AuraEnabled
    public String unitId { get; set; }
    @AuraEnabled
    public String variantNumber { get; set; }
    @AuraEnabled
    public String productName { get; set; }
    @AuraEnabled
    public Integer count {
      get {
        return accs.size();
      }
      set;
    }

    public GroupWrapper() {
      accs = new List<OrderItemWrapper>();
    }
  }

  public class OrderItemWrapper {
    @AuraEnabled
    public OrderItem orderItem { get; set; }
    @AuraEnabled
    public List<SelectOptionWrapper> defectCodes { get; set; }
    @AuraEnabled
    public String sourceOfDefect { get; set; }
    public OrderItemWrapper() {
      orderItem = new OrderItem();
      defectCodes = new List<SelectOptionWrapper>();
      sourceOfDefect = 'Source Of Defect';
    }
  }

  @TestVisible
  public class AssetProductWrapper {
    @TestVisible
    @AuraEnabled
    public String assetId { get; set; }
    @AuraEnabled
    public String assetName { get; set; }
    @AuraEnabled
    public String unitId { get; set; }
    @AuraEnabled
    public String displayName { get; set; }
    @AuraEnabled
    public Boolean isDoor { get; set; }
    @AuraEnabled
    public Boolean isChecked { get; set; }
    @AuraEnabled
    public String assetUrl { get; set; }
    @AuraEnabled
    public List<ServiceProductWrapper> serviceProducts { get; set; }
    public AssetProductWrapper() {
      this.serviceProducts = new List<ServiceProductWrapper>();
      this.isChecked = false;
      this.isDoor = false;
      //this.assetUrl = '/'+this.assetId;
    }
  }

  public class ServiceProductWrapper {
    @AuraEnabled
    public Service_Product__c product { get; set; }
    @AuraEnabled
    public String responsibilty { get; set; }
    @AuraEnabled
    public String notes { get; set; }
    @AuraEnabled
    public String rowIdentifier { get; set; }
    @AuraEnabled
    public String mtoSourceCode { get; set; }
    @AuraEnabled
    public String defectCode { get; set; }
    @AuraEnabled
    public Boolean isSelected { get; set; }
    @AuraEnabled
    public List<SelectOptionWrapper> defectCodes { get; set; }
    @AuraEnabled
    public String selectedDefectCode { get; set; }
    @AuraEnabled
    public String selectedSourceOfDefect { get; set; }
    @AuraEnabled
    public String defectDetails { get; set; }
    @AuraEnabled
    public Boolean showApplyall { get; set; }

    public ServiceProductWrapper(
      Service_Product__c product,
      String responsibilty,
      String notes,
      Boolean isSelected,
      String assetId
    ) {
      this.product = product;
      this.isSelected = false;
      this.responsibilty = responsibilty;
      this.notes = notes;
      this.rowIdentifier = product.Id + '#' + assetId;
      this.defectCodes = new List<SelectOptionWrapper>();
      this.showApplyall = false;
    }
  }

  public static Map<String, List<String>> getDependentPicklistValues(
    Schema.sObjectField dependToken
  ) {
    Schema.DescribeFieldResult depend = dependToken.getDescribe();
    Schema.sObjectField controlToken = depend.getController();
    if (controlToken == null) {
      return new Map<String, List<String>>();
    }

    Schema.DescribeFieldResult control = controlToken.getDescribe();
    List<Schema.PicklistEntry> controlEntries;
    if (control.getType() != Schema.DisplayType.Boolean) {
      controlEntries = control.getPicklistValues();
    }

    String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
    Map<String, List<String>> dependentPicklistValues = new Map<String, List<String>>();
    for (Schema.PicklistEntry entry : depend.getPicklistValues()) {
      if (
        entry.isActive() &&
        String.isNotEmpty(
          String.valueOf(
            ((Map<String, Object>) JSON.deserializeUntyped(
                JSON.serialize(entry)
              ))
              .get('validFor')
          )
        )
      ) {
        List<String> base64chars = String.valueOf(
            ((Map<String, Object>) JSON.deserializeUntyped(
                JSON.serialize(entry)
              ))
              .get('validFor')
          )
          .split('');
        for (
          Integer index = 0;
          index < (controlEntries != null ? controlEntries.size() : 2);
          index++
        ) {
          Object controlValue = (controlEntries == null
            ? (Object) (index == 1)
            : (Object) (controlEntries[index].isActive()
                ? controlEntries[index].getLabel()
                : null));
          Integer bitIndex = index / 6;
          if (bitIndex > base64chars.size() - 1) {
            break;
          }
          Integer bitShift = 5 - Math.mod(index, 6);
          if (
            controlValue == null ||
            (base64map.indexOf(base64chars[bitIndex]) & (1 << bitShift)) == 0
          )
            continue;
          if (!dependentPicklistValues.containsKey((String) controlValue)) {
            dependentPicklistValues.put(
              (String) controlValue,
              new List<String>()
            );
          }
          dependentPicklistValues.get((String) controlValue)
            .add(entry.getLabel());
        }
      }
    }
    return dependentPicklistValues;
  }
}