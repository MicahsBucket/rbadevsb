/*
Classes covered by this test class:
- GanttAction_ToggleAppointmentBookmark
*/

@isTest
public class GanttAction_Test {
	private static final String ACCOUNT_NAME = 'RbA Test Dwelling';
    private static final String MUNICIPALITY_NAME = 'Unit Test Municipality';
    private static final String APPLICATION_NOTES = 'Test Notes';
    private static final String MUNICIPALITY_CONTACT_NAME = 'Unit Test Municipality Contact';
    private static final String RESOURCE1_FIRST_NAME = 'Kaylee';
    private static final String RESOURCE1_LAST_NAME = 'Torres';
    private static final String RESOURCE2_FIRST_NAME = 'Harry';
    private static final String RESOURCE2_LAST_NAME = 'Hanson';
    private static final String WORK_TYPE_NAME = 'Test Type 1';
    private static final String INSTALL_WORK_TYPE_NAME = 'Install';
    private static final String MEASURE_WORK_TYPE_NAME = 'Measure';
    private static final String SERVICE_WORK_TYPE_NAME = 'Service';
    private static final String JOB_SITE_VISIT_WORK_TYPE_NAME = 'Job Site Visit';

	@TestSetup
    static void testSetup() {
    	TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Store_Configuration__c storeConfig = createStoreConfig();
        insert storeConfig;

        Back_Office_Checklist_Configuration__c backOfficeChecklist1 = new Back_Office_Checklist_Configuration__c(
            Store_Configuration__c = storeConfig.id,
            Contract_Signed__c = true,
            Lien_Rights_Signed__c = true
            );
        insert backOfficeChecklist1;

        WorkType workType = new WorkType(
            Name = WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 4,
            DurationType = 'Hours'
        );

        WorkType installWorkType = new WorkType(
            Name = INSTALL_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );

        WorkType measureWorkType = new WorkType(
            Name = MEASURE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );

        WorkType serviceWorkType = new WorkType(
            Name = SERVICE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );

        WorkType jobSiteVisitWorkType = new WorkType(
            Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 1,
            DurationType = 'Hours'
        );

        insert new List<WorkType>{workType,installWorkType,measureWorkType,serviceWorkType,jobSiteVisitWorkType};

        Account store = [SELECT Id
                         FROM Account
                         WHERE Id =: storeConfig.Store__c];
        store.Active_Store_Configuration__c = storeConfig.Id;
        update store;

        Contact testContact1 = new Contact();
        testContact1.LastName = 'Test Last Name One';
        insert testContact1;

        User user1 = createUser(RESOURCE1_FIRST_NAME, RESOURCE1_LAST_NAME);
        User user2 = createUser(RESOURCE2_FIRST_NAME, RESOURCE2_LAST_NAME);
        insert new List<User> { user1, user2 };

        ServiceResource resource1 = createServiceResource(user1, storeConfig);
        ServiceResource resource2 = createServiceResource(user2, storeConfig);
        insert new List<ServiceResource> { resource1, resource2 };

        Account acc = new Account();
        id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        acc.Name = ACCOUNT_NAME;
        acc.RecordTypeId = dwellingRT;
        acc.ShippingStreet = '123 Fake street';
        acc.ShippingCity = 'Milwaukee';
        acc.ShippingState = 'Wisconsin';
        acc.ShippingPostalCode = '55555';
        acc.ShippingCountry = 'United States';
        acc.Store_Location__c = storeConfig.Store__c;
        insert acc;

        Order testOrder =  new Order();
        testOrder.Name ='Sold Order 1';
        testOrder.AccountId = acc.Id;
        testOrder.EffectiveDate = Date.Today();
        testOrder.Status = 'Draft';
        testOrder.Pricebook2Id = Test.getStandardPricebookId();
        testOrder.Customer_Pickup_All__c = FALSE;
        testOrder.Installation_Date__c = system.today()-1;
        insert testOrder;
        testOrder = [SELECT Id, AccountId, OwnerId, Type, OpportunityId, BillToContactId FROM Order WHERE Id = :testOrder.Id];

        Municipality__c m1 = new Municipality__c();
        m1.Name = MUNICIPALITY_NAME;
        m1.Application_Notes__c = APPLICATION_NOTES;
        insert m1;

        OperatingHours oh = new OperatingHours();
        oh.Name = 'Test Operating Hours';
        insert oh;

        ServiceTerritory st1 = new ServiceTerritory();
        st1.Name = 'Test Service Territory';
        st1.OperatingHoursId = oh.Id;
        st1.IsActive = true;
        insert st1;

        WorkOrder wo = UtilityMethods.buildWorkOrder(acc, testOrder, Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId(), testOrder.ownerId, 'Service', m1.Id);
        wo.Description = 'test';
        wo.DurationType = 'Hours';
        wo.Duration = 1;
        insert wo;
    }

    @isTest
    static void test_ToggleAppointmentBookmark() {

        WorkOrder wo = [SELECT Id FROM WorkOrder WHERE AccountId IN (SELECT Id From Account WHERE Name = :ACCOUNT_NAME) LIMIT 1];

        ServiceAppointment appt1 = new ServiceAppointment(
        	ParentRecordId = wo.Id,
        	SchedStartTime = System.now().addDays(7),
        	ArrivalWindowStartTime = System.now().addDays(7),
        	ArrivalWindowEndTime = System.now().addDays(7).addHours(2)
        );
        ServiceAppointment appt2 = new ServiceAppointment(
        	ParentRecordId = wo.Id,
        	SchedStartTime = System.now().addDays(7),
        	ArrivalWindowStartTime = System.now().addDays(7),
        	ArrivalWindowEndTime = System.now().addDays(7).addHours(2),
        	FSL__GanttIcon__c = 'www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png',
        	Bookmarked__c = true
        );
        
        Test.startTest();
        insert new List<ServiceAppointment>{appt1, appt2};

        GanttAction_ToggleAppointmentBookmark actionClass = new GanttAction_ToggleAppointmentBookmark();
        
        String result1 = actionClass.action(new List<Id>{appt1.Id}, null, null, null);
        ServiceAppointment apptResult1 = [SELECT FSL__GanttIcon__c FROM ServiceAppointment WHERE Id = :appt1.Id];
        System.assert(result1.contains('Bookmark(s) added for'));
        System.assertNotEquals(null, apptResult1.FSL__GanttIcon__c);

        String result2 = actionClass.action(new List<Id>{appt1.Id}, null, null, null);
        ServiceAppointment apptResult2 = [SELECT FSL__GanttIcon__c FROM ServiceAppointment WHERE Id = :appt1.Id];
        System.assert(result2.contains('Bookmark(s) removed for'));
        System.assertEquals(null, apptResult2.FSL__GanttIcon__c);

        String result3 = actionClass.action(new List<Id>{appt1.Id, appt2.Id}, null, null, null);
        ServiceAppointment apptResult3 = [SELECT FSL__GanttIcon__c FROM ServiceAppointment WHERE Id = :appt1.Id];
        ServiceAppointment apptResult4 = [SELECT FSL__GanttIcon__c FROM ServiceAppointment WHERE Id = :appt2.Id];
        System.assert(result3.contains('Bookmark(s) added for'));
        System.assert(result3.contains('Bookmark(s) removed for'));
        System.assertNotEquals(null, apptResult3.FSL__GanttIcon__c);
        System.assertEquals(null, apptResult4.FSL__GanttIcon__c);
        
        // this needs to come after the asserts, since testing the toggling functionality requires multiple sequential steps with intermediate asserts
        Test.stopTest(); 
    }


    /**************************
    *      SETUP METHODS
    **************************/

    private static User createUser(String firstName, String lastName) {
        User u = new User(
            FirstName = firstName,
            LastName = lastName,
            Email = firstName + '.' + lastName + '@example.com',
            UserName = firstName + '.' + lastName + '@example.com' + (Math.random() * 1000000).intValue(),
            CompanyName = 'ABC Company',
            Alias = 'tuser',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            TimeZoneSidKey = 'America/Chicago',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        return u;
    }

    private static Store_Configuration__c createStoreConfig() {
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008

        );
        insert store;

        Store_Configuration__c storeConfig = new Store_Configuration__c(
            Store__c = store.Id,
            Order_Number__c = 1,
            Tech_Measure_Work_Order_Queue_Id__c = [SELECT Id FROM Group WHERE  Type = 'Queue' AND NAME = 'Tech Measure WO - Twin Cities' LIMIT 1].id
        );

        return storeConfig;
    }

    private static ServiceResource createServiceResource(User u, Store_Configuration__c storeConfig) {
        ServiceResource sr = new ServiceResource(
            Name = u.LastName,
            RelatedRecordId = u.Id,
            Retail_Location__c = storeConfig.Id,
            IsActive = true
        );
        return sr;
    }
}