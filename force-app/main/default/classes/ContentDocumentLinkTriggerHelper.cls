/**
 * @description       : For the Content doc Trigger
 * @author            : mark.rothermal@andersencorp.com
 * @group             : 
 * @last modified on  : 02-23-2021
 * @last modified by  : mark.rothermal@andersencorp.com
 * Modifications Log 
 * @Test Class : ContentDocumentLinkTrigger
 * Ver   Date         Author                            Modification
 * 1.0   02-23-2021   mark.rothermal@andersencorp.com   Initial Version
**/
public with sharing class ContentDocumentLinkTriggerHelper  {

    public static  String errorMessages='';
    public static String fileUploadError='';
    
    // Ensure that the file visibility on all files on WOs, WOLIs, and SAs is set to AllUsers to provide access to community users who have parent record access
    public static void setVisibilityOnFSLFiles(List<ContentDocumentLink> newCDLs) {
        try {
            // Allow errors to be ignored silently, in case quirks of the ContentDocumentLink model cause errors
            // in situations other than the use case of this code.
            for (ContentDocumentLink cdl: newCDLs) {
                if (cdl.LinkedEntityId != null && 
                    (((String)cdl.LinkedEntityId).startsWithIgnoreCase('a1R') // Finance Applications
                     	|| ((String)cdl.LinkedEntityId).startsWithIgnoreCase('0WO') // Work Orders
                        || ((String)cdl.LinkedEntityId).startsWithIgnoreCase('1WL') // Work Order Line Items
                        || ((String)cdl.LinkedEntityId).startsWithIgnoreCase('08p') // Service Appointments
                        || ((String)cdl.LinkedEntityId).startsWithIgnoreCase('006') // Opportunities
                        || ((String)cdl.LinkedEntityId).startsWithIgnoreCase('003') // Contacts
                        || ((String)cdl.LinkedEntityId).startsWithIgnoreCase('001') // Accounts
                        || ((String)cdl.LinkedEntityId).startsWithIgnoreCase('801'))) // Order
                {
                    cdl.Visibility = 'AllUsers'; // "Available to all users with permission to see the file"
                }
            }
        } catch (Exception ex) {
            // Do nothing
            System.debug(LoggingLevel.INFO,'Error occurred while attempting to set ContentDocumentLink visibility.  Error was handled silently.');
        }
    }


    /**
* @description 
* @param cdl ContentDocumentLink List from Trigger.new
*/ 
    
    public static void isAfterInsert(List<ContentDocumentLink> cdl) {
        System.debug('in after insert for ContentDocumentLinkTriggerHelper');
        for(ContentDocumentLink docLink : cdl) {
            String myIdPrefix = UtilityMethods.findObjectNameFromRecordIdPrefix(docLink.LinkedEntityId); //get the object type name using the prefix in a lookup of the global describe table.
            System.debug(myIdPrefix);
            
            //what object is the files object being shared to?
            switch on myIdPrefix { 
                when 'Opportunity' {
                    //opportunity filter logic here
                    System.debug('uploading to opportunity from the ContentDocumentLinkTriggerHelper');
                    handleFileOpportunity(docLink);
                    continue;
                }
                when 'Order' {
                    //order filter logic here
                }
            }
            
            
            /*
Reference Material:  https://developer.salesforce.com/forums/?id=9060G0000005XgBQAU
*/
        }
    }
    
    /**
* @description 
* @param cdl the ContentDocumentLink contains the related object sharing record.
*/ 
@TestVisible 
    private static string handleFileOpportunity(ContentDocumentLink cdl) {
      ContentVersion cv;
        try{
            String myMsg;
            System.debug(cdl);
            System.debug([SELECT Id, VersionData from ContentVersion where Id =: cdl.ContentDocumentId limit 1]);
            //use cv.VersionData to get the contents of the file.  this is in base64 format.
             cv = [SELECT Id, VersionData,FileExtension,Title from ContentVersion where ContentDocumentId =: cdl.ContentDocumentId limit 1];
            opportunity opp =[SELECT stageName, rSuite_Id__c from Opportunity where id=:cdl.LinkedEntityId limit 1];
            system.debug('the title is'+opp.id);
            if((opp!=null&&opp.StageName=='Sold')&&(cv.FileExtension=='json'||cv.FileExtension=='rbaproj'))
            {
                fileUploadError=' rSuite files are only allowed to uploaded to Unsold Opportunities';
                return null;
            }
            else
            {
                List<Attachment> attachList = [select Id, Name, ParentId, Body from Attachment where ParentId = :cdl.LinkedEntityId ];
                
                for(Attachment a : attachList) {
                    if(a.Name.toLowerCase().endsWith('.rbaproj')) {
                        if(cv.FileExtension=='json')
                        {
                            fileUploadError='Your Opportunity already has an rSuite .proj file uploaded. Please either process that file or remove it and attach the new rSuite file';
                            return null;
                        }
                        
                    } 
                }
                if(cv.FileExtension=='json') {
                    ProjectFileHelper.saveProject(cv.VersionData.toString(), cdl.LinkedEntityId);
                    ErrorLog__c errorLog = new ErrorLog__c();
                    errorLog.ErrorMessage__c=errorMessages;
                    errorLog.RelatedTo__c=cdl.LinkedEntityId;
                    errorLog.FileName__c=cv.Title;
                    upsert errorLog FileName__c;
                } else {
                    // Proj file Logic to handle both proj and josn through upload files button
                    if(cv.FileExtension=='rbaproj') {   
                        String errorMessagesList ='';
                        String warningMessages = '';
                        RSuiteFileHandler.projfile=cv.VersionData.toString();
                        if (String.isNotBlank(cdl.LinkedEntityId)){
                            RSuiteFileHandler.FileUploadResultDTO fr = new RSuiteFileHandler.FileUploadResultDTO();
                            fr = RSuiteFileHandler.uploadProjectFile( cdl.LinkedEntityId );
                            system.debug('the messages no '+fr.messages);
                            if (fr.messages != null){
                                set<string> removeDuplicateMessages = new set<string>();
                                removeDuplicateMessages.addAll(fr.messages);
                                for (String message:removeDuplicateMessages ) {
                                    if(!message.contains('Field with Name')){
                                        warningMessages=warningMessages+message+'\r\n';
                                    }
                                    system.debug('the messages exact '+message);
                                }
                            }
                            system.debug('the messages warnings '+warningMessages);
                            if (fr.errors != null){
                                for (String error: fr.errors) {
                                    errorMessagesList=errorMessagesList+error+'\r\n';
                                }
                            }
                            if (fr.orderId != null){
                                Id projFileOrderId = fr.orderId;
                                myMsg  ='Order Created Successfully: '+projFileOrderId;
                                warningMessages=warningMessages+myMsg+'\r\n';
                            } else {
                                myMsg =  'No Order created';
                                errorMessagesList=errorMessagesList+myMsg+'\r\n';
                            } 
                        } else {
                            myMsg ='No Opportunity Id';
                            errorMessagesList=errorMessagesList+MyMsg+'\r\n';
                        }
                        
                        ErrorLog__c errorLog = new ErrorLog__c();
                        errorLog.ErrorMessage__c=errorMessagesList;
                        errorLog.RelatedTo__c=cdl.LinkedEntityId;
                        errorlog.WarningMessage__c=warningMessages;
                        errorLog.FileName__c=cv.Title;
                        upsert errorLog FileName__c;
                    }
                }
            }
            return null;
        }
        catch (Exception ex){
            string exceptionFileSize='The File Size is too large please contact rsupport'+'\r\n';
            ErrorLog__c errorLog = new ErrorLog__c();
            errorLog.ErrorMessage__c=null;
            errorLog.RelatedTo__c=cdl.LinkedEntityId;
            if( ex.getmessage()=='String length exceeds maximum: 6000000'){
                errorlog.WarningMessage__c=  exceptionFileSize; 
            } else {
                errorlog.WarningMessage__c=ex.getmessage()+'\r\n';    
            }            
            errorLog.FileName__c=cv.Title;
            upsert errorLog FileName__c;
            return null;
        }
    }
}