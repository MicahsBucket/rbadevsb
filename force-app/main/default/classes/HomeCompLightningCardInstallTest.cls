/*
* @author Jason Flippen
* @date 11/17/2020
* @description Test Class for the following Classes:
*              - HomeCompLightningCardInstallScheduling
*/ 
@isTest
public class HomeCompLightningCardInstallTest {

    @testSetup static void setupData() {
 
        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        Account testVenderAcct = testUtility.createVendorAccount('Test Vendor Account');
        insert testVenderAcct;

        Account testStoreAcct = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAcct = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAcct.Store_Location__c = testStoreAcct.Id;
        insert testDwellingAcct;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAcct.Id, 'Closed - Won');
        insert testOpportunity;
    
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;

        List<Product2> testProductList = new List<Product2>();
        // Parent Product
        Product2 testParentProduct = new Product2(Name = 'Test Parent Product',
                                                  RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId(),
                                                  Vendor__c = testVenderAcct.Id,
                                                  Cost_PO__c = true,
                                                  IsActive = true,
                                                  Account_Number__c =  testFan.Id);
        testProductList.add(testParentProduct);
        // Child Product
 
        insert testProductList;

        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        // Parent PricebookEntry
        PricebookEntry testParentPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testParentProduct.Id);
        testPBEList.add(testParentPBEStandard);
        PricebookEntry testParentPBE = testUtility.createPricebookEntry(testPricebook.Id, testParentProduct.Id);
        testPBEList.add(testParentPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order', 
                                     AccountId = testDwellingAcct.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAcct.Id,
                                     OpportunityId = testOpportunity.Id,                                 
                                     Status = 'Install Needed',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;

        Order testOrder1 =  new Order(Name = 'Test Order1', 
                                     AccountId = testDwellingAcct.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAcct.Id,
                                     OpportunityId = testOpportunity.Id,                                 
                                     Status = 'Install Scheduled',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder1;

        Date nextDay = Date.Today().addDays(1);

        Order testOrder2 =  new Order(Name = 'Test Order2',
                                      AccountId = testDwellingAcct.Id,
                                      EffectiveDate = Date.Today(),
                                      Store_Location__c = testStoreAcct.Id,
                                      OpportunityId = testOpportunity.Id,
                                      Status = 'Install Scheduled',
                                      Tech_Measure_Status__c = 'New',
                                      Installation_Date__c = Date.Today(),
                                      Estimated_Ship_Date__c = nextDay,
                                      Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder2;

        OperatingHours oh1 = new OperatingHours();
        oh1.Name = 'Test Operating Hours One';
        oh1.TimeZone = 'America/Chicago';
        insert oh1;
        
        ServiceTerritory st1 = new ServiceTerritory();
        st1.Name = 'Test Service Territory One';
        st1.OperatingHoursId = oh1.Id;
        st1.IsActive = true;
        insert st1;
        
        WorkType measureWorkType = new WorkType(Name = 'Measure',
                                                MinimumCrewSize = 1,
                                                RecommendedCrewSize = 2,
                                                EstimatedDuration = 8,
                                                DurationType = 'Hours');
        insert measureWorkType;
        
        WorkOrder measureWO = new WorkOrder(Recommended_Crew_Size__c = 2,
                                            Street = '219 North Milwaukee Street',
                                            City = 'Milwaukee',
                                            State = 'Wisconsin',
                                            PostalCode = '53208',
                                            CountryCode = 'US',
                                            WorkTypeId = measureWorkType.Id,
                                            ServiceTerritoryId = st1.Id,
                                            Status = 'Scheduled & Assigned',
                                            Confirmed_Appt_w_Customer__c = null,
                                            RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Tech_Measure').getRecordTypeId());
        insert measureWO;
        
    }

    /*
    * @author Jason Flippen
    * @date 11/17/2020
    * @description Method to test the functionality in the getInstallNeededOrderList method.
    */
    static testMethod void testGetInstallNeededOrderList() {

        Test.startTest();

            List<Order> testOrderList = [SELECT Id, OrderNumber FROM Order WHERE Status = 'Install Needed'];

            if (!testOrderList.isEmpty()) {
                List<HomeCompLightningCardInstallScheduling.InstallNeededOrderWrapper> orderList = HomeCompLightningCardInstallScheduling.getInstallNeededOrderList(testOrderList[0].OrderNumber);
                System.assertEquals(1,orderList.size());
            }

        Test.stopTest();

    }

    /*
    * @author Jason Flippen
    * @date 11/17/2020
    * @description Method to test the functionality in the getInstallScheduledOrderList method.
    */
    static testMethod void testGetInstallScheduledOrderList() {

        Test.startTest();

            List<Order> testOrderList = [SELECT Id, OrderNumber FROM Order WHERE Status = 'Install Scheduled' AND Installation_Date__c = null];

            if (!testOrderList.isEmpty()) {
                List<HomeCompLightningCardInstallScheduling.InstallScheduledOrderWrapper> orderList = HomeCompLightningCardInstallScheduling.getInstallScheduledOrderList(testOrderList[0].OrderNumber);
                System.assertEquals(1,orderList.size());
            }

        Test.stopTest();

    }

    /*
    * @author Jason Flippen
    * @date 11/17/2020
    * @description Method to test the functionality in the getInstallConflictOrderList method.
    */
    static testMethod void testGetInstallConflictOrderList() {

        Test.startTest();

            List<Order> testOrderList = [SELECT Id, OrderNumber FROM Order WHERE Status = 'Install Scheduled' AND Install_Ship_Date_Conflict__c = true];

            if (!testOrderList.isEmpty()) {
                List<HomeCompLightningCardInstallScheduling.InstallConflictOrderWrapper> orderList = HomeCompLightningCardInstallScheduling.getInstallConflictOrderList(testOrderList[0].OrderNumber);
                System.assertEquals(1,orderList.size());
            }

        Test.stopTest();

    }
    
    /*
    * @author Jason Flippen
    * @date 11/17/2020
    * @description Method to test the functionality in the getUnconfirmedWorkOrderList method.
    */
    static testMethod void testGetUnconfirmedWorkOrderList() {

        Test.startTest();

            List<WorkOrder> testWorkOrderList = [SELECT Id, WorkOrderNumber FROM WorkOrder WHERE Confirmed_Appt_w_Customer__c = null];

            if (!testWorkOrderList.isEmpty()) {
                List<HomeCompLightningCardInstallScheduling.UnconfirmedWorkOrderWrapper> workOrderList = HomeCompLightningCardInstallScheduling.getUnconfirmedWorkOrderList(testWorkOrderList[0].WorkOrderNumber);
                System.assertEquals(1,workOrderList.size());
            }

        Test.stopTest();

    }

}