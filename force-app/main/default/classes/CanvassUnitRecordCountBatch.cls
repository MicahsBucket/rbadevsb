global class CanvassUnitRecordCountBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable 
{
    //database.executebatch(new CanvassUnitRecordCountBatch(),1);
    
    //CanvassUnitRecordCountBatch rsb = new CanvassUnitRecordCountBatch();
    //String sch = '0 0 * * * ?';
    //system.schedule('Canvass Unit Record Count Batch 00', sch, rsb);
    
    //CanvassUnitRecordCountBatch rsb = new CanvassUnitRecordCountBatch();
    //String sch = '0 15 * * * ?';
    //system.schedule('Canvass Unit Record Count Batch 15', sch, rsb);
    
    //CanvassUnitRecordCountBatch rsb = new CanvassUnitRecordCountBatch();
    //String sch = '0 30 * * * ?';
    //system.schedule('Canvass Unit Record Count Batch 30', sch, rsb);
    
    //CanvassUnitRecordCountBatch rsb = new CanvassUnitRecordCountBatch();
    //String sch = '0 45 * * * ?';
    //system.schedule('Canvass Unit Record Count Batch 45', sch, rsb);
    
    global CanvassUnitRecordCountBatch()
    {

    }
    
    global void execute(SchedulableContext SC)
    {
        database.executeBatch(new CanvassUnitRecordCountBatch(),1);
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        // query to get holding jobs from queue
        return Database.getQueryLocator ([SELECT Id, Name, Market_Id__c, Status__c, ZipCode__c 
                                          FROM Custom_Data_Layer_Batch_Queue__c 
                                          WHERE Status__c = 'Waiting For Record Count'
                                          OR Status__c = 'NA' ]);
    }
    
    global void execute (Database.BatchableContext BC, List<Custom_Data_Layer_Batch_Queue__c> scope)
    {
        try 
        {
            List<Canvass_Unit_Batch_Fields__c> apexBatchJobLimitList = [SELECT Id, Name, Apex_Batch_Job_Limit__c FROM Canvass_Unit_Batch_Fields__c];
            
            if(apexBatchJobLimitList.size() > 0)
			{
                // query to get currently running jobs
                list<AsyncApexJob> listOfRunningJobs = [SELECT Status, NumberOfErrors, ApexClassID, CompletedDate, ExtendedStatus, JobItemsProcessed, JobType, MethodName, TotalJobItems 
                                                        FROM AsyncApexJob 
                                                        WHERE Status != 'Completed' AND Status != 'Aborted' AND Status != 'Failed'];
                // if running jobs less than  50
                if(listOfRunningJobs.size() < apexBatchJobLimitList[0].Apex_Batch_Job_Limit__c || Test.isRunningTest() == true)
                {
                    // iterate over scope
                    for(Custom_Data_Layer_Batch_Queue__c queuedJob :scope)
                    {
                        if(queuedJob.Market_Id__c != null && queuedJob.ZipCode__c != null && queuedJob.Id != null)
                        {
                            if (!Test.isRunningTest())
                            {
                                System.enqueueJob(new CanvassUnitRecordCountQueueablePart1(queuedJob.Market_Id__c,queuedJob.ZipCode__c,queuedJob.Id));
                            }
                        
                            // update status so that job won't be queued again
                            queuedJob.Status__c = 'Counting Records';
                            update queuedJob;
                        }
                    }
                }
            }
        }
        catch(Exception e)
        {   for(Custom_Data_Layer_Batch_Queue__c queuedJob :scope) {
                // update status so that job won't be queued again
                queuedJob.Status__c = 'Failed';
            }
            update scope;
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {

    }
}