/**
* @author Jason Flippen
* @date 02/11/2020 
* @description Class to provide functionality for the newServicePurchaseOrder LWC.
*              Test code coverage provided by the following Test Class:
*			   - NewServicePurchaseOrderControllerTest
**/
public with sharing class NewServicePurchaseOrderController {

    /**
    * @author Jason Flippen
    * @date 02/11/2020
    * @description Method to return Service Request Products.
    * @param Service Request (Order) Id
    * @returns Data wrapped in a Class specific to the Service Request Products
    **/
    @AuraEnabled(cacheable=true)
    public static List<ChargeWrapper> getCharges(Id serviceRequestId) {

        List<ChargeWrapper> wrapperList = new List<ChargeWrapper>();

        // Retrieve data from the Service Request record.
        for (Charge__c c : [SELECT Id,
                                   Service_Request__c,
                                   Service_Product__r.Product_Name__c,
                                   Service_Product__c,
                                   Service_Product__r.Purchase_Order__c,
                                   Installed_Product_Id__c,
                                   Charge_Cost_To__c,
                                   Installed_Product__c,
                                   Variant_Number__c,
                                   Category__c,
                                   What_Where__c,
                                   Service_Product__r.Purchase_Order__r.Vendor__c,
                                   Service_Product__r.Order.Store_Location__c,
                                   Service_Product__r.Purchase_Order__r.Charge_Cost_To__c,
                                   Service_Product__r.Sold_Order_Product_Asset__r.Original_Order_Product__c,
                                   Service_Product__r.Purchase_Order__r.Status__c,
                                   Service_Product__r.Status__c
                            FROM   Charge__c
                            WHERE  Service_Request__c = :serviceRequestId
                            AND    Service_Product__c != null
                            AND    Service_Product__r.Status__c != 'Cancelled']) {

/*
            boolean poExists = false;
            boolean isChecked = false;

            if (c.Service_Product__r.Purchase_Order__c != null) {
                poExists = true;
                if (c.Service_Product__r.Purchase_Order__r.Status__c == 'In Process' )
                    chargeVendorExistingPOMap.put(c.Service_Product__r.Purchase_Order__r.Charge_Cost_To__c + c.Service_Product__r.Purchase_Order__r.Vendor__c, c.Service_Product__r.Purchase_Order__c);
                if (c.Charge_Cost_To__c == c.Service_Product__r.Purchase_Order__r.Charge_Cost_To__c)
                    isChecked = true;
            }
            wrapperList.add(new chargeWrapper(isChecked, poExists, c));
*/
        }

        // Return the List of Wrapper classes.
        return wrapperList;

    } // End Method: getCharges()


/** Wrapper Class **/


    /**
    * @author Jason Flippen
    * @date 02/11/2020
    * @description Wrapper Class for a Charge.
    * @param N/A
    * @returns N/A
    **/
    public class ChargeWrapper {

        public boolean isSelected {get; set;}
        public boolean isDisabled {get; set;}
        public Charge__c charge {get; set;}
    }

}