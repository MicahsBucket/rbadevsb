// Code coverage provided by: GanttAction_ViewWeatherCntrlrTest

public without sharing class GanttAction_ViewWeatherController {
	
	public ServiceAppointment sa {get;set;}
	public String saId {get;set;}
	public String lat {
		get{
			return String.valueOf(sa.Latitude).left(7);
		}
	}
	public String lng {
		get{
			return String.valueOf(sa.Longitude).left(7);
		}
	}
	public String address {
		get {
			return String.format('{0}, {1}, {2}', new List<String>{sa.Street, sa.City, sa.State});
		}
	}


	public GanttAction_ViewWeatherController() {
		system.debug(ApexPages.currentPage().getParameters());
		saId = ApexPages.currentPage().getParameters().get('id');
		system.debug('**** SA ID: ' + saId);
		sa = [SELECT FSL__InJeopardy__c, FSL__InJeopardyReason__c, Weather_Timestamp__c, Weather_Forecast_Warnings__c, Weather_Forecast_JSON__c, Latitude, Longitude, Street, City, State
			FROM ServiceAppointment WHERE Id = :saId];

	}

	@AuraEnabled(cacheable=false)
	public static string updateForecast(Id saId) {
		//try {
			String query = 'SELECT PostalCode,Id,Latitude,Longitude,SchedStartTime,SchedEndTime,Weather_Timestamp__c,FSL__InJeopardy__c,FSL__InJeopardyReason__c,'
        	+ 'ServiceTerritoryId,FSL__IsMultiDay__c,FSL__MDT_Operational_Time__c,Work_Order__r.RecordType.Name,ServiceTerritory.ParentTerritoryId, Weather_Forecast_Warnings__c ' 
			+ 'FROM ServiceAppointment WHERE Id = :saId';

			List<sObject> appointments = Database.query(query); 

			system.debug('***** apps: ' + appointments);

			AerisWeatherForecastsHelper.updateForecasts(appointments);
			UtilityMethods.disableAllFslTriggers();
			update appointments;

			return 'Success';
		// } catch (Exception ex) {
		// 	return 'Unhandled occurred during attempt to update weather forecast.';
		// }
	}
}