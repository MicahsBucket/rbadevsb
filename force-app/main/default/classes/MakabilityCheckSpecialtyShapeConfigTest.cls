/**
 * @File Name          : MakabilityCheckSpecialtyShapeConfigTest.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 5/4/2019, 7:56:54 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/4/2019, 7:56:54 PM   mark.rothermal@andersencorp.com     Initial Version
**/
@isTest
public  class MakabilityCheckSpecialtyShapeConfigTest {

    @isTest
    public static void failingWithTwoLegsTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        Size_Detail_Configuration__c sdc = createTestSizeDetailConfig(pc);
        list<Product_Field_Control__c> pfcs = new list <Product_Field_Control__c>();
        list<Product_Field_Control_Dependency__c> pfcds = new list <Product_Field_Control_Dependency__c>();
        Field_Control_List__c fclRleg = createFieldControlLists('Right Leg Inches');
        Field_Control_List__c fclLleg = createFieldControlLists('Left Leg Inches');
        Field_Control_List__c fclShape = createFieldControlLists('Specialty Shape');
        Product_Field_Control__c pfcShape = createProductFieldControl(pc,fclShape);
        Product_Field_Control__c pfcRleg = createProductFieldControl(pc,fclRleg);
        pfcs.add(pfcRleg);
        Product_Field_Control__c pfcLleg = createProductFieldControl(pc,fclLleg);
        pfcs.add(pfcLleg);
        Product_Field_Control_Dependency__c rightLegPfcd = createProductFieldControlDependencies('Chord',pfcShape,pfcRleg); 
        pfcds.add(rightLegPfcd);       
        Product_Field_Control_Dependency__c leftLegPFcd = createProductFieldControlDependencies('Chord',pfcShape,pfcLleg);  
        pfcds.add(leftLegPFcd);
        MakabilityRestResource.OrderItem configToTest = createOrderItem(pc.Product__c,50,50,true,'1');
        Map<String,Product_Field_Control__c> pfcMap = createPfcMap(pc.Product__c,pfcs);       
        Map<String,Product_Field_Control_Dependency__c> pfcdMap = createPfcdMap(configToTest,pfcds);       
        test.startTest();
 	    MakabilityCheckSpecialtyShapeConfig.specialShapeResult results = MakabilityCheckSpecialtyShapeConfig.checkShape(configToTest,sdc,pfcMap,pfcdMap);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
    //    string assertMsg = results[0].errorMessages[0];
    //    system.assertEquals(assertMsg, 'Grille Config  - passed');
    }
    @isTest
    public static void failingWithOneLegTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        Size_Detail_Configuration__c sdc = createTestSizeDetailConfig(pc);
        list<Product_Field_Control__c> pfcs = new list <Product_Field_Control__c>();
        list<Product_Field_Control_Dependency__c> pfcds = new list <Product_Field_Control_Dependency__c>();
        Field_Control_List__c fclRleg = createFieldControlLists('Right Leg Inches');
        Field_Control_List__c fclLleg = createFieldControlLists('Left Leg Inches');
        Field_Control_List__c fclShape = createFieldControlLists('Specialty Shape');
        Product_Field_Control__c pfcShape = createProductFieldControl(pc,fclShape);
        Product_Field_Control__c pfcRleg = createProductFieldControl(pc,fclRleg);
        pfcs.add(pfcRleg);
        Product_Field_Control__c pfcLleg = createProductFieldControl(pc,fclLleg);
        pfcs.add(pfcLleg);
        Product_Field_Control_Dependency__c rightLegPfcd = createProductFieldControlDependencies('Chord',pfcShape,pfcRleg); 
        pfcds.add(rightLegPfcd);       
        Product_Field_Control_Dependency__c leftLegPFcd = createProductFieldControlDependencies('Chord',pfcShape,pfcLleg);  
        pfcds.add(leftLegPFcd);
        MakabilityRestResource.OrderItem configToTest = createOrderItem(pc.Product__c,50,50,false,'1');
        Map<String,Product_Field_Control__c> pfcMap = createPfcMap(pc.Product__c,pfcs);       
        Map<String,Product_Field_Control_Dependency__c> pfcdMap = createPfcdMap(configToTest,pfcds);       
        test.startTest();
 	    MakabilityCheckSpecialtyShapeConfig.specialShapeResult results = MakabilityCheckSpecialtyShapeConfig.checkShape(configToTest,sdc,pfcMap,pfcdMap);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
    //    string assertMsg = results[0].errorMessages[0];
    //    system.assertEquals(assertMsg, 'Grille Config  - passed');
    }

         
    private static List<Product2> createTestProduct(Integer num){
        List<Product2> pros = new List<Product2>();
        for(Integer i = 0; i < num;i++){
            Product2 p = new Product2();
            p.name = 'test product ' + i;
            pros.add(p);            
        }        
        insert pros;
        return pros;
    }
    
    private static List<Product_Configuration__c> createTestProConfig(Integer num){
        List<Product2> pros = createTestProduct(num);
        List<Product_Configuration__c> pcs = new List<Product_Configuration__c>();
        for(Integer i = 0 ; i < num; i++){
            Product_Configuration__c pc = new Product_Configuration__c();
            pc.name = 'test config ' + i;
            pc.Tempered_Glass_Required_at_UI__c = 100;	
            pc.Product__c = pros[i].id;  
            pcs.add(pc);
        }
        insert pcs;
        return pcs;
    }

    private static Field_Control_List__c createFieldControlLists(String fieldName){
        Field_Control_List__c fcl = new Field_Control_List__c();
        fcl.Name = fieldName;
        fcl.rForce__c = true;
        fcl.Sales__c = true;
        fcl.Tech__c = true;
        insert fcl;
        return fcl;
    }    

    private static Product_Field_Control__c createProductFieldControl(Product_Configuration__c pc, Field_Control_List__c fcl ){
        Product_Field_Control__c pfc = new Product_Field_Control__c();
        pfc.Name = fcl.Name;
        pfc.Field_Control_ID__c = fcl.id;
        pfc.Product_Configuration_ID__c = pc.id;
        pfc.Required__c = true;
        insert pfc;
        return pfc;
    }

    private static map<String,Product_Field_Control__c> createPfcMap(Id prodId,list<Product_Field_Control__c> pfcs){
        map<String,Product_Field_Control__c> result = new map<String,Product_Field_Control__c>();
        for(Product_Field_Control__c pfc : pfcs){
        String key = prodId + '-' + pfc.Name;
        result.put(key,pfc);
        }
        return result;
    }
    private static map<String,Product_Field_Control_Dependency__c> createPfcdMap(MakabilityRestResource.OrderItem oi, List<Product_Field_Control_Dependency__c> pfcds){
        map<String,Product_Field_Control_Dependency__c> result = new map<String,Product_Field_Control_Dependency__c>();
        //uni key p.productId + '-' + 'Left Leg Inches' + '-' + p.specialtyShape
        for(Product_Field_Control_Dependency__c pfcd : pfcds){
            String key = oi.ProductConfiguration.productId + '-' + pfcd.Controlling_Field__r.Field_Control_ID__r.Name + '-' + oi.ProductConfiguration.specialtyShape;
            result.put(key,pfcd);
        }

        return result;
    }
    private static Product_Field_Control_Dependency__c createProductFieldControlDependencies(String controllingValue, Product_Field_Control__c controllingField, Product_Field_Control__c dependentField ){
        Product_Field_Control_Dependency__c pfcd = new Product_Field_Control_Dependency__c();
        pfcd.Action_Taken__c = null;
        pfcd.Make_Required__c = true;
        pfcd.Controlling_Field__c = controllingField.id;
        pfcd.Controlling_Value__c = controllingValue;
        pfcd.Dependent_Field__c = dependentField.id;
        insert pfcd;
        return pfcd;
        
    }
    private static map<String,Product_Field_Control__c> createPfcMap(Id prodId,Product_Field_Control__c pfc){
        map<String,Product_Field_Control__c> result = new map<String,Product_Field_Control__c>();
        String key = prodId + '-' + pfc.Name;
        result.put(key,pfc);
        return result;
    }
    private static void mockUpProductFieldControls(Product_Configuration__c pc){
      List <Field_Control_List__c> fclList = new List<Field_Control_List__c>();
      List <Product_Field_Control__c> pfcList = new List<Product_Field_Control__c>();
      List <Product_Field_Control_Dependency__c> pfcdList = new List <Product_Field_Control_Dependency__c>();
      Product_Field_Control__c specialShape;
      Product_Field_Control__c rightLeg;
      Product_Field_Control__c leftLeg; 
      List <String> fields = new List <String>{'Right Leg Inches', 
                                               'Left Leg Inches', 
                                               'Locks/Sash', 
                                               'Specialty Shape'};

      for(String f:fields ){
          Field_Control_List__c fcl = createFieldControlLists(f);
          fclList.add(fcl);
      }

      insert fclList;
      
      for(Field_Control_List__c fc : fclList){
         Product_Field_Control__c pfc =  createProductFieldControl(pc,fc);
         pfcList.add(pfc);
      }

      insert pfcList;

      for(Product_Field_Control__c pfc : pfcList){
          if(pfc.Field_Control_ID__r.Name == 'Specialty Shape'){
              specialShape = pfc;
          }
          if(pfc.Field_Control_ID__r.Name == 'Right Leg Inches'){
              rightLeg = pfc;
          }
          if(pfc.Field_Control_ID__r.Name == 'Left Leg Inches'){
              leftLeg = pfc;
          }
      }
      createProductFieldControlDependencies('Chord',specialShape, rightLeg);
      createProductFieldControlDependencies('Chord',specialShape, leftLeg);

    }                         
    
    private static Size_Detail_Configuration__c createTestSizeDetailConfig(Product_Configuration__c pc){
        Size_Detail_Configuration__c sdc = new Size_Detail_Configuration__c();  
            sdc.Product_Configuration__c = pc.id;
            sdc.Performance_Category__c = 'DP Upgrade';
            sdc.Extended_Max_Height_Inches__c = 80;
            sdc.Extended_Max_Height_Fraction__c = 'Even';
            sdc.Extended_Max_Width_Inches__c = 80;
            sdc.Extended_Max_Width_Fraction__c = 'Even';
            sdc.Lock_Max_Width_Inches__c = 15;
            sdc.Lock_Max_Height_Inches__c = 15;
            sdc.Lock_Min_Width_Inches__c = 25;
            sdc.Lock_Min_Height_Inches__c = 25;
            sdc.Lock_Max_Width_Fraction__c = 'Even';
            sdc.Lock_Min_Width_Fraction__c = 'Even';
            sdc.Lock_Max_Height_Fraction__c = 'Even';
            sdc.Lock_Min_Height_Fraction__c = 'Even';
            sdc.Lock_Min_Width_Locks__c = '1';
            sdc.Lock_Max_Width_Locks__c = '1';
            sdc.Lock_Max_Height_Locks__c = '1';
            sdc.Lock_Min_Height_Locks__c = '1';
            sdc.Peak_Height_Min__c = 1 ;
            sdc.Peak_Height_Max__c = 2 ;
            sdc.Glass_Square_Foot_Max__c = 40 ;
            sdc.Width_to_Height_Minimum__c = 1 ;
            sdc.Width_to_Height_Maximum__c = 2 ;
            sdc.Match_Leg_Heights__c = true ;
            sdc.Min_Leg_Height_Inches__c = 55;
            sdc.Min_Leg_Height_Fraction__c = 'Even';
            sdc.Max_Leg_Height_Inches__c = 5;
            sdc.Max_Leg_Height_Fraction__c = 'Even';

            sdc.Positive_Force__c = 50;
            sdc.Frame_Type__c = 'Flat Sill';
            sdc.Hardware_Options__c = 'Normal Hinge';
            sdc.Negative_Force__c = 50;
            sdc.Max_Height_Inches__c = 40;
            sdc.Max_Height_Fraction__c = 'Even';
            sdc.Max_Width_Inches__c = 40;
            sdc.Max_Width_Fraction__c = 'Even';
            sdc.Min_Height_Inches__c = 40;
            sdc.Min_Height_Fraction__c = 'Even';        
            sdc.Min_Width_Inches__c = 40;
            sdc.Min_Width_Fraction__c = 'Even';
            sdc.Sash_Operation__c = 'Right';
            sdc.Sash_Ratio__c = '1:1';
            sdc.Specialty_Shape__c = 'Chord';
            sdc.United_Inch_Maximum__c = 120;
            sdc.United_Inch_Minimum__c = 30;
            insert sdc;
            return sdc;
    }

    private static MakabilityRestResource.OrderItem  createOrderItem(Id prodId, Decimal wInch, Decimal hInch,Boolean hasRightLeg, String oiId){
        Map<string,MakabilityRestResource.OrderItem> orderItems = new Map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
        MakabilityCalculator mc = new MakabilityCalculator();
		ProductConfiguration pc = new ProductConfiguration();
        pc.widthInches = 60;
        pc.widthFractions = 'Even';
        pc.heightInches = 60;
        pc.heightFractions = 'Even';        
        pc.frame = 'Flat Sill';
        pc.hardwareOption = 'Normal Hinge';
        pc.sashOperation = 'Right';
        pc.sashRatio = '1:1';
        pc.leftLegInches = wInch;
        pc.leftLegFraction = 'Even';
        pc.rightLegInches = hInch;
        pc.rightLegFraction = 'Even';
        pc.specialtyShape = 'Chord';
        mc.checkSpecialShape = true;
        mc.hasLeftLeg = true;
        mc.hasRightLeg = hasRightLeg;
        /////////////////////       
        pc.productId = prodId; 
        oi.orderItemId = oiId;
        oi.makabilityCalculator = mc;
        oi.productConfiguration = pc;
        return oi;
    }

}