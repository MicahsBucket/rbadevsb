/* Copyright © 2016-2018 7Summits, Inc. All rights reserved. */
@isTest
public class Peak_WelcomeMessageControllerTest {
    @isTest
    public static void testGetLoginCount() {
        Peak_Response testResponse = new Peak_Response();
        testResponse = Peak_WelcomeMessageController.getLoginCount();
        System.assert(testResponse.peakResults[0].count > 0);
    }

}