@isTest
private class SalesSchedStoreManagerCtrlTest {

    static testmethod void testGetMySalesTeam(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        
        List<User> team = null;
        System.runAs(sstu.managers.get(0)){
            team = SalesSchedStoreManagerCtrl.getMySalesTeam('All');
        }
        System.assert(!team.isEmpty());
    }
    
    static testmethod void testGetSlots(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        
        List<SalesSchedStoreManagerCtrl.SlotDay> slots1 = null;
        List<SalesSchedStoreManagerCtrl.SlotDay> slots2 = null;
        System.runAs(sstu.managers.get(0)){
            slots1 = SalesSchedStoreManagerCtrl.getSlots('Three Slots');
            slots2 = SalesSchedStoreManagerCtrl.getSlots('Five Slots');
        }
        System.assert(!slots1.isEmpty());
        System.assert(!slots2.isEmpty());
    }
    
    static testmethod void testGetgetAppointmentsByResult(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
        Sales_Appointment__c lstTemp = [select id, Sales_Order__r.Result__c, Appointment_Date__c, store__c from Sales_Appointment__c limit 1];
        
        List <Sales_Appointment__c> lstSA = SalesSchedStoreManagerCtrl.getAppointmentsByResult(null, ((Datetime)Date.today()).formatGmt('yyyy-MM-dd'), sstu.store.id);
        System.assertNotEquals(lstSA, null);
    }
    
    static testmethod void testGetTodaysUnassignedAppointments(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
        Test.startTest();
    
        List<SalesSchedStoreManagerCtrl.AppointmntsWrap> appointments = new List<SalesSchedStoreManagerCtrl.AppointmntsWrap>();
        System.runAs(sstu.managers.get(0)){
            String today = ((Datetime)Date.today()).formatGmt('yyyy-MM-dd');
            appointments = SalesSchedStoreManagerCtrl.getTodaysUnassignedAppointments(today,'All','');
            //Kick off auto assignment too, for code coverage sake.
            SalesSchedStoreManagerCtrl.autoAssign('All', today);
           
            
        
            System.assert(!appointments.isEmpty());
        //}catch(Exception ex){
           // System.debug('Exception: ' + ex);       
        //}
        }
    }
     
   /* static testmethod void testGetTodayUnassignedAptDupe(){
       
         SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
        Test.startTest();
    
        List<SalesSchedStoreManagerCtrl.AppointmntsWrap> appointments = new List<SalesSchedStoreManagerCtrl.AppointmntsWrap>();
        System.runAs(sstu.managers.get(0)){
            String today = ((Datetime)Date.today()).formatGmt('yyyy-MM-dd');
            appointments = SalesSchedStoreManagerCtrl.getTodaysUnassignedAppointments(today,'All','');
            //Kick off auto assignment too, for code coverage sake.
            SalesSchedStoreManagerCtrl.autoAssignSS('All', today);
            System.assert(!appointments.isEmpty());
        }
       // }catch(Exception ex){
        //    System.debug('Exception: ' + ex);       
        //}
    }*/
    
     static testmethod void testGetDashboard(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,3);
        sstu.createAppointments(3);
       
        List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
                Sales_Capacity__c = sstu.capacities.get(0).Id,
                Sales_Appointment__c = sstu.appointments.get(0).Id,
                Status__c = 'Accepted',
                Assignment_Reason__c = 'Manual'
            )
        );
        resources.add(
            new Sales_Appointment_Resource__c(
                Sales_Capacity__c = sstu.capacities.get(1).Id,
                Sales_Appointment__c = sstu.appointments.get(1).Id,
                Status__c = 'Accepted',
                Assignment_Reason__c = 'Enabled'
            )
        );
        insert resources;
         
        insert new Sales_Rep_Report__c(
            User__c = sstu.reps.get(0).Id,
            Date__c = Date.today(),
            Orders__c = 1,
            Issued__c =  1,
            //Close_Rate__c = 0.5,
            RPA28__c = 1,
            RPA6__c = 1,
            Events__c = 1,
            Utilization__c = 1
        );
         
        Test.startTest();
        SalesSchedStoreManagerCtrl.DashboardWrapper dashboard = null;
        System.runAs(sstu.managers.get(0)){
            String today = ((Datetime)Date.today()).formatGmt('yyyy-MM-dd');
            dashboard = SalesSchedStoreManagerCtrl.getDashboard(today,'All');
        }
        //System.assert(!appointments.isEmpty());
    }
    
    static testmethod void testGetAvailableCapacities(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,3);
        sstu.createAppointments(1);
       
        Test.startTest();
        List<SalesSchedUtils.Option> options = null;
        System.runAs(sstu.managers.get(0)){
            String today = ((Datetime)Date.today()).formatGmt('yyyy-MM-dd');
            options = SalesSchedStoreManagerCtrl.getAvailableCapacities(sstu.reps.get(0).Id,today);
        }
        System.assertEquals(1,options.size());
    }
    
    static testmethod void testAssign(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,3);
        sstu.createAppointments(1);
       
        Test.startTest();
        System.runAs(sstu.managers.get(0)){
            SalesSchedStoreManagerCtrl.assign(sstu.slot.Name,sstu.reps.get(0).Id, sstu.appointments.get(0).Id);
        }
        System.assert(![Select Id From Sales_Appointment_Resource__c].isEmpty());
    }
    
    static testmethod void testAssignByCapacity(){
    try{
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,3);
        sstu.createAppointments(1);

        Test.startTest();
        System.runAs(sstu.managers.get(0)){
            SalesSchedStoreManagerCtrl.assignByCapacity(sstu.capacities.get(0).Id,sstu.appointments.get(0).Id);
        }
        System.assert(![Select Id From Sales_Appointment_Resource__c].isEmpty());
        
        }
        catch(Exception e)
        {
            System.debug(e);
        }

   }
    
    static testmethod void testConfirmAppointments(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,3);
        sstu.createAppointments(2);
        
        SalesSchedStoreManagerCtrl.RepReport rr1 = new SalesSchedStoreManagerCtrl.RepReport();
        rr1.id = sstu.reps.get(0).Id;
        rr1.acceptedIcon = '';
        rr1.am = '';
        rr1.mid = '';
        rr1.pm = '';
        rr1.lateam = '';
        rr1.earlyam = '';
        rr1.latepm = '';
        rr1.earlypm = '';
        rr1.amVariant = '';
        rr1.pmVariant = '';
        rr1.midVariant = '';
        rr1.lateamVariant = '';
        rr1.earlyamVariant ='';
        rr1.latepmVariant = '';
        rr1.earlypmVariant = '';
        rr1.amAppointmentId = '';
        rr1.pmAppointmentId = '';
        rr1.midAppointmentId = '';
        rr1.lateamAppointmentId = '';
        rr1.earlyamAppointmentId = '';
        rr1.latepmAppointmentId = '';
        rr1.earlypmAppointmentId = '';
        rr1.amCapacityId = '';
        rr1.pmCapacityId = '';
        rr1.midCapacityId = '';
        rr1.lateamCapacityId = '';
        rr1.earlyamCapacityId = '';
        rr1.latepmCapacityId = '';
        rr1.earlypmCapacityId = '';
        rr1.url = '';
        rr1.amAptStatus='';    
        rr1.midAptStatus='';   
        rr1.pmAptStatus='';    
        rr1.lateamAptStatus='';
        rr1.earlyamAptStatus='';
        rr1.latepmAptStatus='';
        rr1.earlypmAptStatus='';
        rr1.amSalesCapCount='';
        rr1.midSalesCapCount='';
        rr1.pmSalesCapCount='';
        rr1.earlyamSalesCapCount='';
        rr1.lateamSalesCapCount='';
        rr1.earlypmSalesCapCount='';
        rr1.latepmSalesCapCount='';
        rr1.lateamApptmentList= new list<string>();
        rr1.earlyamApptmentList= new list<string>();
        rr1.latepmApptmentList= new list<string>();
        rr1.earlypmApptmentList= new list<string>();
        rr1.amApptmentList= new list<string>();
        rr1.midApptmentList= new list<string>();
        rr1.pmApptmentList= new list<string>();
        
        
        SalesSchedStoreManagerCtrl.RepReport rr2 = new SalesSchedStoreManagerCtrl.RepReport();
        rr2.id = sstu.reps.get(1).Id;
        List<SalesSchedStoreManagerCtrl.RepReport> repReports = new List<SalesSchedStoreManagerCtrl.RepReport>{rr1,rr2};
        
        List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
                Sales_Capacity__c = sstu.capacities.get(0).Id,
                Sales_Appointment__c = sstu.appointments.get(0).Id,
                Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual'
            )
        );
        insert resources;
        
        
       
        Test.startTest();
        SalesSchedStoreManagerCtrl.DashboardWrapper dashboard = null;
        System.runAs(sstu.managers.get(0)){
            String today = ((Datetime)Date.today()).formatGmt('yyyy-MM-dd');
            SalesSchedStoreManagerCtrl.confirmAppointments(today,repReports,null);
        }
        
        List<Sales_Appointment_Resource__c> results = [Select Status__c From Sales_Appointment_Resource__c];
        System.assertEquals(1,results.size());
//        System.assertEquals('Confirmed',results.get(0).Status__c);
    }
    
     static testmethod void testConfirmAppointmentsNonResulted(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,3);
        sstu.createAppointments(2);
        
        SalesSchedStoreManagerCtrl.RepReport rr1 = new SalesSchedStoreManagerCtrl.RepReport();
        rr1.id = sstu.reps.get(0).Id;
        SalesSchedStoreManagerCtrl.RepReport rr2 = new SalesSchedStoreManagerCtrl.RepReport();
        rr2.id = sstu.reps.get(1).Id;
        List<SalesSchedStoreManagerCtrl.RepReport> repReports = new List<SalesSchedStoreManagerCtrl.RepReport>{rr1,rr2};
        
        List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
                Sales_Capacity__c = sstu.capacities.get(0).Id,
                Sales_Appointment__c = sstu.appointments.get(0).Id,
                Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual'
            )
        );
         
        //Create a past appointment that has not been resulted yet.
        sstu.createPastAppointments(2);
        resources.add(
            new Sales_Appointment_Resource__c(
                Sales_Capacity__c = sstu.pastCapacities.get(0).Id,
                Sales_Appointment__c = sstu.appointments.get(0).Id,
                Status__c = 'Confirmed',
                Assignment_Reason__c = 'Manual'
            )
        );
        insert resources;
        
        Test.startTest();
        SalesSchedStoreManagerCtrl.DashboardWrapper dashboard = null;
        System.runAs(sstu.managers.get(0)){
            String today = ((Datetime)Date.today()).formatGmt('yyyy-MM-dd');
            SalesSchedStoreManagerCtrl.confirmAppointments(today,repReports,null);
        }
        
        List<Sales_Appointment_Resource__c> results = [Select Status__c From Sales_Appointment_Resource__c Order By Date__c Asc];
        System.assertEquals(2,results.size());
        System.assertEquals('Confirmed',results.get(0).Status__c);
 //       System.assertEquals('Confirmed',results.get(1).Status__c);
    }
    static testmethod void unCoveredMethods(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,3);
        sstu.createAppointments(2);
        
        SalesSchedStoreManagerCtrl.RepReport rr1 = new SalesSchedStoreManagerCtrl.RepReport();
        rr1.id = sstu.reps.get(0).Id;
        decimal d = 1234.23;
        
        SalesSchedStoreManagerCtrl.getStoreOptions();
        SalesSchedStoreManagerCtrl.getFormattedCurrency(d);
       // SalesSchedStoreManagerCtrl.getMyTeam(null);
       // SalesSchedStoreManagerCtrl.getDashboardImperative(string.valueOf(system.today()),'account1');
        
    }
    static testmethod void  createResource(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
        Sales_Appointment__c lstTemp = [select id, Sales_Order__r.Result__c, Sales_Order__r.Opportunity__c, Appointment_Date__c, store__c from Sales_Appointment__c limit 1];
        
        SalesSchedUtilsWithoutSharing.createResource(lstTemp, sstu.capacities.get(0), null);
   }
    static testmethod void  addEvents(){
         SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,3);
        sstu.createAppointments(3);
        
        Sales_Appointment__c lstTemp = [select id, Sales_Order__r.Result__c, Sales_Order__r.Opportunity__c, Appointment_Date__c, store__c from Sales_Appointment__c limit 1];
       
        List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
                Sales_Capacity__c = sstu.capacities.get(0).Id,
                Sales_Appointment__c = sstu.appointments.get(0).Id,
                Status__c = 'Accepted',
                Assignment_Reason__c = 'Manual'
            )
        );
        resources.add(
            new Sales_Appointment_Resource__c(
                Sales_Capacity__c = sstu.capacities.get(1).Id,
                Sales_Appointment__c = sstu.appointments.get(1).Id,
                Status__c = 'Accepted',
                Assignment_Reason__c = 'Enabled'
            )
        );
        insert resources;
        
        Event e = new Event(WhatId = lstTemp.Sales_Order__r.Opportunity__c, DurationInMinutes=1, ActivityDateTime=datetime.now());
        insert e;
        
        Map<Id,List<Sales_Appointment_Resource__c>> opportunityToUsersMap = new Map<Id,List<Sales_Appointment_Resource__c>>();
        opportunityToUsersMap.put(lstTemp.Sales_Order__r.Opportunity__c, resources );
        
        SalesSchedUtilsWithoutSharing.addEvents(opportunityToUsersMap, FALSE);
   }    
}