public class CanvassAddNewAddressController {
    public CNVSS_Canvass_Unit__c canvassUnit {get;set;}
    public Boolean dmlExecuted {get;set;}
    public String redirectUrl {get;set;}
    
    public CanvassAddNewAddressController() {
        canvassUnit = new CNVSS_Canvass_Unit__c();
        dmlExecuted = false;
        canvassUnit.CNVSS_Canvass_Market__c = getCanvassMarketFromUser();
    }
    
    private void insertCanvassUnit() {
        canvassUnit.CNVSS_Address_Source__c = 'Canvasser';
        canvassUnit.CNVSS_Street__c = canvassUnit.CNVSS_Street__c.toUpperCase();
        canvassUnit.CNVSS_City__c = canvassUnit.CNVSS_City__c.toUpperCase();
        canvassUnit.CNVSS_PK_Full_Address__c = canvassUnit.CNVSS_House_No__c + ' ' + canvassUnit.CNVSS_Street__c + ', ' 
            + canvassUnit.CNVSS_City__c + ', ' + canvassUnit.CNVSS_State__c + ', ' + canvassUnit.CNVSS_Zip_Code__c;

        // Check for dupes that were inserted by Canvassers
        
        //DW replace with getCanvassUnitIdByAddress(string house_no, string street, string city,string state,string zip) 
        //string canvassUnitId = CanvassUnitUtils.getCanvassUnitIdByFullAddress(canvassUnit.CNVSS_PK_Full_Address__c);
        string canvassUnitId = CanvassUnitUtils.getCanvassUnitIdByAddress(canvassUnit.CNVSS_House_No__c, canvassUnit.CNVSS_Street__c, canvassUnit.CNVSS_City__c,canvassUnit.CNVSS_State__c,canvassUnit.CNVSS_Zip_Code__c);
        
        if(string.isNotBlank(canvassUnitId)) {
            CNVSS_Canvass_Unit__c tempCu = CanvassUnitUtils.getCanvassUnitById(canvassUnitId);
            canvassUnit.Id = canvassUnitId;
            //Update ownerID, date assigned          
            if(tempCu.CNVSS_Address_Source__c == 'Canvasser') {
                canvassUnit.Id = tempCu.Id;
                canvassUnit.OwnerId = UserInfo.getUserId();
                canvassUnit.DateAssigned__c = system.today();
                //update canvassUnit;
                
                Database.SaveResult updateResult = Database.update(canvassUnit);
                if(updateResult.isSuccess()) {
                    dmlExecuted = true;
                }
            }else{
                tempCu.ownerId = UserInfo.getUserId();
                tempCu.DateAssigned__c = system.today();

                if (String.isnotblank(canvassUnit.CNVSS_Homeowner_First_name__c)){
                    tempCu.CNVSS_Homeowner_First_name__c = canvassUnit.CNVSS_Homeowner_First_name__c;
                }
                
                if (String.isnotblank(canvassUnit.CNVSS_Homeowner_Last_name__c)){
                    tempCu.CNVSS_Homeowner_Last_name__c = canvassUnit.CNVSS_Homeowner_Last_name__c;
                }

                if (canvassUnit.CNVSS_Is_Sole_Owner__c == true){
                    tempCu.CNVSS_Is_Sole_Owner__c = canvassUnit.CNVSS_Is_Sole_Owner__c;
                }

                if (String.isnotblank(canvassUnit.CNVSS_Homeowner_2_First_name__c)){
                    tempCu.CNVSS_Homeowner_2_First_name__c = canvassUnit.CNVSS_Homeowner_2_First_name__c;
                }
                
                if (String.isnotblank(canvassUnit.CNVSS_Homeowner_2_Last_name__c)){
                    tempCu.CNVSS_Homeowner_2_Last_name__c = canvassUnit.CNVSS_Homeowner_2_Last_name__c;
                }
                
                if (String.isnotblank(canvassUnit.CNVSS_Home_Phone__c)){
                    tempCu.CNVSS_Home_Phone__c = canvassUnit.CNVSS_Home_Phone__c;
                }

                if (String.isnotblank(canvassUnit.CNVSS_Cell_Phone__c)){
                    tempCu.CNVSS_Cell_Phone__c = canvassUnit.CNVSS_Cell_Phone__c;
                }

                if (String.isnotblank(canvassUnit.CNVSS_Cell_Phone_2__c)){
                    tempCu.CNVSS_Cell_Phone_2__c = canvassUnit.CNVSS_Cell_Phone_2__c;
                }                
               
                if (String.isnotblank(canvassUnit.CNVSS_Email__c)){
                    tempCu.CNVSS_Email__c = canvassUnit.CNVSS_Email__c;
                }  
                
                //update
                Database.SaveResult updateResult = Database.update(tempCu);
                if(updateResult.isSuccess()) {
                    dmlExecuted = true;
                }

            }
        }
        else {
            canvassUnit.DateAssigned__c = system.today();
            Savepoint sp = Database.setSavepoint();
            try{
                insert canvassUnit;
                dmlExecuted = true;

            }catch (Exception e){
                Database.rollback(sp);
                canvassUnit.id = null;
                ApexPages.addMessages(e);
            }
            //insert canvassUnit;
// put back
//            Database.SaveResult result = Database.insert(canvassUnit);
//            if(result.isSuccess()) {
//                dmlExecuted = true;
 //           }
        }
    }
    
    private string getCanvassMarketFromUser() {
        string marketId = '';
        string market = '';
        List<User> u = new List<User>();
        List<CNVSS_Canvass_Market__c> mkt = new List<CNVSS_Canvass_Market__c>();
        u = [SELECT Id, Canvass_Market__c FROM User WHERE Id =: UserInfo.getUserId()];
        if(!u.isEmpty()) {
            market = u[0].Canvass_Market__c;
            if(string.isNotBlank(market)) {
                        mkt = [SELECT Id, Name FROM CNVSS_Canvass_Market__c WHERE Name =: market];
                if(!mkt.isEmpty()) {
                    marketId = mkt[0].Id;
                }
            }
        }
        if(string.isNotBlank(marketId))
            return marketId;
        else
            return ([SELECT Id, Name FROM CNVSS_Canvass_Market__c WHERE Name =: 'Twin Cities'].Id);
    }
    
    //public PageReference saveNewAddress() {
    //    insertCanvassUnit();
    //    string pgUrl ='';
    //    pgUrl = Page.CanvassedLeadInfo.getUrl() + '?canvassId=' + canvassUnit.Id + '&mode=newAddress';
    //    PageReference pg = new PageReference(pgUrl);
    //    pg.setRedirect(true);
    //    return pg;
    //}

    public void saveNewAddress() {
        insertCanvassUnit();
        system.debug(canvassUnit.ID);
        if (canvassUnit.ID != null){
            string pgUrl = Page.CanvassedLeadInfo.getUrl() + '?canvassId=' + canvassUnit.Id + '&mode=newAddress';
            redirectUrl = pgUrl;
        }
        
    }
    
    //public PageReference doNotSaveNewAddress() {
    //    insertCanvassUnit();
    //    string pgUrl ='';
    //    pgUrl = Page.CanvassedLeadInfoComplete.getUrl();
    //    PageReference pg = new PageReference(pgUrl);
    //    pg.setRedirect(true);
    //    return pg;
    //}

    public void doNotSaveNewAddress() {
        //insertCanvassUnit();
        string pgUrl =Page.CNVSS_CanvassHome.getUrl();
        redirectUrl = pgUrl;
    }
}