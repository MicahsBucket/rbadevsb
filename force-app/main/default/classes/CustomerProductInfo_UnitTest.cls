@isTest
private class CustomerProductInfo_UnitTest {
	@testSetup
    static void setup(){
        TestDataFactoryStatic.setUpConfigs();

		// Create accounts
		Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
		Financial_Account_Number__c fan = new Financial_Account_Number__c(Name ='Finan Acc',Account_Type__c='Cost PO');
        
		insert fan;
		Database.insert(storeAccount, true);

		Id storeAccountId = storeAccount.Id;
		Id storeConfigId = storeAccount.Active_Store_Configuration__c;
		Id accountId = TestDataFactoryStatic.createAccount('TestAcct', storeAccountId).Id;

		Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'TestPrimaryContact');
		Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('Test Opportunity', accountId, storeAccountId, 'Sold', date.today());

		System.debug('opportunity.Store_Location__c: ' + opportunity.Store_Location__c);
		
		OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
		Order ord = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        ord.Order_Processed_Date__c = Date.today().addDays(-1);
        ord.Tech_Measure_Date__c = Date.today().addDays(-3);
		Database.update(ord); 

		// Add some products for order items
		Product2 product1 = new Product2(
            Name='Test Product',
            Vendor__c = storeAccount.id,
            Cost_PO__c = true,
            isActive = true,
            Account_Number__c =  fan.id
        );
        insert product1;

		// Add some price books and entries for order items
        Pricebook2 pricebook1 =  TestDataFactoryStatic.createPricebook2Name('Standard Price Book');
        insert pricebook1;

        List<PricebookEntry> pbsToEnter = new List<PricebookEntry>();
        PricebookEntry pricebookEntry1 = TestDataFactoryStatic.createPricebookEntry(Test.getStandardPricebookId(), product1.id);
        pbsToEnter.add(pricebookEntry1);
        PricebookEntry pricebookEntry2 = TestDataFactoryStatic.createPricebookEntry(pricebook1.Id, product1.id);
        pbsToEnter.add(pricebookEntry2);
        insert pbsToEnter;

		// Add two items to the order       
		OrderItem oItem1 = new OrderItem(OrderId = ord.Id, PricebookentryId = pricebookEntry1.Id, Quantity = 2, UnitPrice = 100 );
		insert oItem1;

    }

	@isTest
	private static void getOrderItemsTest(){
       // Order ord = [SELECT Id FROM Order LIMIT 1];
       // List<OrderItem> testOrderItems = CustomerProductInfoController.getOrderItems(ord.Id);
       // System.assertEquals(testOrderItems.size(), 2);
	}

}