/**
 * @description       :  utility classes for running makability in sf
 * @author            : mark.rothermal@andersencorp.com
 * @group             : 
 * @last modified on  : 02-24-2021
 * @last modified by  : mark.rothermal@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                            Modification
 * 1.0   02-24-2021   mark.rothermal@andersencorp.com   Initial Version
**/
public without sharing class MakabilityUtility {

	public static boolean compareMaxHeightOrWidth(decimal requestInches,string requestFractions, decimal configInches, string configFractions  ){
		boolean makable = false;
		//       Decimal convertedRequestInches = decimal.valueOf(requestInches);
		Double requestConverted = requestInches + Constants.fractionConversionMap.get(requestFractions);
		Double configConverted = configInches + Constants.fractionConversionMap.get(configFractions);
		if(requestConverted <= configConverted) {
			makable = true;
		}
		return makable;
	}

    public static boolean compareMinHeightOrWidth(decimal requestInches,string requestFractions, decimal configInches, string configFractions  ){
	    boolean makable = false;
	    //       Decimal convertedRequestInches = decimal.valueOf(requestInches);
	    Double requestConverted = requestInches + Constants.fractionConversionMap.get(requestFractions);
	    Double configConverted = configInches + Constants.fractionConversionMap.get(configFractions);
	    if(requestConverted >= configConverted) {
		    makable = true;
	    }
	    return makable;
    }
    
    public static Double calculateUnitedInches (decimal heightInches, string heightFraction, decimal widthInches, string widthFraction){
	    Double ui;
	    Double height = heightInches + Constants.fractionConversionMap.get(heightFraction);
	    Double width = widthInches + Constants.fractionConversionMap.get(widthFraction);
	    ui = height + width;
	    return ui;
    }

	public static Double multiplyHeightbyWidth(decimal heightInches, string heightFraction, decimal widthInches, string widthFraction){
	    Double result;
	    Double height = heightInches + Constants.fractionConversionMap.get(heightFraction);
	    Double width = widthInches + Constants.fractionConversionMap.get(widthFraction);
	    result = height * width;
	    return result;
	}
	public static Double divideWidthByHeight(decimal heightInches, string heightFraction, decimal widthInches, string widthFraction){
	    Double result;
	    Double height = heightInches + Constants.fractionConversionMap.get(heightFraction);
	    Double width = widthInches + Constants.fractionConversionMap.get(widthFraction);
	    result = width/height;
	    return result;
	}
	public static Double setShortLeg(decimal leftLegInches, string leftLegFraction, decimal rightLegInches, string rightLegFraction){
	    Double result;
	    Double leftLeg = leftLegInches + Constants.fractionConversionMap.get(leftLegFraction);
	    Double rightLeg = rightLegInches + Constants.fractionConversionMap.get(rightLegFraction);		
		if(leftLeg >= rightLeg){
			result = rightLeg;
		} else{
			result = leftLeg;
		}
	    return result;
	}


    // initially used for building error messages in config checks. takes in a prod id and the keyset from the config map.
    public static List<String> splitUniqueKey(id productId, Set<String> keySet){
      	List<String> results = new List<String>();
        String result;
        for(String s : keySet){
            if (s.contains(productId)){
                result = s.substringAfter('-');
                results.add(result);
            }
        }
        return results;
    }
    public static List<ApexClass> fetchMakabilityClasses(){
		List<ApexClass> classNames = new List<ApexClass>();
		if(Test.isRunningTest()){
     		classNames = [SELECT Name FROM ApexClass WHERE Name LIKE '%MakabilityCheck%' AND (Name != 'MakabilityCheckLockSize' OR Name != 'MakabilityCheckSpecialtyShapeConfig')];
			 for(Integer i = 0; i<classNames.size();i++){
				 if(classNames[i].Name == 'MakabilityCheckLockSize' || classNames[i].Name == 'MakabilityCheckSpecialtyShapeConfig'){
					 classNames.remove(i);
				 }
			 }
		} else{
			classNames = (List<ApexClass>) [FIND 'MakabilityService' In All Fields RETURNING ApexClass(Name Where Name Like '%MakabilityCheck%')][0];      
		}
        system.debug('classes in fetch classes' + classNames);
		return classNames;
    }

    public static MakabilityCalculator validateFields(ProductConfiguration pc){
	    MakabilityCalculator mc = new MakabilityCalculator();
        mc.checkSize = false;
        mc.checkColor = false;
        mc.checkGlaze = false;
        mc.checkGrille = false;
        mc.checkScreen = false;
        mc.checkSpecialGrille = false;
		mc.checkFrameNotch = false;
		mc.checkSpecialShape = false;
		mc.hasLeftLeg = false;
		mc.hasRightLeg = false;
		mc.checkLegCalc = false;
		mc.checkS3 = false;

	    if(pc.heightInches !=null || pc.widthInches !=null || pc.widthFractions !=null || pc.heightFractions != null) {
		    mc.checkSize = true;
	    }
	    if(pc.exteriorColor !=null || pc.interiorColor !=null ) {
		    mc.checkColor = true;
	    }
	    if(pc.grillePattern != null && pc.specialtyShape == null) {
		    mc.checkGrille = true;
	    }
	    if(pc.grillePattern != null && pc.specialtyShape != null) {
		    mc.checkSpecialGrille = true;
	    }
		if(pc.specialtyShape != null) {
		    mc.checkSpecialShape = true;
	    }
		if(pc.specialtyShape == 'Chord' || pc.specialtyShape == 'Circle Top' || pc.specialtyShape == 'Springline' ){
			mc.checkLegCalc = true;
		}
	    if(pc.screenType != null || pc.screenSize != null) {
		    mc.checkScreen = true;
	    }
        if(pc.s1Tempering != null || pc.s2Tempering != null || pc.s1Pattern != null || pc.s2Pattern != null){
            mc.checkGlaze = true;
        }
		if(pc.insertFrame != null || pc.frame != null){
			mc.checkFrameNotch = true;
		}
		if(pc.leftLegInches != null || pc.leftLegFraction != null){
			mc.hasLeftLeg = true;
		}
		if(pc.rightLegInches != null || pc.rightLegFraction != null){
			mc.hasRightLeg = true;
		}
		if(pc.s3Pattern != null || pc.s3Type != null){
			mc.checkS3 = true;
		}

	    return mc;
    }

}