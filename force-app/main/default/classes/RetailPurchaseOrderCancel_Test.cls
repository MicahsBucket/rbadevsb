@isTest
private class RetailPurchaseOrderCancel_Test {

    /*******************************************************
                    createTestRecords
    *******************************************************/
    @testSetup static void createTestRecords(){
       
        
        TestUtilityMethods utility = new TestUtilityMethods();
        
        utility.createOrderTestRecords();
        
    }
    
    static testmethod void setPurchaseorderToCancelRPOTest()
    {
       Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;
         Order soldOrder = [SELECT Id FROM Order WHERE Name = 'Sold Order 1'];
        Id devRecordTypeId = Schema.SObjectType.Retail_Purchase_Order__c.getRecordTypeInfosByName().get('RbA Purchase Order').getRecordTypeId();
        Retail_Purchase_Order__c po = new Retail_Purchase_Order__c();
        po.Invoice_Number__c='test';
        po.Order__c=soldorder.id;
        po.RecordTypeId= devRecordTypeId;
        po.Store_Location__c=store.id;
        po.Store_Number__c='1234' ;
        insert po;
        PurchaseOrderButtonCancelController.getStatus(po.id);
        PurchaseOrderButtonCancelController.setPurchaseOrderToCancel(po.id);
    }
    
    static testmethod void setPurchaseorderToCancelCPOTest()
    {
        TestUtilityMethods utilities = new TestUtilityMethods();
       Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;
         Order soldOrder = [SELECT Id FROM Order WHERE Name = 'Sold Order 1'];
        Id devRecordTypeId = Schema.SObjectType.Retail_Purchase_Order__c.getRecordTypeInfosByName().get('Cost Purchase Order').getRecordTypeId();
        Retail_Purchase_Order__c po = new Retail_Purchase_Order__c();
        po.Invoice_Number__c='test';
        po.Order__c=soldorder.id;
        po.RecordTypeId= devRecordTypeId;
        po.Store_Location__c=store.id;
        po.Store_Number__c='1234' ;
        insert po;
        Account vend1 = utilities.createVendorAccount('Vendor Account 1');
		vend1.name='Renewal by Andersen';
        insert vend1;
        
        Product2 product1 = new Product2(
			Name='Renewal By andersen', 
			Vendor__c = vend1.id
		);
        product1.Inventoried_Item__c=true;
        product1.IsActive=true;
		insert product1;
		
		PricebookEntry pricebookEntry1 = utilities.createPricebookEntry(Test.getStandardPricebookId(), product1.id);
		insert pricebookEntry1;
       Retail_Purchase_Order_Items__c roi = new 	Retail_Purchase_Order_Items__c();
        roi.Retail_Purchase_Order__c=po.id;
        roi.Product__c=product1.id;
        insert roi;
        
        PurchaseOrderButtonCancelController.getStatus(po.id);
        PurchaseOrderButtonCancelController.setPurchaseOrderToCancel(po.id);
    }
    }