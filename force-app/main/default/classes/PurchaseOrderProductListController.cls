/*
* @author Jason Flippen
* @date 11/21/2019 
* @description Class to provide functionality for the purchaseOrderProductList LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - PurchaseOrderProductListControllerTest
*/
public with sharing class PurchaseOrderProductListController {

    /*
    * @author Jason Flippen
    * @date 12/01/2019
    * @description Method to return (wrapped) data from a Purchase Order.
    */
    @AuraEnabled(cacheable=true)
    public static PurchaseOrderWrapper getPurchaseOrderData(Id purchaseOrderId) {

        PurchaseOrderWrapper wrapper = new PurchaseOrderWrapper();

        // Retrieve data from the Purchase Order record.
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  Charge_Cost_To__c,
                                                  Confirmed_Timestamp__c,
                                                  Order__c,
                                                  Order__r.Pricebook2Id,
                                                  Order__r.RecordTypeId,
                                                  Order__r.RecordType.DeveloperName,
                                                  RecordTypeId,
                                                  RecordType.DeveloperName,
                                                  Status__c,
                                                  Store_Location__r.Active_Store_Configuration__r.Location_Number__c,
                                                  Submitted_Timestamp__c,
                                                  Vendor__c,
                                                  Vendor__r.Name,
                                                  Vendor__r.Over_Receiving_Allowed__c,
                                                  Vendor__r.Zero_Dollar_Vendor__c,
                                                  (
                                                      SELECT Id,
                                                             OrderItemNumber,
                                                             Charge_Cost_To__c,
                                                             Date_Received__c,
                                                             Date_Vendor_Credit__c,
                                                             Date_Written_Off__c,
                                                             Defect_Code__c,
                                                             Description,
                                                             Desired_width__c,
                                                             Discount_Amount__c,
                                                             GL_Account__c,
                                                             Has_PO__c,
                                                             Installed_Product_Asset__c,
                                                             Installed_Product_Asset__r.Date_Received__c,
                                                             Installed_Product_Asset__r.Quantity,
                                                             Installed_Product_Asset__r.Quantity_Written_Off__c,
                                                             Installed_Product_Asset__r.Vendor_Credit__c,
                                                             MTO_Source_Code__c,
                                                             Multiple_Remakes__c,
                                                             Notes__c,
                                                             NSPR__c,
                                                             OrderId,
                                                             Order.AccountId,
                                                             Order.BillToContactId,
                                                             Order.EffectiveDate,
                                                             Order.Store_Location__c,
                                                             Parent_Order_Item__r.Installed_Product_Asset__c,
                                                             PricebookEntry.Product2Id,
                                                             Pricebookentry.Product2.Description,
                                                             Pricebookentry.Product2.Family,
                                                             Pricebookentry.Product2.Name,
                                                             Pricebookentry.Product2.Name_Part_Number__c,
                                                             Pricebookentry.Product2.Part_Number__c,
                                                             Pricebookentry.Product2.ProductCode,
                                                             Pricebookentry.Product2.Vendor__c,
                                                             Pricebookentry.Product2.Vendor__r.Name,
                                                             Pricebookentry.UnitPrice,
                                                             Purchase_Order__c,
                                                             Purchase_Order__r.Status__c,
                                                             Purchase_Order__r.Store_Location__c,
                                                             Purchase_Order__r.Vendor__c,
                                                             Quantity,
                                                             Quantity_To_Receive__c,
                                                             Quantity_to_Write_Off__c,
                                                             Remake__c,
                                                             Status__c,
                                                             Sold_Order_Product_Asset__c,
                                                             Sold_Order_Product_Asset__r.Product2Id,
                                                             Sold_Order_Product_Asset__r.Product_Name__c,
                                                             Sold_Order_Product_Asset__r.Status,
                                                             Sold_Order_Product_Asset__r.Variant_Number__c,
                                                             Total_Retail_Price__c,
                                                             Total_Wholesale_Cost__c,
                                                             UnitPrice,
                                                             Unit_of_Measure__c,
                                                             Unit_Wholesale_Cost__c,
                                                             Variant_Number__c,
                                                             Vendor_Credit_to_Receive__c
                                                      FROM   Order_Products__r
                                                      ORDER BY Unit_Id__c ASC
                                                  )
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];

        // Set the Wrapper properties.
        wrapper.id = purchaseOrder.Id;
        wrapper.chargeCostTo = purchaseOrder.Charge_Cost_To__c;
        wrapper.confirmedTimestamp = purchaseOrder.Confirmed_Timestamp__c;
        wrapper.orderId = purchaseOrder.Order__c;
        wrapper.orderPricebookId = purchaseOrder.Order__r.Pricebook2Id;
        wrapper.status = purchaseOrder.Status__c;
        wrapper.submittedTimestamp = purchaseOrder.Submitted_Timestamp__c;

        Boolean costPurchaseOrder = false;
        if (purchaseOrder.RecordType.DeveloperName == 'Cost_Purchase_Order') {
            costPurchaseOrder = true;
        }
        wrapper.costPurchaseOrder = costPurchaseOrder;

        Boolean productPurchaseOrder = false;
        Boolean servicePurchaseOrder = false;
        if (purchaseOrder.Order__r.RecordType.DeveloperName == 'CORO_Record_Type') {
            productPurchaseOrder = true;
        }
        else if (purchaseOrder.Order__r.RecordType.DeveloperName == 'CORO_Service') {
            servicePurchaseOrder = true;
        }
        wrapper.productPurchaseOrder = productPurchaseOrder;
        wrapper.servicePurchaseOrder = servicePurchaseOrder;

        String vendorId = '';
        String vendorName = '';
        if (purchaseOrder.Vendor__c != null) {
            wrapper.vendorId = purchaseOrder.Vendor__c;
            wrapper.vendorName = purchaseOrder.Vendor__r.Name;
        }

        // Add the List of OrderItems (aka Order Products) to our Wrapper.
        wrapper.productList = getOrderItemList(purchaseOrder.Order_Products__r);

        // Return the Wrapper Class.
        return wrapper;

    }

    /*
    * @author Jason Flippen
    * @date 11/21/2019
    * @description Method to return a (wrapped) List of OrderItem (Order Product)
    *              records related to a Purchase Order.
    */
    private static List<OrderItemWrapper> getOrderItemList(List<OrderItem> productList) {

        List<OrderItemWrapper> wrapperList = new List<OrderItemWrapper>();
        
        // Iterate through the List of OrderItems and build the List of Wrapper Classes.
        for (OrderItem oi: productList) {

            if (oi.Date_Received__c != Date.today()) {
                oi.Quantity_To_Receive__c = null;
                oi.Date_Received__c = null;
            }

            if (oi.Date_Written_Off__c != Date.today()) {
                oi.Quantity_To_Write_Off__c = null;
                oi.Date_Written_Off__c = null;
            }

            if (oi.Date_Vendor_Credit__c != Date.today()) {
                oi.Vendor_Credit_to_Receive__c = null;
                oi.Date_Vendor_Credit__c = null;
            }

            OrderItemWrapper wrapper = new OrderItemWrapper();
            wrapper.id = oi.Id;
            wrapper.orderItemUrl = '/' + oi.Id;
            wrapper.glAccount = oi.GL_Account__c;
            wrapper.quantity = oi.Quantity;
            wrapper.unitOfMeasure = oi.Unit_of_Measure__c;
            wrapper.chargeCostTo = oi.Charge_Cost_To__c;
            wrapper.unitPrice = oi.UnitPrice;
            wrapper.soldOrderProdAssetVariantNumber = oi.Sold_Order_Product_Asset__r.Variant_Number__c;
            wrapper.variantNumber = oi.Variant_Number__c;
            wrapper.unitWholesaleCost = oi.Unit_Wholesale_Cost__c;
            wrapper.totalWholesaleCost = oi.Total_Wholesale_Cost__c;
            wrapper.mtoSourceCode = oi.MTO_Source_Code__c;
            wrapper.defectCode = oi.Defect_Code__c;
            wrapper.notes = oi.Notes__c;
            wrapper.multipleRemakes = oi.Multiple_Remakes__c;
            wrapper.discountAmount = oi.Discount_Amount__c;
            wrapper.nspr = oi.NSPR__c;
            wrapper.installedProdAssetQty = oi.Installed_Product_Asset__r.Quantity;
            wrapper.installedProdAssetDateReceived = oi.Installed_Product_Asset__r.Date_Received__c;
            wrapper.quantityToReceive = oi.Quantity_To_Receive__c;
            wrapper.installedProdAssetQtyWrittenOff = oi.Installed_Product_Asset__r.Quantity_Written_Off__c;
            wrapper.quantityToWriteOff = oi.Quantity_to_Write_Off__c;
            wrapper.installedProdAssetVenderCredit = oi.Installed_Product_Asset__r.Vendor_Credit__c;
            wrapper.vendorCreditToReceive = oi.Vendor_Credit_to_Receive__c;
            wrapper.productId = oi.Pricebookentry.Product2Id;
            wrapper.productCode = oi.Pricebookentry.Product2.ProductCode;
            wrapper.productDescription = oi.Pricebookentry.Product2.Description;
            wrapper.productFamily = oi.Pricebookentry.Product2.Family;
            wrapper.productName = oi.Pricebookentry.Product2.Name;
            wrapper.productNamePartNumber = oi.Pricebookentry.Product2.Name_Part_Number__c;
            wrapper.productPartNumber = oi.Pricebookentry.Product2.Part_Number__c;
            wrapper.productVendorId = oi.Pricebookentry.Product2.Vendor__c;
            wrapper.productVendorName = oi.Pricebookentry.Product2.Vendor__r.Name;

            // Add the Wrapper Class to our List.            
            wrapperList.add(wrapper);

        }

        // Return the List of Wrapper Classes.
        return wrapperList;

    }

    /*
    * @author Jason Flippen
    * @date 02/11/2020
    * @description Method to return Service Request Products.
    */
    @AuraEnabled
    public static List<ChargeWrapper> getCharges(Id orderId, Id vendorId, String responsibility) {

        List<ChargeWrapper> wrapperList = new List<ChargeWrapper>();

        // Retrieve data from the Service Request record.
        for (Charge__c c : [SELECT Id,
                                   Category__c,
                                   Charge_Cost_To__c,
                                   Installed_Product__c,
                                   Installed_Product_Id__c,
                                   Service_Request__c,
                                   Service_Product__c,
                                   Service_Product__r.Order.Store_Location__c,
                                   Service_Product__r.PricebookEntryId,
                                   Service_Product__r.PricebookEntry.Product2Id,
                                   Service_Product__r.PricebookEntry.Product2.Vendor__c,
                                   Service_Product__r.PricebookEntry.Product2.Vendor__r.Name,
                                   Service_Product__r.Product_Name__c,
                                   Service_Product__r.Purchase_Order__c,
                                   Service_Product__r.Purchase_Order__r.Charge_Cost_To__c,
                                   Service_Product__r.Sold_Order_Product_Asset__c,
                                   Service_Product__r.Sold_Order_Product_Asset__r.Original_Order_Product__c,
                                   Service_Product__r.Sold_Order_Product_Asset__r.Product_Name__c,
                                   Service_Product__r.Sold_Order_Product_Asset__r.Variant_Number__c,
                                   Service_Product__r.Status__c,
                                   Variant_Number__c,
                                   What_Where__c
                            FROM   Charge__c
                            WHERE  Service_Request__c = :orderId
                            AND    Service_Product__c != null
                            AND    Service_Product__r.PricebookEntry.Product2.Vendor__c = :vendorId
                            AND    Service_Product__r.Status__c != 'Cancelled'
                            AND    Charge_Cost_To__c = :responsibility]) {

            if (c.Service_Product__r.Purchase_Order__c == null || 
                (c.Service_Product__r.Purchase_Order__c != null && c.Charge_Cost_To__c != c.Service_Product__r.Purchase_Order__r.Charge_Cost_To__c)) {

                ChargeWrapper wrapper = new ChargeWrapper();
                wrapper.id = c.Id;
                wrapper.category = c.Category__c; // aka Source of Defect
                wrapper.chargeCostTo = c.Charge_Cost_To__c; // aka Responsibility
                wrapper.installedProductId = c.Installed_Product_Id__c;
                wrapper.installedProductName = c.Installed_Product__c;
                wrapper.serviceRequestId = c.Service_Request__c;
                wrapper.serviceProductId = c.Service_Product__c;
                wrapper.serviceProductPricebookEntryId = c.Service_Product__r.PricebookEntryId;
                wrapper.serviceProductName = c.Service_Product__r.Product_Name__c;
                wrapper.serviceProductAssetId = c.Service_Product__r.Sold_Order_Product_Asset__c;
                wrapper.serviceProductAssetOriginalOrderProductId = c.Service_Product__r.Sold_Order_Product_Asset__r.Original_Order_Product__c;
                wrapper.serviceProductAssetProductName = c.Service_Product__r.Sold_Order_Product_Asset__r.Product_Name__c;
                wrapper.serviceProductAssetVariantNumber = c.Service_Product__r.Sold_Order_Product_Asset__r.Variant_Number__c;
                wrapper.serviceProductPurchaseOrderId = c.Service_Product__r.Purchase_Order__c;
                wrapper.serviceProductStoreLocationId = c.Service_Product__r.Order.Store_Location__c;
                wrapper.variantNumber = c.Variant_Number__c;
                wrapper.vendorId = c.Service_Product__r.PricebookEntry.Product2.Vendor__c;
                wrapper.vendorName = c.Service_Product__r.PricebookEntry.Product2.Vendor__r.Name;
                wrapper.whatWhere = c.What_Where__c;
                wrapperList.add(wrapper);

            }

        }

        // Return the List of Wrapper classes.
        return wrapperList;

    }

    /*
    * @author Jason Flippen
    * @date 03/06/2020
    * @description Method to delete a Product from its Purchase Order.
    */
    @AuraEnabled
    public static String deleteProduct(Id orderItemId) {

        String deleteResult = 'Delete Product Success';

        try {

            OrderItem updateOI = new OrderItem(Id = orderItemId,
                                               Purchase_Order__c = null,
                                               Has_PO__c = false);
            update updateOI;

        }
        catch (Exception ex) {
            System.debug('***** PurchaseOrderProductListController.deleteProduct Error: ' + ex);
            deleteResult = ex.getMessage();
        }

        return deleteResult;

    }

    /*
    * @author Jason Flippen
    * @date 08/10/2020
    * @description Method to return details of a Cost PO Part (aka Product).
    */
    @AuraEnabled(cacheable=true)
    public static PartWrapper fetchPartDetail(Id partId, Id pricebookId) {

        PartWrapper wrapper = new PartWrapper();

        // Grab the List of Parts related to the Part Id (should only be one).
        List<Product2> partList = [SELECT Id,
                                          Description,
                                          Name
                                   FROM   Product2 WHERE Id = :partId];
        
        if (!partList.isEmpty()) {

            // Grab the List of PricebookEntries related to the Part Id and Pricebook Id (should only be one).
            List<PricebookEntry> pricebookEntryList = [SELECT Id
                                                       FROM   PricebookEntry
                                                       WHERE  Product2Id = :partId
                                                       AND    Pricebook2Id = :pricebookId];

            if (!pricebookEntryList.isEmpty()) {

                // Update the Wrapper properties.
                wrapper.id = partList[0].Id;
                wrapper.description = partList[0].Description;
                wrapper.name = partList[0].Name;
                wrapper.pricebookEntryId = pricebookEntryList[0].Id;
                wrapper.quantity = 0;

            }

        }

        return wrapper;

    }

    /*
    * @author Jason Flippen
    * @date 08/10/2020
    * @description Method to save a List of Cost PO Parts (aka Products).
    */
    @AuraEnabled
    public static String saveParts(List<PartWrapper> newPartList,
                                   Id purchaseOrderId,
                                   Id orderId) {

        String saveResult = null;
        
        // Iterate through the List of new Parts and grab the List
        // of new OrderItem (aka Order Products) to be inserted.
        List<OrderItem> newOrderItemList = new List<OrderItem>();
        for (PartWrapper part : newPartList) {

            OrderItem newOrderItem = new OrderItem(Has_PO__c = true,
                                                   OrderId = orderId,
                                                   PricebookEntryId = part.pricebookEntryId,
                                                   Purchase_Order__c = purchaseOrderId,
                                                   Quantity = part.quantity,
                                                   UnitPrice = 0);
            newOrderItemList.add(newOrderItem);

        }

        try {

            // If we have new OrderItems to be inserted, insert them.
            if (!newOrderItemList.isEmpty()) {

                insert newOrderItemList;
                
                // Return a "Success" result.
                saveResult = 'Add Parts Success';

            }

        }
        catch (Exception ex) {
            System.debug('***** saveParts Error: ' + ex.getMessage());
            saveResult = ex.getMessage();
        }

        return saveResult;

    }

    /*
    * @author Jason Flippen
    * @date 08/31/2020
    * @description Method to save a List of Serce PO Products.
    */
    @AuraEnabled
    public static String saveProducts(PurchaseOrderWrapper purchaseOrder, List<ChargeWrapper> chargeWrapperList) {

        String saveResult = null;

        Set<Id> existingChargeIdSet = getExistingChargeIdSet(purchaseOrder.id,
                                                             purchaseOrder.orderId,
                                                             purchaseOrder.chargeCostTo,
                                                             purchaseOrder.vendorId);

        Map<Id,OrderItem> OrderItemMap = getOrderItemMap(chargeWrapperList);

        // Iterate through the List of new Parts and grab the List
        // of new OrderItem (aka Order Products) to be updated.
        List<OrderItem> updateOrderItemList = new List<OrderItem>();
        for (ChargeWrapper charge : chargeWrapperList) {

            if (!existingChargeIdSet.contains(charge.id)) {

                OrderItem currentOrderItem = orderItemMap.get(charge.serviceProductId);
                currentOrderItem.Has_PO__c = true;
                currentOrderItem.Purchase_Order__c = purchaseOrder.id;
                
                // Set the remake flag on the order item if the charge cost to is local
                Boolean remake = false;
                if (purchaseOrder.chargeCostTo == 'Retailer') {
                    remake = true;
                }
                currentOrderItem.Remake__c = remake;
    
                // Set the order item description
                String orderItemDescription = '';
                if (String.isNotBlank(charge.serviceProductAssetProductName)) {
                    orderItemDescription = charge.serviceProductAssetProductName + ' - ';
                }
                if (String.isNotBlank(charge.serviceProductAssetVariantNumber)) {
                    orderItemDescription +=  charge.serviceProductAssetVariantNumber;
                }
                currentOrderItem.Description = orderItemDescription;

                updateOrderItemList.add(currentOrderItem);

            }

        }

        try {

            // If we have new OrderItems to be updated, update them.
            if (!updateOrderItemList.isEmpty()) {

                update updateOrderItemList;
                
                // Return a "Success" result.
                saveResult = 'Add Products Success';

            }

        }
        catch (Exception ex) {
            System.debug('***** saveProducts Error: ' + ex.getMessage());
            saveResult = ex.getMessage();
        }

        return saveResult;

    }

    /*
    * @author Jason Flippen
    * @date 09/08/2020
    * @description Method to return a Set of Existing Charge Ids
    */
    private static Set<Id> getExistingChargeIdSet(Id purchaseOrderId,
                                                  Id orderId,
                                                  String responsibility,
                                                  Id vendorId) {

        Set<Id> existingChargeIdSet = new Set<Id>();

        // Retrieve data from the Service Request record.
        for (Charge__c c : [SELECT Id
                            FROM   Charge__c
                            WHERE  Service_Request__c = :orderId
                            AND    Service_Product__c != null
                            AND    Service_Product__r.PricebookEntry.Product2.Vendor__c = :vendorId
                            AND    Service_Product__r.Purchase_Order__c = :purchaseOrderId
                            AND    Service_Product__r.Purchase_Order__r.Status__c = 'In Process'
                            AND    Service_Product__r.Status__c != 'Cancelled'
                            AND    Charge_Cost_To__c = :responsibility]) {
            existingChargeIdSet.add(c.Id);
        }

        return existingChargeIdSet;

    }

    /*
    * @author Jason Flippen
    * @date 09/08/2020
    * @description Method to return an OrderItem Map.
    */
    private static Map<Id,OrderItem> getOrderItemMap(List<ChargeWrapper> chargeWrapperList) {

        Map<Id,OrderItem> orderItemMap = new Map<Id,OrderItem>();

        Set<Id> serviceProductIdSet = new Set<Id>();
        for (chargeWrapper charge : chargeWrapperList) {
            serviceProductIdSet.add(charge.serviceProductId);
        }

        if (!serviceProductIdSet.isEmpty()) {
            
            for (OrderItem oi : [SELECT Id,
                                        Has_PO__c,
                                        Purchase_Order__c,
                                        Sold_Order_Product_Asset__c,
                                        Sold_Order_Product_Asset__r.Variant_Number__c,
                                        Sold_Order_Product_Asset__r.Product_Name__c,
                                        Product_Name__c 
                                 FROM   OrderItem
                                 WHERE  Id IN :serviceProductIdSet])
            {
                orderItemMap.put(oi.Id,oi);
            }

        }

        return orderItemMap;

    }
    @AuraEnabled
    public static string updateRemakeProducts(Id ItemId, Boolean multipleRemakesCheck){
        System.debug('ItemId: '+ ItemId);
        System.debug('multipleRemakesCheck: '+ multipleRemakesCheck);
        OrderItem oiToUpdate = [SELECT Id, Multiple_Remakes__c FROM OrderItem WHERE Id =: ItemId];
        oiToUpdate.Multiple_Remakes__c = multipleRemakesCheck;
        UtilityMethods.disableAllFslTriggers();
        update oiToUpdate;

        return 'Success';
    }

/** Wrapper Classes **/


    /*
    * @author Jason Flippen
    * @date 12/01/2019
    * @description Wrapper Class for Purchase Order
    */
    @TestVisible
    public class PurchaseOrderWrapper {
        
        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String chargeCostTo {get;set;}

        @AuraEnabled
        public Datetime confirmedTimestamp {get;set;}

        @AuraEnabled
        public Boolean costPurchaseOrder {get;set;}

        @AuraEnabled
        public String orderId {get;set;}

        @AuraEnabled
        public String orderPricebookId {get;set;}

        @AuraEnabled
        public Boolean productPurchaseOrder {get;set;}

        @AuraEnabled
        public Datetime submittedTimestamp {get;set;}

        @AuraEnabled
        public Boolean servicePurchaseOrder {get;set;}

        @AuraEnabled
        public String status {get;set;}

        @AuraEnabled
        public String vendorId {get;set;}

        @AuraEnabled
        public String vendorName {get;set;}

        @AuraEnabled
        public List<OrderItemWrapper> productList {get;set;}

    }
    
    /*
    * @author Jason Flippen
    * @date 11/21/2019
    * @description Wrapper Class for OrderItems (Order Products)
    */
    @TestVisible
    public class OrderItemWrapper {

        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String orderItemUrl {get;set;}

        @AuraEnabled
        public String glAccount {get;set;}

        @AuraEnabled
        public Decimal quantity {get;set;}

        @AuraEnabled
        public String unitOfMeasure {get;set;}

        @AuraEnabled
        public String chargeCostTo {get;set;}

        @AuraEnabled
        public Decimal unitPrice {get;set;}

        @AuraEnabled
        public String soldOrderProdAssetVariantNumber {get;set;}

        @AuraEnabled
        public String variantNumber {get;set;}
        
        @AuraEnabled
        public Decimal unitWholesaleCost {get;set;}

        @AuraEnabled
        public Decimal totalWholesaleCost {get;set;}

        @AuraEnabled
        public String mtoSourceCode {get;set;}

        @AuraEnabled
        public String defectCode {get;set;}

        @AuraEnabled
        public String notes {get;set;}

        @AuraEnabled
        public Boolean multipleRemakes {get;set;}

        @AuraEnabled
        public Decimal discountAmount {get;set;}

        @AuraEnabled
        public Boolean nspr {get;set;}

        @AuraEnabled
        public Decimal installedProdAssetQty {get;set;}

        @AuraEnabled
        public Date installedProdAssetDateReceived {get;set;}

        @AuraEnabled
        public Decimal quantityToReceive {get;set;}

        @AuraEnabled
        public Decimal installedProdAssetQtyWrittenOff {get;set;}

        @AuraEnabled
        public Decimal quantityToWriteOff {get;set;}

        @AuraEnabled
        public Decimal installedProdAssetVenderCredit {get;set;}

        @AuraEnabled
        public Decimal vendorCreditToReceive {get;set;}

        @AuraEnabled
        public String productId {get;set;}

        @AuraEnabled
        public String productCode {get;set;}

        @AuraEnabled
        public String productDescription {get;set;}

        @AuraEnabled
        public String productFamily {get;set;}

        @AuraEnabled
        public String productName {get;set;}

        @AuraEnabled
        public String productNamePartNumber {get;set;}

        @AuraEnabled
        public String productPartNumber {get;set;}

        @AuraEnabled
        public String productVendorId {get;set;}

        @AuraEnabled
        public String productVendorName {get;set;}

    }

    /*
    * @author Jason Flippen
    * @date 06/28/2020
    * @description Wrapper Class for a Charge.
    */
    @TestVisible
    public class ChargeWrapper {

        @AuraEnabled
        public String id {get;set;}
        
        @AuraEnabled
        public String category {get;set;}
        
        @AuraEnabled
        public String chargeCostTo {get;set;}

        @AuraEnabled
        public String installedProductId {get;set;}

        @AuraEnabled
        public String installedProductName {get;set;}

        @AuraEnabled
        public String serviceRequestId {get;set;}

        @AuraEnabled
        public String serviceProductId {get;set;}

        @AuraEnabled
        public String serviceProductPricebookEntryId {get;set;}

        @AuraEnabled
        public String serviceProductName {get;set;}

        @AuraEnabled
        public String serviceProductAssetId {get;set;}

        @AuraEnabled
        public String serviceProductAssetOriginalOrderProductId {get;set;}

        @AuraEnabled
        public String serviceProductAssetProductName {get;set;}

        @AuraEnabled
        public String serviceProductAssetVariantNumber {get;set;}

        @AuraEnabled
        public String serviceProductPurchaseOrderId {get;set;}

        @AuraEnabled
        public String serviceProductStoreLocationId {get;set;}

        @AuraEnabled
        public String variantNumber {get;set;}

        @AuraEnabled
        public String vendorId {get;set;}

        @AuraEnabled
        public String vendorName {get;set;}

        @AuraEnabled
        public String whatWhere {get;set;}

    }

    /*
    * @author Jason Flippen
    * @date 08/11/2020
    * @description Wrapper Class for a Part (aka Product).
    */
    @TestVisible
    public class PartWrapper {

        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String description {get;set;}

        @AuraEnabled
        public String name {get;set;}

        @AuraEnabled
        public String pricebookEntryId {get;set;}

        @AuraEnabled
        public Decimal quantity {get;set;}

    }

}