/**
 * @author Connor Davis
 * @description This class will handle all the data querying and inserting/updating for the Add Legacy Asset LWC
 */
public with sharing class AddLegacyAssetController {

    /**
     * @author Connor Davis
     * @description A function that returns the Account that has the specified Id
     * @param The Id for the account we want to find
     * @return Returns the account that has the Id that was passed in
     */
    @AuraEnabled(cacheable=true)
    public static Account getAccountInfo(String accId) {

        Account acc = [SELECT Id, Name, Dwelling_Style__c, Year_Built__c, Account_Balance__c, No_Service__c, No_Service_Notes__c, 
                              Sales_Rep_User__r.Name, Store_Location__r.Name, HOA__r.Name, Historical__r.Name, Building_Permit__r.Name
                              FROM Account WHERE Id = :accId];
        return acc;
    }

    /**
     * @author Connor Davis
     * @description This function gets the products that have the string 'name' in their name
     * @param name The string that the user is searching
     * @return Returns the list of products that have the string 'name' in their name
     */
    @AuraEnabled(cacheable=true)
    public static List<Product2> getProducts(String name) {
        List<Product2> p = [SELECT Id, Name FROM Product2 WHERE Master_Legacy_Product__c = true AND Has_Service_Products__c = true AND Name LIKE :('%' + name + '%') ];
        return p;
    }

    /**
     * @author Connor Davis
     * @description This function inserts/updates a list of assets
     * @param assets A list of assets that will be inserted/updated
     */
    @AuraEnabled
    public static void saveAssets(List<Asset> assets) {

        for (Asset asset : assets) {
            if (String.isBlank(asset.RecordTypeId)) {
                asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('Installed_Products').getRecordTypeId();
            }
        }

        upsert assets;
        
    }
}