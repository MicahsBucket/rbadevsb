/*******************************************************//**

@class  RMS_cancelOrderExtension

@brief  Controller extension for VF page RMS_cancelOrder.page

  Cancels the order and other related records

@author  Creston Kuenzi (Slalom.CDK)

@version  2015-10-25  Slalom.CDK
  Created.

@see    RMS_cancelOrderExtensionTest

@copyright  (c)2015 Slalom.  All Rights Reserved.
      Unauthorized use is prohibited. 

***********************************************************/
public with sharing class RMS_cancelOrderExtension {


  /******* Set up Standard Controller for Order  *****************/
  private Apexpages.StandardController standardController;
  private final Order theOrder;
  
  /******* Constructor  *****************/
  public RMS_cancelOrderExtension(ApexPages.StandardController stdController) {
    this.theOrder = (Order)stdController.getRecord();
  }
  
  /*******************************************************
          cancelOrder method
  *******************************************************/
  public PageReference cancelOrder() {
    system.debug('*****Cancelling an order');

    String theId = ApexPages.currentPage().getParameters().get('id');
   
    system.debug('*****Cancelling an order theId'+ theId);
    
    
    if (theId == null) {
      // Display the Visualforce page's content if no Id is found
      return null;
    }
    system.debug('*****Cancelling an order theId 15'+ String.valueOf(theId).substring(0,15));
    String theId15 = String.valueOf(theId).substring(0,15);

    // Find the order and cancel it
    Order theOrder = [Select Id, Name, Status,OpportunityId, Apex_Context__c, Cancellation_Reason__c, RecordTypeName__c, Revenue_Recognized_Date__c from Order where id =:theId];
    // Allows the order status to be changed
    theOrder.Apex_Context__c = true;
    theOrder.Status = 'Cancelled';
    theOrder.Date_Cancelled__c = System.Today();
    if(theOrder.RecordTypeName__c == 'Change_Order'){
      theOrder.Revenue_Recognized_Date__c = Date.today();
      theOrder.Change_Order_Rev_Recognized_Date__c = Date.today();
    }


    /*List<WorkOrder> workOrdersWithOpenEvents = [Select Id from WorkOrder WHERE Sold_Order__c =:theOrder.Id 
                                                            AND Number_Open_Events_Formula__c > 0];
        if(!workOrdersWithOpenEvents.isEmpty()){
            System.debug('Error encountered in RMS_CancelOrderExtensions: ' + RMS_errorMessages.WORK_ORDERS_WITH_OPEN_EVENTS);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, RMS_errorMessages.WORK_ORDERS_WITH_OPEN_EVENTS)); 
            return null;
    }*/  
    List<Purchase_Order__c> purchaseOrdersToCancel = new List<Purchase_Order__c>();
    List<OrderItem> orderItemsToCancel = new List<OrderItem>();
    List<Order_Discount__c> discountsToCancel = new List<Order_Discount__c>();
    List<WorkOrder> workOrdersToCancel = new List<WorkOrder>();
    List<Opportunity> OpportunitiesToCancel = new List<Opportunity>();
    List<Task> ListOfTasksToUpdate = new List<Task>();

    // Find any purchase orders linked to the order that are In Process and cancel them
    for (Purchase_Order__c poToCancel : [SELECT Id, Status__c FROM Purchase_Order__c 
                      WHERE Order__c =: theOrder.Id AND  Status__c = 'In Process']) // only cancel PO's that are In Process
    {
      poToCancel.Status__c = 'Cancelled';
      purchaseOrdersToCancel.add(poToCancel);
    }

    // Find any order items linked to the order that haven't been cancelled and cancel them
    for (OrderItem orderItemToCancel : [SELECT Id, Status__c FROM OrderItem 
                      WHERE OrderId =: theOrder.Id AND Status__c != 'Cancelled']) 
    {
      orderItemToCancel.Status__c = 'Cancelled';
      orderItemToCancel.Cancellation_Reason__c = theOrder.Cancellation_Reason__c;
      orderItemsToCancel.add(orderItemToCancel);
    }

    // Find any discounts linked to the order that haven't been cancelled and cancel them
    for (Order_Discount__c discToCancel : [SELECT Id, Status__c FROM Order_Discount__c 
                      WHERE Order__c =: theOrder.Id AND Status__c != 'Cancelled']) 
    {
      discToCancel.Status__c = 'Cancelled';
      discountsToCancel.add(discToCancel);
    }
    set<Id> woIds = new set<Id>();
    // Find any non-FSL WO's linked to the order that haven't been cancelled and aren't Closed
    for(WorkOrder woTocancel : [Select Id,  Status, Work_Order_Type__c, Cancel_Reason__c FROM WorkOrder
                                        WHERE Work_Order_Type__c in ('LSWP','Building Permit','HOA','Historical','Paint/Stain', 'Collections', 'Install', 'Tech Measure', 'Job Site Visit', 'Service')
                                        AND Sold_Order__c =:theOrder.Id 
                                        AND (Status != 'Cancelled' AND Status != 'Closed' AND Status != 'Canceled' AND Status != 'Appt Complete / Closed' 
                                                AND Status != 'Appt Complete/Closed' AND Status != 'Complete')]){
        woToCancel.Status = 'Canceled';
        woIds.add(woTocancel.Id);
        if (woToCancel.Cancel_Reason__c == null) woToCancel.Cancel_Reason__c = 'Order Cancelled';
        workOrdersToCancel.add(woTocancel);
    }
    // Find Opportunity associated to the Order and Update the Canceld Order Checkbox.
            for(Opportunity OppToCancel : [Select id, Canceled_Order__c from Opportunity where id = :theOrder.OpportunityId])
            {
               if(OppToCancel.Canceled_Order__c != TRUE)
              OppToCancel.Canceled_Order__c  = TRUE;
              OpportunitiesToCancel.add(OppToCancel); 
            }
    List<ServiceAppointment> saListToUpdate = new List<ServiceAppointment>();
    List<ServiceAppointment> saList = [SELECT Id, Status, ParentRecordId FROM ServiceAppointment WHERE ParentRecordId IN: woIds];
    System.debug(saList);
    for(ServiceAppointment sa: saList){
      sa.Status = 'Canceled';
      saListToUpdate.add(sa);
    }

    List<Task> taskList = [SELECT Id, Status, Related_to_Formula__c FROM Task WHERE Related_to_Formula__c =: theId15];
    System.debug('taskList: '+taskList);
    for(task t: taskList){
      t.Status = 'Cancelled';
      ListOfTasksToUpdate.add(t);
    }

    try{
      // Prevent rollups/rolldowns on the FSL objects from running prior the last DML
      // FSLRollupRolldownController.disableFSLRollupRolldownController = true;

      // upsert all of the cancelled order items, pos, and the order
      upsert purchaseOrdersToCancel;  
      upsert discountsToCancel; 
      upsert orderItemsToCancel;
      if(theOrder.RecordTypeName__c != 'Change_Order'){
        Upsert OpportunitiesToCancel;
      }
      update workOrdersToCancel; 
      update saListToUpdate;
      if(ListOfTasksToUpdate.size() > 0){
        update ListOfTasksToUpdate;
      }
      // Allow rollups/rolldowns to run at this point
      // FSLRollupRolldownController.disableFSLRollupRolldownController = false;
      upsert theOrder;
    } catch (Exception e){
      // TODO: Add comment here
      System.debug('************The following exception occurred in the RMS_cancelOrderExtension in the cancelOrder method:' + e);
      return null;
    }

    // Redirect the user back to the order page
    PageReference pageRef = new PageReference('/' + theId);
    pageRef.setRedirect(true);
    return pageRef;

  }
}