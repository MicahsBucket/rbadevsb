/*
* @author Jason Flippen
* @date 02/05/2020 
* @description Test Class for the following classes:
*              - ServicePurchaseOrderController
*/
@isTest
public class ServicePurchaseOrderListControllerTest {

    /*
    * @author Jason Flippen
    * @date 02/05/2020 
    * @description Method to create data to be consumed by test methods.
    */ 
    @testSetup
    static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();
        
        Account testVendorAccount = testUtility.createVendorAccount('Test Vendor Account');
        insert testVendorAccount;
        
        Account testStoreAccount = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];

        Store_Vendor__c testStoreVendor = new Store_Vendor__c(Store__c = testStoreAccount.Id,
                                                              Vendor__c = testVendorAccount.Id);
        insert testStoreVendor;
        
        Account testDwellingAccount = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAccount.Store_Location__c = testStoreAccount.Id;
        insert testDwellingAccount;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAccount.Id, 'Closed - Won');
        insert testOpportunity;
    
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;

        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId(),
                                            Vendor__c = testVendorAccount.Id,
                                            Service_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c = testFan.Id);
        insert testProduct;

        Vendor_Product__c testVendorProduct = new Vendor_Product__c(Vendor__c = testVendorAccount.Id,
                                                                    Product__c = testProduct.Id);
        insert testVendorProduct;
        
        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order',
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Service').getRecordTypeId(),
                                     AccountId = testDwellingAccount.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAccount.Id,
                                     OpportunityId = testOpportunity.Id,                                 
                                     Status = 'Draft',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;
        
        OrderItem testOrderItem = new OrderItem(OrderId = testOrder.Id,
                                                PricebookentryId = testPBEStandard.Id,
                                                Quantity = 2,
                                                Quanity_Ordered__c = 2,
                                                Unit_Wholesale_Cost__c = 50.00,
                                                UnitPrice = 100,
                                                Variant_Number__c = 'ABCD1234');
        insert testOrderItem;
        
        Charge__c testCharge = new Charge__c(Service_Request__c = testOrder.Id,
                                             Service_Product__c = testOrderItem.Id,
                                             Charge_Cost_To__c = 'Manufacturing');
        insert testCharge;

        Purchase_Order__c testPurchaseOrder = new Purchase_Order__c(Name = 'ABCD12345',
                                                                    Order__c = testOrder.Id,
                                                                    RecordTypeId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Service_Purchase_Order').getRecordTypeId());
        insert testPurchaseOrder;


    }

    /*
    * @author Jason Flippen
    * @date 02/05/2020 
    * @description Method to test the functionality in the Controller.
    */ 
    static testMethod void testController() {

        Order testOrder = [SELECT Id, Status FROM Order WHERE Name = 'Test Order'];

        Test.startTest();

            // Retrieve List of Purchase Order records.  There should only be one.
            List<ServicePurchaseOrderListController.PurchaseOrderWrapper> purchaseOrderList = ServicePurchaseOrderListController.getPurchaseOrderData(testOrder.Id, '/lightning/r/Purchase_Order__c/');
            System.assertEquals(1, purchaseOrderList.size());

            List<ServicePurchaseOrderListController.VendorWrapper> vendorList = ServicePurchaseOrderListController.getVendorList(testOrder.Id);
            System.assertEquals(1, vendorList.size());

            List<ServicePurchaseOrderListController.ResponsibilityWrapper> responsibilityList = vendorList[0].responsibilityList;
            System.assertEquals(1, responsibilityList.size());

            List<ServicePurchaseOrderListController.ChargeWrapper> chargeList = vendorList[0].responsibilityList[0].chargeList;
            System.assertEquals(1, chargeList.size());

            Map<String,String> resultMap = ServicePurchaseOrderListController.createServicePO(chargeList, vendorList[0].value, testOrder.Id);
            String servicePOResult;
            for (String result : resultMap.keySet()) {
                servicePOResult = result;
            }
            System.assertEquals('New Service PO Success', servicePOResult);

            Purchase_Order__c newCostPO = ServicePurchaseOrderListController.getNewCostPOData(testOrder.Id);
            Map<String,String> costPOResultMap = ServicePurchaseOrderListController.createCostPO(newCostPO);
            String costPOResult;
            for (String result : costPOResultMap.keySet()) {
                costPOResult = result;
            }
            System.assertEquals('New Cost PO Success', costPOResult);

        Test.stopTest();

    }

}