@isTest
private class TeamSchedulingControllerTest {

    private static final String WORK_TYPE_NAME = 'Test Type 1';
    private static final String INSTALL_WORK_TYPE_NAME = 'Install';
    private static final String MEASURE_WORK_TYPE_NAME = 'Measure';
    private static final String SERVICE_WORK_TYPE_NAME = 'Service';
    private static final String JOB_SITE_VISIT_WORK_TYPE_NAME = 'Job Site Visit';
    private static final String SERVICE_TERRITORY1_NAME = 'Test Territory 1';
    private static final String SERVICE_TERRITORY2_NAME = 'Test Territory 2';
    private static final String SERVICE_TERRITORY3_NAME = 'Test Territory 3';
    private static final String SERVICE_TERRITORY4_NAME = 'Test Territory 4';
    private static final String RESOURCE1_FIRST_NAME = 'Kaylee';
    private static final String RESOURCE1_LAST_NAME = 'Torres';
    private static final String RESOURCE2_FIRST_NAME = 'Harry';
    private static final String RESOURCE2_LAST_NAME = 'Hanson';
    private static final String CHILD_GROUP_NAME = 'ChildGroup';

    @testSetup
    static void testSetup() {
        // FSLRollupRolldownController.disableFSLRollupRolldownController = true;

        insert new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');

        Store_Configuration__c storeConfig = createStoreConfig();
        insert storeConfig;

        User user1 = createUser(RESOURCE1_FIRST_NAME, RESOURCE1_LAST_NAME);
        User user2 = createUser(RESOURCE2_FIRST_NAME, RESOURCE2_LAST_NAME);
        insert new List<User> { user1, user2 };

        ServiceResource resource1 = createServiceResource(user1, storeConfig);
        ServiceResource resource2 = createServiceResource(user2, storeConfig);
        insert new List<ServiceResource> { resource1, resource2 };

        List<FSL__Scheduling_Policy__c> policies = new List<FSL__Scheduling_Policy__c>();
        policies.add(new FSL__Scheduling_Policy__c(Name = 'Customer First'));
        policies.add(new FSL__Scheduling_Policy__c(Name = 'RbA Policy'));
        insert policies;

        OperatingHours operatingHours = new OperatingHours(
            Name = 'Test Hours 1'
        );
        insert operatingHours;

        TimeSlot ts1 = new TimeSlot();
        ts1.OperatingHoursId = operatingHours.Id;
        ts1.StartTime = Time.newInstance(6, 0, 0, 0);
        ts1.EndTime = Time.newInstance(14, 0, 0, 0);
        ts1.DayOfWeek = 'Monday';
        ts1.Type = 'Normal';

        TimeSlot ts2 = new TimeSlot();
        ts2.OperatingHoursId = operatingHours.Id;
        ts2.StartTime = Time.newInstance(6, 0, 0, 0);
        ts2.EndTime = Time.newInstance(14, 0, 0, 0);
        ts2.DayOfWeek = 'Tuesday';
        ts2.Type = 'Normal';

        TimeSlot ts3 = new TimeSlot();
        ts3.OperatingHoursId = operatingHours.Id;
        ts3.StartTime = Time.newInstance(6, 0, 0, 0);
        ts3.EndTime = Time.newInstance(14, 0, 0, 0);
        ts3.DayOfWeek = 'Wednesday';
        ts3.Type = 'Normal';

        TimeSlot ts4 = new TimeSlot();
        ts4.OperatingHoursId = operatingHours.Id;
        ts4.StartTime = Time.newInstance(6, 0, 0, 0);
        ts4.EndTime = Time.newInstance(14, 0, 0, 0);
        ts4.DayOfWeek = 'Thursday';
        ts4.Type = 'Normal';

        TimeSlot ts5 = new TimeSlot();
        ts5.OperatingHoursId = operatingHours.Id;
        ts5.StartTime = Time.newInstance(6, 0, 0, 0);
        ts5.EndTime = Time.newInstance(14, 0, 0, 0);
        ts5.DayOfWeek = 'Friday';
        ts5.Type = 'Normal';

        insert new List<TimeSlot>{ts1, ts2, ts3, ts4, ts5};

        WorkType workType = new WorkType(
            Name = WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 4,
            DurationType = 'Hours'
        );
        
        WorkType installWorkType = new WorkType(
            Name = INSTALL_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType measureWorkType = new WorkType(
            Name = MEASURE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );
        
        WorkType serviceWorkType = new WorkType(
            Name = SERVICE_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 8,
            DurationType = 'Hours'
        );

        WorkType jobSiteVisitWorkType = new WorkType(
            Name = JOB_SITE_VISIT_WORK_TYPE_NAME,
            MinimumCrewSize = 1,
            RecommendedCrewSize = 2,
            EstimatedDuration = 1,
            DurationType = 'Hours'
        );
        insert new List<WorkType>{worktype, installWorkType, measureWorkType, serviceWorkType, jobSiteVisitWorkType};

        ServiceTerritory territory1 = new ServiceTerritory(
            IsActive = true,
            Name = SERVICE_TERRITORY1_NAME,
            OperatingHoursId = operatingHours.Id
        );
        ServiceTerritory territory2 = new ServiceTerritory(
            IsActive = false,
            Name = SERVICE_TERRITORY2_NAME,
            OperatingHoursId = operatingHours.Id
        );
        ServiceTerritory territory3 = new ServiceTerritory(
            IsActive = true,
            Name = SERVICE_TERRITORY3_NAME,
            OperatingHoursId = operatingHours.Id
        );
        ServiceTerritory territory4 = new ServiceTerritory(
            IsActive = true,
            Name = SERVICE_TERRITORY4_NAME,
            OperatingHoursId = operatingHours.Id
        );
        insert new List<ServiceTerritory> { territory1, territory2, territory3, territory4 };
        territory1.ParentTerritoryId = territory3.Id;
        update territory1;

        insert new List<ServiceTerritoryMember> {
            createTerritoryMember(territory1, resource1),
            createTerritoryMember(territory1, resource2)
        };
        WorkOrder wo = new WorkOrder(
            Recommended_Crew_Size__c = 2,
            Street = '137 5th St N',
            City = 'Bayport',
            State = 'Minnesota',
            PostalCode = '55003-1113',
            CountryCode = 'US',
            WorkTypeId = workType.Id,
            ServiceTerritoryId = territory1.Id
        );
        insert wo;
        WorkOrder installWO = new WorkOrder(
            Recommended_Crew_Size__c = 2,
            Street = '137 5th St N',
            City = 'Bayport',
            State = 'Minnesota',
            PostalCode = '55003-1113',
            CountryCode = 'US',
            WorkTypeId = installWorkType.Id,
            ServiceTerritoryId = territory1.Id
        );
        insert installWO;
        WorkOrder measureWO = new WorkOrder(
            Recommended_Crew_Size__c = 2,
            Street = '137 5th St N',
            City = 'Bayport',
            State = 'Minnesota',
            PostalCode = '55003-1113',
            CountryCode = 'US',
            WorkTypeId = measureWorkType.Id,
            ServiceTerritoryId = territory1.Id
        );
        insert measureWO;
        WorkOrder serviceWO = new WorkOrder(
            Recommended_Crew_Size__c = 2,
            Street = '137 5th St N',
            City = 'Bayport',
            State = 'Minnesota',
            PostalCode = '55003-1113',
            CountryCode = 'US',
            WorkTypeId = serviceWorkType.Id,
            ServiceTerritoryId = territory1.Id
        );
        insert serviceWO;
//        insert new list<WorkOrder>{wo,installWO,measureWO,serviceWO};

        ServiceAppointment appointment = new ServiceAppointment(ParentRecordId = installWO.Id,
                                                                SchedStartTime = Datetime.now(),
                                                                SchedEndTime = Datetime.now().addHours(4));
        insert appointment;

        AssignedResource assignedResource = new AssignedResource(ServiceResourceId = resource1.Id,
                                                                 ServiceAppointmentId = appointment.Id,
                                                                 Scheduled_Via_Custom_Component__c = true);
        insert assignedResource;

        // FSLRollupRolldownController.disableFSLRollupRolldownController = false;
    }

    @isTest
    static void constructorTest() {
        // Given
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        Test.setCurrentPageReference(Page.TeamScheduling);
        System.currentPageReference().getParameters().put('id', workOrderId);

        // When
        TeamSchedulingController controller = new TeamSchedulingController();

        // Then
        System.assertEquals(workOrderId, controller.workOrderId);
    }

/* commenting out for deployment of rurd refactor. 12-12-20 tests failing during deployment but passing in all sandboxes/prod..MTR
    @isTest
    static void getTimeSlotsTest() {
        // Given
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        ServiceAppointment appointment = TeamSchedulingController.getOrCreateServiceAppointment(workOrderId);
        insert appointment;

        // When
        Test.startTest();

        List<TeamSchedulingController.TimeSlot> timeSlots = TeamSchedulingController.getTimeSlots(appointment.Id, workOrderId);

        Test.stopTest();
    }
	
    
    @isTest
    static void getTimeSlotsForCandidatesTest() {
        // Given
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        ServiceAppointment appointment = TeamSchedulingController.getOrCreateServiceAppointment(workOrderId);
        List<Id> resourceIds = new List<Id>(new Map<Id, ServiceResource>([SELECT Id FROM ServiceResource]).keySet());
        insert appointment;

        // When
        Test.startTest();
        Date today = Date.today();
        Date finishDate = today.addDays(5);
        Datetime startTime = Datetime.newInstanceGmt(today.year(), today.month(), today.day());
        Datetime finishTime = Datetime.newInstanceGmt(finishDate.year(), finishDate.month(), finishDate.day());
        List<TeamSchedulingController.TimeSlot> timeSlots = TeamSchedulingController.getTimeSlotsForCandidates(workOrderId, appointment.Id, resourceIds, startTime, finishTime);

        Test.stopTest();
    }

    
    @isTest
    static void getCandidatesTest() {
        // Given
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        ServiceAppointment appointment = TeamSchedulingController.getOrCreateServiceAppointment(workOrderId);
        insert appointment;

        // When
        Test.startTest();

        List<AvailableResource> availableResources = TeamSchedulingController.getCandidates(appointment.Id, workOrderId, Datetime.now(), Datetime.now().addHours(2));

        Test.stopTest();
    }

    @isTest
    static void getAvailableCandidatesTest() {
        // Given
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        ServiceAppointment appointment = TeamSchedulingController.getOrCreateServiceAppointment(workOrderId);
        insert appointment;

        // When
        Test.startTest();

        List<AvailableResource> availableResources = TeamSchedulingController.getAvailableCandidates(appointment.Id, Datetime.now(), Datetime.now().addHours(2));

        Test.stopTest();
    }
*/
    @isTest
    static void getWorkOrderTest() {
        // Given
        Id workOrderId = [SELECT Id FROM WorkOrder WHERE WorkType.Name = :INSTALL_WORK_TYPE_NAME LIMIT 1].Id;

        // When
        Test.startTest();

        WorkOrder wo = TeamSchedulingController.getWorkOrder(workOrderId);

        Test.stopTest();

        // Then
        System.assertEquals(workOrderId, wo.Id);
        System.assertEquals(2, wo.Recommended_Crew_Size__c);
        System.assertEquals('137 5th St N', wo.Street);
        System.assertEquals('Bayport', wo.City);
        System.assertEquals('Minnesota', wo.State);
        System.assertEquals('55003-1113', wo.PostalCode);
        System.assertEquals('US', wo.CountryCode);
        System.assertEquals(INSTALL_WORK_TYPE_NAME, wo.WorkType.Name);
        System.assertEquals(SERVICE_TERRITORY1_NAME, wo.ServiceTerritory.Name);
    }

    @isTest
    static void getOrCreateServiceAppointmentTestNoAppointments() {
        // Given
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;

        // When
        Test.startTest();

        ServiceAppointment appointment = TeamSchedulingController.getOrCreateServiceAppointment(workOrderId);

        Test.stopTest();

        // Then
        System.assertEquals(null, appointment.Id);
        System.assertEquals(workOrderId, appointment.ParentRecordId);
    }

    @isTest
    static void getServiceTerritoriesTest() {
        User u = [SELECT Id FROM User WHERE FirstName = :RESOURCE1_FIRST_NAME AND LastName = :RESOURCE1_LAST_NAME];

        // Create nested groups
        Group g1 = new Group(
            Name = CHILD_GROUP_NAME,
            DeveloperName = CHILD_GROUP_NAME,
            Type = 'Regular'
        );
        Group g2 = new Group(
            Name = SERVICE_TERRITORY1_NAME,
            DeveloperName = 'ParentGroup',
            Type = 'Regular'
        );
        insert new List<Group>{g1,g2};

        // Set User1 to belong to the child group, g1
        GroupMember gm1 = new GroupMember(
            GroupId = g1.Id,
            UserOrGroupId = u.Id
        );
        // Set the child group g1 to belong to the parent group g2, i.e. the service territory group
        GroupMember gm2 = new GroupMember(
            GroupId = g2.Id,
            UserOrGroupId = g1.Id
        );
        insert new List<GroupMember>{gm1, gm2};

        Test.startTest();

        List<ServiceTerritory> territories = TeamSchedulingController.getServiceTerritoriesForUser(null, u.Id);
        system.debug(territories);
        Test.stopTest();

        territories.sort();
        System.assertEquals(2, territories.size()); 
        System.assertEquals(SERVICE_TERRITORY1_NAME, territories[0].Name); // match territory 1 via nested group
        System.assertEquals(SERVICE_TERRITORY3_NAME, territories[1].Name); // match territory 3 as parent of territory 1
    }

    @isTest
    static void getFSLSettingsTest() {
        TeamSchedulingController.FSLSettings settings = TeamSchedulingController.getFSLSettings();

        System.assertNotEquals(null, settings.idealThreshold);
        System.assertNotEquals(null, settings.recommendedThreshold);
        System.assertNotEquals(null, settings.initialAppointmentSearchHours);
    }

    @isTest
    static void saveAppointmentTest() {
        // Given
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        ServiceAppointment appointment = new ServiceAppointment(
            ParentRecordId = workOrderId,
            EarliestStartTime = Date.today(),
            DueDate = Date.today().addDays(3)
        );

        // When
        Test.startTest();

        appointment = TeamSchedulingController.saveAppointment(appointment);

        Test.stopTest();

        System.assertNotEquals(null, appointment.Id);
    }

    @isTest
    static void saveAppointmentWithoutRollupTest() {
        // Given
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        ServiceAppointment appointment = new ServiceAppointment(
            ParentRecordId = workOrderId,
            EarliestStartTime = Date.today(),
            DueDate = Date.today().addDays(3)
        );

        // When
        Test.startTest();

        appointment = TeamSchedulingController.saveAppointmentWithoutRollup(appointment);

        Test.stopTest();

        System.assertNotEquals(null, appointment.Id);
    }

    @isTest
    static void extendDateTest() {
        // Given
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        Datetime initialDueDate = Datetime.now().addDays(3);
        ServiceAppointment appointment = new ServiceAppointment(
            ParentRecordId = workOrderId,
            DueDate = initialDueDate
        );

        // When
        Test.startTest();

        appointment = TeamSchedulingController.extendDate(appointment);

        Test.stopTest();

        System.assertEquals(initialDueDate.addDays(1), appointment.DueDate);
    }

    @isTest
    static void createAppointmentsTestSingleResource() {
        // Given
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        ServiceResource resource1 = [SELECT Id FROM ServiceResource WHERE Name = :RESOURCE1_LAST_NAME];
        ServiceAppointment appointment = TeamSchedulingController.getOrCreateServiceAppointment(workOrderId);
        insert appointment;
        Datetime startTime = Datetime.now().addDays(1);
        startTime = startTime.addSeconds(-startTime.second());
        Datetime endTime = startTime.addHours(1);

        // When
        Test.startTest();

        List<ServiceAppointment> appointments = TeamSchedulingController.createAppointments(appointment.Id, startTime, endTime, new List<Id> { resource1.Id });

        Test.stopTest();

        // Then

        System.assertEquals(1, appointments.size());
        System.assertEquals(startTime, appointments[0].SchedStartTime);
        System.assertEquals(endTime, appointments[0].SchedEndTime);
    }

    @isTest
    static void createAppointmentsTestMultipleResources() {
        Test.startTest();
        // Given
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        ServiceResource resource1 = [SELECT Id FROM ServiceResource WHERE Name = :RESOURCE1_LAST_NAME];
        ServiceResource resource2 = [SELECT Id FROM ServiceResource WHERE Name = :RESOURCE2_LAST_NAME];
        ServiceAppointment appointment = TeamSchedulingController.getOrCreateServiceAppointment(workOrderId);
        insert appointment;
        Datetime startTime = Datetime.now().addDays(1);
        startTime = startTime.addSeconds(-startTime.second());
        Datetime endTime = startTime.addHours(1);

        // When
        

        List<ServiceAppointment> appointments = TeamSchedulingController.createAppointments(appointment.Id, startTime, endTime, new List<Id> { resource1.Id, resource2.Id });

        Test.stopTest();

        // Then

        System.assertEquals(2, appointments.size());
        System.assertEquals(startTime, appointments[0].SchedStartTime);
        System.assertEquals(endTime, appointments[0].SchedEndTime);
        System.assertEquals(startTime, appointments[1].SchedStartTime);
        System.assertEquals(endTime, appointments[1].SchedEndTime);
    }

    @isTest
    static void rescheduleAppointmentsTestSameResource() {
        // Given
        
        Id workOrderId = [SELECT Id FROM WorkOrder WHERE WorkType.Name = :MEASURE_WORK_TYPE_NAME LIMIT 1].Id;
        ServiceResource resource = [SELECT Id FROM ServiceResource WHERE Name = :RESOURCE1_LAST_NAME];
        ServiceAppointment appointment = TeamSchedulingController.getOrCreateServiceAppointment(workOrderId);
        insert appointment;
        appointment.Status = 'Scheduled';
        appointment.FSL__IsMultiDay__c = false;

        Test.startTest();
        update appointment;
        Datetime startTime = Datetime.now().addDays(1);
        startTime = startTime.addSeconds(-startTime.second());
        Datetime endTime = startTime.addHours(8);

        
        List<ServiceAppointment> appointments = TeamSchedulingController.rescheduleAppointments(workOrderId, startTime, endTime, new List<Id> { resource.Id });

        Test.stopTest();

        System.assertEquals(startTime, appointments[0].SchedStartTime);
        System.assertEquals(endTime, appointments[0].SchedStartTime.addMinutes(Integer.valueOf(appointments[0].Duration * 60)));
    }

    @isTest
    static void rescheduleAppointmentsTestMultipleResources() {
        // Given
        
        Id workOrderId = [SELECT Id FROM WorkOrder WHERE WorkType.Name = :MEASURE_WORK_TYPE_NAME LIMIT 1].Id;
        ServiceResource resource1 = [SELECT Id FROM ServiceResource WHERE Name = :RESOURCE1_LAST_NAME];
        ServiceResource resource2 = [SELECT Id FROM ServiceResource WHERE Name = :RESOURCE2_LAST_NAME];
        ServiceAppointment appointment = TeamSchedulingController.getOrCreateServiceAppointment(workOrderId);
        insert appointment;
        appointment.Status = 'Scheduled';
        appointment.FSL__IsMultiDay__c = false;

        Test.startTest();
        update appointment;
        Datetime startTime = Datetime.now().addDays(1);
        startTime = startTime.addSeconds(-startTime.second());
        Datetime endTime = startTime.addHours(8);

        
        List<ServiceAppointment> appointments = TeamSchedulingController.rescheduleAppointments(workOrderId, startTime, endTime, new List<Id> { resource1.Id, resource2.Id });

        Test.stopTest();

        System.assertEquals(startTime, appointments[0].SchedStartTime);
        System.assertEquals(endTime, appointments[0].SchedStartTime.addMinutes(Integer.valueOf(appointments[0].Duration * 60)));
        System.assertEquals(startTime, appointments[1].SchedStartTime);
        System.assertEquals(endTime, appointments[1].SchedStartTime.addMinutes(Integer.valueOf(appointments[0].Duration * 60)));
    }

    @isTest
    static void assignResourceTest() {

        Test.startTest();
        Id workOrderId = [SELECT Id FROM WorkOrder LIMIT 1].Id;
        List<ServiceResource> resources = [SELECT Id FROM ServiceResource WHERE Name = :RESOURCE1_LAST_NAME OR Name = :RESOURCE2_LAST_NAME];
        ServiceAppointment appointment = new ServiceAppointment(ParentRecordId = workOrderId);
        appointment.SchedStartTime = Datetime.now();
        appointment.SchedEndTime = Datetime.now().addHours(4);
        insert appointment;

        AssignedResource assignedResource = TeamSchedulingController.assignResource(appointment.Id, resources[0].Id);
        
        System.assertNotEquals(null, assignedResource.Id);
        assignedResource = [SELECT ServiceResourceId, Scheduled_Via_Custom_Component__c FROM AssignedResource WHERE Id = :assignedResource.Id];
        System.assertEquals(true, assignedResource.Scheduled_Via_Custom_Component__c);

    }

    static testMethod void updateAssignedResourceTest() {

        Test.startTest();

            Id workOrderId = [SELECT Id FROM WorkOrder WHERE WorkType.Name = :INSTALL_WORK_TYPE_NAME].Id;
            Id currentResourceId = null;
            Id newResourceId = null;
            for (ServiceResource sr : [SELECT Id, Name FROM ServiceResource WHERE Name = :RESOURCE1_LAST_NAME OR Name = :RESOURCE2_LAST_NAME]) {
                if (sr.Name == RESOURCE1_LAST_NAME) {
                    currentResourceId = sr.Id;
                }
                else if (sr.Name == RESOURCE2_LAST_NAME) {
                    newResourceId = sr.Id;
                }
            }
            Id serviceAppointmentId = [SELECT Id FROM ServiceAppointment WHERE ParentRecordId = :workOrderId].Id;
            AssignedResource assignedResource = [SELECT Id, ServiceResourceId
                                                 FROM   AssignedResource
                                                 WHERE  ServiceResourceId = :currentResourceId
                                                 AND    ServiceAppointmentId = :serviceAppointmentId];
            
            assignedResource.ServiceResourceId = newResourceId;
            update assignedResource;

        Test.stopTest();

        assignedResource = [SELECT ServiceResourceId, Scheduled_Via_Custom_Component__c FROM AssignedResource WHERE Id = :assignedResource.Id];
        System.assertEquals(false, assignedResource.Scheduled_Via_Custom_Component__c);
  }

    @isTest
    static void timeSlotTest() {
        TeamSchedulingController.TimeSlot timeSlot = new TeamSchedulingController.TimeSlot();
        System.assertNotEquals(null, timeSlot.resources);
    }

    @isTest
    static void availableResourceTest() {
        ServiceResource sr = [SELECT Id, Name, RelatedRecord.SmallPhotoUrl FROM ServiceResource WHERE Name = :RESOURCE1_LAST_NAME];
        Decimal grade = 95.6;
        AvailableResource availableResource = new AvailableResource(sr, grade);

        System.assertEquals(grade, availableResource.grade);
    }

    
    @isTest
    static void setPrimaryInstallerResourceTest() {
        // GIVEN
        ServiceResource resource = [SELECT Id FROM ServiceResource WHERE Name = :RESOURCE1_LAST_NAME];
        WorkType installWorkType = [SELECT Id FROM WorkType WHERE Name = :INSTALL_WORK_TYPE_NAME];
        WorkOrder wo = [SELECT Id FROM WorkOrder WHERE WorkTypeId = :installWorkType.Id];

        // WHEN
        Test.startTest();
            TeamSchedulingController.setPrimaryResource(wo.Id, resource.Id);
        Test.stopTest();

        // THEN
        WorkOrder result = [SELECT Id, Primary_Installer_FSL__c FROM WorkOrder WHERE Id = :wo.Id];
        System.assertEquals(resource.Id, result.Primary_Installer_FSL__c);
    }
	
    @isTest
    static void setPrimaryMeasureResourceTest() {
        // GIVEN
        ServiceResource resource = [SELECT Id FROM ServiceResource WHERE Name = :RESOURCE1_LAST_NAME];
        WorkType measureWorkType = [SELECT Id FROM WorkType WHERE Name = :MEASURE_WORK_TYPE_NAME];
        WorkOrder wo = [SELECT Id FROM WorkOrder WHERE WorkTypeId = :measureWorkType.Id];

        // WHEN
        Test.startTest();
            TeamSchedulingController.setPrimaryResource(wo.Id, resource.Id);
        Test.stopTest();

        // THEN
        WorkOrder result = [SELECT Id, Primary_Tech_Measure_FSL__c FROM WorkOrder WHERE Id = :wo.Id];
        System.assertEquals(resource.Id, result.Primary_Tech_Measure_FSL__c);
    }
	
    @isTest
    static void setPrimaryServiceResourceTest() {

        ServiceResource resource = [SELECT Id FROM ServiceResource WHERE Name = :RESOURCE1_LAST_NAME];
        WorkType serviceWorkType = [SELECT Id FROM WorkType WHERE Name = :SERVICE_WORK_TYPE_NAME];
        WorkOrder wo = [SELECT Id, WorkOrderNumber FROM WorkOrder WHERE WorkTypeId = :serviceWorkType.Id];
        ServiceAppointment appointment = new ServiceAppointment(ParentRecordId = wo.Id);
        appointment.SchedStartTime = Datetime.now();
        appointment.SchedEndTime = Datetime.now().addHours(4);
        appointment.Status = 'Scheduled';
        insert appointment;

        AssignedResource assignedResource = TeamSchedulingController.assignResource(appointment.Id, resource.Id);

        Test.startTest();
            TeamSchedulingController.setPrimaryResource(wo.Id, resource.Id);
        	TeamSchedulingController.AssignedResourcesWrapper resultsWrapper = TeamSchedulingController.getAssignedResources(wo.Id);        
        Test.stopTest();

        WorkOrder result = [SELECT Id, Primary_Service_FSL__c FROM WorkOrder WHERE Id = :wo.Id];
        AssignedResource arResult = [SELECT Id, Is_Primary_Resource_On_Work_Order__c, ServiceResourceId FROM AssignedResource WHERE Id = :AssignedResource.Id];

        System.assertEquals(resource.Id, result.Primary_Service_FSL__c);
        System.assertEquals(true, arResult.Is_Primary_Resource_On_Work_Order__c);

        // Also test getAssignedResources here:
        // moved get assigned resources into test.starttest above. - mtr
 //       TeamSchedulingController.AssignedResourcesWrapper resultsWrapper = TeamSchedulingController.getAssignedResources(wo.Id);
        System.assertEquals(1,resultsWrapper.assignedResources.size());
        System.assertEquals(arResult.ServiceResourceId,resultsWrapper.primaryServiceResourceId);
        System.assertEquals(wo.WorkOrderNumber,resultsWrapper.workOrderNumber);
        
    }
	
    @isTest 
    static void updateWorkOrderDurationTest() {
        WorkOrder wo = [SELECT Id FROM WorkOrder LIMIT 1];

        Test.startTest();
        TeamSchedulingController.updateWorkOrderDuration(wo.Id, Decimal.valueOf(3));
        Test.stopTest();

        WorkOrder woResult = [SELECT Id, Duration FROM WorkOrder WHERE Id = :wo.Id];
        System.assertEquals(Decimal.valueOf(3), woResult.Duration);
    }

    /**************************
     *      SETUP METHODS
     **************************/

     private static User createUser(String firstName, String lastName) {
        User u = new User(
            FirstName = firstName,
            LastName = lastName,
            Email = firstName + '.' + lastName + '@example.com',
            UserName = firstName + '.' + lastName + '@example.com' + (Math.random() * 1000000).intValue(),
            CompanyName = 'ABC Company',
            Alias = 'tuser',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            TimeZoneSidKey = 'America/Chicago',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        return u;
     }

     private static Store_Configuration__c createStoreConfig() {
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;

        Store_Configuration__c storeConfig = new Store_Configuration__c(
            Store__c = store.Id,
            Order_Number__c = 1
        );
        return storeConfig;
     }

     private static ServiceResource createServiceResource(User u, Store_Configuration__c storeConfig) {
        ServiceResource sr = new ServiceResource(
            Name = u.LastName,
            RelatedRecordId = u.Id,
            Retail_Location__c = storeConfig.Id,
            IsActive = true
        );
        return sr;
     }

     private static ServiceTerritoryMember createTerritoryMember(ServiceTerritory territory, ServiceResource resource) {
        ServiceTerritoryMember member = new ServiceTerritoryMember(
            ServiceTerritoryId = territory.Id,
            ServiceResourceId = resource.Id,
            TerritoryType = 'P',
            EffectiveStartDate = Date.today().addDays(-30),
            EffectiveEndDate = Date.today().addDays(30)
        );
        return member;
     }
}