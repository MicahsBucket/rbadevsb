@isTest //(seeAllData=true)
public class BatchJobOrderHistoryTrackingTest {
    @isTest
    static void test_BatchJobOrderHistoryTracking() {
        Id nonServiceRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Change Order').getRecordTypeId(); //default looks like 'Change Order'
        Id serviceRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Service').getRecordTypeId();
        //Create an order to serve as a source for creating more test data.
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.createOrderTestRecords();
        Order sourceOrder = [SELECT Id,Name,AccountId,EffectiveDate,Store_Location__c,BillToContactId,OpportunityId,Status,Pricebook2Id,RecordTypeId,Apex_Context__c,Service_Request_Hold_Reason__c FROM Order][0];
        system.debug('source Order in batch job order histroy tracking test***' + sourceOrder);
        List<Id> testOrderIds = new List<Id>();
        testOrderIds.add(sourceOrder.Id);
        Order sourceServiceOrder;
        Order sourceNonServiceOrder;
        if(sourceOrder.recordTypeId == serviceRecordTypeId){
            //set the service order source
            sourceServiceOrder = sourceOrder;
            //create a new non-service order
            sourceNonServiceOrder = sourceServiceOrder.clone();
            sourceNonServiceOrder.recordTypeId = nonServiceRecordTypeId;
            insert sourceNonServiceOrder;
            testOrderIds.add(sourceNonServiceOrder.Id);
        }else{
            //set the non-service order source
            sourceNonServiceOrder = sourceOrder;
            //create a new non-service order
            sourceServiceOrder = sourceNonServiceOrder.clone();
            sourceServiceOrder.recordTypeId = serviceRecordTypeId;
            insert sourceServiceOrder;
            testOrderIds.add(sourceServiceOrder.Id);
        }
        system.debug('service Order in batch job order histroy tracking test*** '+sourceServiceOrder);
        system.debug('non service Order in batch job order histroy tracking test*** '+sourceNonServiceOrder);

        //Execute testing of the BatchJobOrderHistoryTracking Batch Class
        Test.startTest();
        BatchJobOrderHistoryTracking bc = new BatchJobOrderHistoryTracking();
        Database.executeBatch(bc);
        Test.stopTest();

        //Assert Equals: Make sure we have data in the DateTime fields.
        List<Order> ords = [SELECT Id, status, recordTypeId, lastmodifieddate, Time_New__c ,Time_Product_Ordered__c ,Time_Service_Scheduled__c ,Time_Warranty_Submitted__c ,Time_Warranty_Rejected__c ,Time_Service_on_Hold__c ,Time_Customer_Call_Back__c ,Time_Service_to_be_Scheduled__c ,Time_Service_Complete__c ,Time_To_Be_Ordered__c ,Time_Seasonal_Service__c ,Time_Quote__c ,Time_Draft__c,Time_On_Hold__c,Time_Tech_Measure_Needed__c ,Time_Tech_Measure_Scheduled__c ,Time_Ready_To_Order__c ,Time_Order_Released__c ,Time_Install_Needed__c ,Time_Install_Scheduled__c ,Time_Install_Complete__c ,Time_Job_In_Progress__c ,Time_Job_Close__c ,Job_Close_Date__c ,Time_Pending_Cancellation__c ,Time_Cancelled__c, Order_Processed_Date__c  FROM Order WHERE Id IN :testOrderIds];
        //-assert 2 orders were returned.
        system.assertEquals(2, ords.size(), 'Expected to get exactly 2 orders to verify against. Found ' + String.valueOf(ords.size()));
        //-assert that our fields are now populated in the orders
        for(Order o : ords){
            if(o.Id == sourceServiceOrder.Id){
                //--assert service order time fields are not blank.
             /*   system.assertNotEquals(o.Time_New__c, null);
                system.assertNotEquals(o.Time_Product_Ordered__c, null);
                system.assertNotEquals(o.Time_Service_Scheduled__c, null);
                system.assertNotEquals(o.Time_Warranty_Submitted__c, null);
                system.assertNotEquals(o.Time_Warranty_Rejected__c, null);
                system.assertNotEquals(o.Time_Service_on_Hold__c, null);
                system.assertNotEquals(o.Time_Customer_Call_Back__c, null);
                system.assertNotEquals(o.Time_Service_to_be_Scheduled__c, null);
                system.assertNotEquals(o.Time_Service_Complete__c, null);
                system.assertNotEquals(o.Time_To_Be_Ordered__c, null);
                system.assertNotEquals(o.Time_Seasonal_Service__c, null);
                system.assertNotEquals(o.Time_Quote__c, null);           
				*/
            }else{
                //--assert non-service order time fields are not blank.
             /* system.assertNotEquals(o.Time_Draft__c, null);
                system.assertNotEquals(o.Time_On_Hold__c, null);
                system.assertNotEquals(o.Time_Tech_Measure_Needed__c, null);
                system.assertNotEquals(o.Time_Tech_Measure_Scheduled__c, null);
                system.assertNotEquals(o.Time_Ready_To_Order__c, null);
                system.assertNotEquals(o.Time_Order_Released__c, null);
                system.assertNotEquals(o.Time_Install_Needed__c, null);
                system.assertNotEquals(o.Time_Install_Scheduled__c, null);
                system.assertNotEquals(o.Time_Install_Complete__c, null);
                system.assertNotEquals(o.Time_Job_Close__c, null);
                system.assertNotEquals(o.Job_Close_Date__c, null);
                system.assertNotEquals(o.Time_Pending_Cancellation__c, null);
                system.assertNotEquals(o.Time_Cancelled__c, null);
                system.assertNotEquals(o.Time_Job_In_Progress__c, null);
				*/
            }
        }
    }

    /**
      * @description Creates mock OrderHistory for testing BatchJobOrderHistoryTracking. History is not created during test execution.
      * @param OrderId Id of the order to create mock data for
      * @param isServiceOrderRecordType Used to test the differing statuses between Service and Non-Service Orders.
      * @return
      * @example
      */
    public static List<BatchJobOrderHistoryTracking.OrderHistoryCls> getTestOrderHistoryData(Id OrderId,Boolean isServiceOrderRecordType){
        List<BatchJobOrderHistoryTracking.OrderHistoryCls> retData = new List<BatchJobOrderHistoryTracking.OrderHistoryCls>();
        if(isServiceOrderRecordType){
            //Create a List of Service Status Values
            List<String> isServiceRecordStatuses = new List<String>{'New','Product Ordered','Service Scheduled','Warranty Submitted','Warranty Rejected','Service On Hold','Customer Call Back','Service To Be Scheduled','Service Complete','To Be Ordered','Seasonal Service','Quote'};
            //loop through the service statuses and create the trail of history needed by the tests.
            for(String statusValue : isServiceRecordStatuses){
                BatchJobOrderHistoryTracking.OrderHistoryCls oh = new BatchJobOrderHistoryTracking.OrderHistoryCls(OrderId,'Status',statusValue,null,null,null);
                retData.add(oh);
            }
        }else{
            //Create a List of Non-Service Status Values
            List<String> nonServiceRecordStatuses = new List<String>{'Draft','On Hold','Tech Measure Needed','Tech Measure Scheduled','Ready to Order','Order Released','Install Needed','Install Scheduled','Install Complete','Job in Progress','Job Closed','Pending Cancellation','Cancelled'};
            //loop through the non-service statuses and create the trail of history needed by the tests.
            for(String statusValue : nonServiceRecordStatuses){
                BatchJobOrderHistoryTracking.OrderHistoryCls oh = new BatchJobOrderHistoryTracking.OrderHistoryCls(OrderId,'Status',statusValue,null,null,null);
                retData.add(oh);
            }
        }
        return retData;
    }
}