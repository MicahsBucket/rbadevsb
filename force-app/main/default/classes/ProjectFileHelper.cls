/**
 * @description       : 
 * @author            : bill.hodena@andersencorp.com
 * @group             : 
 * @last modified on  : 03-01-2021
 * @last modified by  : mark.rothermal@andersencorp.com
 * @description  unit test coverage provided by ContentDocumentLinkTriggerHelperTest
 * Modifications Log 
 * Ver   Date         Author                            Modification
 * 1.0   03-01-2019   bill.hodena@andersencorp.com   Initial Version
**/

public without sharing class ProjectFileHelper  {
    
    public  static List<quotelineitem> quotelineiteminsert;
    public static  List<Quote_Discount__c> quoteDiscInsert;
    public static  Set<Id> masterProductIds = new Set<id>();
    public static final String FULL_FRAME = 'Full Frame';
    public static final String INSERT_FRAME = 'Insert Frame';
    public static final String L_TRIM = 'L Trim';
    public static Map<String, String> hardwareColorMap = getHardwareColorMap();
    public static Map<string,string> quoteRsuiteName = new  Map<string,string>();
    public static Id unMatchedPriceBookEntryId;
    
    public static Map<String, String> getHardwareColorMap(){
        Product_Config_Settings__c pcs = Product_Config_Settings__c.getInstance();
        if(pcs == null || pcs.Hardware_Color_Mappings__c == null){
            return null;
        }
        Map<String, String> hardwareColorMap = new Map<String, String>();
        for(String pair : pcs.Hardware_Color_Mappings__c.split(';')){
            List<String> colorPair = pair.split(',');
            hardwareColorMap.put(colorPair[0], colorPair[1]);
        }
        return hardwareColorMap;
    }
    /**
* @description Saves a rSuite project as the related quote and quote line items
* @param fileContents a String containing the JSON that represents an rSuite project
* @param relatedObjectId the Id of the object related to the file upload
*/ 
    public static void saveProject(String fileContents, Id relatedObjectId){
        System.debug(fileContents);
        try{
            JSONParser parser = JSON.createParser(fileContents);
            saveProject(parser, relatedObjectId);
        }catch(Exception ex){
            system.debug('Error in ProjectFileHelper.saveProject: ' + ex.getMessage());
            system.debug('Error in ProjectFileHelper.saveProject - stace trace: ' + ex.getStackTraceString());
            ContentDocumentLinkTriggerHelper.errorMessages=ContentDocumentLinkTriggerHelper.errorMessages+ex.getMessage()+'\r\n';
        }
    }
    
    
    /**
* @description Saves a rSuite project as the related quote and quote line items 
* @param parser a JSONParser containing the JSON formatted project file
*/ 
    public static void saveProject(JSONParser parser, Id relatedObjectId){
        try{
            ProjectFile currentProject = new ProjectFile(parser);
            saveProject(currentProject, relatedObjectId);
        }catch(Exception ex){
            system.debug('Error in ProjectFileHelper.saveProject: ' + ex.getMessage());
            system.debug('Error in ProjectFileHelper.saveProject - stace trace: ' + ex.getStackTraceString());
            ContentDocumentLinkTriggerHelper.errorMessages=ContentDocumentLinkTriggerHelper.errorMessages+ex.getMessage()+'\r\n';
        }
    }
    
    
    /**
* @description Saves a rSuite project as the related quote and quote line items 
* @param projectFile a ProjectFile object instance that has been populated with the parsed JSON 
*/ 
    public static void saveProject(ProjectFile projectFile, Id relatedObjectId){
        try{
            
            list<opportunity> relatedopp =[select accountid ,Pricebook2Id,id from opportunity where id =:relatedObjectId];
            List<projectfile.QuoteContainerList> quotelines=   projectfile.opportunityContainer.quoteContainerList;
            List<quote> quotelistinsert = new List<quote>();
            quotelineiteminsert = new List<quotelineitem>();
            quoteDiscInsert = new List<Quote_Discount__c>();
            map<string ,quotelineitem> rsuiteidtolineitem = new map<string ,quotelineitem>();
            map<quote ,List<quotelineitem>> quotetolineitemmap = new map<quote ,List<quotelineitem>>();
            List<quotelineitem> updateQuotelineitemswithparent  =  new List<quotelineitem>();
            Set<String> hardwareColorSet = hardwareColorMap != null ? 
                new Set<String>(hardwareColorMap.values()) : new Set<String>();
            
            //  updating quotes so only one quote is sold 
            
            List<Quote> quoteToNotSold =[select id , isSold__c,Opportunity.id from quote where     Opportunity.id =:relatedObjectId];
            
            for(Quote Qcsold: quoteToNotSold){
                if(Qcsold.issold__c){
                    Qcsold.issold__c=false;
                }
            }
            if( quoteToNotSold.size()>0){
                update  quoteToNotSold;
            }  
            // Getting Quotes from project file and associating with pricebook and opportunity 
            for(projectfile.QuoteContainerList quotelist : quotelines ){
                projectfile.AQuote quote = quotelist.aQuote;
                List<projectfile.QuoteLineItems> quotelinelist = quotelist.quoteLineItems; 
                quote newquote = quote.quote;
                // newquote.=relatedopp[0].accountid;
                system.debug('pb ID in quote creator' + relatedopp[0].Pricebook2Id);
                newquote.Pricebook2Id = relatedopp[0].Pricebook2Id;
                newquote.OpportunityId = relatedopp[0].id;
                //newquote.AccountId=relatedopp[0].accountid;
                if(!newquote.IsDeleted){
                    quotelistinsert.add(newquote);
                }
            }
            Schema.SObjectField fieldOnQuote = quote.rSuite_Id__c;
            System.debug('the quotes are'+quotelistinsert);
            try{
              // upsert quotelistinsert;
                Database.UpsertResult[] quoteUpsertResult = database.upsert(quotelistinsert,fieldOnQuote,false);                
                errorHandlingMethod(quoteUpsertResult);
            }catch(Exception ex){
                system.debug('error '+ex.getMessage());
                system.debug('error '+ex.getStackTraceString());
                system.debug('error '+ex.getLineNumber());
            }
            //upsert quotelistinsert rSuite_Id__c;
            // Getting quotelines and updating with quoteid 
            for(projectfile.QuoteContainerList quotelist : quotelines ){
                projectfile.AQuote quote= quotelist.aQuote;
                List<projectfile.QuoteLineItems> quotelinelist = quotelist.quoteLineItems;
                List<projectfile.QuoteDiscountList> quoteDiscList = quotelist.quoteDiscountList;
                
                quote newquote = quote.quote;
                for(projectfile.QuoteLineItems quoteitem :quotelinelist ){
                    quotelineitem newquoteitem = quoteitem.quotelineitem;
                    newquoteitem.QuoteId = newquote.id;
                    system.debug('*************child pbe newquote item -- ' + newquoteitem.Child_Product_Pricebook_Entry_Id__c  );
                    system.debug('*************child pbe -- quoteitem.quotelineitem ' + quoteitem.quotelineitem.Child_Product_Pricebook_Entry_Id__c  );
                    if(newquoteitem.MasterProductSFID__c != null){
                        masterProductIds.add(newquoteitem.MasterProductSFID__c); 
                    }
                    if(newquoteitem.Quantity == null){
                        newquoteitem.Quantity = 1;  
                    }
                    if(newquoteitem.UnitPrice == null){
                        newquoteitem.unitprice = 0;  
                    }
                    if ((newquoteitem.Cross_Reference__c == null || newquoteitem.Cross_Reference__c.trim().equals('')) && 
                        (newquoteitem.Child_Product_Pricebook_Entry_Id__c == null || newquoteitem.Child_Product_Pricebook_Entry_Id__c.trim().equals('')) &&
                        newquoteitem.Product_Sub_Category__c != null) {
                            // set the Child_Product_Pricebook_Entry_Id__c field (temporarily)
                            system.debug('*************cross ref before set child to sub cat -- ' + newquoteitem.Cross_Reference__c  );
                            system.debug('*************child pbe before set to product sub cat -- ' + newquoteitem.Child_Product_Pricebook_Entry_Id__c  );
                            newquoteitem.Child_Product_Pricebook_Entry_Id__c = newquoteitem.Product_Sub_Category__c;
                            system.debug('*************Set child pbe to product sub cat -- ' + newquoteitem.Child_Product_Pricebook_Entry_Id__c  );
                        }
                    newquoteitem.Hardware_Finish__c = newquoteitem.Hardware_Style__c;
                    String hardwareColor = newquoteitem.Hardware_Style__c;
                    String interiorColor = newquoteitem.Interior_Color__c;
                    
                    // Check hardware color mappings
                    // Only make a change if hardware color is non-estate (exists in custom setting)
                    Boolean hardwareColorInMap = hardwareColorSet.contains(hardwareColor);
                    if(hardwareColorMap != null && interiorColor != null && hardwareColor != null
                       && hardwareColorInMap){
                           String standardHardwareColor = hardwareColorMap.get(interiorColor);
                           if(hardwareColor != standardHardwareColor){
                               // The colors are not a standard pair
                               newquoteitem.Hardware_Color__c =hardwareColor;
                               newquoteitem.Special_Options__c= true;
                           }
                           newquoteitem.Hardware_Finish__c='Standard';
                       }
                    newquoteitem.Hardware_Style__c='Standard';
                    rsuiteidtolineitem.put(newquoteitem.rSuite_Id__c,newquoteitem);
                    if(!quoteitem.IsDeleted){
                        quotelineiteminsert.add(newquoteitem);
                    }
                }
                
                for(projectfile.QuoteDiscountList quoteDisc :quoteDiscList){
                    if(quoteDisc.Discount_Amount != 0 && !quoteDisc.isDeleted){     
                        quoteDisc.quoteDiscount.Quote__c = newquote.id;
                        quoteDiscInsert.add(quoteDisc.quoteDiscount);  
                    }
                }
                
            }
            System.debug('*****right before method for QLI linking ************');
            //method to link quotelines to pricebookentry
            linkToSaleforceRecords(relatedObjectId);
            system.debug('*****quote line item insert list '+ quotelineiteminsert);
            system.debug('****unmatched pricebook entry id '+ unMatchedPriceBookEntryId);
            
            // Matching Unmatched Misc item to Default Product
            for(quotelineitem quotelineMisc :quotelineiteminsert){
                system.debug('****quote line item PBEID right before setting to unmatched '+ quotelineMisc.PricebookEntryId);
                if(quotelineMisc.PricebookEntryId == null ){
                    quotelineMisc.PricebookEntryId = unMatchedPriceBookEntryId;
                }
            }
            system.debug('The **** quote line item list to insert'+ quotelineiteminsert);
            Schema.SObjectField f = quotelineitem.rSuite_Id__c;
            //Performing database upsert so if any product is not found the reamining items should upsert .
            Database.UpsertResult[] quoteLineUpsertResult = database.upsert(quotelineiteminsert,f,false);
            errorHandlingMethod(quoteLineUpsertResult);
            Schema.SObjectField filedOnQuoteDiscount = Quote_Discount__c.Store_Discount_rSuite_Id__c;
            
            Database.SaveResult[] quoteDiscountUpsertResult = database.insert(quoteDiscInsert,false);

            for(quotelineitem quotelineitemlookup :quotelineiteminsert){
                if(quotelineitemlookup.PricebookEntryId == null){    
                    ContentDocumentLinkTriggerHelper.errorMessages = ContentDocumentLinkTriggerHelper.errorMessages+'Product with Id: '+quotelineitemlookup.Cross_Reference__c  +' and Description: ' + quotelineitemlookup.Description+
                    +' and sub category '+quotelineitemlookup.Product_Sub_Category__c +' is present within in the .JSON file but no such product exists in rForce.'+'\r\n'; 
                }
                if(quotelineitemlookup.Parent_Unit_rSuite_Id__c != null && quotelineitemlookup.id != null && rsuiteidtolineitem.get(quotelineitemlookup.Parent_Unit_rSuite_Id__c) != null){
                    quotelineitemlookup.Parent_Quote_Line_Item__c = rsuiteidtolineitem.get(quotelineitemlookup.Parent_Unit_rSuite_Id__c).id;
                    updateQuotelineitemswithparent.add(quotelineitemlookup);
                }
            }
            Database.UpsertResult[] quoteLineUpsertResultParent = database.upsert(updateQuotelineitemswithparent,f,false);
            errorHandlingMethod(quoteLineUpsertResultParent);
            //upsert quotelineiteminsert rSuite_Id__c;
            // TODO: Perform needed upserts, status changes, etc to save the quote and quote items to the opportunity
            
        }catch(Exception ex){
            system.debug('Error in ProjectFileHelper.saveProject: ' + ex.getMessage());
            system.debug('Error in ProjectFileHelper.saveProject - stace trace: ' + ex.getStackTraceString());
            ContentDocumentLinkTriggerHelper.errorMessages=ContentDocumentLinkTriggerHelper.errorMessages+ex.getMessage()+'\r\n';
        }
    }
    /**
* @description To link the quotelineitems to pricebookentryid based on product crossreference and description
* @param opptyid to fetch the pricebook Entries and child nad master pricebook entries 
*/ 
    public static void linkToSaleforceRecords(Id opptyId) {
        // go get salesforce information for the oppotunity
        system.debug('MTR **** Made it to PBE method *****************');
        Opportunity opp = [select id, Pricebook2Id, Store_Location__c, Store_Location__r.Active_Store_Configuration__c from Opportunity where Id = :opptyId limit 1];
        if (opp != null) {
            // get Pricebook Entries
            List<PricebookEntry> childPBEList = [select Id, Product2Id, Pricebook2Id, Cross_Reference__c,
                                                 Product2.Name, Product2.Master_Product__c, Product2.Master_Product__r.Master_Product__c, 
                                                 Product2.rSuite_Product_Type__c,
                                                 Product2.Master_Product__r.rSuite_Product_Type__c,
                                                 Product2.Master_Product__r.Master_Product__r.rSuite_Product_Type__c
                                                 from PricebookEntry
                                                 where (Product2.RecordType.DeveloperName = :Constants.CHILD_PRODUCT_RECORD_TYPE_NAME or Product2.RecordType.DeveloperName = :Constants.MULLION_PRODUCT_RECORD_TYPE_NAME) 
                                                 and Pricebook2Id = :opp.Pricebook2Id
                                                 and IsActive = true];
            
            List<PricebookEntry> miscChargePBEList = [select Id, Product2Id, Pricebook2Id, Product2.Name, Product2.rSuite_Product_Type__c, Product2.Category__c, 
                                                      Product2.Sub_Category__c, Cross_Reference__c, Product2.Master_Product__c
                                                      from PricebookEntry
                                                      where (Product2.RecordType.DeveloperName = :Constants.MISC_JOB_AND_UNIT_CHARGES_PRODUCT_RECORD_TYPE_NAME or 
                                                             Product2.RecordType.DeveloperName = :Constants.CONSTRUCTION_MATERIALS_PRODUCT_RECORD_TYPE_NAME)
                                                      and Pricebook2Id = :opp.Pricebook2Id
                                                      and IsActive = true];
            
            // Get master products assoicated with misc products
            List<Product2> miscProductsWithMaster = [select Id, Name, Master_Product__c from Product2
                                                     where (RecordType.DeveloperName = :Constants.MISC_JOB_AND_UNIT_CHARGES_PRODUCT_RECORD_TYPE_NAME or 
                                                            RecordType.DeveloperName = :Constants.CONSTRUCTION_MATERIALS_PRODUCT_RECORD_TYPE_NAME) 
                                                     and Master_Product__c != null];
            
            Set<Id> miscMasterProdList = new Set<Id>();
            for(Product2 prod : miscProductsWithMaster){
                miscMasterProdList.add(prod.Master_Product__c);
            }
            //building misc charge map.
            Map<Id,PricebookEntry> miscChargeProduct2IdToPBEmap = new Map<Id,PricebookEntry>();
            For(PricebookEntry pbe : miscChargePBEList){
                miscChargeProduct2IdToPBEmap.put(pbe.Product2Id,pbe);
            }
            
            //Querying the Unmatched pricebookEntry Misc
            List<PricebookEntry> unmatchedPriceBookMisc =[select id,Product2.Name from  PricebookEntry where  Pricebook2Id = :opp.Pricebook2Id
                                                          and IsActive = true and Product2.RecordType.DeveloperName = :Constants.MISC_JOB_AND_UNIT_CHARGES_PRODUCT_RECORD_TYPE_NAME and Product2.Name='Unmatched rSuite Charge'];
            if(unmatchedPriceBookMisc.size()>0){
                unMatchedPriceBookEntryId = unmatchedPriceBookMisc[0].id;
            }
            Map<Id, PricebookEntry> masterProdIdToPBE = new Map<Id, PricebookEntry>();
            Map<Id, PricebookEntry> masterProdIdToPBEMain = new Map<Id, PricebookEntry>();
            
            // Get PBEs for Master products associated with misc
            for( PricebookEntry pbe : [select Id, Product2Id, Pricebook2Id, Product2.Name, Product2.rSuite_Product_Type__c, Product2.Category__c, 
                                       Product2.Sub_Category__c, Cross_Reference__c
                                       from PricebookEntry
                                       where Product2.RecordType.DeveloperName = :Constants.MASTER_PRODUCT_RECORD_TYPE_NAME 
                                       and Pricebook2Id = :opp.Pricebook2Id
                                       and IsActive = true
                                       and Product2Id in :miscMasterProdList]){                
                masterProdIdToPBE.put(pbe.Product2Id, pbe);                               
            }
            
            Set<Id> productHasHardwareStyle = new Set<Id>();
            for(Product_Configuration__c pc : [select id, Product__c from Product_Configuration__c 
                                               where Hardware_Style_Control__c != 'Disabled']){
                productHasHardwareStyle.add(pc.Product__c);
            }
            
            
            
            // get list of discounts for this store
            List<Store_Discount__c> sdList = [select Id, Name,rSuite_Discount_Name__c from Store_Discount__c where Active__c = true 
                                              and Store_Configuration__c = :opp.Store_Location__r.Active_Store_Configuration__c];
            
            Store_Discount__c genericDiscount = null;
            
            
            // update the quotes
            
            for( PricebookEntry pbe : [select Id, Product2Id, Pricebook2Id, Product2.Name, Product2.rSuite_Product_Type__c, Product2.Category__c, 
                                       Product2.Sub_Category__c, Cross_Reference__c
                                       from PricebookEntry
                                       where Product2.RecordType.DeveloperName = :Constants.MASTER_PRODUCT_RECORD_TYPE_NAME 
                                       and Pricebook2Id = :opp.Pricebook2Id
                                       and IsActive = true 
                                      ]){
                masterProdIdToPBEMain.put(pbe.Product2Id, pbe);                              
            }
            
            
            for (QuoteLineItem qli :quotelineiteminsert) {
                Set<Id> productIdSet = new Set<Id>();
                String productType = '';
                Boolean matchFound = false;
                // need to set the pricebook entry id to null it should not be set at this point
                // the xml has it incorrectly in the data
                system.debug('****in pfhelper - qli.PricebookEntryId before setting to null ' + qli.PricebookEntryId);
                qli.PricebookEntryId = null;
                // Matching only on master prduct id when cross reference is null
                system.debug('****in pfhelper - qli.MasterProductSFID__c ' + qli.MasterProductSFID__c);
                 if(qli.MasterProductSFID__c != null) { 
                    if(!test.isRunningTest()){
                        System.debug('***right before masterPBEMainEntry ');
                        If(masterProdIdToPBEMain.get(qli.MasterProductSFID__c) != null){
                        PricebookEntry masterPBEMainEntry = masterProdIdToPBEMain.get(qli.MasterProductSFID__c);
                        //qli.Child_Product_Pricebook_Entry_Id__c = null;
                        qli.PricebookEntryId = masterPBEMainEntry.Id;
                        } else if (miscChargeProduct2IdToPBEmap.get(qli.MasterProductSFID__c) != null){
                            PricebookEntry masterPBEMainEntry =  miscChargeProduct2IdToPBEmap.get(qli.MasterProductSFID__c);
                            qli.PricebookEntryId = masterPBEMainEntry.Id;
                    }
                        System.debug('***right after qli.PricebookEntryId = masterPBEMainEntry.Id ' + qli.PricebookEntryId);
                    }
                    if (qli.Estate_Finish_Hand_Lift__c || qli.Standard_Color_Hand_Lift__c){
                        qli.Lifts_Pulls__c = '1';
                    } 
                    if (qli.Estate_Finish_Extra_Hand_Lift__c || qli.Standard_Color_Extra_Hand_Lift__c){
                        qli.Lifts_Pulls__c = '2';
                    } 
                    if (qli.Estate_Finish_Recessed_Hand_Lift__c || qli.Standard_Color_Recessed_Hand_Lift__c){
                        qli.Lifts_Pulls__c = '1R';
                    }
                    if (qli.Estate_Finish_Extra_Recessed_Hand_Lift__c || qli.Standard_Color_Extra_Recessed_Hand_Lift__c){
                        qli.Lifts_Pulls__c = '2R';
                    }
                    if(qli.Lifts_Pulls__c == null){
                        qli.Lifts_Pulls__c = '0';
                    }
                    if(!test.isRunningTest()){
                        continue;
                    }        
                }
            
                // match the quote line item to the pricebook entry
                system.debug('****in pfhelper - qli.Cross_Reference__c - DID not match a master id???' + qli.Cross_Reference__c);
                system.debug('****in pfhelper - qli.Child_Product_Pricebook_Entry_Id__c - DID not match a master id???' + qli.Child_Product_Pricebook_Entry_Id__c);
                if (qli.Cross_Reference__c != null || qli.Child_Product_Pricebook_Entry_Id__c != null  ) {
                    // process the master/child product (windows/doors/mullions)
                    // Logic to match on product id sent from json 
                    if(qli.MasterProductSFID__c != null) { 
                        if(!test.isRunningTest()){
                            PricebookEntry masterPBEMainEntry = masterProdIdToPBEMain.get(qli.MasterProductSFID__c);
                            //qli.Child_Product_Pricebook_Entry_Id__c = null;
                            qli.PricebookEntryId = masterPBEMainEntry.Id;
                        }
                        if (qli.Estate_Finish_Hand_Lift__c || qli.Standard_Color_Hand_Lift__c){
                            qli.Lifts_Pulls__c = '1';
                        } if (qli.Estate_Finish_Extra_Hand_Lift__c || qli.Standard_Color_Extra_Hand_Lift__c){
                            qli.Lifts_Pulls__c = '2';
                        } if (qli.Estate_Finish_Recessed_Hand_Lift__c || qli.Standard_Color_Recessed_Hand_Lift__c){
                            qli.Lifts_Pulls__c = '1R';
                        } if (qli.Estate_Finish_Extra_Recessed_Hand_Lift__c || qli.Standard_Color_Extra_Recessed_Hand_Lift__c){
                            qli.Lifts_Pulls__c = '2R';
                        } if(qli.Lifts_Pulls__c == null){
                            qli.Lifts_Pulls__c = '0';
                        }
                        continue;
                        
                    }
                    System.debug('*****inside master pbe loop..Hit pbe child list loop- QLI cross reference id...' + qli.Cross_Reference__c );
                    for(PricebookEntry pbe : childPBEList) {
                        if (qli.Cross_Reference__c == pbe.Cross_Reference__c || qli.Child_Product_Pricebook_Entry_Id__c == pbe.Cross_Reference__c ) {
                            System.debug('*****Hit pbe child list match on - PBE cross reference id...' + pbe.Cross_Reference__c  );
                            qli.Child_Product_Pricebook_Entry_Id__c = pbe.Id;

                            if(masterProdIdToPBEMain.containsKey(pbe.Product2.Master_Product__c)){
                                PricebookEntry masterPBEMain = masterProdIdToPBEMain.get(pbe.Product2.Master_Product__c);
                                qli.Child_Product_Pricebook_Entry_Id__c = null;
                                qli.PricebookEntryId = masterPBEMain.Id;   
                            }
                            //qli.PricebookEntryId=pbe.Product2.Master_Product__c;
                            if(pbe.Product2Id != null){
                                productIdSet.add(pbe.Product2Id);
                                productType = pbe.Product2.rSuite_Product_Type__c;
                            }
                            else if(pbe.Product2.Master_Product__c != null){
                                productIdSet.add(pbe.Product2.Master_Product__c);
                                productType = pbe.Product2.Master_Product__r.rSuite_Product_Type__c;
                            }
                            else if(pbe.Product2.Master_Product__r.Master_Product__c != null){
                                productIdSet.add(pbe.Product2.Master_Product__r.Master_Product__c);
                                productType = pbe.Product2.Master_Product__r.Master_Product__r.rSuite_Product_Type__c;
                            }
                            matchFound = true;
                            break;
                        }
                    }
                    
                } else if (qli.Description != null) {
                    system.debug('***hit qli description not null***');
                    for(Product2 prod : miscProductsWithMaster){
                        if(prod.Name == qli.Description && masterProdIdToPBE.containsKey(prod.Master_Product__c)){
                            PricebookEntry masterPBE = masterProdIdToPBE.get(prod.Master_Product__c);
                            qli.Child_Product_Pricebook_Entry_Id__c = null;
                            qli.PricebookEntryId = masterPBE.Id;
                            productIdSet.add(masterPBE.Product2Id);
                            productType = masterPBE.Product2.rSuite_Product_Type__c;
                            matchFound = true;
                            break;
                        }
                    }
                    
                    if(!matchFound){
                        // process other types of records by using the description field
                        for (PricebookEntry pbe: miscChargePBEList) {
                            
                            if (pbe.Product2.Name == qli.Description) {
                                system.debug('****the product ha ha '+pbe.id);
                                qli.Child_Product_Pricebook_Entry_Id__c = null;
                                qli.PricebookEntryId = pbe.Id;
                                productIdSet.add(pbe.Product2Id);
                                productType = pbe.Product2.rSuite_Product_Type__c;
                                matchFound = true;
                                break;
                            }
                        }
                    }
                    
                }
                
                if (!matchFound) {
                    
                    for(Product2 prod : miscProductsWithMaster){
                        if(prod.Name == qli.Child_Product_Pricebook_Entry_Id__c 
                           && masterProdIdToPBE.containsKey(prod.Master_Product__c)){
                               PricebookEntry masterPBE = masterProdIdToPBE.get(prod.Master_Product__c);
                               qli.Child_Product_Pricebook_Entry_Id__c = null;
                               qli.PricebookEntryId = masterPBE.Id;
                               productIdSet.add(masterPBE.Product2Id);
                               productType = masterPBE.Product2.rSuite_Product_Type__c;
                               matchFound = true;
                               break;
                           }
                    }
                    
                    if(!matchFound){
                        // try some other methods - only works for misc items
                        for (PricebookEntry pbe: miscChargePBEList) {
                            if (pbe.Product2.Name == qli.Child_Product_Pricebook_Entry_Id__c) {
                                qli.Child_Product_Pricebook_Entry_Id__c = null;
                                qli.PricebookEntryId = pbe.Id;
                                productIdSet.add(pbe.Product2Id);
                                productType = pbe.Product2.rSuite_Product_Type__c;
                                matchFound = true;
                                break;
                            }
                        }
                    }
                    
                }
                if (!matchFound) {
                    // no luck - no matching product found
                    // add the qli as a message and remove it from the list to insert
                    system.debug('Mtr ***NO match found*** how did we get here... should we set to unmatched here???');
                    qli.PricebookEntryId = unMatchedPriceBookEntryId;
                    
                }
                
                if(!productIdSet.isEmpty()){
                    for(Id productId : productIdSet){
                        if(productHasHardwareStyle.contains(productId)){
                            qli.Hardware_Style__c = 'Standard';
                            break;
                        }
                    }
                }
                
                // Check for blank fields on Window products, update as needed
                if(productType != null && productType == 'Window'){
                    // Check Frame Type, if blank, use EJ Frame value to determine
                    if(qli.Frame_Type__c == null){
                        if(qli.EJ_Frame__c == true){
                            qli.Frame_Type__c = FULL_FRAME;
                        }else{
                            qli.Frame_Type__c = INSERT_FRAME;
                        }
                    }
                     /*
                    Populate values for Lift Pulls
                    1R -> Recessed hand lift
                    2R -> Extra recessed hand lift
                    1 -> Estate finish hand Lift
                    2 ->Extra estate finish hand lift
                    0 -> no selection
                    */
                    if (qli.Estate_Finish_Hand_Lift__c || qli.Standard_Color_Hand_Lift__c){
                        qli.Lifts_Pulls__c = '1';
                    } if (qli.Estate_Finish_Extra_Hand_Lift__c || qli.Standard_Color_Extra_Hand_Lift__c){
                        qli.Lifts_Pulls__c = '2';
                    } if (qli.Estate_Finish_Recessed_Hand_Lift__c || qli.Standard_Color_Recessed_Hand_Lift__c){
                        qli.Lifts_Pulls__c = '1R';
                    } if (qli.Estate_Finish_Extra_Recessed_Hand_Lift__c || qli.Standard_Color_Extra_Recessed_Hand_Lift__c){
                        qli.Lifts_Pulls__c = '2R';
                    } if(qli.Lifts_Pulls__c == null){
                        qli.Lifts_Pulls__c = '0';
                    }
                }
                
                if(productType != null && productType == 'Specialty'){
                    if(qli.Fibrex_L_Trim__c == true){
                        qli.Exterior_Trim__c = L_TRIM;
                    }
                }
                
            }
            // end quoteLineItems
            
            
            //Quote Discount logic and temporarily storing discount name on pivot id
            for ( Quote_Discount__c qd :quoteDiscInsert) {
                
                Boolean matchFound = false;
                
                for(Store_Discount__c sd : sdList){
                    if(qd.Pivotal_Id__c == sd.rSuite_Discount_Name__c){
                        qd.Store_Discount__c = sd.Id;
                        matchFound = true;
                        qd.Pivotal_Id__c=null;
                        break;
                    }
                }
            }
        }
    }
    /**
    * @description Common Error Handling method to capture upsert errors
    * @param upsertResultList The Result of upsert on quote ,quotelines and discounts 
    */ 
    public static void errorHandlingMethod( Database.UpsertResult[] upsertResultList ){
        for (Database.UpsertResult sr : upsertResultList) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred. In database upsert result - project file helper');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    
                    ContentDocumentLinkTriggerHelper.errorMessages=ContentDocumentLinkTriggerHelper.errorMessages+err.getMessage()+'\r\n';
                }
            } else {
                System.debug('No errors found with upsert ' + sr);
            }
        } 
    }
}