/**
 * This class contains unit tests for XMLFactory
 *
 */
@isTest
private class XMLFactoryTest {

  static testMethod void populateSObjectTest() {

    String fieldUpdate = 'Test Update Rock';
		insert new RSuite_Translation__c(
			RSuite_Value__c = 'Red Rock',
			Salesforce_Value__c = fieldUpdate, 
			Field_API_Name__c = 'Exterior_Color__c'
		);
		insert new RSuite_Translation__c(
			RSuite_Value__c = 'No Grilles',
			Field_API_Name__c = 'Grille_Style__c'
		);
		insert new RSuite_Translation__c(
			RSuite_Value__c = 'No Grilles',
			Dependent_Field_API_Name__c = 'Grille_Pattern_S2__c',
			Field_API_Name__c = 'Lites_High_S2__c'
		);
		insert new RSuite_Translation__c(
			RSuite_Value__c = 'No Grilles',
			Dependent_Field_API_Name__c = 'Grille_Pattern_S2__c',
			Field_API_Name__c = 'Lites_Wide_S2__c'
		);

    String xml = '<array><dict><key>Aluminum_Sill_Support__c</key><false/><key>Apply_to_All_Sashes_Grilles__c</key><testunknownfieldtype/>' +
    	'<key>Auxillary_Foot_Lock_Color_Match__c</key><string></string><key>Child_Product_Pricebook_Entry_Id__c</key><string>CD059.99</string>' +
    	'<key>Custom_Grille_Price_S1__c</key><real>0.0</real><key>Grille_Style__c</key><string>No Grilles</string><key>Door_Sill_Color__c</key><string></string><key>Exterior_Color__c</key>' +
    	'<string>Red Rock</string><key>Finger_Lifts__c</key><integer>0</integer><key>Interior_Extension_Jamb_Kit__c</key><true/>' +
    	'<key>BadfieldName__c</key><false/><key>Product_Category__c</key><string>Casement</string><key>Product_Class__c</key>' +
    	'<string>Master Product</string><key>Product_Sub_Category__c</key><string>Casement - Double</string><key>Quantity</key>' +
    	'<string>1</string><key>Sales_Height_Inches__c</key><integer>23</integer><key>UnitPrice</key><real>3095</real><key>crossReference</key><string>CD059.99</string>' +
    	'<key>isDeleted</key><false/><key>rSuite_Id__c</key><string>F1EDD03E-6D15-4FBA-89D5-F7F12E1A6BE4</string><key>rSuite_Product_Type__c</key>' +
    	'<string>Window</string></dict><dict><key>isDeleted</key><true/><key>rSuite_Id__c</key><string>AAAAAAAA-6D15-4FBA-89D5-F7F12E1A6BE4</string>' +
    	'</dict></array>';

		Dom.Document doc = new Dom.Document();
		doc.load(xml);
		Dom.XMLNode xn = doc.getRootElement();
		Dom.XMLNode dictNode = xn.getChildElement('dict', null);
		Dom.XMLNode[] cNodes = dictNode.getChildren();
		if (xn.getName() == Constants.NODE_NAME_ARRAY) {
			List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>();
			Dom.XmlNode[] qliNodes = xn.getChildren();
			XMLFactory.DTO qliDTO = XMLFactory.populateSObject(qliNodes[0], 'QuoteLineItem');
			//System.debug('#####qli-ro: ' + qliDTO.readOnlyFieldMap);
			//System.debug('#####qil-err: ' + qliDTO.errors);
			System.debug('#####:' + qliDTO.sObj);
			//System.assert(qliDTO != null);
			//System.assert(qliDTO.sObj != null);
			//System.assert(qliDTO.readOnlyFieldMap.size() == 3);
			//System.assertEquals(3, qliDTO.errors.size());
			// process a deleted node
			qliDTO = XMLFactory.populateSObject(qliNodes[1], 'QuoteLineItem');

			//System.assertNotEquals(null, qliDTO);
			//System.assertNotEquals(null, qliDTO.sObj);
			//System.assertEquals(fieldUpdate, qliDTO.sObj.get('Exterior_Color__c') );
			//System.assert(qliDTO == null);
		} else {
			//System.assert(false);
		}
        
  }
}