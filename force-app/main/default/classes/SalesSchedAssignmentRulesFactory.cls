/**
 * @File Name          : SalesSchedAssignmentRulesFactory.cls
 * @Description        :
 * @Author             : James Loghry (Demand Chain)
 * @Group              :
 * @Last Modified By   : James Loghry (Demand Chain)
 * @Last Modified On   : 6/11/2019, 10:05:57 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/3/2019, 8:25:08 PM   James Loghry (Demand Chain)     Initial Version
**/
public with sharing class SalesSchedAssignmentRulesFactory  {
	/** @description Defines and executes the assignment rules for a StoreId (SS19-21)
	  * @author Demand Chain - Brian Tremblay */

	// Assigment Rule Class Factory interface template
    public interface IARClassFactory{
        SalesSchedProcessDataWrapper processRule(SalesSchedProcessDataWrapper pdw);
    }

    // Assigment Rule Class Factory base class
    public virtual class ARClassFactoryBase{
    }

	// Custom Exception Class
	public class ARException extends Exception {}

	//==================RUN RULES ENTRY POINT====================
    // Process all rules and save - This is the entry point for the execution of the rules.
    public static void processAllWork(ID StoreId, Date ProcessingDate, List<String> rules,string methodName){

        Sales_Schedule_Setting__mdt sss = [Select Current_Sales_Date__c From Sales_Schedule_Setting__mdt Where QualifiedApiName = 'Default'];
        //if(processingDate < sss.Current_Sales_Date__c){
        //    throw new AuraHandledException('You cannot assign appointments in the past.');
        //}

		SalesSchedProcessDataWrapper pdw = new SalesSchedProcessDataWrapper(StoreId,ProcessingDate);
        pdw.process();

		ClassFactoryManager cfm = new ClassFactoryManager();
		for(String sRuleName : rules){
			String sClassName = 'SalesSchedAssignmentRulesFactory.' + sRuleName;
            System.debug('JWL: going to call process rule');
            pdw = cfm.newClassInstance(sClassName).processRule(pdw);
		}

        //Determine if the current running user is a manager.
        List<User> managers = [Select Id From User Where Id = :UserInfo.getUserId() And UserRole.Name Like '%Manager'];
        if(managers.isEmpty()){
            insert pdw.assignments;
            update pdw.appointmentsToUpdate.values();
        }else{
            SalesSchedUtilsWithoutSharing.insertResources(pdw.assignments);
            SalesSchedUtilsWithoutSharing.updateAppointments(pdw.appointmentsToUpdate.values());
        }

        //System.debug('JWL: updating appointments to update: ' + pdw.appointmentsToUpdate.values());
        //update pdw.appointmentsToUpdate.values();
        if(methodName =='batch'){
        sendAutoAssignmentCompleteEmail(pdw);
        }
    }

	//==================Class Factory Manager====================
	public with sharing class ClassFactoryManager {
	    public ClassFactoryManager(){}

		// Return the appropriate class instance based on className
		public SalesSchedAssignmentRulesFactory.IARClassFactory newClassInstance(String className){
		    Type t = Type.forName(className);
		    return (SalesSchedAssignmentRulesFactory.IARClassFactory) t.newInstance();
		}
	}

	public class RankRepsByCloseRate extends ARClassFactoryBase implements IARClassFactory{
		//Rule Intent: With remaining RepUsers, Highest ranked rep, for each slot, gets appointment with highest total window + door count.
        public SalesSchedProcessDataWrapper processRule(SalesSchedProcessDataWrapper pdw){
            Sales_Schedule_Setting__mdt sss = [Select Distance_Warning__c From Sales_Schedule_Setting__mdt Where QualifiedApiName = 'Default'];
            Decimal distanceWarning = sss.Distance_Warning__c;
            if(pdw.store.Active_Store_Configuration__r.Distance_Warning__c != null){
                distanceWarning = pdw.store.Active_Store_Configuration__r.Distance_Warning__c;
            }

            pdw.appointmentsToUpdate = new Map<Id,Sales_Appointment__c>();

            Set<Id> assignedCapacityIds = new Set<Id>();
            for(Slot__c slot : pdw.slots){
                List<Sales_Appointment__c> appointments = pdw.slotToAppointmentsMap.get(slot.Id);
                List<SalesSchedProcessDataWrapper.UserRank> repRanks = pdw.slotToUserRankMap.get(slot.Id);

                System.debug('JWL: slot: ' + slot.Name);
                System.debug('JWL: repRanks: ' + repRanks);
                System.debug('JWL: appointments: ' + appointments);

                boolean assignAppointments = appointments != null && !appointments.isEmpty() && repRanks != null && !repRanks.isEmpty();
                System.debug('JWL: assignAppointments: ' + appointments);

                while(assignAppointments){
                    Sales_Appointment__c sa = appointments.get(0);
                    Location appointmentLocation = sa.Sales_Order__r.Opportunity__r.Account.ShippingAddress;

                    //Tie break for proximity.
                    Location previousLocation = pdw.repToLastLocationMap.get(repRanks.get(0).user.Id);
                    Integer repRankIndex = null;
                    for(Integer i=0; repRankIndex == null && i < repRanks.size(); i++){
                        if(!assignedCapacityIds.contains(repRanks.get(i).capacityId)
                           	&& (
                                previousLocation == null ||
                                previousLocation.longitude == null ||
                                appointmentLocation == null ||
                                appointmentLocation.longitude == null ||
                                Location.getDistance(previousLocation, appointmentLocation, 'mi') <= distanceWarning
                            )
                          ){
                            repRankIndex = i;
                        }
                    }

                    if(repRankIndex != null){
                        System.debug('JWL: assigning appointment ' + sa.Id + ' to user ' + repRanks.get(0).user.Name + ' with capacity ' + repRanks.get(repRankIndex).capacityId);
                        Id capacityId = repRanks.get(repRankIndex).capacityId;
                        pdw.assignments.add(
                            new Sales_Appointment_Resource__c(
                                Assignment_Reason__c = 'Auto Assignment'
                                ,Status__c = 'Assigned'
                                ,Primary__c = true
                                ,Sales_Capacity__c = capacityId
                                ,Sales_Appointment__c = sa.Id
                            )
                        );
                        assignedCapacityIds.add(capacityId);

                        pdw.appointmentsToUpdate.put(sa.Id,new Sales_Appointment__c(Id=sa.Id,Suggested_Capacity__c = capacityId));

                        pdw.repToLastLocationMap.put(repRanks.get(repRankIndex).user.Id, appointmentLocation);
                        repRanks.remove(repRankIndex);
                    }
                    appointments.remove(0);
                    assignAppointments = !appointments.isEmpty() && !repRanks.isEmpty();
                }
            }
            System.debug('JWL: pdw: ' + pdw);

            return pdw;
        }
    }

    private static void sendAutoAssignmentCompleteEmail(SalesSchedProcessDataWrapper pdw){
        OrgWideEmailAddress owa = [Select Id, Address, DisplayName from OrgWideEmailAddress Where DisplayName = 'Renewal Retail Support' LIMIT 1];
        list<string> emailList = new list<string>();
       if(pdw.store.Id != null){
            //Store_Configuration__c st = [Select Id, Store__c, Email_For_SalesSched__c from Store_Configuration__c where Id =:pdw.store.Id limit 1];
           Account Store =[ Select Name,Email_For_SalesSched__c,Active_Store_Configuration__c From Account Where Id = :pdw.store.Id];
            system.debug('______________Account store-->'+store);
           if(string.isNotBlank(store.Email_For_SalesSched__c)){
                for(string stEmail :store.Email_For_SalesSched__c.split(',')){
                    if(string.isNotBlank(stEmail)){
                        emailList.add(stEmail);
                    } 
                }
            }
        }
        List<Messaging.SingleEmailMessage> mail = new List<Messaging.SingleEmailMessage>();
       /* for(User u : [Select ContactId,Contact.Email,Contact.Secondary_Email__c From User Where UserRole.Name Like '%Manager' And Store_Locations__c includes (:pdw.store.Name) And Profile.Name = 'Partner RMS-Sales']){
            Messaging.SingleEmailMessage sem = new Messaging.SingleEmailMessage();
            sem.setTargetObjectId(u.ContactId);
            if(!String.isEmpty(u.Contact.Secondary_Email__c)){
            	sem.setCcAddresses(new List<String>{u.Contact.Secondary_Email__c});
            }
            sem.setSubject('Auto Assignment Complete - Do Not Reply');
            sem.setPlainTextBody('The Auto Assignment job has completed for store ' + pdw.store.Name + ' on ' + ((Datetime)pdw.day).formatGmt('MM-dd-yyyy') + '\n Do not reply to this email - this mailbox is not monitored, contact your manager with any questions.');
			mail.add(sem);
        }*/
        // for(Store_Configuration__c st: [Select Id, Store__c, Email_For_SalesSched__c from Store_Configuration__c]){
				Messaging.SingleEmailMessage sem = new Messaging.SingleEmailMessage();
				//sem.setTargetObjectId(st.Id);
                if(!emailList.isEmpty()){
                //sem.setCcAddresses(emailList);
                sem.setToAddresses(emailList);
                }
				sem.setSubject('Auto Assignment Complete - Do Not Reply');
            	sem.setPlainTextBody('The Auto Assignment job has completed for store ' + pdw.store.Name + ' on ' + ((Datetime)pdw.day).formatGmt('MM-dd-yyyy') + '\n Do not reply to this email - this mailbox is not monitored, contact your manager with any questions.');
				mail.add(sem);
		
        if(!emailList.isEmpty()){ 
            Messaging.sendEmail(mail);
          }
        //Messaging.sendEmail(mail);
    }

	////================USAGE============
	//Id StoreId = '001W000000nnlf2IAA';
	//Date ProcessingDate = Date.valueOf('2019-03-26');
	//List<String> lOrderedExecutionOfRules = new List<String>{'rulePlace1TopRankUserPerSlot','rulePlaceUsersByRankPerSlot'};
	//SalesSchedAssignmentRulesFactory.processAllWork(StoreId, ProcessingDate, lOrderedExecutionOfRules);

	//Testing with: https://renewalbyandersen--devdc.lightning.force.com/lightning/r/Sales_Appointment__c/a6KW00000004pERMAY/view

}