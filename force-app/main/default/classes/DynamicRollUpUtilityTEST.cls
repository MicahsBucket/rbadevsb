/**
 * @author Micah Johnson - Demand Chain 2020
 * @description Test class to test functionality of DynamicRollupQue, DynamicRollupHelper, DynamicRollupUtility
 **/

@isTest
public with sharing class DynamicRollUpUtilityTEST {
    @isTest(seeAllData=true)
    public static void DynamicRollUpUtilityTEST1() {
        List<sObject> ListToPass = new List<sObject>();
        Order o = [SELECT Id, Sold_Order__c FROM Order WHERE Sold_Order__c != null LIMIT 1];
        ListToPass.add(o);
        System.enqueueJob(new DynamicRollUpQue(ListToPass));
    }
}