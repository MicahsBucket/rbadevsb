public without sharing class FSLRollupRolldownCustomLogic {
    
 // RBA - Mark R - commenting out class as part of Demand Chain - Rollup rolldown update 10/2020 - deprecated. do not delete save for reference.
    
    
    /***** Example Template - Iterating over each work order's service appointments.  This basic structure should be used in ALL methods in this class.
        
        // Loop through work orders
        for (FSLRollupRolldownController.workOrderNode woNode : objectNodes.workOrderNodes.values()) {
            WorkOrder wo = woNode.me;
            WorkOrder oldWorkOrder = objectMaps.oldWorkOrders.get(wo.Id); // this will be null if the WO is being inserted
    
            // Loop through this work order's child service appointments
            for (FSLRollupRolldownController.ServiceAppointmentNode saNode : woNode.serviceAppointmentNodes.values()) {
                ServiceAppointment sa = saNode.me;
                ServiceAppointment oldServiceAppointment = objectMaps.oldServiceAppointments.get(sa.Id); // this will be null if the SA is being inserted

                // Perform Logic Here
                
            }

            //Perform Logic Here
        }
    *****/

/*
    public static Id installRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install').getRecordTypeId();
    public static Id serviceRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service').getRecordTypeId();
    public static Id techMeasureRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Tech Measure').getRecordTypeId();
    public static Id jobSiteVisitRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Job Site Visit').getRecordTypeId();
    public static Id orderCoroRTId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Record Type').getRecordTypeId();
    public static Id orderAroRTId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('ARO Record Type').getRecordTypeId();
    public static Id orderServiceRTId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Service').getRecordTypeId();
    public static Id dwellingRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dwelling').getRecordTypeId();

    static {
        // SET SIMPLE ROLLDOWNS HERE
        FSLRollupRolldownController.addRolldown('WorkOrder','ServiceAppointment','Opportunity__c','Opportunity__c');
        FSLRollupRolldownController.addRolldown('WorkOrder','ServiceAppointment','ServiceTerritoryId','ServiceTerritoryId');
    }


    // CUSTOM ROLLDOWN LOGIC FROM ACCOUNT TO ORDER - DO NOT DELETE
    public static void rolldownAccountToOrder(FSLRollupRolldownController.ObjectMapContainer objectMaps, FSLRollupRolldownController.ObjectNodeContainer objectNodes) {
        
        for (FSLRollupRolldownController.AccountNode acctNode : objectNodes.accountNodes.values()) {
            Account a = acctNode.me;

            // If the Account is a Dwelling, roll down the address
            if (a.RecordTypeId == dwellingRecordTypeId) {
                for (FSLRollupRolldownController.OrderNode oNode : acctNode.orderNodes.values()) {
                    Order o = oNode.me;
                    
                    o.ShippingCountry = a.ShippingCountry;
                    o.ShippingStreet = a.ShippingStreet;
                    o.ShippingCity = a.ShippingCity;
                    o.ShippingState = a.ShippingState;
                    o.ShippingPostalCode = a.ShippingPostalCode;
                    o.BillingCountry = a.ShippingCountry;
                    o.BillingStreet = a.ShippingStreet;
                    o.BillingCity = a.ShippingCity;
                    o.BillingState = a.ShippingState;
                    o.BillingPostalCode = a.ShippingPostalCode;
                }
            }
        }
    }


    // CUSTOM ROLLDOWN LOGIC FROM ORDER TO WORK ORDER - DO NOT DELETE
    public static void rolldownOrderToWorkOrder(FSLRollupRolldownController.ObjectMapContainer objectMaps, FSLRollupRolldownController.ObjectNodeContainer objectNodes) {
        
        for (FSLRollupRolldownController.OrderNode oNode : objectNodes.orderNodes.values()) {
            Order o = oNode.me;
            Order oldOrder = objectMaps.oldOrders.get(o.Id);

            for (FSLRollupRolldownController.WorkOrderNode woNode : oNode.workOrderNodes.values()) {
                WorkOrder wo = woNode.me;

                // If the work order is not canceled or completed, roll down the address
                if (wo.Status != 'Canceled' && wo.Status != 'Appt Complete / Closed') {
                    wo.Country = o.ShippingCountry;
                    wo.Street = o.ShippingStreet;
                    wo.City  = o.ShippingCity;
                    wo.State = o.ShippingState;
                    wo.PostalCode = o.ShippingPostalCode;
                }

                // If the Order status changes to 'Closed' or 'Warranty Submitted' (implying it is a Service Request), set the Work Order status to its "final" status
                if (oldOrder != null && o.Status != oldOrder.Status && (o.Status == 'Closed' || o.Status == 'Warranty Submitted')) {
                    if (wo.Status == 'To be Scheduled') {
                        wo.Status = 'Canceled';
                        if (wo.Cancel_Reason__c == null) {
                            wo.Cancel_Reason__c = 'Order Cancelled';
                        }
                    } else if (wo.Status == 'Scheduled & Assigned') {
                        wo.Status = 'Appt Complete / Closed';
                    }
                }

                // If the Order status is 'Cancelled', change the Work Order status to "Canceled"
                if (o.Status == 'Cancelled') {
                    if (wo.Status == 'To be Scheduled' || wo.Status == 'Scheduled & Assigned') {
                        wo.Status = 'Canceled';
                        if (wo.Cancel_Reason__c == null) {
                            wo.Cancel_Reason__c = 'Order Cancelled';
                        }
                    } 
                }
            }
        }
    }


    // CUSTOM ROLLDOWN LOGIC FROM WORK ORDER TO SERVICE APPOINTMENT - DO NOT DELETE
    public static void rolldownWorkOrderToServiceAppointment(FSLRollupRolldownController.ObjectMapContainer objectMaps, FSLRollupRolldownController.ObjectNodeContainer objectNodes) {
        System.debug('+++++Entering rolldownWorkOrderToServiceAppointment');
                
        for (FSLRollupRolldownController.workOrderNode woNode : objectNodes.workOrderNodes.values()) {
            WorkOrder wo = woNode.me;
            WorkOrder oldWo = objectMaps.oldWorkOrders.get(wo.Id);

            for (FSLRollupRolldownController.ServiceAppointmentNode saNode : woNode.serviceAppointmentNodes.values()) {
                ServiceAppointment sa = saNode.me;

                // If the Service Appointment status is 'None' (new) or 'Scheduled', roll down the address
                if (sa.Status == 'None' || sa.Status == 'Scheduled') {
                    sa.Country = wo.Country;
                    sa.Street = wo.Street;
                    sa.City = wo.City;
                    sa.State = wo.State;
                    sa.PostalCode = wo.PostalCode;
                }

                if((wo.Status == 'Canceled' || wo.Status == 'Cancelled') && sa.Status != 'Completed') {
                    // allow the SA to be cancelled from any incomplete status
                    sa.Status = 'Canceled';
                } 

                // Roll down status
                if(oldWo != null && wo.Status != oldWo.Status) {
                    
                    if(wo.Status == 'Suspended / On Hold' && sa.Status != 'None') {
                        sa.Status = 'Cannot Complete';
                    }
                    else if(wo.Status == 'Appt Complete / Closed') { 
                        if(sa.Status != 'User Suspended'){
                        sa.Status = 'Completed';
                        }  else if(sa.Status == 'User Suspended'){
                        sa.Status = 'Permanently Suspended ';
                        }
                    }
                    else if(wo.Status == 'To be Scheduled' && sa.Status != 'None') 
                    {
                        if (sa.Status == 'Scheduled'
                            || sa.Status == 'Dispatched'
                            || sa.Status == 'Scheduled & Assigned'
                            || sa.Status == 'In Progress') 
                        {
                            // if the work order is still 'To be scheduled', do not allow the appointment status to progress to a scheduled status
                            sa.Status = 'None';
                        }
                    }
                }

                // Roll down duration
                if (sa.DurationType == 'Minutes' && wo.DurationType == 'Hours') {
                    // convert from hours to minutes
                    sa.Duration = (wo.Duration * 60);
                } else if (sa.DurationType == 'Hours' && wo.DurationType == 'Minutes') {
                    // convert from minutes to hours
                    sa.Duration = (wo.Duration / 60.0).intValue();
                } else {
                    // assume they have the same DurationType
                    sa.Duration = wo.Duration;
                }
                //For blank orders on SA
                if(sa.Order__c == null && sa.Sold_Order_Form__c != null){
                    sa.Order__c = sa.Sold_Order_Form__c;
                 }
            }
        }
        System.debug('+++++Exiting rolldownWorkOrderToServiceAppointment');
    }

    // CUSTOM ROLLDOWN LOGIC FROM SERVICE APPOINTMENT TO ASSIGNED RESOURCE - DO NOT DELETE
    public static void rolldownServiceAppointmentToAssignedResource(FSLRollupRolldownController.ObjectMapContainer objectMaps, FSLRollupRolldownController.ObjectNodeContainer objectNodes) {

    }

    // CUSTOM ROLLUP LOGIC FROM ASSIGNED RESOURCE TO SERVICE APPOINTMENT - DO NOT DELETE
    public static void rollupAssignedResourceToServiceAppointment(FSLRollupRolldownController.ObjectMapContainer objectMaps, FSLRollupRolldownController.ObjectNodeContainer objectNodes) {

        for (FSLRollupRolldownController.serviceAppointmentNode saNode : objectNodes.serviceAppointmentNodes.values()) {
            ServiceAppointment sa = saNode.me;

            for (FSLRollupRolldownController.assignedResourceNode arNode : saNode.assignedResourceNodes.values()) {
                AssignedResource ar = arNode.me;

                sa.ARs_Scheduled_Via_Custom_Component__c = ar.Scheduled_Via_Custom_Component__c; // all ARs on the SA should have the same value for Scheduled_Via_Custom_Component__c
            }
        }
    }

    // CUSTOM ROLLUP LOGIC FROM SERVICE APPOINTMENT TO WORK ORDER - DO NOT DELETE
    public static void rollupServiceAppointmentToWorkOrder(FSLRollupRolldownController.ObjectMapContainer objectMaps, FSLRollupRolldownController.ObjectNodeContainer objectNodes) {
        System.debug('+++++Entering rollupServiceAppointmentToWorkOrder');

        // Iterate over work orders
        for (FSLRollupRolldownController.workOrderNode woNode : objectNodes.workOrderNodes.values()) {
            WorkOrder wo = woNode.me;

            //if the work order is not install, tech measure, service, or JSV, don't perform rollup calculations.
            if(!(wo.RecordTypeId == installRTId || wo.RecordTypeId == techMeasureRTId || wo.RecordTypeId == serviceRTId || wo.RecordTypeId == jobSiteVisitRTId)) {
                continue;
            }

            // Set up variables here
            Integer numberOfCanceledSAs = 0;
            Integer numberOfAssignedSAs = 0;
            Integer numberOfNewSAs = 0;
            Integer numberOfUserSuspendedSAs = 0;
            Integer numberOfCompletedSAs = 0;
            Integer numberOfCannotCompleteSAs = 0;
            Integer numberOfMultiDaySAs = 0;

            Id primaryServiceResource_TechMeasure = null;
            Id primaryServiceResource_Install = null;
            Id primaryServiceResource_Service = null;
            Id primaryServiceResource_JobSiteVisit = null;
            
            Date appointmentScheduledOn = null;  //rForce 1305
            Datetime earliestScheduledStartTime = null;
            Datetime latestScheduledEndTime = null;

            Datetime earliestScheduledStartTime_TechMeasure = null;
            Datetime earliestScheduledStartTime_Install = null;
            Datetime earliestScheduledStartTime_Service = null;
            Datetime earliestScheduledStartTime_JobSiteVisit = null;

            Boolean workOrderHasPrimaryAssignedResource = false;

            // Iterate over each work order's service appointments
            for (FSLRollupRolldownController.ServiceAppointmentNode childSANode : woNode.serviceAppointmentNodes.values()) {
                ServiceAppointment sa = childSANode.me;

                if (sa.FSL__IsMultiDay__c) {
                    numberOfMultiDaySAs++;
                }

                // Update the number of assigned service appointments
                if (sa.Status == 'Scheduled'
                || sa.Status == 'Dispatched'
                || sa.Status == 'Scheduled & Assigned'
                || sa.Status == 'Appt Complete / Closed'
                || sa.Status == 'Completed'
                || sa.Status == 'In Progress') {
                    numberOfAssignedSAs++;

                    if (sa.Status == 'Appt Complete / Closed' || sa.Status == 'Completed') {
                        numberOfCompletedSAs++;
                    }

                    // Loop over service appointment's assigned resources to determine which ServiceResourceId to use as the WO's primary resource
                    for (FSLRollupRolldownController.AssignedResourceNode arNode : childSANode.assignedResourceNodes.values()) {
                        AssignedResource ar = arNode.me;
                        if (wo.RecordTypeId == installRTId &&
                            (primaryServiceResource_Install == null || ar.Is_Primary_Resource_On_Work_Order__c)) {
                            primaryServiceResource_Install = ar.ServiceResourceId;
                        } else if (wo.RecordTypeId == serviceRTId &&
                            (primaryServiceResource_Service == null || ar.Is_Primary_Resource_On_Work_Order__c)) {
                            primaryServiceResource_Service = ar.ServiceResourceId;
                        } else if (wo.RecordTypeId == techMeasureRTId &&
                            (primaryServiceResource_TechMeasure == null || ar.Is_Primary_Resource_On_Work_Order__c)) {
                            primaryServiceResource_TechMeasure = ar.ServiceResourceId;
                        } else if (wo.RecordTypeId == jobSiteVisitRTId &&
                            (primaryServiceResource_JobSiteVisit == null || ar.Is_Primary_Resource_On_Work_Order__c)) {
                            primaryServiceResource_JobSiteVisit = ar.ServiceResourceId;
                        }

                        // Note whether the work order has an assigned resource that is flagged as the primary resource.
                        // This is used later to prevent legacy data from being overwritten on multi-resource work orders.
                        if (ar.Is_Primary_Resource_On_Work_Order__c) {
                            workOrderHasPrimaryAssignedResource = true;
                        }
                    }
                }

                // Update the number of assigned service appointments
                if (sa.Status == 'New'
                    || sa.Status == 'None'
                    || sa.Status == null) {
                    numberOfNewSAs++;
                }

                // Update the number of canceled service appointments
                if (sa.Status == 'Canceled' || sa.Status == 'Cancelled') {
                    numberOfCanceledSAs++;
                }

                // Update the number of cannot-complete service appointments
                if (sa.Status == 'Cannot Complete') {
                    numberOfCannotCompleteSAs++;
                }
                if (sa.Status == 'User Suspended') {
                    numberOfUserSuspendedSAs++;
                }
                
                // Update the start and end times/dates for the work order based on scheduled appointments
                if (sa.Status == 'Scheduled'
                || sa.Status == 'Dispatched'
                || sa.Status == 'Scheduled & Assigned'
                || sa.Status == 'Appt Complete / Closed'
                || sa.Status == 'Completed'
                || sa.Status == 'In Progress') {
                    // Calculate the earliest scheduled start time, and latest scheduled end time
                    if (earliestScheduledStartTime == null || (sa.SchedStartTime != null && sa.SchedStartTime < earliestScheduledStartTime)) {
                        earliestScheduledStartTime = sa.SchedStartTime;
                    }

                    if (latestScheduledEndTime == null || (sa.SchedEndTime != null && sa.SchedEndTime > latestScheduledEndTime)) {
                        latestScheduledEndTime = sa.SchedEndTime;
                    }
                    
                    // Update appointmentScheduledOn  rForce 1305
                    if(appointmentScheduledOn == null || (sa.Appointment_Scheduled_On__c != null && sa.Appointment_Scheduled_On__c > appointmentScheduledOn )){
                        appointmentScheduledOn = sa.Appointment_Scheduled_On__c; 
                    }

                    // Based on the service appointment's Work_Order_Type__c, calculate the appropriate type-specific earliest scheduled date
                    switch on sa.Work_Order_Type__c {
                        when 'Tech Measure' {
                            if (earliestScheduledStartTime_TechMeasure == null || (sa.SchedStartTime != null && sa.SchedStartTime < earliestScheduledStartTime_TechMeasure)) {
                                earliestScheduledStartTime_TechMeasure = sa.SchedStartTime;
                            }
                        }
                        when 'Install' {
                            if (earliestScheduledStartTime_Install == null || (sa.SchedStartTime != null && sa.SchedStartTime < earliestScheduledStartTime_Install)) {
                                earliestScheduledStartTime_Install = sa.SchedStartTime;
                            }
                        }
                        when 'Service' {
                            if (earliestScheduledStartTime_Service == null || (sa.SchedStartTime != null && sa.SchedStartTime < earliestScheduledStartTime_Service)) {
                                earliestScheduledStartTime_Service = sa.SchedStartTime;
                            }
                        }
                        when 'Job Site Visit' {
                            if (earliestScheduledStartTime_JobSiteVisit == null || (sa.SchedStartTime != null && sa.SchedStartTime < earliestScheduledStartTime_JobSiteVisit)) {
                                earliestScheduledStartTime_JobSiteVisit = sa.SchedStartTime;
                            }
                        }
                    }
                }
            }
            
            // Set numberOfAssignedSAs
            wo.Number_of_Assigned_Service_Appointments__c = numberOfAssignedSAs;

            // Determine and set WO status
            if (numberOfAssignedSAs > 0 && wo.Status != 'Canceled') {
                // has at least one scheduled SA
                if (numberOfCompletedSAs == numberOfAssignedSAs) {
                    // all scheduled appointments have been completed; set WO status to Completed
                    wo.Status = 'Appt Complete / Closed';
                } else if(wo.Status != 'Appt Complete / Closed') {
                    wo.Status = 'Scheduled & Assigned';
                }
            } else if ( (numberOfNewSAs > 0 || (numberOfNewSAs == 0 && numberOfAssignedSAs== 0 && numberOfUserSuspendedSAs >0 )) && wo.Status != 'Canceled') {
                // no scheduled SAs, but at least one has a status of new/none/null
                wo.Status = 'To be Scheduled';
            } else if (numberOfCannotCompleteSAs > 0  && wo.Status != 'Canceled') {
                wo.Status = 'Suspended / On Hold';
            } else if (woNode.serviceAppointmentNodes.size() == numberOfCanceledSAs && numberOfCanceledSAs > 0) {
                // all service appointments are canceled
                wo.Status = 'Canceled';
                if (wo.Cancel_Reason__c == null) {
                    wo.Cancel_Reason__c = 'Order Cancelled';
                }
            } 

            // Updated scheduled dates on WO
            wo.Scheduled_Start_Time__c = earliestScheduledStartTime;
            wo.Scheduled_End_Time__c = latestScheduledEndTime;
            wo.Scheduled_Appt_Date_Time_Tech_Measure__c = earliestScheduledStartTime_TechMeasure;
            wo.Scheduled_Appt_Date_Time_Install__c = earliestScheduledStartTime_Install;
            wo.Scheduled_Appt_Date_Time_Service__c = earliestScheduledStartTime_Service;
            wo.Scheduled_Appt_Date_Time_Job_Site_Visit__c = earliestScheduledStartTime_JobSiteVisit;
            wo.Appointment_Scheduled_On__c = appointmentScheduledOn;  //rForce 1305

            // Set the primary resource fields on the WO
            if (numberOfAssignedSAs == 0) {
                if (wo.RecordTypeId == installRTId) {
                    wo.Primary_Installer_FSL__c = null;
                } else if (wo.RecordTypeId == serviceRTId) {
                    wo.Primary_Service_FSL__c = null;
                } else if (wo.RecordTypeId == techMeasureRTId) {
                    wo.Primary_Tech_Measure_FSL__c = null;
                } else if (wo.RecordTypeId == jobSiteVisitRTId) {
                    wo.Primary_Visitor__c = null;
                }
            } else if (numberOfAssignedSAs == 1 || (numberOfAssignedSAs > 1 && workOrderHasPrimaryAssignedResource)) {
                // Note: the check using workOrderHasPrimaryAssignedResource is to prevent legacy data from being overwritten.
                if (wo.RecordTypeId == installRTId) {
                    wo.Primary_Installer_FSL__c = primaryServiceResource_Install;
                } else if (wo.RecordTypeId == serviceRTId) {
                    wo.Primary_Service_FSL__c = primaryServiceResource_Service;
                } else if (wo.RecordTypeId == techMeasureRTId) {
                    wo.Primary_Tech_Measure_FSL__c = primaryServiceResource_TechMeasure;
                } else if (wo.RecordTypeId == jobSiteVisitRTId) {
                    wo.Primary_Visitor__c = primaryServiceResource_JobSiteVisit;
                }
            }


            // 1/21/2019: If this WO's duration is changing and it has multiple multi-day SAs, throw an error
            // because the FSL managed package triggers won't allow the duration to roll down in this scenario.
            if (numberOfMultiDaySAs > 1 
                && objectMaps.oldWorkOrders.containsKey(wo.Id) 
                && objectMaps.oldWorkOrders.get(wo.Id).Duration != wo.Duration) {

                throw new UnableToManuallyChangeDuration_CustomException('Unable to manually change the Work Order duration if more than one multi-day Service Appointments exist on this Work Order.  Please use the Resources or Time Slots scheduling forms instead.');
            }
        }

        System.debug('+++++Exiting rollupServiceAppointmentToWorkOrder');
    }

    // CUSTOM ROLLUP LOGIC FROM WORK ORDER TO ORDER - DO NOT DELETE
    public static void rollupWorkOrderToOrder(FSLRollupRolldownController.ObjectMapContainer objectMaps, FSLRollupRolldownController.ObjectNodeContainer objectNodes) {
        System.debug('+++++Exiting rollupWorkOrderToOrders');
        //Iterate over Orders
        for (FSLRollupRolldownController.OrderNode oNode : objectNodes.orderNodes.values()) {
            Order o = oNode.me;
            String OrderStatusBefore = o.status;

            if(o.RecordTypeId == orderServiceRTId) {
                continue; //do not update the service order type.
            }

            Integer numberOfNewTechMeasure = 0;
            Integer numberOfScheduledTechMeasure = 0;
            Integer numberOfCompletedTechMeasure = 0;
            Integer numberOfCancelledTechMeasure = 0;
                
            Integer numberOfNewInstall = 0;
            Integer numberOfScheduledInstall = 0;
            Integer numberOfCompletedInstall = 0;
            Integer numberOfCancelledInstall = 0;
            
            if((o.RecordTypeId == orderCoroRTId || o.RecordTypeId == orderAroRTId) && o.Status != 'Job Closed' && o.Status != 'On Hold') {
                for (FSLRollupRolldownController.WorkOrderNode childWONode : oNode.WorkOrderNodes.values()) {
                    WorkOrder wo = childWONode.me;
                    
                    if(!(wo.RecordTypeId == installRTId || wo.RecordTypeId == techMeasureRTId)) {
                        continue;
                    }

                    if(wo.RecordTypeId == installRTId ){
                        if(wo.Status == 'To be Scheduled') {
                            numberOfNewInstall++;
                        }
                        else if(wo.Status == 'Scheduled & Assigned') {
                            numberOfScheduledInstall++;
                        }
                        else if(wo.Status == 'Appt Complete / Closed') {
                            numberOfCompletedInstall++;
                        }
                        else if(wo.Status == 'Canceled' || wo.Status == 'Cancelled') {
                            numberOfCancelledInstall++;
                        }
                    }   
                    else if(wo.RecordTypeId == techMeasureRTId) {
                        if(wo.Status == 'To be Scheduled') {
                            numberOfNewTechMeasure++;
                        }
                        else if(wo.Status == 'Scheduled & Assigned') {
                            numberOfScheduledTechMeasure++;
                        }
                        else if(wo.Status == 'Appt Complete / Closed') {
                            numberOfCompletedTechMeasure++;
                        }
                        else if(wo.Status == 'Canceled' || wo.Status == 'Cancelled') {
                            numberOfCancelledTechMeasure++;
                        }                    
                    }
                }
                System.debug('@pav new o.Status New '+o.Status);
                System.debug('@pav new numberOfNewTechMeasure New'+OrderStatusBefore);
                if(o.Status != 'Job Closed' && o.Status != 'On Hold' && o.Status != 'Cancelled' && o.Status != 'Pending Cancellation' && o.Backoffice_Check_RollUp__c == 0 && o.Status != 'Pending Cancellation' )
                { 
                    System.debug('@pav numberOfNewTechMeasure'+numberOfNewTechMeasure);
                    System.debug('@pav numberOfScheduledTechMeasure'+numberOfScheduledTechMeasure);
                    System.debug('@pav numberOfCompletedTechMeasure'+numberOfCompletedTechMeasure);
                    System.debug('@pav o.Status'+o.Status);
                    System.debug('@pav o.Status'+o.ordernumber);
                    
                    //Tech Measure Needed -- no other tech measures in progress.
                    if (numberOfNewTechMeasure > 0 && o.Status != 'Cancelled' && o.Status != 'Install Complete' && o.Backoffice_Check_RollUp__c == 0 
                         && !OrderStatusBefore.contains('Install') && !OrderStatusBefore.contains('Job Closed') 
                        && !OrderStatusBefore.contains('Ready to Order') && !OrderStatusBefore.contains('Order Released') 
                       && !OrderStatusBefore.contains('Pending Cancellation') && !OrderStatusBefore.contains('On Hold') &&
                         
                       ( 
                             ((numberOfScheduledTechMeasure > 0 || numberOfCompletedTechMeasure > 0) && o.Status != 'Ready to Order')
                             ||
                             (numberOfScheduledTechMeasure == 0 && numberOfCompletedTechMeasure == 0 && o.Status != 'Ready to Order') 
                             || 
                             (numberOfScheduledTechMeasure == 0 && numberOfCompletedTechMeasure > 0 && (o.status == 'Ready to Order' || o.status == 'Order Released' || o.status == 'Tech Measure Scheduled'))                             
                         )
                      ){  
                        o.Apex_Context__c = true;
                        o.Status = 'Tech Measure Needed';
                    }
                    // Tech Measure Scheduled -- at least one tech measure scheduled. no new tech measure.
                    else if( numberOfScheduledTechMeasure > 0 && o.Status != 'Cancelled' && o.Status != 'Install Complete' /*&& o.Status != 'Draft'*/ 
/*                       && o.Backoffice_Check_RollUp__c == 0 && !OrderStatusBefore.contains('Install') && !OrderStatusBefore.contains('Job Closed') 
                       && !OrderStatusBefore.contains('Ready to Order') && !OrderStatusBefore.contains('Order Released')
                       && !OrderStatusBefore.contains('Pending Cancellation') && !OrderStatusBefore.contains('On Hold'))
                    {
                        o.Apex_Context__c = true;
                        o.Status = 'Tech Measure Scheduled';
                    }                
                    //Ready to Order -- no new tech measures.  no scheduled tech measures.  at least one completed tech measure.
                    else if( numberOfNewTechMeasure == 0 && numberOfScheduledTechMeasure == 0 && numberOfCompletedTechMeasure > 0 
                            && numberOfScheduledInstall == 0 && numberOfCompletedInstall == 0 && o.Status != 'Cancelled' && o.Status != 'Install Complete' 
                            && o.Status != 'Install Scheduled' && o.Status != 'Install Needed' && o.Status != 'Order Released' && !OrderStatusBefore.contains('Install') 
                            && !OrderStatusBefore.contains('Job Closed')) {
                                
                        o.Apex_Context__c = true;
                        o.Status = 'Ready to Order';
                    }
                    //Install Needed -- no tech measures in progress. new install. 
                    else if(numberOfNewTechMeasure == 0 && numberOfScheduledTechMeasure == 0 && numberOfCompletedTechMeasure > 0 && numberOfNewInstall > 0 
                            && o.Status != 'Cancelled' && o.Status != 'Install Complete' && o.Status != 'Ready to Order' && !OrderStatusBefore.contains('Install Scheduled')
                            && o.Status != 'Order Released' ) {
                        o.Apex_Context__c = true;
                        o.Status = 'Install Needed';
                    }
                    //Install Scheduled -- no tech measures in progress. no new installs.
                    else if(   numberOfNewTechMeasure == 0 && numberOfScheduledTechMeasure == 0 && numberOfCompletedTechMeasure > 0 && numberOfNewInstall == 0 
                            && numberOfScheduledInstall > 0 && o.Status != 'Cancelled' && o.Status != 'Install Complete' && o.Status != 'Job Closed' 
                            && o.Status != 'Ready to Order' && o.Status != 'Order Released'  ) {
                        o.Apex_Context__c = true;
                        o.Status = 'Install Scheduled';
                    }   
                    /* lets check
                     * if(numberOfNewTechMeasure == 0 && numberOfScheduledTechMeasure == 0 && numberOfCompletedTechMeasure > 0 && numberOfScheduledInstall == 0 && numberOfCompletedInstall == 0 && o.Open_POS__c != null && o.Open_POS__c == 0 && o.Status != 'Cancelled' && o.Status != 'Install Complete' && o.Status != 'Install Scheduled' && o.Status != 'Install Needed' && o.Status == 'Ready to Order') {
                        o.Apex_Context__c = true;
                        o.Status = 'Order Released';  
                    } */
/*                }              
           }
        }
    }
    

    // CUSTOM ROLLUP LOGIC FROM ORDER TO ACCOUNT - DO NOT DELETE
    public static void rollupOrderToAccount(FSLRollupRolldownController.ObjectMapContainer objectMaps, FSLRollupRolldownController.ObjectNodeContainer objectNodes) {

    }

    public class UnableToManuallyChangeDuration_CustomException extends Exception {}
    */
}