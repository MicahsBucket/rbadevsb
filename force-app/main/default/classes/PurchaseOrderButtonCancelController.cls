/**
 * @File Name          : PurchaseOrderButtonCancelController.cls
 * @Description        : 
 * @Author             : Ramakrishna Manchala
 * @Group              : 
 * @Last Modified By   : Ramakrishna Manchala
 * @Last Modified On   : 1/2/2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    1/2/2020 1:16 PM          Ramakrishna Manchala     Initial Version
**/
public class PurchaseOrderButtonCancelController {
    
    
    @AuraEnabled()
    public static   String  setPurchaseOrderToCancel(String purchaseOrderId) {
       
        try
        {
            
            
            Retail_Purchase_Order__c po = [select id ,Recordtype.developername,order__r.id from Retail_Purchase_Order__c where id =:purchaseOrderId limit 1];
             List<OrderItem  > OrderItems =[select id from OrderItem  where  Retail_Purchase_Order__c =:po.id];
            system.debug(po.Order_Products__r.size());
            if(po.recordtype.developername=='RbA_Purchase_Order')
            {
                po.Status__c='Cancelled';
             
                for(OrderItem oi :OrderItems)  
                {
                 oi.Retail_Purchase_Order__c=null;  
                
                    
                }
                  
              
                if(OrderItems.size()>0)
                update  OrderItems;
                update po;
            }
            else if(po.recordtype.developername=='Cost_Purchase_Order')
            {  
                 po.Status__c='Cancelled';
               List<Retail_Purchase_Order_Items__c > retailPurchaseItems =[select id from Retail_Purchase_Order_Items__c where  Retail_Purchase_Order__c =:po.id];
                if(retailPurchaseItems.size()>0)
                {
                  for(Retail_Purchase_Order_Items__c rpoi :retailPurchaseItems)  
                {
                 rpoi.Retail_Purchase_Order__c=null;
                 
                    
                } 
                    
                if(retailPurchaseItems.size()>0)
                 update retailPurchaseItems;   
                    
                }
                update po;
                
            } 
            
            
        }
        catch(Exception e)
            
        {
            
        }
        
        return null; 
        
    }
    
    
     @AuraEnabled(cacheable=true)
    public static   String  getStatus(String purchaseId) {
        
        Retail_Purchase_Order__c po = [select id ,Recordtype.developername,status__c  from Retail_Purchase_Order__c where id =:purchaseId limit 1];  
        
        return po.Status__c;
    }

}