/******************************************************************
 * Author : Sundeep
 * Class Name :BatchJobPostInstallationSurvey
 * Createdate :
 * Description : Batch Job For Creating Surveys. 
 * Change Log
 * ---------------------------------------------------------------------------
 * Date      Name          Description
 * ---------------------------------------------------------------------------
 * 
 * 
 * ----------------------------------------------------------------------------
 * *****************************************************************/

global class BatchJobPostInstallationSurvey implements Database.Batchable<sObject> {
    
    String query;
    global set<string> OrderIds;
    
    global BatchJobPostInstallationSurvey() {
       
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Date d1 = date.today();
		Date d2 = Date.today().addDays(-1);
        Id recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Service').getRecordTypeId();
         
       /* if(test.isRunningTest()){
            query = 'SELECT Id, Installation_Date__c, OpportunityId, Store_Location__c FROM Order';
        } else {*/
             
            //  id tempstr = '801610000035Bh1AAE';
           // query = 'SELECT Id, Installation_Date__c, Opportunity.owner.Id, OpportunityId, Store_Location__c FROM Order WHERE id IN :OrderIds';
          // query = 'SELECT Id, Installation_Date__c,Opportunity.owner.Id, OpportunityId, Store_Location__c FROM Order WHERE Installation_Date__c = :d AND RecordTypeId != :recordTypeId';
          query = 'SELECT Id, Installation_Date__c,Opportunity.owner.Id, OpportunityId,Survey_Created__c, EffectiveDate, Store_Location__c FROM Order WHERE Survey_Created__c=false and Installation_Date__c <= :d1  and  Installation_Date__c >= :d2 AND RecordTypeId != :recordTypeId';
       // }
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Order> scope) {
        list<order> ordList = new list<order>();
        for(order o:scope){
            ordList.add(o);
        }
        if(!ordList.isEmpty()){
            PostInstallationSurveyHandler.surveyProcessCreation(ordList);
        }
        
        
    }

    
    global void finish(Database.BatchableContext BC) {
        
        
    }
    
   
}