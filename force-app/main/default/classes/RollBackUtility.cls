/**
 * @author Micah Johnson - Demand Chain 2020
 * @description This Utility is fired if an sObject's status is identified in the trigger as needing to rollback the Order status 
 * @param parameters passed in are the old and new maps of the sObject that was inserted/updated as well as a string defining which object needs to be checked
 * @returns N/A
 **/

public with sharing class RollBackUtility {
    public static void RollBackStatuses(map<Id, sObject> newMap, map<Id, sObject> oldMap, string type) {
        System.debug('RollBackUtility nMap>>>> '+ newMap);
        System.debug('RollBackUtility oMap>>>> '+ oldMap);
        System.debug('RollBackUtility oType>>>> '+ type);
        UtilityMethods.disableAllFslTriggers();
        if(type == 'WorkOrder'){
            map<Id, WorkOrder> newWorkOrderMap = new map<Id, WorkOrder>();
            map<Id, WorkOrder> oldWorkOrderMap = new map<Id, WorkOrder>();
            if(oldMap != null){
                oldWorkOrderMap.putAll((map<Id, WorkOrder>)oldMap);
            }
            else{
                oldWorkOrderMap = null;
            }
            newWorkOrderMap.putAll((map<Id, WorkOrder>)newMap);

            Order orderToCheckStatus = [SELECT Id, Apex_Context__c, Status, Time_Order_Released__c, Order_Rolled_Back__c, Time_Ready_to_Order__c, Time_Tech_Measure_Scheduled__c,
                                        Primary_Tech_Measure__c, Primary_Installer__c,Tech_Measure_Date__c,Tech_Measure_Status__c, Primary_Tech_Measure_FSL__c,
                                        Primary_Installer_FSL__c, Primary_Installer_Name_Text__c,
                                        (SELECT Id, Name, Order__c, Signed_Date__c, Completed__c FROM Back_Office_Checks__r)
                                        FROM Order WHERE Id =: newWorkOrderMap.values()[0].Sold_Order__c];
            System.debug('RollBackUtility orderToCheckStatus>>>> '+ orderToCheckStatus);
            orderToCheckStatus.Apex_Context__c = true;
            update orderToCheckStatus;

            Boolean areBOChecksAllComplete = true;
            for(Back_Office_Check__c boc: orderToCheckStatus.Back_Office_Checks__r){
                if(boc.Completed__c == false){
                    areBOChecksAllComplete = false;
                    orderToCheckStatus.Status = 'Draft';
                    break;
                }
            }
            List<WorkOrder> listOfRelatedWorkOrdersToCheck = [SELECT Id, Status, Sold_Order__c, Work_Order_Type__c, Primary_Tech_Measure_FSL__c,
                                                                Primary_Installer_FSL__c
                                                                FROM WorkOrder 
                                                                WHERE Sold_Order__c =: newWorkOrderMap.values()[0].Sold_Order__c 
                                                                AND Work_Order_Type__c =: newWorkOrderMap.values()[0].Work_Order_Type__c 
                                                                ];
            System.debug('RollBackUtility listOfRelatedWorkOrdersToCheck>>>> '+ listOfRelatedWorkOrdersToCheck);

            
            ////CHECKING IF WO IS TECH MEASURE///////
            if(newWorkOrderMap.values()[0].Work_Order_Type__c == 'Tech Measure'){
                boolean isToBeScheduledStatus = false;
                boolean isScheduledStatus = false;
                integer WorkOrdersListSize = listOfRelatedWorkOrdersToCheck.size();
                integer numberOfCanceled = 0;
                integer numberOfCompleted = 0;
    
                if(!listOfRelatedWorkOrdersToCheck.isEmpty()){
                    for(WorkOrder wo: listOfRelatedWorkOrdersToCheck){
                        if(wo.Status == 'To be Scheduled' ){
                            System.debug('To be Scheduled');
                            isToBeScheduledStatus = true;
                            wo.Primary_Tech_Measure_FSL__c = null;
                            update wo;
                        }
                        if(wo.Status == 'Scheduled & Assigned' ){
                            System.debug('Scheduled & Assigned');
                            isScheduledStatus = true;
                        }
                        if(wo.Status == 'Canceled'  ){
                            numberOfCanceled += 1;
                        }
                        if(wo.Status == 'Appt Complete / Closed'  ){
                            System.debug('Appt Complete / Closed');
                            numberOfCompleted += 1;
                        }
                    }
                }
                // System.debug('isToBeScheduledStatus= '+isToBeScheduledStatus);
                // System.debug('isScheduledStatus= '+isScheduledStatus);
                // System.debug('numberOfCanceled= '+numberOfCanceled);
                // System.debug('numberOfCompleted= '+numberOfCompleted);
                if(isToBeScheduledStatus == true && orderToCheckStatus.Status != 'Ready to Order'){
                    if(orderToCheckStatus.Status != 'On Hold'){
                        if(areBOChecksAllComplete == true){
                            orderToCheckStatus.Status = 'Tech Measure Needed';
                        }
                        orderToCheckStatus.Tech_Measure_Status__c = 'Tech Measure Needed';
                        orderToCheckStatus.Primary_Tech_Measure__c = null;
                        orderToCheckStatus.Primary_Tech_Measure_FSL__c = null;
                        orderToCheckStatus.Tech_Measure_Date__c = null;
                        ServiceAppointment sa = [SELECT Id, Status, Primary_Service_Resource__c, ParentRecordId, 
                                                    Primary_Tech_Measure__c, SchedStartTime, SchedEndTime 
                                                    FROM ServiceAppointment
                                                    WHERE ParentRecordId =: newWorkOrderMap.values()[0].Id];
                        sa.Status = 'New';
                        sa.Primary_Service_Resource__c = null;
                        sa.Primary_Tech_Measure__c = null;
                        sa.SchedStartTime = null;
                        sa.SchedEndTime = null;
                        if(!Test.isRunningTest()){
                            update sa;
                        }

                    }
                    orderToCheckStatus.Order_Rolled_Back__c = true;
                }
                if(isScheduledStatus == true &&  isToBeScheduledStatus == false && orderToCheckStatus.Status != 'Ready to Order'){
                    if(orderToCheckStatus.Status != 'On Hold'){
                        if(areBOChecksAllComplete == true){
                            orderToCheckStatus.Status = 'Tech Measure Scheduled';
                        }
                        orderToCheckStatus.Tech_Measure_Status__c = 'Tech Measure Scheduled';
                    }
                    orderToCheckStatus.Order_Rolled_Back__c = true;
                }
                if(isScheduledStatus == false &&  isToBeScheduledStatus == false && (numberOfCanceled == WorkOrdersListSize) && orderToCheckStatus.Status != 'Ready to Order'){
                    if(orderToCheckStatus.Status != 'On Hold'){
                        if(areBOChecksAllComplete == true){
                            orderToCheckStatus.Status = 'Tech Measure Needed';
                        }
                        orderToCheckStatus.Tech_Measure_Status__c = 'Tech Measure Needed';
                        orderToCheckStatus.Primary_Tech_Measure__c = null;
                        orderToCheckStatus.Primary_Tech_Measure_FSL__c = null;
                        orderToCheckStatus.Tech_Measure_Date__c = null;
                        ServiceAppointment sa = [SELECT Id, Status, Primary_Service_Resource__c, ParentRecordId, 
                                                    Primary_Tech_Measure__c, SchedStartTime, SchedEndTime 
                                                    FROM ServiceAppointment
                                                    WHERE ParentRecordId =: newWorkOrderMap.values()[0].Id];
                        sa.Status = 'New';
                        sa.Primary_Service_Resource__c = null;
                        sa.Primary_Tech_Measure__c = null;
                        sa.SchedStartTime = null;
                        sa.SchedEndTime = null;
                        if(!Test.isRunningTest()){
                            update sa;
                        }
                    }
                    orderToCheckStatus.Order_Rolled_Back__c = true;
                }
                if(isScheduledStatus == false &&  isToBeScheduledStatus == false && (numberOfCompleted == WorkOrdersListSize)){
                    if(orderToCheckStatus.Status != 'On Hold'){
                        if(areBOChecksAllComplete == true){
                            orderToCheckStatus.Status = 'Ready to Order';
                        }
                        orderToCheckStatus.Tech_Measure_Status__c = 'Ready To Order';
                    }
                }
                system.debug('orderToCheckStatus>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>' + orderToCheckStatus);
                if(oldWorkOrderMap != null){
                    if(newWorkOrderMap.values()[0].Status != oldWorkOrderMap.values()[0].Status && listOfRelatedWorkOrdersToCheck.size() > 1){
                        orderToCheckStatus.Order_Rolled_Back__c = true;
                    }
                }
               
                update orderToCheckStatus;

                
            }
            ////CHECKING IF WO IS INSTALL///////
            if(newWorkOrderMap.values()[0].Work_Order_Type__c == 'Install' && areBOChecksAllComplete == true){
                boolean isToBeScheduledStatus = false;
                boolean isScheduledStatus = false;
                integer WorkOrdersListSize = listOfRelatedWorkOrdersToCheck.size();
                integer numberOfCanceled = 0;
                integer numberOfCompleted = 0;
                integer numberOfCompletedWORelease = 0;
                integer numberOfSandA = 0;
        
                if(!listOfRelatedWorkOrdersToCheck.isEmpty()){
                    for(WorkOrder wo: listOfRelatedWorkOrdersToCheck){
                        if(wo.Status == 'To be Scheduled'){
                            isToBeScheduledStatus = true;
                            wo.Primary_Installer_FSL__c = null;
                            update wo;
                        }
                        if(wo.Status == 'Scheduled & Assigned' ){
                            isScheduledStatus = true;
                            numberOfSandA += 1;
                        }
                        if(wo.Status == 'Canceled' ){
                            numberOfCanceled += 1;
                        }
                        if(wo.Status == 'Appt Complete / Closed' && orderToCheckStatus.Time_Order_Released__c == null ){
                            numberOfCompletedWORelease += 1;
                        }
                        if(wo.Status == 'Appt Complete / Closed' && orderToCheckStatus.Time_Order_Released__c != null ){
                            numberOfCompleted += 1;
                        }
                    }
                }
                if(isToBeScheduledStatus == true && orderToCheckStatus.Time_Order_Released__c == null){
                    if(orderToCheckStatus.Status != 'On Hold'){
                        orderToCheckStatus.Status = 'Ready to Order';
                    }
                    orderToCheckStatus.Order_Rolled_Back__c = true;
                }
                if(isToBeScheduledStatus == true && orderToCheckStatus.Time_Order_Released__c != null){
                    if(orderToCheckStatus.Status != 'On Hold'){
                        system.debug('In Here at 159');
                        orderToCheckStatus.Status = 'Install Needed';
                        orderToCheckStatus.Primary_Installer__c = null;
                        orderToCheckStatus.Primary_Installer_FSL__c = null;
                        ServiceAppointment sa = [SELECT Id, Status, Primary_Service_Resource__c, ParentRecordId, 
                                                    Primary_Installer__c, SchedStartTime, SchedEndTime 
                                                    FROM ServiceAppointment
                                                    WHERE ParentRecordId =: newWorkOrderMap.values()[0].Id];
                        sa.Status = 'New';
                        sa.Primary_Service_Resource__c = null;
                        sa.SchedStartTime = null;
                        sa.SchedEndTime = null;
                        if(!Test.isRunningTest()){
                            update sa;
                        }
                        
                    }
                    orderToCheckStatus.Order_Rolled_Back__c = true;
                }
                
                if(isScheduledStatus == true &&  isToBeScheduledStatus == false && (numberOfSandA == WorkOrdersListSize) && orderToCheckStatus.Time_Order_Released__c != null){
                    if(orderToCheckStatus.Status != 'On Hold'){
                        orderToCheckStatus.Status = 'Install Scheduled';
                    }
                }
                if(isScheduledStatus == false &&  isToBeScheduledStatus == false && (numberOfCanceled == WorkOrdersListSize) && orderToCheckStatus.Time_Order_Released__c != null){
                    if(orderToCheckStatus.Status != 'On Hold'){
                        system.debug('In Here at 159');
                        orderToCheckStatus.Status = 'Install Needed';
                        orderToCheckStatus.Primary_Installer__c = null;
                        orderToCheckStatus.Primary_Installer_FSL__c = null;
                        ServiceAppointment sa = [SELECT Id, Status, Primary_Service_Resource__c, ParentRecordId, 
                                                    Primary_Installer__c, SchedStartTime, SchedEndTime 
                                                    FROM ServiceAppointment
                                                    WHERE ParentRecordId =: newWorkOrderMap.values()[0].Id];
                        sa.Status = 'New';
                        sa.Primary_Service_Resource__c = null;
                        sa.Primary_Installer__c = null;
                        sa.SchedStartTime = null;
                        sa.SchedEndTime = null;
                        if(!Test.isRunningTest()){
                            update sa;
                        }
                        
                    }
                    orderToCheckStatus.Order_Rolled_Back__c = true;
                }
                if(isScheduledStatus == false &&  isToBeScheduledStatus == false && (numberOfCompletedWORelease == WorkOrdersListSize) && orderToCheckStatus.Time_Order_Released__c != null){
                    if(orderToCheckStatus.Status != 'On Hold'){
                        orderToCheckStatus.Status = 'Ready to Order';
                    }
                }
                if(isScheduledStatus == true &&  isToBeScheduledStatus == false && (numberOfCompleted == WorkOrdersListSize) && orderToCheckStatus.Time_Order_Released__c != null){
                    if(orderToCheckStatus.Status != 'On Hold'){
                        orderToCheckStatus.Status = 'Install Complete';
                    }
                }
                system.debug('orderToCheckStatus>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>' + orderToCheckStatus);
                if(oldWorkOrderMap != null){
                    if(newWorkOrderMap.values()[0].Status != oldWorkOrderMap.values()[0].Status && listOfRelatedWorkOrdersToCheck.size() > 1){
                        orderToCheckStatus.Order_Rolled_Back__c = true;
                    }
                }
                // orderToCheckStatus.Apex_Context__c = false;
                update orderToCheckStatus;
                
            }
        }
        if(type == 'PurchaseOrder'){
            system.debug('In PO Rollback');
            UtilityMethods.disableAllFslTriggers();
            // UtilityMethods.enableOrderTrigger();
            map<Id, Purchase_Order__c> newPurchaseOrderMap = new map<Id, Purchase_Order__c>();
            map<Id, Purchase_Order__c> oldPurchaseOrderMap = new map<Id, Purchase_Order__c>();
            if(oldMap != null){
                oldPurchaseOrderMap.putAll((map<Id, Purchase_Order__c>)oldMap);
            }
            newPurchaseOrderMap.putAll((map<Id, Purchase_Order__c>)newMap);

            List<Order> relatedOrderList= [SELECT Id, Apex_Context__c, Status, Order_Rolled_Back__c, Time_Tech_Measure_Needed__c, Time_Install_Scheduled__c, Time_Ready_to_Order__c, Time_Order_Released__c, Time_Install_Needed__c FROM Order WHERE Id =: newPurchaseOrderMap.values().Order__c ];
            System.debug(relatedOrderList);
            System.debug('RollBackUtility orderToCheckStatus>>>> '+ relatedOrderList);
            relatedOrderList[0].Apex_Context__c = true;
            update relatedOrderList[0];
            Boolean areBOChecksAllComplete = true;
            for(Back_Office_Check__c boc: relatedOrderList[0].Back_Office_Checks__r){
                if(boc.Completed__c == false){
                    areBOChecksAllComplete = false;
                    relatedOrderList[0].Status = 'Draft';
                    break;
                }
            }
            List<WorkOrder> listOfRelatedWorkOrdersToCheck = [SELECT Id, Status, Sold_Order__c, Work_Order_Type__c 
                                                                FROM WorkOrder 
                                                                WHERE Sold_Order__c =: relatedOrderList[0].Id
                                                                ];
            System.debug('RollBackUtility listOfRelatedWorkOrdersToCheck>>>> '+ listOfRelatedWorkOrdersToCheck);
            
            List<Purchase_Order__c> listOfPurchaseOrdersToCheck = new List<Purchase_Order__c>();
            
            relatedOrderList[0].Apex_Context__c = true;
            update relatedOrderList[0];
    
            List<OrderItem> oiList = [SELECT Id, OrderId, Product2.RecordType.Name, Product2.RecordTypeId, Purchase_Order__c FROM OrderItem WHERE OrderId =: newPurchaseOrderMap.values().Order__c AND Product2.RecordTypeId = '01261000000SG0zAAG' AND Purchase_Order__c = null];
            List<Purchase_Order__c> poList = new List<Purchase_Order__c>();
            if(relatedOrderList.size() > 0){
                poList = [SELECT Id, Order__c, Status__c, RecordTypeName__c  FROM Purchase_Order__c WHERE Order__c =: newPurchaseOrderMap.values().Order__c AND RecordTypeName__c != 'Service_Purchase_Order'];
                System.debug(poList);
                if(poList.size()>0){
                    for(Purchase_Order__c po: poList){
                        listOfPurchaseOrdersToCheck.add(po);
                    }
                }   
            }
    
            boolean isInProgress = false;
            boolean isReleasedStatus = false;
            integer PurchaseOrdersListSize = listOfPurchaseOrdersToCheck.size();
            integer numberOfCanceled = 0;
            boolean isConfirmed = false;
    
            if(!listOfPurchaseOrdersToCheck.isEmpty()){
                for(Purchase_Order__c po: poList){
                    if(po.Status__c == 'In Process'){
                        isInProgress = true;
                    }
                    if(po.Status__c == 'Released' && oiList.Size()==0){
                       isReleasedStatus = true;
                    }
                    if(po.Status__c == 'Confirmed' ){
                       isConfirmed = true;
                    }
                    if(po.Status__c == 'Cancelled'){
                       numberOfCanceled += 1;
                    }
                }
            }
            if(numberOfCanceled > 0){
                if(relatedOrderList[0].Status != 'On Hold'){
                    if(areBOChecksAllComplete == true){
                        relatedOrderList[0].Status = 'Ready to Order';
                    }
                    if(relatedOrderList[0].Time_Ready_To_Order__c == null){
                        relatedOrderList[0].Time_Ready_To_Order__c = Datetime.now();
                    }
                }
                if(relatedOrderList[0].Order_Rolled_Back__c == true){
                    relatedOrderList[0].Order_Rolled_Back__c = false;
                }
                else if(relatedOrderList[0].Order_Rolled_Back__c == false){
                    relatedOrderList[0].Time_Ready_to_Order__c = Datetime.now();
                }
            }
            if(isInProgress == true ){
                if(relatedOrderList[0].Status != 'On Hold'){
                    if(areBOChecksAllComplete == true){
                        relatedOrderList[0].Status = 'Ready to Order';
                    }
                    if(relatedOrderList[0].Time_Ready_To_Order__c == null){
                        relatedOrderList[0].Time_Ready_To_Order__c = Datetime.now();
                    }
                }
                if(relatedOrderList[0].Order_Rolled_Back__c == true){
                    relatedOrderList[0].Order_Rolled_Back__c = false;
                }
                else if(relatedOrderList[0].Order_Rolled_Back__c == false){
                    relatedOrderList[0].Time_Ready_to_Order__c = Datetime.now();
                }
            }
            if(isReleasedStatus == true &&  isInProgress == false && oiList.Size() == 0){
                if(relatedOrderList[0].Status != 'On Hold'){
                    if(areBOChecksAllComplete == true){
                        relatedOrderList[0].Status = 'Order Released';
                    }
                    if(relatedOrderList[0].Time_Order_Released__c == null){
                        relatedOrderList[0].Time_Order_Released__c = Datetime.now();
                    }
                }
                if(relatedOrderList[0].Order_Rolled_Back__c == true){
                    relatedOrderList[0].Order_Rolled_Back__c = false;
                    if(relatedOrderList[0].Time_Order_Released__c == null){
                        relatedOrderList[0].Time_Order_Released__c = Datetime.now();
                    }
                }
                else if(relatedOrderList[0].Order_Rolled_Back__c == false){
                    if(relatedOrderList[0].Time_Order_Released__c == null){
                        relatedOrderList[0].Time_Order_Released__c = Datetime.now();
                    }
                }
            }
            if(isReleasedStatus == false &&  isInProgress == false && isConfirmed == true ){
                if(relatedOrderList[0].Status != 'On Hold'){
                    
                    if(relatedOrderList[0].Time_Install_Needed__c != null && relatedOrderList[0].Time_Install_Scheduled__c == null){
                        if(areBOChecksAllComplete == true){
                            relatedOrderList[0].Status = 'Install Needed';
                        }
                        relatedOrderList[0].Install_Order_Status__c = 'To Be Scheduled';
                        relatedOrderList[0].Time_Install_Needed__c = Datetime.now();
                    }
                    else if(relatedOrderList[0].Time_Install_Needed__c != null && relatedOrderList[0].Time_Install_Scheduled__c != null){
                        if(areBOChecksAllComplete == true){
                            relatedOrderList[0].Status = 'Install Scheduled';
                        }
                        relatedOrderList[0].Install_Order_Status__c = 'Scheduled & Assigned';
                        relatedOrderList[0].Time_Install_Scheduled__c = Datetime.now();
                    }
                }
                if(relatedOrderList[0].Order_Rolled_Back__c == true){
                    relatedOrderList[0].Order_Rolled_Back__c = false;
                }
                else if(relatedOrderList[0].Order_Rolled_Back__c == false){
                    relatedOrderList[0].Time_Install_Needed__c = Datetime.now();
                }
            }
            if(isReleasedStatus == false &&  isInProgress == false && isConfirmed == false  && (numberOfCanceled == PurchaseOrdersListSize)){
                if(relatedOrderList[0].Status != 'On Hold'){
                    if(areBOChecksAllComplete == true){
                        relatedOrderList[0].Status = 'Ready to Order';
                    }
                    relatedOrderList[0].Install_Order_Status__c = 'To Be Scheduled';
                    // if(relatedOrderList[0].Time_Ready_To_Order__c == null){
                    //     relatedOrderList[0].Time_Ready_To_Order__c = Datetime.now();
                    // }
                }
                if(relatedOrderList[0].Order_Rolled_Back__c == true){
                    relatedOrderList[0].Order_Rolled_Back__c = false;
                }
                else if(relatedOrderList[0].Order_Rolled_Back__c == false){
                    relatedOrderList[0].Time_Ready_to_Order__c = Datetime.now();
                }
            }
            if(relatedOrderList[0].Order_Rolled_Back__c == true){
                relatedOrderList[0].Order_Rolled_Back__c = false;
            }
            system.debug('relatedOrderList[0]>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>' + relatedOrderList[0]);
            
            update relatedOrderList[0];

            // relatedOrderList[0].Apex_Context__c = false;
            // update relatedOrderList[0];
            
            UtilityMethods.enableAllFslTriggers();
        }
    }
}