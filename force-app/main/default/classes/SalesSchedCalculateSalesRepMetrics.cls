/**
 * @File Name          : SalesSchedCalculateSalesRepMetrics.cls
 * @Description        :
 * @Author             : James Loghry (Demand Chain)
 * @Group              :
 * @Last Modified By   : Connor.Davis@andersencorp.com
 * @Last Modified On   : 02-16-2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/7/2019, 12:30:41 PM   James Loghry (Demand Chain)     Initial Version
**/
public class SalesSchedCalculateSalesRepMetrics  {
	/** @description Calculates Sales Reps CloseRates and RPA (SS19-84 / SS19-97). Could be called from Batch or other action.
	  * ASSUMPTION:  All User context scrubbing/selection has been done by the instanciation of this class.  No additional restrictions are used here.
	  * @author Demand Chain -  */

	////////////////////////////////////////
	// CLASS VARIABLES
	////////////////////////////////////////
	//public static String C_strAppointmentCloseRateMetricWindow = 'LAST_N_DAYS:14';
	//public static String C_strAppointmentRPAMetricWindow = 'LAST_N_MONTHS:6';
	private static String C_strSalesOrderStatus_SALE = 'Sale';
	private static String C_strSalesOrderStatus_NOTHOME = 'Not Home';
    private static String C_strSalesCapacityField_USER = 'User__c';
	private static String C_strSalesOrderField_RESULT = 'Result__c';
	private static String C_strOpportunityField_RESIT = 'Same_Rep_Resit__c';
	private static String C_strOpportunityField_REPEATSALE = 'Repeat_Sale__c';
	private static String C_strOpportunityField_FOLLOWUP = 'Follow_Up__c';
	private static String C_strOpportunityField_CANCELSAVE = 'Cancel_Save__c';
    private static String C_strOpportunityField_OWNERID = 'OwnerId';
    private static String C_strOpportunityField_LASTDNSREP = 'Last_DNS_Rep__c';
    private static string C_strOpportunityField_CANCELORDER = 'Canceled_Order__c';
  



	////////////////////////////////////////
	// MAIN WORKER METHODS
	////////////////////////////////////////
    public static void calculateMetrics(Date reportDate){
		// Contact.Unavailable__c is the API name for Exclude from sales manager table
        List<User> users = [Select Id From User Where Profile.Name = 'Partner RMS-Sales' and IsActive = true and Contact.Unavailable__c = false];
        calculateMetrics(reportDate, users);
    }

    public static void calculateMetrics(Date reportDate, List<User> lstScopedUsers){
  		// PREPARE:  Instanciate the master array to map Rep to metrics for subsequent measurements
		Map<Id, SalesSchedCalculateSalesRepMetrics.UserMetrics> mapMetrics = new Map<Id, SalesSchedCalculateSalesRepMetrics.UserMetrics>();
		for(User objUser : lstScopedUsers) {
			mapMetrics.put(objUser.Id, new SalesSchedCalculateSalesRepMetrics.UserMetrics(objUser.Id));
		}
        Set<Id> userIds = mapMetrics.keySet();

		// PREPARE:  Get user's Sales_Rep_Reports__r. (Should be only one, if one doesn't exist then create one for the user)
		Map<Id, Sales_Rep_Report__c> mapReports = new Map<Id, Sales_Rep_Report__c>();
		for(User objUser : [SELECT Id, (SELECT Id from Sales_Rep_Reports__r Where Date__c = :reportDate ORDER BY LastModifiedDate DESC LIMIT 1) from User WHERE Id in :userIds]) {
			if(!objUser.Sales_Rep_Reports__r.isEmpty()) {
				mapReports.put(objUser.Id, objUser.Sales_Rep_Reports__r);
			} else {
				mapReports.put(objUser.Id, new Sales_Rep_Report__c(User__c=objUser.Id, Date__c = reportDate));
			}
		}

        //To get the last "fully resulted day", subtract two days from the next sales date / tomorrow.
        reportDate = reportDate.addDays(-2);
		// SS19-84:  Fill Rep array with data for close rate
		Date appointmentsNDays = reportDate.addDays(-13);
        system.debug('Database.query(strQueryCloseRate)---'+Database.query(strQueryCloseRate));
        for(AggregateResult objAR : Database.query(strQueryCloseRate)) {
            System.debug('13 days records--->'+JSON.serialize(objAR));

           //system.debug('objAR-----13 day--->'+objAR);
			// Store individual aggregate fields
			Id idUser = (Id)objAR.get('User__c');
			String strResult = (String)objAR.get(C_strSalesOrderField_RESULT);
			Boolean bolSameRepResit = (Boolean)objAR.get(C_strOpportunityField_RESIT);
			Boolean bolRepeatSale = (Boolean)objAR.get(C_strOpportunityField_REPEATSALE);
			Boolean bolFollowUp = (Boolean)objAR.get(C_strOpportunityField_FOLLOWUP);
			Boolean bolCancelSave = (Boolean)objAR.get(C_strOpportunityField_CANCELSAVE);
            String ownerId = (String)objAR.get(C_strOpportunityField_OWNERID);
            String lastDNS = (String)objAR.get(C_strOpportunityField_LASTDNSREP);
			Integer intRecCount = (Integer)objAR.get('RecCount');
            Boolean bolCancelOrder = (Boolean)objAR.get(C_strOpportunityField_CANCELORDER);
			System.debug('User Id For UM for the 13 days records--->'+ ownerId);
            System.debug('User Id For UM for the 13 days records--->'+ lastDNS);
			// Pull Metric instance for this user and result
            System.debug('bolCancelOrder--->'+ bolCancelOrder);

            if(mapMetrics != null){
                UserMetrics um = mapMetrics.get(idUser);
                if(um != null){
                    Map<String, SalesSchedCalculateSalesRepMetrics.UserMetricResult> umrMap = um.mapMetric;
                    if(umrMap != null){
                        UserMetricResult umr = umrMap.get(strResult);
                        if(umr != null){
                            // Push Metric into Object
						 if(bolSameRepResit && !bolCancelSave && !bolCancelOrder && !bolRepeatSale) {
                                System.debug('JWL: same rep resit count: 13 days records---> ' + intRecCount);
                                umr.intSameRepResitCount += intRecCount;
                            } else if(bolSameRepResit && bolCancelOrder) {
                                //System.debug('JWL: repeat sale count: 13 days records---> ' + intRecCount);
                            } else if(bolRepeatSale) {
                                System.debug('JWL: repeat sale count: 13 days records---> ' + intRecCount);
                                //umr.intRepeatSaleCount += intRecCount;
                            } else if(bolFollowUp) {
                                System.debug('JWL: followup: 13 days records---> ' + intRecCount);
                                umr.intFollowUpCount += intRecCount;
                            } else if(bolCancelSave && bolSameRepResit) {
                                System.debug('JWL: cancel save: 13 days records---> ' + intRecCount);
                                umr.intCancelSaveCount += intRecCount;
                            } else if(bolCancelSave && !bolSameRepResit) {
                                System.debug('JWL: cancel save: 13 days records---> ' + intRecCount);
                                umr.intCancelSaveCount += intRecCount;
                            }else if(bolCancelOrder && !bolSameRepResit) {
                                System.debug('JWL: cancel order: 13 days records---> ' + intRecCount);
                                umr.intCancelOrder += intRecCount;
                            } 
                            else {
                                System.debug('JWL: general: 13 days records---> ' + intRecCount);
                                umr.intGeneralCount += intRecCount;
                            }
                            umrMap.put(strResult,umr);
                            System.debug('User Map : 13 days records--->' + umrMap);
                        }
                    }
                }
            }
		}

        
        appointmentsNDays = reportDate.addDays(-27);
        for(AggregateResult objAR : Database.query(strQueryRPA)) {
                  // system.debug('objAR-----27 day--->'+objAR);

			//System.debug(JSON.serialize(objAR));

			// Store individual aggregate fields
			Id idUser = (Id)objAR.get(C_strSalesCapacityField_USER);
			String strResult = (String)objAR.get(C_strSalesOrderField_RESULT);
			Boolean bolSameRepResit = (Boolean)objAR.get(C_strOpportunityField_RESIT);
			Boolean bolRepeatSale = (Boolean)objAR.get(C_strOpportunityField_REPEATSALE);
			Boolean bolFollowUp = (Boolean)objAR.get(C_strOpportunityField_FOLLOWUP);
			Boolean bolCancelSave = (Boolean)objAR.get(C_strOpportunityField_CANCELSAVE);
            Boolean bolCancelOrder = (Boolean)objAR.get(C_strOpportunityField_CANCELORDER);
            String ownerId = (String)objAR.get(C_strOpportunityField_OWNERID);
            String lastDNS = (String)objAR.get(C_strOpportunityField_LASTDNSREP);
			Double dblSalesValue = objAR.get('SalesValueSum')==null?0:(Double)objAR.get('SalesValueSum');
			Integer intRecCount = (Integer)objAR.get('RecCount');

			// Pull Metric instance for this user and result
			if(mapMetrics != null){
                UserMetrics um = mapMetrics.get(idUser);
                if(um != null){
                    Map<String, SalesSchedCalculateSalesRepMetrics.UserMetricResult> umrMap = um.mapMetric;
                    if(umrMap != null){
                        UserMetricResult umr = umrMap.get(strResult);
                        if(umr != null){
                            // Push Metric into Object
							if(bolSameRepResit && !bolCancelSave && !bolCancelOrder && !bolRepeatSale) {
								umr.dblSameRepResitSum28d += dblSalesValue;
                                umr.intSameRepResitCount28d += intRecCount;
							}else if(bolSameRepResit && bolCancelOrder) {
                                
                            }else if(bolRepeatSale) {
								//umr.dblRepeatSaleSum28d = dblSalesValue;
                                //umr.intRepeatSaleCount28d = intRecCount;
							}else if(bolFollowUp) {
								umr.dblFollowUpSum28d += dblSalesValue;
                                umr.intFollowUpCount28d += intRecCount;
							}else if(bolCancelSave && bolSameRepResit) {
								umr.dblCancelSaveSum28d += dblSalesValue;
                                umr.intCancelSaveCount28d += intRecCount;
							}else if(bolCancelSave && !bolSameRepResit) {
								umr.dblCancelSaveSum28d += dblSalesValue;
                                umr.intCancelSaveCount28d += intRecCount;
							}else if(bolCancelOrder && !bolSameRepResit) {
								umr.dblCancelOrderSum28d += dblSalesValue;
                                umr.intCancelOrderCount28d += intRecCount;
							}else{
								umr.dblGeneralSum28d += dblSalesValue;
                                umr.intGeneralCount28d += intRecCount;
							}
                            umrMap.put(strResult, umr);
                        }
                    }
                }
            }
		}
		
        // SS19-97:  Fill Rep array with data for Sales Rep RPA - 6mo
		appointmentsNDays = reportDate.addMonths(-6);
        system.debug('Database.query(strQueryRPA)---'+Database.query(strQueryRPA));
        for(AggregateResult objAR : Database.query(strQueryRPA)) {

			System.debug('for 6 months RPA' + JSON.serialize(objAR));

			// Store individual aggregate fields
			Id idUser = (Id)objAR.get(C_strSalesCapacityField_USER);
			String strResult = (String)objAR.get(C_strSalesOrderField_RESULT);
			Boolean bolSameRepResit = (Boolean)objAR.get(C_strOpportunityField_RESIT);
			Boolean bolRepeatSale = (Boolean)objAR.get(C_strOpportunityField_REPEATSALE);
			Boolean bolFollowUp = (Boolean)objAR.get(C_strOpportunityField_FOLLOWUP);
			Boolean bolCancelSave = (Boolean)objAR.get(C_strOpportunityField_CANCELSAVE);
            Boolean bolCancelOrder = (Boolean)objAR.get(C_strOpportunityField_CANCELORDER);
            String ownerId = (String)objAR.get(C_strOpportunityField_OWNERID);
            String lastDNS = (String)objAR.get(C_strOpportunityField_LASTDNSREP);
			Double dblSalesValue = objAR.get('SalesValueSum')==null?0:(Double)objAR.get('SalesValueSum');
			Integer intRecCount = (Integer)objAR.get('RecCount');

			// Pull Metric instance for this user and result
			if(mapMetrics != null){
                UserMetrics um = mapMetrics.get(idUser);
                if(um != null){
                    Map<String, SalesSchedCalculateSalesRepMetrics.UserMetricResult> umrMap = um.mapMetric;
                    if(umrMap != null){
                        UserMetricResult umr = umrMap.get(strResult);
                        if(umr != null ){ 
                            // Push Metric into Object
							if(bolSameRepResit && !bolCancelSave && !bolCancelOrder && !bolRepeatSale) {
								umr.dblSameRepResitSum6mo += dblSalesValue;
                                umr.intSameRepResitCount6mo += intRecCount;
							}  else if(bolSameRepResit && bolCancelOrder) {
                                
                            }else if(bolRepeatSale) {
								//umr.dblRepeatSaleSum6mo = dblSalesValue;
                                //umr.intRepeatSaleCount6mo = intRecCount;
							}else if(bolFollowUp) {
								umr.dblFollowUpSum6mo += dblSalesValue;
                                umr.intFollowUpCount6mo += intRecCount;
							}else if(bolCancelSave && bolSameRepResit) {
								umr.dblCancelSaveSum6mo += dblSalesValue;
                                umr.intCancelSaveCount6mo += intRecCount;
							}else if(bolCancelSave && !bolSameRepResit) {
								umr.dblCancelSaveSum6mo += dblSalesValue;
                                umr.intCancelSaveCount6mo += intRecCount;
							}else if(bolCancelOrder && !bolSameRepResit) {
								umr.dblCancelOrderSum6mo += dblSalesValue;
                                umr.intCancelOrderCount6mo += intRecCount;
							}else{
								umr.dblGeneralSum6mo += dblSalesValue;
                                umr.intGeneralCount6mo += intRecCount;
							}
                            umrMap.put(strResult, umr);
                        }
                    }
                }
            }
		}


        appointmentsNDays = reportDate.addDays(-13);
        for(AggregateResult objAR : Database.query(strQueryNDayAppointments)){
            Id userId = (Id)objAR.get(C_strSalesCapacityField_USER);
			mapReports.get(userId).Appointments_14_Days__c = (Integer)objAR.get('RecCount');
        }

        appointmentsNDays = reportDate.addDays(-2);
        for(AggregateResult objAR : Database.query(strQueryNDayAppointments)){
            Id userId = (Id)objAR.get(C_strSalesCapacityField_USER);
			mapReports.get(userId).Appointments_3_Days__c = (Integer)objAR.get('RecCount');
        }

		// Spin and create summaries for use
		for(SalesSchedCalculateSalesRepMetrics.UserMetrics objMetric : mapMetrics.values()) {
			objMetric.createSummary();
		}

		// START CALCULATOR AT REP LEVEL
		for(SalesSchedCalculateSalesRepMetrics.UserMetrics objMetric : mapMetrics.values()) {
            ////////////////////////////////////
			// SS19-84:  Calculate the user's CloseRate then store the value in Sales_Rep_Reports__r.Close_Rate__c
			////////////////////////////////////
			//System.debug(' START CALCULATOR AT REP LEVEL ' + objMetric);
			// Target calcuation = (Number of Sales) / (Number of Issued Appointments)
			Double dblCloseRate = 0;
			Integer intNumberOfSales = 0;
			Integer intNumberOfIssuedAppointments = 0;
            

			// Number of Sales = Total appointments in the last 14 days with "Sale" result that are not "Cancel-Save" or "Repeat Sale"
			intNumberOfSales+= objMetric.mapMetric.get(C_strSalesOrderStatus_SALE).intGeneralCount;
			intNumberOfSales+= objMetric.mapMetric.get(C_strSalesOrderStatus_SALE).intFollowUpCount;

            //Added on 6/6/19 per email from Jessica Thury to include Same Rep Resits into Sales Rep Report / Assignment calculations.
            intNumberOfSales+= objMetric.mapMetric.get(C_strSalesOrderStatus_SALE).intSameRepResitCount;
            //Added on 12/12/19 per Qlik data Confirmed from Lyle and Jessica 
            intNumberOfSales+= objMetric.mapMetric.get(C_strSalesOrderStatus_SALE).intCancelSaveCount;

			// Number of Issued Appointments = Total appointments in the last 14 days that are not "Not Home" Sales Order Status or "Go Back" (Resits)?
			for(String strValue : SalesSchedCalculateSalesRepMetrics.getValuesForPicklist('Sales_Order__c', 'Result__c')) {
				if((strValue != C_strSalesOrderStatus_NOTHOME) ) {
                    //intNumberOfIssuedAppointments+= objMetric.mapMetric.get(strValue).intRepeatSaleCount;
					intNumberOfIssuedAppointments+= objMetric.mapMetric.get(strValue).intGeneralCount;
					intNumberOfIssuedAppointments+= objMetric.mapMetric.get(strValue).intFollowUpCount;
					//intNumberOfIssuedAppointments+= objMetric.mapMetric.get(strValue).intCancelSaveCount;
                    intNumberOfIssuedAppointments+= objMetric.mapMetric.get(strValue).intCancelOrder;
                    //Added on 6/6/19 per email from Jessica Thury to include Same Rep Resits into Sales Rep Report / Assignment calculations.
                    //intNumberOfIssuedAppointments+= objMetric.mapMetric.get(strValue).intSameRepResitCount;
				}
			}
            

			// Final calculation = (Number of Sales) / (Number of Issued Appointments)
			//if(intNumberOfIssuedAppointments > 0) {
			//	dblCloseRate = intNumberOfSales / intNumberOfIssuedAppointments;
			//}
			System.debug('SalesSchedCalculateSalesRepMetrics.CalculateSalesRepMetrics DEBUG:  Final Calculation CloseRate= ' + String.valueOf(intNumberOfSales) + ' / ' + String.valueOf(intNumberOfIssuedAppointments) + ' = ' + dblCloseRate);

            // Push final calcuation to the Rep's Sales_Rep_Reports__r.Close_Rate__c field
			Sales_Rep_Report__c report = mapReports.get(objMetric.idUser);
            //report.Close_Rate__c = dblCloseRate;
            report.Orders__c = intNumberOfSales;
            report.Issued__c = intNumberOfIssuedAppointments;
            //report.Issued__c = intNumberOfIssuedAppointments;
            //mapReports.get(objMetric.idUser).Close_Rate__c = dblCloseRate;

			////////////////////////////////////
			// SS19-97:  Calculate the user's RPA:  Then store the value in Sales_Rep_Reports__r.RPA6__c
			////////////////////////////////////

			// Target calculation = (Total Sales Value) / (Number of Issued Appointments)
			Double dblSalesRep6moRPA = 0;
            Double dblSalesRep28dRPA = 0;
			Double dblTotalSalesValue6mo = objMetric.dbl6moRPASum;
            Double dblTotalSalesValue28d = objMetric.dbl28dRPASum;
            Integer intNumberOfIssuedAppointments6mo = 0;
            Integer intNumberOfIssuedAppointments28d = 0;
            Double dbTotalSalValFinal28d =0;
            Double dbTotalSalValFinal6mo =0;
           
            // total sale value calculation for 28 days
            dbTotalSalValFinal28d+= objMetric.mapMetric.get(C_strSalesOrderStatus_SALE).dblGeneralSum28d;
            dbTotalSalValFinal28d+= objMetric.mapMetric.get(C_strSalesOrderStatus_SALE).dblFollowUpSum28d;
            dbTotalSalValFinal28d+= objMetric.mapMetric.get(C_strSalesOrderStatus_SALE).dblCancelSaveSum28d;
            dbTotalSalValFinal28d+= objMetric.mapMetric.get(C_strSalesOrderStatus_SALE).dblSameRepResitSum28d;
            
            // total sale value calculation for 6 months 
             dbTotalSalValFinal6mo+= objMetric.mapMetric.get(C_strSalesOrderStatus_SALE).dblGeneralSum6mo;
            dbTotalSalValFinal6mo+= objMetric.mapMetric.get(C_strSalesOrderStatus_SALE).dblFollowUpSum6mo;
            dbTotalSalValFinal6mo+= objMetric.mapMetric.get(C_strSalesOrderStatus_SALE).dblCancelSaveSum6mo;
            dbTotalSalValFinal6mo+= objMetric.mapMetric.get(C_strSalesOrderStatus_SALE).dblSameRepResitSum6mo; 
                      
			// Total Sales Value = Sum Total of Sales Value(contract_amount_quoted__c) of all Sales Appointments in the 6 month period
			// Number of Issued Appointments = Total appointments in the last 6 months that are not "Not Home" Sales Order Status or "Go Back" (Resits)?
			for(String strValue : SalesSchedCalculateSalesRepMetrics.getValuesForPicklist('Sales_Order__c', 'Result__c')) {
				if(strValue != C_strSalesOrderStatus_NOTHOME) {
					//intNumberOfIssuedAppointments6mo += objMetric.mapMetric.get(strValue).intRepeatSaleCount6mo;
					intNumberOfIssuedAppointments6mo += objMetric.mapMetric.get(strValue).intGeneralCount6mo;
					intNumberOfIssuedAppointments6mo += objMetric.mapMetric.get(strValue).intFollowUpCount6mo;
					//intNumberOfIssuedAppointments6mo += objMetric.mapMetric.get(strValue).intCancelSaveCount6mo;
                    //intNumberOfIssuedAppointments6mo += objMetric.mapMetric.get(strValue).intSameRepResitCount6mo;
                    intNumberOfIssuedAppointments6mo += objMetric.mapMetric.get(strValue).intCancelOrderCount6mo;
                    
					//intNumberOfIssuedAppointments28d += objMetric.mapMetric.get(strValue).intSameRepResitCount28d;
                    //intNumberOfIssuedAppointments28d += objMetric.mapMetric.get(strValue).intRepeatSaleCount28d;
					intNumberOfIssuedAppointments28d += objMetric.mapMetric.get(strValue).intGeneralCount28d;
					intNumberOfIssuedAppointments28d += objMetric.mapMetric.get(strValue).intFollowUpCount28d;
					//intNumberOfIssuedAppointments28d += objMetric.mapMetric.get(strValue).intCancelSaveCount28d;
					intNumberOfIssuedAppointments28d += objMetric.mapMetric.get(strValue).intCancelOrderCount28d;
				}
			}

           // System.debug('JWL: dblTotalSalesValue: ' + dblTotalSalesValue6mo);
            //System.debug('JWL: intNumberOfIssuedAppointments: ' + intNumberOfIssuedAppointments6mo);


			// Final calculation = (Total Sales Value) / (Number of Issued Appointments)
			if(intNumberOfIssuedAppointments6mo > 0) {
				dblSalesRep6moRPA = dbTotalSalValFinal6mo / intNumberOfIssuedAppointments6mo;
			}

            if(intNumberOfIssuedAppointments28d > 0) {
				dblSalesRep28dRPA = dbTotalSalValFinal28d / intNumberOfIssuedAppointments28d;
			}

			//System.debug('SalesSchedCalculateSalesRepMetrics.CalculateSalesRepMetrics DEBUG:  Final Calculation 6moRPA = ' + String.valueOf(dblSalesRep6moRPA) + ' / ' + String.valueOf(intNumberOfIssuedAppointments6mo) + ' = ' + dblSalesRep6moRPA);

			// Push final calcuation to the Rep's Sales_Rep_Reports__r.RPA6__c field
			//System.debug('JWL: dblSalesRep6moRPA: ' + dblSalesRep6moRPA);
			mapReports.get(objMetric.idUser).RPA6__c = dblSalesRep6moRPA;
			mapReports.get(objMetric.idUser).RPA28__c = dblSalesRep28dRPA;
		}



		// FINAL ACTION:  Upsert the Sales_Rep_Reports__r record.
		//System.debug(JSON.serialize(mapReports));

       // System.debug('JWL: mapReports; ' + mapReports.values());
		if(!mapReports.isEmpty()) {
			upsert mapReports.values();
		}

        //if(runAssignments){
        //    Database.executeBatch(new SalesSchedAssignmentBatch(reportDate),1);
        //}
	}

	////////////////////////////////////////
	// SUPPORT METHODS
	////////////////////////////////////////
	private static String strQueryCloseRate {
		get {
			return 'SELECT ' +
						'Sales_Capacity__r.User__c ' + C_strSalesCapacityField_USER + ',  ' +
						'Sales_Appointment__r.Sales_Order__r.Result__c ' + C_strSalesOrderField_RESULT + ',  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Same_Rep_Resit__c ' + C_strOpportunityField_RESIT + ',  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Repeat_Sale__c ' + C_strOpportunityField_REPEATSALE + ',  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Follow_Up__c ' + C_strOpportunityField_FOLLOWUP + ',  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Cancel_Save__c ' + C_strOpportunityField_CANCELSAVE + ',  ' +
                		'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Owner.Name ' + C_strOpportunityField_OWNERID + ', '+
                		'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Last_DNS_Rep__c ' + C_strOpportunityField_LASTDNSREP + ', '+
                        'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Canceled_Order__c '+ C_strOpportunityField_CANCELORDER + ', '+
						'COUNT(Id) RecCount  ' +
					'FROM Sales_Appointment_Resource__c  ' +
					'WHERE  ' +
						'Sales_Capacity__r.User__c in :userIds  ' +
                		'AND Sales_Appointment__r.Appointment_Date__c >= :appointmentsNDays ' +
                		'AND Sales_Appointment__r.Appointment_Date__c <= :reportDate ' +
						'AND Sales_Appointment__r.Sales_Order__r.Result__c != null  ' +
                        'AND Primary__c = true ' +
					'GROUP BY  ' +
						'Sales_Capacity__r.User__c,  ' +
						'Sales_Appointment__r.Sales_Order__r.Result__c,  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Same_Rep_Resit__c,  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Repeat_Sale__c,  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Follow_Up__c,  ' +
                		'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Owner.Name,  ' +
                		'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Last_DNS_Rep__c,  ' +
                        'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Canceled_Order__c, '+
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Cancel_Save__c';
		}
	}
    
    ////////////////////////////////////////
	// SUPPORT METHODS
	////////////////////////////////////////
	private static String strQueryNDayAppointments {
		get {
			return
                'Select ' +
                	'Sales_Capacity__r.User__c ' +  C_strSalesCapacityField_USER + ',' +
                	'Count(Id) RecCount ' +
                'From ' +
                	'Sales_Appointment_Resource__c ' +
                'Where ' +
                	'Sales_Capacity__r.User__c in :userIds ' +
                	'And Sales_Appointment__r.Appointment_Date_Time__c >= :appointmentsNDays ' +
                	'And Sales_Appointment__r.Appointment_Date__c <= :reportDate ' +
                	'And Sales_Appointment__r.Cancelled__c = false ' +
                    'And Primary__c = true ' +
                'Group By ' +
                	'Sales_Capacity__r.User__c';
       }
	}

	private static String strQueryRPA{
		get {
			return 'SELECT ' +
						'Sales_Capacity__r.User__c ' + C_strSalesCapacityField_USER + ',  ' +
						'Sales_Appointment__r.Sales_Order__r.Result__c ' + C_strSalesOrderField_RESULT + ',  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Same_Rep_Resit__c ' + C_strOpportunityField_RESIT + ',  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Repeat_Sale__c ' + C_strOpportunityField_REPEATSALE + ',  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Follow_Up__c ' + C_strOpportunityField_FOLLOWUP + ',  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Cancel_Save__c ' + C_strOpportunityField_CANCELSAVE + ',  ' +
						'SUM(Sales_Appointment__r.Sales_Order__r.Contract_Amount_Quoted__c) SalesValueSum,  ' +
                		'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Owner.Name ' + C_strOpportunityField_OWNERID + ', '+
                		'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Last_DNS_Rep__c ' + C_strOpportunityField_LASTDNSREP + ', '+
                		'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Canceled_Order__c '+ C_strOpportunityField_CANCELORDER + ', '+
						'COUNT(Id) RecCount  ' +
					'FROM Sales_Appointment_Resource__c  ' +
					'WHERE  ' +
						'Sales_Capacity__r.User__c in :userIds  ' +
						'AND Sales_Appointment__r.Appointment_Date__c >= :appointmentsNDays ' +
                		'AND Sales_Appointment__r.Appointment_Date__c <= :reportDate ' +
                		'AND Sales_Appointment__r.Sales_Order__r.Result__c != null  ' +
                        'AND Primary__c = true ' +
					'GROUP BY  ' +
						'Sales_Capacity__r.User__c,  ' +
						'Sales_Appointment__r.Sales_Order__r.Result__c,  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Same_Rep_Resit__c,  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Repeat_Sale__c,  ' +
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Follow_Up__c,  ' +
                		'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Owner.Name,  ' +
                		'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Last_DNS_Rep__c,  ' +
                		'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Canceled_Order__c, '+
						'Sales_Appointment__r.Sales_Order__r.Opportunity__r.Cancel_Save__c';
		}
	}

	////////////////////////////////////////
	// UTILITY METHODS
	// TODO:  Move these to a main utility class if/when available
	////////////////////////////////////////

	private static List<String> getValuesForPicklist(String objectAPIName, String fieldName) {
        List<String> lstPickvals=new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectAPIName);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(fieldName).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            lstPickvals.add(a.getValue());//add the value  to our final list
        }
        return lstPickvals;
    }

	////////////////////////////////////////
	// INNER CLASSES
	////////////////////////////////////////

	public class UserMetrics {
		public Id idUser {get;set;}
		public Map<String, SalesSchedCalculateSalesRepMetrics.UserMetricResult> mapMetric {get;set;}
		public SalesSchedCalculateSalesRepMetrics.UserMetricResult objSummary {get;set;}
		public Integer intFullAppointmentCount {get;set;}
		public Double dbl6moRPASum {get;set;}
		public Double dbl28dRPASum {get;set;}

		public UserMetrics(Id passIdUser) {
			idUser = passIdUser;
			mapMetric = new Map<String, SalesSchedCalculateSalesRepMetrics.UserMetricResult>();
			objSummary = new SalesSchedCalculateSalesRepMetrics.UserMetricResult();
			intFullAppointmentCount = 0;
			dbl6moRPASum = 0;
            dbl28dRPASum = 0;

			for(String strValue : SalesSchedCalculateSalesRepMetrics.getValuesForPicklist('Sales_Order__c', 'Result__c')) {
				mapMetric.put(strValue, new SalesSchedCalculateSalesRepMetrics.UserMetricResult());
			}
		}

		public void createSummary() {

			intFullAppointmentCount = 0;
			objSummary.intSameRepResitCount = 0;
			objSummary.intRepeatSaleCount = 0;
			objSummary.intFollowUpCount = 0;
			objSummary.intCancelSaveCount = 0;
			objSummary.intGeneralCount = 0;
            objSummary.intCancelOrder =0;
			objSummary.intSameRepResitCount6mo = 0;
			objSummary.intRepeatSaleCount6mo = 0;
			objSummary.intFollowUpCount6mo = 0;
			objSummary.intCancelSaveCount6mo = 0;
			objSummary.intGeneralCount6mo = 0;
            objSummary.intCancelOrderCount6mo =0;
			objSummary.dblSameRepResitSum6mo = 0;
			objSummary.dblRepeatSaleSum6mo = 0;
			objSummary.dblFollowUpSum6mo = 0;
			objSummary.dblCancelSaveSum6mo = 0;
			objSummary.dblGeneralSum6mo = 0;
            objSummary.dblCancelOrderSum6mo =0;
            objSummary.intSameRepResitCount28d = 0;
			objSummary.intRepeatSaleCount28d = 0;
			objSummary.intFollowUpCount28d = 0;
			objSummary.intCancelSaveCount28d = 0;
			objSummary.intGeneralCount28d = 0;
            objSummary.intCancelOrderCount28d = 0;
			objSummary.dblSameRepResitSum28d = 0;
			objSummary.dblRepeatSaleSum28d = 0;
			objSummary.dblFollowUpSum28d = 0;
			objSummary.dblCancelSaveSum28d = 0;
			objSummary.dblGeneralSum28d = 0;
            objSummary.dblCancelOrderSum28d = 0;
			for(SalesSchedCalculateSalesRepMetrics.UserMetricResult objResult : mapMetric.values()) {
				objSummary.intSameRepResitCount+= objResult.intSameRepResitCount;
				objSummary.intRepeatSaleCount+= objResult.intRepeatSaleCount;
				objSummary.intFollowUpCount+= objResult.intFollowUpCount;
				objSummary.intCancelSaveCount+= objResult.intCancelSaveCount;
				objSummary.intGeneralCount+= objResult.intGeneralCount;
                objSummary.intCancelOrder+= objResult.intCancelOrder;
				objSummary.intSameRepResitCount6mo+= objResult.intSameRepResitCount6mo;
				objSummary.intRepeatSaleCount6mo+= objResult.intRepeatSaleCount6mo;
				objSummary.intFollowUpCount6mo+= objResult.intFollowUpCount6mo;
				objSummary.intCancelSaveCount6mo+= objResult.intCancelSaveCount6mo;
				objSummary.intGeneralCount6mo+= objResult.intGeneralCount6mo;
                objSummary.intCancelOrderCount6mo+= objResult.intCancelOrderCount6mo;
				objSummary.dblSameRepResitSum6mo+= objResult.dblSameRepResitSum6mo;
				objSummary.dblRepeatSaleSum6mo+= objResult.dblRepeatSaleSum6mo;
				objSummary.dblFollowUpSum6mo+= objResult.dblFollowUpSum6mo;
				objSummary.dblCancelSaveSum6mo+= objResult.dblCancelSaveSum6mo;
				objSummary.dblGeneralSum6mo+= objResult.dblGeneralSum6mo;
                objSummary.dblCancelOrderSum6mo+= objResult.dblCancelOrderSum6mo;
                objSummary.intSameRepResitCount28d+= objResult.intSameRepResitCount28d;
				objSummary.intRepeatSaleCount28d+= objResult.intRepeatSaleCount28d;
				objSummary.intFollowUpCount28d+= objResult.intFollowUpCount28d;
				objSummary.intCancelSaveCount28d+= objResult.intCancelSaveCount28d;
				objSummary.intGeneralCount28d+= objResult.intGeneralCount28d;
                objSummary.intCancelOrderCount28d+= objResult.intCancelOrderCount28d;
				objSummary.dblSameRepResitSum28d+= objResult.dblSameRepResitSum28d;
				objSummary.dblRepeatSaleSum28d+= objResult.dblRepeatSaleSum28d;
				objSummary.dblFollowUpSum28d+= objResult.dblFollowUpSum28d;
				objSummary.dblCancelSaveSum28d+= objResult.dblCancelSaveSum28d;
				objSummary.dblGeneralSum28d+= objResult.dblGeneralSum28d;
                objSummary.dblCancelOrderSum28d+= objResult.dblCancelOrderSum28d;
			 
            }
			intFullAppointmentCount+= objSummary.intSameRepResitCount;
			intFullAppointmentCount+= objSummary.intRepeatSaleCount;
			intFullAppointmentCount+= objSummary.intFollowUpCount;
			intFullAppointmentCount+= objSummary.intCancelSaveCount;
			intFullAppointmentCount+= objSummary.intGeneralCount;

			dbl6moRPASum+=objSummary.dblSameRepResitSum6mo;
			dbl6moRPASum+=objSummary.dblRepeatSaleSum6mo;
			dbl6moRPASum+=objSummary.dblFollowUpSum6mo;
			dbl6moRPASum+=objSummary.dblCancelSaveSum6mo;
			dbl6moRPASum+=objSummary.dblGeneralSum6mo;

            dbl28dRPASum+=objSummary.dblSameRepResitSum28d;
			dbl28dRPASum+=objSummary.dblRepeatSaleSum28d;
			dbl28dRPASum+=objSummary.dblFollowUpSum28d;
			dbl28dRPASum+=objSummary.dblCancelSaveSum28d;
			dbl28dRPASum+=objSummary.dblGeneralSum28d;

		}

	}

	public class UserMetricResult {
		public Integer intSameRepResitCount {get;set;}
		public Integer intRepeatSaleCount {get;set;}
		public Integer intFollowUpCount {get;set;}
		public Integer intCancelSaveCount {get;set;}
		public Integer intGeneralCount {get;set;}
        public Integer intCancelOrder {get;set;}
		public Integer intSameRepResitCount6mo {get;set;}
		public Integer intRepeatSaleCount6mo {get;set;}
		public Integer intFollowUpCount6mo {get;set;}
		public Integer intCancelSaveCount6mo {get;set;}
		public Integer intGeneralCount6mo {get;set;}
        public Integer intCancelOrderCount6mo {get;set;}
		public Double dblSameRepResitSum6mo {get;set;}
		public Double dblRepeatSaleSum6mo {get;set;}
		public Double dblFollowUpSum6mo {get;set;}
		public Double dblCancelSaveSum6mo {get;set;}
		public Double dblGeneralSum6mo {get;set;}
        public Double dblCancelOrderSum6mo {get;set;}
       	public Integer intSameRepResitCount28d {get;set;}
		public Integer intRepeatSaleCount28d {get;set;}
		public Integer intFollowUpCount28d {get;set;}
		public Integer intCancelSaveCount28d {get;set;}
		public Integer intGeneralCount28d {get;set;}
        public Integer intCancelOrderCount28d {get;set;}
		public Double dblSameRepResitSum28d {get;set;}
		public Double dblRepeatSaleSum28d {get;set;}
		public Double dblFollowUpSum28d {get;set;}
		public Double dblCancelSaveSum28d {get;set;}
		public Double dblGeneralSum28d {get;set;}
        public Double dblCancelOrderSum28d {get;set;}

		public UserMetricResult() {
			intSameRepResitCount = 0;
			intRepeatSaleCount = 0;
			intFollowUpCount = 0;
			intCancelSaveCount = 0;
			intGeneralCount = 0;
            intCancelOrder = 0;
			intSameRepResitCount6mo = 0;
			intRepeatSaleCount6mo = 0;
			intFollowUpCount6mo = 0;
            intCancelSaveCount6mo = 0;
			intGeneralCount6mo = 0;
            intCancelOrderCount6mo = 0;
			dblSameRepResitSum6mo = 0;
			dblRepeatSaleSum6mo = 0;
			dblFollowUpSum6mo = 0;
			dblCancelSaveSum6mo = 0;
			dblGeneralSum6mo = 0;
            dblCancelOrderSum6mo = 0;
            intSameRepResitCount28d = 0;
			intRepeatSaleCount28d = 0;
			intFollowUpCount28d = 0;
			intCancelSaveCount28d = 0;
			intGeneralCount28d = 0;
            intCancelOrderCount28d = 0; 
			dblSameRepResitSum28d = 0;
			dblRepeatSaleSum28d = 0;
			dblFollowUpSum28d = 0;
			dblCancelSaveSum28d = 0;
			dblGeneralSum28d = 0;
            dblCancelOrderSum28d = 0;
		}
	}
}