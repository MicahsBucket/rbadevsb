/*
* @author Jason Flippen
* @date 09/15/2020 
* @description Class to provide functionality for the orderPurchaseOrderList LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - OrderPurchaseOrderListControllerTest
*/
public with sharing class OrderPurchaseOrderListController {

    /*
    * @author Jason Flippen
    * @date 01/11/2021
    * @description Method to return the Default Store Location Id from the Order.
    */
    @AuraEnabled(cacheable=true)
    public static String getDefaultStoreLocationId(Id orderId) {

        String storeLocationId = '';

/*
        User currentUser = [SELECT Id,
                                   Default_Store_Location__c
                            FROM   User
                            WHERE  Id = :UserInfo.getUserId()];
        
        String storeLocationName = currentUser.Default_Store_Location__c;

        Account storeLocation = null;
        if (String.isNotBlank(storeLocationName)) {
            storeLocation = [SELECT Id
                             FROM   Account
                             WHERE  Name = :storeLocationName];
            
            storeLocationId = storeLocation.Id;
        }
*/
        Order order = [SELECT Store_Location__c
                       FROM   Order
                       WHERE  Id = :orderId];
        
        Id orderStoreLocationId = order.Store_Location__c;
        if (String.isNotBlank(orderStoreLocationId)) {
            storeLocationId = orderStoreLocationId;
        }

        return storeLocationId;

    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to return a wrapped Purchase Order data
    */
    @AuraEnabled(cacheable=true)
    public static List<PurchaseOrderWrapper> getPurchaseOrderData(Id orderId, String recordPageUrl) {

        List<PurchaseOrderWrapper> wrapperList = new List<PurchaseOrderWrapper>();

        // Are we in a Community?
        Boolean sourceIsCommunity = true;
        String networkId = Network.getNetworkId();
        if (networkId == null) {
            sourceIsCommunity = false;
        }

        for (Purchase_Order__c po : [SELECT Id,
                                            Name,
                                            RecordTypeId,
                                            RecordType.Name,
                                            Status__c,
                                            Estimated_Ship_Date__c,
                                            Total__c 
                                     FROM   Purchase_Order__c
                                     WHERE  Order__c = :orderId]) {
            
            PurchaseOrderWrapper wrapper = new PurchaseOrderWrapper();
            wrapper.id = po.Id;
            wrapper.name = po.Name;
            String recordUrl = recordPageUrl + po.Id;
            if (sourceIsCommunity == false) {
                recordUrl += '/view';
            }
            wrapper.purchaseOrderUrl = recordUrl;
            wrapper.recordTypeId = po.RecordTypeId;
            wrapper.recordTypeName = po.RecordType.Name;
            wrapper.status = po.Status__c;
            wrapper.estimatedShipDate = po.Estimated_Ship_Date__c;
            wrapper.amount = po.Total__c;
            wrapperList.add(wrapper);
        }
        
        return wrapperList;

    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to return (wrapped) List of Vendors.
    */
    @AuraEnabled
    public static List<VendorWrapper> getVendorList(Id orderId) {
        
        List<VendorWrapper> wrapperList = new List<VendorWrapper>();

        // Grab some detail from the Order (and its related Products).
        Order order = [SELECT Id,
                              Store_Location__c,
                              (
                                  SELECT Id,
                                         Product2Id,
                                         Product2.Name,
                                         Description,
                                         Quantity,
                                         Unit_Id__c,
                                         Unit_Wholesale_Cost__c,
                                         Variant_Number__c
                                  FROM   OrderItems
                                  WHERE  Product2.IsActive = true
                                  AND    Purchase_Order__c = null
                                  AND    Status__c != 'Cancelled'
                              )
                       FROM   Order
                       WHERE  Id = :orderId];


        if (!order.OrderItems.isEmpty() && String.isNotBlank(order.Store_Location__c)) { 
        
            // Build Maps and Sets to create Vendor Wrapper records.
            Map<Id,List<OrderItem>> productOrderItemListMap = getProductOrderItemListMap(order);
            Map<Id,String> eligibleVendorMap = getEligibleVendorMap(order.Store_Location__c);
            Map<Id,Set<Id>> vendorProductSetMap = getVendorProductSetMap(order, eligibleVendorMap.keySet());

            if (!vendorProductSetMap.isEmpty()) {
                
                for (Id vendorId : vendorProductSetMap.keySet()) {

                    String vendorName = eligibleVendorMap.get(vendorId);

                    VendorWrapper vendorWrapper = new VendorWrapper();
                    vendorWrapper.label = vendorName;
                    vendorWrapper.value = vendorId;
                    wrapperList.add(vendorWrapper);

                    // Iterate through the Products related to this Vendor and add
                    // them to the "orderItemList" property of the VendorWrapper.
                    for (Id productId : vendorProductSetMap.get(vendorId)) {

                        // Make sure we're able to add this Product to a new PO on this Order.
                        if (productOrderItemListMap.containsKey(productId)) {

                            for (OrderItem orderItem : productOrderItemListMap.get(productId)) {

                                // Add the OrderItems to the VendorWrapper.
                                OrderItemWrapper orderItemWrapper = new OrderItemWrapper();
                                orderItemWrapper.id = orderItem.Id;
                                orderItemWrapper.productId = orderItem.Product2Id;
                                orderItemWrapper.productName = orderItem.Product2.Name;
                                orderItemWrapper.productDescription = orderItem.Description;
                                orderItemWrapper.quantity = orderItem.Quantity;
                                orderItemWrapper.unitId = orderItem.Unit_Id__c;
                                orderItemWrapper.unitWholesaleCost = orderItem.Unit_Wholesale_Cost__c;
                                orderItemWrapper.variantNumber = orderItem.Variant_Number__c;
                                vendorWrapper.orderItemList.add(orderItemWrapper);

                            }

                        }

                    }

                }

            }

        }
        System.debug('***** wrapperList: ' + wrapperList);

        return wrapperList;

    }

    /*
    * @author Jason Flippen
    * @date 03/05/2021
    * @description Method to return a Product-to--OrderItem Map.
    */
    private static Map<Id,List<OrderItem>> getProductOrderItemListMap(Order order) {

        Map<Id,List<OrderItem>> productOrderItemListMap = new Map<Id,List<OrderItem>>();

        // Build the Product-to-OrderItem Map.
        for (OrderItem oi : order.OrderItems) {
            if (!productOrderItemListMap.containsKey(oi.Product2Id)) {
                productOrderItemListMap.put(oi.Product2Id, new List<OrderItem>());
            }
            productOrderItemListMap.get(oi.Product2Id).add(oi);
        }

        return productOrderItemListMap;

    }

    /*
    * @author Jason Flippen
    * @date 03/05/2021
    * @description Method to return a Map of Vendors eligible for Service POs.
    */
    private static Map<Id,String> getEligibleVendorMap(Id storeLocationId) {

        Map<Id,String> eligibleVendorMap = new Map<Id,String>();

        // Add Vendors to our Eligible Vendor Set if
        // available the Store the Order is connected to.
        for (Store_Vendor__c storeVendor : [SELECT Store__c,
                                                   Vendor__c,
                                                   Vendor__r.Name
                                            FROM   Store_Vendor__c
                                            WHERE  Store__c = :storeLocationId
                                            AND    Vendor__c != null
                                            AND    Vendor__r.Active__c = true]) {
            eligibleVendorMap.put(storeVendor.Vendor__c, storeVendor.Vendor__r.Name);
        }

        return eligibleVendorMap;

    }

    /*
    * @author Jason Flippen
    * @date 03/05/2021
    * @description Method to return a Vendor-to-Product Map.
    */
    private static Map<Id,Set<Id>> getVendorProductSetMap(Order order, Set<Id> eligibleVendorIdSet) {

        Map<Id,Set<Id>> vendorProductSetMap = new Map<Id,Set<Id>>();

        // Grab a Set of Product Ids related to the Order.
        Set<Id> productIdSet = new Set<Id>();
        for (OrderItem oi : order.OrderItems) {
            productIdSet.add(oi.Product2Id);
        }
        
        // Build a Map of unique Vendors related to the Products.
        for (Vendor_Product__c vendorProduct : [SELECT Product__c,
                                                       Vendor__c
                                                FROM   Vendor_Product__c
                                                WHERE  Vendor__c IN :eligibleVendorIdSet
                                                AND    Product__c IN :productIdSet
                                                AND    Product__r.Product_PO__c = true]) {
            
            if (!vendorProductSetMap.containsKey(vendorProduct.Vendor__c)) {
                vendorProductSetMap.put(vendorProduct.Vendor__c, new Set<Id>());
            }
            vendorProductSetMap.get(vendorProduct.Vendor__c).add(vendorProduct.Product__c);

        }
        
        return vendorProductSetMap;

    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to create the new Purchase Order.
    */
    @AuraEnabled
    public static Map<String,String> createPO(List<OrderItemWrapper> wrapperList, Id orderId, Purchase_Order__c newPurchaseOrder, String vendorName) {

        Map<String,String> resultMap = new Map<String,String>();

        newPurchaseOrder.Name = 'PlaceholderPOName';
        newPurchaseOrder.Comments__c = null;
        newPurchaseOrder.Date__c = Date.today();
        newPurchaseOrder.Order__c = orderId;
        Id purchaseOrderRTId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Product_Purchase_Order').getRecordTypeId();
        if (vendorName.contains('Renewal by Andersen')) {
            purchaseOrderRTId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('RbA_Purchase_Order').getRecordTypeId();
        }
        newPurchaseOrder.RecordTypeId = purchaseOrderRTId;
        newPurchaseOrder.Status__c = 'In Process';
        newPurchaseOrder.Store_Abbreviation__c = getStoreAbbreviation(orderId);
        newPurchaseOrder.Tax__c = 0;

        // Create a savepoint prior to making any changes so
        // we can roll them back if we encounter an error.
        Savepoint savePoint = Database.setSavepoint();

        try {

            insert newPurchaseOrder;

            List<OrderItem> updateOrderItemList = new List<OrderItem>();
            for (OrderItemWrapper wrapper : wrapperList) {
                OrderItem updateOrderItem = new OrderItem(Id = wrapper.id,
                                                          Purchase_Order__c = newPurchaseOrder.Id,
                                                          Has_PO__c = true);
                updateOrderItemList.add(updateOrderItem);
            }

            if (!updateOrderItemList.isEmpty()) {
                update updateOrderItemList;
            }

            resultMap.put('New PO Success', newPurchaseOrder.Id);

        }
        catch (Exception ex) {

            System.debug('***** The following exception occurred in the OrderPurchaseOrderListController in the createPO method:' + ex);
            // Rollback the changes and return the error message.
            Database.rollback(savePoint);
            resultMap.put(ex.getMessage(), '');
        }

        return resultMap;

    }

    /*
    * @author Jason Flippen
    * @date 03/02/2021
    * @description Method to retrieve a concatenated Store Abbreviation.
    */
    private static String getStoreAbbreviation(Id orderId) {
        
        String returnValue = '';

        // Retrive the current user
        User currentUser = [SELECT User_Abbreviation__c FROM User WHERE Id =: UserInfo.getUserId()];
        
        // If a test is running just set the user's abbreviation, otherwise get it from the current user.
        String userAbbreviation = 'X';
        if (Test.isRunningTest() == true) {
            userAbbreviation = 'A';
        }
        else if (String.isNotBlank(currentUser.User_Abbreviation__c)) {
            userAbbreviation = currentUser.User_Abbreviation__c;
        }

        // Set the type of PO
        String typeAbbreviation = 'P';
        
        // Query the Order to retrieve the Store Location (Id).
        Map<Id,Order> orderMap = new Map<Id,Order>([SELECT Id,
                                                           Store_Location__c
                                                    FROM   Order
                                                    WHERE  Id = :orderId]);

        String storeAbbreviation = 'X';
        if (!orderMap.isEmpty()) {

            Order order = orderMap.values()[0];

            // Query the Store Location (Account) to retrieve the store abbreviation.
            Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id,
                                                                     Active_Store_Configuration__c,
                                                                     Active_Store_Configuration__r.Store_Abbreviation__c
                                                              FROM   Account
                                                              WHERE  Id = :order.Store_Location__c
                                                              AND    Active_Store_Configuration__c != null]);
            
            if (!accountMap.isEmpty()) {

                // Update the store abbreviation to the value from the Store Location (Account).
                storeAbbreviation = '';
                Account account = accountMap.values()[0];
                if (account.Active_Store_Configuration__r.Store_Abbreviation__c != null) {
                    storeAbbreviation = account.Active_Store_Configuration__r.Store_Abbreviation__c;
                }

            }

        }

        // Set the return value to the store abbreviation, user abbreviation, and type abbreviation (concatenated).
        returnValue = storeAbbreviation + userAbbreviation + typeAbbreviation;

        return returnValue;

    }


/** Wrapper Classes **/


    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Wrapper Class for a Purchase Order.
    */
    @TestVisible
    public class PurchaseOrderWrapper {

        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String name {get;set;}

        @AuraEnabled
        public String purchaseOrderUrl {get;set;}

        @AuraEnabled
        public String recordTypeId {get;set;}

        @AuraEnabled
        public String recordTypeName {get;set;}

        @AuraEnabled
        public String status {get;set;}

        @AuraEnabled
        public Date estimatedShipDate {get;set;}

        @AuraEnabled
        public Decimal amount {get;set;}

    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Wrapper Class for Vendors to be used in a Combobox.
    */
    @TestVisible
    public class VendorWrapper {

        @AuraEnabled
        public String label {get;set;}

        @AuraEnabled
        public String value {get;set;}

        @AuraEnabled
        public List<OrderItemWrapper> orderItemList {get;set;}

        public VendorWrapper() {
            this.orderItemList = new List<OrderItemWrapper>();
        }

    }

     /*
    * @author Jason Flippen
    * @date 09/16/2020
    * @description Wrapper Class for Order Items.
    */
    @TestVisible
    public class OrderItemWrapper {

        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String productId {get;set;}

        @AuraEnabled
        public String productName {get;set;}

        @AuraEnabled
        public String productDescription {get;set;}

        @AuraEnabled
        public Decimal quantity {get;set;}

        @AuraEnabled
        public String unitId {get;set;}

        @AuraEnabled
        public Decimal unitWholesaleCost {get;set;}

        @AuraEnabled
        public String variantNumber {get;set;}

    }

}