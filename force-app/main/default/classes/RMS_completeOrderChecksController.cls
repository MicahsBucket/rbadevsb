/*******************************************************//**
@class  RMS_completeOrderChecksController

@brief  Controller for RMS_completeOrderChecks visualforce page

@author  Slalom

@version    2016-2-18  Slalom.CDK
    Created.

@copyright  (c)2016 Slalom.  All Rights Reserved. 
            Unauthorized use is prohibited.

***********************************************************/
public with sharing class RMS_completeOrderChecksController {

    public list<Back_Office_Check__c> selectedChecks {get;set;}
    public Id ordId{get;set;}
    
    public RMS_completeOrderChecksController(ApexPages.StandardSetController controller) {
        
        ordId = ApexPages.currentPage().getParameters().get('Id');
        Order order = [SELECT Id, EffectiveDate FROM Order WHERE Id =: ordId];
        List<Back_Office_Check__c> relatedBOC = [SELECT Id, Name, Completed__c, Signed_Date__c, Order__c FROM Back_Office_Check__c WHERE Order__c = :ordId ];
            for(Back_Office_Check__c i : relatedBOC){
                if(i.Signed_Date__c == null){
                        i.Signed_Date__c = Order.EffectiveDate;
                    }
            }

        selectedChecks = relatedBOC;

    }
    
    public pageReference save(){
        try{
        Map<ID, Order> newOrderforUnique = new Map<ID, Order>();
        //RMS_WorkOrderCreationManager.updatingBackOfcRecs = true;
        update selectedChecks;
        Boolean allChecksCompleted = true;
        for(Back_Office_Check__c check : selectedChecks){
            if(!check.Completed__c){
                allChecksCompleted = false;
                break;
            }
        }

        if(allChecksCompleted){
            Order order = [SELECT Id, Status,RecordTypeId,Unique_Identifier__c, Tech_Measure_Status__c, Order_Processed_Date__c, Apex_Context__c FROM Order WHERE Id =: ordId];
            list<Workorder> WOList = new list<Workorder>([Select id, status , Sold_Order__c from Workorder Where Sold_Order__c =: ordId order by Lastmodifieddate ASC]);
            if(order.Order_Processed_Date__c == null){
                 order.Order_Processed_Date__c = System.Today();   
                }
            // if(order.Status == 'Draft'){
            //     // Allows the order status to be changed
            //     order.Apex_Context__c = true;                
                
            //     if(!WOList.isEmpty()){
            //         for(workorder rec : WOList){
            //             system.debug('@pav before Start'+order.status);
            //             if(rec.status == 'Appt Complete / Closed' && order.Status != 'Tech Measure Scheduled'){
            //                 order.Status = 'Ready to Order';
            //                 system.debug('@pav before App / closed'+order.status);
            //             }else  if(rec.Status == 'Scheduled & Assigned'){
            //                 order.Status = 'Tech Measure Scheduled';
            //                 system.debug('@pav before TMS'+order.status);
                            
            //             }else if (order.Status != 'Tech Measure Scheduled' && rec.Status =='To be Scheduled' && order.Status != 'Ready to Order') {
            //                 order.Status = 'Tech Measure Needed';
            //                 system.debug('@pav before TMN'+order.status); 

            //             }
            //         }
            //     } 
            // }
                    //RMS_WorkOrderCreationManager.updatingBackOfcRecs = false;
                    update order;
                 newOrderforUnique.put(order.id,order);
                 OrderActions.updateUniqueIdentifier(null, newOrderforUnique,  True);
                //  System.enqueueJob(new StatusCheckQue(newOrderforUnique, null, 'TechMeasureOrderUpdate'));
                // if(order.Record)
                StatusCheckUtility.techMeasureStatusOnOrderUpdate(newOrderforUnique, null);
            } 
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Some Back Office Checks could not be saved. Please check Error Messages above.'));
        }
        //JETT-2143 Removed page redirect from controller, tab closing/page redirection is handled by vf page instead.
        return null;
    }
}