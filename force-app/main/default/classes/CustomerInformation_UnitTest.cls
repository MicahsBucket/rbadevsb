@isTest
public class CustomerInformation_UnitTest 
{
	static testmethod void createTestRecords() {      
        RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name = 'Turn Financial Transactions Off', Value__c = 'Yes');
        insert turnOffFinancialTrigger;
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        List<Account> dwellingsToInsert = new List<Account>();               
        Account dwelling1 = utility.createDwellingAccount('Dwelling Account');
        Opportunity testOpp=utility.createOpportunity('Testing Opportunity',dwelling1.id,'New',UserInfo.getUserId(),System.today());
        insert testOpp;
        Opportunity testOpp2=utility.createOpportunity('Testing Opportunity',dwelling1.id,'New',UserInfo.getUserId(),System.today());
        insert testOpp2;
        Account store1 = [SELECT Id,Active_Store_Configuration__c from Account Where Name = '77 - Twin Cities, MN'];
        System.debug('_____store1______'+store1);        
        Store_Configuration__c storeConfig1 = [SELECT id,Invitation_Delay_Days__c,Activate_Community__c FROM Store_Configuration__c WHERE Store__c = :store1.id ];
        storeConfig1.Sales_Tax__c = 5;
        storeConfig1.Activate_Community__c=true;
        storeConfig1.Invitation_Delay_Days__c=0;
        storeConfig1.Portal_Activation_Date__c=System.today()-1;
        storeConfig1.Remit_to_Company_Name__c='TestLocation';
        System.debug('______storeConfig1______'+storeConfig1);
        update storeConfig1;
        dwelling1.Store_Location__c = store1.Id;
        dwellingsToInsert.add(dwelling1);
        insert dwellingsToInsert;        
        
        ID contactRType=[select id from RecordType where SobjectType='Contact' and name='Customer'].id;
        ID contactCustomerRType=[select id from RecordType where SobjectType='Contact' and name='Customer Contacts'].id;
        List<Contact> contactsToInsert = new List<Contact>();       
        
        Contact contact1 = new contact (Email='RatnaTest@Ratnatest.com', FirstName='Contact',LastName='1',AccountId=dwelling1.id, Primary_Contact__c =true,RecordTypeId=contactRType);
        contactsToInsert.add(contact1);
        
        Contact contact2 = new contact (Email='RatnaTest2@Ratnatest.com', FirstName='Contact',LastName='2',AccountId=dwelling1.id, Primary_Contact__c =true,RecordTypeId=contactRType);
        contactsToInsert.add(contact2);
        
        Contact contact3 = new contact (Email='RatnaTest@Ratnatest.com', FirstName='Contact',LastName='3',AccountId=dwelling1.id, Primary_Contact__c =false,RecordTypeId=contactRType);
        contactsToInsert.add(contact3);
        insert contactsToInsert;
        
        List<OpportunityContactRole> oppRoles=new List<OpportunityContactRole>();
        OpportunityContactRole role1=new OpportunityContactRole(contactId=contact1.id,OpportunityId=testOpp.Id,Role='Decision Maker',isPrimary=true);
        OpportunityContactRole role2=new OpportunityContactRole(contactId=contact2.id,OpportunityId=testOpp.Id,Role='Decision Maker');
        OpportunityContactRole role3=new OpportunityContactRole(contactId=contact3.id,OpportunityId=testOpp2.Id,Role='Decision Maker',isPrimary=true);
        OpportunityContactRole role4=new OpportunityContactRole(contactId=contact2.id,OpportunityId=testOpp2.Id,Role='Decision Maker');
        oppRoles=new List<OpportunityContactRole>{role1,role2,role3,role4};
        insert oppRoles;
        
        Product2 masterProduct = new Product2( name = 'master',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Master_Product'));
        Product2 servProduct = new Product2( name = 'service',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'));
        Product2 servProduct2 = new Product2(name = 'service2',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'));
        
        Id pricebookId = Test.getStandardPricebookId();
        system.debug(pricebookId);
        
        Pricebook2 pricebook1 =  utility.createPricebook2Name('Standard Price Book');
        insert pricebook1;
        
        List<Product2> products = new List<Product2>{masterProduct,servProduct,servProduct2};
            insert products;
        
        PricebookEntry pricebookEntryMaster= utility.createPricebookEntry(pricebookId, masterProduct.id);     
        PricebookEntry pricebookEntryServ = utility.createPricebookEntry(pricebookId, servProduct.id);    
        PricebookEntry pricebookEntryServ2 = utility.createPricebookEntry(pricebookId, servProduct2.id);      
        
        List<PriceBookEntry> pbEntries = new List<PricebookEntry>{pricebookEntryMaster,pricebookEntryServ,pricebookEntryServ2};
            insert pbEntries;
            
        List<Order> ordersToInsert = new List<Order>();
        ID orderRType=[select id from RecordType where SobjectType='Order' and name='CORO Record Type'].id;
        Order order =  new Order(   Name='Sold Order 1', 
                                 AccountId = dwelling1.id, 
                                 Primary_contact__c=contact1.id,
                                 Secondary_contact__c=contact2.id,
                                 BilltoContactId = contact1.id,
                                 OpportunityId=testOpp.id,
                                 EffectiveDate= Date.Today(), 
                                 Store_Location__c = store1.Id,                           
                                 Status ='Install Complete', 
                                 Pricebook2Id = Test.getStandardPricebookId(),
                                 Customer_Pickup_All__c = FALSE,
                                 Installation_Date__c = system.today()-1,
                                 CustomerPortalUser__c=userinfo.getUserId(),
                                 RecordTypeId=orderRType
                                );
        ordersToInsert.add(order);
        Order order2 =  new Order(   Name='Sold Order 1', 
                                 AccountId = dwelling1.id, 
                                 BilltoContactId = contact3.id,
                                 Primary_contact__c=contact3.id,
                                 Secondary_contact__c=contact2.id, 
                                 OpportunityId=testOpp2.id,
                                 EffectiveDate= Date.Today(), 
                                 Store_Location__c = store1.Id,                           
                                 Status ='Install Complete', 
                                 Pricebook2Id = Test.getStandardPricebookId(),
                                 Customer_Pickup_All__c = FALSE,
                                 Installation_Date__c = system.today()-1,
                                 CustomerPortalUser__c=userinfo.getUserId(),
                                 RecordTypeId=orderRType
                                );
        ordersToInsert.add(order2); 
        insert ordersToInsert;
        
        
        OrderItem orderItemMaster = new OrderItem(OrderId = Order.id, PricebookentryId = pricebookEntryMaster.Id, Quantity = 2, UnitPrice = 100,Quote_Accepted__c = FALSE );
        insert orderItemMaster;
        
        OrderItem orderItemMaster2 = new OrderItem(OrderId = Order2.id, PricebookentryId = pricebookEntryMaster.Id, Quantity = 2, UnitPrice = 100,Quote_Accepted__c = FALSE );
        insert orderItemMaster2;
        
        test.startTest();
        CustomerInformationController.getContactInfo(false);
        CustomerInformationController.updateContact(contact1);
        CustomerInformationController.createNewContact('test', 'last', 'test@test.com', '1234567890', '1234567890', 'test', 'Cottage Grove', 'Minnesota', 'United States', '123456');
        CustomerInformationController.getCurrentUserId();
        CustomerInformationController.getContactPrimaryInfo(true,order2.Id);
        CustomerInformationController.getSecondaryContactInfo(false,order.Id);
        test.stopTest();
        
    }
}