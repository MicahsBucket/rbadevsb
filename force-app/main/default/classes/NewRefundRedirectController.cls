public with sharing class NewRefundRedirectController {
    @AuraEnabled
    public static Order getRecordData(String recordId, String objectName) {
        System.debug('recordId: '+ recordId);
        System.debug('objectName: '+ objectName);
        Order o = [SELECT Id, AccountId, BillToContactId, Store_Location__c FROM Order WHERE Id =: recordId];
        return o;
    }
}