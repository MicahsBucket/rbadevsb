@isTest
private class BatchEnabledPAS_UnitTest {
	
	@testSetup
	static void setup(){
		TestDataFactoryStatic.setUpConfigs();
		Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
		Id storeAccountId = storeAccount.Id;
		Id storeConfigId = storeAccount.Active_Store_Configuration__c;
		Id accountId = TestDataFactoryStatic.createAccount('Calvins', storeAccountId).Id;
		Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'CalvinPrime');
		Contact secContact = TestDataFactoryStatic.createContact(accountId, 'CalvinSec');
		Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('Calvins Opportunity', accountId, storeAccountId, 'Sold', date.today());
		OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
		OpportunityContactRole oppContJunc2 = TestDataFactoryStatic.createOppCon('Decision Maker', false, secContact, opportunity);
	}

	@isTest static void createContactJuncTest() {
		Account account1 = TestDataFactory.createStoreAccount('Calvins Store');
		Database.insert(account1, true);
		Id accountId = account1.Id;
		Survey__c newSurvey = TestDataFactory.createSurveys('Post Appointment', 1, accountId, 'Pending')[0];
		newSurvey.Source_System__c = 'EnabledPlus';
		Database.update(newSurvey, true);
		Test.startTest();
			BatchEnabledPostAppointmentSurveys obj = new BatchEnabledPostAppointmentSurveys();
			Database.executeBatch(obj);
			BatchEnabledPostAppointmentSurveys obj2 = new BatchEnabledPostAppointmentSurveys();
			Database.executeBatch(obj2);
		Test.stopTest();
		Id primContId = [SELECT Id, FirstName FROM Contact WHERE FirstName = 'CalvinPrimeTest' LIMIT 1][0].Id;
		Id secContId = [SELECT Id, FirstName FROM Contact WHERE FirstName = 'CalvinSecTest' LIMIT 1][0].Id;
		Integer numOfJunctions = [SELECT Id FROM Contact_Survey__c].size();
		Contact_Survey__c primContJunc = [SELECT Id, Contact_Id__c, Survey_Id__c, Type__c FROM Contact_Survey__c WHERE Type__c = 'Primary' LIMIT 1];
		Contact_Survey__c secContJunc = [SELECT Id, Contact_Id__c, Survey_Id__c, Type__c FROM Contact_Survey__c WHERE Type__c = 'Secondary' LIMIT 1];
		system.assertEquals(newSurvey.Id, primContJunc.Survey_Id__c);
		system.assertEquals(primContId, primContJunc.Contact_Id__c);
		system.assertEquals('Primary', primContJunc.Type__c);
		system.assertEquals(2, numOfJunctions);

		system.assertEquals(newSurvey.Id, secContJunc.Survey_Id__c);
		system.assertEquals(secContId, secContJunc.Contact_Id__c);
		system.assertEquals('Secondary', secContJunc.Type__c);
	}
	
}