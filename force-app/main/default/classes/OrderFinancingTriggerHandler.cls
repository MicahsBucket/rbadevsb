public with sharing class OrderFinancingTriggerHandler{

  @future
  public static void updateFinanceExpirationDateonOrder(set<Id> recordIds ){

  List<Order_Financing__c> relatedIds = [SELECT Id, Related_Order__c FROM Order_Financing__c WHERE Id IN:recordIds];
  set<Id> ordIds = new Set<Id>(); 
  List<Order> ord = new List<Order>();
  List<AggregateResult> requests = new List<AggregateResult>();
  for( Order_Financing__c OrdFin: relatedIds ){    
        ordIds.add(OrdFin.Related_Order__c); 
    }
   ord = [SELECT Id, Order_Financing_Expiration_Date__c FROM Order WHERE Id IN:ordIds ];    
    requests = [SELECT Related_Order__c, Min(Expiration_Date__c) LatestFinaExPDT FROM Order_Financing__c WHERE Related_Order__c IN:ordIds  GROUP BY Related_Order__c ];
    for(Order o: ord){
        Date Lastdate;        
        for (AggregateResult b : requests){         
          if (b.get('Related_Order__c')==o.Id){ 
                if(b.get('LatestFinaExPDT') != NULL){
                    Lastdate = (Date)b.get('LatestFinaExPDT');
                    system.debug('@@@@'+Lastdate );
                }                 
            }
        } 
        o.Order_Financing_Expiration_Date__c = Lastdate;
    }
    update (ord); 
    }
  }