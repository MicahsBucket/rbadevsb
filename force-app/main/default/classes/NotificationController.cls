public  class NotificationController {
    @AuraEnabled public static ServiceNotes__c Notification{get;set;}
    @AuraEnabled public   static  boolean recordUpdateVisibility{get;set;} 
    
    @AuraEnabled(cacheable=true)
    public static   ServiceNotes__c  getMessage() {
        
        string Message;
        NotificationController notificationControl = new NotificationController();
        Notification= [SELECT  id ,NotificationMessage__c from ServiceNotes__c where name='notes' limit 1];
        Message=Notification.NotificationMessage__c;
        return Notification ;
        
        
        
    }
    @AuraEnabled(cacheable=true)
    public static   Boolean  getUpdate() {
        
        
        List<PermissionSetAssignment> permissionSetToUser=  [SELECT Id, AssigneeId,PermissionSet.Label, Assignee.Name, PermissionSet.IsOwnedByProfile
                                                             FROM PermissionSetAssignment where AssigneeId=:userinfo.getUserId() AND  PermissionSet.Label='CustomerServiceNotes'];
        
        if(permissionSetToUser.size()>0)
        {
            recordUpdateVisibility=true;
        }
        else
        {
            recordUpdateVisibility=false;
        }
        
        
        return recordUpdateVisibility ;
    }
    
    
}