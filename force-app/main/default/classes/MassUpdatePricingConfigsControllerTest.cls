/**
* @author: Jason Flippen
* @description: Test class for Pricing Configurations Mass Update
* @class coverage: MassUpdatePricingConfigsController
*/
@isTest
public class MassUpdatePricingConfigsControllerTest {

    /**
    * @author: Jason Flippen
    * @description: Method to test the getPricingConfigurations method in the Controller.
    * @params: N/A
    * @returns: N/A
    */ 
    private static testMethod void testGetPricingConfigurations() {

        // Create Test Data.
        Wholesale_Pricebook__c testWholesalePricebook = new Wholesale_Pricebook__c(Name = 'Test Wholesale Pricebook',
                                                                                   Status__c = 'In Process',
                                                                                   Effective_Date__c = Date.today());
        insert testWholesalePricebook;

        Id pcRecordTypeId = Schema.SObjectType.Pricing_Configuration__c.getRecordTypeInfosByDeveloperName().get('Window_Pricing_Configuration').getRecordTypeId();
        Pricing_Configuration__c testPricingConfiguration = new Pricing_Configuration__c(Name = 'Test Pricing Configuration',
                                                                                         RecordTypeId = pcRecordTypeId,
                                                                                         Wholesale_Pricebook__c = testWholesalePricebook.Id,
                                                                                         DP_Performance_Upgrade__c = 1.00,
                                                                                         EJ_Full_Frame__c = 1.00,
                                                                                         Factory_Applied_Brickmould__c = 1.00,
                                                                                         Brickmould_3_Inch__c = 1.00,
                                                                                         Overfit_Flange_Price__c = 1.00,
                                                                                         Overfit_Chamfered_Edge__c = 1.00,
                                                                                         Nail_Flange__c = 1.00,
                                                                                         Base_Unit__c = 1.00,
                                                                                         Dual_Color__c = 1.00,
                                                                                         Stainable_Wood_Interior__c = 1.00,
                                                                                         Premium_Stainable_Wood_Interior__c = 1.00,
                                                                                         Wrapped_Interior_Color__c = 1.00,
                                                                                         Tempered__c = 1.00,
                                                                                         SmartSun__c = 1.00,
                                                                                         Heat_Lock__c = 1.00,
                                                                                         Obscure__c = 1.00,
                                                                                         ObscureTempered__c = 1.00,
                                                                                         SpecialtyGlazing__c = 1.00,
                                                                                         SpecialtyTemperedGlazing__c = 1.00,
                                                                                         Laminated__c = 1.00,
                                                                                         LaminatedAndTempered__c = 1.00,
                                                                                         Breather_Tubes__c = 1.00,
                                                                                         Grilles_Between_Glass__c = 1.00,
                                                                                         GBG_Wide_Bar__c = 1.00,
                                                                                         Interior_Wood_Only__c = 1.00,
                                                                                         Interior_Wood_Grilles_INTW__c = 1.00,
                                                                                         INTW_Wide_Bar__c = 1.00,
                                                                                         Full_Divided_Light__c = 1.00,
                                                                                         FDL_Wide_Bar__c = 1.00,
                                                                                         Applied_Grilles__c = 1.00,
                                                                                         Corrosion_Resistant_Hardware__c = 1.00,
                                                                                         Extra_Lock__c = 1.00,
                                                                                         Extra_Lift_or_Pull__c = 1.00,
                                                                                         Window_Opening_Control_Device__c = 1.00,
                                                                                         Wide_Opening_Hinge__c = 1.00,
                                                                                         Recessed_Lift_Charge__c = 1.00,
                                                                                         Estate_Finish_Finger_Lifts__c = 1.00,
                                                                                         Estate_Finish_Hand_Lift_or_Hand_Pull__c = 1.00,
                                                                                         Estate_Finish_Hardware__c = 1.00,
                                                                                         Estate_Finish_Lock_and_Keeper__c = 1.00,
                                                                                         Fixed_Sash_Deduction_Casement__c = 1.00,
                                                                                         AluminumScreenCloth__c = 1.00,
                                                                                         TruScene__c = 1.00,
                                                                                         TruScene_Dual__c = 1.00,
                                                                                         TruScene_Wood_Veneer__c = 1.00,
                                                                                         Aluminum_Screen_Half__c = 1.00,
                                                                                         TruScene_Half__c = 1.00);

        insert testPricingConfiguration;

        List<String> fieldNameList = new List<String>{'DP_Performance_Upgrade__c',
                                                      'EJ_Full_Frame__c',
                                                      'Factory_Applied_Brickmould__c',
                                                      'Brickmould_3_Inch__c',
                                                      'Overfit_Flange_Price__c',
                                                      'Overfit_Chamfered_Edge__c',
                                                      'Nail_Flange__c',
                                                      'Base_Unit__c',
                                                      'Dual_Color__c',
                                                      'Stainable_Wood_Interior__c',
                                                      'Premium_Stainable_Wood_Interior__c',
                                                      'Wrapped_Interior_Color__c',
                                                      'Tempered__c',
                                                      'SmartSun__c',
                                                      'Heat_Lock__c',
                                                      'Obscure__c',
                                                      'ObscureTempered__c',
                                                      'SpecialtyGlazing__c',
                                                      'SpecialtyTemperedGlazing__c',
                                                      'Laminated__c',
                                                      'LaminatedAndTempered__c',
                                                      'Breather_Tubes__c',
                                                      'Grilles_Between_Glass__c',
                                                      'GBG_Wide_Bar__c',
                                                      'Interior_Wood_Only__c',
                                                      'Interior_Wood_Grilles_INTW__c',
                                                      'INTW_Wide_Bar__c',
                                                      'Full_Divided_Light__c',
                                                      'FDL_Wide_Bar__c',
                                                      'Applied_Grilles__c',
                                                      'Corrosion_Resistant_Hardware__c',
                                                      'Extra_Lock__c',
                                                      'Extra_Lift_or_Pull__c',
                                                      'Window_Opening_Control_Device__c',
                                                      'Wide_Opening_Hinge__c',
                                                      'Recessed_Lift_Charge__c',
                                                      'Estate_Finish_Finger_Lifts__c',
                                                      'Estate_Finish_Hand_Lift_or_Hand_Pull__c',
                                                      'Estate_Finish_Hardware__c',
                                                      'Estate_Finish_Lock_and_Keeper__c',
                                                      'Fixed_Sash_Deduction_Casement__c',
                                                      'AluminumScreenCloth__c',
                                                      'TruScene__c',
                                                      'TruScene_Dual__c',
                                                      'TruScene_Wood_Veneer__c',
                                                      'Aluminum_Screen_Half__c',
                                                      'TruScene_Half__c'};

        Test.startTest();

            // Retrieve the List of Pricing Configuration Columns related to the Test Wholesale Pricebook.
            List<String> productList = new List<String>();
            List<MassUpdatePricingConfigsController.PricingConfigurationWrapper> wrapperList = MassUpdatePricingConfigsController.getPricingConfigurations(testWholesalePricebook.Id,
                                                                                                                                                           productList,
                                                                                                                                                           null,
                                                                                                                                                           null,
                                                                                                                                                           fieldNameList,
                                                                                                                                                           'increaseByDollar',
                                                                                                                                                           2.00,
                                                                                                                                                           null,
                                                                                                                                                           'nearestWholeDollar');
            System.assertEquals(3.00,wrapperList[0].baseUnitNew);

        Test.stopTest();

    }

    /**
    * @author: Jason Flippen
    * @description: Method to test the getPricingConfigurationColumns method in the Controller.
    * @params: N/A
    * @returns: N/A
    */ 
    private static testMethod void testGetPricingConfigurationColumns() {

        // Create Test Data.
        List<String> fieldNameList = new List<String>{'DP_Performance_Upgrade__c',
                                                      'EJ_Full_Frame__c',
                                                      'Factory_Applied_Brickmould__c',
                                                      'Brickmould_3_Inch__c',
                                                      'Overfit_Flange_Price__c',
                                                      'Overfit_Chamfered_Edge__c',
                                                      'Nail_Flange__c',
                                                      'Base_Unit__c',
                                                      'Dual_Color__c',
                                                      'Stainable_Wood_Interior__c',
                                                      'Premium_Stainable_Wood_Interior__c',
                                                      'Wrapped_Interior_Color__c',
                                                      'Tempered__c',
                                                      'SmartSun__c',
                                                      'Heat_Lock__c',
                                                      'Obscure__c',
                                                      'ObscureTempered__c',
                                                      'SpecialtyGlazing__c',
                                                      'SpecialtyTemperedGlazing__c',
                                                      'Laminated__c',
                                                      'LaminatedAndTempered__c',
                                                      'Breather_Tubes__c',
                                                      'Grilles_Between_Glass__c',
                                                      'GBG_Wide_Bar__c',
                                                      'Interior_Wood_Only__c',
                                                      'Interior_Wood_Grilles_INTW__c',
                                                      'INTW_Wide_Bar__c',
                                                      'Full_Divided_Light__c',
                                                      'FDL_Wide_Bar__c',
                                                      'Applied_Grilles__c',
                                                      'Corrosion_Resistant_Hardware__c',
                                                      'Extra_Lock__c',
                                                      'Extra_Lift_or_Pull__c',
                                                      'Window_Opening_Control_Device__c',
                                                      'Wide_Opening_Hinge__c',
                                                      'Recessed_Lift_Charge__c',
                                                      'Estate_Finish_Finger_Lifts__c',
                                                      'Estate_Finish_Hand_Lift_or_Hand_Pull__c',
                                                      'Estate_Finish_Hardware__c',
                                                      'Estate_Finish_Lock_and_Keeper__c',
                                                      'Fixed_Sash_Deduction_Casement__c',
                                                      'AluminumScreenCloth__c',
                                                      'TruScene__c',
                                                      'TruScene_Dual__c',
                                                      'TruScene_Wood_Veneer__c',
                                                      'Aluminum_Screen_Half__c',
                                                      'TruScene_Half__c'};

        Test.startTest();

            // Retrieve the List of Pricing Configuration Columns related to the Test Wholesale Pricebook.
            List<MassUpdatePricingConfigsController.PricingConfigurationColumnWrapper> columnWrapperList = MassUpdatePricingConfigsController.getPricingConfigurationColumns(fieldNameList);
            System.assertEquals(true,columnWrapperList.size()>1);

        Test.stopTest();

    }

    /**
    * @author: Jason Flippen
    * @description: Method to test the savePricingConfigurationUpdates method in the Controller.
    * @params: N/A
    * @returns: N/A
    */ 
    private static testMethod void testSavePricingConfigurationUpdates() {

        // Create Test Data.
        Wholesale_Pricebook__c testWholesalePricebook = new Wholesale_Pricebook__c(Name = 'Test Wholesale Pricebook',
                                                                                   Status__c = 'In Process',
                                                                                   Effective_Date__c = Date.today());
        insert testWholesalePricebook;

        Id pcRecordTypeId = Schema.SObjectType.Pricing_Configuration__c.getRecordTypeInfosByDeveloperName().get('Window_Pricing_Configuration').getRecordTypeId();
        Pricing_Configuration__c testPricingConfiguration = new Pricing_Configuration__c(Name = 'Test Pricing Configuration',
                                                                                         RecordTypeId = pcRecordTypeId,
                                                                                         Wholesale_Pricebook__c = testWholesalePricebook.Id);
        insert testPricingConfiguration;

        MassUpdatePricingConfigsController.PricingConfigurationWrapper updatePCW = new MassUpdatePricingConfigsController.PricingConfigurationWrapper();
        updatePCW.pricingConfigurationId = testPricingConfiguration.Id;
        updatePCW.productName = 'Test Product';
        updatePCW.beginningUI = 1;
        updatePCW.endingUI = 1;
        updatePCW.dpPerformanceUpgradeNew = 1.00;
        updatePCW.ejFullFrameNew = 1.00;
        updatePCW.factoryAppliedBrickmouldNew = 1.00;
        updatePCW.brickmould3InchNew = 1.00;
        updatePCW.overfitFlangePriceNew = 1.00;
        updatePCW.overfitChamferedEdgeNew = 1.00;
        updatePCW.nailFlangeNew = 1.00;
        updatePCW.baseUnitNew = 1.00;
        updatePCW.dualColorNew = 1.00;
        updatePCW.stainableWoodInteriorNew = 1.00;
        updatePCW.premiumStainableWoodInteriorNew = 1.00;
        updatePCW.wrappedInteriorColorNew = 1.00;
        updatePCW.temperedNew = 1.00;
        updatePCW.smartSunNew = 1.00;
        updatePCW.heatLockNew = 1.00;
        updatePCW.obscureNew = 1.00;
        updatePCW.obscureTemperedNew = 1.00;
        updatePCW.specialtyGlazingNew = 1.00;
        updatePCW.specialtyTemperedGlazingNew = 1.00;
        updatePCW.laminatedNew = 1.00;
        updatePCW.laminatedAndTemperedNew = 1.00;
        updatePCW.breatherTubesNew = 1.00;
        updatePCW.grillesBetweenGlassNew = 1.00;
        updatePCW.gbgWideBarNew = 1.00;
        updatePCW.interiorWoodOnlyNew = 1.00;
        updatePCW.interiorWoodGrillesINTWNew = 1.00;
        updatePCW.intwWideBarNew = 1.00;
        updatePCW.fullDividedLightNew = 1.00;
        updatePCW.fdlWideBarNew = 1.00;
        updatePCW.appliedGrillesNew = 1.00;
        updatePCW.corrosionResistantHardwareNew = 1.00;
        updatePCW.extraLockNew = 1.00;
        updatePCW.extraLiftOrPullNew = 1.00;
        updatePCW.windowOpeningControlDeviceNew = 1.00;
        updatePCW.wideOpeningHingeNew = 1.00;
        updatePCW.recessedLiftChargeNew = 1.00;
        updatePCW.estateFinishFingerLiftsNew = 1.00;
        updatePCW.estateFinishHandLiftOrHandPullNew = 1.00;
        updatePCW.estateFinishHardwareNew = 1.00;
        updatePCW.estateFinishLockAndKeeperNew = 1.00;
        updatePCW.fixedSashDeductionCasementNew = 1.00;
        updatePCW.aluminumScreenClothNew = 1.00;
        updatePCW.truSceneNew = 1.00;
        updatePCW.truSceneDualNew = 1.00;
        updatePCW.truSceneWoodVeneerNew = 1.00;
        updatePCW.aluminumScreenHalfNew = 1.00;
        updatePCW.truSceneHalfNew = 1.00;

        List<MassUpdatePricingConfigsController.PricingConfigurationWrapper> updatePCWList = new List<MassUpdatePricingConfigsController.PricingConfigurationWrapper>();
        updatePCWList.add(updatePCW);

        Test.startTest();

            // Save the updated Pricing Configuration record.
            Boolean saveResult = MassUpdatePricingConfigsController.savePricingConfigurationUpdates(updatePCWList);
            System.assertEquals(true,saveResult);

        Test.stopTest();

    }

}