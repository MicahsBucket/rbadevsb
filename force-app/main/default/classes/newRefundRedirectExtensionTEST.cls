@isTest
public with sharing class newRefundRedirectExtensionTEST {
    @isTest
    public static void redirectTestFromOrder(){
		TestUtilityMethods utility = new TestUtilityMethods();
		
		utility.createOrderTestRecords();

		Refund__c p = new Refund__c();
		Order soldOrder = [SELECT Id FROM Order WHERE Name = 'Sold Order 1'];
		p.Order__c = soldOrder.Id;


		PageReference pageRef = Page.RMS_newRefundRedirect;
		Test.setCurrentPage(pageRef);

		ApexPages.StandardController stdController = new ApexPages.StandardController(p);
		newRefundRedirectExtension customController  = new newRefundRedirectExtension(stdController);
				
		customController.redirect();
		
		
		// remove the store location from order and retest
		soldOrder.Store_Location__c = null;
		update soldOrder;
		ApexPages.StandardController stdController2 = new ApexPages.StandardController(p);
		newRefundRedirectExtension customController2  = new newRefundRedirectExtension(stdController2);
				
		customController2.redirect();		

	}
	/*******************************************************
					redirectTestWithoutOrder
    *******************************************************/
    @isTest
	public static void redirectTestWithoutOrder(){
		TestUtilityMethods utility = new TestUtilityMethods();
		
		utility.createOrderTestRecords();

		Refund__c p = new Refund__c();

		PageReference pageRef = Page.RMS_newRefundRedirect;
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController stdController = new ApexPages.StandardController(p);
		newRefundRedirectExtension customController  = new newRefundRedirectExtension(stdController);
		
		// test with store location
		customController.testStoreLocation = '77 - Twin Cities, MN';				
		customController.redirect();

		// test with null store location
		customController.testStoreLocation = null;				
		customController.redirect();

	}
}