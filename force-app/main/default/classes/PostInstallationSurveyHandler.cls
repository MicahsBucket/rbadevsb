/******************************************************************
 * Author : Sundeep
 * Class Name :PostInstallationSurveyHandler
 * Createdate :
 * Description : Methods for Creating the Survey
 * Change Log
 * ---------------------------------------------------------------------------
 * Date:  3/14/2019       Name: Sundeep          Description
 * ---------------------------------------------------------------------------
 * 
 * 
 * ----------------------------------------------------------------------------
 * *****************************************************************/

public class PostInstallationSurveyHandler {
    
    public static void surveyProcessCreation(list<Order> scope){
        
        Map<Id,List<OpportunityContactRole>> mapOpportunityIdToPrimaryContactIdList = new Map<Id,List<OpportunityContactRole>>();
        Map<Id,List<OpportunityContactRole>> mapOpportunityIdToSecondaryContactIdList = new Map<Id,List<OpportunityContactRole>>();
        Map<Id, Survey__c> primaryContactSurveyMap = new Map<Id, Survey__c>();
        Map<Id, Survey__c> secondaryContactSurveyMap = new Map<Id, Survey__c>();
        Map<String,Id> WorkTypeMap = new Map<String,Id>();
        List<string> workTypeNames = new List<string>();
        system.debug('Order ------------------->'+scope);
        Map<Id,User> SalesRepUserMap = New Map<Id,User>();
        Map<Id,Worker__c> SalesRepexistingWorkMap = New Map<Id,Worker__c>();
        Map<Id,Worker__c> FinalOrdSalRepWorkMap = New Map<Id,Worker__c>();
        
        Map<Id,Worker__c> PriInstexistingWorkMap = New Map<Id,Worker__c>();
        Map<Id,Worker__c> FinalOrdPriInstWorkMap = New Map<Id,Worker__c>();
        Map<Id,User> PriInstUserMap = New Map<Id,User>();
        
        Map<Id,Worker__c> PriTechexistingWorkMap = New Map<Id,Worker__c>();
        Map<Id,Worker__c> FinalOrdPriTechWorkMap = New Map<Id,Worker__c>();
        Map<Id,User> PriTechUserMap = New Map<Id,User>();
        
        workTypeNames.add('Measure');
        workTypeNames.add('Install' );
        List<Survey__c> surveyList = new List<Survey__c>();
        for(WorkType wt:  [SELECT Id, Name FROM WorkType WHERE Name In :workTypeNames]){
            WorkTypeMap.put(wt.name,wt.Id);
        }
        
        
        String sourceSystem = 'rForce';
        Id recordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get('Post Install').getRecordTypeId();
        String installerRole ='Install';
        String techMeasurerRole = 'Tech Measure';
        String salesRepRole = 'Sales Rep';           
        
        Map<Id,Id> mapOrderIdToOpportunityId = new Map<Id,Id>();
        Map<Id, Order> mapOrderIdToOrder = new Map<Id,Order>();
        Map<Id,Id> orderOptyOwnerMap = new Map<Id,Id>();
        
        Map<Id,Id> OrderPriTechResourseMap = new Map<Id,Id>();
        Map<Id,Id> OrderPriInstResourseMap = new Map<Id,Id>();
        Map<Id,List<Worker__c>> ordPriTechWorkersMap = new Map<Id,List<Worker__c>>();
        Map<Id,List<Worker__c>> ordPriTInstWorkersMap = new Map<Id,List<Worker__c>>();
        Map<Id,Opportunity> mapOpportunities = new Map<id,opportunity>();
        Map<id,order> OrdersMap = new Map<Id,Order>();
        
        for(order order:scope){
            if(order.OpportunityId != null){
                mapOrderIdToOpportunityId.put(order.Id, order.OpportunityId);
                orderOptyOwnerMap.put(order.Id,order.Opportunity.owner.Id);
            }
            mapOrderIdToOrder.put(order.Id, order);
            
        }   
        system.debug('@sun mapOrderIdToOrder'+mapOrderIdToOrder);
        
        if(!mapOrderIdToOpportunityId.isEmpty()){
            //create a map of opportunity Ids to opportunities
            for(Opportunity opty :[SELECT Id, Store_Location__c,owner.id, Enabled_Appointment_Id__c, Enabled_Lead_Id__c, Pivotal_Id__c, LeadSource 
                                   FROM Opportunity 
                                   WHERE Id IN: mapOrderIdToOpportunityId.values()]){
                                       mapOpportunities.put(opty.Id,opty);                                                          
                                       
                                   }
            
            for(Order ord:[SELECT Id,Store_Location__c,OpportunityId,Opportunity.owner.Id,Primary_Tech_Measure_FSL__c,Primary_Tech_Measure_FSL__r.RelatedRecordId,Primary_Installer_FSL__c,Primary_Installer_FSL__r.RelatedRecordId 
                           From Order where Id IN: mapOrderIdToOpportunityId.keyset()]){
                               OrdersMap.put(ord.Id,ord);
                               //set<id> serResoursId = new Set<Id>();
                               if(ord.Primary_Tech_Measure_FSL__c != null && ord.Primary_Tech_Measure_FSL__r.RelatedRecordId!=null){
                                   //serResoursId.add(ord.Primary_Tech_Measure_FSL__r.RelatedRecordId);
                                   OrderPriTechResourseMap.put(ord.Id,ord.Primary_Tech_Measure_FSL__r.RelatedRecordId);
                               }
                               if(ord.Primary_Installer_FSL__c != null && ord.Primary_Installer_FSL__r.RelatedRecordId!=null){
                                   //serResoursId.add(ord.Primary_Installer_FSL__r.RelatedRecordId);
                                   OrderPriInstResourseMap.put(ord.Id,ord.Primary_Installer_FSL__r.RelatedRecordId);
                                   
                                   
                               }
                               
                               
                           } 
            system.debug('@sun OrderPriInstResourseMap'+OrderPriInstResourseMap);
            
            // Primary Tech measure 
            Map<Id,Worker__c > newPriTechWrkrMap = new Map<Id,Worker__c >();
            if(!OrderPriTechResourseMap.isEmpty()){
                for(User U: [SELECT Id, Email, FirstName, LastName, MobilePhone, Enabled_User_Id__c, Pivotal_Id__c FROM User WHERE Id IN :OrderPriTechResourseMap.values()]){
                    PriTechUserMap.put(u.id,u);
                }
                            system.debug('@sun PriTechUserMap'+PriTechUserMap);

                For(Worker__c existingWorkers : [SELECT Id, Enabled_User_Id__c, User__c FROM Worker__c WHERE  User__c IN :OrderPriTechResourseMap.values() AND Source_System__c != 'Manual Process']){
                    PriTechexistingWorkMap.put(existingWorkers.User__c,existingWorkers);
                    
                }
                                     system.debug('@sun PriTechexistingWorkMap'+PriTechexistingWorkMap);

                for(Id ordId : OrderPriTechResourseMap.keyset())
                {
                    
                    if(OrderPriTechResourseMap.containsKey(ordId)){
                        Id userid = OrderPriTechResourseMap.get(ordId);
                        if(PriTechexistingWorkMap.containsKey(userid)){
                            FinalOrdPriTechWorkMap.put(ordId,PriTechexistingWorkMap.get(userid));
                        }else{
                            user u = PriTechUserMap.get(userid);
                            boolean isDes = true;
                            Decimal enabledDec;
                            if(u.Enabled_User_Id__c != null){
                                try{
                                    enabledDec = Decimal.valueOf(u.Enabled_User_Id__c);
                                    
                            }
                                catch(exception e){
                                    isDes = false;
                                }
                            }
                            Worker__c newWorker = new Worker__c();
                                newWorker.Email__c = u.Email;
                                if(isDes){
                                    newWorker.Enabled_User_Id__c = enabledDec;
                                }                                
                                newWorker.First_Name__c = u.FirstName;
                                newWorker.Last_Name__c = u.LastName;
                                newWorker.Mobile__c = u.MobilePhone;
                                //  Pivotal_Employee_Id__c = u.Pivotal_Id__c,
                                newWorker.User__c = u.Id;
                                newWorker.Role__c = 'Tech Measurer';
                                newWorker.Store_Account__c = OrdersMap.get(ordId).Store_Location__c;
                                newWorker.Source_System__c = 'rForce';
                            
                            newPriTechWrkrMap.put(ordId,newWorker);
                            
                        }
                        
                    }
                    
                    
                }
                            system.debug('@sun FinalOrdPriTechWorkMap.values()'+FinalOrdPriTechWorkMap.values());

                DataBase.insert(newPriTechWrkrMap.values(),false);
                FinalOrdPriTechWorkMap.putAll(newPriTechWrkrMap);
                
            }//End Of Prim Tech Construction
            
            
            //Primary Install
            Map<Id,Worker__c > NewPriTechWorker = new Map<Id,Worker__c >();
            if(!OrderPriInstResourseMap.isEmpty()){
                for(User U: [SELECT Id, Email, FirstName, LastName, MobilePhone, Enabled_User_Id__c, Pivotal_Id__c FROM User WHERE Id IN :OrderPriInstResourseMap.values()]){
                    PriInstUserMap.put(u.id,u);
                }
                
                For(Worker__c existingWorkers : [SELECT Id, Enabled_User_Id__c, User__c FROM Worker__c WHERE  User__c IN :OrderPriInstResourseMap.values() AND Source_System__c != 'Manual Process']){
                    PriInstexistingWorkMap.put(existingWorkers.User__c,existingWorkers);
                    
                }
                
                for(Id ordId : OrderPriInstResourseMap.keyset())
                {
                    
                    if(OrderPriInstResourseMap.containsKey(ordId)){
                        Id userid = OrderPriInstResourseMap.get(ordId);
                        if(PriInstexistingWorkMap.containsKey(userid)){
                            FinalOrdPriInstWorkMap.put(ordId,PriInstexistingWorkMap.get(userid));
                        }else{
                            user u = PriInstUserMap.get(userid);
                            boolean isDes = true;
                            Decimal enabledDec;
                            if(u.Enabled_User_Id__c != null){
                                try{
                                    enabledDec = Decimal.valueOf(u.Enabled_User_Id__c);
                                    
                            }
                                catch(exception e){
                                    isDes = false;
                                }
                            }
                            Worker__c newWorker = new Worker__c();
                                newWorker.Email__c = u.Email;
                                if(isDes){
                                    newWorker.Enabled_User_Id__c = enabledDec;
                                }
                                newWorker.First_Name__c = u.FirstName;
                                newWorker.Last_Name__c = u.LastName;
                                newWorker.Mobile__c = u.MobilePhone;
                                //  Pivotal_Employee_Id__c = u.Pivotal_Id__c,
                                newWorker.User__c = u.Id;
                                newWorker.Role__c = 'Installer';
                                newWorker.Store_Account__c = OrdersMap.get(ordId).Store_Location__c;
                                newWorker.Source_System__c = 'rForce';
                            
                            NewPriTechWorker.put(ordId,newWorker);
                            
                        }
                        
                    }
                    
                    
                }
                DataBase.insert(NewPriTechWorker.values(),false);
                FinalOrdPriInstWorkMap.putAll(NewPriTechWorker);
                
            }// end of Primary Installer
            
            //Sales Rep logic start here 
            
            Map<Id,Worker__c> newSalWorkerMap = new Map<Id,Worker__c>();
            if(!orderOptyOwnerMap.isEmpty()){
                for(User U: [SELECT Id, Email, FirstName, LastName, MobilePhone, Enabled_User_Id__c, Pivotal_Id__c FROM User WHERE Id IN :orderOptyOwnerMap.values()]){
                    SalesRepUserMap.put(u.id,u);
                }
                For(Worker__c existingWorkers : [SELECT Id, Enabled_User_Id__c, User__c FROM Worker__c WHERE  User__c IN :orderOptyOwnerMap.values() AND Source_System__c != 'Manual Process']){
                    SalesRepexistingWorkMap.put(existingWorkers.User__c,existingWorkers);
                    
                }
                for(Id ordId : orderOptyOwnerMap.keyset())
                {    
                    if(orderOptyOwnerMap.containsKey(ordId)){
                        Id userid = orderOptyOwnerMap.get(ordId);
                        if(SalesRepexistingWorkMap.containsKey(userid)){
                            FinalOrdSalRepWorkMap.put(ordId,SalesRepexistingWorkMap.get(userid));
                        }else{
                            system.debug('@sun '+SalesRepUserMap.get(userid));
                            user u = SalesRepUserMap.get(userid);
                            boolean isDes = true;
                            Decimal enabledDec;
                            if(u.Enabled_User_Id__c != null){
                                try{
                                    enabledDec = Decimal.valueOf(u.Enabled_User_Id__c);
                                    
                            }
                                catch(exception e){
                                    isDes = false;
                                }
                            }
                            Worker__c newWorker = new Worker__c();
                                newWorker.Email__c = u.Email;
                                if(isDes){
                                    newWorker.Enabled_User_Id__c = enabledDec;
                                }
                                newWorker.First_Name__c = u.FirstName;
                                newWorker.Last_Name__c = u.LastName;
                                newWorker.Mobile__c = u.MobilePhone;
                                //  Pivotal_Employee_Id__c = u.Pivotal_Id__c,
                                newWorker.User__c = u.Id;
                                newWorker.Role__c = 'Sales Rep';
                                newWorker.Store_Account__c = OrdersMap.get(ordId).Store_Location__c;
                                newWorker.Source_System__c = 'rForce';
                            newSalWorkerMap.put(ordId,newWorker);
                            
                        }
                        
                    }
                }
                
                 system.debug('@sun FinalOrdSalRepWorkMap.values()'+FinalOrdSalRepWorkMap.values());
            }
            DataBase.insert(newSalWorkerMap.values(),false);
            FinalOrdSalRepWorkMap.putAll(newSalWorkerMap);
            
        }    
        
        if(!mapOrderIdToOpportunityId.isEmpty()){
            for(OpportunityContactRole oppConRole : [SELECT Id, OpportunityId, ContactId,Contact.MailingState, Contact.MailingStreet, Contact.MailingPostalCode, 
                                                     Contact.MailingCity, Contact.MailingCountry, Contact.Email, Contact.FirstName, Contact.LastName, Contact.HomePhone, Contact.MobilePhone, Contact.Phone,
                                                     isPrimary FROM OpportunityContactRole WHERE OpportunityId IN :mapOrderIdToOpportunityId.values()]){
                                                         if(oppConRole.isPrimary){
                                                             if(mapOpportunityIdToPrimaryContactIdList.containsKey(oppConRole.OpportunityId)){
                                                                 mapOpportunityIdToPrimaryContactIdList.get(oppConRole.OpportunityId).add(oppConRole);
                                                             }else{
                                                                 mapOpportunityIdToPrimaryContactIdList.put(oppConRole.OpportunityId, new list<OpportunityContactRole>{oppConRole}); 
                                                             }
                                                             
                                                         }else{
                                                             if(mapOpportunityIdToSecondaryContactIdList.containsKey(oppConRole.OpportunityId)){
                                                                 mapOpportunityIdToSecondaryContactIdList.get(oppConRole.OpportunityId).add(oppConRole);
                                                             }else{
                                                                 mapOpportunityIdToSecondaryContactIdList.put(oppConRole.OpportunityId, new list<OpportunityContactRole>{oppConRole}); 
                                                             }
                                                             
                                                         }
                                                         
                                                     }
            system.debug('@sun mapOpportunityIdToSecondaryContactIdList.values()'+mapOpportunityIdToSecondaryContactIdList.values());
            for(Id OrderId:mapOrderIdToOpportunityId.keyset()){
               
              list<OpportunityContactRole> primOptyContRl;
              Id opportunityId = mapOrderIdToOpportunityId.get(OrderId);
                 primOptyContRl = mapOpportunityIdToPrimaryContactIdList.get(OpportunityId)!= null ? mapOpportunityIdToPrimaryContactIdList.get(OpportunityId) : new list<OpportunityContactRole>();
              list<OpportunityContactRole> secondOptyContRl = mapOpportunityIdToSecondaryContactIdList.get(opportunityId)!= null ?  mapOpportunityIdToSecondaryContactIdList.get(opportunityId) : new list<OpportunityContactRole>();
                Opportunity opportunity = mapOpportunities.get(mapOrderIdToOpportunityId.get(OrderId));
                if(!primOptyContRl.isEmpty()){
                     Survey__c newSurvey = new Survey__c();
                newSurvey.Contact_Name__c = primOptyContRl[0].contactId;
                newSurvey.Source_System__c = sourceSystem;
                newSurvey.RecordTypeId = recordTypeId;
                newSurvey.Store_Account__c = opportunity.Store_Location__c;
                newSurvey.Order_Name__c = orderId;
                newSurvey.Installation_Date__c = mapOrderIdToOrder.get(orderId).Installation_Date__c;
                newSurvey.Opportunity__c = opportunity.Id;
                newSurvey.Enabled_Appointment_Id__c = opportunity.Enabled_Appointment_Id__c;
                newSurvey.Enabled_Lead_Id__c = opportunity.Enabled_Lead_Id__c;
                newSurvey.Pivotal_Rn_AppointmentID__c = opportunity.Pivotal_Id__c;
                newSurvey.Appointment_Date__c=mapOrderIdToOrder.get(orderId).EffectiveDate; 			/* to map the Booking date to Survey Appointment Date */
                newSurvey.Primary_Contact_First_Name__c = primOptyContRl[0].contact.FirstName;
                newSurvey.Primary_Contact_Last_Name__c = primOptyContRl[0].contact.LastName;
                newSurvey.Primary_Contact_Email__c = primOptyContRl[0].contact.Email;
                newSurvey.Primary_Contact_Home_Phone__c = primOptyContRl[0].contact.HomePhone;
                newSurvey.Primary_Contact_Work_Phone__c = primOptyContRl[0].contact.Phone;
                newSurvey.Primary_Contact_Mobile_Phone__c = primOptyContRl[0].contact.MobilePhone;
                newSurvey.State__c = primOptyContRl[0].contact.MailingState;
                newSurvey.City__c = primOptyContRl[0].contact.MailingCity;
                newSurvey.Country__c = primOptyContRl[0].contact.MailingCountry;
                newSurvey.Street__c = primOptyContRl[0].contact.MailingStreet;
                newSurvey.Zip__c = primOptyContRl[0].contact.MailingPostalCode;
                newSurvey.Lead_Source__c = opportunity.LeadSource;
                if(!secondOptyContRl.isEmpty() ){//&& secondOptyContRl[0].contactId != null){
                    newSurvey.Secondary_Contact_First_Name__c = secondOptyContRl[0].Contact.FirstName;
                    newSurvey.Secondary_Contact_Last_Name__c = secondOptyContRl[0].Contact.LastName;
                    newSurvey.Secondary_Contact_Email__c = secondOptyContRl[0].Contact.Email;
                    newSurvey.Secondary_Contact_Home_Phone__c = secondOptyContRl[0].Contact.HomePhone;
                    newSurvey.Secondary_Contact_Mobile_Phone__c = secondOptyContRl[0].Contact.MobilePhone;
                    newSurvey.Secondary_Contact_Work_Phone__c = secondOptyContRl[0].Contact.Phone;
                    secondaryContactSurveyMap.put(secondOptyContRl[0].contactId, newSurvey); 
                    
                }
                primaryContactSurveyMap.put(newSurvey.Contact_Name__c, newSurvey);
                surveyList.add(newSurvey);
                    
                }
               
                
            }
            
            
        }
            
        Set<Id> servids = new Set<Id>();
        Map<Id,Id> servyOrdMap = new Map<Id,Id>();
        if(!surveyList.isEmpty()){
            List<Database.saveResult> results = Database.insert(surveyList, false);
            for(Database.saveResult result:results) {
                
                if(result.isSuccess()){
                    
                    servids.add(result.getId());
                }
            }
            
            
        }
        list<Order> UpdatedOrds = new list<Order>();
        if(!servids.isEmpty()){
            list<Survey__c> srvlist = [SELECT Id,Order_Name__c From Survey__c where Id IN:servids];
            for(Survey__c srv:srvlist){
                servyOrdMap.put(srv.Id,srv.Order_Name__c);
                if(mapOrderIdToOrder.containskey(srv.Order_Name__c)){
                    order ord = mapOrderIdToOrder.get(srv.Order_Name__c);
                    ord.Survey_Created__c = True;
                    UpdatedOrds.add(ord);
                }
            }
        }
        if(!UpdatedOrds.IsEmpty()){
            update UpdatedOrds;
        }       
        
        List<Survey_Worker__c> junctionsToCreate = new List<Survey_Worker__c>();
        If(!servyOrdMap.isEmpty()){
            for(Id ServyIds: servyOrdMap.keyset()){
                if(FinalOrdSalRepWorkMap.containsKey(servyOrdMap.get(ServyIds)) && FinalOrdSalRepWorkMap.get(servyOrdMap.get(ServyIds))!= null){
                    Survey_Worker__c newSalRepSurJn = new Survey_Worker__c(
                        Survey_Id__c = ServyIds,
                        Worker_Id__c = FinalOrdSalRepWorkMap.get(servyOrdMap.get(ServyIds)).Id,
                        Role__c = 'Sales Rep'
                    );
                    junctionsToCreate.add(newSalRepSurJn);
                }
                if(FinalOrdPriInstWorkMap.containsKey(servyOrdMap.get(ServyIds)) && FinalOrdPriInstWorkMap.get(servyOrdMap.get(ServyIds))!= null){
                    Survey_Worker__c newPriInstSurJn = new Survey_Worker__c(
                        Survey_Id__c = ServyIds,
                        Worker_Id__c = FinalOrdPriInstWorkMap.get(servyOrdMap.get(ServyIds)).Id,
                        Role__c = 'Installer'
                    );
                    junctionsToCreate.add(newPriInstSurJn);
                    
                }
                if(FinalOrdPriTechWorkMap.containsKey(servyOrdMap.get(ServyIds)) && FinalOrdPriTechWorkMap.get(servyOrdMap.get(ServyIds))!= null){
                    Survey_Worker__c newPriTechSurJn = new Survey_Worker__c(
                        Survey_Id__c = ServyIds,
                        Worker_Id__c = FinalOrdPriTechWorkMap.get(servyOrdMap.get(ServyIds)).Id,
                        Role__c = 'Tech Measurer'
                    );
                    junctionsToCreate.add(newPriTechSurJn);
                    
                }
                
                
            }
            system.debug('@sun junctionsToCreate'+junctionsToCreate);
            Database.insert(junctionsToCreate, false);
            
        }
        List<Contact_Survey__c> contactSurveyJuncList = new List<Contact_Survey__c>();
        for(Id c : primaryContactSurveyMap.keySet()){
            if(c != null){
                Contact_Survey__c primConSurvJunc = new Contact_Survey__c();
                primConSurvJunc.Type__c = 'Primary';
                primConSurvJunc.Contact_Id__c = c;
                primConSurvJunc.Survey_Id__c = primaryContactSurveyMap.get(c).Id;
                contactSurveyJuncList.add(primConSurvJunc);
            }
        }
        if(secondaryContactSurveyMap.keySet().size() != 0){
            for(Id c : secondaryContactSurveyMap.keySet()){
                Contact_Survey__c secondConSurvJunc = new Contact_Survey__c();
                secondConSurvJunc.Type__c = 'Secondary';
                secondConSurvJunc.Contact_Id__c = c;
                secondConSurvJunc.Survey_Id__c = secondaryContactSurveyMap.get(c).Id;
                contactSurveyJuncList.add(secondConSurvJunc);
            }
        }
        
        
        Database.insert(contactSurveyJuncList, false);
        
        
        
    }

}