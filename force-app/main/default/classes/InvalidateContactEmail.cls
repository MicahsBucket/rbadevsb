/**
 * @description       : 
 * @author            : mark.rothermal@andersencorp.com
 * @group             : 
 * @last modified on  : 07-27-2020
 * @last modified by  : mark.rothermal@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                            Modification
 * 1.0   07-22-2020   ratna.pudota@andersencorp.com   Initial Version
 * 1.1   7-22-20        mark.rothermal@andersencorp.com updated to include phone and mobile phone fields. added comments to class and methods.
**/
Global class InvalidateContactEmail implements Database.Batchable<SObject> , Database.Stateful {

    Global Integer totalContactsEdited = 0;
    Global Integer emailEdited = 0;
    Global Integer secondaryEmailEdited = 0;
    Global Integer phoneEdited = 0;
    Global Integer mobilePhoneEdited = 0;
    Global Boolean isSandbox = UtilityMethods.isSandbox();

	/**
	* @description - Builds a list of contacts for batching.
	* @author mark.rothermal@andersencorp.com | 07-22-2020 
	* @param bc 
	* @return Database.QueryLocator 
    * TODO - future state, have batch send an email in case this job is run from production. right now setting a blank query string throws a hard error
    * that appears to prevent any email from being sent, even when attempted from the querylocator method. MTR 7-27-20
	**/
	Global Database.QueryLocator start(Database.BatchableContext bc){
        String query;
        if(Test.isRunningTest()){
            isSandbox = true;
        }
        If(isSandbox == true){
            query = 'SELECT Id,Email,Secondary_Email__c,Phone,MobilePhone from Contact WHERE'+ 
            '(Email!=null AND (NOT Email like \'%example.com%\')) OR '+
            '(Secondary_Email__c!=null AND (NOT Secondary_Email__c like \'%example.com%\')) OR' +
            '(Phone != null AND Phone !=\'(555)555-5555\') OR'+
            '(MobilePhone != null AND MobilePhone != \'(555)555-5555\')';
        } else{
            query = '';
        }    
        return Database.getQueryLocator(query);
    }

    /**
    * @description replace all valid phone numbers with 555 number and convert all valid emails to @example email addresses.
    * @author mark.rothermal@andersencorp.com | 07-22-2020 
    * @param BC 
    * @param scope - in this case scope should be a list of contact records.
    **/
    Global void execute(Database.BatchableContext BC, List<SObject> scope){
        If(isSandbox == true){
            for(Contact con:(List<Contact>)scope){
                totalContactsEdited++;
                String placeholderEmail = con.Email;
                if(placeholderEmail != null && !placeholderEmail.contains('@example.com')){
                    placeholderEmail = placeholderEmail.replace('@','=');
                    placeholderEmail += '@example.com';
                    con.Email = placeholderEmail;
                    emailEdited++;
                }
                String placeholderSecondary = con.Secondary_Email__c;
                if(placeholderSecondary != null && !placeholderSecondary.contains('@example.com')){
                    placeholderSecondary = placeholderSecondary.replace('@','=');
                    placeholderSecondary += '@example.com';
                    con.Secondary_Email__c = placeholderSecondary;
                    secondaryEmailEdited++;  
                }
                if(con.Phone != null){
                    con.Phone = '(555)555-5555';
                    phoneEdited++;  
                }
                if(con.MobilePhone != null){
                    con.MobilePhone = '(555)555-5555';  
                    mobilePhoneEdited++;
                }   
            }
            update scope;
        }
    }
    /**
    * @description Final Email to Send Completion of Batch
    * @author mark.rothermal@andersencorp.com | 07-24-2020 
    * @param BC 
    **/
    Global void finish(Database.BatchableContext BC){ 
        //Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob apexJob = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                FROM AsyncApexJob WHERE Id =: BC.getJobId()];  
        String emailList;
        if(Test.isRunningTest()){
            emailList = 'testEmail@email.com' ;
        } else {
            emailList = [Select Sandbox_Value__c 
                         FROM Application_Property__mdt 
                         Where Key__c = 'EmailAddresses' 
                         And MasterLabel = 'Admins to Email' limit 1].Sandbox_Value__c;
        }
        List<string> emailAddresses = emailList.split(';');                    
        //Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        emailAddresses.add(apexJob.CreatedBy.Email);
        system.debug('apexJob.CreatedBy.Email : '+apexJob.CreatedBy.Email);
        mail.setToAddresses(emailAddresses);
        mail.setSubject('Results of Contact Data Masking. Status is ' + apexJob.Status);
        mail.setReplyTo('noreply@salesforce.com');
        mail.setSenderDisplayName('Salesforce - Data Masking Contacts Batch Job');
        mail.setPlainTextBody('From InvalidateContactEmail class. \n'
        + 'The batch Apex job processed ' + apexJob.TotalJobItems + ' batches with '+ apexJob.NumberOfErrors + ' failures. \n' 
        + 'Number of Contacts edited: ' + totalContactsEdited + '\n'
        + 'Number of Emails updated: ' + emailEdited + '\n'
        + 'Number of Secondary Emails updated: ' + secondaryEmailEdited + '\n'
        + 'Number of Phone numbers updated: ' + phoneEdited + '\n'
        + 'Number of Mobile phone numbers updated: ' + mobilePhoneEdited);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}