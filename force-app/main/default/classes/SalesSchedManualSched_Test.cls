/**
 * @File Name          : SalesSchedManualSched_Test.cls
 * @Description        : 
 * @Author             : Sundeep Goddety (Three Bridge)
 * @Group              : 
 * @Last Modified By   : Connor.Davis@andersencorp.com
 * @Last Modified On   : 02-16-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/5/2019, 11:20:16 PM   Sundeep Goddety (Three Bridge)   Initial Version
**/

@isTest
public class SalesSchedManualSched_Test {
    
    static testmethod void testExecute(){
        testUtilityMethods  utl = new testUtilityMethods();
        
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
        Store_Configuration__c scof=[Select MaualBatchApex__c,Current_Sales_Date__c From Store_Configuration__c Where Order_Number__c = 0000001 limit 1];
        scof.Call_Center_Close_Time__c = Time.newInstance(20,0,0,0);
        scof.MaualBatchApex__c = true;
        update scof;
        Sales_Appointment__c sa = [Select Id,Appointment_Date_Time__c From Sales_Appointment__c];
        sa.Appointment_Date_Time__c = sa.Appointment_Date_Time__c.addDays(-7);
        update sa;
        
        sstu.salesOrder.Result__c = 'Sale';
        sstu.salesOrder.Time_Arrived__c = Datetime.now().addDays(-7).time();
        sstu.salesOrder.Time_Left__c = Datetime.now().addDays(-7).time();
        
        update sstu.salesOrder;
        
        sstu.opportunity.Same_Rep_Resit__c = true;
        sstu.opportunity.Repeat_Sale__c = true;
        sstu.opportunity.Follow_Up__c = true;
        sstu.opportunity.Cancel_Save__c = true;
        update sstu.opportunity;
        
        Date d = Date.today().addDays(-7);
        String todayStr = ((Datetime)d).formatGmt('yyyy-MM-dd');
        Sales_Capacity__c sc =
            new Sales_Capacity__c(
                User__c=sstu.capacities.get(0).User__c,
                OwnerId=sstu.capacities.get(0).User__c,
                Store__c= sstu.store.Id,
                Status__c='Available',
                Date__c=d,
                Slot__c=sstu.capacities.get(0).Slot__c,
                External_Id__c = sstu.capacities.get(0).User__c + '_' + todayStr + '_' + sstu.capacities.get(0).Slot__c
            );
        insert sc;
        
        List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
                Sales_Capacity__c = sc.Id,
                Sales_Appointment__c = sstu.appointments.get(0).Id,
                Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
        insert resources;
        
        resources.get(0).Status__c = 'Resulted';
        update resources;
                List<User> ul = [Select Id From User Where Profile.Name = 'Partner RMS-Sales'];

        Test.startTest();        
        SalesSchedCalSalesRepMetricsManualSched schedule = new SalesSchedCalSalesRepMetricsManualSched();
        schedule.execute(null);
        
        Test.stopTest();
        
        
    }
    
}