/**
* @author Jason Flippen
* @description Provides database functionality for the orderProductList LWC
* @ edit - Mark Rothermal - RFBuild-217 - 6/26/19
* @ edit - Mark Rothermal - RFBuild-232 - 7/19/19
*/ 
public with sharing class OrderProductListController {

    /**
    * @author Jason Flippen
    * @description Combines Parent & Child OrderItems in a List of Wrapper objects.
    * @param Id of the Order the OrderItems are associated with.
    * @returns Wrapped List of Parent/Child OrderItem records.
    */ 
    @TestVisible static Boolean setFlsForTest = true;
    
    @AuraEnabled // removed 8/2/19  -Mark Rothermal- cache not refreshing properly on change in ROP community. (cacheable=true)
    public static List<OrderItemWrapper> getOrderItems(Id orderId) {

        List<OrderItemWrapper> oiWrapperList = new List<OrderItemWrapper>();

        // Retrieve the Id of the Master Product RecordType from the Product2 object.
        Id masterProductRTId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Master_Product').getRecordTypeId();
       
        // perform a field level security check for wholesale cost. 
        Schema.DescribeFieldResult unitWholeSaleField = OrderItem.Unit_Wholesale_Cost__c.getDescribe();
        Boolean flsForUnitWholesalePrice = unitWholeSaleField.isAccessible();
        if(Test.isRunningTest()){
            flsForUnitWholesalePrice = setFlsForTest;
        }
        system.debug('Does User have permission to read unit wholesale price field '+flsForUnitWholesalePrice);
        System.debug(unitWholeSaleField.getLabel());
        // Group the OrderItem data in Parent and Parent-to-Child Maps.
        Map<Id,OrderItem> parentOIMap = new Map<Id,OrderItem>();
        Map<Id,Map<Id,OrderItem>> parentToChildOIMap = new Map<Id,Map<Id,OrderItem>>();
        // query that includes wholesale cost and total wholesale cost.
        if(flsForUnitWholesalePrice == true){
        //////////////////////////////////////////////////////////////////////////////////////
        for (OrderItem oi : [SELECT Id,
                                    Parent_Order_Item__c,
                                    Purchase_Order__c,
                                    Purchase_Order_Name__c,
                                    Purchase_Order__r.Status__c,
                                    Purchase_Order__r.Released_Timestamp__c,
                                    Product2Id,
                                    Product2.Name,
                                    Product2.IsActive,
                                    Product2.Pivotal_Id__c,
                                    Product2.RecordTypeId,
                                    Product2.Open_In_Visualforce__c,
                                    Quantity,
                                    Retail_Purchase_Order__c,
                                    Retail_Purchase_Order__r.Name,
                                    Retail_Purchase_Order__r.Retail_Purchase_Order_Name__c,
                                    Retail_Purchase_Order__r.Rpo_Name_For_OPL__c,
                                    Retail_Purchase_Order__r.Status__c,
                                    Retail_Purchase_Order__r.Released_Timestamp__c,                                    
                                    Service__c,
                                    Status__c,
                                    Total_Retail_Price__c,
                                    Total_Wholesale_Cost__c,
                                    UnitPrice,
                                    Unit_Id__c,
                                    Unit_Wholesale_Cost__c,
                                    Variant_Number__c,
                                    Config_ID__c
                             FROM   OrderItem
                             WHERE  OrderId = :orderId
                             AND Status__c != 'Cancelled'
                             ORDER BY Unit_Id__c]) {
            
            // If a Parent_Order_Item__c is null this is a Parent record.
            if (oi.Parent_Order_Item__c == null) {
                parentOIMap.put(oi.Id, oi);
            }
            else {
                if (!parentToChildOIMap.containsKey(oi.Parent_Order_Item__c)) {
                    parentToChildOIMap.put(oi.Parent_Order_Item__c, new Map<Id,OrderItem>());
                }
                parentToChildOIMap.get(oi.Parent_Order_Item__c).put(oi.Id,oi);
            }

        }

        // Iterate through the Maps and build the return "wrapper" List.
        for (OrderItem parentOI : parentOIMap.values()) {
            OrderItemWrapper oiWrapper = new OrderItemWrapper();
            oiWrapper.id = parentOI.Id;
            oiWrapper.unitId = parentOI.Unit_Id__c;
            oiWrapper.productName = parentOI.Product2.Name;
            oiWrapper.productId = parentOI.Product2Id;
            oiWrapper.productRecordTypeId = parentOI.Product2.RecordTypeId;
            oiWrapper.isMasterProductRecordType = (parentOI.Product2.RecordTypeId == masterProductRTId);
            oiWrapper.productOpenInVisualforce = parentOI.Product2.Open_In_Visualforce__c;
            oiWrapper.productPivotalId = parentOI.Product2.Pivotal_Id__c;
            oiWrapper.productIsActive = parentOI.Product2.IsActive;
            oiWrapper.purchaseOrder = parentOI.Purchase_Order__c;
            oiWrapper.retailPurchaseOrder = parentOI.Retail_Purchase_Order__c;
            oiWrapper.quantity = parentOI.Quantity;
            oiWrapper.totalRetailPrice = parentOI.Total_Retail_Price__c;
            oiWrapper.unitWholesaleCost = parentOI.Unit_Wholesale_Cost__c;
            oiWrapper.totalWholesaleCost = parentOI.Total_Wholesale_Cost__c;
            oiWrapper.variantNumber = parentOI.Variant_Number__c;
            oiWrapper.unitPrice = parentOI.UnitPrice;
            oiWrapper.service = parentOI.Service__c;
            oiWrapper.status = parentOI.Status__c;
            oiWrapper.purchaseOrderStatus = parentOI.Purchase_Order__r.Status__c;
            oiWrapper.purchaseOrderName = parentOI.Purchase_Order_Name__c;
            oiWrapper.purchaseOrderReleasedTimestamp = parentOI.Purchase_Order__r.Released_Timestamp__c;
            oiWrapper.retailPurchaseOrderStatus = parentOI.Retail_Purchase_Order__r.Status__c;
            oiWrapper.retailPurchaseOrderName = parentOI.Retail_Purchase_Order__r.Retail_Purchase_Order_Name__c;
            oiWrapper.retailPurchaseOrderNameOPL = parentOI.Retail_Purchase_Order__r.Rpo_Name_For_OPL__c;
            oiWrapper.retailPurchaseOrderReleasedTimestamp = parentOI.Retail_Purchase_Order__r.Released_Timestamp__c;
            oiWrapper.configId = parentOI.Config_ID__c;

            // Iterate through the child OI Map (if applicable)
            // and add those records to the Wrapper
            List<OrderItemWrapper> childOIWList = new List<OrderItemWrapper>();
            if (parentToChildOIMap.containsKey(parentOI.Id)) {
                for (OrderItem childOI : parenttoChildOIMap.get(parentOI.Id).values()) {
                    OrderItemWrapper childOIWrapper = new OrderItemWrapper();
                    childOIWrapper.id = childOI.Id;
                    childOIWrapper.unitId = childOI.Unit_Id__c;
                    childOIWrapper.productName = childOI.Product2.Name;
                    childOIWrapper.productId = childOI.Product2Id;
                    childOIWrapper.productRecordTypeId = childOI.Product2.RecordTypeId;
                    childOIWrapper.isMasterProductRecordType = (childOI.Product2.RecordTypeId == masterProductRTId);
                    childOIWrapper.productOpenInVisualforce = childOI.Product2.Open_In_Visualforce__c;
                    childOIWrapper.productPivotalId = childOI.Product2.Pivotal_Id__c;
                    childOIWrapper.productIsActive = childOI.Product2.IsActive;
                    childOIWrapper.purchaseOrder = childOI.Purchase_Order__c;
                    childOIWrapper.retailPurchaseOrder = childOI.Retail_Purchase_Order__c;
                    childOIWrapper.quantity = childOI.Quantity;
                    childOIWrapper.totalRetailPrice = childOI.Total_Retail_Price__c;
                    childOIWrapper.unitWholesaleCost = childOI.Unit_Wholesale_Cost__c;
                    childOIWrapper.totalWholesaleCost = childOI.Total_Wholesale_Cost__c;
                    childOIWrapper.variantNumber = childOI.Variant_Number__c;
                    childOIWrapper.unitPrice = childOI.UnitPrice;
                    childOIWrapper.service = childOI.Service__c;
                    childOIWrapper.status = childOI.Status__c;
                    childOIWrapper.purchaseOrderStatus = childOI.Purchase_Order__r.Status__c;
                    childOIWrapper.purchaseOrderName = childOI.Purchase_Order_Name__c;
                    childOIWrapper.purchaseOrderReleasedTimestamp = childOI.Purchase_Order__r.Released_Timestamp__c;
                    childOIWrapper.retailPurchaseOrderStatus = childOI.Retail_Purchase_Order__r.Status__c;
                    childOIWrapper.retailPurchaseOrderName = childOI.Retail_Purchase_Order__r.Retail_Purchase_Order_Name__c;
                    childOIWrapper.retailPurchaseOrderNameOPL = childOI.Retail_Purchase_Order__r.Rpo_Name_For_OPL__c;
                    childOIWrapper.retailPurchaseOrderReleasedTimestamp = childOI.Retail_Purchase_Order__r.Released_Timestamp__c;
                    childOIWrapper.configId = childOI.Config_ID__c;
                    childOIWList.add(childOIWrapper);
                }
                oiWrapper.miscCharges = childOIWList;
            }
            oiWrapper.miscChargesCount = childOIWList.size();
            oiWrapperList.add(oiWrapper);
        }
        System.debug('The oiWrapperList value: '+oiWrapperList);
        return oiWrapperList;
        /////////////////////////////////////////////////////////////////////////////////
        // query that does not include wholesale cost or total wholesale cost.
        } else{
            system.debug('hit no unit wholesale cost code block');
        for (OrderItem oi : [SELECT Id,
                                    Parent_Order_Item__c,
                                    Purchase_Order__c,
                                    Purchase_Order_Name__c,
                                    Purchase_Order__r.Status__c,
                                    Purchase_Order__r.Released_Timestamp__c,
                                    Product2Id,
                                    Product2.Name,
                                    Product2.IsActive,
                                    Product2.Pivotal_Id__c,
                                    Product2.RecordTypeId,
                                    Product2.Open_In_Visualforce__c,
                                    Quantity,
                                    Retail_Purchase_Order__c,
                                    Retail_Purchase_Order__r.Name,
                                    Retail_Purchase_Order__r.Retail_Purchase_Order_Name__c,
                                    Retail_Purchase_Order__r.Rpo_Name_For_OPL__c,
                                    Retail_Purchase_Order__r.Status__c,
                                    Retail_Purchase_Order__r.Released_Timestamp__c,
                                    Service__c,
                                    Status__c,
                                    Total_Retail_Price__c,
                                    UnitPrice,
                                    Unit_Id__c,
                                    Variant_Number__c,
                                    Config_ID__c
                             FROM   OrderItem
                             WHERE  OrderId = :orderId
                             AND Status__c != 'Cancelled'
                             ORDER BY Unit_Id__c]) {
            
            // If a Parent_Order_Item__c is null this is a Parent record.
            if (oi.Parent_Order_Item__c == null) {
                parentOIMap.put(oi.Id, oi);
            }
            else {
                if (!parentToChildOIMap.containsKey(oi.Parent_Order_Item__c)) {
                    parentToChildOIMap.put(oi.Parent_Order_Item__c, new Map<Id,OrderItem>());
                }
                parentToChildOIMap.get(oi.Parent_Order_Item__c).put(oi.Id,oi);
            }

        }

        // Iterate through the Maps and build the return "wrapper" List.
        for (OrderItem parentOI : parentOIMap.values()) {
            OrderItemWrapper oiWrapper = new OrderItemWrapper();
            oiWrapper.id = parentOI.Id;
            oiWrapper.unitId = parentOI.Unit_Id__c;
            oiWrapper.productName = parentOI.Product2.Name;
            oiWrapper.productId = parentOI.Product2Id;
            oiWrapper.productRecordTypeId = parentOI.Product2.RecordTypeId;
            oiWrapper.isMasterProductRecordType = (parentOI.Product2.RecordTypeId == masterProductRTId);
            oiWrapper.productOpenInVisualforce = parentOI.Product2.Open_In_Visualforce__c;
            oiWrapper.productPivotalId = parentOI.Product2.Pivotal_Id__c;
            oiWrapper.productIsActive = parentOI.Product2.IsActive;
            oiWrapper.purchaseOrder = parentOI.Purchase_Order__c;
            oiWrapper.retailPurchaseOrder = parentOI.Retail_Purchase_Order__c;
            oiWrapper.quantity = parentOI.Quantity;
            oiWrapper.totalRetailPrice = parentOI.Total_Retail_Price__c;
            oiWrapper.variantNumber = parentOI.Variant_Number__c;
            oiWrapper.unitPrice = parentOI.UnitPrice;
            oiWrapper.service = parentOI.Service__c;
            oiWrapper.status = parentOI.Status__c;
            oiWrapper.purchaseOrderStatus = parentOI.Purchase_Order__r.Status__c;
            oiWrapper.purchaseOrderName = parentOI.Purchase_Order_Name__c;
            oiWrapper.purchaseOrderReleasedTimestamp = parentOI.Purchase_Order__r.Released_Timestamp__c;
            oiWrapper.retailPurchaseOrderStatus = parentOI.Retail_Purchase_Order__r.Status__c;
            oiWrapper.retailPurchaseOrderName = parentOI.Retail_Purchase_Order__r.Retail_Purchase_Order_Name__c;            
            oiWrapper.retailPurchaseOrderNameOPL = parentOI.Retail_Purchase_Order__r.Rpo_Name_For_OPL__c;
            oiWrapper.retailPurchaseOrderReleasedTimestamp = parentOI.Retail_Purchase_Order__r.Released_Timestamp__c;
            oiWrapper.configId = parentOI.Config_ID__c;            

            // Iterate through the child OI Map (if applicable)
            // and add those records to the Wrapper
            List<OrderItemWrapper> childOIWList = new List<OrderItemWrapper>();
            if (parentToChildOIMap.containsKey(parentOI.Id)) {
                for (OrderItem childOI : parenttoChildOIMap.get(parentOI.Id).values()) {
                    OrderItemWrapper childOIWrapper = new OrderItemWrapper();
                    childOIWrapper.id = childOI.Id;
                    childOIWrapper.unitId = childOI.Unit_Id__c;
                    childOIWrapper.productName = childOI.Product2.Name;
                    childOIWrapper.productId = childOI.Product2Id;
                    childOIWrapper.productRecordTypeId = childOI.Product2.RecordTypeId;
                    childOIWrapper.isMasterProductRecordType = (childOI.Product2.RecordTypeId == masterProductRTId);
                    childOIWrapper.productOpenInVisualforce = childOI.Product2.Open_In_Visualforce__c;
                    childOIWrapper.productPivotalId = childOI.Product2.Pivotal_Id__c;
                    childOIWrapper.productIsActive = childOI.Product2.IsActive;
                    childOIWrapper.purchaseOrder = childOI.Purchase_Order__c;
                    childOIWrapper.retailPurchaseOrder = childOI.Retail_Purchase_Order__c;
                    childOIWrapper.quantity = childOI.Quantity;
                    childOIWrapper.totalRetailPrice = childOI.Total_Retail_Price__c;
                    childOIWrapper.variantNumber = childOI.Variant_Number__c;
                    childOIWrapper.unitPrice = childOI.UnitPrice;
                    childOIWrapper.service = childOI.Service__c;
                    childOIWrapper.status = childOI.Status__c;
                    childOIWrapper.purchaseOrderStatus = childOI.Purchase_Order__r.Status__c;
                    childOIWrapper.purchaseOrderName = childOI.Purchase_Order_Name__c;
                    childOIWrapper.purchaseOrderReleasedTimestamp = childOI.Purchase_Order__r.Released_Timestamp__c;
                    childOIWrapper.retailPurchaseOrderStatus = childOI.Retail_Purchase_Order__r.Status__c;
                    childOIWrapper.retailPurchaseOrderName = childOI.Retail_Purchase_Order__r.Retail_Purchase_Order_Name__c;
                    childOIWrapper.retailPurchaseOrderNameOPL = childOI.Retail_Purchase_Order__r.Rpo_Name_For_OPL__c;
                    childOIWrapper.retailPurchaseOrderReleasedTimestamp = childOI.Retail_Purchase_Order__r.Released_Timestamp__c;
                    childOIWrapper.configId = childOI.Config_ID__c;
                    childOIWList.add(childOIWrapper);
                }
                oiWrapper.miscCharges = childOIWList;
            }
            oiWrapper.miscChargesCount = childOIWList.size();
            oiWrapperList.add(oiWrapper);
        }
        System.debug('The oiWrapperList value: '+oiWrapperList);
        return oiWrapperList;            
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    } // End Method: getOrderItems()

    /**
    * @author Jason Flippen
    * @description Updates OrderItem records to a Status of "Cancelled".
    * @param List of OrderItem Ids.
    * @returns Result of the update. True if successful, False if unsuccessful.
    */ 
    @AuraEnabled()
    public static String cancelOrderItems(List<Id> orderItemIdList,
                                          String cancellationReason) {

        String returnValue = '';

        // Build the list of OrderItems to be updated to "Cancelled".
        // updating method to also cancel out retail purchase orders 7-18-19 Mark Rothermal
        List<OrderItem> updateOIList = new List<OrderItem>();
        for (Id orderItemId : orderItemIdList) {
            OrderItem updateOI = new OrderItem(Id = orderItemId,
                                               Status__c = 'Cancelled',
                                               Cancellation_Reason__c = cancellationReason,
                                               Purchase_Order__c = null,
                                               Retail_Purchase_Order__c = null);
            updateOIList.add(updateOI);
        }

        // If we have any records to update, update them.
        if (!updateOIList.isEmpty()) {
            try {
                update updateOIList;
            }
            catch (Exception ex) {
                System.debug('OrderProductListController.cancelOrder Exception: ' + ex.getMessage());
                returnValue = ex.getMessage();
            }
        }

        return returnValue;

    } // End Method: cancelOrderItems()

    /**
    * @description if revenue has been reconized - hide the add product button.
    * @author Dinesh | 1/17/2020 
    * @param orderId 
    * @return boolean 
    **/
    @AuraEnabled
    public static boolean showProductButton(id orderId){
        boolean showButton=true;
        Id coroRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('ARO Record Type').getRecordTypeId();
        Id aroRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Record Type').getRecordTypeId();
        Id patioDoorOnlyRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Patio Door Only').getRecordTypeId();

        String profileName = [select Id,Name from profile where id =: UserInfo.getProfileId()].Name;
        Set<Id> recordTypes = new Set<Id>();
        recordTypes.add(aroRecordTypeId);
        recordTypes.add(coroRecordTypeId);
        Order getRevRec = new Order();
        getRevRec = [Select id,Revenue_Recognized_Date__c,RecordTypeId from Order where id=:orderId];
        if(getRevRec.Revenue_Recognized_Date__c != null && recordTypes.contains(getRevRec.RecordTypeId) && profileName != 'Super Administrator' && profileName != 'RMS-Data Integration'){
            system.debug('inside if statement for revenue recieved');
            showButton=false;
        }
        if(getRevRec.RecordTypeId == patioDoorOnlyRecordTypeId){
            system.debug('inside if statement for patio door only');
            showButton=false;
        }
        system.debug('@@@@@showButton'+showButton);
        return showButton;        
    }

    /**
    * @description  If order is a patio only record type, hide the entire component.
    * @author mark.rothermal@andersencorp.com | 1/17/2020 
    * @param orderId 
    * @return boolean 
    **/
    @AuraEnabled
    public static boolean showOrderList(id orderId){
        boolean showComponent=true;

        Id patioDoorOnlyRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Patio Door Only').getRecordTypeId();

        Order currentOrder = new Order();
        currentOrder = [Select id,RecordTypeId from Order where id=:orderId];
        if(currentOrder.RecordTypeId == patioDoorOnlyRecordTypeId){
            system.debug('inside if statement for patio door only');
            showComponent=false;
        }
        system.debug('show order product list = '+showComponent);
        return showComponent;        
    }


    /**
    * @author Jason Flippen
    * @description Class to combine (wrap) Parent and Child OrderItem records.
    * updated 7-18-19 added retail purchase order fields. mark rothermal
    */
    @TestVisible
    public class OrderItemWrapper {

        @TestVisible
        @AuraEnabled public String id {get;set;}
        @AuraEnabled public String unitId {get;set;}
        @AuraEnabled public String productName {get;set;}
        @AuraEnabled public String productId {get;set;}
        @AuraEnabled public String productRecordTypeId {get;set;}
        @AuraEnabled public Boolean isMasterProductRecordType {get;set;}
        @AuraEnabled public Boolean productOpenInVisualforce {get;set;}
        @AuraEnabled public String productPivotalId {get;set;}
        @AuraEnabled public Boolean productIsActive {get;set;}
        @AuraEnabled public String purchaseOrder {get;set;}
        @AuraEnabled public String retailPurchaseOrder {get;set;}
        @AuraEnabled public Decimal quantity {get;set;}
        @AuraEnabled public Decimal totalRetailPrice {get;set;}
        @AuraEnabled public Decimal unitWholesaleCost {get;set;}
        @AuraEnabled public Decimal totalWholesaleCost {get;set;}
        @AuraEnabled public String variantNumber {get;set;}
        @AuraEnabled public Decimal unitPrice {get;set;}
        @AuraEnabled public Boolean service {get;set;}
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public String purchaseOrderStatus {get;set;}
        @AuraEnabled public String purchaseOrderName {get; set;}
        @AuraEnabled public Datetime purchaseOrderReleasedTimestamp {get;set;}
        @AuraEnabled public String retailPurchaseOrderStatus {get;set;}
        @AuraEnabled public String retailPurchaseOrderName {get; set;}
        @AuraEnabled public String retailPurchaseOrderNameOPL {get; set;}       
        @AuraEnabled public Datetime retailPurchaseOrderReleasedTimestamp {get;set;}
        @AuraEnabled public Decimal miscChargesCount {get;set;}
        @AuraEnabled public List<OrderItemWrapper> miscCharges {get;set;}
        @AuraEnabled public String configId {get;set;}

    } // End (Wrapper) Class

    @AuraEnabled(Cacheable=true)
    public static List<string> getCancellationReasonOptions(){
        String objectName = 'OrderItem';
        String fieldName ='Cancellation_Reason__c';
        List<String> cancelOptions = new List<String>();
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            cancelOptions.add(pickListVal.getValue());
        } 
        return cancelOptions;
    }

}