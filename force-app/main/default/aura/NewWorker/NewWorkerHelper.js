({
	//clears all input fields
	clearFields : function(component, event) {
		var firstName = component.find("firstName").set("v.value", "");
        var lastName = component.find("lastName").set("v.value", "");
        var email = component.find("email").set("v.value", "");
        var phone = component.find("phone").set("v.value", "");
        var tech = component.find('techMeasure').set("v.value", false);
        var sales = component.find('salesRep').set("v.value", false);
        var installer = component.find('installer').set("v.value", false);
        $A.get('e.force:refreshView').fire();
	},

	//displays error message based on the message variable passed in
	failMessage : function(component, event, helper, message) {
		var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Failed!",
                "message": message,
                "type": "error",
                });
                toastEvent.fire();
    },
})