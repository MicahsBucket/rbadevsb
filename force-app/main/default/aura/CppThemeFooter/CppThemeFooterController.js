({
    loadContact:function(component,event,helper)
    {
        console.log("____footer event called_________"); 
        var action=component.get("c.loadFooterDetails");
        action.setCallback(this,function(response)
                           {
                               if(response.getState()=="SUCCESS")
                               {
                                   var footerResponse=response.getReturnValue();  
                                   if(footerResponse.contactRecord!=undefined && footerResponse.contactRecord.MobilePhone!=undefined)
                                   {
                                       footerResponse.contactRecord.MobilePhone=helper.formatPhoneNumber(footerResponse.contactRecord.MobilePhone);
                                   }
                                   else if(footerResponse.contactRecord!=undefined && footerResponse.contactRecord.HomePhone!=undefined)
                                   {
                                       footerResponse.contactRecord.MobilePhone=helper.formatPhoneNumber(footerResponse.contactRecord.HomePhone);
                                   }
                                   component.set("v.contactRecord",footerResponse.contactRecord);
                                   component.set("v.orderRecord",footerResponse.orderRecord);
                                   component.set("v.isInstallationComplete",footerResponse.isInstallationComplete); 
                                   component.set("v.isSurveySent",footerResponse.isSurveySent); 
                               }
                           });	
        $A.enqueueAction(action);
    },
	closeModel : function(component, event, helper) {
        /*component.find("curalateDiv").destroy();*/
                component.set("v.openCuralate", false);
                component.set("v.openRBAForm",false);
                component.set("v.rbaFormResponse",null);
        window.crl8.destroyExperience('homepage');
                
    },
    /*afterCuralateScriptLoaded:function(component,event,helper)
    {
    	console.log("_____curalate script loaded_______");    
    },*/
    callCuralate:function(component,event,helper)
    {
        component.set("v.openCuralate", true);
    },
    /*Model:function(component,event,helper)
    {
        window.crl8.ready(function() {
                        window.crl8.createExperience('homepage');
});
    },*/
    connectRbaForm:function(component,event,helper)
    {
        component.set("v.openRBAForm",true);
        /*var action=component.get("c.submitRBAFrom");
        action.setCallback(this,function(response)
                           {
                               if(response.getState()=="SUCCESS")
                               {
                                   var rbaResponse=response.getReturnValue();  
                                   component.set("v.rbaFormResponse",rbaResponse);
                                   console.log("_____rbaResponse____"+JSON.stringify(rbaResponse));
                               }
                           });	
        $A.enqueueAction(action);*/
    },
    sendRequestToRBA : function(component,event,helper)
    {
        var isValidPattern = true;  
        var zipCodePattern=/^((\d{5})|([A-Z]\d[A-Z]\s\d[A-Z]\d))$/;
        var zipCodeField=component.find("zipCode");
        var zipCodeValue=zipCodeField.get("v.value");
        isValidPattern=helper.patternMatcher(zipCodeField,zipCodeValue,zipCodePattern,isValidPattern);
        
        var phonePattern=/^\d{3}[-]\d{3}[-]\d{4}$/;
        var phoneField=component.find("phone");
        var phoneValue=phoneField.get("v.value");
        isValidPattern=helper.patternMatcher(phoneField,phoneValue,phonePattern,isValidPattern);
        
        var emailPattern=/^(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*;\s*|\s*$))*$/;
        var emailField=component.find("email");
        var emailValue=emailField.get("v.value");
        isValidPattern=helper.patternMatcher(emailField,emailValue,emailPattern,isValidPattern);
        
        //Validating all input fields together by providing the same aureid 'field' 
        let isAllValid = component.find('recordName').reduce(function(isValidSoFar, inputCmp){
            //display the error messages
            inputCmp.showHelpMessageIfInvalid();
            //check if the validity condition are met or not.
            return isValidSoFar && inputCmp.get('v.validity').valid;
        },true);
        if(!isAllValid)
        {
            isValidPattern=false;
        }
        if(isValidPattern) {
            var contactForm=component.get("v.contactRecord");
            var rbaButton=component.find("rbaButton");
            rbaButton.set("v.label","Processing... please wait")
            var phoneValue=phoneField.get("v.value"); 
            console.log("_____contactform_____"+JSON.stringify(contactForm));
            var action=component.get("c.submitRBAFrom");    
            action.setParams({
                "contactRecord":JSON.stringify(contactForm)
            });
            action.setCallback(this,function(response)
                               {
                                   if(response.getState()=="SUCCESS")
                                   {
                                       var rbaResponse=response.getReturnValue();  
                                       component.set("v.rbaFormResponse",rbaResponse);
                                       console.log("_____rbaResponse____"+JSON.stringify(rbaResponse));
                                   }
                               });	
            $A.enqueueAction(action);
        }
    }
})