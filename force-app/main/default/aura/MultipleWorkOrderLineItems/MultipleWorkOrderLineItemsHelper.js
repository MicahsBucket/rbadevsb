({
    executeAction : function(component, actionName, params) {
        return new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get(actionName);

            action.setParams(params);
            action.setCallback(this, function(response) {
                if (component.isValid() && response.getState() === 'SUCCESS') {
                    resolve(response.getReturnValue());
                } else {
                    console.log(response.getError()[0]);
                    reject(response.getError()[0]);
                }
            });
            $A.enqueueAction(action);
        }));
    },

    rowUp: function (component, rowIndex, rowData, action) {
        var data = component.get('v.data');
        var listLength = data.length;

        if(listLength > 1 && rowIndex > 0){
            var currentRow = data.splice(rowIndex, 1);
            data.splice(rowIndex - 1, 0, currentRow[0]);

            //Set List_Index__c
            data[rowIndex - 1].List_Index__c = (rowIndex);
            data[rowIndex ].List_Index__c = (rowIndex + 1);
            component.set('v.data', data);
        }
    },

    rowDown: function (component, rowIndex, rowData, action) {
        var data = component.get('v.data');
        var listLength = data.length;
        
        if(listLength > 1 && rowIndex < (listLength - 1)){
            var currentRow = data.splice(rowIndex, 1);
            data.splice(rowIndex + 1, 0, currentRow[0]);

            //Set List_Index__c
            data[rowIndex].List_Index__c = (rowIndex + 1);
            data[rowIndex + 1].List_Index__c = (rowIndex + 2);
            component.set('v.data', data);
        }
    },

    deleteRow: function (component, rowIndex, rowData, action) {
        component.set('v.DMLstatusMessage', 'Deleting item, please wait...');

        if(rowData.Id == '' || rowData.Id == null){
            var data = component.get('v.data');
            var deletedRow = data.splice(rowIndex, 1);

            //reorder List_Index__c
            data.forEach(function(row, index){
                row.List_Index__c = index + 1;
            });
            component.set('v.data', data);
            component.set('v.DMLstatusMessage', 'Success! Row Deleted.');
        } else {
            component.set('v.showSpinner',true);
            this.executeAction(component, 'c.deleteLineItem', {
                woliId : rowData.Id
            }).then($A.getCallback(function(didSucceed) {
                if(didSucceed == true){
                    var data = component.get('v.data');
                    var deletedRow = data.splice(rowIndex, 1);

                    //reorder List_Index__c
                    data.forEach(function(row, index){
                        row.List_Index__c = index + 1;
                    });
                    component.set('v.data', data);
                    component.set('v.DMLstatusMessage', 'Success! Row Deleted.');
                } else {
                    component.set('v.DMLstatusMessage', 'Failed To Delete Row. Please Try again.')
                }
            })).finally($A.getCallback(function() {
                component.set('v.showSpinner',false);
            }));
        }
    },

    setStatus: function (component, rowIndex, action) {
        var data = component.get('v.data');
        data[rowIndex].Status = action.name;
        component.set('v.data', data);
    },

    toggleType: function (component, row) {
        var data = component.get('v.data');
        data = data.map(rowData => {
            if (rowData.List_Index__c == row.List_Index__c) {
                switch(row.Line_Item_Type__c) {
                    case 'Check In':
                        rowData.Line_Item_Type__c = 'Check Out';
                        break;
                    case 'Check Out':
                        rowData.Line_Item_Type__c = 'None';
                        break;
                    case 'None':
                        rowData.Line_Item_Type__c = 'Check In';
                        break;
                    default:
                        break;
                }
            }
            return rowData
        });
        component.set("v.data", data);
    }
})