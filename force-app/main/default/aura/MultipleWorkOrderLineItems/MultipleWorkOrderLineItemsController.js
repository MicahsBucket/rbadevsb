({
	init: function (component, event, helper) {
        var rowActions = [];

        //Intialize columns
        var columns = [ 
                        {type: 'button-icon', fixedWidth: 40, typeAttributes: {
                            iconName: 'utility:chevronup',
                            name: 'row_up',
                            title: 'Click to Move Row Up'}},
                        {type: 'button-icon', fixedWidth: 40, typeAttributes: {
                            iconName: 'utility:chevrondown',
                            name: 'row_down',
                            title: 'Click to Move Row Down'}},
                        {label: 'Subject', fieldName: 'Subject', type: 'text', editable: true},
                        {label: 'Office Notes', fieldName: 'Description', type: 'text', editable: true},
                        {label: 'Job Notes', fieldName: 'Job_Notes__c', type: 'text', editable: true},
                        {label: 'Customer-Facing', fieldName: 'visibleToClient', type: 'boolean', initialWidth: 155, editable: true},
                        {label: 'Line Type', type: 'button', initialWidth: 120, typeAttributes: 
                        	{label: { fieldName: 'Line_Item_Type__c'}, 
                        	name: 'toggle_type', 
                        	title: 'Toggle between Check-In and Check-Out types'}},
                        {label: 'Status', fieldName: 'Status', type: 'text', initialWidth: 140, cellAttributes: { alignment: 'center' }},
                        {type: 'action', fixedWidth: 35, cellAttributes: { alignment: 'left' }, typeAttributes: { rowActions: rowActions }},
                        {type: 'button-icon', fixedWidth: 65, cellAttributes: { alignment: 'right' }, typeAttributes: {
                            iconName: 'utility:delete',
                            name: 'row_delete',
                            title: 'Click to remove WOLI'}},
                        ];

        component.set('v.columns', columns);

        Promise.all([
            helper.executeAction(component, 'c.getWOLIStatuses', {}),
            helper.executeAction(component, 'c.getWorkOrderLineItems', {workOrderId : component.get('v.recordId')})
        ]).then($A.getCallback(function(results) {
            
            var woliStatuses = results[0];
            var woliList = results[1];

            // First, deal with WOLI statuses
            woliStatuses.forEach(function(row){
                rowActions.push({'label': row, 'name': row});
            });
            component.set('v.statusActions', rowActions);

            // Second, deal with WOLIs
            if(woliList != null){
                woliList.forEach(function(row){
                    row.linkName = '/' + row.Id;
                    if (row.Visibility__c == 'Customer-Facing') {
                    	row.visibleToClient = true;
                    } else {
                    	row.visibleToClient = false;
                    }

                    if (!row.Line_Item_Type__c) {
                        row.Line_Item_Type__c = 'None';
                    }
                });
                component.set('v.data', woliList);
            }

            component.set('v.showSpinner',false);
        }));

    },

    cancelChanges: function (component, event, helper) {

    },

    addRow: function (component, event, helper) {
        var data = component.get('v.data');
        var listLength = data.length + 1;
        var workOrderNumber = component.get('v.workOrderNumber');
        var recordId = component.get('v.recordId');
        var newRow = {WorkOrderId: recordId,
                        WorkOrderNumber: workOrderNumber,
                        List_Index__c: listLength,
                        LineItemNumber: "(New Line Item)",
                        Subject: "add subject ...",
                        Description: "",
                        Job_Notes__c: "",
                        Status: "New",
                        Line_Item_Type__c: "Check In",
                    	visibleToClient: false};
        data.push(newRow);
        component.set('v.data', data);
    },

    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var rowData = event.getParam('row');
        var rowIndex = (component.get('v.data')).indexOf(rowData);

        switch (action.name) {
            case 'row_up':
                helper.rowUp(component, rowIndex, rowData, action);
                break;
            case 'row_down':
                helper.rowDown(component, rowIndex, rowData, action);
                break;
            case 'row_delete':
                helper.deleteRow(component, rowIndex, rowData, action);
                break;
            case 'New':
            case 'In Progress':
            case 'On Hold':  
            case 'Completed':
            case 'Closed':  
            case 'Cannot Complete':  
            case 'Canceled':  
            case 'Cancelled':  
                helper.setStatus(component, rowIndex, action);
                break;
            case 'toggle_type':
                helper.toggleType(component, rowData);
            default:
                // helper. or do nothing
                break;
        }
    },

    saveChanges: function (component, event, helper) {

        //component.set('v.draftValues', event.getParam('draftValues'));
        //var draftValues = component.get('v.draftValuesAttribute');
        var draftValues = component.find('datatable').get('v.draftValues')
        var data = component.get('v.data');

        // Add Draft values to data
        if(draftValues != null){
            draftValues.forEach(function(row, index){
                var rowIdIndex = row.id.slice(4, row.id.length);
                for(var x in row){
                    if(x != 'id'){
                        data[rowIdIndex][x] = row[x];
                    }
                }
            });
        }

        data.forEach(function(row){
        	if (row.visibleToClient) {
        		row.Visibility__c = 'Customer-Facing';
        	} else {
        		row.Visibility__c = 'Internal';
        	}
            delete row.visibleToClient;
            
            if (row.Line_Item_Type__c === 'None') {
                row.Line_Item_Type__c = null;
            }
        });

        if(component.get('v.data').length > 0){
            component.set('v.DMLstatusMessage', 'Loading: please wait...');
            component.set('v.showSpinner',true);

            helper.executeAction(component, 'c.setWorkOrderLineItems', {
                workOrderId : component.get('v.recordId'),
                woliDetails : JSON.stringify(data)
            }).then($A.getCallback(function(result) {
                if(result.isSuccess){
                    // If Success
                    var data = result.updatedData;
                    if(data != null){
                        data.forEach(function(row){
                            row.linkName = '/' + row.Id;
                            if (row.Visibility__c == 'Customer-Facing') {
		                    	row.visibleToClient = true;
		                    } else {
		                    	row.visibleToClient = false;
                            }
                            
                            if (!row.Line_Item_Type__c) {
                                row.Line_Item_Type__c = 'None';
                            }
                        });
                        component.set('v.data', data);
                    }
                    component.find('datatable').set('v.draftValues',null);
                    component.set('v.DMLstatusMessage', 'Success! Changes saved.');
                } else {
                    // If Fail
                    component.set('v.DMLstatusMessage', 'Error: Line items could not be saved. Please try again');
                }
            })).finally($A.getCallback(function() {
                component.set('v.showSpinner',false);
            }));
        }
        component.set('v.draftValues', []);
    }
})