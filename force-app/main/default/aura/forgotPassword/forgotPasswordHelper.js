({
    qsToEventMap: {
        'expid'  : 'e.c:setExpId'
    },
    
    handleForgotPassword: function (component, event, helpler) {
        var username = component.find("username").get("v.value"); 
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(username!=undefined&&mailformat.test(username)&&!username.includes('.myproject'))
        {
            username+='.myproject';
        }
        console.log("_____username______"+username);
	    var checkEmailUrl = component.get("v.checkEmailUrl");
        console.log("_____checkEmailUrl______"+checkEmailUrl);        
        var action = component.get("c.forgotPassword");
        action.setParams({username:username, checkEmailUrl:checkEmailUrl});
        action.setCallback(this, function(a) {
            var rtnValue = a.getReturnValue();
            console.log("_______rtnValue_________"+JSON.stringify(rtnValue));
            if (rtnValue != null) {
               component.set("v.errorMessage",rtnValue);
               component.set("v.showError",true);
            }
       });
       $A.enqueueAction(action); 
    },

    setBrandingCookie: function (component, event, helpler) {
        var expId = component.get("v.expid");
        if (expId) {
            var action = component.get("c.setExperienceId");
            action.setParams({expId:expId});
            action.setCallback(this, function(a){ });
            $A.enqueueAction(action);
        }
    }
})