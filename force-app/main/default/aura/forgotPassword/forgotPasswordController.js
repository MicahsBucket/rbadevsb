({
    handleForgotPassword: function (component, event, helpler) {
        helpler.handleForgotPassword(component, event, helpler);
    },
    handleForgotPassword2:function(component,event,helper)
    {
        var username = component.find("username").get("v.value"); 
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(username!=undefined&&mailformat.test(username)&&!username.includes('.myproject'))
        {
            username+='.myproject';
        }
        console.log("_____username______"+username);
        var checkEmailUrl = component.get("v.checkEmailUrl");
        console.log("_____checkEmailUrl______"+checkEmailUrl);         
    },
    onKeyUp: function(component, event, helpler){
    //checks for "enter" key
        if (event.getParam('keyCode')===13) {
            helpler.handleForgotPassword(component, event, helpler);
        }
    },
    appendMyProject:function(component,event,helper)
    {
        var username = component.find("username").get("v.value"); 
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(username.length>0&&username!=undefined&&mailformat.test(username)&&!username.includes('.myproject'))
        {
            username+='.myproject';
        }
        component.find("username").set("v.value",username); 
    },
    setExpId: function (component, event, helper) {
        var expId = event.getParam('expid');
        if (expId) {
            component.set("v.expid", expId);
        }
        helper.setBrandingCookie(component, event, helper);
    },

    initialize: function(component, event, helper) {
        $A.get("e.siteforce:registerQueryEventMap").setParams({"qsToEvent" : helper.qsToEventMap}).fire();
    }
})