({
    init : function(component, event, helper) {
        var recordId = component.get('v.recordId');

        Promise.all([
            helper.executeAction(component, 'c.getWorkOrder', { workOrderId: recordId }),
            helper.executeAction(component, 'c.getOrCreateServiceAppointment', { workOrderId: recordId }),
            helper.executeAction(component, 'c.getServiceTerritoriesForCurrentUser', { workOrderId: recordId }),
            helper.executeAction(component, 'c.getFSLSettings', {})
        ]).then($A.getCallback(function(results) {
            var workOrder = results[0],
                serviceAppointment = results[1],
                serviceTerritories = results[2],
                settings = results[3];

            component.set('v.workOrder', workOrder);
            component.set('v.resourcesNeeded', workOrder.Recommended_Crew_Size__c);
            component.set('v.serviceAppointment', serviceAppointment);
            component.set('v.selectedTerritoryId', workOrder.ServiceTerritoryId);
            component.set('v.duration', workOrder.Duration);
            component.set('v.serviceTerritories', serviceTerritories);
            component.set('v.settings', settings);
            component.set('v.formattedAddress', helper.formatAddress(serviceAppointment));
            component.set('v.rescheduling', serviceAppointment.Status === helper.SA_STATUS_SCHEDULED);

            component.set('v.showSpinner', false);
            component.set('v.loaded', true);
        }));
    },

    getCandidates : function(component, event, helper) {
        if (!helper.validate(component)) {
            return;
        }

        component.set('v.showSpinner', true);

        helper.saveAppointment(component)
            .then($A.getCallback(function() {
                helper.getCandidates(component)
                    .then($A.getCallback(function() {
                        component.set('v.showSpinner', false);
                        component.set('v.showCandidatesSpinner', false);
                        component.set('v.step', 1);
                    }));
            }))
            .catch($A.getCallback(function() {
                component.set('v.showSpinner', false);
                component.set('v.showCandidatesSpinner', false);
            }));
    },

    getTimeSlots : function(component, event, helper) {
        if (!helper.validate(component)) {
            return;
        }

        component.set('v.showSpinner', true);

        helper.getTimeSlots(component)
            .then($A.getCallback(function() {
                component.set('v.showSpinner', false);
                component.set('v.step', 2);
            }))
            .catch($A.getCallback(function() {
                component.set('v.showSpinner', false);
            }));
    },

    selectTimeSlot : function(component, event, helper) {
        var el = event.currentTarget,
            dayIndex = parseInt(el.dataset.day),
            slotIndex = parseInt(el.dataset.slot),
            daySlots = component.get('v.daySlots'),
            selectedSlot = daySlots[dayIndex].timeSlots[slotIndex];

        component.set('v.selectedTimeSlot', selectedSlot);

        component.set('v.showSpinner', true);

        if (component.get('v.selectedResourceIds').length > 1) {
            component.set('v.showSpinner', false);
            component.set('v.step', 3);
        } else {
            helper.schedule(component);
        }
    },

    schedule: function(component, event, helper) {
        component.set('v.showSpinner', true);
        helper.schedule(component);
    },

    onResourceChange : function(component, event, helper) {
        helper.updateSelectedResources(component);
    },

    onRadioChange: function(component, event, helper) {
        var name = event.currentTarget.id;
        var nameArray = name.split('-');
        var index = nameArray[1];
        var resources = component.get('v.selectedResources');
        var resource = resources[parseInt(index)];
        component.set('v.primaryResource', resource);
    },

    back: function(component) {
        var step = component.get('v.step');
        if (step === 3) {
            component.set('v.selectedResourceIds', []);
            component.set('v.primaryResource', null);
            component.set('v.selectedResources', []);
        }
        component.set('v.step', Math.max(0, step - 1));
    },

    handleEarliestStartTimeChange: function(component) {
        var startDate = component.get('v.serviceAppointment.EarliestStartTime'),
            endDate = component.get('v.serviceAppointment.DueDate');

        if (startDate > endDate) {
            component.set('v.serviceAppointment.DueDate',startDate);
        }
    }

})