({
    ADDRESS_COMPONENTS: ['Street', 'City', 'State', 'PostalCode', 'CountryCode'],

    SA_STATUS_NEW: 'None',
    SA_STATUS_SCHEDULED: 'Scheduled',

    saveAppointment: function(component) {
        var params = {
            appointment: component.get('v.serviceAppointment')
        };

        return this.executeAction(component, 'c.saveAppointmentWithoutRollup', params)
            .then($A.getCallback(function(serviceAppointment) {
                component.set('v.serviceAppointment', serviceAppointment);
                return serviceAppointment;
            }));
    },

    getCandidates: function(component) {
        var serviceAppointment = component.get('v.serviceAppointment'),
            params = {
                appointmentId : serviceAppointment.Id,
                startTime : serviceAppointment.EarliestStartTime,
                finish : serviceAppointment.DueDate,
                territoryId: component.get('v.selectedTerritoryId'),
                duration: component.get('v.duration')
            };

        return this.executeAction(component, 'c.getAvailableCandidatesForTerritory', params)
            .then($A.getCallback(function(candidates) {
                component.set('v.candidates', candidates);
            }))
            .catch($A.getCallback(function(error) {
                component.set('v.showSpinner', false);
                console.log(error.message);
            }));
    },

    getTimeSlots: function(component) {
        var helper = this,
            serviceAppointment = component.get('v.serviceAppointment'),
            resourcesNeeded = component.get('v.resourcesNeeded'),
            params = {
                workOrderId : component.get('v.recordId'),
                serviceAppointmentId : serviceAppointment.Id,
                resourceIds : component.get('v.selectedResourceIds'),
                startTime : serviceAppointment.EarliestStartTime,
                finish : serviceAppointment.DueDate,
                territoryId : component.get('v.selectedTerritoryId'),
                duration: component.get('v.duration')
            };

        return helper.executeAction(component, 'c.getTimeSlotsForCandidatesForTerritory', params)
            .then($A.getCallback(function(timeSlots) {
                component.set('v.timeSlots', timeSlots);
                if (timeSlots) {
                    var days = [],
                        slotDate,
                        previousDate;

                    component.set('v.appointmentTimeZone', timeSlots[0].appointmentTimeZone);
                    timeSlots.forEach(function(timeSlot) {

                        slotDate = new Date(timeSlot.start).getDate();

                        if (!previousDate || slotDate !== previousDate) {
                            days.push({
                                day: timeSlot.start,
                                timeSlots: [timeSlot]
                            });
                        } else {
                            days[days.length - 1].timeSlots.push(timeSlot);
                        }

                        previousDate = slotDate;
                    });

                    component.set('v.daySlots', days);
                }
            }))
            .catch($A.getCallback(function(error) {
                console.log(error.message);
            }));

    },

    createAppointments: function(component) {
        var helper = this,
            selectedTimeSlot = component.get('v.selectedTimeSlot'),
            resourceIds = component.get('v.selectedResourceIds'),
            params = {
                serviceAppointmentId: component.get('v.serviceAppointment').Id,
                startTime: selectedTimeSlot.start,
                endTime: selectedTimeSlot.finish,
                resourceIds: resourceIds,
                duration: component.get('v.duration')
            };

        return this.executeAction(component, 'c.createAppointmentsWithDuration', params)
            .then($A.getCallback(function(updatedAppointments) {
                return Promise.all(updatedAppointments.map(function(appointment) {
                    appointment.Scheduled_Via__c = 'Custom - Resources';
                    var saveParams = {
                        appointment: appointment
                    };

                    return helper.executeAction(component, 'c.saveAppointment', saveParams);
                }));
            }));
    },

    rescheduleAppointments: function(component) {
        var helper = this,
            selectedTimeSlot = component.get('v.selectedTimeSlot'),
            params = {
                workOrderId: component.get('v.workOrder').Id,
                startTime: selectedTimeSlot.start,
                endTime: selectedTimeSlot.finish,
                resourceIds: component.get('v.selectedResourceIds'),
                duration: component.get('v.duration')
            };

        return this.executeAction(component, 'c.rescheduleAppointmentsWithDuration', params)
            .then($A.getCallback(function(updatedAppointments) {
                return Promise.all(updatedAppointments.map(function(appointment) {
                    appointment.Scheduled_Via__c = 'Custom - Resources';
                    var saveParams = {
                        appointment: appointment
                    };
                    return helper.executeAction(component, 'c.saveAppointmentWithoutRollup', saveParams);
                }));
            }));
    },

    updateSelectedResources: function(component) {
        var selectedResources = [],
            selectedResourceIds = [],
            resourcesNeeded = component.get('v.resourcesNeeded'),
            resourceCmp = component.find('resource');

        if (Array.isArray(resourceCmp)) {
            selectedResourceIds = resourceCmp.filter(function(cmp) {
                return cmp.get('v.checked');
            }).map(function(cmp) {
                return cmp.get('v.value');
            });
        } else {
            var isChecked = resourceCmp.get('v.checked');
            if (isChecked) {
                selectedResourceIds.push(resourceCmp.get('v.value'));
            }
        }

        component.set('v.selectedResourceIds', selectedResourceIds);
        var candidates = component.get('v.candidates');

        candidates.forEach(function(resource) {
            if (selectedResourceIds.indexOf(resource.id) > -1) {
                selectedResources.push(resource);
            }
        });
        component.set('v.selectedResources', selectedResources);
    },

    assignResources: function(component) {
        var helper = this,
            appointments = component.get('v.createdAppointments'),
            resourceIds = component.get('v.selectedResourceIds'),
            assignedResources = [],
            promise = Promise.resolve();

        resourceIds.forEach(function(resourceId, index) {
            var params = {
                appointmentId: appointments[index].Id,
                resourceId: resourceId
            };

            promise = promise.then($A.getCallback(function() {
                return helper.executeAction(component, 'c.assignResource', params)
                    .then($A.getCallback(function(resource) {
                        assignedResources.push(resource);
                    }));
            }));
        });

        return promise.then($A.getCallback(function() {
            return assignedResources;
        }));
    },

    schedule: function(component) {
        var helper = this;
        if (!helper.validate(component)) {
            return;
        }

        if (component.get('v.rescheduling')) {
            helper.updateDurationOnWorkOrder(component)
                .then($A.getCallback(function() {
                    return helper.rescheduleAppointments(component);
                }))
                .then($A.getCallback(function(appointments) {
                    component.set('v.createdAppointments', appointments);
                }))
                .then($A.getCallback(function() {
                    return helper.assignResources(component);
                }))
                .then($A.getCallback(function(resources) {
                    component.set('v.assignedResources', resources);
                }))
                .then($A.getCallback(function() {
                    var primaryResource = component.get('v.primaryResource');
                    if (primaryResource) {
                        helper.setPrimaryResource(component, primaryResource.id);
                    }
                    component.set('v.showSpinner', false);
                    component.set('v.step', 4);
                }))
                .catch($A.getCallback(function(error) {
                    component.set('v.showSpinner', false);
                    component.set('v.scheduleErrorMessage', error.message);
                }));
        } else {
            helper.updateDurationOnWorkOrder(component)
                .then($A.getCallback(function() {
                    return helper.createAppointments(component);
                }))
                .then($A.getCallback(function(appointments) {
                    component.set('v.createdAppointments', appointments);

                    return helper.assignResources(component);
                }))
                .then($A.getCallback(function(resources) {
                    component.set('v.assignedResources', resources);
                }))
                .then($A.getCallback(function() {
                    var primaryResource = component.get('v.primaryResource');
                    if (primaryResource) {
                        helper.setPrimaryResource(component, primaryResource.id);
                    }
                    component.set('v.showSpinner', false);
                    component.set('v.step', 4);
                }))
                .catch($A.getCallback(function(error) {
                    component.set('v.showSpinner', false);
                    component.set('v.scheduleErrorMessage', error.message);
                }));
        }

    },

    updateDurationOnWorkOrder: function(component) {
        var params = {
                workOrderId: component.get('v.workOrder').Id,
                duration: component.get('v.duration')
        };
        return this.executeAction(component, 'c.updateWorkOrderDuration', params);
    },

    setPrimaryResource: function(component, resourceId) {
        var params = {
            workOrderId: component.get('v.recordId'),
            resourceId: resourceId
        };

        return this.executeAction(component, 'c.setPrimaryResource', params);
    },

    formatAddress: function(record) {
        var tokens = [];

        this.ADDRESS_COMPONENTS.forEach(function(component) {
            if (record[component]) {
                tokens.push(record[component]);
            }
        });

        return tokens.join(', ');
    },

    validate: function(component) {
        var helper = this,
            step = component.get('v.step'),
            valid = true;

        if (step === 0) {
            valid = component.find('formInput').reduce(function(validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return inputCmp.get('v.validity').valid && validSoFar;
            }, true);

            valid = component.find('dateInput').reduce(function(validSoFar, dateCmp) {
                return helper.validateDateInput(dateCmp) && validSoFar;
            }, valid);
        } else if (step === 1) {
            var resourcesNeeded = component.get('v.resourcesNeeded'),
                selectedResourceIds = component.get('v.selectedResourceIds');

            valid = selectedResourceIds.length != 0 && selectedResourceIds.length >= resourcesNeeded;
            component.set('v.resourcesError', !valid);
        }

        return valid;
    },

    validateDateInput: function(dateCmp) {
        var dateString = dateCmp.get('v.value'),
            valid = true;

        if (dateString) {
            var d = new Date(dateString);

            if (window.isNaN(d.getTime())) {
                dateCmp.set('v.errors', [{ message: 'Invalid date' }]);
                valid = false;
            } else {
                dateCmp.set('v.errors', null);
            }
        } else {
            if (dateCmp.get('v.required')) {
                dateCmp.set('v.errors', [{ message: 'Complete this field' }]);
                valid = false;
            } else {
                dateCmp.set('v.errors', null);
            }
        }

        return valid;
    },

    executeAction: function(component, actionName, params) {
        return new Promise(function(resolve, reject) {
            var action = component.get(actionName);

            action.setParams(params);
            action.setCallback(this, function(response) {
                if (component.isValid() && response.getState() === 'SUCCESS') {
                    resolve(response.getReturnValue());
                } else {
                    reject(response.getError()[0]);
                }
            });
            $A.enqueueAction(action);
        });
    }
})