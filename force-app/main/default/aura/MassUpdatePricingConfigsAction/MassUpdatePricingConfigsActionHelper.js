({
    enablePreviewButton : function(component) {

        var unitValue = component.get("v.unitValue");
        var trimValue = component.get("v.trimValue");
        var colorValue = component.get("v.colorValue");
        var glazingValue = component.get("v.glazingValue");
        var grilleValue = component.get("v.grilleValue");
        var hardwareValue = component.get("v.hardwareValue");
        var screenValue = component.get("v.screenValue");
        var increaseByValue = component.get("v.increaseByValue");
        var increaseDollarValue = component.get("v.increaseDollarValue");
        var increasePercentageValue = component.get("v.increasePercentageValue");

        // Check if the required fields have values and
        // enable/disable the Preview button accordingly.
        if ((unitValue == undefined || unitValue == '') &&
            (trimValue == undefined || trimValue == '') &&
            (colorValue == undefined || colorValue == '') &&
            (glazingValue == undefined || glazingValue == '') &&
            (grilleValue == undefined || grilleValue == '') &&
            (hardwareValue == undefined || hardwareValue == '') &&
            (screenValue == undefined || screenValue == '')) {
            component.set("v.disablePreview", true);
        }
        else {

            if (increaseByValue == 'increaseByDollar' &&
                increaseDollarValue != undefined &&
                increaseDollarValue != null &&
                increaseDollarValue != '') {
                component.set("v.disablePreview", false);
            }
            else if (increaseByValue == 'increaseByPercentage' &&
                    increasePercentageValue != undefined &&
                    increasePercentageValue != null &&
                    increasePercentageValue != '') {
                component.set("v.disablePreview", false);
            }
            else {
                component.set("v.disablePreview", true);
            }

        }

    }
})