/**
 * Created by lee-anneclarke on 12/13/18.
 */

({
    toggleSearch : function(component, event, helper) {
        var searchForm = component.find("cSearchForm");
        $A.util.toggleClass(searchForm, "slds-hide");
    }
});