({
    init : function(component, event, helper) {
        helper.getOrderDetails(component);
    },
    
    next : function(component, event, helper) {
        helper.nextStage(component);
    },    
    
    prev : function(component, event, helper) {
        helper.prevStage(component);
    },
    
    openModal : function(component, event, helper){
        console.log("Open the checklist modal");
        component.set("v.openChecklist", true);
    },

    closeModal :function(component, event, helper){
        component.set("v.openChecklist", false);
    },
    
    handleCollapse: function(component, event, helper) {
        var section = component.find("timeline_section_mobile");
        var button = component.find("collapseButton");
        $A.util.toggleClass(section, 'slds-is-open');
        $A.util.toggleClass(button, 'slds-is-active');
        component.set("v.buttonText", component.get("v.buttonText") == 'See full project timeline' ? 'Collapse project timeline' : 'See full project timeline');
    },
    
    
    initOrder : function(component, event, helper){
        var order = event.getParam("order");
        var orderId = event.getParam("orderId");
        var orderDate = event.getParam("orderDate");
        
        //set the default values
        component.set('v.stage', 1);
        component.set('v.currentStage', 1);
        component.set('v.totalStage', 5);
        component.set('v.stages', []);
        
        //set updated values which is pass through change project event
        component.set('v.orderId', orderId);
        component.set('v.order', order);
        component.set('v.orderStatus', order.Status);
        helper.getOrderDetails(component);
    }
})