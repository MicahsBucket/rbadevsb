({
    makeActive : function(component) {
        const stage = component.get("v.stage");
        const stages = component.get("v.stages");
        const timeline_line = component.find("timeline_line");
        const prev = component.find("prev");
        const next = component.find("next");
        const timeline_fill = component.find("timeline_fill");
        const text_wrapper = component.find("text_wrapper");
        
        
        let links = [];
        links = component.find("link");
        console.log("links", component.find("link"));
        if(links && links.length > 0) {
            links.forEach((link, index) => {                    
                if((index+1) < stage) {
                    $A.util.addClass(link, 'cd-h-timeline__date--completed');
                } else if( (index + 1) == stage) {                	
                    $A.util.addClass(link, 'cd-h-timeline__date--'+ component.get("v.currentStageType"));
                	$A.util.addClass(link, 'cd-h-timeline__date--current-stage');
                }
        	});
    	}
        $A.util.removeClass(next, 'cd-h-timeline__navigation--inactive');
        if(stage > 0) {
            for(let i = 0; i < 20; i++) {
                $A.util.removeClass(timeline_line, 'step_' + (i+1));
                $A.util.removeClass(timeline_fill, 's' + (i+1));
                $A.util.removeClass(text_wrapper, 's' + (i+1));
            }
             //alert(stage);
            $A.util.removeClass(prev, 'cd-h-timeline__navigation--inactive');
            $A.util.addClass(timeline_line, 'step_' + stage);
            $A.util.addClass(timeline_fill, 's' + stage);
            $A.util.addClass(text_wrapper, 's' + stage);

            component.set("v.stage", stage);
            this.stageIsInstallation(component); 

            // set onHold Stage
            var onHold = component.get('v.mileStoneDates.isOnHold');
            console.log('hold', onHold);
            if(onHold === true){
                stages[stage - 1].status = "ON HOLD";
                 stages[stage - 1].date = "";
                this.markHold(component, stage);
                component.set("v.onHold", true);
            } else {
                component.set("v.onHold", false);
            }
		}
    },
     
    // mark hold stage and make it orange
    markHold : function(component, stageNo) {
        let stage = component.get("v.stage");
        let links = [];
        links = component.find("link");
        if(links) {
            links.forEach((link, index) => {                    
                if((index+1) == stageNo) {
                    console.log("link:::", link)
                    $A.util.removeClass(link, 'cd-h-timeline__date--completed');
                    $A.util.removeClass(link, 'cd-h-timeline__date--pending');
                    $A.util.addClass(link, 'cd-h-timeline__date--hold');
                }
            });
        }
        component.set("v.stage", stage);
        
    }, 
    
    
    prevStage : function(component) {
        const timeline_line = component.find("timeline_line");
        const next = component.find("next");
        const prev = component.find("prev");
        const text_wrapper = component.find("text_wrapper");
        let stage = component.get("v.stage");
        const currentStage = component.get("v.currentStage");
        console.log("stage::" + stage);
        
        // Make the center stage's image bigger        
        let links = [];
        links = component.find("link");
        if(links && links.length > 0) {
          	links.forEach((link) => {                    
                $A.util.removeClass(link, 'cd-h-timeline__date--current-stage')
        	}); 
            if(stage >= 2) $A.util.addClass(links[stage - 2], 'cd-h-timeline__date--current-stage');
            else $A.util.addClass(links[0], 'cd-h-timeline__date--current-stage');
    	}
        
        // move timeline by 1 stage from right to left
        for(let i = stage + 1; i >= 0; i--) {
            $A.util.removeClass(timeline_line, 'step_' + i);
            $A.util.removeClass(text_wrapper, 's' + i);
        } 
        if((stage - 1) >= 1) {
            $A.util.removeClass(next, 'cd-h-timeline__navigation--inactive');
            $A.util.addClass(timeline_line, 'step_' + (stage - 1));
            $A.util.addClass(text_wrapper, 's' + (stage - 1));
            component.set("v.stage", stage - 1);
            this.stageIsInstallation(component); 
        }
        if (stage == 1) {
            $A.util.addClass(prev, 'cd-h-timeline__navigation--inactive');
        }
    },
        
        
    nextStage : function(component) {
        const timeline_line = component.find("timeline_line");
        const next = component.find("next");
        const prev = component.find("prev");
        const totalStage = component.get("v.totalStage");
    	const text_wrapper = component.find("text_wrapper");
        let stage = component.get("v.stage");
    	const currentStage = component.get("v.currentStage");        
        
        // Make the center stage's image bigger        
        let links = [];
        links = component.find("link");
        if(links && links.length > 0) {
          	links.forEach((link) => {                    
                $A.util.removeClass(link, 'cd-h-timeline__date--current-stage')
        	}); 
            if(stage < links.length) $A.util.addClass(links[stage], 'cd-h-timeline__date--current-stage');
            else $A.util.addClass(links[links.length - 1], 'cd-h-timeline__date--current-stage');
    	}
        
        
        // move timeline by 1 stage from right to left
        if(stage < totalStage) {
            for(let i = 0; i <= stage; i++) {
              	$A.util.removeClass(timeline_line, 'step_' + i);
                $A.util.removeClass(text_wrapper, 's' + i);
            }
            $A.util.addClass(timeline_line, 'step_' + (stage + 1));
            $A.util.addClass(text_wrapper, 's' + (stage + 1));
        	component.set("v.stage", stage + 1);
            $A.util.removeClass(prev, 'cd-h-timeline__navigation--inactive');   
            this.stageIsInstallation(component);          
        } else {
            $A.util.addClass(next, 'cd-h-timeline__navigation--inactive');
        }
    },
        
        
    createStageArray : function(component) {
        const today = $A.localizationService.formatDate(new Date().setHours(24),"YYYY-MM-DDTHH:MM:SS");
        const mileStones = component.get('v.mileStoneDates');
        const   lstTechMeasures = component.get('v.mileStoneDates.lstTechMeasure');
        let currentStage = 1;
        let arrStage = [];
        let currentStageType = "pending";
        let measureInPast = 0;
        let installInPast = 0;
        let shipPast = 0;
        let reinstallPast = 0;
        let surveyPast = 0; 
		let measureInFuture = false;
		let customBuildHold = false;
        let dateFormat = $A.get("$Browser.isPhone") ? "MM/dd/yyyy": "MMMM dd, yyyy";  // Setting the date format according to the device
        
        
        if(mileStones.isOnHold === true) {
            currentStageType = "hold";
        }
        
        /* ###########################################
        	ORDER PLACED STAGE
        ########################################### */
        if(mileStones && mileStones.orderPlaceDate != "") {
            arrStage.push({
                className: 'order',
                stage: 'ORDER PLACED',
                status: 'SCHEDULED',
                date: $A.localizationService.formatDate(new Date(mileStones.orderPlaceDate).setHours(24), dateFormat),
                text: '<h3 class="text-uppercase slds-text-heading_small slds-m-bottom_medium">CONGRATULATIONS!</h3><p class="slds-m-bottom_medium">Your replacement window order has been placed! While waiting for your upcoming tech measure appointment, feel free to reach out to your design consultant with any questions you may have – they will be happy to help!</p>'
            });
        } 
        
        /* ###########################################
        	TECH MEASURE STAGE
        ########################################### */
        if(lstTechMeasures.length == 0) {
            arrStage.push({
                className: 'measure',
                stage: 'MEASURE APPOINTMENT',
                status: '',
                date: '',
                text: '<h3 class="text-uppercase slds-text-heading_small slds-m-bottom_medium">WHAT TO EXPECT FROM YOUR MEASURE APPOINTMENT</h3><p class="slds-m-bottom_medium">Think of your Project Technician as your personal construction resource. He or she will:</p> <p>•  Take detailed window and framing measurements</p> <p>•   Assess the condition of your frames, sash, casing, and trim </p> <p>•   Note any climate concerns, potential structural issues, and aesthetics</p>  <p>•   Compare project specs and note any corrections</p>  <p>•   Ask – and answer – additional questions</p>  <p>•   Prepare you for your installation appointment</p>'
        	});
        } else {
            lstTechMeasures.forEach((item) => {
                var tmd = $A.localizationService.formatDate(new Date(item.Scheduled_Start_Time__c), "yyyyMMdd");
                var td = $A.localizationService.formatDate(new Date(), "yyyyMMdd");         
                arrStage.push({
                    className: 'measure',
                    stage: 'MEASURE APPOINTMENT',
                    status: tmd < td ? 'COMPLETED' : 'SCHEDULED',
                    date: $A.localizationService.formatDate(new Date(item.Scheduled_Start_Time__c), dateFormat),
                    text: '<h3 class="text-uppercase slds-text-heading_small slds-m-bottom_medium">WHAT TO EXPECT FROM YOUR MEASURE APPOINTMENT</h3><p class="slds-m-bottom_medium">Think of your Project Technician as your personal construction resource. He or she will:</p> <p>•  Take detailed window and framing measurements</p> <p>•   Assess the condition of your frames, sash, casing, and trim </p> <p>•   Note any climate concerns, potential structural issues, and aesthetics</p>  <p>•   Compare project specs and note any corrections</p>  <p>•   Ask – and answer – additional questions</p>  <p>•   Prepare you for your installation appointment</p>'
                });
            	// Set current stage and current stage status
                if (tmd < td) {
                    currentStage++;
                    measureInPast++;
                } else if (tmd == td) {currentStage++; currentStageType = "pending";}
                else { measureInFuture = true;}
            });
        }
        // Set current stage and current stage status
        if(measureInFuture === true) currentStage++;
        if(measureInPast > 0) currentStage = 1 + measureInPast + 1;
        if(lstTechMeasures.length == 0 || (measureInPast > 0 && measureInPast < lstTechMeasures.length) || (measureInPast == 0 && lstTechMeasures.length > 0)) {
            currentStageType = "pending";
        }
        
        /* ###########################################
        	CUSTOM BUILD STAGE
        ########################################### */
        arrStage.push({
            className: 'build',
            stage: 'CUSTOM BUILD',
            status: '',//ESTIMATED COMPLETION
            date: '',
            text: '<h3 class="text-uppercase slds-text-heading_small slds-m-bottom_medium">IT’S MORE THAN JUST THE GLASS</h3><p class="slds-m-bottom_medium">We want to produce the best of the best for our customers, so every window in our factory is treated with care and attention to detail. Renewal by Andersen® windows are made from Fibrex® material, an Andersen-exclusive composite that combines the strength and stability of wood with the low-maintenance features of vinyl. <a href="https://www.renewalbyandersen.com/signature-service/an-exclusive-product">Click here</a> to read more about this exclusive product that will soon be in your home!</p>'
        });
        if(mileStones.isOnHold === true && mileStones.installDates.length === 0 && measureInFuture === false) {
        	customBuildHold = true;
            arrStage[lstTechMeasures.length + 1].status = "ON HOLD";
        }
        
        
        
        /* ###########################################
        	SHIPPING STAGE
        ########################################### */
        if(component.get('v.mileStoneDates.showShipDetails') && component.get('v.mileStoneDates.EstimatedShipDate')) {
            arrStage.push({
                className: 'ship',
                stage: 'SHIPPING',
                status: 'SCHEDULED',
                date: $A.localizationService.formatDate(new Date(component.get('v.mileStoneDates.EstimatedShipDate')).setHours(24), dateFormat),
                text: '<h3 class="text-uppercase slds-text-heading_small slds-m-bottom_medium">What to expect from your <strong>Shipping</strong></h3><p class="slds-m-bottom_medium">Success! Your order has left our manufacturing facility and is on its way to you. As a reminder, if you have any questions before your installation, feel free to reach out through the ‘Contact Us’ page. We hope you are looking forward to installation day as much as we are!</p>'
            })
            if(today >= component.get('v.mileStoneDates.EstimatedShipDate')) {
                shipPast = 1;
            	currentStage = 1 + lstTechMeasures.length + 1 + shipPast;
            }
        }
        
        /* ###########################################
        	INSTALL STAGE
        ########################################### */
        if(mileStones.installDates.length === 0) {
             // Set up the string here with what is in text right now
            // If there are any pre-install checklist items selected, add the link to the checklist
            // The link should open up the checklist, which is a seperate component
            arrStage.push({
                className: 'install',
                stage: 'INSTALLATION',
                status: '',
                date: '',
                text: '<h3 class="text-uppercase slds-text-heading_small slds-m-bottom_medium"> WHAT TO EXPECT WITH YOUR INSTALLATION</h3><p class="slds-m-bottom_medium">A great window is only as good as its installation– which is why our installers are truly the heroes of Renewal by Andersen’s full-service window replacement process. They will treat your home as if it were their own and clean up afterwards – including the disposal of your old windows!</p>'
            });
        } else {
            mileStones.lstInstall.forEach(({Scheduled_Start_Time__c: item}) => {
                arrStage.push({
                    className: 'install',
                    stage: 'INSTALLATION',
                    status: 'SCHEDULED',
                    date: $A.localizationService.formatDate(new Date(item), dateFormat),
                    text: '<h3 class="text-uppercase slds-text-heading_small slds-m-bottom_medium">WHAT TO EXPECT WITH YOUR INSTALLATION</h3><p class="slds-m-bottom_medium">A great window is only as good as its installation– which is why our installers are truly the heroes of Renewal by Andersen’s full-service window replacement process. They will treat your home as if it were their own and clean up afterwards – including the disposal of your old windows!</p>'
                });
            
                var tmd = new Date(item).getTime();
                var td = new Date().getTime();
                if (tmd < td) {
                    currentStage++;
                    installInPast++;
                }
            });
        }
        
        console.log("ship:: ", shipPast);
        if(installInPast > 0) {            
            currentStage = 1 + lstTechMeasures.length + 1 + shipPast + installInPast;
        }
        
		/* ###########################################
        	RE-INSTALL STAGE
        ########################################### */		
        if(component.get("v.mileStoneDates.ReinstallDate") != undefined) {
            arrStage.push({
                className: 'install',
                stage: 'RE-INSTALLATION',
                status: 'SCHEDULED',
                date: $A.localizationService.formatDate(new Date(component.get("v.mileStoneDates.ReinstallDate")).setHours(24), dateFormat),
                text: '<h3 class="text-uppercase slds-text-heading_small slds-m-bottom_medium">WHAT TO EXPECT WITH YOUR INSTALLATION</h3><p class="slds-m-bottom_medium">We want to make sure everything is just right, so we will be back to put the final touches on your installation!</p>'
            });
            
            var tmd = $A.localizationService.formatDate(new Date(component.get("v.mileStoneDates.ReinstallDate")).setHours(24), "yyyyMMdd");
    		var td = $A.localizationService.formatDate(new Date(), "yyyyMMdd"); 
            if(tmd < td) {
                reinstallPast = 1;
                currentStage = 1 + lstTechMeasures.length + 1 + shipPast + mileStones.lstInstall.length + reinstallPast;
                console.log(lstTechMeasures.length, '-', shipPast, '-', mileStones.lstInstall.length,'-',reinstallPast);
            }
        }
        
        
        /* ###########################################
        	SURVEY STAGE
        ########################################### */
        arrStage.push({
            className: 'survey',
            stage: 'SURVEY',
            status: component.get("v.mileStoneDates.surveySentDate") != undefined ? 'SCHEDULED' : '',
            date: component.get("v.mileStoneDates.surveySentDate") != undefined ? $A.localizationService.formatDate(new Date(component.get("v.mileStoneDates.surveySentDate")).setHours(24), dateFormat): '',
            text: '<h3 class="text-uppercase slds-text-heading_small slds-m-bottom_medium">TELL US ABOUT YOUR EXPERIENCE</h3><p class="slds-m-bottom_medium">The name Renewal by Andersen is synonymous with quality and care. At Renewal by Andersen, we promise to be there for you – two weeks after installation or two decades <a href="https://www.renewalbyandersen.com/homeowners/warranty">Click here</a> to find more information about our limited warranty. It\’s our promise to keep the lines of communication open, and to listen to your feedback, questions, and concerns.</p>'
		});
        
        if(component.get("v.mileStoneDates.surveySentDate") != undefined) {
            var tmd = $A.localizationService.formatDate(new Date(component.get("v.mileStoneDates.surveySentDate")), "yyyyMMdd");
    		var td = $A.localizationService.formatDate(new Date(), "yyyyMMdd"); 
            if(tmd <= td) {
                surveyPast = 1;                
            }
        }
        
        /* ###########################################
        	COMPLETE STAGE
        ########################################### */        
        if(component.get("v.mileStoneDates.surveySentDate") != undefined && mileStones.isOnHold !== true) {
            var tmd = $A.localizationService.formatDate(new Date(component.get("v.mileStoneDates.surveySentDate")), "yyyyMMdd");
    		var td = $A.localizationService.formatDate(new Date(), "yyyyMMdd"); 
            if(tmd < td) {
                arrStage.push({
                    className: 'complete',
                    stage: 'CONGRATULATIONS',
                    status: 'YOUR PROJECT IS COMPLETE',
                    date: '',
                    text: ''
                    // text: 'Thank you for choosing us as your Window and Door replacement experts.'
                });
            }
        }
        
		/*
        if(mileStones.ReinstallDate != "") {
            var tmd = new Date(mileStones.ReinstallDate).getTime();
    		var td = new Date().getTime();
            if(tmd <= td) {
                currentStage = 1 + lstTechMeasures.length + 1 + mileStones.installDates.length + 2;
            }
        }
        */		
		
		// Current stage check for install, survey and complete stages
        if(mileStones.lstInstall.length > 0 && installInPast === mileStones.lstInstall.length) {
            currentStage = 1 + lstTechMeasures.length + 1 + shipPast + mileStones.lstInstall.length + reinstallPast;
            if(component.get("v.mileStoneDates.surveySentDate") != undefined && mileStones.isOnHold !== true) {
            	currentStage = 1 + lstTechMeasures.length + 1 + shipPast + mileStones.lstInstall.length + reinstallPast + 1;    
            }            
            if(surveyPast == 0) {
                currentStage = 1 + lstTechMeasures.length + 1 + shipPast + mileStones.lstInstall.length + reinstallPast;
            }
            if((arrStage.filter((item) => item.stage == 'CONGRATULATIONS').length) > 0) {
                currentStage = currentStage + 1;
                currentStageType = 'completed'
            }
        }

		//console.log("measureInFuture:: ", measureInFuture, "customBuildHold:: ", customBuildHold, 'currentStage:: ', currentStage);
		console.log("currentStage:: ", currentStage, 'reinstallpast',reinstallPast );
        component.set("v.totalStage", arrStage.length);
        component.set("v.stage", currentStage);
		component.set("v.currentStageType", currentStageType);
		
		// Change the color of on Hold in the bottom text
        if(mileStones.isOnHold === true) {
            if(currentStage <=  lstTechMeasures.length + 1) {
                arrStage[currentStage-1].date = '';
                arrStage[currentStage-1].text = '<h3 class="text-uppercase slds-text-heading_small slds-m-bottom_medium">Tech Measure is <strong style="color:#f18346">ON HOLD</strong></h3><p class="slds-m-bottom_medium">'+mileStones.isOnHoldMessage +'</p>';
            }
            if(currentStage == lstTechMeasures.length + 2) {
                arrStage[currentStage-1].text = '<h3 class="text-uppercase slds-text-heading_small slds-m-bottom_medium">Your Custom Build is <strong style="color:#f18346">ON HOLD</strong></h3><p class="slds-m-bottom_medium">'+mileStones.isOnHoldMessage +'</p>'
            } 
            
            if(currentStage > lstTechMeasures.length + 2) {
                arrStage[currentStage-1].text = '<h3 class="text-uppercase slds-text-heading_small slds-m-bottom_medium">Installation is <strong style="color:#f18346">ON HOLD</strong></h3><p class="slds-m-bottom_medium">'+mileStones.isOnHoldMessage +'</p>'
            }
          }

        // mark previous stages as completed.
        for(var i = 0; i < currentStage - 1; i++) {
            if(i == 0) arrStage[i].status = "COMPLETE";
            else arrStage[i].status = "COMPLETED";
        }
        if(arrStage[currentStage - 1].date != "") {
        	var tmd = $A.localizationService.formatDate(new Date(arrStage[currentStage - 1].date).setHours(24), "yyyyMMdd");
    		var td = $A.localizationService.formatDate(new Date(), "yyyyMMdd"); 
            if(tmd < td) {
                arrStage[currentStage - 1].status = 'COMPLETED';
                if((currentStage -1) == 0) arrStage[currentStage - 1].status = 'COMPLETE';
            }
		}
        
		if(arrStage.length > 0)	 {
            component.set("v.currentMilesstone", arrStage[currentStage - 1]);
            component.set("v.nextMilesstone", arrStage[currentStage]);
        }
		
        console.log("arrStage:: ", arrStage, currentStage);
		return arrStage;
    },
    
    stageIsInstallation : function(component) {

        let stages = component.get("v.stages");
        console.log("CD: currentStage: " + stages[component.get("v.stage") - 1].stage);
        component.set("v.stageIsInstall", stages[component.get("v.stage") - 1].stage === 'INSTALLATION');
    },

    getOrderDetails : function(component) {
        //$A.get('e.force:refreshView').fire();
        var ordId = component.get('v.orderId');
        var action = component.get("c.getAllMileStonesForOrder");
        action.setParams({"orderId" : ordId});
        //Added for the MileStone Activity Section
        action.setCallback(this, function(returnVal){
            var mDate = returnVal.getReturnValue() ;
            component.set("v.mileStoneDates",mDate);             
            console.log('after component.set', mDate);
            const arrStage = this.createStageArray(component);
            component.set("v.stages", []);
            component.set("v.stages", arrStage);   
            component.set("v.checklistItems", mDate.checklistItems);
            component.set("v.hasChecklistItems", mDate.checklistItems.length != 0);
            component.set("v.openChecklist", false);          
            this.makeActive(component);
        });
        $A.enqueueAction(action);
    }
})