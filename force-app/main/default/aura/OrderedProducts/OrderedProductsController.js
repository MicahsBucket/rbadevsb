({
    // Init
    init : function(component, event, helper) {

        //finds the most recent order
       component.set("v.toggleSpinner", true);
        console.log("init loaded");
        var getId = component.get('c.getOrderId');
        getId.setCallback(this, function(returnVal){
           var ordId = returnVal.getReturnValue();
           component.set('v.orderId', ordId);
        }); 
        $A.enqueueAction(getId);

        //finds all of the orders the user is associated to
        var getOrders = component.get('c.getOrders');
        getOrders.setCallback(this, function(returnVal){
            var orders = returnVal.getReturnValue();
            var orderList = [];
            //creates the list of order dates that is displayed to the user 
            for(var i = 0; i < orders.length; i++){
                orderList.push(orders[i].EffectiveDate);
            }
            component.set('v.orderList', orderList);
			//component.set("v.toggleSpinner", false);
            console.log("loading end");
        });
        $A.enqueueAction(getOrders);
       // helper.helperFun(component,event,'articleFour');
    },

    //changes the displayed order based on the users selection
    updateOrderId : function(component, event, helper){
        var date = component.find('orderSelect').get('v.value');
        var getNewId = component.get('c.getNewOrderId'); 
        getNewId.setParams({
            "bookingDate" : date 
        })
        getNewId.setCallback(this, function(returnVal){
            var orderId = returnVal.getReturnValue();
            component.set('v.orderId', orderId);
            //causes all of the child components to refresh based on the new order Id
        });
        $A.enqueueAction(getNewId);
    },
     
})