({
    doInit : function(component, event, helper) {  
        //ContentUrl "https://test.renewalbyandersen.com/components/header"
        var url = component.get("v.ContentUrl");
        if(url != null){
            fetch(url).then(response => response.text()).then(text => component.set("v.htmlString", text)); 
        }
        
    }
})