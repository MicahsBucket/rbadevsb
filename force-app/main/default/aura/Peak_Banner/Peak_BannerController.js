({
    initPeakBanner: function(component, event, helper) {
        helper.setVerticalAlignment(component, event, helper);
        helper.setPadding(component, event, helper);
    }
});