({
	callOrdering : function(component, event, helper) {

		var communityPageName = component.get("v.orderingPageName");

		if (communityPageName == '') {

			var navigationEvent = $A.get("e.force:navigateToComponent");
			navigationEvent.setParams({
				componentDef: "c:homeComponentLightningCardOrdering"
			});
			navigationEvent.fire();
			
		}
		else {

			component.find("navigationService").navigate({
				type: "comm__namedPage",
				attributes: {
					name: communityPageName
				}
			}, false);
	
		}

	},

	callTechSchedule : function(component, event, helper) {
		
		var communityPageName = component.get("v.techSchedulingPageName");
			
		if (communityPageName == '') {

			var navigationEvent = $A.get("e.force:navigateToComponent");
			navigationEvent.setParams({
				componentDef: "c:homeComponentLightningCardTechScheduling"
			});
			navigationEvent.fire();

		}
		else {

			component.find("navigationService").navigate({
				type: "comm__namedPage",
				attributes: {
					name: communityPageName
				}
			}, false);
	
		}

	},

	callServiceOrdering : function(component, event, helper) {

		var communityPageName = component.get("v.serviceOrderingPageName");
			
		if (communityPageName == '') {

			var navigationEvent = $A.get("e.force:navigateToComponent");
			navigationEvent.setParams({
				componentDef: "c:homeComponentLightningCardServiceOrdering"
			});
			navigationEvent.fire();

		}
		else {

			component.find("navigationService").navigate({
				type: "comm__namedPage",
				attributes: {
					name: communityPageName
				}
			}, false);
			
		}


	},

	callInstallSchedule : function(component, event, helper) {
		
		var communityPageName = component.get("v.installSchedulingPageName");
			
		if (communityPageName == '') {

			var navigationEvent = $A.get("e.force:navigateToComponent");
			navigationEvent.setParams({
				componentDef: "c:homeComponentLightningCardInstallScheduling"
			});
			navigationEvent.fire();

		}
		else {

			component.find("navigationService").navigate({
				type: "comm__namedPage",
				attributes: {
					name: communityPageName
				}
			}, false);
	
		}

	},

	callServiceWarranty : function(component, event, helper) {

		var communityPageName = component.get("v.serviceWarrantyPageName");
			
		if (communityPageName == '') {

			var navigationEvent = $A.get("e.force:navigateToComponent");
			navigationEvent.setParams({
				componentDef: "c:homeComponentLightningCardServiceWarranty"
			});
			navigationEvent.fire();

		}
		else {

			component.find("navigationService").navigate({
				type: "comm__namedPage",
				attributes: {
					name: communityPageName
				}
			}, false);
			
		}

	},
	
	callDashboards : function(component, event, helper) {

		var getDashboardIdAction = component.get("c.getDashboardId");

		 // Configure the response handler for the action.
		 getDashboardIdAction.setCallback(this, function(response) {
 
			// Make sure everything is find and grab the return value.
			var state = response.getState();
 
			if (component.isValid()) {
 
				var dashboardId = response.getReturnValue();
				if (dashboardId != null && state === "SUCCESS") {
					var navigationEvent = $A.get("e.force:navigateToSObject");
					navigationEvent.setParams({
						"recordId": dashboardId
					});
					navigationEvent.fire();
				}
				else {
					var navigationEvent = $A.get("e.force:navigateToObjectHome");
					navigationEvent.setParams({
						"scope": "Dashboard"
					});
					navigationEvent.fire();				}
				
			}
			else {
				console.log('Unknown problem, response state: ' + state);
			}

		});
 
		// Execute the action.
		$A.enqueueAction(getDashboardIdAction);

	}

})