({
    
    initOrder: function(component, event, helper) {
        
        // Set the current order
        var order = event.getParam("order");  
        //console.log("______Customer payment controller____order_____"+JSON.stringify(order));
        //alert('The Message received is '+order.Store_Location__r.Active_Store_Configuration__r.Display_Total_Price__c);
        // Format the data for the order
        var retailSubtotal ='';
        if(order.Retail_Subtotal__c != undefined){
            retailSubtotal=Number.parseFloat(order.Retail_Subtotal__c).toFixed(2).toLocaleString('en-US', { maximumSignificantDigits: 2 });
            retailSubtotal = helper.commafy(retailSubtotal);
            //alert('The retail Subtotal is '+retailSubtotal);
            if (retailSubtotal.indexOf('.')!=-1){
                order.Retail_Subtotal__c =retailSubtotal.split('.')[0]; 
            }else{
                order.Retail_Subtotal__c =retailSubtotal;
            }
            
        }
        var discountamount = '';		
        if(order.Discount_Amount__c != undefined){
            discountamount = Number.parseFloat(order.Discount_Amount__c).toFixed(2).toLocaleString();
            discountamount = helper.commafy(discountamount);
            if (discountamount.indexOf('.')!=-1){
                order.Discount_Amount__c = discountamount.split('.')[0];
            }else{
                order.Discount_Amount__c = discountamount;
            }
            
        }
        var retailtotal = '';
        if(order.Retail_Total__c != undefined){
            retailtotal = Number.parseFloat(order.Retail_Total__c).toFixed(2).toLocaleString();
            retailtotal = helper.commafy(retailtotal);
            if (retailtotal.indexOf('.')!=-1){
                order.Retail_Total__c = retailtotal.split('.')[0];
            }else{
                order.Retail_Total__c = retailtotal;
            }
        }
        var paymentsrecieved = '';
        if(order.Payments_Received__c != undefined){
            paymentsrecieved = Number.parseFloat(order.Payments_Received__c).toFixed(2).toLocaleString();
            paymentsrecieved = helper.commafy(paymentsrecieved);
            if (paymentsrecieved.indexOf('.')!=-1){
                order.Payments_Received__c = paymentsrecieved.split('.')[0];
                }else{
				order.Payments_Received__c = paymentsrecieved;
			}
		}
		var amountdue = '';
		if(order.Amount_Due__c != undefined){
			amountdue = Number.parseFloat(order.Amount_Due__c).toFixed(2).toLocaleString();
            amountdue = helper.commafy(amountdue);
			if (amountdue.indexOf('.')!=-1){
				order.Amount_Due__c = amountdue.split('.')[0];
			}else{
				order.Amount_Due__c = amountdue;
			}
		}
		/*
		// Bind the order to the control
        component.set("v.currentOrder", order);

		// Debug
		// console.log(JSON.stringify(order)); 
		*/
        var refundAmount = '';
        if(order.Amount_Refunded__c != undefined){
            refundAmount = Number.parseFloat(order.Amount_Refunded__c).toFixed(2).toLocaleString();
            refundAmount = helper.commafy(refundAmount);
            if (refundAmount.indexOf('.')!=-1){
                order.Amount_Refunded__c = refundAmount.split('.')[0];
            }else{
                order.Amount_Refunded__c = refundAmount;
            }
        }
        var businessAdjustments = '';
        if(order.Business_Adjustments__c != undefined){
            businessAdjustments = Number.parseFloat(order.Business_Adjustments__c).toFixed(2).toLocaleString();
            businessAdjustments = helper.commafy(businessAdjustments);
            if (businessAdjustments.indexOf('.')!=-1){
                order.Business_Adjustments__c = businessAdjustments.split('.')[0];
            }else{
                order.Business_Adjustments__c = businessAdjustments;
            }
        }
        
        if(order.Payments__r != undefined){
            var paymentRecords = order.Payments__r.records;
            var icounter = 0;
            for(icounter = 0; icounter < paymentRecords.length; icounter++){
                var paymentamount = '';
                if(paymentRecords[icounter].Payment_Amount__c != undefined){
                    paymentamount = Number.parseFloat(paymentRecords[icounter].Payment_Amount__c).toFixed(2).toLocaleString();
                    paymentamount = helper.commafy(paymentamount);
                    if (paymentamount.indexOf('.')!=-1){
                        paymentRecords[icounter].Payment_Amount__c = paymentamount.split('.')[0];
                    }else{
                        paymentRecords[icounter].Payment_Amount__c = '(' + paymentamount + ')' ;
                    }
                }       
            }
            component.set("v.paymentRecords",paymentRecords);
        }
        
        // Bind the order to the control
        component.set("v.currentOrder", order); 
    },
    goToPaymentProcessor: function(component, event, helper){
        var order = component.get("v.currentOrder");
        console.log('order in nav event', JSON.parse(JSON.stringify(order)));
        var url = order.Store_Location__r.Active_Store_Configuration__r.Payment_Url__c;
        console.log('url in nav event ', url);
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
                "url": url
            });
        urlEvent.fire();
    }
})