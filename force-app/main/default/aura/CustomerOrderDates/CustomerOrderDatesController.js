({  
    //finds the dates associated to an order and displays them to the user
    init: function (component, event, helper) {
        var ordId = component.get('v.orderId');
        var getOrder = component.get("c.getOrderDates");
        var today = $A.localizationService.formatDate(new Date() + 86400, "YYYY-MM-DD"); // today's date
        //var today = $A.localizationService.formatDate(new Date().setHours(0),"YYYY-MM-DDTHH:MM:SS") ; // today's date
       var todayfortechmeasure =  $A.localizationService.formatDate(new Date().setHours(24),"YYYY-MM-DDTHH:MM:SS") ;
        var todayInstalltion =  $A.localizationService.formatDate(new Date().setHours(28),"YYYY-MM-DDTHH:MM:SS");
        //var todayfortechmeasure = $A.localizationService.formatDate(new Date().setHours(0),"YYYY-MM-DDTHH:MM:SS") ; // today's date
        console.log('-------------today',today) ;
        component.set('v.todayfortechmeasure',todayfortechmeasure);
        component.set('v.todayInstalltion',todayInstalltion);
        component.set('v.today', today);  // today's date
        component.set('v.isTechMeasureInFuture', 'true');
        component.set('v.isInstallInFuture', 'true');
        component.set('v.isAnyInstallComplete', 'false');
        console.log('The Order Id &&&& is '+ordId);
        getOrder.setParams({
            "orderId" : ordId
        });

        getOrder.setCallback(this, function(returnVal){
            var order = returnVal.getReturnValue();
            //maxes the date/time fields readable to the user
            if(order.Time_Install_Scheduled__c != null && order.Time_Install_Scheduled__c != undefined){
                var install = order.Time_Install_Scheduled__c
                var installList = install.split('T');
                order.Time_Install_Scheduled__c = installList[0];
                /*var ready = order.Time_Ready_to_Order__c
                var readyList = ready.split('T');
                order.Time_Ready_to_Order__c = installList[0]; Ramya : 516*/
            }
            
            
            //resets the display of the current status of an order when a new order is selected by the user
            //helper.clearStatus(component, event, helper);
            //updates the display of the current status of an order when a new order is selected
            //helper.showStatus(order, component, event, helper);
            component.set('v.order', order);
            component.set('v.orderStatus', order.Status);
        })
        $A.enqueueAction(getOrder);
        
console.log('after enqueueAction of getOrder');
        
        var action = component.get("c.getAllMileStonesForOrder");
        action.setParams({
            "orderId" : ordId
        });
        
console.log('after setParams #2');        
        
        //Added for the MileStone Activity Section
        action.setCallback(this, function(returnVal){
            var mDate = returnVal.getReturnValue() ;
            component.set("v.mileStoneDates",mDate); 
            
            console.log('after component.set');
            //console.log('-----------------mDate',mDate) ;
            //console.log('-----------------mDate.isOnHold'+mDate.isOnHold) ;
            //component.set('v.isOnHold',mDate.isOnHold) ;
            var p = component.get('c.processMileStones');
            p.setParams({
                "component" : component
            });            
            $A.enqueueAction(p);
            
            
        });
        $A.enqueueAction(action);
    },
    processMileStones: function(component, event, helper) {
        console.log('-----processMileStones') ;
        var today = $A.localizationService.formatDate(new Date() + 86400, "YYYY-MM-DD");
       
          //var today = $A.localizationService.formatDate(new Date().setHours(0),"YYYY-MM-DDTHH:MM:SS") ; 
        console.log('v.mileStoneDates.lstTechMeasure',component.get('v.mileStoneDates')) ;
        var lstTechMeasureDate = [] ;
        lstTechMeasureDate = component.get('v.mileStoneDates.lstTechMeasure') ;
       console.log('lstTechMeasureDate.length '+lstTechMeasureDate.length ) ;
       if(lstTechMeasureDate.length > 0 )
        {
            for(var i = 0 ; i < lstTechMeasureDate.length ; i ++)	
            {
                console.log('-----1--'+today) ;
                console.log('-----1--'+lstTechMeasureDate[i].Scheduled_Start_Time__c) ;
                
                if(lstTechMeasureDate[i].Scheduled_Start_Time__c > today)
                    component.set('v.showCustomBuild',false) ;
                else
                    component.set('v.showCustomBuild',true) ;
            }
        }
        // Getting Most recent Tech Measure Id.
        
        var lstTechMeasures = component.get('v.mileStoneDates.lstTechMeasure') ;
        
        if( lstTechMeasures!=null && lstTechMeasures.length > 0)
        {
            component.set('v.mostRecentTechMeasureId',lstTechMeasures[lstTechMeasures.length - 1].Id) ;
            
        }
        // Getting the Most recent Installation Id
        var lstInstallations = component.get('v.mileStoneDates.lstInstall') ;
        
        if( lstInstallations!=null && lstInstallations.length > 0)
        {
            component.set('v.mostRecentInstallationId',lstInstallations[lstInstallations.length - 1].Id) ;
            
        }
        var techDatesLen = component.get("v.mileStoneDates.techMeasureDate").length  ;
        var mostRecentTechDate = null;
        var isTechMesureinFuture = 'false';
        var isAnyInstallComplete = 'false';
        
        for (var i = 0; i<techDatesLen; i++) { 
            var techDates = (component.get("v.mileStoneDates.techMeasureDate")[i]).substring(0,10);
            if (i==0 || techDates>mostRecentTechDate)
                mostRecentTechDate = techDates;
        }
        // Comparing the Tech Measure date with current date to highlight/color the "Custom Build" Icon, Road and circle.
        //var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        var today = $A.localizationService.formatDate(new Date().setHours(24),"YYYY-MM-DDTHH:MM:SS") ; // today's date
        if (mostRecentTechDate!=null && mostRecentTechDate > today){
            isTechMesureinFuture = 'TRUE';
        }
        if (mostRecentTechDate!=null){
            component.set("v.TechMeasureMostRecentDate", mostRecentTechDate);
        }
        component.set("v.isTechMeasureInFuture", isTechMesureinFuture);
        var installDatesLen = component.get("v.mileStoneDates.installDates").length  ;
        
        var mostRecentinstallDate = null;
        var isinstallMesureinFuture = 'false';
        
        var installDateList = [];
        var counter =0;
        var today = $A.localizationService.formatDate(new Date().setHours(24),"YYYY-MM-DDTHH:MM:SS") ;
        for (var i = 0; i<installDatesLen; i++) { 
            var installDates = (component.get("v.mileStoneDates.installDates")[i]).substring(0,10);
            if (i==0 || installDates>mostRecentinstallDate)
                mostRecentinstallDate = installDates;
            
            if (installDates < today) {// Atleast one install has happened, so custom build is complete
                isAnyInstallComplete = 'true';
            }       
            
            // convert from datetime to date
            installDateList[counter] = installDates;
            console.log('install date is' + installDateList[counter]);
            counter++
        }
        component.set("v.isAnyInstallComplete", isAnyInstallComplete);
        
        component.set("v.InstallationDateList", installDateList); 
        
        
        // Comparing the Install date with current date to highlight/color the "Install" Icon, Road and circle. Ramya
        //var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        var today = $A.localizationService.formatDate(new Date().setHours(24),"YYYY-MM-DDTHH:MM:SS") ;
        
        if (mostRecentinstallDate!=null && mostRecentinstallDate > today){
            isinstallMesureinFuture = 'TRUE';
        }
        if (mostRecentinstallDate!=null){
            component.set("v.InstallMostRecentDate", mostRecentinstallDate);
        }
        component.set("v.isInstallInFuture", isinstallMesureinFuture);
        
        //SURVEY STAGE
       
        if(component.get("v.mileStoneDates.surveySentDate") != undefined)
        {
            var getEvent = $A.get("e.c:MileStoneActivityEvent") ;
            getEvent.setParams({
                "MileStoneDate" : component.get("v.mileStoneDates.surveySentDate"),
                "MileStoneStages" : 'Survey Sent',
                "SecondMileStoneDate" : (component.get("v.mileStoneDates.installDate")).substring(0,10),
                "ReInstallDate" : component.get("v.mileStoneDates.ReinstallDate")
            }) ;
            console.log("### surveysent");
            getEvent.fire() ; 
        }
        //RE-INSTALL STAGE
        else if(component.get("v.mileStoneDates.ReinstallDate") != undefined)
        {
            var getEvent = $A.get("e.c:MileStoneActivityEvent") ;
            getEvent.setParams({
                "MileStoneDate" : component.get("v.mileStoneDates.ReinstallDate"),
                "MileStoneStages" : 'Re-Install',
                "SecondMileStoneDate" : (component.get("v.mileStoneDates.installDate")).substring(0,10),
                "ReInstallDate" : component.get("v.mileStoneDates.ReinstallDate")
            }) ;
            console.log("### Re-Install Stage");
            getEvent.fire() ; 
        }
        
        //INSTALL STAGE
            
            else if(
                component.get("v.mileStoneDates.lstInstall" ) != null && 
                   ( 
                    ((component.get("v.mileStoneDates.lstInstall" ).length > 0) 
                     && (component.get("v.isInstallInFuture") == 'false'))
                    ||
                    (
                        (component.get("v.mileStoneDates.lstInstall" ).length > 1) 
                     && (component.get("v.isInstallInFuture") == 'TRUE'))                 
                   )
                  )
            {
                
                var getTEchMeasureSize = component.get("v.mileStoneDates.techMeasureDate").length ; 
                var getTechMeasureDate = (component.get("v.mileStoneDates.techMeasureDate")[getTEchMeasureSize - 1]).substring(0,10); 
                var getEvent = $A.get("e.c:MileStoneActivityEvent") ;
                getEvent.setParams({
                    "MileStoneDate" : (component.get("v.mileStoneDates.installDate")).substring(0,10),
                    "MileStoneStages" : 'Install Date',
                    "SecondMileStoneDate" : getTechMeasureDate,
                    "ReInstallDate" : component.get("v.mileStoneDates.ReinstallDate")
                }) ;
                console.log("### InstallStage");
                getEvent.fire() ;
            } 
        
        //SHIPPING STAGE
             
          // else if(component.get('v.mileStoneDates.showShipDetails') && component.get('v.today') > component.get('v.mileStoneDates.EstimatedShipDate'))
           else if( component.get('v.mileStoneDates.showShipDetails') && 
                   (component.get('v.today') >= component.get('v.mileStoneDates.EstimatedShipDate'))
                  )
            {
                var getTEchMeasureSize = component.get("v.mileStoneDates.techMeasureDate").length ; 
                var getTechMeasureDate = (component.get("v.mileStoneDates.techMeasureDate")[getTEchMeasureSize - 1]).substring(0,10);
                var getEvent = $A.get("e.c:MileStoneActivityEvent") ;
                getEvent.setParams({
                    "MileStoneDate" : (component.get("v.mileStoneDates.EstimatedShipDate")).substring(0,10),
                    "MileStoneStages" : 'Shipping Stage',
                    "SecondMileStoneDate" : getTechMeasureDate,
                    "ReInstallDate" : component.get("v.mileStoneDates.ReinstallDate")
                }) ;
                console.log("### Shipping Stage");
                getEvent.fire() ;                                    
            }
        
        //CUSTOM BUILD STAGE 
                
               else if( 
                       component.get("v.mileStoneDates.techMeasureDate") != null && (
                       component.get("v.mileStoneDates.techMeasureDate").length > 0 &&
                       component.get('v.showCustomBuild') == true) 
                 	||
                      	(component.get("v.mileStoneDates.installDate" ) != 'undefined') && 
                      	 component.get("v.isInstallInFuture") == 'TRUE' )
                    
                {
                    
                    var getTEchMeasureSize = component.get("v.mileStoneDates.techMeasureDate").length ; 
                    var getTechMeasureDate = (component.get("v.mileStoneDates.techMeasureDate")[getTEchMeasureSize - 1]).substring(0,10); 
                    
                    var getEvent = $A.get("e.c:MileStoneActivityEvent") ;
                    getEvent.setParams({
                        "MileStoneDate" : getTechMeasureDate,
                        "MileStoneStages" : 'Custom Build',
                        "SecondMileStoneDate" : component.get("v.mileStoneDates.orderPlaceDate"),
                        "ReInstallDate": component.get("v.mileStoneDates.ReinstallDate"),
                        "showShippingStage" : component.get('v.mileStoneDates.showShipDetails')
                    }) ;
                    console.log("### Custom Build");
                    getEvent.fire() ;                                    
                }          
        
        // MEASURE APPOINTMENT STAGE
                    
                else if (component.get("v.mileStoneDates.techMeasureDate")!= null &&
                             component.get("v.mileStoneDates.techMeasureDate").length > 0 ) 
                    {
                        var getTEchMeasureSize = component.get("v.mileStoneDates.techMeasureDate").length ; 
                        var getTechMeasureDate = (component.get("v.mileStoneDates.techMeasureDate")[getTEchMeasureSize - 1]).substring(0,10); 
                        
                        var getEvent = $A.get("e.c:MileStoneActivityEvent") ;
                        getEvent.setParams({
                            "MileStoneDate" : getTechMeasureDate,
                            "MileStoneStages" : 'Tech Measure',
                            "SecondMileStoneDate" : component.get("v.mileStoneDates.orderPlaceDate"),
                            "ReInstallDate" : component.get("v.mileStoneDates.ReinstallDate")
                        }) ;
                        console.log("### TechMesure");
                        getEvent.fire() ;                                    
                    }
       
        //IN-HOME CONSULTATION & ORDER PLACED STAGES
                        
                   else if(component.get("v.mileStoneDates.orderPlaceDate") != undefined)
                   {
                            
                            var getEvent = $A.get("e.c:MileStoneActivityEvent") ;
                            getEvent.setParams({
                                "MileStoneDate" : component.get("v.mileStoneDates.orderPlaceDate"),
                                "MileStoneStages" : 'Order Placed',
                                "SecondMileStoneDate" : component.get("v.mileStoneDates.consultationDate"),
                                "ReInstallDate" : component.get("v.mileStoneDates.ReinstallDate")
                            }) ;
                            console.log("### OrderPlaced");
                            getEvent.fire() ;
                        } else {
                            
                        }   
        console.log('-----------showCustomBuild'+component.get('v.showCustomBuild')) ;
        console.log('-----------TechMeasureMostRecentDate'+component.get('v.TechMeasureMostRecentDate')) ;
        console.log('-----------isTechMeasureInFuture'+component.get('v.isTechMeasureInFuture')) ;
        console.log('-----------v.mileStoneDates.EstimatedShipDate'+component.get('v.mileStoneDates.EstimatedShipDate')) ;
        console.log('-----------v.today'+component.get('v.today')) ;
       // v.mileStoneDates.EstimatedShipDate > v.today
        
    },
    // MileStone Activity Icon buttons for Mobile Device
    pressButton: function(component, event, helper) {
        var ctarget = event.currentTarget;
        var buttonPressed = ctarget.dataset.value;
        var device = $A.get("$Browser.formFactor");
        var modal = component.find("myModal");
        var backdrop = component.find("modalBackdrop");
        
        if(buttonPressed == "consultation") {
            component.set("v.modalHeading", "In-home Consultation");
            component.set("v.modalText", "Our design consultants listen to your needs and create a plan to help improve energy efficiency and enhance your home’s beauty. Your design consultant will meet with you in the comfort of your own home, at a time that fits your schedule, and walk through the details of your situation. Whether you have five questions or 50, your consultant will work with you to design a solution that works for you!");
            
        } else if(buttonPressed == "order-placed") {
            component.set("v.modalHeading", "Order Placed");
            component.set("v.modalText", "Congratulations! Your replacement window order has been placed! While waiting for your upcoming tech measure appointment, feel free to reach out to your design consultant with any questions you may have – they will be happy to help!");
            
        } else if(buttonPressed == "tech-measure") {
            component.set("v.modalHeading", "Measure Appointment");
            component.set("v.modalText", "Think of your Project Technician as your personal construction resource. He or she will: <br/> •  Take detailed window and framing measurements <br/> •	Assess the condition of your frames, sash, casing, and trim  <br/> •	Note any climate concerns, potential structural issues, and aesthetics  <br/>•	Compare project specs and note any corrections  <br/>•	Ask – and answer – additional questions  <br/>•	Prepare you for your installation appointment");       
            
        } else if(buttonPressed == "custom-build") {
            component.set("v.modalHeading", "Custom Build");
            component.set("v.modalText", "We want to produce the best of the best for our customers, so every window in our factory is treated with care and attention to detail. Renewal by Andersen® windows are made from Fibrex® material, an Andersen-exclusive composite that combines the strength and stability of wood with the low-maintenance features of vinyl. <a href=\"https://www.renewalbyandersen.com/signature-service/an-exclusive-product\">Click here</a> to read more about this exclusive product that will soon be in your home!");
            
        } else if(buttonPressed == "Shipping") {
            component.set("v.modalHeading", "Shipping");
            component.set("v.modalText","Success! Your order has left our manufacturing facility and is on its way to you. As a reminder, if you have any questions before your installation,feel free to reach out through the ‘Contact Us’ page. We hope you are looking forward to installation day as much as we are!");
            
        } else if(buttonPressed == "Re-Install") {
            component.set("v.modalHeading", "Installation ");
            component.set("v.modalText", "We want to make sure everything is just right, so we will be back to put the final touches on your installation!");
            
        } else if(buttonPressed == "installation") {
            component.set("v.modalHeading", "Installation ");
            component.set("v.modalText", "A great window is only as good as its installation– which is why our installers are truly the heroes of Renewal by Andersen’s full-service window replacement process. They will  treat your home as if it were their own and clean up afterwards – including the disposal of your old windows!");
            
        } else if(buttonPressed == "survey") {
            component.set("v.modalHeading", "Survey");
            component.set("v.modalText", "The name Renewal by Andersen is synonymous with quality and care. At Renewal by Andersen, we promise to be there for you – two weeks after installation or two decades <a href=\"https://www.renewalbyandersen.com/homeowner-help/warranty\">Click here</a> to find more information about our limited warranty. It\’s our promise to keep the lines of communication open, and to listen to your feedback, questions, and concerns.");
        }
        /* if(device == 'PHONE' || device == 'DESKTOP') { */
        if(device == 'PHONE') {
            $A.util.addClass(modal, 'slds-fade-in-open');
            $A.util.addClass(backdrop, 'slds-backdrop_open');
        };
    },
    closeModal: function(component, event, helper) {
        var modal = component.find("myModal");
        var backdrop = component.find("modalBackdrop");
        $A.util.removeClass(modal, 'slds-fade-in-open');
        $A.util.removeClass(backdrop, 'slds-backdrop_open');        
    },
    
    initOrder : function(component, event, helper){
        var order = event.getParam("order");
        var orderId = event.getParam("orderId");
        var orderDate = event.getParam("orderDate");
        // var ordId = component.get('v.orderId');
        component.set('v.orderId', orderId);
        component.set('v.order', order);
        component.set('v.orderStatus', order.Status);
        
        var action = component.get("c.getAllMileStonesForOrder");
        action.setParams({
            "orderId" : orderId
        });
        // @@ This is the code which copy pasted from the init method because in init method getting the order based on the orderId as well
        // as millestones for that orde now in this method i just want to get all the milestones based on the orderId. Where I am having the OrderId 
        // from the application event. So there is no seperate method to get all the milestones fro the related order. If you want to build the reusability 
        // code, segrate the logic in to methods and move to helper. 
        //Added for the MileStone Activity Section
        action.setCallback(this, function(returnVal){
            var mDate = returnVal.getReturnValue() ;
            component.set("v.mileStoneDates",mDate); 
            console.log('-----------------mDate',mDate) ;
            console.log('-----------------mDate.isOnHold',+mDate.isOnHold) ;
            component.set('v.isOnHold',mDate.isOnHold) ;
            console.log('The milestone Dates are ',mDate);
            component.set("v.mileStoneDates",mDate); 
            var state  = returnVal.getState() ;
            console.log(state) ;
            var a = component.get('c.processMileStones');
            a.setParams({
                "component" : component
            });            
            $A.enqueueAction(a);
            
        });
        $A.enqueueAction(action);
    }
})