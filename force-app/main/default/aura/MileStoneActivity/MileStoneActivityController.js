({
    doInit2: function(component, event, helper) {
        console.log("111");
        component.set("v.toggleSpinner", true);
        var device = $A.get("$Browser.formFactor");
        var actWrapper = component.find("activityWrapper");
        if(device == "PHONE") {
            $A.util.addClass(actWrapper, 'hideDiv');
        } else {
            $A.util.removeClass(actWrapper, 'hideDiv');
        }
        
    },
	handleMileStoneEvent : function(component, event, helper) {
        
        var getCurrentMileStoneDate = event.getParam("MileStoneDate") ;
        var getMileStoneStage = event.getParam("MileStoneStages");
        var reinstallDate = event.getParam("ReInstallDate")
        console.log('ReInstallDate---',reinstallDate);
         var getTechMeasureDate = event.getParam("getTechMeasureDate")
        console.log('getTechMeasureDate---',getTechMeasureDate);
        var getPreviousMileStoneDate = event.getParam("SecondMileStoneDate") ;
        console.log('Current stage is :' + getMileStoneStage);
        
        if(getMileStoneStage == 'Survey Sent') // SURVEY STAGE
        {
            component.set('v.mileStoneCurrentActivity', '');
            component.set('v.mileStoneActivityCurrentStageContent','<br/><b class="title">Survey</b> <br/> The name Renewal by Andersen is synonymous with quality and care. At Renewal by Andersen, we promise to be there for you – two weeks after installation or two decades <a href="https://www.renewalbyandersen.com/homeowners/warranty">Click here</a> to find more information about our limited warranty. It\’s our promise to keep the lines of communication open, and to listen to your feedback, questions, and concerns.') ;
           	component.set('v.mileStoneNextActivity', '');
            component.set('v.MileStoneNextStageActivityContent','<br/> Thank you for your business.');
        }
        
        else if(getMileStoneStage == 'Re-Install' ) // RE-INSTALLATION,INSTALLATION & SURVEY STAGES
        {
             component.set('v.mileStoneCurrentActivity', 'Current Activity');
             component.set('v.mileStoneNextActivity', 'Next Activity');
             component.set('v.MileStoneNextStageActivityContent','<br/> <b class="title">Survey</b> <br/> The name Renewal by Andersen is synonymous with quality and care. At Renewal by Andersen, we promise to be there for you – two weeks after installation or two decades <a href="https://www.renewalbyandersen.com/homeowners/warranty">Click here</a> to find more information about our limited warranty. It\’s our promise to keep the lines of communication open, and to listen to your feedback, questions, and concerns.');

            if (new Date(reinstallDate) <= new Date()){
                component.set('v.mileStoneActivityCurrentStageContent','<br/><b class="title">Installation</b> </br> We want to make sure everything is just right, so we will be back to put the final touches on your installation!') ;
            } else {
            	component.set('v.mileStoneActivityCurrentStageContent','<br/><b class="title">Installation</b> </br> A great window is only as good as its installation– which is why our installers are truly the heroes of Renewal by Andersen’s full-service window replacement process. They will treat your home as if it were their own and clean up afterwards – including the disposal of your old windows!') ;
            }
        }
        else if(getMileStoneStage == 'Shipping Stage')
        {
            component.set('v.mileStoneCurrentActivity', 'Current Activity');
            component.set('v.mileStoneActivityCurrentStageContent','<br/><b class="title">Shipping</b> </br> Success! Your order has left our manufacturing facility and is on its way to you. As a reminder, if you have any questions before your installation, feel free to reach out through the ‘Contact Us’ page. We hope you are looking forward to installation day as much as we are!') ;
            component.set('v.mileStoneNextActivity', 'Next Activity');
            component.set('v.MileStoneNextStageActivityContent','<br/> <b class="title">Installation</b> <br/> A great window is only as good as its installation– which is why our installers are truly the heroes of Renewal by Andersen’s full-service window replacement process. They will treat your home as if it were their own and clean up afterwards – including the disposal of your old windows!');
        
        }
        else if(getMileStoneStage == 'Install Date') // INSTALLATION & SURVEY STAGES
        {
            component.set('v.mileStoneCurrentActivity', 'Current Activity');
            component.set('v.mileStoneActivityCurrentStageContent','<br/><b class="title">Installation</b> </br> A great window is only as good as its installation– which is why our installers are truly the heroes of Renewal by Andersen’s full-service window replacement process. They will treat your home as if it were their own and clean up afterwards – including the disposal of your old windows!') ;
            component.set('v.mileStoneNextActivity', 'Next Activity');
            component.set('v.MileStoneNextStageActivityContent','<br/> <b class="title">Survey</b> <br/> The name Renewal by Andersen is synonymous with quality and care. At Renewal by Andersen, we promise to be there for you – two weeks after installation or two decades <a href="https://www.renewalbyandersen.com/homeowners/warranty">Click here</a> to find more information about our limited warranty. It\’s our promise to keep the lines of communication open, and to listen to your feedback, questions, and concerns.');
        }
        else if(getMileStoneStage == 'Tech Measure') // CUSTOM BUILD & Installation
        {
            component.set('v.mileStoneCurrentActivity', 'Current Activity');
           	component.set('v.mileStoneActivityCurrentStageContent', '<br/><b class="title">Measure Appointment</b><br/> Think of your Project Technician as your personal construction resource. He or she will: <br/> •  Take detailed window and framing measurements <br/> •	Assess the condition of your frames, sash, casing, and trim  <br/> •	Note any climate concerns, potential structural issues, and aesthetics  <br/>•	Compare project specs and note any corrections  <br/>•	Ask – and answer – additional questions  <br/>•	Prepare you for your installation appointment');
            component.set('v.mileStoneNextActivity', 'Next Activity');
            component.set("v.MileStoneNextStageActivityContent",'<br/> <b class="title">Custom Build</b> <br/> We want to produce the best of the best for our customers, so every window in our factory is treated with care and attention to detail. Renewal by Andersen® windows are made from Fibrex® material, an Andersen-exclusive composite that combines the strength and stability of wood with the low-maintenance features of vinyl. <a href="https://www.renewalbyandersen.com/signature-service/an-exclusive-product">Click here</a> to read more about this exclusive product that will soon be in your home!');
            
        }
         else if(getMileStoneStage == 'Custom Build') // CUSTOM BUILD & Installation
        {  
            if(!event.getParam('showShippingStage'))
            {
            component.set('v.mileStoneCurrentActivity', 'Current Activity');
           	component.set('v.mileStoneActivityCurrentStageContent', '<br/><b class="title">Custom Build</b> <br/> We want to produce the best of the best for our customers, so every window in our factory is treated with care and attention to detail. Renewal by Andersen® windows are made from Fibrex® material, an Andersen-exclusive composite that combines the strength and stability of wood with the low-maintenance features of vinyl. <a href="https://www.renewalbyandersen.com/signature-service/an-exclusive-product">Click here</a> to read more about this exclusive product that will soon be in your home!');
            component.set('v.mileStoneNextActivity', 'Next Activity');
            component.set("v.MileStoneNextStageActivityContent",'<br/> <b class="title">Installation</b> </br> A great window is only as good as its installation– which is why our installers are truly the heroes of Renewal by Andersen’s full-service window replacement process. They will treat your home as if it were their own and clean up afterwards – including the disposal of your old windows!');
            }
            else
            {
             	component.set('v.mileStoneCurrentActivity', 'Current Activity');
                component.set('v.mileStoneActivityCurrentStageContent', '<br/><b class="title">Custom Build</b> <br/> We want to produce the best of the best for our customers, so every window in our factory is treated with care and attention to detail. Renewal by Andersen® windows are made from Fibrex® material, an Andersen-exclusive composite that combines the strength and stability of wood with the low-maintenance features of vinyl. <a href="https://www.renewalbyandersen.com/signature-service/an-exclusive-product">Click here</a> to read more about this exclusive product that will soon be in your home!');
                component.set('v.mileStoneNextActivity', 'Next Activity');
                component.set("v.MileStoneNextStageActivityContent",'<br/> <b class="title">Shipping</b> </br> Success! Your order has left our manufacturing facility and is on its way to you. As a reminder, if you have any questions before your installation, feel free to reach out through the ‘Contact Us’ page. We hope you are looking forward to installation day as much as we are!');
            }
        }
        else if(getMileStoneStage == 'Order Placed') // IN-HOME CONSULTATION & ORDER PLACED STAGES
        { 
		   	component.set('v.mileStoneCurrentActivity', 'Current Activity');
           	component.set('v.mileStoneActivityCurrentStageContent','<br/><b class="title">Order Placed</b><br/>Congratulations! Your replacement window order has been placed! While waiting  for your upcoming tech measure appointment, feel free to reach out to your design consultant with any questions you may have – they will be happy to help! ') ;        
           	component.set('v.mileStoneNextActivity', 'Next Activity');
           	component.set('v.MileStoneNextStageActivityContent', '<br/> <b class="title">Measure Appointment</b><br/> Think of your Project Technician as your personal construction resource. He or she will: <br/> •  Take detailed window and framing measurements <br/> •	Assess the condition of your frames, sash, casing, and trim  <br/> •	Note any climate concerns, potential structural issues, and aesthetics  <br/>•	Compare project specs and note any corrections  <br/>•	Ask – and answer – additional questions  <br/>•	Prepare you for your installation appointment') ;
        }
        component.set("v.toggleSpinner", false);   
	}
})