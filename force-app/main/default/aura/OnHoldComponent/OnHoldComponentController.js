({

    //determines which store is selected and shows hold information for the selected store
    displayHoldInfo: function (component, event, helper) {
    
        var totalHoldAction = component.get("c.getHolds");
        var holdsUsedAction = component.get("c.holdsUsed");

        totalHoldAction.setCallback(this, function(tHolds) {
            holdsUsedAction.setCallback(this, function(uHolds){
                var totalHolds = tHolds.getReturnValue();
                var usedList = uHolds.getReturnValue();
                var holdsUsed = usedList[0];
                var incompleteUsed = usedList[1];
                var holdsLeft = totalHolds - (holdsUsed + incompleteUsed);
                //if total holds is reduced by an admin this keeps the number from dropping below zero,
                if(holdsLeft < 0){
                    holdsLeft = 0;
                }
                component.set("v.totalHolds", totalHolds);
                component.set("v.holdsUsed", holdsUsed);
                component.set("v.holdsLeft", holdsLeft);
                component.set("v.incompleteUsed", incompleteUsed);
            });
        });
        $A.enqueueAction(totalHoldAction);
        $A.enqueueAction(holdsUsedAction);
    },

})