({
	sendToMedallia : function(component, event) {
		var recordId = component.get("v.recordId");
		var updateAction = component.get("c.updateStatus");
		var status = 'Send';
		updateAction.setParams({
			"recordId" : recordId,
			"status" : status
		});
		updateAction.setCallback(this, function(result){
			var state = result.getState();
			var resultStatus = result.getReturnValue();
			if(state === "SUCCESS"){
				if(resultStatus == 'already sent'){
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
				"title": "Notification",
				"message": "Survey already sent",
				"type": "info",
				});
				toastEvent.fire();
                } else if(resultStatus == 'locked'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    "title": "Notification",
                    "message": "Surveys in recission period or in a status cannot be updated",
                    "type": "error",
                    });
                    toastEvent.fire();
                } else {
					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
					"title": "Success!",
					"message": "Survey is Processing",
					"type": "success",
					});
					toastEvent.fire();
                	$A.get('e.force:refreshView').fire();
				}
			} 
			component.set("v.noEmail", false);
		});
		$A.enqueueAction(updateAction);

		window.setTimeout(
            $A.getCallback(function() {
                component.set("v.disable", false);
            }), 3000
        );
	},
})