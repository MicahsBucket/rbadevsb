({
	//determines if the survey record type is Post Appointment or Post Install
	init : function(component, event, helper){
		var recordId = component.get("v.recordId");
		var getRecordType = component.get("c.determineRecordType");
		getRecordType.setParams({
			"recordId" : recordId
		});
		getRecordType.setCallback(this, function(list){
            debugger;
			var returnList = list.getReturnValue();
			var isPostInstall = returnList[0];
			var isIncomplete = returnList[1];
            var isService = returnList[2];
            
			console.log('install: '+isPostInstall);
			console.log('incomplete: ' + isIncomplete);
			//hides parts of the component based on record type
			component.set("v.isPostInstall", isPostInstall);
			if(!isPostInstall && !isService){
				component.set("v.isPostAppointment", true);
			}
			
            component.set("v.isService",isService);
			component.set("v.isIncomplete", isIncomplete);
			
		});
		$A.enqueueAction(getRecordType);


	},

	//checks to see if the user has filled in email information and gives them a chance to add one if none exists
	onSendClick : function(component, event, helper){
		component.set('v.disable', true);
		var recordId = component.get("v.recordId");
		var recordAction = component.get("c.getSurvey");
		recordAction.setParams({
			"recordId" : recordId
		});
		recordAction.setCallback(this, function(survey){
			var piSurvey = survey.getReturnValue();
			component.set("v.surveyRecord", piSurvey);
			if(piSurvey.Primary_Contact_Email__c == null && piSurvey.Secondary_Contact_email__c == null){
				component.set("v.noEmail", true);
			} else {
				helper.sendToMedallia(component, event);
			}
		});
		$A.enqueueAction(recordAction);
		window.setTimeout(
            $A.getCallback(function() {
                component.set("v.disable", false);
            }), 3000
        );
	},

	//updates survey with email if the user entered one and then changes survey status to Send
	onEmailCheck : function(component, event, helper){
		component.set('v.disable', true);
		var email = component.find("email").get("v.value");
		var recordId = component.get("v.recordId");
		if(email != null){
			var saveEmail = component.get("c.editRecord");
			saveEmail.setParams({
				"email" : email,
				"recordId" : recordId
			});
			$A.enqueueAction(saveEmail);
		}
		helper.sendToMedallia(component, event);
	},

	//sets the survey status to hold and sends the user back to the list view
	onHoldClick : function(component, event, helper){
        component.set('v.disable', true);
		var recordId = component.get("v.recordId");
		var updateAction = component.get("c.updateStatus");
		var status = 'Hold';
		updateAction.setParams({
			"recordId" : recordId,
			"status" : status
		});
		updateAction.setCallback(this, function(holdsLeft){
			var result = holdsLeft.getReturnValue();
			if(result == 'hold limit'){
				var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Failed!",
                "message": "You have reached your Hold limit.  To put this survey on Hold, you must first send a different survey that is currently in Hold or Incomplete Data status",
                "type": "error",
                });
                toastEvent.fire();
			} else if(result == 'already held'){
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
				"title": "Notification",
				"message": "Survey already on hold",
				"type": "info",
				});
				toastEvent.fire();
			} else if(result == 'already sent'){
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
				"title": "Notification",
				"message": "Survey already sent",
				"type": "info",
				});
				toastEvent.fire();
			} else {
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
				"title": "Success!",
				"message": "Survey on hold",
				"type": "success",
				});
				toastEvent.fire();
				$A.get('e.force:refreshView').fire();	
			}
		});
		$A.enqueueAction(updateAction);
	},
})