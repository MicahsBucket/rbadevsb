({
    handleValidationToastEvent : function(component, event, helper) {
            console.log("in toast event aura controller.", event);
            let message = event.getParam('message');
            console.log('message in aura', message);
            let errorMessage = message;
            let errorTitle =    'Validation Error!';
            helper.showToast('error', errorTitle, errorMessage, null, null);     
        }
})