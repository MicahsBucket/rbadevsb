//
// Created by 7Summits - Joe Callin on 9/25/17.
//
({
    setLink: function (component) {
        var link = component.get('v.link');
        var text = component.get('v.text');
        if(!$A.util.isUndefinedOrNull(link) && !$A.util.isEmpty(link)) {
            if($A.util.isUndefinedOrNull(text) || $A.util.isEmpty(text)){
                component.set('v.text', link);
            }
            link = link.replace(/\s/g,'');
            if(link.indexOf('mailto:') !== -1 || link.indexOf('tel:') !== -1){
                component.set('v.setHref', true);
            }
            component.set('v.linkUrl', link);
            component.set('v.displayLink', true);
        }
    },

})