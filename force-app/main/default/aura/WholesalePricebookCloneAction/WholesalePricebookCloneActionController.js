({
    doInit: function(component, event, helper) {

        var wholesalePricebookId = component.get("v.recordId");

        component.set("v.newWholesalePricebook.Cloned_Wholesale_Pricebook__c", wholesalePricebookId);

        // Prepare the action to load related Pricing Configurations.
        var getRelatedPricingConfigurationsAction = component.get("c.getRelatedPricingConfigurations");
        getRelatedPricingConfigurationsAction.setParams({
            "wholesalePricebookId": wholesalePricebookId
        });

        // Configure response handler.
        getRelatedPricingConfigurationsAction.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.relatedPricingConfigurations", response.getReturnValue());
//                component.set("v.newWholesalePricebook.Effective_Date__c", Date.now());
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(getRelatedPricingConfigurationsAction);
    },

    handleSaveWholesalePricebook: function(component, event, helper) {

        component.set("v.showSpinner", true);

        // Prepare the action to create the new contact
        var saveWholesalePricebookAction = component.get("c.saveWholesalePricebook");
        saveWholesalePricebookAction.setParams({
            "newWholesalePricebook": component.get("v.newWholesalePricebook"),
            "includePricingConfigurations": component.get("v.cloneWithPricingConfigurations"),
            "pricingConfigurationList": component.get("v.relatedPricingConfigurations")
        });

        // Configure the response handler for the action
        saveWholesalePricebookAction.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {

                var newWholesalePricebookId = response.getReturnValue();

                // Prepare Toast Message.
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "message": "Wholesale Pricebook cloned successfully!",
                    "type": "success",
                    "mode": "pesky"
                });

                // Close the quick action panel and show the toast message.
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();

                // Navigate to the new Wholesale Pricebook record.
                var navigateEvent = $A.get("e.force:navigateToSObject");
                navigateEvent.setParams({
                    "recordId": newWholesalePricebookId,
                    "slideDevName": "detail"
                });
                navigateEvent.fire();
            }
            else if (state === "ERROR") {

                // Prepare Toast Message.
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "message": "Unexpected Error! Please try again.",
                    "type": "error",
                    "mode": "sticky"
                });

                // Show the toast message.
                resultsToast.fire();

                console.log('Problem cloning Wholesale Pricebook, response state: ' + state);
            }
            else {
                console.log('Unknown problem, response state: ' + state);
            }
        });

        // Send the request to create the new contact
        $A.enqueueAction(saveWholesalePricebookAction);
        
    },

	handleCancel: function(component, event, helper) {
	    $A.get("e.force:closeQuickAction").fire();
    }

})