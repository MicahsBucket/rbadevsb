({
	updateResult : function(component,event) {
        var params = event.getParam('arguments');
        var resultList = params.resultList;
        var errors = [];
        var isMakable = true;
        
        for(var i = 0; i<resultList.length;i++){
            if(resultList[i].isMakable == false){
                isMakable = false;
            }                         
            errors.push(...resultList[i].errorMessages);
        }
 //       console.log('result List in makability result controller', resultList);
//        console.log('errors array', errors);        
         component.set("v.productMakable", isMakable);		        
         component.set("v.errorsList", errors);		

	}
})