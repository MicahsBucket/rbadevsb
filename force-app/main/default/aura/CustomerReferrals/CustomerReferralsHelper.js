({
    doinit : function(component) {
        this.clearForm(component);        
    },
    submitReferral:function(component){
        var firstName = component.find('firstName').get('v.value');
        var lastName = component.find('lastName').get('v.value');
        var email = component.find('email').get('v.value');
        var phone = component.find('phone').get('v.value');        
        
        var action = component.get("c.sendReferral");        
        action.setParams({ 
            'fname' : firstName,
            'lname' : lastName,
            'phone' : phone,
            'email' : email        
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                this.successToast();
                this.clearForm(component);
                
            } else if (state === "INCOMPLETE"){
                console.log("callout did not finish");
            } else if (state === "ERROR"){
                var errors = response.getError();
                if(errors){
                    console.log("Error Message: "+ errors[0].message);
                    this.errorToast();
                }
            } else {
                console.log('unknown error');
                this.errorToast();              
            }  
        });
        $A.enqueueAction(action);
    },
    clearForm:function(component){
        var firstName = component.find('firstName');
        var lastName = component.find('lastName');
        var email = component.find('email');
        var phone = component.find('phone'); 
        firstName.set("v.value",null);
        lastName.set("v.value",null);
        email.set("v.value",null);
        phone.set("v.value",null);
        
        
    },
    successToast: function(){
        var toast = $A.get("e.force:showToast");
        toast.setParams({
            "title": "Success!",
            "message": "Your referral has been submitted, Thank you!",
            "type": "success",
            "duration":"3",
            "mode":"dismissible"
        });
        toast.fire();   
    },
    errorToast: function(){
        var toast = $A.get("e.force:showToast");
        toast.setParams({
            "title": "Error!",
            "message": "There was an error processing your referral, please try again.",
            "type": "error",
            "duration":"3",
            "mode":"dismissible"
        });
        toast.fire();
    },
    validateForm: function(component){
        var allFieldsValidated = true;
        var firstName = component.find('firstName');
        var firstNameValue = firstName.get('v.value');
        var lastName = component.find('lastName');
        var lastNameValue = lastName.get('v.value');
        var email = component.find('email');
        var emailValue = email.get('v.value');
        var phone = component.find('phone'); 
        var phoneValue = phone.get('v.value');
        
        if($A.util.isEmpty(firstNameValue)){
            firstName.set('v.validity',{valid:false,badInput:true});
            firstName.showHelpMessageIfInvalid();
            allFieldsValidated = false;
        }
        if($A.util.isEmpty(lastNameValue)){
            lastName.set('v.validity',{valid:false,badInput:true});
            lastName.showHelpMessageIfInvalid();
            allFieldsValidated = false;
        }
        if($A.util.isEmpty(emailValue)){
            email.set('v.validity',{valid:false,badInput:true});
            email.showHelpMessageIfInvalid();
            allFieldsValidated = false;
        }        
        if(!$A.util.isEmpty(emailValue)){
            if(email.get('v.validity').valid == false){
                console.log('validity in email field',email.get('v.validity').valid );
                allFieldsValidated = false;
            }
        }
        if(!$A.util.isEmpty(phoneValue)){
            if(phone.get('v.validity').valid == false){   
                console.log('validity in phone field',phone.get('v.validity').valid );
                allFieldsValidated = false;  
            }
        }
        if(allFieldsValidated) {
            this.submitReferral(component);
        }  
    },
    
    
})