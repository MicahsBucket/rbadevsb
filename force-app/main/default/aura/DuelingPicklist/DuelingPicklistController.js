({  
    //grabs all available and selected installers and puts them into seperate lists as select options
    doInit : function(component, event, helper)
    {
        var recordId = component.get("v.recordId");
        var activeInstallersAction = component.get("c.getAllAvailableInstallers");
        activeInstallersAction.setParams({
            "recordId":recordId
        })
        activeInstallersAction.setCallback(this, function(e){
            if(e.getState()==='SUCCESS')
            {
                var leftOption = component.find('leftOptions');
                leftOption.set("v.options",e.getReturnValue());
            }
        });
        $A.enqueueAction(activeInstallersAction);

        var selectedInstallersAction = component.get("c.getAllSelectedInstallers");
        selectedInstallersAction.setParams({
            "recordId":recordId
        })
        var rightOptions = component.find('rightOptions');
        selectedInstallersAction.setCallback(this, function(e){
            if(e.getState()==='SUCCESS')
            {
                var workers = e.getReturnValue();
                var rightValues = [];
                rightOptions.set("v.options",workers);
                for(var i = 0; i < workers.length; i++){
                    rightValues.push(workers[i].value);
                }

                var updateInstallers = component.getEvent("updateInstallers");
                updateInstallers.setParams({
                "installers":rightValues
                });
                updateInstallers.fire();
            }
        });
        $A.enqueueAction(selectedInstallersAction);



    },

    //moves installers from selected to available
    moveLeft : function(component, event, helper)
    {
        var leftComp = component.find('leftOptions');
        var rightComp = component.find('rightOptions');
        var rightOptions = rightComp.get('v.options');
        var rightValue = rightComp.get('v.value');
        if(!rightValue)return;
        var leftOptions = leftComp.get('v.options');
        if(!leftOptions)
            leftOptions=[];

        var values = rightValue.split(';');
        for(var i=0;i<values.length;i++)
        {
            for(var j=0;j<rightOptions.length;j++)
            {
                if(values[i]===rightOptions[j].value)
                {
                    leftOptions.push({'label':rightOptions[j].label,
                                       'value':rightOptions[j].value});
                    rightOptions.splice(j,1);
                    break;
                }
            }
        }
        rightComp.set('v.options',rightOptions);
        if(leftOptions.length>0)
        {
            leftComp.set('v.options',leftOptions);
        }

        var installerIds = []
        for(var i = 0; i < rightOptions.length; i++){
            installerIds.push(rightOptions[i].value);
        }

        //sends new list to NewManualPostInstall.cmp
        var updateInstallers = component.getEvent("updateInstallers");
        updateInstallers.setParams({
        	"installers":installerIds
   		});
        updateInstallers.fire();
    },

    //moves installers from available to selected
    moveRight : function(component, event, helper)
    {

        var leftComp = component.find('leftOptions');
        var rightComp = component.find('rightOptions');
        var leftOptions = leftComp.get('v.options');
        var leftValue = leftComp.get('v.value');
        if(!leftValue)
            return;
        var rightOptions = rightComp.get('v.options');
        if(!rightOptions)
            rightOptions = [];
       	if(leftValue!='undefined'&& leftValue!='')
        {
            var values = leftValue.split(';');
			for(var i=0;i<values.length;i++)
            {
                for(var j=0;j<leftOptions.length;j++)
                {
                    if(values[i]===leftOptions[j].value)
                    {
                        rightOptions.push({'label':leftOptions[j].label,
                                           'value':leftOptions[j].value});
                        leftOptions.splice(j,1);
                        break;
                    }
                }
            }
            leftComp.set('v.options',leftOptions);
        }
        if(rightOptions.length>0)
        {
            rightComp.set('v.options',rightOptions);
        }

        var installerIds = []
        for(var i = 0; i < rightOptions.length; i++){
            installerIds.push(rightOptions[i].value);
        }

        //sends new list to NewManualPostInstall.cmp
        var updateInstallers = component.getEvent("updateInstallers");
        updateInstallers.setParams({
        	"installers":installerIds
   		});
        updateInstallers.fire();
    },

})