({
	getOrderItems : function(component, event, helper) {
        var allFieldsValidated = true;
        var orderNumber = component.find('orderNumber');
        var orderNumberValue = orderNumber.get('v.value');       
        if($A.util.isEmpty(orderNumberValue)){
            orderNumber.set('v.validity',{valid:false,badInput:true});
            orderNumber.showHelpMessageIfInvalid();
            allFieldsValidated = false;
        }
        if(allFieldsValidated){
		helper.getAllOrderItems(component);            
        }
	},
    validateOrder : function(component,event,helper){
        helper.validateOrderItem(component,event);
     },
})