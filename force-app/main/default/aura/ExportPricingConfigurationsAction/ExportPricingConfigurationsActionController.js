({
    doInit: function(component, event, helper) {

        // Prepare action to get List of Pricing Configuration
        // records related to the Wholesale Pricebook.
        var getPricingConfigurationsAction = component.get("c.getPricingConfigurations");
        getPricingConfigurationsAction.setParams({
            "wholesalePricebookId": component.get("v.recordId")
        });

        // Configure the response handler for the action.
        getPricingConfigurationsAction.setCallback(this, function(response) {

            // Make sure everything is find and grab the return value.
            var state = response.getState();
            var returnWrapper = response.getReturnValue();

            var resultsToast = $A.get("e.force:showToast");
            if (component.isValid() && state === "SUCCESS") {

                // If everything is fine, create the CSV
                // and download it to the User's desktop.
                var csvString = helper.createCSVFile(component,returnWrapper);
                if (csvString === '') {

                    // Prepare Toast Message.
                    resultsToast.setParams({
                        "message": "Unable to export CSV file! Please try again.",
                        "type": "error",
                        "mode": "sticky"
                    });

                }
                else {
                    
                    // Create a temp href (link) tag to download the CSV file.
                    var hiddenElement = document.createElement('a');
                    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
                    hiddenElement.target = '_self';
                    hiddenElement.download = 'ExportData.csv';
                    document.body.appendChild(hiddenElement);
                    hiddenElement.click(); // using click() js function to download csv file

                    // Prepare Toast Message.
                    resultsToast.setParams({
                        "message": "Export Successful!",
                        "type": "success",
                        "mode": "pesky"
                    });

                }

            }
            else if (state === "ERROR") {

                // Prepare Toast Message.
                resultsToast.setParams({
                    "message": "Unexpected Error! Please try again.",
                    "type": "error",
                    "mode": "sticky"
                });

            }
            else {
                console.log('Unknown problem, response state: ' + state);
            }

            // Close the Quick Action window and show the Toast.
            $A.get("e.force:closeQuickAction").fire();
            resultsToast.fire();

        });

        // Execute the action.
        $A.enqueueAction(getPricingConfigurationsAction);
    }

})