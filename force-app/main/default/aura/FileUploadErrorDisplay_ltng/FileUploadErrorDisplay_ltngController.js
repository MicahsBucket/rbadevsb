({
     doInit : function(component, event) {
         var action = component.get("c.returnErrorMessage");
         action.setParams({ "recordId" : component.get("v.recordId") });
         action.setCallback(this, function(response) {
              var state = response.getState();
             if (state === "SUCCESS"){ 
                 console.log('the message'+JSON.stringify(response.getReturnValue()));
                 //alert(JSON.stringify());
                 var warningmessage=[];
                 if(response.getReturnValue()==null)
                 {
                     component.set("v.displayFlag",false); 
                 }
                 else
                 {   var str=response.getReturnValue();
                  if(str.WarningMessage__c==null || str.WarningMessage__c==="")
                  {
                      if(str.ErrorMessage__c)
                      {
                    var errormessage=str.ErrorMessage__c.split("\r\n");  
                      
                    for (var i = 0; i < errormessage.length; i++){
               if (errormessage[i].includes("is present within")){ 
                warningmessage.push(errormessage[i])     
                     }
                    }
                      }
                  }
                  else
                  {  
                      if(str.WarningMessage__c)
                      {
                    var warningmessage=str.WarningMessage__c.split("\r\n")
                    }
                      if(str.ErrorMessage__c)
                      {
                     var errormessage=str.ErrorMessage__c.split("\r\n");
                      }
                  }
                     component.set("v.messages",warningmessage);
                     component.set("v.Warningmessages",errormessage);
                 }
      } 
    else {
      console.log(state);
    }
         });
         $A.enqueueAction(action);
     }
 })