({
    closeQuickAction: function(component, event, helper) {
        $A.get('e.force:closeQuickAction').fire();
    },
    
    handleCancelClick: function(component, event, helper) {
        $A.get('e.force:closeQuickAction').fire();
    },

    handleDisableSave: function(component, event, helper) {
        component.set('v.disableSaveButton', true);
    },

    handleEnableSave: function(component, event, helper) {
        component.set('v.disableSaveButton', false);
    },

    handleSaveClick: function(component, event, helper) {
        var cancelOrderAction = component.find('cancelOrderAction');
        cancelOrderAction.handleSaveClick();
        
    },

    hideSpinner: function(component, event, helper) {
        component.set('v.showSpinner', false);
        component.set('v.disableCancelButton', false);
        component.set('v.disableSaveButton', false);
	},
    
	showSpinner: function(component, event, helper) {
        component.set('v.showSpinner', true);
        component.set('v.disableCancelButton', true);
        component.set('v.disableSaveButton', true);
	},
    
	showToast: function(component, event, helper) {

        var toastMessage = event.getParam('message');
        var toastType = event.getParam('type');
      
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            message: toastMessage,
            type: toastType
        });
        toastEvent.fire();
        $A.get('e.force:refreshView').fire();
	}
    
})