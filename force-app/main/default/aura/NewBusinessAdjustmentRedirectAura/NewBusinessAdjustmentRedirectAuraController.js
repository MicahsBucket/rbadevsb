({

    initializeComponent: function(component, event, helper) {

        var recordId = '';
        var objectName = '';

        // Find the Parent Id and Object Name.  This only works for Desktop Users.
        var pageReference = component.get("v.pageReference");
        var state = pageReference.state; 
        var context = state.inContextOfRef;
        if (context.startsWith("1\.")) {

            context = context.substring(2);
            var addressableContext = JSON.parse(window.atob(context));
            console.log('addressableContext',JSON.stringify(addressableContext));

            recordId = addressableContext.attributes.recordId;
            objectName = addressableContext.attributes.objectApiName;

        }

        var getRecordDataAction = component.get("c.getRecordData");
        getRecordDataAction.setParams({
            'recordId': recordId,
            'objectName': objectName
        });

        // Configure the response handler for the action
        getRecordDataAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state: ', state)
            if (state === "SUCCESS") {

                var record = response.getReturnValue();
                console.log('record: ', record)
                // Navigate to new Business_Adjustment__c record.
                var createPaymentEvent = $A.get("e.force:createRecord");
                let today = Date.now();
                createPaymentEvent.setParams({
                    "entityApiName": "Business_Adjustment__c",
                    "recordTypeId": record.recordTypeId,
                    "defaultFieldValues": {
                        'Order__c': record.OrderId,
                        'Contact__c': record.OrderBillToContactId,
                        'Store_Location__c': record.OrderStoreLocation,
                        'Date__c': record.todaysDate
                    }
                });
                createPaymentEvent.fire();

            }
            else if (state === "ERROR") {
                console.log('Problem retrieving Record, response state: ' + state);
            }
            else {
                console.log('Unknown problem, response state: ' + state);
            }

        });

        // Send the request to get Record data.
        $A.enqueueAction(getRecordDataAction);    

    }

})