//
// Created by 7Summits - Joe Callin on 10/2/17.
//
({
    loadLinkList: function (component) {
        var linkList = [];
        for(var i = 1; i <= 10; i++){
            var linkText = component.get('v.linkText' + i);
            var linkUrl = component.get('v.linkUrl' +  i);
            if(!$A.util.isUndefinedOrNull(linkUrl) && !$A.util.isEmpty(linkUrl)){
                linkList.push({
                    text: linkText,
                    url: linkUrl
                });
            }
        }
        component.set('v.linkList', linkList);
    },
    setAlignment: function(component) {
        var alignment = component.get('v.alignment');
        if(alignment === 'left'){
            alignment = ''
        }else if(alignment === 'right'){
            alignment = 'end';
        }
        component.set('v.alignment', alignment);
    }
})