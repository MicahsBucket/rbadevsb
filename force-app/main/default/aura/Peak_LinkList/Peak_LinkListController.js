//
// Created by 7Summits - Joe Callin on 10/2/17.
//
({
    init: function (component, event, helper) {
        var linkList = component.get('v.linkList');
        if($A.util.isUndefinedOrNull(linkList) || $A.util.isEmpty(linkList)){
            helper.loadLinkList(component);
        }
        helper.setAlignment(component);
        component.set('v.isInit', true);
    }
})