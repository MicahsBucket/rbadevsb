({
	init : function(component, event, helper) {
        helper.getContactInfo(false, component, event, helper);
		var gtUserId = component.get("c.getCurrentUserId");
		 gtUserId.setCallback(this, function(returnVal){             
             component.set("v.currentUserId",returnVal.getReturnValue());
            
         });
        $A.enqueueAction(gtUserId);	   
	},

    //toggles visibility to create new contact
	toggleNewContact : function(component, event, helper) {
		var show = component.get("v.newContactBool");
		if(show == false){
			component.set("v.newContactBool", true);
		} else {
			component.set("v.newContactBool", false);
		}
	},

    updateSecContacts : function(component, event, helper) {
        //Secondary contact info
        var newContact = component.get('v.newContactBool');
        var isSecond = component.get('v.secondBool');

        if(isSecond){
            console.log('The secondary contact update*******');
            helper.updateSecondaryContact(component, event, helper);
        } else if(newContact){
            var secFirstName = component.find('secFirstName').get('v.value');
            var secLastName = component.find('secLastName').get('v.value');
            if(secFirstName != null && secFirstName != undefined && secFirstName != '' && secLastName != null && secLastName != undefined && secLastName != ''){
                helper.createSecondaryContact(component, event, helper);
                component.set('v.secondBool', true);
                component.set('v.newContactBool', false);
            } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Failure!",
                "message": "The Secondary Contact failed to Save",
                "type": "Error",
                });
                toastEvent.fire();
                return;
            }
        }

    },

    clickPhone : function(component, event, helper){
        document.getElementById('phoneLink').click();
    },
    
    createSecContacts : function(component,event,helper){
        console.log('inside create Sec Contacts');
        helper.createSecondaryContact(component,event,helper);
    },
    eventOrder : function(component, event, helper){
     helper.getSecondaryContact(component, event, helper);
        console.log("fire Event for the Secondary :");
        
        
    },
})