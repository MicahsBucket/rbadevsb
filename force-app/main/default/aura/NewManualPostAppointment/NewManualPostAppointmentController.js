({
    /*after new button is pressed the fields to enter a
    new survey are displayed and the list of sales reps is populated*/
    displayNewSurvey : function(component, event, helper) {
        component.set("v.newSurvey", true);
        component.set("v.hideNew", false);
        var setSalesReps = component.get("c.getWorkers");
        var salesRole = 'Sales Rep';

        setSalesReps.setParams({
                  "workerRole": salesRole
              });
        setSalesReps.setCallback(this, function(salesReps){
            var salesRepList = salesReps.getReturnValue();
            component.set("v.salesReps", salesRepList);

        });
        $A.enqueueAction(setSalesReps);
    },

    setSalesRep : function(component, event, helper){
        var salesRep = event.getSource().get("v.value");
        component.set("v.selectedSalesRep", salesRep);
    },

    //saves but doesnt send survey
    saveSurvey : function(component, event, helper){
        component.set("v.disable", true);
        component.set("v.surveyStatus", "Pending");
        var firstName = component.find('firstName').get("v.value");
        var lastName = component.find('lastName').get("v.value");
        var email = component.find('email').get("v.value");
        var phone = component.find('phone').get("v.value");

        //email validation
        if(email != null && email != ""){
            var atpos = email.indexOf("@");
            var dotpos = email.lastIndexOf("."); 
            if(atpos < 1 || ( dotpos - atpos < 2 )){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Failure!",
                "message": "The email format you entered is invalid",
                "type": "error",
                });
                toastEvent.fire();
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.disable", false);
                    }), 3000
                );
                return;
            } 
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
            "title": "Failure!",
            "message": "The survey failed to save, ensure required fields are filled",
            "type": "error",
            });
            toastEvent.fire();
            window.setTimeout(
                $A.getCallback(function() {
                    component.set("v.disable", false);
                }), 3000
            );
            return;
        }
        if(firstName != "" && lastName != ""){
            helper.submitSurvey(component, event, helper);
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
            "title": "Failure!",
            "message": "The survey failed to save, ensure required fields are filled",
            "type": "error",
            });
            toastEvent.fire();
            window.setTimeout(
                $A.getCallback(function() {
                    component.set("v.disable", false);
                }), 3000
            );
        }
    },

    //saves and sends survey
    sendSurvey : function(component, event, helper){
        debugger;
        component.set("v.disable", true);
        component.set("v.surveyStatus", "Send");
        var firstName = component.find('firstName').get("v.value");
        var lastName = component.find('lastName').get("v.value");
        var email = component.find('email').get("v.value");
        
        //email validation
        if(email != null && email != ""){
            var atpos = email.indexOf("@");
            var dotpos = email.lastIndexOf(".");
            if(atpos < 1 || ( dotpos - atpos < 2 )){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Failure!",
                "message": "The email format you entered is invalid",
                "type": "error",
                });
                toastEvent.fire();
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.disable", false);
                    }), 3000
                );
                return;
            } 
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
            "title": "Failure!",
            "message": "The survey failed to save, ensure required fields are filled",
            "type": "error",
            });
            toastEvent.fire();
            window.setTimeout(
                $A.getCallback(function() {
                    component.set("v.disable", false);
                }), 3000
            );
        }
        

        var phone = component.find('phone').get("v.value");
        var salesRep = component.get("v.selectedSalesRep");
        var apptResult = component.find('appointmentResult').get('v.value');
        var appointmentDate = component.find('appointmentDate').get("v.value");
        var today = new Date();
        var today1 = new Date();
        today1.setDate(today.getDate());
        var yesterday = new Date();
        var twoDaysAgo = new Date();
        var threeDaysAgo = new Date();
        yesterday.setDate(today.getDate()-1);
        twoDaysAgo.setDate(today.getDate()-2);
        threeDaysAgo.setDate(today.getDate()-3);
        today = helper.getDate(today, component, event, helper);
        yesterday = helper.getDate(yesterday, component, event, helper);
        twoDaysAgo = helper.getDate(twoDaysAgo, component, event, helper);
        threeDaysAgo = helper.getDate(threeDaysAgo, component, event, helper);
        var recissionWarning = component.get('v.recissionWarning');
        if(apptResult == 'Sale'){
            if(appointmentDate == today || appointmentDate == yesterday || appointmentDate == twoDaysAgo || appointmentDate == threeDaysAgo){
                if(recissionWarning){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Warning",
                        "message": "This order is still within the recision period",
                        "type": "info",
                    });
                    toastEvent.fire();
                    component.set('v.recissionWarning', false);
                    window.setTimeout(
                        $A.getCallback(function() {
                            component.set("v.disable", false);
                        }), 3000
                    );
                    return;
                }
            }             
        }
            
        var dateCompareBool = helper.dateComparer(today1, appointmentDate, component, event, helper);
        if(dateCompareBool){
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Failure!",
                    "message": "You can't send a survey for an appointment that hasn't occured yet",
                    "type": "error",
                });
                toastEvent.fire(); 
                    window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.disable", false);
                    }), 3000
                );
                return;
        }
        if(firstName != "" && lastName != "" && email != null && email != "" && salesRep != null && apptResult != null && appointmentDate != null && appointmentDate != ''){
                helper.submitSurvey(component, event, helper);
            } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Failure!",
                    "message": "The survey failed to send, ensure required fields are filled",
                    "type": "error",
                });
                toastEvent.fire();
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.disable", false);
                    }), 3000
                ); 
            }    
    },

})