({
	clearFields : function(component, event) {
		var firstName = component.find("firstName").set("v.value", "");
        var lastName = component.find("lastName").set("v.value", "");
        var email = component.find("email").set("v.value", "");
        var phone = component.find("phone").set("v.value", "");
        var appointmentDate = component.find("appointmentDate").set('v.value', null);
	},

	//aggregates survey data and sends it to the apex controller
	submitSurvey : function(component, event, helper){
		var firstName = component.find('firstName').get("v.value");
		var lastName = component.find('lastName').get("v.value");
		var email = component.find('email').get("v.value");
		var phone = component.find('phone').get("v.value");
		var appointmentDate = component.find('appointmentDate').get("v.value");
		var appointmentResult = component.find('appointmentResult').get("v.value");
		var salesRep = component.get("v.selectedSalesRep");
		var saveAction = component.get("c.savePAS");
		var surveyStatus = component.get("v.surveyStatus");
		saveAction.setParams({
			"firstName" : firstName,
			"lastName" : lastName,
			"email" : email,
			"phone" : phone,
			"appointmentDate" : appointmentDate,
			"appointmentResult" : appointmentResult,
			"salesRep" : salesRep,
			"surveyStatus" : surveyStatus
		});
		saveAction.setCallback(this, function(response){
			var state = response.getState();
           if(state === "SUCCESS"){
            //needs to be two variables because on page load neither should show otherwise they act like the same boolean
            //resets fields after succesful save
            helper.clearFields(component, event);
            var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
				"title": "Success!",
				"message": "The record has been saved successfully.",
				"type": "success",
				});
				toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            component.set("v.newSurvey", false);
            component.set("v.hideNew", true);
           } else {
            //shows error message if save fails
            var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
				"title": "Failure!",
				"message": "The survey failed to save",
				"type": "error",
				});
				toastEvent.fire();
				window.setTimeout(
                $A.getCallback(function() {
                    component.set("v.disable", false);
                }), 3000
            );
           }
		});
		$A.enqueueAction(saveAction);
	},
    
    getDate: function(dateVal, component, event, helper){
        var dd = dateVal.getDate();
		var mm = dateVal.getMonth()+1; 
		var yyyy = dateVal.getFullYear();

		if(dd<10) {
  		  dd = '0'+dd
		} 
		if(mm<10) {
  		  mm = '0'+mm
		} 

		dateVal = yyyy + '-' + mm + '-' + dd;
       return dateVal;
    },

    dateComparer: function(today, appointmentDate, component, event, helper){
    	var todaydd = today.getDate();
    	var todaymm = today.getMonth()+1;
    	var todayyyyy = today.getFullYear();
    	var apptdd = parseInt(appointmentDate.substring(8,10));
    	var apptmm = parseInt(appointmentDate.substring(5,7));
    	var apptyyyy = parseInt(appointmentDate.substring(0,4));
    	if(todayyyyy < apptyyyy){
    		return true;
    	} else if(todayyyyy == apptyyyy){
    		if(todaymm < apptmm){
    			return true;
    		} else if(todaymm == apptmm){
    			if(todaydd < apptdd){
    				return true;
    			}
    		} 
    	} else {
    		return false;
    	}
    }
})