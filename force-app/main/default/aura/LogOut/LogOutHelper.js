({
	doInit : function(component) {
        var action = component.get("c.getSitePrefix");
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var siteUrl = response.getReturnValue();
             //   console.log("response in success " + siteUrl);
                window.location.replace(siteUrl+"/secur/logout.jsp");
                
            } else if (state === "INCOMPLETE"){
              //  console.log("callout did not finish");
            } else if (state === "ERROR"){
                var errors = response.getError();
                if(errors){
                    console.log("Error Message: "+ errors[0].message);
                }
            } else {
                console.log('unknown error');
            }  
        });
        $A.enqueueAction(action);   	
	},
})