/*
* Copyright (c) 2018. 7Summits Inc.
* Created by 7Summits - Joe Callin on 5/29/18.
*/
({
    init: function(component, event, helper) {
        helper.buildList(component, event, helper);
    },
    linkClick: function(component, event, helper) {
        var url = event.currentTarget.dataset.url;
        helper.goToLink(component, event, helper, url);
    },
})