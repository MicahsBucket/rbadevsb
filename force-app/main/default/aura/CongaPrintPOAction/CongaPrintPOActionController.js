({
    initializeComponent : function(component, event, helper) {

        var action = component.get("c.getCongaPrintUrl");
        action.setParams({"purchaseOrderId": component.get("v.recordId")});

        // Configure the response handler for the action
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var congaPrintUrl = response.getReturnValue();
                if (congaPrintUrl !== '') {
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                      "url": congaPrintUrl
                    });
                    urlEvent.fire();
                }

            }
            else if (state === "ERROR") {
                console.log('Problem saving contact, response state: ' + state);
            }
            else {
                console.log('Unknown problem, response state: ' + state);
            }

            // Close the Quick Action window
            $A.get("e.force:closeQuickAction").fire();

        });

        // Send the request to create the new contact
        $A.enqueueAction(action);
    }
})