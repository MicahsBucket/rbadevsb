({
    //finds the name of the tech measure and the installer
	getWorkerName: function (orderVal, installerBool, workerVariable, component, event, helper) {
        var order = orderVal;
        var findWorker = component.get('c.getWorker');
        findWorker.setParams({
            'isInstaller' : installerBool,
            'ord' : order
        })
        findWorker.setCallback(this, function(returnVal){
            var workerName = returnVal.getReturnValue();
            component.set(workerVariable, workerName);
        })
        $A.enqueueAction(findWorker);
	},

    //finds the current Order and updates worker names
    findOrder: function(order, component, event, helper){
		 //alert('inside find Order');							 
        var action = component.get('c.getOrder');
        action.setParams({
           'orderId' : order
        })
        action.setCallback(this, function(returnVal){
            var orderVal = returnVal.getReturnValue();
            component.set('v.order', orderVal);
            helper.getWorkerName(orderVal, true, 'v.installerName', component, event, helper);
            helper.getWorkerName(orderVal, false, 'v.techMeasurerName', component, event, helper);
        })
        $A.enqueueAction(action);
    },

    //finds and sets the Sales Reps Number
    setNumber: function (order, component, event, helper){
        var getSalesRepNumber = component.get('c.getNumber');
        getSalesRepNumber.setParams({
            "orderId" : order
        });
        getSalesRepNumber.setCallback(this, function(returnVal){
            var salesRep = returnVal.getReturnValue();
            component.set('v.salesRepEmail', salesRep.Email);
            component.set('v.salesRepNumber', salesRep.MobilePhone);
        });
        $A.enqueueAction(getSalesRepNumber);
    },
    
    getOrderUser : function (order, component, event, helper){
    	var action = component.get('c.getUsers');
        action.setParams({
            'orderId' : order
        })
        //alert('Inside 2');	
        action.setCallback(this, function(returnVal){
            var allUsersNew = returnVal.getReturnValue();
            //alert('The retvalue is '+allUsersNew);
            var splitString=allUsersNew.split('@@');
            var salesrep= splitString[0];
            var techmeasure= splitString[1];
            var installer = splitString[2];
            var techMeasureLabel = splitString[3];
            var primaryInstallerLabel = splitString[4];
            if(!$A.util.isEmpty(salesrep))
                component.set("v.salesrep",salesrep);
            if(!$A.util.isEmpty(techmeasure))
                component.set("v.techmeasure",techmeasure);
            if(!$A.util.isEmpty(installer))
                component.set("v.installer",installer);
            if(!$A.util.isEmpty(techMeasureLabel))
                component.set("v.techMeasureLabel",techMeasureLabel);
            if(!$A.util.isEmpty(primaryInstallerLabel))
                component.set("v.primaryInstallerLabel",primaryInstallerLabel);
            
        });
        $A.enqueueAction(action);  	 
    }
})