({
	init: function (component, event, helper) {
        var orderId = component.get('v.orderId');
        //finds the order based on the order Id passed from Collapsable Content Component
       console.log("@@@@MiddleComponent OrderId" ,component.get("v.OrderStoreConfigWrap.primaryInstaller"));
       console.log("@@@@SalesRep" ,component.get("v.OrderStoreConfigWrap.salesRepImage")); 
        helper.findOrder(orderId, component, event, helper);
		 //alert('Inside 1 ' +orderId );
        var action = component.get('c.getUsers');
        action.setParams({
            'orderId' : orderId
        })
        //alert('Inside 2');	
        action.setCallback(this, function(returnVal){
            var allUsersNew = returnVal.getReturnValue();
            //alert('The retvalue is '+allUsersNew);
            var splitString=allUsersNew.split('@@');
            var salesrep= splitString[0];
            var techmeasure= splitString[1];
            var installer = splitString[2];
            if(!$A.util.isEmpty(salesrep))
                component.set("v.salesrep",salesrep);
            if(!$A.util.isEmpty(techmeasure))
                component.set("v.techmeasure",techmeasure);
            if(!$A.util.isEmpty(installer))
                component.set("v.installer",installer);
            
        });
        $A.enqueueAction(action);
        //sets the sales reps phone number
        helper.setNumber(orderId, component, event, helper);
	},
	 handleComponentEvent: function(component,event,helper){
        var message = event.getParam("ChangeOrderId");        
        //alert('The order Id received is '+message);
		component.set("v.orderId", message);
        helper.findOrder(message, component, event, helper);
        helper.getOrderUser(message, component, event, helper);
    },

})