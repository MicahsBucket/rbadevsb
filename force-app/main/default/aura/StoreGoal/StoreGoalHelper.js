({
	getData : function(cmp) {
        var action = cmp.get('c.getGoals');
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set('v.StoreGoal', response.getReturnValue());
                console.log(response);
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    },
    
    getStoreName : function(cmp) {
        var action = cmp.get('c.getStoreName');
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set('v.StoreName', response.getReturnValue());
                console.log('storeName success: ' + response);
            }
            else if( state === "ERROR") {
                var errors = response.getError();
                console.error('error: ' + errors);
            }
        }));
        $A.enqueueAction(action);
    },
    
    getUserStoreConfiguration : function(cmp) {
    	var action = cmp.get('c.getUserStoreConfiguration');
        action.setCallback(this,$A.getCallback(function (response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                cmp.set("v.Store_Configuration__c", response.getReturnValue());
            }
            else if(state === "ERROR") {
                var errors = resposne.getError();
                console.error('Error: ' + errors);
            }
        }));
        $A.enqueueAction(action);
    },
    
    getYear : function(month) {
        var year = '1234';
        var currentMonth = $A.localizationService.formatDate(new Date(), "MM");
        var currentYear = $A.localizationService.formatDate(new Date(), "yyyy");
        switch(month) {
            case 'January' :
                year = parseInt(currentYear) + 1;
                break;
            case 'February':
                if(currentMonth == '1') {
                    year = currentYear;
                }
                else {
                    year = parseInt(currentYear) + 1;
                }
                break;
            case 'March':
                if(currentMonth == '1' || currentMonth == '2') {
                    year = currentYear;
                }
                else {
                    year = parseInt(currentYear) + 1;
                }
                break;
            case 'April':
                if(currentMonth == '1' || currentMonth == '2' || currentMonth == '3') {
                    year = currentYear;
                }
                else {
                    year = parseInt(currentYear) + 1;
                }
                break;
            case 'May':
                if(currentMonth == '1' || currentMonth == '2' || currentMonth == '3' || currentMonth == '4') {
                    year = currentYear;
                }
                else {
                    year = parseInt(currentYear) + 1;
                }
                break;
            case 'June':
                if(currentMonth == '1' || currentMonth == '2' || currentMonth == '3' || currentMonth == '4' || currentMonth == '5') {
                    year = currentYear;
                }
                else {
                    year = parseInt(currentYear) + 1;
                }
                break;
            case 'July':
                if(currentMonth == '1' || currentMonth == '2' || currentMonth == '3' || currentMonth == '4' || currentMonth == '5' || currentMonth == '6') {
                    year = currentYear;
                }
                else {
                    year = parseInt(currentYear) + 1;
                }
                break;
            case 'August': 
                if(currentMonth == '1' || currentMonth == '2' || currentMonth == '3' || currentMonth == '4' || currentMonth == '5' 
                   || currentMonth == '6' || currentMonth == '7') {
                    year = currentYear;
                }
                else {
                    year = parseInt(currentYear) + 1;
                }
                break;
            case 'September':
                if(currentMonth == '1' || currentMonth == '2' || currentMonth == '3' || currentMonth == '4' || currentMonth == '5' 
                   || currentMonth == '6' || currentMonth == '7' || currentMonth == '8') {
                    year = currentYear;
                }
                else {
                    year = parseInt(currentYear) + 1;
                }
                break;
            case 'October':
                if(currentMonth == '1' || currentMonth == '2' || currentMonth == '3' || currentMonth == '4' || currentMonth == '5' 
                   || currentMonth == '6' || currentMonth == '7' || currentMonth == '8' || currentMonth == '9') {
                    year = currentYear;
                }
                else {
                    year = parseInt(currentYear) + 1;
                }
                break;
            case 'November':
                if(currentMonth == '1' || currentMonth == '2' || currentMonth == '3' || currentMonth == '4' || currentMonth == '5' 
                   || currentMonth == '6' || currentMonth == '7' || currentMonth == '8' || currentMonth == '9' || currentMonth == '10') {
                    year = currentYear;
                }
                else {
                    year = parseInt(currentYear) + 1;
                }
                break;
            case 'December':
                if(currentMonth == '1' || currentMonth == '2' || currentMonth == '3' || currentMonth == '4' || currentMonth == '5' 
                   || currentMonth == '6' || currentMonth == '7' || currentMonth == '8' || currentMonth == '9' || currentMonth == '10'
                   || currentMonth == '11') {
                    year = currentYear;
                }
                else {
                    year = parseInt(currentYear) + 1;
                }
                break;
            default:
        }
        return year;
    },
    
    validateStoreGoalForm : function(component) {
        //unique month
        //valid input for revenue
        //valid input for s1 windows
        //valid input for s2 windows
        //valid input for patio
        //valid input for entry
        return true;
    }
})