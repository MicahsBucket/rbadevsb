({
    doInit : function(component, event, helper) {
        var action = component.get("c.getGoals");
        helper.getUserStoreConfiguration(component);
        component.find("storeGoalRecordCreator").getNewRecord(
            'Store_Goal__c', //sObject type
            null, //recordTypeId
            false, //skip cache?
            $A.getCallback(function() {
                var rec = component.get("v.newStoreGoalRecord");
                var error = component.get("v.newStoreGoalError");
                if(error || (rec === null)) {
                    console.log("Error initializing record template: " + error);
                }
                else {
                    console.log("Record template initialized: " + JSON.stringify(rec));
                }
            }));
        component.set('v.Months', [{label: 'January ' + helper.getYear('January'),		value: 'January ' + helper.getYear('January')},
                                   {label: 'February ' + helper.getYear('February'),	value: 'February ' + helper.getYear('February')},
                                   {label: 'March ' + helper.getYear('March'),			value: 'March '+ helper.getYear('March')},
                                   {label: 'April ' + helper.getYear('April'),			value: 'April ' + helper.getYear('April')},
                                   {label: 'May ' + helper.getYear('May'),				value: 'May ' + helper.getYear('May')},
                                   {label: 'June ' + helper.getYear('June'),			value: 'June ' + helper.getYear('June')},
                                   {label: 'July ' + helper.getYear('July'),			value: 'July ' + helper.getYear('July')},
                                   {label: 'August ' + helper.getYear('August'),		value: 'August ' + helper.getYear('August')},
                                   {label: 'September ' + helper.getYear('September'),	value: 'September ' + helper.getYear('September')},
                                   {label: 'October ' + helper.getYear('October'),		value: 'October ' + helper.getYear('October')},
                                   {label: 'November ' + helper.getYear('November'),	value: 'November ' + helper.getYear('November')},
                                   {label: 'Dececmber ' + helper.getYear('December'),	value: 'December ' + helper.getYear('December')}]);
        component.set('v.mycolumns', [{label: 'Month', 				fieldName: 'Month_Year__c',			type: 'text'},
            						  {label: 'Installed Revenue', 	fieldName: 'Installed_Revenue__c', 	type: 'currency'},
            						  {label: 'Series 1 Windows', 	fieldName: 'Series_1_Windows__c', 	type: 'number'},
            						  {label: 'Series 2 Windows', 	fieldName: 'Series_2_Windows__c',	type: 'number'},
            						  {label: 'Patio Doors', 		fieldName: 'Patio_Doors__c', 		type: 'number'},
						              {label: 'Entry Doors', 		fieldName: 'Entry_Doors__c', 		type: 'number'}]);
        helper.getData(component);
        
        
        helper.getStoreName(component);
    },
    
    handleSaveStoreGoal: function(component, event, helper) {
        if(helper.validateStoreGoalForm(component)) {
            //set the store configuration link
            //need to write helper and server controller to get the store configuration\
            helper.getUserStoreConfiguration(component);
            //console.log('Store Config in save: ' + component.get("v.Store_Configuration__c"));
            component.set("v.simpleNewStoreGoalRecord.Store_Configuration__c", component.get("v.Store_Configuration__c"));
            component.set("v.simpleNewStoreGoalRecord.Name",component.get("v.simpleNewStoreGoal.Month_Year__c"));
            //console.log(JSON.stringify(component.get("v.simpleNewStoreGoalRecord")));
            
            component.find("storeGoalRecordCreator").saveRecord(function(saveResult) {
                if(saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    //record is saveed successfully
                    /*var resultsToast = $A.get("e.force:showToast");
	                resultsToast.setParams({
                        "title": "Store Goal Saved",
                        "message": "The new Store Goal was created."
                    });
                    resultsToast.fire();*/

                    console.log('record saved successfully');
                   /* console.log('refreshing page');
                    var closequickactionevent = $A.get("e.force:closeQuickAction");
                    console.log('quickActionEvent = ' + closequickactionevent);
                    closequickactionevent.fire();
                    console.log('quickAction closed');
                    var refreshevent = $A.get("e.force:refreshView");
                    console.log('refreshEvent = ' + refreshevent);
                    refreshevent.fire();*/
                    component.set("v.newRecordButton", false);
                    console.log(component.get("v.newRecordButton"));
                } else if (saveResult.state === "INCOMPLETE") {
                    // handle the incomplete state
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    // handle the error state
                    console.log('Problem saving contact, error: ' + 
                                 JSON.stringify(saveResult.error));
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state +
                                ', error: ' + JSON.stringify(saveResult.error));
                }
            });
        }
    },
    
    handleClick: function(component, event, helper) {
        component.set("v.newRecordButton", true);
    },
    
     handleClickCancelButton: function(component, event, helper) {
        component.set("v.newRecordButton", false);
    }
})