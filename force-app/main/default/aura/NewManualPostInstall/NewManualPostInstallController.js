({
	init : function(component, event, helper) {
		var salesRole = 'Sales Rep';
		var techRole = 'Tech Measurer';
		

		//get sales reps to populate dropdown menu
		var setSalesReps = component.get("c.getWorkers");		
		setSalesReps.setParams({
                  "workerRole": salesRole
              });
        setSalesReps.setCallback(this, function(salesReps){
        	var salesRepList = salesReps.getReturnValue();
        	component.set("v.salesReps", salesRepList);
        	component.set("v.selectedSalesRep", null);

        });
        $A.enqueueAction(setSalesReps);

        //get tech measures to populate dropdown menu
        var setTechMeasurer = component.get("c.getWorkers");
        setTechMeasurer.setParams({
                  "workerRole": techRole
              });
        setTechMeasurer.setCallback(this, function(techMeasurers){
        	var techMeasurerList = techMeasurers.getReturnValue();
        	component.set("v.techMeasurers", techMeasurerList);
        	component.set("v.selectedTechMeasurer", null);
        });
        $A.enqueueAction(setTechMeasurer);

        //get survey to populate existing fields for user
        var recordId = component.get("v.recordId");
        var setSurvey = component.get("c.getSurvey");
        setSurvey.setParams({
        	"recordId":recordId
        })
        setSurvey.setCallback(this, function(survey){
        	var surveyRecord = survey.getReturnValue();
            if(surveyRecord.Shipping_Date__c == null){
                component.set("v.shippingNotNull", false);
            }
            if(surveyRecord.Installation_Date__c == null){
                component.set("v.installNotNull", false);
            }
        	component.set("v.survey", surveyRecord);
        });
        $A.enqueueAction(setSurvey);
		
        helper.getCurrentSalesRep(component, event, helper);
        helper.getCurrentTechMeasure(component, event, helper);

	},

	setSalesRep : function(component, event, helper){
		var salesRep = event.getSource().get("v.value");
		if(salesRep != undefined){
			component.set("v.selectedSalesRep", salesRep);
			component.set("v.isNullSalesRep", false);
		}
	},

	setTechMeasurer : function(component, event, helper){
		var techMeasurer = event.getSource().get("v.value");
		component.set("v.selectedTechMeasurer", techMeasurer);
	},

	

	handleUpdateInstallers : function(component, event, helper){
		var updateInst = event.getParam("installers");
		component.set("v.installers", updateInst);
	},

	saveSurvey : function(component, event, helper){
		component.set("v.disable", true);
		helper.saveSurvey(component, event, helper);
	},

	sendSurvey : function(component, event, helper){
		component.set("v.disable", true);

		if(component.get("v.noEmail") == false){
			if(component.find('email').get('v.value') == null || component.find('email').get('v.value') == undefined){
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
				"title": "Notification",
				"message": "Please add the recipients email to the Survey",
				"type": "info",
				});
				toastEvent.fire();
				component.set('v.noEmail', true);
				window.setTimeout(
				    $A.getCallback(function() {
				        component.set("v.disable", false);
				    }), 3000
				);
				return;
			}
		}
		helper.saveSurvey(component, event, helper);
		helper.sendToMedallia(component, event);
	},

	onHoldClick : function(component, event, helper){
		component.set("v.disable", true);
		helper.saveSurvey(component, event, helper);
		var recordId = component.get("v.recordId");
		var updateAction = component.get("c.updateStatus");
		var status = 'Hold';
		updateAction.setParams({
			"recordId" : recordId,
			"status" : status
		});
		updateAction.setCallback(this, function(holdsLeft){
			
			var result = holdsLeft.getReturnValue();
			if(result == 'hold limit'){
				var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Failed!",
                "message": "You have reached your Hold limit.  To put this survey on Hold, you must first send a different survey that is currently in Hold or Incomplete Data status",
                "type": "error",
                });
                toastEvent.fire();
			} else if(result == 'already held'){
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
				"title": "Notification",
				"message": "Survey already on hold",
				"type": "info",
				});
				toastEvent.fire();
			} else if(result == 'already sent'){
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
				"title": "Notification",
				"message": "Survey already sent",
				"type": "info",
				});
				toastEvent.fire();
			} else if(result.includes('has')) {
				var message = "Placing a survey on Hold adds 56 days from today before it is auto sent. This survey currently " + result;
				var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Failed!",
                "message": message,
                "type": "error",
                });
                toastEvent.fire();
			} else {
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
				"title": "Success!",
				"message": "Survey on hold",
				"type": "success",
				});
				toastEvent.fire();
                $A.get('e.force:refreshView').fire();
			}
		});
		$A.enqueueAction(updateAction);
		window.setTimeout(
		    $A.getCallback(function() {
		        component.set("v.disable", false);
		    }), 3000
		);
	},

	cancel : function(component, event, helper){
		component.set("v.disable", true);
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
		"url": "/post-install-survey"
		});
		urlEvent.fire();
	}

})