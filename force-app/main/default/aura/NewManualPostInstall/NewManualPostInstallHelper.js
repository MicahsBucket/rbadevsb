({
     //get original sales rep associate with a survey so their junction object can be deleted if they are removed from a survey and so they are the default selection
    getCurrentSalesRep : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var setOriginalSalesRep = component.get("c.getOriginalWorker");
        var roleSales = "Sales Rep";
        setOriginalSalesRep.setParams({
            "recordId":recordId,
            "role": roleSales
        });
        setOriginalSalesRep.setCallback(this, function(salesRep){
            var originalSalesRep = salesRep.getReturnValue();
            component.set("v.originalSalesRep", originalSalesRep);
            if(originalSalesRep != null){
                component.set("v.existingSalesRep", true);
                component.set("v.isNullSalesRep", false);
                component.set("v.selectedSalesRep", originalSalesRep.Id);
            }
        });
        $A.enqueueAction(setOriginalSalesRep);
    },
    
    //get original tech measurer associate with a survey so their junction object can be deleted if they are removed from a survey and so they are the default selection
    getCurrentTechMeasure : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var setOriginalTechMeasurer = component.get("c.getOriginalWorker");
        var roleTech = "Tech Measurer";
        setOriginalTechMeasurer.setParams({
            "recordId":recordId,
            "role": roleTech
        });
        setOriginalTechMeasurer.setCallback(this, function(techMeasurer){
            
            var originalTechMeasurer = techMeasurer.getReturnValue();
            component.set("v.originalTechMeasurer", originalTechMeasurer);
            if(originalTechMeasurer != null){
                component.set("v.existingTechMeasurer", true);
                component.set("v.isNullTechMeasurer", false);
                component.set("v.selectedTechMeasurer", originalTechMeasurer.Id);
            }
        });
        $A.enqueueAction(setOriginalTechMeasurer);
    },

    newToastEvent : function(component, event, helper, title, message, type){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
        "title": title,
        "message": message,
        "type": type,
        });
        toastEvent.fire();
    },

    saveSurvey : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var firstName = component.find('firstName').get("v.value");
        var lastName = component.find('lastName').get("v.value");
        var email = component.find('email').get("v.value");
        var phone = component.find('phone').get("v.value");
        var street = component.find('street').get("v.value");
        var city = component.find('city').get("v.value");
        var state = component.find('state').get("v.value");
        var zip = component.find('zip').get("v.value");
        var country = component.find('country').get("v.value");
        var salesRep = component.get("v.selectedSalesRep");
        var techMeasurer = component.get("v.selectedTechMeasurer");
        var originalSalesRep = component.get("v.originalSalesRep");
        var comments = component.find('comments').get("v.value");
        var originalTechMeasurer = component.get("v.originalTechMeasurer");
        var installers = component.get("v.installers");
        if(firstName != '' && lastName != '' && street != '' && city != '' && zip != '' && country != '' && firstName != undefined && lastName != undefined && street != undefined && city != undefined && zip != undefined && country != undefined){
               var saveAction = component.get("c.savePIS");
            saveAction.setParams({
                "recordId":recordId,
                "firstName" : firstName,
                "lastName" : lastName,
                "email" : email,
                "phone" : phone,
                "street" : street,
                "city" : city,
                "state" : state,
                "zip" : zip,
                "country" : country,
                "techMeasurer" : techMeasurer,
                "salesRep" : salesRep,
                "installers" : installers,
                "originalSalesRep" : originalSalesRep,
                "originalTechMeasurer" : originalTechMeasurer,
                "comment" : comments 
            });
            saveAction.setCallback(this, function(response){
                var upsertError = response.getReturnValue();
                var state = response.getState();
                if(state === "SUCCESS" && upsertError === "No Errors"){
                    helper.getCurrentSalesRep(component, event, helper);
                    var title = "Success!";
                    var message = "The survey has been saved successfully.";
                    var type = "success";
                    helper.newToastEvent(component, event, helper, title, message, type);
                    window.setTimeout(
                        $A.getCallback(function() {
                            location.reload();
                        }), 3000
                    );
                } else if(upsertError === "Validation Error") {
                    var title = "Notification";
                    var message = "The survey record is locked";
                    var type = "info";
                    helper.newToastEvent(component, event, helper, title, message, type);
                    component.set("v.disable", false);
                    return;
                } else {
                    var title = "Failed!";
                    var message = "The survey failed to save";
                    var type = "error";
                    helper.newToastEvent(component, event, helper, title, message, type);
                    component.set("v.disable", false);
                    return;
                }
            });
            $A.enqueueAction(saveAction); 
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
            "title": "Failed!",
            "message": "The survey failed to save, ensure all required fields are filled",
            "type": "error",
            });
            toastEvent.fire();
            component.set("v.disable", false);
            return;
        }
    },

    sendToMedallia : function(component, event, helper) {
        component.set("v.disable", true);
        var recordId = component.get("v.recordId");
        var updateAction = component.get("c.updateStatus");
        var status = 'Send';
        updateAction.setParams({
            "recordId" : recordId,
            "status" : status
        });
        updateAction.setCallback(this, function(result){
            var state = result.getState();
            var resultStatus = result.getReturnValue();
            if(state === "SUCCESS"){ 
                if(resultStatus == 'already sent'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    "title": "Notification",
                    "message": "Survey already sent",
                    "type": "info",
                    });
                    toastEvent.fire();
                } else if(result == 'already held'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    "title": "Notification",
                    "message": "Survey already on hold",
                    "type": "info",
                    });
                    toastEvent.fire();
                } else if(resultStatus == 'hold'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    "title": "Success!",
                    "message": "Survey is on hold",
                    "type": "sucess",
                    });
                    toastEvent.fire();
                    component.set("v.disable", false);
                    window.setTimeout(
                        $A.getCallback(function() {
                            location.reload();
                        }), 3000
                    );
                } else if(resultStatus == 'send'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    "title": "Success!",
                    "message": "The survey has been sent successfully.",
                    "type": "success",
                    });
                    toastEvent.fire();
                    window.setTimeout(
                        $A.getCallback(function() {
                            location.reload();
                        }), 3000
                    );
                } else if(resultStatus == 'hold limit'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    "title": "Failed!",
                    "message": "You have reached your on hold limit.  To put this survey on hold you must first send a different survey that is currently on hold.",
                    "type": "error",
                    });
                    toastEvent.fire();
                    window.setTimeout(
                        $A.getCallback(function() {
                            location.reload();
                        }), 3000
                    );
                } 
            }
        });
        $A.enqueueAction(updateAction);
        window.setTimeout(
            $A.getCallback(function() {
                component.set("v.disable", false);
            }), 3000
        );

    },

    pause : function(component, event, helper){
        console.log('Pausing');
    }
})