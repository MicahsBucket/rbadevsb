({
    handleCloseQuickAction : function(component, event, helper) {
        let eventBoolean = event.getParam('closeQuickAction');
        console.log('hit close quick action ' , eventBoolean);
        if(eventBoolean == true){
            var closeWindow = $A.get("e.force:closeQuickAction");
            closeWindow.fire();
        }
    },
    handleRefreshView : function(component, event, helper){
        let eventBoolean = event.getParam('refreshview');
        if(eventBoolean == true){
            $A.get('e.force:refreshView').fire();
            var closeWindow = $A.get("e.force:closeQuickAction");
            closeWindow.fire();
        }
    }
})