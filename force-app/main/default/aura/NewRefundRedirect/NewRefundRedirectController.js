({

    initializeComponent: function(component, event, helper) {

        var recordId = '';
        var objectName = '';

        // Find the Parent Id and Object Name.  This only works for Desktop Users.
        var pageReference = component.get("v.pageReference");
        var state = pageReference.state; 
        var context = state.inContextOfRef;
        if (context.startsWith("1\.")) {

            context = context.substring(2);
            var addressableContext = JSON.parse(window.atob(context));
            console.log('addressableContext',JSON.stringify(addressableContext));

            recordId = addressableContext.attributes.recordId;
            objectName = addressableContext.attributes.objectApiName;

        }

        var getRecordDataAction = component.get("c.getRecordData");
        console.log('getRecordDataAction: ', getRecordDataAction)
        getRecordDataAction.setParams({
            'recordId': recordId,
            'objectName': objectName
        });

        ///Configure the response handler for the action
        getRecordDataAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('response.getState()', state);
            if (state === "SUCCESS") {

                var record = response.getReturnValue();
                console.log('response.getReturnValue()', record);
                // Navigate to new Payment record.
                var createPaymentEvent = $A.get("e.force:createRecord");
                createPaymentEvent.setParams({
                    "entityApiName": "Refund__c",
                    "recordTypeId": record.recordTypeId,
                    "defaultFieldValues": {
                        'Order__c': record.Id,
                        'Contact__c': record.BillToContactId,
                        'Store_Location__c': record.Store_Location__c
                        // 'AccountId': record.AccountId
                    }
                });
                createPaymentEvent.fire();

            }
            else if (state === "ERROR") {
                console.log('Problem retrieving Record, response state: ' + state);
            }
            else {
                console.log('Unknown problem, response state: ' + state);
            }

        });

        ///Send the request to get Record data.
        $A.enqueueAction(getRecordDataAction);    

    }

})