({
     //get original sales rep associate with a survey so their junction object can be deleted if they are removed from a survey and so they are the default selection
    getCurrentSalesRep : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var setOriginalSalesRep = component.get("c.getOriginalWorker");
        var roleSales = "Sales Rep";
        setOriginalSalesRep.setParams({
            "recordId":recordId,
            "role": roleSales
        });
        setOriginalSalesRep.setCallback(this, function(salesRep){
            var originalSalesRep = salesRep.getReturnValue();
            component.set("v.originalSalesRep", originalSalesRep);
            if(originalSalesRep != null){
                component.set("v.existingSalesRep", true);
                component.set("v.isNullSalesRep", false);
                component.set("v.selectedSalesRep", originalSalesRep.Id);
            }
        });
        $A.enqueueAction(setOriginalSalesRep);
    },

    newToastEvent : function(component, event, helper, title, message, type){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
        "title": title,
        "message": message,
        "type": type,
        });
        toastEvent.fire();
    },

    saveSurvey : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var firstName = component.find('firstName').get("v.value");
        var lastName = component.find('lastName').get("v.value");
        var email = component.find('email').get("v.value");
        var phone = component.find('phone').get("v.value");
        var salesRep = component.get("v.selectedSalesRep");
        var originalSalesRep = component.get("v.originalSalesRep");
        var appointmentDate = component.find('appointmentDate').get("v.value");
        var result = component.find("appointmentResult").get('v.value');
        if(result != '------' && result != undefined && firstName != "" && lastName != ""){
            var saveAction = component.get("c.savePAS");
            saveAction.setParams({
                "recordId":recordId,
                "firstName" : firstName,
                "lastName" : lastName,
                "email" : email,
                "phone" : phone,
                "salesRep" : salesRep,
                "originalSalesRep" : originalSalesRep,
                "appointmentDate" : appointmentDate,
                "result" : result
            });
        saveAction.setCallback(this, function(response){
            var upsertError = response.getReturnValue();
            var state = response.getState();
            if(state === "SUCCESS" && upsertError === "No Errors"){
                helper.getCurrentSalesRep(component, event, helper);
                component.set('v.readyToSend', true);
                var title = "Success!";
                var message = "The survey has been saved successfully.";
                var type = "success";
                helper.newToastEvent(component, event, helper, title, message, type);
                window.setTimeout(
                    $A.getCallback(function() {
                        location.reload();
                    }), 3000
                );
            } else if(upsertError === "Validation Error") {
                var title = "Notification";
                var message = "The survey record is locked";
                var type = "info";
                helper.newToastEvent(component, event, helper, title, message, type);
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.disable", false);
                    }), 3000
                );
                return;
            } else {
                var title = "Failed!";
                var message = "The survey failed to save, ensure required fields are filled";
                var type = "error";
                helper.newToastEvent(component, event, helper, title, message, type);
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.disable", false);
                    }), 3000
                );
                return;
            }
        });
        $A.enqueueAction(saveAction);
        } else {
            //shows error message if save fails
            var title = "Failed!";
            var message = "The survey failed to save, ensure required fields are filled";
            var type = "error";
            helper.newToastEvent(component, event, helper, title, message, type);
            window.setTimeout(
                $A.getCallback(function() {
                    component.set("v.disable", false);
                }), 3000
            );
        }
    },

    sendToMedallia : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var updateAction = component.get("c.updateStatus");
        var status = 'Send';
        updateAction.setParams({
            "recordId" : recordId,
            "status" : status
        });
        updateAction.setCallback(this, function(result){
            var state = result.getState();
            var resultStatus = result.getReturnValue();
            if(state === "SUCCESS"){ 
                if(resultStatus == 'already sent'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    "title": "Notification",
                    "message": "Survey already sent",
                    "type": "info",
                    });
                    toastEvent.fire();
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    "title": "Success!",
                    "message": "The survey has been sent successfully.",
                    "type": "success",
                    });
                    toastEvent.fire();
                    window.setTimeout(
                        $A.getCallback(function() {
                            location.reload();
                        }), 3000
                    );
                }
            }
        });
        $A.enqueueAction(updateAction);
        window.setTimeout(
            $A.getCallback(function() {
                component.set("v.disable", false);
            }), 3000
        );
    },
    
    getDate: function(dateVal, component, event, helper){
        var dd = dateVal.getDate();
        var mm = dateVal.getMonth()+1; 
        var yyyy = dateVal.getFullYear();

        if(dd < 10) {
          dd = '0'+dd;
        } 
        if(mm < 10) {
          mm = '0'+mm;
        } 

        dateVal = yyyy + '-' + mm + '-' + dd;
        return dateVal;
    },

    dateComparer: function(today, appointmentDate, component, event, helper){
        var todaydd = today.getDate();
        var todaymm = today.getMonth()+1;
        var todayyyyy = today.getFullYear();
        var apptdd = parseInt(appointmentDate.substring(8,10));
        var apptmm = parseInt(appointmentDate.substring(5,7));
        var apptyyyy = parseInt(appointmentDate.substring(0,4));
        if(todayyyyy < apptyyyy){
            return true;
        } else if(todayyyyy == apptyyyy){
            if(todaymm < apptmm){
                return true;
            } else if(todaymm == apptmm){
                if(todaydd < apptdd){
                    return true;
                }
            } 
        } else {
            return false;
        }
    },
       
})