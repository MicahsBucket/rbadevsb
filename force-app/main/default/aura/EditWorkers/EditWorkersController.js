/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Signature Service Community
* @date 6/01/17
* @description allows user to select a role which gets a list of workers.
* the user can select a user and edit particular fieldss
**/


({
    searchWorkers : function(component, event, helper) {
        var search = component.find("autocomplete").get("v.value");
        var findWorkerAction = component.get("c.findWorker");
        findWorkerAction.setParams({
            "searchString" : search
        });
        findWorkerAction.setCallback(this, function(response){
            debugger;
            var searchWorkers = response.getReturnValue();
            component.set("v.searchWorkers", searchWorkers);
        });    
        $A.enqueueAction(findWorkerAction);
        
    },
//after a user picks a role all of the workers with that role are displayed 
	displayWorkerList : function(component, event, helper) {
        helper.displayWorkerList(component, event);
	},

//sends the active worker to the cmp, also selects the checkboxes that correspond to the workers role(s)
	editDisplay: function (component, event, helper){
        helper.editDisplay(component, event, helper);
        component.set("v.editDisplayed", true);
    },

//updates worker based on changes made by the user
	saveEdit : function(component, event, helper) {
        component.set("v.disable", true);
		var firstName = component.find("firstName").get("v.value");
        var lastName = component.find("lastName").get("v.value");
        var email = component.find("email").get("v.value");
        var phone = component.find("phone").get("v.value");
         
        var workersSelected = "";
        var tech = component.get("v.isTechMeasurer");
        var sales = component.get("v.isSalesRep");
        var installer = component.get("v.isInstaller");
        var inactive = component.get("v.isInactive");

        //creates a concatenated list because the field is a multiselect picklist 
        if(tech) workersSelected += "Tech Measurer;"; 
        if(installer) workersSelected += "Installer;";
        if(sales) workersSelected += "Sales Rep;"; 
        if(inactive) workersSelected += "Inactive;"; 
        

        //if email is left blank no new worker is created
        if(email != undefined && email != "" && email != null && workersSelected != "" && workersSelected != "Inactive;"){
        var workerAction = component.get("c.saveWorker");
        workerAction.setParams({
        	"worker": component.get("v.editWorker"),
            "firstName": firstName,
            "lastName": lastName,
            "email": email,
            "phone": phone,
            "workersSelected": workersSelected
        });
        $A.enqueueAction(workerAction);
        workerAction.setCallback(this, function(response) {
            var state = response.getState();
           if(state === "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Success!",
                "message": "Worker update successful",
                "type": "success",
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
           } else {
            //shows error message if save fails
            var message = "Worker update failed";
            helper.failMessage(component, event, helper, message);
           }
        });
        
        } else {
            var message = "Add email and/or select worker role";
            helper.failMessage(component, event, helper, message);
        }
        window.setTimeout(
            $A.getCallback(function() {
                component.set("v.disable", false);
            }), 3000
        );
	},

    //resets fields and updates the list of workers based on changes made
	clearFields: function (component, event, helper) {
        helper.clearFields(component, event);
        helper.displayWorkerList(component,event);
    },
})