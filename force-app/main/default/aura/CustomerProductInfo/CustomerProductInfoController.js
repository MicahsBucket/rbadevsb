({

    initOrder: function(component, event, helper) {
        component.set("v.toggleSpinner", false);
		var isPhone=$A.get("$Browser.isPhone");
        var isTablet=$A.get("$Browser.isTablet");
        // Set the current order
        var order = event.getParam('order');
        
        // BEGIN Code related to CPP-150
        var products = order.OrderItems.records; 
		var unitsMap = new Map(); // Map based on the order item unit Id for combining records
        var productsMap = new Map(); // Map with the final products after condensing the sub items
		var productsUpdated = new Array(); // Array to store the updated order item list

        // First, we need to condense the parent order items into a map
        for(var i = 0; i < products.length; i++){
            if(products[i].Parent_Order_Item__c == undefined)
            {
                // If the item has no parent, then it is a parent
                productsMap.set(products[i].Id, products[i]);
                
                //console.log("_____products["+i+"]_____"+JSON.stringify(products[i]));
                var product=products[i];
                helper.displayToggleButton(product,order);
                if(order.Status=='Draft')
                {
                    product.Height_Inches__c=null;
                    product.Width_Inches__c=null;
                }
                if(product.Height_Fraction__c!=undefined)
                {
                    var heightFraction=product.Height_Fraction__c;
                    heightFraction=heightFraction.toUpperCase();
                    if(heightFraction.includes("EVEN"))
                    {
                        heightFraction=heightFraction.replace("EVEN","");
                    }
                    product.Height_Fraction__c=heightFraction;
                    /*
                    var heightType=typeof product.Height_Fraction__c;
                    var isheightTypeNumeric=helper.isNumeric(product.Height_Fraction__c);
                    console.log("____product.Height_Fraction__c_is____"+isheightTypeNumeric);
                    //var unitNumber=helper.extractNumber(unitName);
                    if(isheightTypeNumeric==false)
                    {
                        var heights=helper.extractNumber(product.Height_Fraction__c);
                        product.Height_Fraction__c=heights[0];
                    }
                    */
                }
                if(product.Width_Fraction__c!=undefined)
                {
                    var widthFraction=product.Width_Fraction__c;
                    widthFraction=widthFraction.toUpperCase();
                    if(widthFraction.includes("EVEN"))
                    {
                        widthFraction=widthFraction.replace("EVEN","");
                    } 
                    product.Width_Fraction__c=widthFraction;
                    /*var widthType=typeof product.Width_Fraction__c;
                    var iswidthTypeNumeric=helper.isNumeric(product.Width_Fraction__c);
                    console.log("____product.Width_Fraction__c_is____"+iswidthTypeNumeric);
                    //var unitNumber=helper.extractNumber(unitName);
                    if(iswidthTypeNumeric==false)
                    {
                        var widths=helper.extractNumber(product.Width_Fraction__c);
                        product.Width_Fraction__c=widths[0];
                    }*/
                }
            } 
        }
        // Second, add the child cost to the parent order item
        for(var i = 0; i < products.length; i++){
            if(products[i].Parent_Order_Item__c != undefined){
                //productsMap.set(products[i].Id, products[i]); 
                var parentItem = productsMap.get(products[i].Parent_Order_Item__c);
                if(parentItem != undefined){
                    productsMap.get(products[i].Parent_Order_Item__c).Total_Retail_Price__c += products[i].Total_Retail_Price__c;
                }
            } 
        }
		// Lastly, map the products by unit Id
		
		//Below variables declared for family and unit name sorting
		var roomNames=[];
        var roomNameMap=new Map();
        //Till Here
        
        //Below variables are used to capture products with Unit Id but null/blank room names
        var productWithUnitBlankRoom=new Map();
        
        //Below variable is used to capture the count of unit Id used
        var prodUnitIdCount=[];
        var prodUnitIdCountMap=new Map();
        
        
        //Below map will store products for each unit name
        var prodUnitIdUnique=[]; 
        var productWithUnitRoom=new Map();
        var uniqueUnitIds=[];
        
        for(var product of productsMap.values()){
            var productName='';
            if (product.PricebookEntry.Product2.Name.indexOf('(')!=-1){
                productName = product.PricebookEntry.Product2.Name.substr(0,product.PricebookEntry.Product2.Name.indexOf('('));
            }else{
                productName =  product.PricebookEntry.Product2.Name;
            }
            product.PricebookEntry.Product2.Name = productName;
            // Combines multiple products with the same unit into one object with an increased quantity
            var unitNames=(product.Unit_Id__c != undefined) ? product.Unit_Id__c.replace(/\s/g,'') : product.Unit_Id__c;
            //console.log("______unitNames__________"+JSON.stringify(unitNames));
            var splitunitNames =(unitNames != undefined) ? unitNames.split(',') : []; // This last value becomes the default Unit Id when there is none available			
            //New code for collecting products for each room
            if(splitunitNames.length>0)
            {
                for(var i=0;i<splitunitNames.length;i++)
                {
                    var uName=splitunitNames[i];
                    if(!prodUnitIdUnique.includes(uName))
                    {
                        uniqueUnitIds.push(uName);
                        prodUnitIdUnique.push(uName);
                    }
                    var prdInMap=productWithUnitRoom[uName];        
                    if(prdInMap==undefined)
                    {
                        prdInMap=[];
                        prdInMap.push(product);
                    }
                    else
                    {
                        prdInMap.push(product);    
                    }
                    productWithUnitRoom[uName]=prdInMap;       
                }
            }
            else
            {
                var prdInMap=productWithUnitRoom[unitNames];   
                if(!uniqueUnitIds.includes(unitNames))
                {
                    uniqueUnitIds.push(unitNames);
                }
                if(prdInMap==undefined)
                {
                    prdInMap=[];
                    prdInMap.push(product);
                }
                else
                {
                    prdInMap.push(product);    
                }
                productWithUnitRoom[unitNames]=prdInMap;         
            }
        }
        var uniqueUnitIds=helper.startSortingAlphaNumeric(uniqueUnitIds);
        roomNames=roomNames.sort();
        
        var prodUnitWithBlankRoom=[];
        var productCompleteList=[];
        
        var uniqueUnits=[];
        var specialCharUnits=[];
        var mapAlphaNumericUnits=new Map();
        var mapSpecialCharUnits=new Map();
        for(var i=0;i<uniqueUnitIds.length;i++)
        {
            var unitName=uniqueUnitIds[i];
            var unitType=typeof unitName;
            var isUnitNoNumeric=helper.isNumeric(unitName);
            var addUnitNumToUniqUnitNameArray=true;
            if(isUnitNoNumeric==false)
			{
                var unitNumber=helper.extractNumber(unitName);
                //extractNumber will return array of numbers eg: if the Unitnumber is 3A32 then this method will return 3,32
                //var unitNameAtIndexZero=unitNumber[0];
                var unitNameAtIndexZero;
                if(unitNumber==null)
                {
                    unitNameAtIndexZero=unitName;
                }
                else
                {
                   unitNameAtIndexZero=unitNumber[0]; 
                }
                
                addUnitNumToUniqUnitNameArray=false;
                
                if(unitNumber!=null)
                {
                     if(!uniqueUnits.includes(unitNameAtIndexZero))
                     {
                         uniqueUnits.push(unitNameAtIndexZero);
                     }
                    var checkSpChar=helper.checkSpecialCharacters(unitName);
                    if(!checkSpChar)
                    {
                        var alphaUnitNameInMap=mapAlphaNumericUnits[unitNameAtIndexZero];
                       
                        if(alphaUnitNameInMap==undefined)
                        {
                            alphaUnitNameInMap=[];
                            alphaUnitNameInMap.push(unitName);
                        }
                        else
                        {
                            if(!alphaUnitNameInMap.includes(unitName))
                            {
                                alphaUnitNameInMap.push(unitName);     	   
                            }
                        }
                        mapAlphaNumericUnits[unitNameAtIndexZero]=helper.startSortingAlphaNumeric(alphaUnitNameInMap);                        	    
                    }
                    else
                    {
                        var alphaUnitNameInMap=mapSpecialCharUnits[unitNameAtIndexZero];
                        if(alphaUnitNameInMap==undefined)
                        {
                            alphaUnitNameInMap=[];
                            alphaUnitNameInMap.push(unitName);
                        }
                        else
                        {
                            if(!alphaUnitNameInMap.includes(unitName))
                            {
                                alphaUnitNameInMap.push(unitName);     	   
                            }
                        }
                        mapSpecialCharUnits[unitNameAtIndexZero]=helper.startSortingAlphaNumeric(alphaUnitNameInMap);
                    }
                }
                else
                {
                    addUnitNumToUniqUnitNameArray=false;
                    if(!specialCharUnits.includes(unitName))
                    {
                        specialCharUnits.push(unitName);
                    }
                }
            }
            if(addUnitNumToUniqUnitNameArray&&(!uniqueUnits.includes(unitName)))
            {
                uniqueUnits.push(unitName);
            }
        }
        var uniqueUnits=helper.startSortingAlphaNumeric(uniqueUnits);
        for(var i=0;i<uniqueUnits.length;i++)
        {
            var unitname=uniqueUnits[i];
            var productList=productWithUnitRoom[unitname];
            if(productList!=undefined)
            {
                var productname=productList[0].PricebookEntry.Product2.Name;
                var orderUnit=helper.addMulitpleProductsToList(productList,unitname,productname);
                orderUnit.TotalCost = Number.parseFloat(orderUnit.TotalCost).toFixed(2).toLocaleString();
                orderUnit.TotalCost= helper.commafy(orderUnit.TotalCost.split('.')[0]);
                productCompleteList.push(orderUnit);
                var alphaUnits=mapAlphaNumericUnits[unitname];//mapSpecialCharUnits
                if(alphaUnits!=undefined)
                {
                    for(var j=0;j<alphaUnits.length;j++)
                    {
                        var aplhaunitname=alphaUnits[j];
                        var alphaUnitProductList=productWithUnitRoom[aplhaunitname];
                        if(alphaUnitProductList!=undefined)
                        {
                            var productname2=alphaUnitProductList[0].PricebookEntry.Product2.Name;
                            var orderUnit2=helper.addMulitpleProductsToList(alphaUnitProductList,aplhaunitname,productname2);
                            orderUnit2.TotalCost = Number.parseFloat(orderUnit2.TotalCost).toFixed(2).toLocaleString();
                            orderUnit2.TotalCost= helper.commafy(orderUnit2.TotalCost.split('.')[0]);
                            productCompleteList.push(orderUnit2);
                        }
                    }
                }
                alphaUnits=mapSpecialCharUnits[unitname];//mapSpecialCharUnits
                if(alphaUnits!=undefined)
                {
                    for(var j=0;j<alphaUnits.length;j++)
                    {
                        var aplhaunitname=alphaUnits[j];
                        var alphaUnitProductList=productWithUnitRoom[aplhaunitname];
                        if(alphaUnitProductList!=undefined)
                        {
                            var productname2=alphaUnitProductList[0].PricebookEntry.Product2.Name;
                            var orderUnit2=helper.addMulitpleProductsToList(alphaUnitProductList,aplhaunitname,productname2);
                            orderUnit2.TotalCost = Number.parseFloat(orderUnit2.TotalCost).toFixed(2).toLocaleString();
                            orderUnit2.TotalCost= helper.commafy(orderUnit2.TotalCost.split('.')[0]);
                            productCompleteList.push(orderUnit2);
                        }
                    }
                }
            }
            else
            {
                var alphaUnits=mapAlphaNumericUnits[unitname];
                if(alphaUnits!=undefined)
                {
                    for(var j=0;j<alphaUnits.length;j++)
                    {
                        var aplhaunitname=alphaUnits[j];
                        var alphaUnitProductList=productWithUnitRoom[aplhaunitname];
                        if(alphaUnitProductList!=undefined)
                        {
                            var productname2=alphaUnitProductList[0].PricebookEntry.Product2.Name;
                            var orderUnit2=helper.addMulitpleProductsToList(alphaUnitProductList,aplhaunitname,productname2);
                            orderUnit2.TotalCost = Number.parseFloat(orderUnit2.TotalCost).toFixed(2).toLocaleString();
                            orderUnit2.TotalCost= helper.commafy(orderUnit2.TotalCost.split('.')[0]);
                            productCompleteList.push(orderUnit2);
                        }
                    }
                }
                
                alphaUnits=mapSpecialCharUnits[unitname];//mapSpecialCharUnits
                if(alphaUnits!=undefined)
                {
                    for(var j=0;j<alphaUnits.length;j++)
                    {
                        var aplhaunitname=alphaUnits[j];
                        var alphaUnitProductList=productWithUnitRoom[aplhaunitname];
                        if(alphaUnitProductList!=undefined)
                        {
                            var productname2=alphaUnitProductList[0].PricebookEntry.Product2.Name;
                            var orderUnit2=helper.addMulitpleProductsToList(alphaUnitProductList,aplhaunitname,productname2);
                            orderUnit2.TotalCost = Number.parseFloat(orderUnit2.TotalCost).toFixed(2).toLocaleString();
                            orderUnit2.TotalCost= helper.commafy(orderUnit2.TotalCost.split('.')[0]);
                            productCompleteList.push(orderUnit2);
                        }
                    }
                }
            }
        }        
        for(var i=0;i<specialCharUnits.length;i++)
        {
            var specialUN=specialCharUnits[i];
            var alphaUnitProductList=productWithUnitRoom[specialUN];
            if(alphaUnitProductList!=undefined)
            {
                var productname2=alphaUnitProductList[0].PricebookEntry.Product2.Name;
                var orderUnit2=helper.addMulitpleProductsToList(alphaUnitProductList,specialUN,productname2);
                orderUnit2.TotalCost = Number.parseFloat(orderUnit2.TotalCost).toFixed(2).toLocaleString();
                orderUnit2.TotalCost= helper.commafy(orderUnit2.TotalCost.split('.')[0]);
                productCompleteList.push(orderUnit2);
            }
        }
        //console.log("value----"+JSON.stringify(productCompleteList));
        // Bind the order and updated product list to the control
		component.set('v.currentOrder', order);
		//component.set('v.products', productsUpdated);
		if(!isPhone)	
        {	
            for(var i=0;i<productCompleteList.length;i++)	
            {	
                var products=productCompleteList[i].Products;	
                for(var j=0;j<products.length;j++)	
                {	
                    var product=products[j];	
                    product.includeToggle=true;    	
                }	
            }	
        }
		component.set('v.products',productCompleteList);
    },
    getToggleButtonValue:function(component,event,helper)
    {
        console.log("_____toggle button called_____");
        var ctarget = event.currentTarget;
        var id_str = event.target.dataset.value;
        console.log("___id_str_____"+id_str);
        var productWrapper=component.get('v.products');
        var productWrapperI=0;
        var productsJ=0;
        for(var i=0;i<productWrapper.length;i++)
        {
        	var products=productWrapper[i].Products;
            for(var j=0;j<products.length;j++)
            {
                var product=products[j];
                if(product.Id==id_str)
                {   
                    if(product.includeToggle==false)
                    {
                    	product.includeToggle=true;    
                    }
                    else
                    {
                        product.includeToggle=false;
                    }
                }
            }
            
        }
        console.log("_______products______"+JSON.stringify(productWrapper));
        component.set('v.products',productWrapper);
    }
})