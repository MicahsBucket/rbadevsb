({
    commafy : function(num){
      var parts = (''+(num<0?-num:num)).split("."), s=parts[0], L, i=L= s.length, o='';
      while(i--){ o = (i===0?'':((L-i)%3?'':',')) 
                      +s.charAt(i) +o }
      return (num<0?'-':'') + o + (parts[1] ? '.' + parts[1] : ''); 
    },
    sortNumberList:function(numbersToSort)
    {
        numbersToSort.sort(this.sortNumber);
        return numbersToSort;
    },
    sortNumber:function(a,b)
    {
        return a-b;
    },
    createProductDetail:function(productName,unitName,unitNames,productRecord)
    {
        return {Name: productName,
                Unit: unitName, 
                Room: productRecord.Room__c, 
                Quantity: (unitNames.length>0?productRecord.Quantity / unitNames.length:productRecord.Quantity),
                TotalCost: (unitNames.length>0?Math.trunc(productRecord.Total_Retail_Price__c / unitNames.length):productRecord.Total_Retail_Price__c),
                Products: [productRecord]};
    },
    startSortingAlphaNumeric:function(alphaNumbericArray) 
    {
        alphaNumbericArray.sort(this.sortAlphaNumeric);
        return alphaNumbericArray;
    },
    sortAlphaNumeric:function(a, b)
    {
        var aA = a.replace(/[^a-zA-Z]/g, "");
        var bA = b.replace(/[^a-zA-Z]/g, "");
        if (aA === bA) 
        {
            var aN = parseInt(a.replace(/[^0-9]/g, ""), 10);
            var bN = parseInt(b.replace(/[^0-9]/g, ""), 10);
            return aN === bN ? 0 : aN > bN ? 1 : -1;
        } 
        else 
        {
            return aA > bA ? 1 : -1;
        }
    },
    isNumeric:function(unitNumber) 
    {
        if(unitNumber!=undefined)
        {
        	return !isNaN(parseFloat(unitNumber)) && isFinite(unitNumber);    
        }
        return null;
    },
    extractNumber:function(unitNumber)
    {
    	return unitNumber.match(/\d+/g);     
    },
    addMulitpleProductsToList:function(productRecords,unitName,productName)
    {  
        var quantity=0;
        var totalCost=0;
        var productsList=[];
        for(var k=0;k<productRecords.length;k++)
        {
            //var unitNames=(unitProductRecord.Unit_Id__c != undefined) ? unitProductRecord.Unit_Id__c.split(',') : [];
            
            var productRecord2=productRecords[k];
            if(productRecord2.Height_Inches__c!=undefined&&productRecord2.Height_Inches__c!=null&&productRecord2.Width_Inches__c!=undefined&&productRecord2.Width_Inches__c!=null)
            {
                if(productsList.length>0)
                {
                    productsList.splice(0, 0, productRecord2); 
                }
                else
                {
                    productsList.push(productRecord2);
                }
            }
            else
            {
                productsList.push(productRecord2);
            }
            var unitNames=(productRecord2.Unit_Id__c != undefined) ? productRecord2.Unit_Id__c.split(',') : [];
            
            quantity=quantity+(unitNames.length>0?productRecord2.Quantity / unitNames.length:productRecord2.Quantity);
            if(productRecord2.Total_Retail_Price__c!=undefined&&productRecord2.Total_Retail_Price__c!=null)
            {
                totalCost=totalCost+(unitNames.length>0?Math.trunc(productRecord2.Total_Retail_Price__c / unitNames.length):productRecord2.Total_Retail_Price__c);
            }
            else
            {
                //system.debug(productRecord2.Total_Retail_Price__c);
            }
        }
        /*var orderUnit={    
            Quantity: quantity,
            TotalCost: totalCost,
            Products: productRecords};*/ 
        var productAtZeroIndex=productRecords[0];
        productName=productsList[0].PricebookEntry.Product2.Name;
        var prodId=productsList[0].Id;
        var orderUnit={Name: productName,
                       FirstProdId:prodId,
                       Unit: (unitName!="undefined")?unitName:'', 
                       Room: productAtZeroIndex.Room__c, 
                       Quantity: quantity,
                       TotalCost: totalCost,
                       Products: productsList};
        return orderUnit;
    },
    checkSpecialCharacters:function(unitId)
    {
        var format = /[ !@#$%^&*()_+\-=\[\]{}'':"";\\/,.<>\/?]/; 
        return format.test(unitId);
    },
    displayToggleButton:function(product,order)
    {
        var toggleCount=0;
        product.displayToggle=false;
        product.excludeToggle1=undefined;
        product.excludeToggle2=undefined;
        product.includeToggle=true;
        if(product.Specialty_Shape__c!=undefined)
        {
            toggleCount++;
            this.excludeToggleFields(toggleCount,'Specialty_Shape__c',product);
        }
        if(product.Height_Inches__c!=undefined && product.Width_Inches__c && order.Status!='Draft')
        {
            toggleCount++;
            this.excludeToggleFields(toggleCount,'Height_Width',product);
        }
        if(product.Interior_Color__c!=undefined)
        {
            toggleCount++;
            this.excludeToggleFields(toggleCount,'Interior_Color__c',product);
        }
        if(product.Exterior_Color__c!=undefined)
        {
            toggleCount++;
            this.excludeToggleFields(toggleCount,'Exterior_Color__c',product);
        }
        if(product.Glazing_S1__c!=undefined && product.Glazing_S2__c!=undefined && product.Glass_Pattern_S1__c!=undefined && product.Glass_Pattern_S2__c!=undefined)
        {
            toggleCount++;
            this.excludeToggleFields(toggleCount,'Glazing_S1__c',product);
        }
        if(product.Grille_Pattern__c!=undefined && product.Grille_Style__c!=undefined)
        {
            toggleCount++;
            this.excludeToggleFields(toggleCount,'Grille_Pattern__c',product);
        }
        if(product.Variant_Number__c!=undefined)
        {
            toggleCount++;
            this.excludeToggleFields(toggleCount,'Variant_Number__c',product);
        }
        if(toggleCount>2)
        {
            product.displayToggle=true;
        }
    },
    excludeToggleFields:function(toggleCount,fieldName,product)
    {
        if(toggleCount<3)
        {
            if(toggleCount==1)
            {
                product.excludeToggle1=fieldName;    
                product.includeToggle=false;
            }
            else if(toggleCount==2)
            {
                product.excludeToggle2=fieldName;    
                product.includeToggle=false;
            }
        }
        //console.log("____toggleCount_____"+toggleCount+"___fieldName____"+fieldName);
        //console.log("____product.excludeToggle1_____"+product.excludeToggle1+"___product.excludeToggle2____"+product.excludeToggle2);
    }
})