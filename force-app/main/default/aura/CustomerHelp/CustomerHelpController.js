({
	init: function(component, event, helper) {
      //call the helper function with pass [component, Controller field and Dependent Field] Api name 
      helper.fetchPicklistValues(component, 'MailingCountryCode', 'MailingStateCode');
   },
    
    // function call on change tha controller field  
   onControllerFieldChange: function(component, event, helper) {
      //alert('Inside one '+event.getSource().get("v.value"));
      // get the selected value
      var scontrollerValueKey = event.getSource().get("v.value");
 		//alert('The selected value is '+scontrollerValueKey);
      //component.set("v.primContact.MailingCountry",controllerValueKey);
      // get the map values   
      var Map = component.get("v.depnedentFieldMap");
 
      // check if selected value is not equal to None then call the helper function.
      // if controller field value is none then make dependent field value is none and disable field
      if (scontrollerValueKey != '--- None ---') {
 
         // get dependent values for controller field by using map[key].  
         // for i.e "India" is controllerValueKey so in the map give key Name for get map values like 
         // map['India'] = its return all dependent picklist values.
         var ListOfDependentFields = Map[scontrollerValueKey];
         helper.fetchDepValues(component, ListOfDependentFields);
 
      } else {
         var defaultVal = [{
            class: "optionClass",
            label: '--- None ---',
            value: '--- None ---'
         }];
         component.find('conState').set("v.options", defaultVal);
         component.set("v.isDependentDisable", true);
      }
   },
 
   // function call on change tha Dependent field    
   onDependentFieldChange: function(component, event, helper) {
      //alert(event.getSource().get("v.value"));
      component.set("v.primContact.MailingState",event.getSource().get("v.value"));
   },
    submitContact21 : function(component,event,helper){
        //alert(1);
        var action1 = component.get("c.submitContact");
       // alert('The values primcontact  '+component.get("v.primContact.FirstName"));
        //alert('The notess is '+component.get("v.noteAreaField"));
        action1.setParams({'notess' : component.get("v.noteAreaField"), 'stopic' : component.get("v.selectedTopic") });
        action1.setCallback(this,function(response){
            //alert(2);
             if (response.getState() === "SUCCESS") {
                 var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success Message',
            message: 'Email Sent Successfully',
            messageTemplate: 'Record {0} created! See it {1}!',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
                 //alert(response.getReturnValue());
                 //alert(3);             	
             }else{
                 //alert(4);  
                 var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error Message',
            message:'Unable to create Contact',
            messageTemplate: 'Mode is pester ,duration is 5sec and Message is overrriden',
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();               
             }            
        });
        $A.enqueueAction(action1);
    }
})