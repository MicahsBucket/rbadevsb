({

   fetchPicklistValues: function(component, event, helper) {
      // call the server side function
      var action = component.get("c.getAllOrders");
      // pass paramerters [object name , contrller field name ,dependent field name] -
      // to server side function

    /*  action.setParams({
         'objApiName': component.get("v.objInfo"),
         'contrfieldApiName': controllerField,
         'depfieldApiName': dependentField
      });*/
      //set callback
      action.setCallback(this, function(response) {
          //alert('The response is '+response.getState());
         if (response.getState() == "SUCCESS") {
            //store the return response from server (map<string,List<string>>)
            var StoreResponse = response.getReturnValue();
            console.log("response 0000000"+JSON.stringify(StoreResponse));
            // once set #StoreResponse to depnedentFieldMap attribute
            component.set("v.depnedentFieldMap", StoreResponse.emailCatOptions);
            component.set("v.FAQWrap",StoreResponse);
            // create a empty array for store map keys(@@--->which is controller picklist values)

            var listOfkeys = []; // for store all map keys (controller picklist values)
            var ControllerField = []; // for store controller picklist value to set on ui field.

            // play a for loop on Return map
            // and fill the all map key on listOfkeys variable.
            for (var singlekey in StoreResponse.emailCatOptions) {
               listOfkeys.push(singlekey);
            }

            //set the controller field value for ui:inputSelect
            if (listOfkeys != undefined && listOfkeys.length > 0) {
               ControllerField.push({
                  label: "--- None ---",
                  value: "--- None ---"
               });
            }

            for (var i = 0; i < listOfkeys.length; i++) {
               ControllerField.push({
                  label: listOfkeys[i],
                  value: listOfkeys[i]
               });
            }
            // set the ControllerField variable values to country(controller picklist field)
            component.set("v.orderList", StoreResponse.selectedOrdersList);
         }
      });
      $A.enqueueAction(action);
   },


   fetchDepValues: function(component, ListOfDependentFields) {
      // create a empty array var for store dependent picklist values for controller field)
      var dependentFields = [];

    /*  if (ListOfDependentFields != undefined && ListOfDependentFields.length > 0) {
         dependentFields.push({
            class: "optionClass",
            label: "--- None ---",
            value: "--- None ---"
         });

      } */
      for (var i = 0; i < ListOfDependentFields.length; i++) {
         dependentFields.push({
            label: ListOfDependentFields[i].strLabel,
            value: ListOfDependentFields[i].strValue
         });
      }
      // set the dependentFields variable values to State(dependent picklist field) on ui:inputselect
      component.set("v.topicList",  ListOfDependentFields);
      // make disable false for ui:inputselect field
      component.set("v.isDependentDisable", false);
   },
})