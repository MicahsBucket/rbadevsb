({	
	//chooses which menu nav to use based on web or mobile
	doInit : function(cmp) {
		if(window.innerWidth <= 800 && window.innerHeight <= 600) {
           var isMobile = true;
           cmp.set("v.mobileOrWeb", isMobile);        
   	    } else {
           var isWeb = false;
     	   cmp.set("v.mobileOrWeb", isWeb);
   	    }
    var getStoreAction = cmp.get("c.getStoreAccount");
    getStoreAction.setCallback(this, function(returnVal){
      var storeAccount = returnVal.getReturnValue();
      cmp.set("v.storeAccount", storeAccount);
    });
    $A.enqueueAction(getStoreAction);

	},
	//menu navigation
    onClickMenu : function(cmp, event, helper) {
        var id = event.target.dataset.menuItemId;
        if (id) {
            cmp.getSuper().navigate(id);
            var oldActiveId = cmp.get("v.active");
            var oldActive = cmp.find(oldActiveId);
            $A.util.removeClass(oldActive, 'active');
            var newActive = cmp.find(id);
            $A.util.addClass(newActive, 'active');
        }
   },
   // creates dropdown menu when you click hamburger icon
   displayMenu : function(cmp, event, helper) {
   		if(!cmp.get("v.menuVisible")){
   			$('.mobileMenu').slideDown();
   			cmp.set("v.menuVisible", true);
   		} else {
   			$('.mobileMenu').slideUp();
   			cmp.set("v.menuVisible", false);
   		}
   },
	
  /*sets menu item to active
            var oldActiveId = cmp.get("v.active");
            var oldActive = cmp.find(oldActiveId);
            $A.util.removeClass(oldActive, 'active');
            var newActive = cmp.find(id);
            $A.util.addClass(newActive, 'active');
    */
})