({

    initializeComponent: function(component, event, helper) {

        var recordId = '';
        var objectName = '';

        // Find the Parent Id and Object Name.  This only works for Desktop Users.
        var pageReference = component.get("v.pageReference");
        var state = pageReference.state; 
        var context = state.inContextOfRef;
        if (context.startsWith("1\.")) {

            context = context.substring(2);
            var addressableContext = JSON.parse(window.atob(context));
            console.log('addressableContext',JSON.stringify(addressableContext));

            recordId = addressableContext.attributes.recordId;
            component.set('v.recordId', recordId);

            objectName = addressableContext.attributes.objectApiName;
            component.set('v.objectName', objectName);

        }

    },

    closeQuickAction: function(component, event, helper) {
        $A.get('e.force:closeQuickAction').fire();
    },
    
    handleCancelClick: function(component, event, helper) {

        var recordId = component.get('{!v.recordId}');

        if (recordId != '') {

            var navigateEvent = $A.get('e.force:navigateToSObject');
            navigateEvent.setParams({
              recordId: recordId,
              slideDevName: "detail",
              isredirect: true
            });
            navigateEvent.fire();

        }

    },

    handleDisableSave: function(component, event, helper) {
        component.set('v.disableSaveButton', true);
    },

    handleEnableSave: function(component, event, helper) {
        component.set('v.disableSaveButton', false);
    },

    handleSaveClick: function(component, event, helper) {
        var newWorkOrderRedirectChildComponent = component.find('newWorkOrderRedirectChildComponent');
        newWorkOrderRedirectChildComponent.handleSaveClick();
    },

    hideSpinner: function(component, event, helper) {
        component.set('v.showSpinner', false);
        component.set('v.disableCancelButton', false);
        component.set('v.disableSaveButton', false);
	},
    
	showSpinner: function(component, event, helper) {
        component.set('v.showSpinner', true);
        component.set('v.disableCancelButton', true);
        component.set('v.disableSaveButton', true);
	},
    
	showToast: function(component, event, helper) {

        var toastMessage = event.getParam('message');
        var toastType = event.getParam('type');
      
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            message: toastMessage,
            type: toastType
        });
        toastEvent.fire();

	}

})