/**
 * @File Name          : addNewOrderProductButton.js
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 6/6/2019, 3:02:26 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/6/2019, 3:02:26 PM   mark.rothermal@andersencorp.com     Initial Version
**/
import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getIsSandboxOrg from '@salesforce/apex/UtilityMethods.isSandboxOrg';	
import getIsrForceUser from '@salesforce/apex/UtilityMethods.isrForceUser';	


const sfClassicEditBaseUrl = '/apex/EditOrderProduct';
const sfAROEditBaseUrl = '/rForceARO/apex/EditOrderProduct';

let partnerResult;
let sandboxResult;

export default class AddNewOrderProductButton extends NavigationMixin(LightningElement) {
    @api orderid;
    @api inrop;
    @api editbaseurl;
	@track isSandboxOrg;
	@track isrForceUser;

    connectedCallback(){
        console.log('hit connectedCallback', this.orderid);
        console.log('hit connectedCallback', this.inrop);

    }
    addProduct(){
        console.log('new add product clicked', this.orderid);
        console.log('new add product clicked', this.inrop);
				getIsSandboxOrg ().then(result => {
		console.log('Sandbox Result', result);
		sandboxResult = result;
			this.isSandboxOrg = sandboxResult;	
			

		getIsrForceUser ().then(result => {
		console.log('Partner Result', result);
		partnerResult = result;
			this.isrForceUser = partnerResult;
		
		}).catch(error => {
			this.error = error;
			console.log('error in callback for get Partner User', error);

		}).then(() => {
        if(this.inrop === true){
            console.log('hit nav mixin');
			window.open(this.editbaseurl+'?orderId='+this.orderid , '_top');
			} else if(this.inrop !== true) {
			if(this.isSandboxOrg === true && this.isrForceUser === true) {
				this.editBaseUrl = sfAROEditBaseUrl;
			} else {
			this.editBaseUrl = sfClassicEditBaseUrl;
			}
			console.log('Base URL: ', this.editBaseUrl);
            window.open(this.editbaseurl+'?orderId='+this.orderid , '_top');
            // this[NavigationMixin.Navigate]({
            //     type: "standard__namedPage",
            //     attributes: {
            //         pageName: "createorderproduct"
            //     }
            // });
        } else {
            console.log('hit standard redirect');
			if(this.isSandboxOrg === true && this.isrForceUser === true) {
			window.open('/rForceARO/apex/EditOrderProduct?orderId='+this.orderid , '_top');
			} else {
            window.open('/apex/EditOrderProduct?orderId='+this.orderid , '_top');
			}
		}
		});


		}).catch(error => {
			this.error = error;
			console.log('error in callback for get Sandbox Org', error);
		});


    }    
}