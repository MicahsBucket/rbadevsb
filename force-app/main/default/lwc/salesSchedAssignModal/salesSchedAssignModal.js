import { LightningElement, api, track, wire } from 'lwc';
import getAppointment from '@salesforce/apex/SalesSchedAppointmentCtrl.getAppointment';
//import getAppointment from '@salesforce/apex/salesSchedPreviousAppt.getAppointment';
import clearResultDetails from '@salesforce/apex/SalesSchedAppointmentCtrl.clearResultantDetails';
import confirmAppointments from "@salesforce/apex/SalesSchedAppointmentCtrl.confirmAppointments";
//import getPastAppointments from '@salesforce/apex/SalesSchedAppointmentCtrl.getPastAppointments';
import deleteRidealong from '@salesforce/apex/SalesSchedAppointmentCtrl.deleteRidealong';
import deleteAssignment from '@salesforce/apex/SalesSchedAppointmentCtrl.deleteAssignment';
import getAvailableCapacities from '@salesforce/apex/SalesSchedAppointmentCtrl.getAvailableCapacities';
import { refreshApex } from '@salesforce/apex';

//The imports below are needed for adding a ride along rep. 
import { createRecord } from 'lightning/uiRecordApi';
import { updateRecord } from 'lightning/uiRecordApi';
import SALES_APPOINTMENT_RESOURCE_OBJECT from '@salesforce/schema/Sales_Appointment_Resource__c';
import SALES_CAPACITY_FIELD from '@salesforce/schema/Sales_Appointment_Resource__c.Sales_Capacity__c';
import SALES_APPOINTMENT_FIELD from '@salesforce/schema/Sales_Appointment_Resource__c.Sales_Appointment__c';
import STATUS_FIELD from '@salesforce/schema/Sales_Appointment_Resource__c.Status__c';
import PRIMARY_FIELD from '@salesforce/schema/Sales_Appointment_Resource__c.Primary__c';
import ID_FIELD from '@salesforce/schema/Sales_Appointment_Resource__c.Id';
import REASON_FIELD from '@salesforce/schema/Sales_Appointment_Resource__c.Assignment_Reason__c';

import findFinanceCompany from '@salesforce/apex/SalesSchedAppointmentCtrl.getFinanceCompanies';
import findFinancePlan from '@salesforce/apex/SalesSchedAppointmentCtrl.getFinancePlans';

import RESULT_FIELD from '@salesforce/schema/Sales_Order__c.Result__c'
import PAYMENT_TYPE_FIELD from '@salesforce/schema/Sales_Order__c.Payment_Type__c';
import RESCHEDULE_FIELD from '@salesforce/schema/Sales_Order__c.Rescheduled__c';
import FINANCE_ISSUES_FIELD from '@salesforce/schema/Sales_Order__c.Finance_Issues__c';
import STORE_FINANCE_PROGRAM_FIELD from '@salesforce/schema/Sales_Order__c.Store_Finance_Program__c';
import TURNED_DOWN_BY_FIELD from '@salesforce/schema/Sales_Order__c.Turned_Down_By__c';

const detailcols = [
  { label: 'Date', fieldName: 'lastSalesDate' },
  { label: 'Result', fieldName: 'salesOrderResult' },
 // { label: 'Result', fieldName: 'salUrl', type: 'url', typeAttributes: {label: { fieldName: 'salesOrderResult' }, target: '_blank'}},
  { label: 'Done By', fieldName: 'opptyOwner' },
  { type: 'button',  typeAttributes: { label: "Resulted Details",  title: "Resulted Details", variant: "828f69"  } }   
];

const DELAY = 300;

export default class SalesSchedAssignModal extends LightningElement {
  @api appointmentId; //Passed in by a parent component to determine what appointment to display.
  @api displayRideAlongDetails; //Passed in by a parent component, determines if ride along info is shown (e.g. if the appointment is already assigned.)
  @api readOnly;
  @api prepopulatedRepId;
  @api prepopulatedRepName;
  @track isPhoneAvailable = false;
  @api capacityId;
  @api unassigned = false;
  @track repId; //Specified if this appointment is assigned to a specific rep.
  @track repName; //Specified if this appointment is assigned to a specific rep.
  @track appwrapper; //This variable will be populated once the appointmentId updates and apex is called to fetch the appointment info.
  @track error;
  @track showridealong; //Set to true IF displayRideAlongDetails is true, and if the user clicked on the add ride along rep button.
  @track detailcols = detailcols;
  @track ridealongs = false;
  @track toggleUpdateAssignment = false;
  @track assignable;
  appointmentReturn;
  primaryResourceId;
  @track assignmentSearchKey = '';
  @track selectedAssignment;
  @track selectedAssignmentObject;
  @track assignmentFocus;
  @track openResultant = false;
  @track openResultantInEditMode=false;
  @track capacities;//238
  @track salesRepCapacities;
  @track acprimaryContact;
  @track acprimaryContactBol = false;
  @track selSaleOrder;
  @track isNotFinalized;
  @track resultOptions;
  @track resultStatus;

  @track paymentType;
  @track financeIssues;
	@track followups;
	@track financeCompanySearchKey = '';
	@track financePlanSearchKey = '';
	@track selectedFinanceCompany;
	@track selectedFinanceProgram;
	@track selectedFinanceProgramObject;
	@track selectedFinanceCompanyObject;
	@track financePlanFocus;
	@track financeCompanyFocus;
	//
	@track turnedDownBySearchKey = '';
	@track selectedTurnedDownBy;
	@track selectedTurnedDownByObject;
  @track turnedDownByFocus;
  
  //below statements are created by ratna for highlighting field if it is empty
  @track WhyNotCovered;
  @track keyContactPerson;
  @track Comments;
  @track ProjectDescription;
  @track whyNoDemo;
  @track followUpDateTime;
  @track mail;
  @track ownerPresent;
  @track seriesOneWindow;
  @track seriesTwoWindow;
  @track seriesAQuote;
  @track patioDoors;
  @track entryDoors;
  @track amountIfSold;
  @track seriesOneQuote;
  @track seriesTwoQuote;
  @track patioQuote;
  @track entryQuote;
  @track caQuote;
  @track cashField;
  @track bayWQuote;
  @track bowWQuote;
  @track keyReason;
  @track futurePurchase;
  @track depositType;
  @track deposit;
  @track construction;
  @track customer;
  @track concerns;
  @track addressConcerns;
  @track partialSale;
  @track historicalArea;
  @track accountNumber;
  @track approved;
  @track renewal;
  @track isReadyToSubmit=false;
  @track reschedule;

  @wire(getAvailableCapacities, { appointmentId: '$appointmentId', searchText: '$assignmentSearchKey' })
  //capacities; //238
  populateCapacity(result) {
    this.mainCapacity = result;
    this.capacities = result.data;
    this.salesRepCapacities=[];
    console.log("_____this.capacities____"+JSON.stringify(this.capacities));
    if (result.data!=undefined) {
      for (var i = 0; i < result.data.length; i++) {
        console.log("____indiex value____"+i);
        var capacity = result.data[i];
        console.log("___capacity____"+JSON.stringify(capacity));
        if (!capacity.isDeferred) {
          this.salesRepCapacities.push(capacity);
        }
      }
    }
    console.log("____capacitites__ratna____"+JSON.stringify(result.data));
    // rest of the code
  }

  @wire(findFinanceCompany, {
		configId: '$appwrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__c',
		searchText: '$financeCompanySearchKey'
	})
	financeCompanies;

	@wire(findFinanceCompany, {
    configId: '$appwrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__c',
		searchText: '$turnedDownBySearchKey'
	})
	turnedDownByCompanies;

	@wire(findFinancePlan, {
    configId: '$appwrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__c',
		companyId: '$selectedFinanceCompany',
		searchText: '$financePlanSearchKey'
	})
	financePlans; 

  //Get the appointment information that is displayed in the modal.
  @wire(getAppointment, { appointmentId: '$appointmentId' })
  handleGetAppointment(appointmentReturn) {
    this.appointmentReturn = appointmentReturn;
    let ridealongs = [];
    let repName = undefined;
    let repId = undefined;
    let primaryResourceId = undefined;
    this.isNotFinalized=false;
    if (appointmentReturn && appointmentReturn.data && appointmentReturn.data.primaryContact) {
      if(appointmentReturn.data.primaryContact.HomePhone || appointmentReturn.data.primaryContact.MobilePhone){
        this.isPhoneAvailable = true;
      }
      if (appointmentReturn.data.appointment.Sales_Appointment_Resources__r) {

        appointmentReturn.data.appointment.Sales_Appointment_Resources__r.forEach(rep => {
          if (rep.Primary__c) {
            repId = rep.Sales_Capacity__r.User__r.Id;
            repName = rep.Sales_Capacity__r.User__r.Name;
            primaryResourceId = rep.Id;
          }
          if (rep.Sales_Capacity__r.User__r.Id === this.prepopulatedRepId && (rep.Status__c === 'Manually Assigned' || rep.Status__c === 'Assigned' || rep.Status__c === 'Manually Reassigned')) {
            //appointmentReturn.data.isLoggedInUserStatusConfirmed = true;
            this.isNotFinalized=true;
          }
          ridealongs.push({
            type: 'icon',
            label: rep.Sales_Capacity__r.User__r.Name + (rep.Primary__c ? ' (Primary)' : ' (Ride Along)'),
            name: rep.Id,
            iconName: 'utility:resource_capacity',
            alternativeText: rep.Sales_Capacity__r.User__r.Name,
            showClose: !rep.Primary__c
          });
        });
      }
      
      this.repId = repId;
      this.primaryResourceId = primaryResourceId;
      this.repName = repName;
      this.ridealongs = ridealongs;
      this.appwrapper = appointmentReturn.data;
      
      this.displayRideAlongDetails = true;
    }

    if (appointmentReturn && appointmentReturn.error) {
      if (!appointmentReturn.error.message && appointmentReturn.error.body.message) {
        this.error = { message: appointmentReturn.error.body.message };
        return;
      }
      this.error = appointmentReturn.error;
    }
    this.resultOptions = [
      { label: '-- None --', value: ''},
      { label: 'Sale', value: 'Sale' },
      { label: 'Demo No Sale', value: 'Demo No Sale' },
      { label: 'No Demo', value: 'No Demo' },
      { label: 'Not Home', value: 'Not Home' },
      { label: 'Not Covered', value: 'Not Covered' }
    ];
    this.paymentTypeOptions = [
      { label: '-- None --', value: '' },
      { label: 'Cash', value: 'Cash' },
      { label: 'Finance', value: 'Finance' },
      { label: 'Cash & Finance', value: 'Cash & Finance' },
      { label: 'Finance Turn-Down', value: 'Finance Turn-Down' }
    ];
    this.yesNoOptions = [
      { label: '-- None --', value: ''},
      { label: 'Yes', value: 'Yes' },
      { label: 'No', value: 'No' }
    ];
  }

  handleToggleUpdateAssignment() {
    this.toggleUpdateAssignment = true;
  }
 //238
  @api
  getAvailableCapacity() {
    if (this.mainCapacity.data || this.mainCapacity.error)
    refreshApex(this.mainCapacity);
  }
  handleFinalizeFromAppointmentCard() {
    console.log("_____this.appointmentReturn.data.appointment.Sales_Appointment_Resources__r____"+JSON.stringify(this.appointmentReturn.data.appointment.Sales_Appointment_Resources__r));
    confirmAppointments({
      resourceListStr:JSON.stringify(this.appointmentReturn.data.appointment.Sales_Appointment_Resources__r)
    })
    .then(result => {
      this.reset();
      this.dispatchEvent(new CustomEvent('refresh'));
    })
    .catch(error => {
      this.appwrapper = false;
      this.error = error;
    });
  }
  //When the lightning data service record is submitted for the primary rep, then this is called.
  handleAssignSubmit(event) {
  
    if (!this.selectedAssignment) {
      return;
    }

    event.preventDefault();
    const newFields = {};

    newFields[SALES_CAPACITY_FIELD.fieldApiName] = this.selectedAssignment;
    newFields[SALES_APPOINTMENT_FIELD.fieldApiName] = this.appointmentId;
    newFields[PRIMARY_FIELD.fieldApiName] = true;
    newFields[STATUS_FIELD.fieldApiName] = (this.primaryRepId ? 'Manually Reassigned' : 'Manually Assigned');
    newFields[REASON_FIELD.fieldApiName] = 'Manual';

    if (this.primaryRepId) {
      newFields[ID_FIELD.fieldApiName] = this.primaryRepId;
      const recordInput = { fields: newFields };
      updateRecord(recordInput)
        .then(result => {
          this.reset();
          this.dispatchEvent(new CustomEvent('refresh'));
        })
        .catch(error => {
          this.appwrapper = false;
          this.error = error;
        });
    } else {
      const recordInput = { apiName: SALES_APPOINTMENT_RESOURCE_OBJECT.objectApiName, fields: newFields };
      createRecord(recordInput)
        .then(result => {
          this.reset();
          this.dispatchEvent(new CustomEvent('refresh'));
        })
        .catch(error => {
          this.appwrapper = false;
          this.error = error;
        });
    }
   
   
  }

  handlePrepopulatedSubmit() {
    const newFields = {};
    newFields[SALES_CAPACITY_FIELD.fieldApiName] = this.capacityId;
    newFields[SALES_APPOINTMENT_FIELD.fieldApiName] = this.appointmentId;
    newFields[PRIMARY_FIELD.fieldApiName] = true;
    newFields[STATUS_FIELD.fieldApiName] = (this.primaryRepId ? 'Manually Reassigned' : 'Manually Assigned');
    newFields[REASON_FIELD.fieldApiName] = 'Manual';

    const recordInput = { apiName: SALES_APPOINTMENT_RESOURCE_OBJECT.objectApiName, fields: newFields };
    createRecord(recordInput)
      .then(result => {
        this.reset();
        this.dispatchEvent(new CustomEvent('refresh'));
      })
      .catch(error => {
        this.appwrapper = false;
        this.error = error;
      });
  }

  //Displays the lightning data service / record form for ride along rep input.
  handleAddRideAlong() {
    this.showridealong = true;
  }
 
  //When the lightning data service record is submitted for adding a ride along rep, then this is called.
  handleRidealongSubmit(event) {
      if (!this.selectedAssignment) {
      return;
      }

    event.preventDefault();

    const newFields = {};
    newFields[SALES_CAPACITY_FIELD.fieldApiName] = this.selectedAssignment;
    newFields[SALES_APPOINTMENT_FIELD.fieldApiName] = this.appointmentId;
    newFields[PRIMARY_FIELD.fieldApiName] = false;
    newFields[STATUS_FIELD.fieldApiName] = 'Manually Assigned';
    const recordInput = { apiName: SALES_APPOINTMENT_RESOURCE_OBJECT.objectApiName, fields: newFields };
    createRecord(recordInput)
      .then(result => {
        this.reset();
        this.dispatchEvent(new CustomEvent('refresh'));
        //this.showridealong = false;
        //this.error = false;
       // refreshApex(this.appointmentReturn);
       // refreshApex(this.capacities);
      })
      .catch(error => {
        this.error = error;
        this.showridealong = false;
      });
  }

  handleRidealongRemove(event) {
    let ridealong = event.detail;
    debugger;
    deleteRidealong({ appointmentResourceId: ridealong })
      .then(result => {
        //refreshApex(this.appointmentReturn);
        this.reset();
        this.dispatchEvent(new CustomEvent('refresh'));
      })
      .catch(error => { });
  }

  handleRemoveAssignment(event) {
    try {
      if (this.appointmentId) {
        deleteAssignment({ appointmentId: this.appointmentId })
          .then(result => {
            this.reset();
            this.dispatchEvent(new CustomEvent('refresh'));
          })
          .catch(error => { });
      }
    } catch (ex) {
      console.log(ex);
    }
  }

  handleErrorModalClose() {
    this.error = false;
  }

  handleAssignmentModalClose() {
    //this.appwrapper = false;
    //this.appointmentId = undefined;
    this.reset();
    this.dispatchEvent(new CustomEvent('assignmodalclose'));
  }
  handleAssignmentChange(event) {
    // Debouncing this method: Do not update the reactive property as long as this function is
    // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
    window.clearTimeout(this.delayTimeout);
    const searchKey = event.target.value;

    // eslint-disable-next-line @lwc/lwc/no-async-operation
    this.delayTimeout = setTimeout(() => {
      this.assignmentSearchKey = searchKey;
    }, DELAY);
  }

  handleAssignmentClick(event) {
  
    this.selectedAssignment = event.detail.value;
    this.selectedAssignmentObject = event.detail;
   
  }

  handleAssignmentRemove() {
    this.selectedAssignment = undefined;
    this.selectedAssignmentObject = undefined;
  }

  handleRemoveAssignmentFocus() {
    this.assignmentFocus = false;
  }

  handleAssignmentFocus() {
    this.assignmentFocus = true;
  }
  /*handleResultantData() {
    //console.log("event function:");
    //const row = event.detail.row;
    //console.log("event function after:", row);
    this.openResultant = true;
  }*/
   
    handleResultantData() {
    this.openResultant = true;
  }
  handleResultantDataInEdit() {
    this.openResultantInEditMode=true;
    //this.isReadyToSubmit=true;
    this.resultStatus = this.appwrapper.appointment.Sales_Order__r.Result__c;
    if (this.appwrapper.appointment.Sales_Order__r.Store_Finance_Program__r && this.resultStatus==='Sale') {
      let sfp = this.appwrapper.appointment.Sales_Order__r.Store_Finance_Program__r;
      this.selectedFinanceProgramObject = {
        label: sfp.Name,
        value: sfp.Id
      };
      this.selectedFinanceCompanyObject = {
        label: sfp.Finance_Company__r.Name,
        value: sfp.Finance_Company__r.Id
      };
      this.selectedFinanceProgram = sfp.Id;
      this.selectedFinanceCompany = sfp.Finance_Company__r.Id;
    }
    else {
      this.selectedFinanceProgram = undefined;
      this.selectedFinanceCompany = undefined;
      this.selectedFinanceProgramObject = undefined;
      this.selectedFinanceCompanyObject = undefined;
    }
    if(this.appwrapper.appointment.Sales_Order__r.Turned_Down_By__c && this.resultStatus==='Sale'){
      console.log("CD: Setting the selected object for turned down by to ", this.appwrapper.appointment.Sales_Order__r.Turned_Down_By__c);
      this.selectedTurnedDownByObject = {
        label: this.appwrapper.appointment.Sales_Order__r.Turned_Down_By__r.Name,
        value: this.appwrapper.appointment.Sales_Order__r.Turned_Down_By__r.Id
      };

      this.selectedTurnedDownBy = this.appwrapper.appointment.Sales_Order__r.Turned_Down_By__r.Id;
    }
    else{
      this.selectedTurnedDownByObject = null;
      this.selectedTurnedDownBy = null;
    }
    console.log("______this.appwrapper.appointment__________"+JSON.stringify(this.appwrapper.appointment));
    this.paymentType = this.appwrapper.appointment.Sales_Order__r.Payment_Type__c;
    this.reschedule = this.appwrapper.appointment.Sales_Order__r.Rescheduled__c;
    this.financeIssues = this.appwrapper.appointment.Sales_Order__r.Finance_Issues__c;
    /* Below code written by Ratna for hightlighting fields*/
		this.WhyNotCovered=this.appwrapper.appointment.Sales_Order__r.Why_Not_Covered__c;
		this.keyContactPerson=this.appwrapper.appointment.Sales_Order__r.Key_Contact_Person__c;
		this.Comments=this.appwrapper.appointment.Sales_Order__r.Comments__c;
		this.ProjectDescription=this.appwrapper.appointment.Sales_Order__r.Project_Description__c;
		this.whyNoDemo=this.appwrapper.appointment.Sales_Order__r.Why_No_Demo__c;
		this.followUpDateTime=this.appwrapper.appointment.Sales_Order__r.Follow_Up_Appt_Date_Time__c;
		this.mail=this.appwrapper.appointment.Sales_Order__r.Mail_and_Canvass_Neighborhood__c;
		this.ownerPresent=this.appwrapper.appointment.Sales_Order__r.Were_All_Owners_Present__c;
		this.seriesOneWindow=this.appwrapper.appointment.Sales_Order__r.Series_One_Windows__c;
    this.seriesTwoWindow=this.appwrapper.appointment.Sales_Order__r.Series_Two_Windows__c;
    this.seriesAQuote = this.appwrapper.appointment.Sales_Order__r.Includes_Series_A_Window__c;
		this.patioDoors=this.appwrapper.appointment.Sales_Order__r.Patio_Doors__c;
		this.entryDoors=this.appwrapper.appointment.Sales_Order__r.Entry_Doors__c;
		this.amountIfSold=this.appwrapper.appointment.Sales_Order__r.Amount_If_Sold__c;
		this.seriesOneQuote=this.appwrapper.appointment.Sales_Order__r.Series_One_Windows_Quoted__c;
		this.seriesTwoQuote=this.appwrapper.appointment.Sales_Order__r.Series_Two_Windows_Quoted__c;
		this.patioQuote=this.appwrapper.appointment.Sales_Order__r.Patio_Doors_Quoted__c;
		this.entryQuote=this.appwrapper.appointment.Sales_Order__r.Entry_Doors_Quoted__c;
		this.caQuote=this.appwrapper.appointment.Sales_Order__r.Contract_Amount_Quoted__c;
		this.bayWQuote=this.appwrapper.appointment.Sales_Order__r.BAY_Windows_Quoted__c;
		this.bowWQuote=this.appwrapper.appointment.Sales_Order__r.BOW_Windows_Quoted__c;
		this.keyReason=this.appwrapper.appointment.Sales_Order__r.Key_Reason_Customer_Didnt_Proceed__c;
		this.futurePurchase=this.appwrapper.appointment.Sales_Order__r.Future_Purchase__c;
		this.depositType=this.appwrapper.appointment.Sales_Order__r.Deposit_Type__c;
		this.deposit=this.appwrapper.appointment.Sales_Order__r.Deposit__c;
		this.construction=this.appwrapper.appointment.Sales_Order__r.Construction_Department_Notes__c;
		this.customer=this.appwrapper.appointment.Sales_Order__r.Why_Customer_Purchased__c;
		this.concerns=this.appwrapper.appointment.Sales_Order__r.Concerns__c;
		this.addressConcerns=this.appwrapper.appointment.Sales_Order__r.Address_Concerns__c;
		this.partialSale=this.appwrapper.appointment.Sales_Order__r.Partial_Sale__c;
		this.historicalArea=this.appwrapper.appointment.Sales_Order__r.Historical_Area__c;
		this.accountNumber=this.appwrapper.appointment.Sales_Order__r.Account_Number__c;
		this.approved=this.appwrapper.appointment.Sales_Order__r.Approved__c;
		this.renewal=this.appwrapper.appointment.Sales_Order__r.Next_For_Renewal__c;
		this.cashField=this.appwrapper.appointment.Sales_Order__r.Cash__c;
		//this.reschedulePK=this.appwrapper.appointment.Sales_Order__r.Rescheduled__c;
		/* Till Here */	
		
  }
  handleCloseEditMode() {
    this.openResultant = false;
    this.openResultantInEditMode=false;
    //this.resultStatus = undefined;
		//this.paymentType = undefined;
		//this.displayResulting = undefined;
  }
  handleResultChange(event) {
		this.resultStatus = event.detail.value;
		console.log("____resultStatus______"+this.resultStatus);
  }
  handleRescheduleChange(event) {
		this.reschedule = event.detail.value;
		//this.reschedulePK=event.detail.value;
	}
  handleResultingSuccess() {
		this.displayResulting = false;
		this.resultStatus = false;
    this.paymentType = false;
    this.reset();
		this.dispatchEvent(new CustomEvent('refresh'));
	}
  handleResultingError(event) {
		console.log("____error_event.detail__" + JSON.stringify(event.detail));
		this.error = event.detail;
  }
  
  handleResultingSubmit(event) {
    event.preventDefault(); // stop the form from submitting
    
    const fields = event.detail.fields;
    fields[RESULT_FIELD.fieldApiName] = (this.resultStatus ? this.resultStatus : '');
    fields[PAYMENT_TYPE_FIELD.fieldApiName] = (this.paymentType ? this.paymentType : '');
    fields[RESCHEDULE_FIELD.fieldApiName] = (this.reschedule ? this.reschedule : '');
    fields[FINANCE_ISSUES_FIELD.fieldApiName] = (this.financeIssues ? this.financeIssues : '');
    fields[STORE_FINANCE_PROGRAM_FIELD.fieldApiName] = (this.selectedFinanceProgram ? this.selectedFinanceProgram : '');
    fields[TURNED_DOWN_BY_FIELD.fieldApiName] = (this.selectedTurnedDownBy ? this.selectedTurnedDownBy : '');
    console.log("____this.resultStatus_____"+this.resultStatus);
    let el = this.template.querySelector('lightning-record-edit-form');
    console.log("_____fields[RESULT_FIELD.fieldApiName]______"+JSON.stringify(fields[RESULT_FIELD.fieldApiName]));
		if (el) {
			el.submit(fields);
			//this.dispatchEvent(new CustomEvent('refreshappointments'));
		}
	}
  
  handleMultiResultData(event){
    const row = event.detail.row;
    this.selSaleOrder = row;
    this.acprimaryContactBol = true; 
    console.log("Event Console" , JSON.stringify(row));
      }

    handleCloseAction(event){
    this.acprimaryContactBol = false; 
        }
  
  clearResultantDetails() { 
    let salesOrderStatus=this.appwrapper.appointment.Sales_Order__r.Result__c;
    // eslint-disable-next-line eqeqeq
    if(salesOrderStatus=='Sale')
    {
      // eslint-disable-next-line no-alert
      alert("Sales Order is resulted as a Sale, you cannot clear Result Details. Managers can edit the Sales Order, or Contact rSupport for assistance.");
      return;
    }
    // eslint-disable-next-line no-alert
    let clrDetails = confirm("Are you sure you want to clear Result Details? This cannot be undone.");
    if (clrDetails) {
      clearResultDetails({ salesorderId: this.appwrapper.appointment.Sales_Order__c })
      .then(() => {
        //refreshApex(this.appointmentReturn);
        this.reset();
        this.dispatchEvent(new CustomEvent('refresh'));
      })
      .catch(error => { 
        console.log("____error______"+JSON.stringify(error));
      });
    }
  }
  closeResultant() {
    this.openResultant = false;
    this.openResultantInEditMode=false;
  }
  noAction()
  {

  }
  reset() {
    refreshApex(this.appointmentReturn);
    this.appwrapper = false;
    this.isPhoneAvailable = false;
    this.appointmentId = undefined;
    this.capacityId = undefined;
    this.error = false;
    this.repId = undefined;
    this.repName = undefined;
    this.prepopulatedRepId = undefined;
    this.prepopulatedRepName = undefined;
    this.unassigned = false;
    this.toggleUpdateAssignment = false;
    this.ridealongs = false;
    this.displayRideAlongDetails = false;
    this.openResultant = false;
    this.openResultantInEditMode=false;
    this.dispatchEvent(new CustomEvent('reset'));
  }

  /** Getters **/

  /**Ratna written below getters for Resultant Details */
  handleFinancePlanChange(event) {
		// Debouncing this method: Do not update the reactive property as long as this function is
		// being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
		window.clearTimeout(this.delayTimeout);
		const searchKey = event.target.value;

		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.delayTimeout = setTimeout(() => {
			this.financePlanSearchKey = searchKey;
		}, DELAY)
	}
  handleFinancePlanFocus() {
		this.financePlanFocus = true;
  }
  handleRemovePlanFocus() {
		this.financePlanFocus = false;
	}
  handleCompanyClick(event) {
		this.selectedFinanceCompany = event.detail.value;
		this.selectedFinanceCompanyObject = event.detail;
  }
  handleCompanyRemove() {
		this.selectedFinanceCompany = undefined;
		this.selectedFinanceProgram = undefined;
		this.selectedFinanceCompanyObject = undefined;
		this.selectedFinanceProgramObject = undefined;
  }
  handleFinanceCompanyFocus() {
		this.financeCompanyFocus = true;
	}
  handleRemoveCompanyFocus() {
		this.financeCompanyFocus = false;
  }
  handlePlanClick(event) {
		this.selectedFinanceProgram = event.detail.value;
		this.selectedFinanceProgramObject = event.detail;
  }
  handleFinanceIssuesChange(event) {
		this.financeIssues = event.detail.value;
	}
  handleFinanceCompanyChange(event) {
		// Debouncing this method: Do not update the reactive property as long as this function is
		// being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
		window.clearTimeout(this.delayTimeout);
		const searchKey = event.target.value;

		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.delayTimeout = setTimeout(() => {
			this.financeCompanySearchKey = searchKey;
		}, DELAY)
  }
  handleRemoveTurnedDownByFocus() {
		this.turnedDownByFocus = false;
	}

	handleTurnedDownByFocus() {
		this.turnedDownByFocus = true;
  }
  handleTurnedDownByChange(event) {
		// Debouncing this method: Do not update the reactive property as long as this function is
    // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
		window.clearTimeout(this.delayTimeout);
    const searchKey = event.target.value;

		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.delayTimeout = setTimeout(() => {
      this.turnedDownBySearchKey = searchKey;
		}, DELAY)
  }
  handleTurnedDownByClick(event) {
		this.selectedTurnedDownBy = event.detail.value;
		this.selectedTurnedDownByObject = event.detail;
	}

	handleTurnedDownByRemove() {
		this.selectedTurnedDownBy = undefined;
		this.selectedTurnedDownByObject = undefined;
	}
  get displayResultedButton() {
    return this.appwrapper.appointment.Sales_Order__r.Resulted__c;
  }
  get displayReResultedButton() {
    return this.appwrapper.appointment.Sales_Order__r.Re_Result_Option__c;
  }
  get displaySaleResulted() {
    if(this.openResultantInEditMode === false) {
      return this.appwrapper.appointment.Sales_Order__r.Result__c === 'Sale';
    }
    else {
      return this.resultStatus && ('Sale' === this.resultStatus);
    }
  }
  get displaySaleDemoNoSaleOrNoDemoResulted() {
    return this.displaySaleResulted || this.displayDemoNoSaleResulted || this.displayNoDemoResulted;
  }
  get displaySaleOrDemoNoSaleResulted() {
    if(this.openResultantInEditMode === false) {
      let result = this.appwrapper.appointment.Sales_Order__r.Result__c;
      return ('Sale' === result || 'Demo No Sale' === result);
    }
    else {
      return this.resultStatus && ('Sale' === this.resultStatus || 'Demo No Sale' === this.resultStatus);
    }
  }
  get displaySeriesTwo() {
    return this.appwrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_2__c;
  }
  get displaySeriesA() {
    return this.appwrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_A__c;
  }
  get displayEntryDoor() {
    return this.appwrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Entry_Doors__c;
  }
  get displayDemoNoSaleResulted() {
    if(this.openResultantInEditMode === false) {
      return 'Demo No Sale' === this.appwrapper.appointment.Sales_Order__r.Result__c;
    }
    else {
      return this.resultStatus && ('Demo No Sale' === this.resultStatus);
    }
  }
  handlePaymentTypeChange(event) {
    this.paymentType = event.detail.value;
  }
  get displayStoreFinance()
  {
    return this.paymentType!=null;
  }
  get displayFinance() {
    console.log("_____this.paymentType____displayFinance____"+JSON.stringify(this.paymentType));
    
  }
  get displayFinanceResulted() {
    if(this.openResultantInEditMode === false) {
      return this.appwrapper.appointment.Sales_Order__r.Payment_Type__c === 'Finance';
    }
    else {
      return this.paymentType && 'Finance' === this.paymentType;    
    }
  }
  get displayCashAndFinanceResulted() {
    if(this.openResultantInEditMode === false) {
      return this.appwrapper.appointment.Sales_Order__r.Payment_Type__c === 'Cash & Finance';
    }
    else {
      return this.paymentType && 'Cash & Finance' === this.paymentType;
    }
  }
  get displayFinanceTurnDownResulted() {
    if(this.openResultantInEditMode === false) {
      return this.appwrapper.appointment.Sales_Order__r.Payment_Type__c === 'Finance Turn-Down';
    }
    else {
      return this.paymentType && 'Finance Turn-Down' === this.paymentType;
    }
  }
  get displayFinanceDetailsResulted() {
    return 'No' === this.appwrapper.appointment.Sales_Order__r.Finance_Issues__c;
  }
  get displayNoDemoResulted() {
    if(this.openResultantInEditMode === false) {
      return 'No Demo' === this.appwrapper.appointment.Sales_Order__r.Result__c;
    }
    else {
      return this.resultStatus && 'No Demo' === this.resultStatus;
    }
  }
  get displayFollowup() {
    if(this.openResultantInEditMode === false) {
      return 'Yes' === this.appwrapper.appointment.Sales_Order__r.Rescheduled__c;
    }
    else {
      return this.reschedule && 'Yes' === this.reschedule;
    }
  }
  get displayNotHomeResulted() {
    if(this.openResultantInEditMode === false) {
      return 'Not Home' === this.appwrapper.appointment.Sales_Order__r.Result__c;
    }
    else {
      return this.resultStatus && 'Not Home' === this.resultStatus;
    }
  }
  get displayNotCoveredResulted() {
    if(this.openResultantInEditMode === false) {
      return 'Not Covered' === this.appwrapper.appointment.Sales_Order__r.Result__c;
    }
    else {
      return this.resultStatus && 'Not Covered' === this.resultStatus;
    }
  }
  get displayRideAlongAdvice() {
    return this.appwrapper && this.appwrapper.hasRideAlongs;
  }
  /**till here--Resultant details getters */
  get primaryRepId() {
    return ((this.appwrapper && this.appwrapper.primaryRep) ? this.appwrapper.primaryRep.Id : false);
  }

  get additionalDetailsText() {
    return this.showdetails ? 'Hide Additional Details' : 'Show Additional Details';
  }

  get showAssignedTo() {
    return !this.unassigned && this.repName && !this.toggleUpdateAssignment;
  }

  get showPrepopulatedName() {
    return !this.unassigned && !this.repName && !this.toggleUpdateAssignment;
  }

  get showUpdateAssignment() {
    return this.unassigned || this.toggleUpdateAssignment;
  }

  get details() {
    //console.log(JSON.stringify(this.appwrapper.appointment.));
    console.log(JSON.stringify(this.appwrapper.actSalOrdHist));
    this.acprimaryContact = this.appwrapper.actSalOrdHist.con;
    let details = [];
    return this.appwrapper.actSalOrdHist.asaccwrap;
  /*  if (this.appwrapper && this.appwrapper.appointment) {
      let opp = this.appwrapper.appointment.Sales_Order__r.Opportunity__r;
      if (opp.Last_Sale_Date__c && opp.Last_Sale_Rep__c) {
        let d = moment(opp.Last_Sale_Date__c).format();
        details.push({
          doneby: opp.Last_Sale_Rep__c,
          date: d,
          result: 'Sale'
        });
      }

      if (opp.Last_DNS_Date__c && opp.Last_DNS_Rep__c) {
        let d = moment(opp.Last_DNS_Date__c).format();
        details.push({
          doneby: opp.Last_DNS_Rep__c,
          date: d,
          result: 'Demo No Sale'
        });
      }
    }

    if (details.length > 0) {
      debugger;
      return details.sort((d1, d2) => moment(d2.date) - moment(d1.date));
    }
    debugger;
    return false; */
  }
  /* Below methods written by Ratna for highlightning fields */
	checkValue(event)
	{
		this.WhyNotCovered=event.target.value;
	}
	keyContactValue(event)
	{
    this.keyContactPerson=event.target.value;
    console.log("______this.keyContactPerson______"+JSON.stringify(this.keyContactPerson));
	}
	commentsValue(event)
	{
		this.Comments=event.target.value;
	}
	projectDescriptionValue(event)
	{
		this.ProjectDescription=event.target.value;
	}
	whyNoDemoValue(event)
	{
		this.whyNoDemo=event.target.value;
  }
	get WhyNotCoveredColor()
	{
    return this.WhyNotCovered===undefined||this.WhyNotCovered.length===0?'changeColorRed':'';
  }
  get isSaveEnabled()
  {
    if(this.resultStatus==='Not Covered')
    {
      if(this.WhyNotCoveredColor===''&&this.keyContactPersonColor==='')
      {
        return true;
      }
    }
    if(this.resultStatus==='Not Home')
    {
      if(this.commentsColor===''&&this.projectDescriptionColor===''&&this.keyContactPersonColor==='')
      {
        return true;
      }
    }
    if(this.resultStatus==='No Demo')
    {
      if(this.keyContactPersonColor===''&&this.ownerPresentColor===''&&this.whyNoDemoColor===''&&this.reschedulePKColor===''&&this.mailColor==='')
      {
        if(this.reschedule==='Yes'&&this.followUpDateTimeColor==='')
        {
          return true;
        }
        if(this.reschedule==='No')
        {
          return true;
        }
      }
    }
    if(this.resultStatus==='Demo No Sale')
    {
      if(this.keyContactPersonColor===''&&this.ownerPresentColor===''&&this.seriesOneWindowColor===''&&this.patioDoorsColor===''
        &&this.amountIfSoldColor===''&&this.seriesOneQuoteColor===''&&this.patioQuoteColor===''
        &&this.caQuoteColor===''&&this.projectDescriptionColor===''&&this.keyReasonColor===''&&this.mailColor===''&&this.reschedulePKColor===''&&this.futurePurchaseColor==='')//&&this.bayWQuoteColor===''&&this.bowWQuoteColor===''
        {
          var isValidatedSeriesTwo=(this.displaySeriesTwo==true?false:true);
          var isValidatedEntryDoor=(this.displayEntryDoor==true?false:true);
          var isValidatedSeriesA = (this.displaySeriesA == true ? false : true);
          var isValidatedFollowUpDT=false;
          if(this.displaySeriesTwo && this.seriesTwoWindowColor==='' && this.seriesTwoQuoteColor==='' )
          {
            isValidatedSeriesTwo=true;
          }
          if (this.displaySeriesA && this.seriesAQuoteColor === '') 
          {
            isValidatedSeriesA = true;
          }
          if(this.displayEntryDoor && this.entryDoorsColor==='' && this.entryQuoteColor==='')
          {
            isValidatedEntryDoor=true;
          }
          if(this.reschedule==='Yes'&&this.followUpDateTimeColor==='')
          {
            isValidatedFollowUpDT=true
          }
          if(this.reschedule==='No')
          {
            isValidatedFollowUpDT=true
          }
        if (isValidatedSeriesTwo && isValidatedFollowUpDT && isValidatedEntryDoor && isValidatedSeriesA)
          {
            return true;
          }
        }
    }
    if(this.resultStatus==='Sale')
    {
        if(this.keyContactPersonColor===''&&this.ownerPresentColor===''&&this.seriesOneWindowColor===''&&this.patioDoorsColor===''&&this.amountIfSoldColor===''
        &&this.seriesOneQuoteColor===''&&this.patioQuoteColor===''&&this.partialSaleColor===''&&this.caQuoteColor===''&&this.bayWQuoteColor===''&&this.bowWQuoteColor===''
        &&this.historicalAreaColor===''&&this.contructionColor===''&&this.customerColor===''&&this.concernsColor===''&&this.addressConcernsColor===''&&this.mailColor===''&&this.paymentTypeColor===''&&this.depositTypeColor===''
        &&this.depositColor==='') 
        {
          var isValidatedSeriesTwo=(this.displaySeriesTwo==true?false:true);
          var isValidatedEntryDoor=(this.displayEntryDoor==true?false:true);
          if(this.displaySeriesTwo && this.seriesTwoWindowColor==='' && this.seriesTwoQuoteColor==='' )
          {
            isValidatedSeriesTwo=true;
          }
          if(this.displayEntryDoor && this.entryDoorsColor==='' && this.entryQuoteColor==='')
          {
            isValidatedEntryDoor=true;
          }
          if((this.displaySeriesTwo || this.displayEntryDoor) && (!isValidatedSeriesTwo || !isValidatedEntryDoor))
          {
            return false;
          }
           
          if(this.paymentType === 'Finance'&&this.financeCompanyColor===''&&this.financePlanColor===''&&this.accountNumberColor===''&&this.approvedColor==='')
          {
            return true;
          }
          if(this.paymentType === 'Cash & Finance'&&this.cashFieldColor===''&&this.financeCompanyColor===''&&this.financePlanColor===''&&this.accountNumberColor===''&&this.approvedColor==='')
          {
            return true;
          }
          if(this.paymentType === 'Finance Turn-Down'&&this.financeTurnDownColor===''&&this.renewalColor===''&&this.financeIssueColor==='')
          {
            return true;
          }
          if(this.paymentType === 'Cash')
          {
            return true;
          }
        }
    }
    
    return false;
  }
	get keyContactPersonColor()
	{
    console.log("______this.keyContactPerson__________"+JSON.stringify(this.keyContactPerson));
		return this.keyContactPerson===undefined||this.keyContactPerson.length===0?'changeColorRed':'';
	}
	get commentsColor()
	{
		return this.Comments===undefined||this.Comments.length===0?'changeColorRed':'';
	}
	get projectDescriptionColor()
	{
		return this.ProjectDescription===undefined||this.ProjectDescription.length===0?'changeColorRed':'';
	}	
	get whyNoDemoColor()
	{
		return this.whyNoDemo===undefined||this.whyNoDemo.length===0?'changeColorRed':'';
	}
	get reschedulePKColor()
	{
		return this.reschedule===undefined||this.reschedule.length===0?'changeColorRed':'';
	}

	followUpDateTimeValue(event)
	{
		this.followUpDateTime=event.target.value;
	}
	get followUpDateTimeColor()
	{
		return this.followUpDateTime===undefined||this.followUpDateTime.length===0?'changeColorRed':'';
	}

	mailValue(event)
	{
		this.mail=event.target.value;
	}
	get mailColor()
	{
		return this.mail===undefined||this.mail.length===0?'changeColorRed':'';
	}
	cashFieldValue(event)
	{
		this.cashField=event.target.value;
	}
	get cashFieldColor()
	{
		return this.cashField===undefined||this.cashField.length===0?'changeColorRed':'';	
	}

	ownerPresentValue(event)
	{
		this.ownerPresent=event.target.value;
	}
	get ownerPresentColor()
	{
		return this.ownerPresent===undefined||this.ownerPresent.length===0?'changeColorRed':'';
	}

	seriesOneWindowValue(event)
	{
		this.seriesOneWindow=event.target.value;
	}
	get seriesOneWindowColor()
	{
		return this.seriesOneWindow===undefined||this.seriesOneWindow.length===0?'changeColorRed':'';
	}

	seriesTwoWindowValue(event)
	{
		this.seriesTwoWindow=event.target.value;
	}
	get seriesTwoWindowColor()
	{
		return this.seriesTwoWindow===undefined||this.seriesTwoWindow.length===0?'changeColorRed':'';
	}

	patioDoorsValue(event)
	{
		this.patioDoors=event.target.value;
	}
	get patioDoorsColor()
	{
		return this.patioDoors===undefined||this.patioDoors.length===0?'changeColorRed':'';
	}

	entryDoorsValue(event)
	{
		this.entryDoors=event.target.value;
	}
	get entryDoorsColor()
	{
		return this.entryDoors===undefined||this.entryDoors.length===0?'changeColorRed':'';
	}

	amountIfSoldValue(event)
	{
		this.amountIfSold=event.target.value;
	}
	get amountIfSoldColor()
	{
		return this.amountIfSold===undefined||this.amountIfSold.length===0?'changeColorRed':'';
	}

	seriesOneQuoteValue(event)
	{
		this.seriesOneQuote=event.target.value;
	}
	get seriesOneQuoteColor()
	{
		return this.seriesOneQuote===undefined||this.seriesOneQuote.length===0?'changeColorRed':'';
	}

	seriesTwoQuoteValue(event)
	{
		this.seriesTwoQuote=event.target.value;
	}
	get seriesTwoQuoteColor()
	{
		return this.seriesTwoQuote===undefined||this.seriesTwoQuote.length===0?'changeColorRed':'';
	}
  seriesAQuoteValue(event) {
    this.seriesAQuote = event.target.value;
  }
  get seriesAQuoteColor() {
    return this.seriesAQuote === undefined || this.seriesAQuote.length === 0 ? 'changeColorRed' : '';
  }
	patioQuoteValue(event)
	{
		this.patioQuote=event.target.value;
	}
	get patioQuoteColor()
	{
		return this.patioQuote===undefined||this.patioQuote.length===0?'changeColorRed':'';
	}
	
	entryQuoteValue(event)
	{
		this.entryQuote=event.target.value;
	}
	get entryQuoteColor()
	{
		return this.entryQuote===undefined||this.entryQuote.length===0?'changeColorRed':'';
	}

	caQuoteValue(event)
	{
		this.caQuote=event.target.value;
	}
	get caQuoteColor()
	{
		return this.caQuote===undefined||this.caQuote.length===0?'changeColorRed':'';
	}

	bayWQuoteValue(event)
	{
		this.bayWQuote=event.target.value;
	}
	get bayWQuoteColor()
	{
		return this.bayWQuote===undefined||this.bayWQuote.length===0?'changeColorRed':'';
	}

	bowWQuoteValue(event)
	{
		this.bowWQuote=event.target.value;
	}
	get bowWQuoteColor()
	{
		return this.bowWQuote===undefined||this.bowWQuote.length===0?'changeColorRed':'';
	}

	keyReasonValue(event)
	{
		this.keyReason=event.target.value;
	}
	get keyReasonColor()
	{
		return this.keyReason===undefined||this.keyReason.length===0?'changeColorRed':'';
	}

	futurePurchaseValue(event)
	{
		this.futurePurchase=event.target.value;
	}
	get futurePurchaseColor()
	{
		return this.futurePurchase===undefined||this.futurePurchase.length===0?'changeColorRed':'';
	}

	get paymentTypeColor()
	{
		return this.paymentType===undefined||this.paymentType.length===0?'changeColorRed':'';
	}

	depositTypeValue(event)
	{
		this.depositType=event.target.value;
	}
	get depositTypeColor()
	{
		return this.depositType===undefined||this.depositType.length===0?'changeColorRed':'';
	}

	depositValue(event)
	{
		this.deposit=event.target.value;
	}
	get depositColor()
	{
		return this.deposit===undefined||this.deposit.length===0?'changeColorRed':'';
	}

	constructionValue(event)
	{
		this.construction=event.target.value;
	}
	get contructionColor()
	{
		return this.construction===undefined||this.construction.length===0?'changeColorRed':'';
	}

	customerValue(event)
	{
		this.customer=event.target.value;
	}
	get customerColor()
	{
		return this.customer===undefined||this.customer.length===0?'changeColorRed':'';
	}

	concernsValue(event)
	{
		this.concerns=event.target.value;
	}
	get concernsColor()
	{
		return this.concerns===undefined||this.concerns.length===0?'changeColorRed':'';
	}

	addressConcernsValue(event)
	{
		this.addressConcerns=event.target.value;
	}
	get addressConcernsColor()
	{
		return this.addressConcerns===undefined||this.addressConcerns.length===0?'changeColorRed':'';
	}

	partialSaleValue(event)
	{
		this.partialSale=event.target.value;
	}
	get partialSaleColor()
	{
		return this.partialSale===undefined||this.partialSale.length===0?'changeColorRed':'';
	}

	historicalAreaValue(event)
	{
		this.historicalArea=event.target.value;
	}
	get historicalAreaColor()
	{
		return this.historicalArea===undefined||this.historicalArea.length===0?'changeColorRed':'';
	}

	get financeCompanyColor()
	{
		return this.selectedFinanceCompany===undefined||this.selectedFinanceCompany.length===0?'changeColorRed':'';
	}

	get financePlanColor()
	{
		return this.selectedFinanceProgram===undefined||this.selectedFinanceProgram.length===0?'changeColorRed':'';
	}

	accountNumberValue(event)
	{
		this.accountNumber=event.target.value;
	}
	get accountNumberColor()
	{
		return this.accountNumber===undefined||this.accountNumber.length===0?'changeColorRed':'';
	}

	approvedValue(event)
	{
		this.approved=event.target.value;
	}
	get approvedColor()
	{
		return this.approved===undefined||this.approved.length===0?'changeColorRed':'';
	}

	get financeTurnDownColor()
	{
		return this.selectedTurnedDownBy===undefined||this.selectedTurnedDownBy.length===0?'changeColorRed':''
	}

	renewalValue(event)
	{
		this.renewal=event.target.value;
	}
	get renewalColor()
	{
		return this.renewal===undefined||this.renewal.length===0?'changeColorRed':'';
	}

	get financeIssueColor()
	{
		return this.financeIssues===undefined||this.financeIssues.length===0?'changeColorRed':'';
	}
	
}