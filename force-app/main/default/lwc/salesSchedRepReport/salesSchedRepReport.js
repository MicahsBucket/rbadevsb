import { LightningElement, track, api } from 'lwc';
import assign from '@salesforce/apex/SalesSchedStoreManagerCtrl.assign';
import TIMEZONE from "@salesforce/i18n/timeZone"; //SS-247

const initialcols = [
  {
    label: 'Sales Rep', type: 'url', cellAttributes: { class: { fieldName: 'newrep' } }, typeAttributes: {value: { fieldName: 'url' }, label: { fieldName: 'rep' }, target: '_blank'}, fieldName: 'url', sortable: true},
    { type: "button", initialWidth: 75, cellAttributes: {class: { fieldName: 'newrep'}}, typeAttributes: { label: "All", name: "All", title: "All", variant: { fieldName: "Neutral" } } },   /*SS-167*/
    { label: '14-Day Close Rate', cellAttributes: {class: { fieldName: 'newrep'}, alignment: 'left'}, fieldName: 'closerate', type: 'percent', sortable: true },
    { label: 'Orders / Issued', cellAttributes: {class: { fieldName: 'newrep'}, alignment: 'left'}, fieldName: 'ordersissued', sortable: true},
    { label: 'RPA 28-day', cellAttributes: {class: { fieldName: 'newrep'}, alignment: 'left'}, fieldName: 'rpa28', type: 'currency', sortable: true},
    { label: 'Last 6-Mth RPA', cellAttributes: {class: { fieldName: 'newrep'}, alignment: 'left'}, fieldName: 'rpa6', type: 'currency', sortable: true }
    //{ label: 'Utilization', cellAttributes: {class: { fieldName: 'newrep'}}, fieldName: 'utilization', type: 'number', sortable: true, typeAttributes: {class: 'stuff'}}
];

const slotlabels = {
  cellAttributes: {class: { fieldName: 'newrep'}},
    all: 'ALL',
    am : 'AM',
    mid : 'MID',
    pm : 'PM',
    earlyam : 'EARLY AM',
    earlypm : 'EARLY PM',
    lateam : 'LATE AM',
    latepm : 'LATE PM'
};

const detailcols = [
    { label: 'Date', fieldName: 'date', type: 'date'},
    { label: 'Result', fieldName: 'result'},
    { label: 'Done By', fieldName: 'doneby'}
];

export default class SalesSchedRepReport extends LightningElement {
    @api report;
    @api slots;
    //@api appcapcount;
    @api appointments = false;
    @api salesrepoptions;
    @track columns;
    @track viewappointmenterror;
    @track appwrapper;
    @track showlanding = false;
    @track selectedSlot;
    @track showridealong = false;
    @track appointmentId = false;
    @track slotname;
    @track prepopulatedRepName;
    @track prepopulatedRepId;
    @track sortedby;
    @track sorteddirection;
    @track capacityId;
    @track showAssignModal = false;
    @track AppointmentIdList = false; // SS-167
    @track showAssignModalAll = false;
    @track Header;
    //@track repUnavailable = false;

    connectedCallback() {
        let columnTemp = [...initialcols];
        //let apptcapcount = this.appcapcount;
        //let acount = apptcapcount.reduce((acc, cur) => ({ ...acc, [cur.key]: cur.value }), {});
        this.slots.forEach(function(slot){
            let slotLabel = slotlabels[slot];
            columnTemp.push({
                cellAttributes: {class: { fieldName: 'newrep'}},
                label: slotLabel,
                fieldName: slot,
                sortable: true,
                type: 'salesSchedAssignColumn',
                initialWidth: 75,
                typeAttributes : {
                    buttonLabel : {fieldName : slot},
                    appointmentSlot : slot,
                    appointmentId : {fieldName : slot + 'AppointmentId'},
                    buttonClass : {fieldName: slot + 'Variant'},
                    repId : {fieldName: 'id'},
                    aptStatus: { fieldName: slot + 'AptStatus' },
                    isRideAlong: { fieldName: slot + 'IsRideAlong' }, //SS19-405
                    salesCapCount: {fieldName: slot + 'SalesCapCount'},
                    ApptIdList: { fieldName: slot + "ApptmentList" }, //SS262
                    mouseHover: {fieldName: slot + 'hover'}
                }
            });
        });
        columnTemp.push({ label: 'Accepted',cellAttributes: { iconName: { fieldName: 'acceptedIcon' }, class: { fieldName: 'newrep'}, iconPosition: 'right' }});
        this.columns = columnTemp;
        this.appointmentId = false;
        this.sorteddirection = 'desc';
        this.sortedby = 'closerate';
        //this.sortReport('closerate');
    }


    handleUpdateSort(event){
        this.sortReport(event.detail.fieldName);
    }

    sortReport(fieldName){
        if(fieldName === this.sortedby){
            this.sorteddirection = (this.sorteddirection === 'asc' ? 'desc' : 'asc');
        }else{
            this.sorteddirection = 'desc';
        }
        this.sortedby = fieldName;
        let multiplier = (this.sorteddirection === 'desc' ? -1 : 1);

        let reportSorted;
        if(fieldName === 'closerate'){
            //First sort by rpa6, then rpa28 for any tie breakers.
            reportSorted = [...this.report].sort((a,b) => multiplier * (a.rpa6 - b.rpa6));
            reportSorted = reportSorted.sort((a,b) => multiplier * (a.rpa28 - b.rpa28));
            reportSorted = reportSorted.sort((a,b) => multiplier * (a.closerate - b.closerate));
        }else if(fieldName === 'ordersissued'){
            reportSorted = [...this.report].sort((a,b) => multiplier * ((a.orders * a.closerate) - (b.orders * b.closerate)));
        }else{
            reportSorted = [...this.report].sort((a,b) => multiplier * (a[fieldName] - b[fieldName]));
        }
        this.report = reportSorted;
    }

    handleSlotClick(event){
        this.slotid = event.details.dataset.slotid;
    }

    handleAssignModalClose(event){
        this.appointmentId = false;
    }

    handleRowAction(event){

         let action = event.detail.action;
        let row = event.detail.row;
        if(action.name.startsWith('appointment_')){
            let slot = action.name.replace('appointment_','');
            let appointmentIdKey = slot + 'AppointmentId';
            let capacityIdKey = slot + 'CapacityId';
            this.appointmentId = row[appointmentIdKey];
            this.capacityId = row[capacityIdKey];
            this.prepopulatedRepId = row.id;
            this.prepopulatedRepName = row.rep;
            if(this.appointments && !this.appointmentId && slot){
                this.slotname = slot;
            }
            if(this.appointmentId){
                this.showAssignModal = true;
            }
        }
   /* Start SS-262*/
   debugger;
 
   if (event.detail.action.name === "All") {
     let tempList = [];
     Object.keys(slotlabels).forEach(slotLabel => {
       if (event.detail.row[slotLabel + "ApptmentList"]) {
         let IdArray = JSON.parse(
           JSON.stringify(event.detail.row[slotLabel + "ApptmentList"])
         );
         tempList.push(...IdArray);
       }
     });
     this.Header = event.detail.row.rep;
     this.AppointmentIdList = tempList;
     if (this.AppointmentIdList) {
       if (this.AppointmentIdList.length > 0) this.showAssignModalAll = true;
     }
   }
   /* End SS-262*/
    }
    
    handleShowAssignmentAction(event){
        let reports = this.report;
      console.log("_____ratna statment___reports____"+JSON.stringify(reports));
          if(reports){
            let row;
            reports.forEach(rec => {
                if(rec.id === event.detail.repId){
                    row = rec;
                }
            });

            if(row){
                let slot = event.detail.appointmentSlot;
                let capacityIdKey = slot + 'CapacityId';
                this.appointmentId = event.detail.appointmentId;
                this.capacityId = row[capacityIdKey];
                this.prepopulatedRepId = row.id;
                this.prepopulatedRepName = row.rep;
                if(this.appointments && !this.appointmentId && slot){
                    this.slotname = slot;
                }
                if(this.appointmentId){
                    this.showAssignModal = true;
                    window.setTimeout(() => {
                        this.template.querySelector("[data-id=modalBox]").getAvailableCapacity();
                      }, 100);
                }
            }
           
        }
        
    }

    handleShowAssignment(event){
        this.appointmentId = event.detail.value;
        if(this.appointmentId){
            this.showAssignModal = true;
        }
        this.slotname = false;
    }

    handleAppointmentModalClose(event){
        this.slotname = false;
    }

    get appointmentsForSlot() {

      if(this.slotname == 'earlyam'){
        this.slotname = 'early am';
      }else if(this.slotname == 'lateam'){
        this.slotname = 'late am';
      }else if(this.slotname == 'earlypm'){
        this.slotname = 'early pm';
      }else if(this.slotname == 'latepm'){
        this.slotname = 'late pm';
      }else {
        this.slotname = this.slotname;
      }
        if (this.appointments && this.slotname) {
      let appointments = this.appointments;
      appointments = appointments.filter(
        appointment =>
          appointment.appntment.Slot__r.Name.toLowerCase() === this.slotname && appointment.primaryCont
      ); //Grab only the appointments that are in the selected time slot and have a primary contact
      //appointments = appointments.map(appointment => ({ label: appointment.appntment.Sales_Order__r.Opportunity__r.Account.Name, appointment.appntment.Sales_Order__r.Opportunity__r.Units__c, value: appointment.appntment.Id}));
      //SS-247 Start
      appointments = appointments.map(appointment => ({
        label:
          appointment.formattedTime +
          " - " +
          appointment.primarycontact.Name +
          " - " +
          appointment.appntment.Sales_Order__r.Opportunity__r.Account
            .ShippingCity +
          ", " +
          appointment.appntment.Sales_Order__r.Opportunity__r.Account
            .ShippingStateCode +
          " " +
          appointment.appntment.Sales_Order__r.Opportunity__r.Account
            .ShippingPostalCode.slice(0,5) +
          " - " +
          (appointment.appntment.Sales_Order__r.Opportunity__r.of_Windows__c
            ? appointment.appntment.Sales_Order__r.Opportunity__r.of_Windows__c
            : "0") +
          " Windows, " +
          (appointment.appntment.Sales_Order__r.Opportunity__r.Num_of_Doors__c
            ? appointment.appntment.Sales_Order__r.Opportunity__r
                .Num_of_Doors__c
            : "0") +
          " Doors",
      value: appointment.appntment.Id
      }));
      //SS-247 end
      return appointments;
    }
  }

    refresh(){
        this.dispatchEvent(new CustomEvent('refresh'));
    }

    reset(){
      this.showAssignModalAll = false;
        this.showAssignModal = false;
        this.appointmentId = false;
        this.AppointmentIdList = false; //SS-167
    }

    get displayReport(){     
        return this.report && this.columns;
    }
}