import {wire, track} from 'lwc';
import { Calendar } from 'c/calendar';
import getMonth from '@salesforce/apex/SalesSchedCapacityCalendarCtrl.getMonth';
import getWeek from '@salesforce/apex/SalesSchedCapacityCalendarCtrl.getWeek';
import addCapacity from '@salesforce/apex/SalesSchedCapacityCalendarCtrl.addCapacity';
import { refreshApex } from '@salesforce/apex';

export default class SalesSchedCapacityCalendar extends Calendar{

    @track error;
    //@track day;
    @track store = 'All';
    @track storeLabel = 'All';
    @track showStoreOptions;
    @track week;

    constructor(){
        super();
        this.view = 'week';
        //this.day = this.startDay;
    }

    @wire(getWeek,{day : '$startDay', store : '$store'})
    week;

    @wire(getMonth, {day : '$startDay', store : '$store'})
    month;

    handleAddCapacity(event){
        let dataset = event.currentTarget.dataset;
        addCapacity({
            userId : dataset.userId,
            slotId : dataset.slotId,
            storeId : this.store,
            dateString : dataset.date
        }).then(result => {
            refreshApex(this.week);
        })
        .catch(error => {
            this.error = error;
        });
    }

    handleDisplayMonthView(){
        this.updateView('month');
    }

    handleDisplayWeekView(){
        this.updateView('week', this.startDay.clone().startOf('month'));
    }

    handleEditWeek(event){
        this.updateView('week', event.currentTarget.dataset.day);
    }

    handleShowStoreOptions() {
        this.showStoreOptions = true;
    }

    handleStoreChange(event) {
        this.showStoreOptions = false;
        let val = event.detail.value;
        this.store = val;
        let storeLabel;
        console.log("_____this.wiredData____"+JSON.stringify(this.wiredData));
        this.wiredData.data.storeOptions.forEach(function(store) {
            if(val === store.value){
                storeLabel = store.label;
            }
        });
        this.storeLabel = storeLabel;
    }

    updateView(view, startDay){
        if(startDay){
            this.startDay = moment(startDay,'YYYY-MM-DD');
            this.currentMoment = this.startDay.startOf(view);
        }
        this.view = view;
        super.initializePeriods();
    }

    get displayEditWeekButtons(){
        return this.store !== 'All';
    }

    get wiredData(){
        return this.isMonthView ? this.month : this.week;
    }
}