/**
 * @File Name          : createOrderShareCode.js
 * @Description        : 
 * @Author             : saisandeep.chanamolu@andersencorp.com
 * @Group              : 
 * @Last Modified By   : saisandeep.chanamolu@andersencorp.com
 * @Last Modified On   : 03-10-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0        3/10/2021   saisandeep.chanamolu@andersencorp.com    Initial Version
**/
import nonApplicableLiftPullsForCoastal from '@salesforce/label/c.NonApplicableLiftPullsForCoastal'
import costalPerfrmCategory from '@salesforce/label/c.CoastalPerformanceCategory';
import productCodesForLiftPulls from '@salesforce/label/c.ProductCodesForLiftPulls';
import grilleStylesForWideBar from '@salesforce/label/c.GrilleStylesForWideBar';
const customlabels ={productCodesForLiftPulls,nonApplicableLiftPullsForCoastal,costalPerfrmCategory,grilleStylesForWideBar};
const FIELDS = [
    'Product2.RecordTypeId',
    'Product2.Family',
    'Product2.IsActive',
    'Product2.Name',
    'Product2.Master_Product__c',
    'Product2.Pivotal_Id__c',
    'Product2.Run_Makability__c',
    'Product2.Description',
    'Product2.Unit_Short_Description__c',
    'Product2.Vendor__r.Name'
];
const sillAngleZeroThruFourteenList=[
    {label:'--Select--',value:'--Select--'},
    {label:'0',value:'0'},
    {label:'1',value:'1'},
    {label:'2',value:'2'},
    {label:'3',value:'3'},
    {label:'4',value:'4'},
    {label:'5',value:'5'},
    {label:'6',value:'6'},
    {label:'7',value:'7'},
    {label:'8',value:'8'},
    {label:'9',value:'9'},
    {label:'10',value:'10'},
    {label:'11',value:'11'},
    {label:'12',value:'12'},
    {label:'13',value:'13'},
    {label:'14',value:'14'}
];

// single leg shapes, used as part of a custom validation rule - 
// The shapes below will need to have either the left or right leg inches/fractions fields filled out. 
// otherewise the form will fail validation. it will also fail if one of the shapes below has both filled out.
const singleLegShapes =[
    'Quarter Circle',
    'Half Springline',
    'Right Triangle'
];

// Sill Angle lists for setting sill angle field based on frame type.
const sillAngleFsOnlyList=[
    {label:'--Select--',value:'--Select--'},
    {label:'FS',value:'FS'}
];

const pivotalIdsForMakability = [
    'MR2019'
];

const handleLiftPulls = (perfCategory, productCode,liftPullsClone,liftPullsField) => {
    let liftsPullsList =liftPullsClone;
    console.log('The perfperfCategory: '+perfCategory);
    console.log('The productCode: '+productCode);
    
        console.log('The customlabels: '+customlabels.nonApplicableLiftPullsForCoastal);
            if(perfCategory && perfCategory.value==customlabels.costalPerfrmCategory) {
                let applicableProductCodes= customlabels.productCodesForLiftPulls.split(",");
                if(productCode && applicableProductCodes.indexOf(productCode) !=-1) {
                    liftsPullsList= liftPullsClone;
                for( var i = 0; i < liftsPullsList.length; i++){
                    if ((customlabels.nonApplicableLiftPullsForCoastal.split(",")).indexOf(liftsPullsList[i].value)!=-1) {
                        if(liftPullsField && liftPullsField.value == liftsPullsList[i].value) {
                           
                            liftPullsField.value ='';
                        }
                        liftsPullsList.splice(i, 1); i--; 
                    }
                }

                }

            }
    console.log('The liftsPullsList value: '+liftsPullsList);
    return liftsPullsList;
}; 

const handleWideBar = (perfCategory,grilleStyle,wideBar) =>{
       console.log('Perfcategory: '+perfCategory.value);
       console.log('girllestyle: '+grilleStyle.value);
       console.log('girllestyle disabled: '+grilleStyle.disabled);
       console.log('widerBarValue: '+wideBar.value);
       let currentFields = document.querySelectorAll( 'lightning-input, lightning-combobox, lightning-input-field ');
            console.log('The currentFieldsValue in sharecode: ',currentFields);
       if(perfCategory && grilleStyle && wideBar) {
           if(perfCategory.value==customlabels.costalPerfrmCategory && grilleStyle.value==customlabels.grilleStylesForWideBar) {
            wideBar.checked=false;
            wideBar.disabled=true;
           }else if(grilleStyle.disabled==true) {
             wideBar.disabled=true;
           }else {
            wideBar.disabled=false;
           }
          
       }
};

const handleGlazingValues = (glazings1,glazings3,glazings2,glazingList) =>{
    console.log('The this.glazingList values in share code: ',glazingList);
    if(glazings1 && glazings1.value) {
        let valueFound = false;
        glazingList.forEach(element => {
            if(glazings1.value == element.value) {
                valueFound = true;
            }
            console.log('Element value: '+element.value);
        });

        if(!valueFound) {
            glazings1.value ='' ;
        }  
    }

    if(glazings3 && glazings3.value) {
        let valueFound = false;
        glazingList.forEach(element => {
            if(glazings3.value == element.value) {
                valueFound = true;
            }
            
        });

        if(!valueFound) {
            glazings3.value ='' ;
           // let glassPatternS3 = this.template.querySelector('lightning-combobox.Glass_Pattern_S3__c');
          //  if(glassPatternS3)
          //  glassPatternS3.value='';
        }  
    }

    if(glazings2 && glazings2.value) {
        let valueFound = false;
        glazingList.forEach(element => {
            if(glazings2.value == element.value) {
                valueFound = true;
            }
            
        });

        if(!valueFound) {
            glazings2.value ='' ;
        }  
    }
};

export function invokeSpecialOptions(currentFields,interiorSash,hardwareColor,screenColor,interiorGrilleColor,exteriorGrilleColor,exteriorTrimColor,screenColorDefault) {
    console.log('The currentFieldsValue: ',currentFields);
    let interiorColorValue;
    let exteriorColorValue;
    let grilleStyleValue;
    let hasInteriorSashColor;
    let hasHardwareColor;
    let hasScreenColor;
    let hasInteriorGrilleColor;
    let hasExteriorGrilleColor;
    let hasExteriorTrimColor;
    for(let field of currentFields){
        switch(field.name){
            case "Interior_Color__c" : {
                interiorColorValue = field.value;
                break;
            }
            case "Exterior_Color__c" : {
                exteriorColorValue = field.value;
                break;
            }
            case "Grille_Style__c" : {
                grilleStyleValue = field.value;
                break;
            }
            case "Interior_Sash_Color__c" : {
                hasInteriorSashColor = true;
                break;
            }
            case "Hardware_Color__c" : {
                hasHardwareColor = true;
                break;
            }
            case "Screen_Color__c" : {
                hasScreenColor = true;
                break;
            }
            case "Exterior_Grille_Color__c" : {
                hasExteriorGrilleColor = true;
                break;
            }
            case "Interior_Grille_Color__c" : {
                hasInteriorGrilleColor = true;
                break;
            }
            case "Exterior_Trim_Color__c" : {
                hasExteriorTrimColor = true;
                break;
            }
            default: {
                break;
            }
        }
    }
    if(hasInteriorSashColor){
       setInteriorSashColor(interiorColorValue,interiorSash);
    }
   if(hasHardwareColor){
       console.log('HardwareColorinShareCode: ',hardwareColor);
        setHardwareColor(interiorColorValue,hardwareColor);
    }
    if(hasScreenColor){
        setScreenColor(exteriorColorValue, interiorColorValue,screenColor,screenColorDefault);
    }
   if(hasExteriorGrilleColor && hasInteriorGrilleColor){
        setGrilleColors(exteriorColorValue, interiorColorValue, grilleStyleValue,interiorGrilleColor,exteriorGrilleColor);
    }
    if(hasExteriorTrimColor){
        setExteriorTrimColor(exteriorColorValue,exteriorTrimColor);
    }
}
    

function setInteriorSashColor (inColor,interiorSash) {
    console.log('hit setInteriorSashC + in-color ',inColor );
    interiorSash.value = inColor;
}

function setHardwareColor(inColor,hardwareColor) {
    console.log('hit setHardwareColor  + in-color ',inColor );
    //let hardwareColor = this.template.querySelector('lightning-combobox.Hardware_Color__c');
    switch(inColor){
        case "White" : {
            hardwareColor.value = inColor;
            break;
        }
        case "Canvas" : {
            hardwareColor.value  = inColor;
            break;
        }
        case "Sandtone" : {
            hardwareColor.value  = 'Stone';
            break;
        }
        case "Terratone" : {
            hardwareColor.value  = 'Stone';
            break;
        }
        case "Pine" : {
            hardwareColor.value  = 'Stone';
            break;
        }
        case "Oak" : {
            hardwareColor.value  = 'Stone';
            break;
        }
        case "Maple" : {
            hardwareColor.value  = 'Stone';
            break;
        }
        case "Dark Bronze" : {
            hardwareColor.value  = inColor;
            break;
        }
        case "Black" : {
            hardwareColor.value  = inColor;
            break;
        }
        default: {
            break;
        }
    }                        
}

function setScreenColor(exColor, inColor,screenColor,screenColorDefault){
    console.log('hit setScreenColor ex-color + in-color ', exColor,' ',inColor );
    //let screenColor = this.template.querySelector('lightning-combobox.Screen_Color__c');
    let colorToMatch;
    if(screenColorDefault === 'Exterior Color'){
        colorToMatch = exColor;
    }
    if(screenColorDefault === 'Interior Color'){
        colorToMatch = inColor;
    }
    switch(colorToMatch){
        case "White" : {
            screenColor.value = colorToMatch;
            break;
        }
        case "Canvas" : {
            screenColor.value  = colorToMatch;
            break;
        }
        case "Sandtone" : {
            screenColor.value  = 'Sandtone';
            break;
        }
        case "Terratone" : {
            screenColor.value  = 'Terratone';
            break;
        }
        case "Pine" : {
            screenColor.value  = 'Stone';
            break;
        }
        case "Oak" : {
            screenColor.value  = 'Stone';
            break;
        }
        case "Maple" : {
            screenColor.value  = 'Stone';
            break;
        }
        case "Dark Bronze" : {
            screenColor.value  = colorToMatch;
            break;
        }
        case "Black" : {
            screenColor.value  = colorToMatch;
            break;
        }
        case "Forest Green" : {
            screenColor.value  = colorToMatch;
            break;
        }
        case "Red Rock" : {
            screenColor.value  = colorToMatch;
            break;
        }
        case "Cocoa Bean" : {
            screenColor.value  = colorToMatch;
            break;
        }
        default: {
            break;
        }
    }                      
}

function setGrilleColors(exColor, inColor, grilleStyle,interiorGrilleColor,exteriorGrilleColor){
    console.log('hit setGrilleColors ex-color + in-color + grille-style ', exColor,' ',inColor, ' ', grilleStyle );
    let grilleStyles =['Interior Wood Only (INTW)','Interior Wood and GBG (INTW + GBG)','Full Divided Light (FDL with spacer)','Simulated Divided Light (FDL w/o spacer)'];
  //  let interiorGrilleColor = this.template.querySelector('lightning-combobox.Interior_Grille_Color__c');
    //let exteriorGrilleColor = this.template.querySelector('lightning-combobox.Exterior_Grille_Color__c');
    if(grilleStyle === 'Grilles Between Glass (GBG)'){
        switch(inColor){
            case "White" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Canvas" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Sandtone" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Terratone" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Pine" : {
                interiorGrilleColor.value = 'Sandtone';
                break;
            }
            case "Oak" : {
                interiorGrilleColor.value = 'Sandtone';
                break;
            }
            case "Maple" : {
                interiorGrilleColor.value = 'Sandtone';
                break;
            }
            case "Dark Bronze" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Black" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Forest Green" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            default: {
                break;
            }
        }              
    }
    if(grilleStyles.includes(grilleStyle)){
        switch(inColor){
            case "White" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Canvas" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Sandtone" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Terratone" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Pine" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Oak" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Maple" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Dark Bronze" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Black" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            case "Forest Green" : {
                interiorGrilleColor.value = inColor;
                break;
            }
            default: {
                break;
            }
        }    
    }
        //set exterior color - 
    if (grilleStyle) {
        switch (exColor) {
            case "White": {
                exteriorGrilleColor.value = exColor;
                break;
            }
            case "Canvas": {
                exteriorGrilleColor.value = exColor;
                break;
            }
            case "Sandtone": {
                exteriorGrilleColor.value = exColor;
                break;
            }
            case "Terratone": {
                exteriorGrilleColor.value = exColor;
                break;
            }
            case "Dark Bronze": {
                exteriorGrilleColor.value = exColor;
                break;
            }
            case "Forest Green": {
                exteriorGrilleColor.value = exColor;
                break;
            }
            case "Black": {
                exteriorGrilleColor.value = exColor;
                break;
            }
            case "Red Rock": {
                exteriorGrilleColor.value = exColor;
                break;
            }
            case "Cocoa Bean": {
                exteriorGrilleColor.value = exColor;
                break;
            }
            default: {
                break;
            }
        }
    }

}

function setExteriorTrimColor(exColor,exteriorTrimColor){
    console.log('hit setExteriorTrimColor ex-color ', exColor);
    //let exteriorTrimColor = this.template.querySelector('lightning-combobox.Exterior_Trim_Color__c');

    switch(exColor){
        case "White" : {
            exteriorTrimColor.value = exColor;
            break;
        }
        case "Canvas" : {
            exteriorTrimColor.value = exColor;
            break;
        }
        case "Sandtone" : {
            exteriorTrimColor.value = 'Stone';
            break;
        }
        case "Terratone" : {
            exteriorTrimColor.value = 'Stone';
            break;
        }
        case "Dark Bronze" : {
            exteriorTrimColor.value = exColor;
            break;
        }
        case "Black" : {
            exteriorTrimColor.value = exColor;
            break;
        }
        case "Red Rock" : {
            exteriorTrimColor.value = exColor;
            break;
        }
        case "Cocoa Bean" : {
            exteriorTrimColor.value = exColor;
            break;
        }
        case "Forest Green" : {
            exteriorTrimColor.value = exColor;
            break;
        }                                    
        default: {
            break;
        }
    }                            
}

const controlQuantityForMasterProduct = (isMasterProduct,quantity) =>{
    console.log('isMasterProduct: '+isMasterProduct);
    console.log('quantity: '+quantity);
    if(isMasterProduct && quantity) {
        quantity.value="1";
        quantity.disabled = true;
    }else if(!isMasterProduct && quantity) {
        quantity.disabled = false;
    }
    
    
};

export { handleLiftPulls,handleWideBar,handleGlazingValues,FIELDS,sillAngleZeroThruFourteenList,singleLegShapes,sillAngleFsOnlyList,pivotalIdsForMakability,controlQuantityForMasterProduct };