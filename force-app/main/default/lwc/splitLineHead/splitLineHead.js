import { LightningElement, api, track } from 'lwc';
import splitLines2 from '@salesforce/apex/DaylConfigurationOutputsCallout.splitLines2';
import getSplitLineMockData from '@salesforce/apex/SplitLineMockData.MockData';

export default class SplitLineHead extends LightningElement {
    @api recordId;
    @track splitLines;
    connectedCallback(){
        this.getSplitLines();
    }

    getSplitLines(){
        splitLines2({OrderItemId: this.recordId})
        .then((result) => {
            console.log('getSplitLines result', result.state);
            console.log('getSplitLines ', result);
            if(result.state == 'SUCCESS'){
                // alert('We have hit the Endpoint!');
                getSplitLineMockData({})
                .then((result) => {
                    console.log('Split Line Mock Data Response', result);
                    // this.openConfigData = false;
                    // this.showSplitLines = true;
                    this.splitLines = result;
                })
                .catch((error) => {
                    console.log('error in start config callback ', error);
                    
                });
            }
        })
        .catch((error) => {
            console.log('getConfigurationData Error', error);
        });
    }
}