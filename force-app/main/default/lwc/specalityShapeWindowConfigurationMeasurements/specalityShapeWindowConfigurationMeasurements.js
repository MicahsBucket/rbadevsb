import { LightningElement, track, api } from 'lwc';

export const SPECIALITY_SHAPE_CONFIGURATIONS = {
    equallegarch: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: true,
        leftLegFractions: true,
        rightLegInches: true,
        rightLegFractions: true,
        midPointInches: false,
        midPointFractions: false
    },
    pentagon: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: false,
        leftLegFractions: false,
        rightLegInches: false,
        rightLegFractions: false,
        midPointInches: false,
        midPointFractions: false
    },
    peakpentagon: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: true,
        leftLegFractions: true,
        rightLegInches: true,
        rightLegFractions: true,
        midPointInches: false,
        midPointFractions: false
    },
    hexagon: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: false,
        leftLegFractions: false,
        rightLegInches: false,
        rightLegFractions: false,
        midPointInches: false,
        midPointFractions: false
    },
    octagon: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: false,
        leftLegFractions: false,
        rightLegInches: false,
        rightLegFractions: false,
        midPointInches: false,
        midPointFractions: false
    },
    circle: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: false,
        leftLegFractions: false,
        rightLegInches: false,
        rightLegFractions: false,
        midPointInches: false,
        midPointFractions: false
    },
    circletop: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: false,
        leftLegFractions: false,
        rightLegInches: false,
        rightLegFractions: false,
        midPointInches: false,
        midPointFractions: false
    },
    chord: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: false,
        leftLegFractions: false,
        rightLegInches: false,
        rightLegFractions: false,
        midPointInches: false,
        midPointFractions: false
    },
    springline: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: false,
        leftLegFractions: false,
        rightLegInches: false,
        rightLegFractions: false,
        midPointInches: false,
        midPointFractions: false
    },
    quartercircle: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: true,
        leftLegFractions: true,
        rightLegInches: true,
        rightLegFractions: true,
        midPointInches: false,
        midPointFractions: false
    },
    righttriangle: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: false,
        leftLegFractions: false,
        rightLegInches: true,
        rightLegFractions: true,
        midPointInches: false,
        midPointFractions: false
    },
    trapezoid: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: true,
        leftLegFractions: true,
        rightLegInches: true,
        rightLegFractions: true,
        midPointInches: false,
        midPointFractions: false
    },
    unequallegarch: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: true,
        leftLegFractions: true,
        rightLegInches: true,
        rightLegFractions: true,
        midPointInches: true,
        midPointFractions: true
    },
    halfspringline: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: true,
        leftLegFractions: true,
        rightLegInches: true,
        rightLegFractions: true,
        midPointInches: false,
        midPointFractions: false
    },
    triangle: {
        widthInches: true,
        widthFractions: true,
        heightInches: true,
        heightFractions: true,
        leftLegInches: true,
        leftLegFractions: true,
        rightLegInches: true,
        rightLegFractions: true,
        midPointInches: false,
        midPointFractions: false
    }
};

export default class SpecalityShapeWindowConfigurationMeasurements extends LightningElement {
    @track
    specialityShapeConfigurations = SPECIALITY_SHAPE_CONFIGURATIONS;
    @api shapestring;
    @api productfieldcontrol;
    @api fractionlist;
    // @track isRightTriangle = false;
   // @track shape;

    renderedCallback() {
        console.log('shapestring renderedCallback: ', this.shapestring);
        const elements = this.template.querySelectorAll('lightning-input, lightning-combobox, lightning-input-field');
        debugger;
        //this.shape = this.getshapeConfig();
    }

    getshapeConfig() {
        return this.specialityShapeConfigurations[this.shapestring.split(' ').join('').toLowerCase()];
    }

    get isRightTriangle() {
        debugger;
        const shp = this.shapestring.split(' ').join('').toLowerCase();
        const val = 'rightTriangle'.toLowerCase() === shp ? true : false;
        console.log('isRightTriangle: ', val);
        return val;
    }

    get shape() {
        console.log('get sgape: ', this.shapestring);
        const shp = this.shapestring.split(' ').join('').toLowerCase();
        this.isRightTriangle = shp === 'rightTriangle'.toLowerCase() === shp ? true : false;
        console.log('shp: ', shp);
        let returnShape;
        switch (shp) {
            case 'hexagon'.toLowerCase():
                returnShape = this.hexagon;
                console.log(returnShape);
                return returnShape;
            case 'equalLegArch'.toLowerCase():
                returnShape = this.equalLegArch;
                console.log(returnShape);
                return returnShape;
            case 'pentagon'.toLowerCase():
                returnShape = this.pentagon;
                console.log(returnShape);
                return returnShape;
            case 'peakPentagon'.toLowerCase():
                returnShape = this.peakPentagon;
                console.log(returnShape);
                return returnShape;
            case 'octagon'.toLowerCase():
                returnShape = this.octagon;
                console.log(returnShape);
                return returnShape;
            case 'circle'.toLowerCase():
                returnShape = this.circle;
                console.log(returnShape);
                return returnShape;
            case 'circleTop'.toLowerCase():
                returnShape = this.circleTop;
                console.log(returnShape);
                return returnShape;
            case 'chord'.toLowerCase():
                returnShape = this.chord;
                console.log(returnShape);
                return returnShape;
            case 'springLine'.toLowerCase():
                returnShape = this.springLine;
                console.log(returnShape);
                return returnShape;
            case 'quarterCircle'.toLowerCase():
                returnShape = this.quarterCircle;
                console.log(returnShape);
                return returnShape;
            case 'rightTriangle'.toLowerCase():
                returnShape = this.rightTriangle;
                console.log(returnShape);
                return returnShape;
            case 'trapezoid'.toLowerCase():
                returnShape = this.trapezoid;
                console.log(returnShape);
                return returnShape;
            case 'unEqualLegArch'.toLowerCase():
                returnShape = this.unEqualLegArch;
                console.log(returnShape);
                return returnShape;
            case 'halfSpringLine'.toLowerCase():
                returnShape = this.halfSpringLine;
                console.log(returnShape);
                return returnShape;
            case 'triangle'.toLowerCase():
                returnShape = this.triangle;
                console.log(returnShape);
                return returnShape;
            default:
                break;
        }
    }

    get equalLegArch() {
        return this.getshapeConfig('equalLegArch');
    }

    get pentagon() {
        return this.getshapeConfig('pentagon');
    }

    get peakPentagon() {
        return this.getshapeConfig('peakPentagon');
    }

    get hexagon() {
        return this.getshapeConfig('hexagon');
    }

    get octagon() {
        return this.getshapeConfig('octagon');
    }

    get circle() {
        return this.getshapeConfig('circle');
    }

    get circleTop() {
        return this.getshapeConfig('circleTop');
    }

    get chord() {
        return this.getshapeConfig('chord');
    }

    get springLine() {
        return this.getshapeConfig('springLine');
    }

    get quarterCircle() {
        return this.getshapeConfig('quarterCircle');
    }

    get rightTriangle() {
        return this.getshapeConfig('rightTriangle');
    }

    get trapezoid() {
        return this.getshapeConfig('trapezoid');
    }

    get unEqualLegArch() {
        return this.getshapeConfig('unEqualLegArch');
    }

    get halfSpringLine() {
        return this.getshapeConfig('halfSpringLine');
    }

    get triangle() {
        return this.getshapeConfig('triangle');
    }

    haandlehange(event) {
        debugger;
        console.log('speciality event: ', event);
        const widthInches = this.template.querySelector('lightning-input[data-id="Width_Inches__c"]');
        const widthInchesTarget = event.target.data-id;
        const widthInchesDetail = event.detail.data-id;
        let name = event.target.name ? event.target.name : event.target.fieldName;
        let value = Array.isArray(event.detail.value) ? event.detail.value[0] : event.detail.value;
        console.log('child hit handle change for ', name);
        console.log('child hit handle change for value', value);
        console.log('widtInches: ', widtInches);
        console.log('widthInchesTarget: ', widthInchesTarget);
        console.log('widthInchesDetail: ', widthInchesDetail);

        const customEvnt = new CustomEvent('specialitychildchange', event);
        this.dispatchEvent(customEvnt);
    }
}