import { LightningElement, track,  wire, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getServiceWarranty from '@salesforce/apex/HomeCompLightningCardServiceAndWarranty.getServiceCompleted';
import getWarrantyRejected from '@salesforce/apex/HomeCompLightningCardServiceAndWarranty.getWarrantyRejected';

const ServiceReqColumns = [
    {label: 'Order Number', fieldName: 'ServiceReqURL', type: "url", typeAttributes: {label: { fieldName: 'ServiceReqNumber'}, value:{fieldName: 'ServiceReqURL'}, target: '_blank'}, sortable: true}, 
    {label: 'Bill To Contact', fieldName: 'CustomerURL', type: "url", typeAttributes: {label: { fieldName: 'CustomerName'}, value:{fieldName: 'CustomerURL'}, target: '_blank'}, sortable: true},
    {label: 'Account', fieldName: 'AccountURL', type: "url", typeAttributes: {label: { fieldName: 'AccountName'}, value:{fieldName: 'AccountURL'}, target: '_blank'}, sortable: true},
    {label: 'Amount Due', fieldName: 'AmountDue', type: 'currency', sortable: true},
];

const WarrantyRejectedColumns = [
        {label: 'Order Number', fieldName: 'ServiceReqURL', type: "url", typeAttributes: {label: { fieldName: 'ServiceReqNumber'}, value:{fieldName: 'ServiceReqURL'}, target: '_blank'}, sortable: true}, 
        {label: 'Bill To Contact', fieldName: 'CustomerURL', type: "url", typeAttributes: {label: { fieldName: 'CustomerName'}, value:{fieldName: 'CustomerURL'}, target: '_blank'}, sortable: true},
        {label: 'Warranty Rejected Date', fieldName: 'WarrantyRejectionDate', type: 'date', sortable: true},
        {label: 'Warranty Rejected Reason', fieldName: 'RejectionReason', type: 'text', sortable: true}
];

export default class HomeComponentLightningCardServiceWarranty extends  NavigationMixin (LightningElement) {

    @api recordId;

    @track ServiceReqColumns=ServiceReqColumns;
    @track listServiceWarranty;
    @track listServiceWarrantyDup;

    @track WarrantyRejectedColumns = WarrantyRejectedColumns;
    @track listWarrantyRejected;

    sVal = '';

    @wire(getServiceWarranty, {searchKey: ''})
    wiredGetServiceWarranty({ error, data }) {             
        if (data) {           
            this.listServiceWarranty =  data; 
            this.listServiceWarrantyDup = data;              
        }
        else if (error) {
            this.error = error;
        }     
            
        console.log('getServiceWarranty ' +JSON.stringify( this.listServiceWarranty));       
    }

    @wire(getWarrantyRejected, {searchKey: ''})
    wiredWarrantyRejectedMethod({ error, data }) {             
        if (data) {           
            this.listWarrantyRejected =  data;               
        }
        else if (error) {
            this.error = error;
        }     
        console.log('listWarrantyRejected ' +JSON.stringify(data));                
    }

    updateSeachKeySerComp(event) {
        this.sVal = event.target.value;
        this.handleSearchSerComp();
    }

    handleSearchSerComp() {
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getServiceWarranty({ searchKey: this.sVal })
            .then(result => {
                // set @track contacts variable with return contact list from server  
                this.listServiceWarranty = result;
            })
            .catch(error => {
                // display server exception in toast msg 
                const event = new ShowToastEvent({
                    title: 'Error',
                    variant: 'error',
                    message: error.body.message,
                });
                this.dispatchEvent(event);
                // reset contacts var with null   
                this.listServiceWarranty = null;
            });
        }
        else {
            this.listServiceWarranty = this.listServiceWarrantyDup;
        }
    }

    updateSeachKeyWarrantyRej(event) {
        this.sVal = event.target.value;
        this.handleSearchWarrantyRej();
    }

    handleSearchWarrantyRej() {
        
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getWarrantyRejected({ searchKey: this.sVal })
            .then(result => {
                // set @track contacts variable with return contact list from server  
                this.listWarrantyRejected = result;
            })
            .catch(error => {
                // display server exception in toast msg 
                const event = new ShowToastEvent({
                    title: 'Error',
                    variant: 'error',
                    message: error.body.message,
                });
                this.dispatchEvent(event);   
                this.listWarrantyRejected = null;
            });
        }
        else {
            // fire toast event if input field is blank
            const event = new ShowToastEvent({
                variant: 'error',
                message: 'Search text missing..',
            });
            this.dispatchEvent(event);
        }

    }

    serviceCompletedSortData(fieldName, sortDirection) {
        var data = JSON.parse(JSON.stringify(this.listServiceWarranty));
        
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        if(fieldName === 'AmountDue' ){
            data.sort((a,b) => {
                let valueA = key(a) ? key(a) : ''; 
                let valueB = key(b) ? key(b) : '';
                return reverse * ((valueA > valueB) - (valueB > valueA));
            });
        }
        else {
            data.sort((a,b) => {
                let valueA = key(a) ? key(a).toLowerCase() : ''; 
                let valueB = key(b) ? key(b).toLowerCase() : '';
                return reverse * ((valueA > valueB) - (valueB > valueA));
            });
        }            
        
        this.listServiceWarranty = data;

    }

    serviceCompletedUpdateColumnSorting(event) {
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.serviceCompletedSortData(this.sortedBy,this.sortedDirection);
    }

    warrantyRejectedSortData(fieldName, sortDirection) {
        var data = JSON.parse(JSON.stringify(this.listWarrantyRejected));
        
        var key =(a) => a[fieldName];
        var reverse = sortDirection === 'asc' ? 1: -1;
        data.sort((a,b) => {
            let valueA = key(a) ? key(a).toLowerCase() : '';
            let valueB = key(b) ? key(b).toLowerCase() : '';
            return reverse * ((valueA > valueB) - (valueB > valueA));
        });

        this.listWarrantyRejected = data;

    }

    warrantyRejectedUpdateColumnSorting(event) {
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.warrantyRejectedSortData(this.sortedBy,this.sortedDirection);
    }

}