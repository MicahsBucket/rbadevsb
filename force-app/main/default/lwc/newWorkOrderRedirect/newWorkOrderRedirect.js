/**
* @author Jason Flippen
* @date 01/29/2021 
* @description Provides functionality for the newWorkOrderRedirect LWC.
*
*			   Supporting database functionality provided by the following Apex Class:
*			   - NewWorkOrderRedirectController
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/
import { LightningElement, api, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getOrderData from '@salesforce/apex/NewWorkOrderRedirectController.getOrderData';
import getWorkOrderRecordTypeList from '@salesforce/apex/NewWorkOrderRedirectController.getWorkOrderRecordTypeList';
import createWorkOrder from '@salesforce/apex/NewWorkOrderRedirectController.createWorkOrder';

export default class NewWorkOrderRedirect extends NavigationMixin(LightningElement) {
    
    @api recordId;
    @api objectName;

    order;
    workOrderRecordTypeList = [];
    selectedWorkOrderRecordTypeId = '';
    isEligible = false;
    displayText = '';

    /**
    * @author Jason Flippen
    * @date 01/29/2021
    * @description Method to retrieve Order data.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @wire(getOrderData, { orderId: '$recordId' })
    wiredGetOrderData({ error, data }) {

        if (data) {

            console.log('Order Data', JSON.parse(JSON.stringify(data)));
            this.order = JSON.parse(JSON.stringify(data));

            if (this.order != null) {

                // Are we able to create a Work Order from this Order?
                if (this.order.Status == 'Draft') {
                    this.isEligible = false;
                    this.displayText = 'You cannot create Work Orders from an Order in Draft status';
                }
                else {
                    this.isEligible = true;
                    this.getWorkOrderRecordTypeList();
                }

            }
            else {
                this.isEligible = false;
                this.displayText = 'Unable to create Work Order';
            }

            const disableSaveEvent = new CustomEvent('DisableSave');
            this.dispatchEvent(disableSaveEvent);

        }
        else if (error) {
            console.log('error in getOrderData callback', error);
        }

    }

    /**
    * @author Jason Flippen
    * @date 01/29/2021
    * @description Method to List of Work Order Record Types available
    *              to be created for this type of Order.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    getWorkOrderRecordTypeList() {

        console.log('This Order', this.order);

        getWorkOrderRecordTypeList({ order: this.order })
        .then(result => {
            console.log('Work Order Type List', JSON.parse(JSON.stringify(result)));
            this.workOrderRecordTypeList = JSON.parse(JSON.stringify(result));
        })
        .catch(error => {
            console.log('Error in getWorkOrderRecordTypeList callback', error);
        })

    }

    /**
    * @author Jason Flippen
    * @date 01/29/2021
    * @description Method to create a new Work Order.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @api
    handleSaveClick() {

        const showSpinner = new CustomEvent('ShowSpinner');
        const hideSpinner = new CustomEvent('HideSpinner');
        const closeQuickAction = new CustomEvent('CloseQuickAction');

        this.dispatchEvent(showSpinner);

        // Call Apex method to update the Order.
        createWorkOrder({ order: this.order,
                          workOrderRecordTypeId: this.selectedWorkOrderRecordTypeId })
        .then((result) => {

            var resultMap = result;

            var returnValue = '';
            var saveSuccess = false;
            for (var key in resultMap) {
                returnValue = key;
                if (key === 'New Work Order Success') {
                    returnValue = resultMap[key];
                    saveSuccess = true;
                }
                break;
            }
            console.log('Return Value', returnValue);

            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(closeQuickAction);    

            if (saveSuccess === true) {

                // Navigate to the new Cost Purchase Order.
                this[NavigationMixin.Navigate]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: returnValue,
                        objectApiName: 'WorkOrder', // objectApiName is optional
                        actionName: 'view'
                    }
                });

            }
            else  {

                const toastMessage = returnValue;
                const toastType = 'error';
                const showToastEvent = new CustomEvent('ShowToast', { detail: { message: toastMessage, type: toastType } });
                this.dispatchEvent(showToastEvent);

            }

        })
        .catch((error) => {
            console.log('Create Work Order Error', error);
            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(closeQuickAction);
        });

    }

    /**
    * @author Jason Flippen
    * @date 01/29/2021
    * @description Method handle the "Change" event on the Work Order Record Type combobox. 
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    handleWorkOrderRecordTypeChange(event) {

        this.selectedWorkOrderRecordTypeId = event.detail.value;
        console.log('Selected Work Order Record Type Id', this.selectedWorkOrderRecordTypeId);

        if (this.selectedWorkOrderRecordTypeId === '') {
            const disableSaveEvent = new CustomEvent('DisableSave');
            this.dispatchEvent(disableSaveEvent);
        }
        else {
            const enableSaveEvent = new CustomEvent('EnableSave');
            this.dispatchEvent(enableSaveEvent);
        }
        
    }

}