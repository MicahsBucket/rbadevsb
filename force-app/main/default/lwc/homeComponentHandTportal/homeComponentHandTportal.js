import { LightningElement, api, track, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getRelatedFilesByRecordId from '@salesforce/apex/PDFViewerController.getRelatedFilesByRecordId';
import getReleaseDocNames from '@salesforce/apex/PDFViewerController.getReleaseDocNames';

const columns = [
    {label: 'Release Documents', fieldName: 'ReleaseDocURL', type: "url", typeAttributes: {label: { fieldName: 'ReleaseDocName'}, value:{fieldName: 'ReleaseDocURL'}, target: '_blank'}}, 
    // {label: 'Status', fieldName: 'ReleaseDocName', type: 'text'} 
]; 

export default class HomeComponentHandTportal extends  NavigationMixin (LightningElement) {

    @api recordId;
    @api heightInRem;
    @api mySupportCasesPageName;
    @api renewUUrl;
    
    @track fileID;
    @track listReleaseDoc = [];
    @track listReleaseDocNames ;
    @track columns=columns;
    
    pdfFiles = [];

    @wire(getReleaseDocNames, {})
    wiredReleaseDocNamesMethod({ error, data }) {
        if (data) {
            this.listReleaseDocNames = data;
        } else if (error) {
            this.error = error;
        }
        console.log('listServReqs ' +JSON.stringify(data));
        console.log('listServReqs ' +JSON.stringify( this.listReleaseDocNames));       
    }
    
    @wire(getRelatedFilesByRecordId, {})
    wiredRelatedFilesByRecordIdMethod({ error, data }) {
        if (data) {           
            this.listReleaseDoc = data;
            this.pdfFiles = data;
            const fileIDs = Object.keys(data);
            this.fileID =  fileIDs.length ? fileIDs[0] : undefined;
            
        } else if (error) {
            this.error = error;
            this.pdfFiles = undefined;
            this.fileID = undefined;
        }
        //this.tableLoadingState = false;
    }
    
    // Maps file ID and title to tab value and label
    get tabs() {
        
        if (!this.fileID) return [];

        const tabs = [];
        const files = Object.entries(this.pdfFiles);
        for (const [ID, title] of files) {
            tabs.push({
                value: ID,
                label: title
            });
        }
        return tabs;
    }

    // event handler for each tab: onclick tab, change fileID
    setFileID(e) {
        this.fileID = e.target.value;
    }

    navigateToMyOpenCasesListView() {

		if (this.mySupportCasesPageName == null) {

            // Navigate to the Case object's "Contact Open Cases" list view.
            this[NavigationMixin.Navigate]({
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: 'Case',
                    actionName: 'list'
                },
                state: {
                    // 'filterName' is a property on the page 'state'
                    // and identifies the target list view.
                    // It may also be an 18 character list view id.
                    filterName: 'Contact_Open_Cases' // or by 18 char '00BT0000002TONQMA4'
                }
            });

		}
		else {

            // Navigate to the Case object's "Contact Open Cases" list view.
            this[NavigationMixin.Navigate]({
                type: 'comm__namedPage',
                attributes: {
                    name: this.mySupportCasesPageName
                }
            });

        }
	
    }

    navigateToRenewU() {

        if (this.renewUUrl != null) {

            // Navigate to the RewewU web page.
            this[NavigationMixin.Navigate]({
                type: 'standard__webPage',
                attributes: {
                    url: this.renewUUrl
                }
            },
            false // Replaces the current page in your browser history with the URL
            );
            
        }

    }

}