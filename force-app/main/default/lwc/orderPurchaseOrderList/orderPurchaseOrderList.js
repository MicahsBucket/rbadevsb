/*
* @author Jason Flippen
* @date 09/15/2020 
* @description Related List designed to be used in place of the standard Related List for Product/RbA Purchase Order.
*
*              Server-Side support provided by the following Class:
*			   - OrderPurchaseOrderListController
*/
import { LightningElement, api, track, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getPurchaseOrderData from '@salesforce/apex/OrderPurchaseOrderListController.getPurchaseOrderData';
import getDefaultStoreLocationId from '@salesforce/apex/OrderPurchaseOrderListController.getDefaultStoreLocationId';
import getVendorList from '@salesforce/apex/OrderPurchaseOrderListController.getVendorList';
import createPO from '@salesforce/apex/OrderPurchaseOrderListController.createPO';

const columns = [
    { label: 'Purchase Order Number', fieldName: 'purchaseOrderUrl', type: 'url', typeAttributes: { label : { fieldName: 'name' }, target: '_top', tooltip: { fieldName: 'name' } } },
    { label: 'Record Type', fieldName: 'recordTypeName', type: 'string', cellAttributes: { alignment: 'left' } },
    { label: 'Amount', fieldName: 'amount', type: 'currency', cellAttributes: { alignment: 'left'} },
    { label: 'Status', fieldName: 'status', type: 'string', cellAttributes: { alignment: 'left'} },
    { label: 'Estimated Ship Date', fieldName: 'estimatedShipDate', type: 'date-local', cellAttributes: { alignment: 'left' }, typeAttributes: { month: '2-digit', day: '2-digit' } },
];

const productColumns = [
    { label: 'Unit Id', fieldName: 'unitId', type: 'string', wrapText: true, cellAttributes: { alignment: 'left' } },
    { label: 'Product', fieldName: 'productName', type: 'string', wrapText: true, cellAttributes: { alignment: 'left' } },
    { label: 'Description', fieldName: 'productDescription', type: 'string', wrapText: true, cellAttributes: { alignment: 'left'} },
    { label: 'Quantity', fieldName: 'quantity', type: 'number', initialWidth: 100, cellAttributes: { alignment: 'left' } },
    { label: 'Variant Number', fieldName: 'variantNumber', type: 'string', initialWidth: 150, wrapText: true, cellAttributes: { alignment: 'left'} },
    { label: 'Cost', fieldName: 'unitWholesaleCost', type: 'currency', initialWidth: 100, cellAttributes: { alignment: 'left'} },
];

export default class orderPurchaseOrderList extends NavigationMixin(LightningElement) {

    @api recordId;
    @api recordPageUrl;

    @track error;
    @track title = 'Purchase Orders (0)';
    @track columnList = columns;
    @track recordList = [];
    @track showData = false;
    @track showNewPOModal = false;
    @track showSpinner = false;

    @track vendorList = [];
    @track selectedVendorId = null;
    @track selectedVendorName = '';
    @track productColumnList = productColumns;
    @track productRecordList = [];
    @track noProductsAvailable = true;
    @track disableNewPurchaseOrderButton = true;
    @track disablePOCancelButton = false;
    @track disablePOSaveButton = true;
    @track newPO = {};

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to retrieve List of columns and records to be displayed. 
    */
    @wire(getPurchaseOrderData, { orderId: '$recordId', recordPageUrl: '$recordPageUrl' })
    wiredGetPurchaseOrderData({ error, data }) {

        if (error) {
            console.log('Error in getPurchaseOrderData callback', error);
            this.error = error;
        }
        else if (data) {
            console.log('Record Data', JSON.parse(JSON.stringify(data)));
            this.recordList = JSON.parse(JSON.stringify(data));
            this.title = 'Purchase Orders (' + this.recordList.length + ')';
            if (this.recordList.length > 0) {
                this.showData = true;
                this.newPO.Order__c = this.recordId;
            }
            this.getUserDefaultStoreLocation();
            this.getAvailableVendors();
        }

    }

    /*
    * @author Jason Flippen
    * @date 03/11/2021
    * @description Method to retrieve Default Store Location from the Order.
    */
    getUserDefaultStoreLocation() {

        getDefaultStoreLocationId({ orderId: this.recordId })
        .then(result => {
            console.log('default store location', result);
            let storeLocationId = null;
            if (result !== '') {
                storeLocationId = result;
            }
            this.newPO.Store_Location__c = storeLocationId;
        })
        .catch(error => {
            console.log('Error in getDefaultStoreLocationId callback', error);
            this.error = error;
        })

    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method retrieve List of available Vendors eligible
    *              to be saved with the new Purchase Order. 
    */
    getAvailableVendors() {

        getVendorList({ orderId: this.recordId })
        .then(result => {
            console.log('Vendor List', JSON.parse(JSON.stringify(result)));
            this.vendorList = JSON.parse(JSON.stringify(result));
            console.log('Vendor Count', this.vendorList.length);
            if (this.vendorList.length === 1) {
                console.log('Selected Vendor Id', this.vendorList[0].value);
                this.selectedVendorId = this.vendorList[0].value;
                this.selectedVendorName = this.vendorList[0].label;
                this.newPO.Vendor__c = this.selectedVendorId;
                this.getAvailableProducts();
            }
            this.disableNewPurchaseOrderButton = false;
            this.enablePOSaveButton();
        })
        .catch(error => {
            console.log('Error in getVendorList callback', error);
            this.error = error;
        })

    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to retrieve List of Products eligible
    *              to be saved with the new Purchase Order. 
    */
    getAvailableProducts() {
        
        const vendorValue = this.selectedVendorId;

        let updatedProductRecordList = [];
        this.vendorList.forEach(function(item, index) {
            let vendor = JSON.parse(JSON.stringify(item));
            if (vendor.value === vendorValue) {
                console.log('Vendor', vendor);
                console.log('OrderItemList', JSON.parse(JSON.stringify(vendor.orderItemList)));
                updatedProductRecordList = JSON.parse(JSON.stringify(vendor.orderItemList));
            }
        });

        this.productRecordList = updatedProductRecordList;
        this.noProductsAvailable = true
        if (this.productRecordList.length > 0) {
            this.noProductsAvailable = false;
        }

    }


/** Click Events **/


    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to display the new Purchase Order LWC in a modal window.
    */
    handleNewPOClick(event) {

        // If we have Vendors to select from let's show the modal window.
        if (this.vendorList.length > 0) {
            this.showNewPOModal = true;
        }
        else {
            this.handleNoRecordsAvailable();
        }

    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to close the Purchase Order modal window.
    */
    handleCancelPOClick(event) {
        this.showNewPOModal = false;
    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to handle the click event of the PO "Save" button.
    */
    handleSavePOClick(event) {

        this.disablePOCancelButton = true;
        this.disablePOSaveButton = true;
        this.showSpinner = true;

        createPO({ wrapperList: this.productRecordList,
                   orderId: this.recordId,
                   newPurchaseOrder: this.newPO,
                   vendorName: this.selectedVendorName })
        .then(result => {

            var resultMap = result;

            var returnValue = '';
            var saveSuccess = false;
            for (var key in resultMap) {
                returnValue = key;
                if (key === 'New PO Success') {
                    returnValue = resultMap[key];
                    saveSuccess = true;
                }
                break;
            }
            console.log('Return Value', returnValue);

            this.showNewPOModal = false;

            if (saveSuccess === true) {
                this.handleSavePOSuccess(returnValue);
            }
            else if (saveSuccess === false) {
                this.handleSavePOFailure(returnValue);
            }

        })
        .catch(error => {
            console.log('Error in createPO callback', error);
            this.error = error;
        })

    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to handle the "Edit" click event.
    */
    handleEditClick(row) {

        // Opens the Purchase Order record modal
        // to view a particular record.
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: row.id,
                objectApiName: 'Purchase_Order__c', // objectApiName is optional
                actionName: 'edit'
            }
        });

    }


/** Change Events **/


    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method handle the "Change" event on the Vendor combobox. 
    */
    handleVendorChange(event) {
        this.selectedVendorId = event.detail.value
        this.newPO.Vendor__c = this.selectedVendorId;
        for (var index in this.vendorList) {
            var vendor = this.vendorList[index];
            if (vendor.value === this.selectedVendorId) {
                this.selectedVendorName = vendor.label;
                break;
            }
        }
        this.getAvailableProducts();
        this.enablePOSaveButton();
    }

    /*
    * @author Jason Flippen
    * @date 06/09/2020
    * @description Method to handle the changed Store Location value.
    */
    handlePOStoreLocationChange(event) {
        var newPOStoreLocation = event.target.value;
        if (newPOStoreLocation === '') {
            newPOStoreLocation = null;
        }
        this.newPO.Store_Location__c = newPOStoreLocation;
        this.enablePOSaveButton();
    }

    /*
    * @author Jason Flippen
    * @date 09/16/2020
    * @description Method to handle the changed Requested Ship Date value.
    */
    handlePOReqShipDateChange(event) {
        this.newPO.Requested_Ship_Date__c = event.target.value;
    }


/** Miscellaeous Methods **/


    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to set the "disablePOSaveButton" flag.
    */
    enablePOSaveButton() {

        console.log('Selected Vendor Id', this.selectedVendorId);
        console.log('Store Location', this.newPO.Store_Location__c);
        console.log('productRecordList.length', this.productRecordList.length);
        if (this.selectedVendorId !== null &&
            this.newPO.Store_Location__c !== null &&
            this.productRecordList.length > 0) {
            this.disablePOSaveButton = false
        }
        else {
            this.disablePOSaveButton = true;
        }

    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to handle the successful creation of a new PO.
    */
    handleSavePOSuccess(purchaseOrderId) {

        this.disablePOCancelButton = false;
        this.disablePOSaveButton = false;
        this.showSpinner = false;

        // Notify the User that the PO was created successfully.
        const toastEvent = new ShowToastEvent({
            message: 'New Purchase Order created',
            variant: 'success',
            mode: 'pester'
        });
        this.dispatchEvent(toastEvent);

        // Navigate to the new Purchase Order.
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: purchaseOrderId,
                objectApiName: 'Purchase_Order__c', // objectApiName is optional
                actionName: 'view'
            }
        });

    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to handle the new PO creation failure.
    */
    handleSavePOFailure(errorMessage) {

        this.disablePOCancelButton = false;
        this.disablePOSaveButton = false;
        this.showSpinner = false;

        // Notify the user that the PO creation failed.
        const toastEvent = new ShowToastEvent({
            title: 'Unable to create New Purchase Order',
            message: errorMessage,
            variant: 'error',
            mode: 'sticky'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to notify the User that there are no
    *              Products available to be create the PO.
    */
    handleNoRecordsAvailable(event) {

        // Close the modal window and notify the
        // user that we cannot create a PO.
        this.showNewPOModal = false;
        const toastEvent = new ShowToastEvent({
            message: 'No Products available to be added to a Purchase Order',
            variant: 'info'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 09/15/2020
    * @description Method to handle the row action click event.
    */
    handleRowAction(event) {

		var actionName = event.detail.action.name;
		var row = event.detail.row;
        
        switch (actionName) {
			case 'edit':
				this.handleEditClick(row);
				break;

			default:
        }

	}
    
}