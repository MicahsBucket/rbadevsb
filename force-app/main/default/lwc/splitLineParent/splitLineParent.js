import { LightningElement, api, track } from 'lwc';

export default class SplitLineParent extends LightningElement {
    @api splitParent;

    @track openDropdown = false

    openAndCloseDropdown(){
        if(this.openDropdown == true){
            this.openDropdown = false;
        }
        else{
            this.openDropdown = true;
        }
        
    }
}