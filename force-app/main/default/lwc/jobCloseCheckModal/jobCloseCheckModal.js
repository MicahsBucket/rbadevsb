import { LightningElement, api, wire} from 'lwc';
import getOrder from '@salesforce/apex/closeJobButtonController.getOrder';
import closeJob from '@salesforce/apex/closeJobButtonController.closeJob';

export default class JobCloseCheckModal extends LightningElement {
    @api recordId;
    isModalOpen = true;
    isEligible = false;
    textToDisplay = '';
    
    connectedCallback(){
        console.log('recordId', this.recordId);
        this.getOrderDetails();
    }

    getOrderDetails(){
        getOrder({recordId: this.recordId})
        .then(data => {
            if(data){
                console.log('getOrderDetails data',data);
                if(data.Status == 'Job Closed'){
                    this.isEligible = false;
                    this.textToDisplay = 'Job is already closed.';
                    this.dispatchEvent(new CustomEvent('DisableSave'));
                }
                else if((data.Status != 'Job in Progress' && data.Status != 'Install Complete')){
                    this.isEligible = false;
                    this.textToDisplay = 'Revenue must be recognized before it can go into a Job Closed status.';
                    this.dispatchEvent(new CustomEvent('DisableSave'));

                }
                else if(data.Revenue_Recognized_Date__c == null){
                    this.isEligible = false;
                    this.textToDisplay = 'Cannot close this order before Revenue Recognition.';
                    this.dispatchEvent(new CustomEvent('DisableSave'));
                }
                else{
                    this.isEligible = true;
                    this.textToDisplay = 'Are you sure you want to close this order?';
                }
            }
            else{
                this.data = null;
            }
        })
        .catch(error => {
            console.log(error);
        });
    }

    openModal() {
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
    }
    closeJob() {
        console.log('recordId: ', this.recordId);
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isModalOpen = false;
    }

    @api
    handleSaveClick() {
        console.log('IN CLICK HANDLER ON JobCloseCheckModal ID', this.recordId);
        var showSpinner = new CustomEvent('ShowSpinner');
        var hideSpinner = new CustomEvent('HideSpinner');
        var closeQuickAction = new CustomEvent('CloseQuickAction');

        this.dispatchEvent(showSpinner);

        // Call Apex method to cancel the Purchase Order.
        closeJob({ OrderId: this.recordId })
        .then((result) => {

            var toastMessage;
            var toastType;
            var saveSuccess = false;
            if (result === 'Job Closed') {
                saveSuccess = true;
                toastMessage = 'Job is Closed';
                toastType = 'success';
                this.getOrderDetails();
            }
            else  {
                toastMessage = result;
                toastType = 'error';
            }

            this.dispatchEvent(hideSpinner);
            
            const showToastEvent = new CustomEvent('ShowToast', { detail: { message: toastMessage, type: toastType } });
            this.dispatchEvent(showToastEvent);
            this.dispatchEvent(closeQuickAction);

//             if (saveSuccess === true) {

//                 // Workaround way to refresh the record page.
//                 updateRecord({ fields: { Id: this.recordId } });

//                 // Fire event to refresh the cache.
//                 fireEvent(this.pageRef, 'updatePOEvent', '');

// //                // Use the provisioned value to refresh wiredGetPurchaseOrderData.
// //                return refreshApex(this.wiredPOResult);
//             }
            
        })
        .catch((error) => {
            console.log('Cancel Purchase Order Error', error);
            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(this.closeQuickAction);
        });

    }

    /*
    * @author Jason Flippen
    * @date 03/16/2020
    * @description Method to refresh the cache. 
    */
    // refreshPOData() {
    //     console.log('Refreshing cancelPO');
    //     return refreshApex(this.wiredPOResult);
    // }

}