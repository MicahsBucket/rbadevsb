/*
* @author Jason Flippen
* @date 02/06/2020 
* @description Related List designed to be used in place of the standard Related List for Service Purchase Order.
*
*              Server-Side support provided by the following Class:
*			   - ServicePurchaseOrderListController
*/
import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import { registerListener } from 'c/pubsub';
import getPurchaseOrderData from '@salesforce/apex/ServicePurchaseOrderListController.getPurchaseOrderData';
import getVendorList from '@salesforce/apex/ServicePurchaseOrderListController.getVendorList';
import getNewCostPOData from '@salesforce/apex/ServicePurchaseOrderListController.getNewCostPOData';
import createServicePO from '@salesforce/apex/ServicePurchaseOrderListController.createServicePO';
import createCostPO from '@salesforce/apex/ServicePurchaseOrderListController.createCostPO';

/*
const actions = [
    { label: 'Edit', name: 'edit' },
    { label: 'Delete', name: 'delete'}
];
*/

const columns = [
    { label: 'Purchase Order Number', fieldName: 'purchaseOrderUrl', type: 'url', typeAttributes: { label : { fieldName: 'name' }, target: '_top', tooltip: { fieldName: 'name' } } },
    { label: 'Record Type', fieldName: 'recordTypeName', type: 'string', cellAttributes: { alignment: 'left' } },
    { label: 'Amount', fieldName: 'amount', type: 'currency', cellAttributes: { alignment: 'left'} },
    { label: 'Status', fieldName: 'status', type: 'string', cellAttributes: { alignment: 'left'} },
    { label: 'Estimated Ship Date', fieldName: 'estimatedShipDate', type: 'date-local', cellAttributes: { alignment: 'left' }, typeAttributes: { month: '2-digit', day: '2-digit' } },
];

const productColumns = [
    { label: 'Product', fieldName: 'serviceProductName', type: 'string', cellAttributes: { alignment: 'left' } },
    { label: 'Installed Product', fieldName: 'installedProductName', type: 'string', cellAttributes: { alignment: 'left'} },
    { label: 'Variant Number', fieldName: 'variantNumber', type: 'string', cellAttributes: { alignment: 'left'} },
    { label: 'Responsibility', fieldName: 'chargeCostTo', type: 'string', cellAttributes: { alignment: 'left'} },
    { label: 'Source of Defect', fieldName: 'category', type: 'string', cellAttributes: { alignment: 'left'} },
    { label: 'Defect Details', fieldName: 'whatWhere', type: 'string', cellAttributes: { alignment: 'left'} },
];

export default class servicePurchaseOrderList extends NavigationMixin(LightningElement) {

    @api recordId;
    @api recordPageUrl;

    wiredPurchaseOrderResult;
    wiredNewCostPOResult;

    @track error;
    @track title = 'Purchase Orders (0)';
    @track columnList = columns;
    @track recordList = [];
    @track showData = false;
    @track showNewCostPOModal = false;
    @track showNewServicePOModal = false;
    @track showSpinner = false;

    @track vendorList = [];
    @track selectedVendorId = '';
    @track responsibilityList = [];
    @track selectedResponsibility = '';
    @track productColumnList = productColumns;
    @track productRecordList = [];
    @track noProductsAvailable = true;
    @track disableNewServicePOButton = true;
    @track disableServicePOCancelButton = false;
    @track disableServicePOSaveButton = true;
    @track disableNewCostPOButton = true;
    @track disableCostPOSaveButton = true;
    @track disableCostPOCancelButton = false;
    @track newCostPO = {};

    @track selectedProductRecordList = [];

    @wire(CurrentPageReference) pageRef;

    connectedCallback() {
        registerListener('updateOrderEvent', this.refreshPurchaseOrderData, this);
    }

    /*
    * @author Jason Flippen
    * @date 06/22/2020
    * @description Method to retrieve new (Cost) Purchase Order record. 
    */
    @wire(getNewCostPOData, { orderId: '$recordId' })
    wiredGetNewCostPOData(result) {

        // Hold on to the provisioned value so it can be refreshed.
        this.wiredNewCostPOResult = result;
        const { data, error } = result; // destructure the provisioned value
        if (data) {
            console.log('New Cost PO Data', JSON.parse(JSON.stringify(data)));
            this.newCostPO = JSON.parse(JSON.stringify(data));
            this.disableNewCostPOButton = false;
        }
        else if (error) {
            this.error = error;
            console.log('Error in wiredGetNewCostPOData callback', error);
        }

    }

    /*
    * @author Jason Flippen
    * @date 02/06/2020
    * @description Method to retrieve List of columns and records to be displayed. 
    */
    @wire(getPurchaseOrderData, { orderId: '$recordId', recordPageUrl: '$recordPageUrl' })
    wiredGetPurchaseOrderData(result) {

        // Hold on to the provisioned value so it can be refreshed.
        this.wiredPurchaseOrderResult = result;
        const { data, error } = result; // destructure the provisioned value
        if (data) {
            console.log('Record Data', JSON.parse(JSON.stringify(data)));
            this.recordList = JSON.parse(JSON.stringify(data));
            this.title = 'Purchase Orders (' + this.recordList.length + ')';
            if (this.recordList.length > 0) {
                this.showData = true;
            }
            this.getAvailableVendors();
        }
        else if (error) {
            this.error = error;
            console.log('Error in wiredGetPurchaseOrderData callback', error);
        }

    }

    /*
    * @author Jason Flippen
    * @date 02/14/2020
    * @description Method retrieve List of available Vendors eligible
    *              to be saved with the new Purchase Order. 
    */
    getAvailableVendors() {

        this.disableNewServicePOButton = true;

        getVendorList({ orderId: this.recordId })
        .then(result => {
            console.log('Vendor List', JSON.parse(JSON.stringify(result)));
            this.vendorList = JSON.parse(JSON.stringify(result));
            console.log('Vendor Count', this.vendorList.length);
            if (this.vendorList.length === 1) {
                console.log('Selected Vendor Id', this.vendorList[0].value);
                this.selectedVendorId = this.vendorList[0].value;
                this.getAvailableResponsibilities();
            }
            this.disableNewServicePOButton = false;
        })
        .catch(error => {
            console.log('Error in getVendors callback', error);
            this.error = error;
        })

    }

    /*
    * @author Jason Flippen
    * @date 03/19/2020
    * @description Method to retrieve List of available Responsibilities
    *              eligible to be saved with the new Purchase Order. 
    */
    getAvailableResponsibilities() {
        
        this.selectedResponsibility = '';
        const vendorValue = this.selectedVendorId;

        let updatedResponsibilityList = [];
        this.vendorList.forEach(function(item, index) {
            let vendor = JSON.parse(JSON.stringify(item));
            if (vendor.value === vendorValue) {
                console.log('Vendor', vendor);
                console.log('ResponsibilityList', JSON.parse(JSON.stringify(vendor.responsibilityList)));
                updatedResponsibilityList = JSON.parse(JSON.stringify(vendor.responsibilityList));
            }
        });

        this.responsibilityList = updatedResponsibilityList;
        if (this.responsibilityList.length === 1) {
            console.log('Selected Responsibility', this.responsibilityList[0].value);
            this.selectedResponsibility = this.responsibilityList[0].value;
            this.getAvailableProducts();
        }

    }

    /*
    * @author Jason Flippen
    * @date 02/11/2020
    * @description Method to retrieve List of Products eligible
    *              to be saved with the new Purchase Order. 
    */
    getAvailableProducts() {
        
        const responsibilityValue = this.selectedResponsibility;

        let updatedProductRecordList = [];
        this.responsibilityList.forEach(function(item, index) {
            let responsibility = JSON.parse(JSON.stringify(item));
            if (responsibility.value === responsibilityValue) {
                console.log('Resonsibility', responsibility);
                console.log('ChargeList', JSON.parse(JSON.stringify(responsibility.chargeList)));
                updatedProductRecordList = JSON.parse(JSON.stringify(responsibility.chargeList));
            }
        });

        this.productRecordList = updatedProductRecordList;
        this.noProductsAvailable = true
        if (this.productRecordList.length > 0) {
            this.noProductsAvailable = false;
        }

    }


/** Click Events **/


    /*
    * @author Jason Flippen
    * @date 02/06/2020
    * @description Method to display the new Cost Purchase Order LWC in a modal window.
    */
    handleNewCostPOClick(event) {
        this.refreshNewCostPOData();
        this.showNewCostPOModal = true;

    }

    /*
    * @author Jason Flippen
    * @date 02/06/2020
    * @description Method to close the Cost Purchase Order modal window.
    */
    handleCancelCostPOClick(event) {
        this.showNewCostPOModal = false;
    }

    /*
    * @author Jason Flippen
    * @date 06/15/2020
    * @description Method to handle the click event of the Cost PO "Save" button.
    */
    handleSaveCostPOClick(event) {

        this.disableCostPOCancelButton = true;
        this.disableCostPOSaveButton = true;
        this.showSpinner = true;

        createCostPO({ newPurchaseOrder: this.newCostPO  })
        .then(result => {

            var resultMap = result;

            var returnValue = '';
            var saveSuccess = false;
            for (var key in resultMap) {
                returnValue = key;
                if (key === 'New Cost PO Success') {
                    returnValue = resultMap[key];
                    saveSuccess = true;
                }
                break;
            }
            console.log('Return Value', returnValue);

            this.showNewCostPOModal = false;

            if (saveSuccess === true) {
                this.handleSaveCostPOSuccess(returnValue);
            }
            else if (saveSuccess === false) {

                this.handleSaveCostPOFailure(returnValue);

            }

        })
        .catch(error => {
            console.log('Error in createCostPO callback', error);
            this.error = error;
        })

    }

    /*
    * @author Jason Flippen
    * @date 02/14/2020
    * @description Method refresh the List of Rows that have been selected. 
    */
    handleRowSelectionClick(event) {
        console.log('Event Detail Selected Rows', event.detail.selectedRows);
        // console.log('selectedProductRecordList', this.selectedProductRecordList );
        this.selectedProductRecordList = event.detail.selectedRows;
        console.log('selectedProductRecordList in handleRowSelectionClick', this.selectedProductRecordList);
        this.enableServicePOSaveButton();
    }

    /*
    * @author Jason Flippen
    * @date 02/06/2020
    * @description Method to display the new Service Purchase Order LWC in a modal window.
    */
    handleNewServicePOClick(event) {

        // If we have Vendors to select from let's show the modal window.
        if (this.vendorList.length > 0) {
            this.showNewServicePOModal = true;
        }
        else {
            this.handleNoRecordsAvailable();
        }
    }

    /*
    * @author Jason Flippen
    * @date 02/06/2020
    * @description Method to close the Service Purchase Order modal window.
    */
    handleCancelServicePOClick(event) {
        this.selectedProductRecordList = [];
        this.showNewServicePOModal = false;
    }

    /*
    * @author Jason Flippen
    * @date 02/14/2020
    * @description Method to fire the "handleSaveClick" event in the child component.
    */
    handleSaveServicePOClick(event) {

        this.disableServicePOCancelButton = true;
        this.disableServicePOSaveButton = true;
        this.showSpinner = true;

        createServicePO({ chargeWrapperList: this.selectedProductRecordList, selectedVendorId: this.selectedVendorId, orderId: this.recordId })
        .then(result => {

            const resultMap = result;

            let returnValue = '';
            let saveSuccess = false;
            for (var key in resultMap) {
                returnValue = key;
                if (key === 'New Service PO Success') {
                    returnValue = resultMap[key];
                    saveSuccess = true;
                }
                break;
            }
            console.log('Return Value', returnValue);

            this.showNewServicePOModal = false;

            if (saveSuccess === true) {

                this.handleSaveServicePOSuccess(returnValue);

/*
                // Custom event that bubbles to Parent component.
                const saveSuccessEvent = new CustomEvent('savesuccess', {
                    detail: returnValue,
                    bubbles: true
                });
                
                // Fire the custom event
                this.dispatchEvent(saveSuccessEvent);
*/
            
            }
            else if (saveSuccess === false) {

                this.handleSaveServicePOFailure(returnValue);

/*
                // Custom event that bubbles to Parent component.
                const saveFailureEvent = new CustomEvent('savefailure', {
                    detail: returnValue,
                    bubbles: true
                });
                
                // Fire the custom event
                this.dispatchEvent(saveFailureEvent);
*/

            }

        })
        .catch(error => {
            console.log('Error in createServicePO callback', error);
            this.error = error;
        })


    }

    /*
    * @author Jason Flippen
    * @date 02/07/2020
    * @description Method to handle the "Edit" click event.
    */
    handleEditClick(row) {

        // Opens the Purchase Order record modal
        // to view a particular record.
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: row.id,
                objectApiName: 'Purchase_Order__c', // objectApiName is optional
                actionName: 'edit'
            }
        });

    }


/** Change Events **/


    /*
    * @author Jason Flippen
    * @date 08/25/2020
    * @description Method to handle the changed Vendor value from the New Cost PO screen.
    */
    handleCostPOVendorChange(event) {
        let newCostPOVendor = event.target.value;
        if (newCostPOVendor === '') {
            newCostPOVendor = null;
        }
        this.newCostPO.Vendor__c = newCostPOVendor;
        this.enableCostPOSaveButton();
    }

    /*
    * @author Jason Flippen
    * @date 06/09/2020
    * @description Method to handle the changed Store Location value.
    */
    handleCostPOStoreLocationChange(event) {
        let newCostPOStoreLocation = event.target.value;
        if (newCostPOStoreLocation === '') {
            newCostPOStoreLocation = null;
        }
        this.newCostPO.Store_Location__c = newCostPOStoreLocation;
        this.enableCostPOSaveButton();
    }

    /*
    * @author Jason Flippen
    * @date 06/09/2020
    * @description Method to handle the changed Requested Ship Date value.
    */
    handleCostPOReqShipDateChange(event) {
        this.newCostPO.Requested_Ship_Date__c = event.target.value;
    }

    /*
    * @author Jason Flippen
    * @date 06/09/2020
    * @description Method to handle the changed Reference value.
    */
    handleCostPOReferenceChange(event) {
        this.newCostPO.Reference__c = event.target.value;
    }

    /*
    * @author Jason Flippen
    * @date 06/09/2020
    * @description Method to handle the changed Comments value.
    */
    handleCostPOCommentsChange(event) {
        this.newCostPO.Comments__c = event.target.value;
    }

    /*
    * @author Jason Flippen
    * @date 02/14/2020
    * @description Method handle the "Change" event on the Vendor combobox. 
    */
    handleVendorChange(event) {
        this.selectedProductRecordList = [];
        this.productRecordList = [];
        this.template.querySelector('lightning-datatable[data-field="purchaseOrderProductComponent"]').selectedRows = [];
        this.selectedVendorId = event.detail.value;
        this.getAvailableResponsibilities();
        this.enableServicePOSaveButton();
    }

    /*
    * @author Jason Flippen
    * @date 03/19/2020
    * @description Method handle the "Change" event on the Responsibility combobox. 
    */
    handleResponsibilityChange(event) {
        this.selectedProductRecordList = [];
        this.productRecordList = [];
        this.template.querySelector('lightning-datatable[data-field="purchaseOrderProductComponent"]').selectedRows = [];
        this.selectedResponsibility = event.detail.value;
        this.getAvailableProducts();
        this.enableServicePOSaveButton();
    }


/** Miscellaeous Methods **/


    /*
    * @author Jason Flippen
    * @date 08/25/2020
    * @description Method to set the "disableCostPOSaveButton" flag.
    */
    enableCostPOSaveButton() {

        if (this.newCostPO.Vendor__c !== null && this.newCostPO.Store_Location__c !== null) {
            this.disableCostPOSaveButton = false
        }
        else {
            this.disableCostPOSaveButton = true;
        }

    }

    /*
    * @author Jason Flippen
    * @date 06/15/2020
    * @description Method to handle the successful creation of a new Cost PO.
    */
    handleSaveCostPOSuccess(purchaseOrderId) {

        this.disableCostPOCancelButton = false;
        this.disableCostPOSaveButton = false;
        this.showSpinner = false;

        // Notify the User that the Cost PO was created successfully.
        const toastEvent = new ShowToastEvent({
            message: 'New Cost Purchase Order created',
            variant: 'success',
            mode: 'pester'
        });
        this.dispatchEvent(toastEvent);

        // Navigate to the new Cost Purchase Order.
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: purchaseOrderId,
                objectApiName: 'Purchase_Order__c', // objectApiName is optional
                actionName: 'view'
            }
        });

    }

    /*
    * @author Jason Flippen
    * @date 06/15/2020
    * @description Method to handle the new Cost PO creation failure.
    */
    handleSaveCostPOFailure(errorMessage) {

        this.disableCostPOCancelButton = false;
        this.disableCostPOSaveButton = false;
        this.showSpinner = false;

        // Notify the user that the Cost PO creation failed.
        const toastEvent = new ShowToastEvent({
            title: 'Unable to create New Cost Purchase Order',
            message: errorMessage,
            variant: 'error',
            mode: 'sticky'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 02/19/2020
    * @description Method fired from child component to handle the scenario
    *              where no records are available to create a Service PO.
    */
    handleNoRecordsAvailable(event) {

        // Close the modal window and notify the user
        // that we cannot create a Service PO.
        this.showNewServicePOModal = false;
        const toastEvent = new ShowToastEvent({
            message: 'All products on this Service Request have been added to a Purchase Order',
            variant: 'info'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 03/05/2021
    * @description Method to set the "disableServicePOSaveButton" flag.
    */
    enableServicePOSaveButton() {

        if (this.selectedVendorId !== null &&
            this.selectedResponsibility != null &&
            this.selectedProductRecordList.length > 0) {
            this.disableServicePOSaveButton = false
        }
        else {
            this.disableServicePOSaveButton = true;
        }

    }



    /*
    * @author Jason Flippen
    * @date 02/14/2020
    * @description Method fired from child component to handle
    *              the successful creation of a new Service PO.
    */
    handleSaveServicePOSuccess(purchaseOrderId) {

        this.disableServicePOCancelButton = false;
        this.disableServicePOSaveButton = false;
        this.showSpinner = false;

        // Close the modal window and notify the user that
        // the Service PO was created successfully.
        this.showNewServicePOModal = false;
        const toastEvent = new ShowToastEvent({
            message: 'New Service Purchase Order created',
            variant: 'success',
            mode: 'pester'
        });
        this.dispatchEvent(toastEvent);

        // Navigate to the new Service Purchase Order.
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: purchaseOrderId,
                objectApiName: 'Purchase_Order__c', // objectApiName is optional
                actionName: 'view'
            }
        });

    }

    /*
    * @author Jason Flippen
    * @date 02/14/2020
    * @description Method fired from child component to handle the
    *              new Service PO creation failure.
    */
    handleSaveServicePOFailure(errorMessage) {

        this.disableServicePOCancelButton = false;
        this.disableServicePOSaveButton = false;
        this.showSpinner = false;

        // Close the modal window and notify the user
        // that the Service PO creation failed.
        this.showNewServicePOModal = false;
        const toastEvent = new ShowToastEvent({
            title: 'Unable to create New Service Purchase Order',
            message: errorMessage,
            variant: 'error',
            mode: 'sticky'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 02/07/2020
    * @description Method to handle the row action click event.
    */
    handleRowAction(event) {

		const actionName = event.detail.action.name;
		const row = event.detail.row;
        
        switch (actionName) {
			case 'edit':
				this.handleEditClick(row);
				break;

			default:
        }
/*
        switch (actionName) {
			case 'delete':
				this.handleDeleteClick(row);
				break;

			default:
        }
*/

	}

    /*
    * @author Jason Flippen
    * @date 03/03/2021
    * @description Method to refresh the cache. 
    */
    refreshPurchaseOrderData() {
        console.log('Refreshing Purchase Order Data');
        return refreshApex(this.wiredPurchaseOrderResult);
    }

    /*
    * @author Jason Flippen
    * @date 03/03/2021
    * @description Method to refresh the cache. 
    */
    refreshNewCostPOData() {
        console.log('Refreshing New Cost PO Data');
        return refreshApex(this.wiredNewCostPOResult);
    }

}