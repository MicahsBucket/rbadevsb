import { LightningElement, api} from 'lwc';

export default class Modal extends LightningElement {
    @api visible = false;
    @api modalSize = 'medium';
    @api showHeader = false;
    @api showFooter = false;

    close(){
        this.visible = false;
        this.dispatchEvent(new CustomEvent('modalclose'));
    }

    open(){
        this.visible = true;
    }

    get modalSizeCSS(){
        if(this.modalSize === 'medium'){
            return 'slds-modal_medium';
        }else if(this.modalSize === 'small'){
            return 'slds-modal_small';
        }else if(this.modalSize === 'large'){
            return 'slds-modal_large';
        }
        return '';
    }
}