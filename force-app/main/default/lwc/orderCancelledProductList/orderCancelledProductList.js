import { LightningElement, api, track, wire } from 'lwc';
import getCancelledOrderItems from '@salesforce/apex/OrderCancelledProductListController.getCancelledOrderItems';

const actions = [
    { label: 'View', name: 'view' }
];

const columns = [
    { label: 'Unit Id', fieldName: 'unitId', cellAttributes: {'alignment': 'left'} },
    { label: 'Product', fieldName: 'productName', cellAttributes: {'alignment': 'left'} },
    { label: 'Purchase Order', fieldName: 'purchaseOrderName', cellAttributes: {'alignment': 'left'}, type: 'string' },
    { label: 'Quantity', fieldName: 'quantity', cellAttributes: {'alignment': 'left'}, type: 'number' },
    { label: 'Unit Price', fieldName: 'unitPrice', cellAttributes: {'alignment': 'left'}, type: 'currency' },
    { label: 'Total Retail Price', fieldName: 'totalRetailPrice', cellAttributes: {'alignment': 'left'}, type: 'currency' },
    { label: 'Wholesale Cost', fieldName: 'unitWholesaleCost', cellAttributes: {'alignment': 'left'}, type: 'currency' },
    { label: 'Total Wholesale Cost', fieldName: 'totalWholesaleCost', cellAttributes: {'alignment': 'left'}, type: 'currency' },
    { label: 'Variant Number', fieldName: 'variantNumber', cellAttributes: {'alignment': 'left'}, type: 'string' },
    { label: 'Cancellation Reason', fieldName: 'cancellationReason', cellAttributes: {'alignment': 'left'}, type: 'string'},
    {
        type: 'action',
        typeAttributes: { rowActions: actions, menuAlignment: 'Auto' },
    }
];

export default class orderCancelledProductList extends LightningElement {

    @api recordId;
	@api rowId;
    @api childData = [];
	@track columns = columns;
	@track title = 'Cancelled Products (0)';
    @track showData = false;
	@track data = [];
	@track error;

	@wire(getCancelledOrderItems, { orderId: '$recordId' })
	wiredCancelledOrderItems({error, data}) {
		if (error) {
			this.error = error;
			console.log('error in callback', error);
        }
        else if (data) {
            console.log('Data:', data);
			let tempJSON = JSON.parse(JSON.stringify(data).split('miscCharges').join('_children'));
			console.log('tempJSON data', JSON.parse(JSON.stringify(tempJSON)));
			this.data = tempJSON;
            this.title = 'Cancelled Products (' + this.data.length + ')';
            if (this.data.length > 0) {
                this.showData = true;
            }
		}
    }
    
    handleRowAction(event) {

		console.log('In Row Action');
		const actionName = event.detail.action.name;
		const row = event.detail.row;
		console.log('stringify row: ' + JSON.parse(JSON.stringify(row)));
		this.rowId = row.id;
		this.denyCancel = false;
		switch (actionName) {
			case 'view':
				this.handleViewClicked(row);
				break;

			default:
		}

	}

	handleViewClicked(row) {

		// Navigate to the appropriate read-only Product page.
		if (row.productOpenInVisualforce === true) {
			window.open('/apex/RMS_prodConfigEdit?id=' + row.id, '_top');
		}
		else {
			window.open('/apex/ReadOnlyOrderProductPage?orderItemId=' + row.id + '&orderId=' + this.recordId, '_top');
		}

	}

}