import { LightningElement, api, wire,track } from 'lwc';
import getAccountDetails from '@salesforce/apex/Account_Details_Controller.getAccount';
import getOrderDetails from '@salesforce/apex/Account_Details_Controller.getOrders';

const columns = [
    //{label: 'Action', fieldName: 'editUrl', type: 'url', 
    //typeAttributes: {label:  'Edit' , target: '_blank'}},
    { label: 'Order Number', fieldName: 'orderNumberUrl' , type: 'url', 
    typeAttributes: {label: {fieldName: 'OrderNumber'}, target: '_blank'}},
    { label: 'Status', fieldName: 'status', type: 'text' },
    { label: 'Booking Date', fieldName: 'effectiveDate', type:'date-local',
    typeAttributes: {day: 'numeric', month: 'numeric' , year: 'numeric'}},
    { label: 'Retail Total', fieldName: 'retailTotal', type: 'currency',
    cellAttributes: { alignment: 'left' }},    
    { label: 'Amount Due', fieldName: 'amountDue', type: 'currency', 
    cellAttributes: { alignment: 'left' }},
];
export default class AccountDetails extends LightningElement {
    @api recordId;

    @wire(getAccountDetails, { accountId: '$recordId'  })
    account;

    @wire(getOrderDetails, { accountId: '$recordId'  }) 
    data ;
    @track columns = columns;

    
    get accounturl() {
        return '/'+this.account.data.Id;
    }

    get salesRepurl() {
        return '/'+this.account.Sales_Rep_User__c;
    }

    get hoaurl() {
        return '/'+this.account.HOA__c;
    }

    get historicalurl() {
        return '/'+this.account.Historical__c;
    }

    get buildingPermiturl() {
        return '/'+this.account.Building_Permit__c;
    }

}