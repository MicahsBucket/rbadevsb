import { LightningElement, wire, track,api } from 'lwc';
import findProducts from '@salesforce/apex/ProductSelectorController.findProducts';
const DELAY = 300;

export default class OrderItemSelector extends LightningElement {

    @api inrop;
    @api searchTerm;
    @track productRecordId = '';
    @track productName = '';
    @track searching = false;
    @track productCode='';


    connectedCallback(){
        if(this.inrop === undefined){
            this.inrop = false;
        }
        console.log('hit connectedCallback - product selector', this.inrop);
    }

    @wire(findProducts, { searchTerm: '$searchTerm', inRop:'$inrop' })
    products;

    searchChangeHandler(event) {
        console.log('on change handler',event.target.value);
        console.log('on change handler',event.target);
        console.log('on change handler',event);
        if(event.target.value === null || event.target.value === undefined || event.target.value === ''){
            this.sendClearEvent();
        }
        window.clearTimeout(this.delayTimeout);
        const searchTerm = event.target.value;
        this.searching = true;

        // Set a delay so we dont fire too many apex queries
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            this.searchTerm = searchTerm;
        }, DELAY);

    }

    clickHandler(event) {

        // Set tracked variables
        this.searching = false;
        this.productRecordId = event.target.value;
        this.productName = event.target.title;
        this.searchTerm = event.target.title;
        this.productCode=event.target.dataset.id;
        console.log('Data RecID: '+event.target.dataset.id);


        // Set the text in the search box to the selected product
        // let field = this.template.querySelector('lightning-input');
        // field.value = this.productName;

        // eslint-disable-next-line no-console
        // console.log(field);

        // eslint-disable-next-line no-console
        console.log('this.productRecordId= ' + this.productRecordId);

        // eslint-disable-next-line no-console
        console.log('this.productName= ' + this.productName);

        // Set up and send product selection event
        const selectevent = new CustomEvent('productidupdate', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                Product2Id: this.productRecordId,
                ProductName: this.productName,
                ProductCodeValue:this.productCode
            },
        });
        this.dispatchEvent(selectevent);
    }
    sendClearEvent(){
        console.log('hit send clear event');
        const selectevent = new CustomEvent('productidupdate', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                Product2Id: null,
                ProductName: null,
                ProductCodeValue:null,
                clearForm:true
            },
        });
        this.dispatchEvent(selectevent);
    }
    @api
    searchFocus()
    { console.log('trying to set focus here');
       const searchbox= this.template.querySelector("lightning-input[data-id=productsearch]");
       console.log('trying to set focus here after'+searchbox);
  searchbox.focus();
    }
}