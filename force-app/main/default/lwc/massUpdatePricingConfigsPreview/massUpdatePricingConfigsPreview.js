import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getPricingConfigurations from '@salesforce/apex/MassUpdatePricingConfigsController.getPricingConfigurations';
import getPricingConfigurationColumns from '@salesforce/apex/MassUpdatePricingConfigsController.getPricingConfigurationColumns';
import savePricingConfigurationUpdates from '@salesforce/apex/MassUpdatePricingConfigsController.savePricingConfigurationUpdates';

export default class MassUpdatePricingConfigsPreview extends NavigationMixin(LightningElement) {

    @api wholesalePricebookId;
    @api productList;
    @api beginningUI;
    @api endingUI;
    @api unitField;
    @api trimField;
    @api colorField;
    @api glazingField;
    @api grilleField;
    @api hardwareField;
    @api screenField;
    @api increaseBy;
    @api increaseDollar;
    @api increasePercentage;
    @api roundTo;

    @track showSpinner = false;
    @track updatedPricingConfigurationList = [];
    @track pricingConfigurationsNotFound = true;
    @track error;

    fieldNameList = [];
    columnList = [];

    connectedCallback() {
        console.log('Wholesale Pricebook Id:', this.wholesalePricebookId);
    }

    // Injects the page reference that describes the current page
    @wire(CurrentPageReference)
    setCurrentPageReference(currentPageReference) {

        // Grab the parameters from the querystring.
        this.currentPageReference = currentPageReference;
        this.wholesalePricebookId = this.currentPageReference.state.c__recordId;
        this.productList = this.currentPageReference.state.c__productList;
        this.beginningUI = this.currentPageReference.state.c__beginningUI;
        this.endingUI = this.currentPageReference.state.c__endingUI;

        this.fieldNameList = [];
        if (this.currentPageReference.state.c__unit !== undefined &&
            this.currentPageReference.state.c__unit !== '') {

            if (this.currentPageReference.state.c__unit === 'All') {
                this.fieldNameList.push('DP_Performance_Upgrade__c');
                this.fieldNameList.push('EJ_Full_Frame__c');
            }
            else {
                this.fieldNameList.push(this.currentPageReference.state.c__unit);
            }

        }

        if (this.currentPageReference.state.c__trim !== undefined &&
            this.currentPageReference.state.c__trim !== '') {

            if (this.currentPageReference.state.c__trim === 'All') {
                this.fieldNameList.push('Aluminum_Drip_Cap_1_or_2_piece__c');
                this.fieldNameList.push('Brickmould_3_Inch__c');
                this.fieldNameList.push('Factory_Applied_Brickmould__c');
                this.fieldNameList.push('Fibrex_L_Trim__c');
                this.fieldNameList.push('Overfit_Flange_Price__c');
                this.fieldNameList.push('Overfit_Chamfered_Edge__c');
                this.fieldNameList.push('Nail_Flange__c');
            }
            else {
                this.fieldNameList.push(this.currentPageReference.state.c__trim);
            }

        }

        if (this.currentPageReference.state.c__color !== undefined &&
            this.currentPageReference.state.c__color !== '') {

            if (this.currentPageReference.state.c__color === 'All') {
                this.fieldNameList.push('Base_Unit__c');
                this.fieldNameList.push('Dual_Color__c');
                this.fieldNameList.push('Stainable_Wood_Interior__c');
                this.fieldNameList.push('Premium_Stainable_Wood_Interior__c');
                this.fieldNameList.push('Wrapped_Interior_Color__c');
            }
            else {
                this.fieldNameList.push(this.currentPageReference.state.c__color);
            }

        }

        if (this.currentPageReference.state.c__glazing !== undefined &&
            this.currentPageReference.state.c__glazing !== '') {

            if (this.currentPageReference.state.c__glazing === 'All') {
                this.fieldNameList.push('Tempered__c');
                this.fieldNameList.push('SmartSun__c');
                this.fieldNameList.push('Heat_Lock__c');
                this.fieldNameList.push('Obscure__c');
                this.fieldNameList.push('ObscureTempered__c');
                this.fieldNameList.push('Enhanced_Triple_Pane__c');
                this.fieldNameList.push('SpecialtyGlazing__c');
                this.fieldNameList.push('SpecialtyTemperedGlazing__c');
                this.fieldNameList.push('Laminated__c');
                this.fieldNameList.push('LaminatedAndTempered__c');
                this.fieldNameList.push('Breather_Tubes__c');
            }
            else {
                this.fieldNameList.push(this.currentPageReference.state.c__glazing);
            }

        }

        if (this.currentPageReference.state.c__grille !== undefined &&
            this.currentPageReference.state.c__grille !== '') {

            if (this.currentPageReference.state.c__grille === 'All') {
                this.fieldNameList.push('Grilles_Between_Glass__c');
                this.fieldNameList.push('GBG_Wide_Bar__c');
                this.fieldNameList.push('Interior_Wood_Only__c');
                this.fieldNameList.push('Interior_Wood_Grilles_INTW__c');
                this.fieldNameList.push('INTW_Wide_Bar__c');
                this.fieldNameList.push('Full_Divided_Light__c');
                this.fieldNameList.push('FDL_Wide_Bar__c');
                this.fieldNameList.push('Applied_Grilles__c');
            }
            else {
                this.fieldNameList.push(this.currentPageReference.state.c__grille);
            }

        }

        if (this.currentPageReference.state.c__hardware !== undefined &&
            this.currentPageReference.state.c__hardware !== '') {

            if (this.currentPageReference.state.c__hardware === 'All') {
                this.fieldNameList.push('Corrosion_Resistant_Hardware__c');
                this.fieldNameList.push('Extra_Lock__c');
                this.fieldNameList.push('Extra_Lift_or_Pull__c');
                this.fieldNameList.push('Window_Opening_Control_Device__c');
                this.fieldNameList.push('Wide_Opening_Hinge__c');
                this.fieldNameList.push('Recessed_Lift_Charge__c');
                this.fieldNameList.push('Estate_Finish_Finger_Lifts__c');
                this.fieldNameList.push('Estate_Finish_Hand_Lift_or_Hand_Pull__c');
                this.fieldNameList.push('Estate_Finish_Hardware__c');
                this.fieldNameList.push('Estate_Finish_Lock_and_Keeper__c');
                this.fieldNameList.push('Fixed_Sash_Deduction_Casement__c');
            }
            else {
                this.fieldNameList.push(this.currentPageReference.state.c__hardware);
            }

        }

        if (this.currentPageReference.state.c__screen !== undefined &&
            this.currentPageReference.state.c__screen !== '') {

            if (this.currentPageReference.state.c__screen === 'All') {
                this.fieldNameList.push('AluminumScreenCloth__c');
                this.fieldNameList.push('TruScene__c');
                this.fieldNameList.push('TruScene_Dual__c');
                this.fieldNameList.push('TruScene_Wood_Veneer__c');
                this.fieldNameList.push('Aluminum_Screen_Half__c');
                this.fieldNameList.push('TruScene_Half__c');
            }
            else {
                this.fieldNameList.push(this.currentPageReference.state.c__screen);
            }

        }

        this.increaseBy = this.currentPageReference.state.c__increaseBy;
        this.increaseDollar = this.currentPageReference.state.c__increaseDollar;
        this.increasePercentage = this.currentPageReference.state.c__increasePercentage;
        this.roundTo = this.currentPageReference.state.c__roundTo;

        this.getPricingConfigurationColumnList();
        this.getPricingConfigurationList();
    }

    getPricingConfigurationColumnList() {
        getPricingConfigurationColumns({ fieldNameList: this.fieldNameList })
        .then(result => {
            this.columnList = JSON.parse(JSON.stringify(result));
            console.log('Column List:', this.columnList);
        })
        .catch(error => {
            console.log('Error Retrieving Pricing Configuration Columns');
            console.log('Error:', error);
        });
    }

    getPricingConfigurationList() {
        getPricingConfigurations({ wholesalePricebookId: this.wholesalePricebookId,
                                   productList: this.productList,
                                   beginningUI: this.beginningUI,
                                   endingUI: this.endingUI,
                                   fieldNameList: this.fieldNameList,
                                   increaseBy: this.increaseBy,
                                   increaseDollar: this.increaseDollar,
                                   increasePercentage: this.increasePercentage,
                                   roundTo: this.roundTo })
        .then(result => {
            this.updatedPricingConfigurationList = JSON.parse(JSON.stringify(result));
            if (this.updatedPricingConfigurationList.length>0) {
                this.pricingConfigurationsNotFound = false;
            }
            else {
                this.pricingConfigurationsNotFound = true;
            }
            console.log('Updated Pricing Configuration List:', this.updatedPricingConfigurationList);
        })
        .catch(error => {
            console.log('Error Retrieving Pricing Configurations');
            console.log('Error:', error);
        });
    }

    handleApplyClick(event) {

        this.showSpinner = true;

        savePricingConfigurationUpdates({ updateList: this.updatedPricingConfigurationList})
        .then(result => {
            if (result === true) {

                const evt = new ShowToastEvent({
                    message: 'Pricing Configuration(s) updated successfully!',
                    variant: 'success',
                });
                this.dispatchEvent(evt);

                // Navigate to the Wholesale Pricebook home page
                this[NavigationMixin.Navigate]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: this.wholesalePricebookId,
                        actionName: 'view',
                    }
                });

            }
            else {
                const evt = new ShowToastEvent({
                    message: 'Pricing Configuration(s) update failed. Please try again.',
                    variant: 'error',
                });
                this.dispatchEvent(evt);
            }
        })
        .catch(error => {
            console.log('Error Updating Pricing Configurations');
            console.log('Error:', error);
        });

    }

    handleCancelClick(event) {
        // Navigate to the Wholesale Pricebook home page
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.wholesalePricebookId,
                actionName: 'view',
            }
        });
    }

}