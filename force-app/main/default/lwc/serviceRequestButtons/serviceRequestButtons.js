import { LightningElement, api ,track, wire} from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import getOrders from '@salesforce/apex/RMS_LWC_SerReqRelListFromAccount.fetchOrders';
import assetExits from '@salesforce/apex/RMS_LWC_SerReqRelListFromAccount.assetExits';
import saveOrder from '@salesforce/apex/CreateServiceRequestController.saveOrderRecord'
import ServiceConsole from '@salesforce/resourceUrl/ServiceConsole';
import { NavigationMixin } from 'lightning/navigation';
import ServiceRequestImage from '@salesforce/resourceUrl/ServiceRequestIcon'; 

export default class ServiceRequestButtons extends NavigationMixin (LightningElement) {

    @api accid;
    @api storeid;
    @api errormessage;
    @api assetcount; 
    @api accountBalance;
    @api accountBalanceMsg;
    urlvalue ;

    @track draftValues = [];
 
    @track listServReqs = [];
    @track relatedOrders = [];
    @track selectedOrderId = '';
    @track newServReq = false;     
    @track orderId;
    @track servReqId;
    @track mouseX;
    @track mouseY;
    @api recordId;
    @api storeId; 
    @track tableLoadingState = true;
    @track rowOffset = 0;
    @track sortedBy;
    @track sortedDirection = 'asc';
    @track disableNextButton = true;

    @track newServReq = false;

    ServiceReqImage = ServiceRequestImage;

    @wire(getOrders, {accId : '$accid'})
    wiredGetOrders({error, data}) {
        console.log('before if', JSON.stringify(data));
        if (data) {
            console.log('inside date');
            console.log('@@@@@',JSON.stringify(data));
            this.relatedOrders = data;
        } else if (error) {
            console.log('Error: ', error);
        }
    }

    connectedCallback() {
        console.log('@@@accountBalance'+this.accountBalance);
        if(this.accountBalance > 0){
            this.accountBalanceMsg = 'There is an outstanding balance of $'+this.accountBalance;
        }
        
        Promise.all([
            loadScript(this, ServiceConsole),
        ])
            .then(() => {
                this.sforce = window.sforce;
            })
            .catch(error => {
               console.log(error);
            }); 
    }
   
    newServReqBtnClicked(){
        assetExits({accId : this.accid}).
            then(result => {
                console.log('@@@ cont'+result);
                if(!result){
                    //window.open('/apex/RMS_addAssetstoServiceLegacy?id='+this.accid,'_parent');
                    this[NavigationMixin.Navigate]({
                        type: 'standard__component',
                        attributes: {
                            componentName: 'c__AddLegacyAssetWrapper'
                            
                            //objectApiName: 'Account',
                            //actionName: 'home',
                        },
                        state: {
                            "c__aid": this.accid,
                            
                        }
                    });
                }
                else{
                    if (this.assetcount) {
                        //this.urlvalue = '/apex/RMS_createServiceRequestRedirect?aid='+this.accid+'&slid='+this.storeid;
                        this.newServReq = true;
                    } else {
                        this.urlvalue = '/apex/RMS_addAssetstoServiceLegacy?id='+this.accid+'&retURL='+this.accid;
                    }
                    console.log('Into Meth Errmsg'+this.errormessage);
                    
                    if (this.errormessage)  {
                        alert(this.errormessage);
                    } else {
            
                        if(this.sforce.console.isInConsole()) {
                            console.log('Inside console');     
                            this.sforce.console.openPrimaryTab(null,this.urlvalue ,true, 
                                null,null);
                        } else{
                            //window.open(this.urlvalue,'_top');
                            this.newServReq = true;
                        }   
                    }
                }
            })
            .catch(error => {
                console.log(error);
            });
        
       
    }

    newLegacyServReqBtnClicked(){
        if (this.errormessage)  {
            alert(this.errormessage);
        } else {

            if(this.sforce.console.isInConsole()) {
                this.sforce.console.openPrimaryTab(null,'/apex/RMS_createServiceRequestRedirect?aid='+this.accid+'&slid='+this.storeid+'&ServiceType=Legacy' ,true, 
                    null,null);
            } else{
                window.open('/apex/RMS_createServiceRequestRedirect?aid='+this.accid+'&slid='+this.storeid+'&ServiceType=Legacy','_parent');
                //this.newServReq = true;
            }   
        }        
    }

    addAsset(){
        
            if(this.sforce.console.isInConsole()) {
               //this.sforce.console.openPrimaryTab(null,'/apex/RMS_addAssetstoServiceLegacy?id='+this.accid+'&retURL='+this.accid,true, 
                 //   null,null);
                this[NavigationMixin.Navigate]({
                    type: 'standard__component',
                    attributes: {
                        componentName: 'c__AddLegacyAssetWrapper'
                        
                        //objectApiName: 'Account',
                        //actionName: 'home',
                    },
                    state: {
                        "c__aid": this.accid,
                        
                    }
                });
            } else{
                //window.open('/apex/RMS_addAssetstoServiceLegacy?id='+this.accid+'&retURL='+this.accid,'_parent');
                this[NavigationMixin.Navigate]({
                    type: 'standard__component',
                    attributes: {
                        componentName: 'c__AddLegacyAssetWrapper'
                        
                        //objectApiName: 'Account',
                        //actionName: 'home',
                    },
                    state: {
                        "c__aid": this.accid,
                        
                    }
                });
            }
    }

    backButtonClicked(event) {
        this.newServReq = false;
    }

    nextButtonClicked(event) {
        if (this.selectedOrder) {
            if (this.selectedOrder === 'NA'){
                saveOrder({currentOrder:'',accountId : this.accid}).
                    then(result => {
                        console.log('@@@ cont'+result);
                        // window.open('/'+result,'_parent'); 
                        //window.open('/apex/CreateServiceRequest?aid='+this.accid+'&slid='+this.storeid+'&oid='+result+'&isServiceRecordCreated=true','_parent'); 
                        this[NavigationMixin.Navigate]({
                            type: 'standard__component',
                            attributes: {
                                componentName: 'c__CreateServiceRequestWrapper'
                                
                                //objectApiName: 'Account',
                                //actionName: 'home',
                            },
                            state: {
                                "c__aid": this.accid,
                                "c__slid": this.storeid,
                                "c__oid": result,
                                "c__isServiceRecordCreated": "true"
                            }
                        });
                    })
                    .catch(error => {
                        console.log(error);
                    });
            }
            else{
                //window.open('/apex/CreateServiceRequest?aid='+this.accid+'&slid='+this.storeid+'&oid='+this.selectedOrder+'&isServiceRecordCreated=false','_parent');  
                this[NavigationMixin.Navigate]({
                    type: 'standard__component',
                    attributes: {
                        componentName: 'c__CreateServiceRequestWrapper'
                    },
                    state: {
                        "c__aid": this.accid,
                        "c__slid": this.storeid,
                        "c__oid": this.selectedOrder,
                        "c__isServiceRecordCreated": "false"
                    }
                }); 
            }    
        } 
    }

    get orderDropdownOptions() {

        let options = [];
        let orders = this.relatedOrders;

        for (let i = 0; i < orders.length; i++) {

            console.log(orders[i].Installation_Date__c);

            var formattedInstallationDate = null;
            var installationDate = orders[i].Installation_Date__c;
            if (installationDate != null) {
                var installationDateList = installationDate.split('-');
                formattedInstallationDate = parseInt(installationDateList[1],10) + '/' + parseInt(installationDateList[2],10) + '/' + installationDateList[0];
            }

            var orderLabel = orders[i].OrderNumber;
            if (formattedInstallationDate != null) {
                orderLabel += ' | Install: ' + formattedInstallationDate;
            }
            options.push({ label: orderLabel, value: orders[i].Id })

        }

        options.push({label:'Not Listed',value :'NA'});
        console.log('Options: ', options);

        return options;
    }
    
    onOrderOptionChange(event) {
        this.selectedOrder = event.detail.value;
        console.log('Selected Order', this.selectedOrder);
        this.enableNextButton();
    }

    enableNextButton() {
        if (this.selectedOrder === '') {
            this.disableNextButton = true;
        }
        else {
            this.disableNextButton = false;
        }
    }

}