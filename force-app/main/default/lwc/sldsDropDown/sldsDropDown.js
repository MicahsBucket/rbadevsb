import { LightningElement, api, track } from 'lwc';

export default class SldsDropDown extends LightningElement {
    @api itemIcon = 'standard:account';
    @api items;
    @api label;
    @api selectedItem;
    @track displaySelectedItem;
    @api focalPoint; // changed variable name from focus to focalpoint - the term focus was causing a component breaking error. 
    //We think it is related to changes in the Winter '20 release. Mark Rothermal 10/7/19 rfbuild-285

    connectedCallback(){
        this.displaySelectedItem = !!this.selectedItem;
    }

    handleDisplaySearch(event){
        let value = event.currentTarget.dataset.value;
        this.displaySelectedItem = false;
        this.dispatchEvent(new CustomEvent('itemdeselect',{detail : value}));
    }

    handleItemClick(event){
        let value = event.currentTarget.dataset.value;
        let name = event.currentTarget.dataset.name;
        this.selectedItem = {label:name,value:value};
        this.displaySelectedItem = true;
        this.focalPoint = false;
        this.dispatchEvent(new CustomEvent('itemselect',{detail : this.selectedItem}));
    }

    get displayItems(){
        return this.items && this.items.length > 0 && this.focalPoint;
    }

    get displaySelectedItemIfNotBlank(){
        console.log('JWL: selectedItem for ' + this.label + ': ' + this.selectedItem);
        console.log('JWL: displaySelectedItem: ' + this.displaySelectedItem);
        return this.displaySelectedItem && !!this.selectedItem;
    }
}