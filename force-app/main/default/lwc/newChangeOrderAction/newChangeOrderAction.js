/**
* @author Jason Flippen
* @date 01/25/2021 
* @description Provides functionality for the newChangeOrderAction LWC.
*
*			   Supporting database functionality provided by the following Apex Class:
*			   - NewChangeOrderActionController
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/
import { LightningElement, api, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { refreshApex } from '@salesforce/apex';
import { registerListener } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
import getOrderData from '@salesforce/apex/NewChangeOrderActionController.getOrderData';
import createChangeOrder from '@salesforce/apex/NewChangeOrderActionController.createChangeOrder';

export default class NewChangeOrderAction extends NavigationMixin(LightningElement) {
    
    @api recordId;

    wiredOrderResult;
    order;
    displayText = '';

    @wire(CurrentPageReference) pageRef;

    /**
    * @author Jason Flippen
    * @date 01/25/2021
    * @description Event fired when the element is inserted into the document.
    *              It's purpose here is to subscribe the "updateOrderEvent".
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    connectedCallback() {
        registerListener('updateOrderEvent', this.refreshOrderData, this);
    }
    
    /**
    * @author Jason Flippen
    * @date 01/25/2021
    * @description Method to retrieve Order data.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @wire(getOrderData, { orderId: '$recordId' })
    wiredGetOrderData(result) {

        // Hold on to the provisioned value so it can be refreshed.
        this.wiredOrderResult = result;
        const { data, error } = result; // destructure the provisioned value
        if (data) {
            console.log('Order Data', JSON.parse(JSON.stringify(data)));
            this.order = JSON.parse(JSON.stringify(data));

            let disableSave = false;

            // Are we able to create a Change Order from this Order?
            if (this.order.Revenue_Recognized_Date__c == null) {
                disableSave = true;
                this.displayText = 'You cannot create a change order from an order that has not been revenue recognized';
            }
            else {
                this.displayText = 'Are you sure you want to create a new Change Order?';
            }

            if (disableSave === false) {
                const enableSaveEvent = new CustomEvent('EnableSave');
                this.dispatchEvent(enableSaveEvent);
            }
            else {
                const disableSaveEvent = new CustomEvent('DisableSave');
                this.dispatchEvent(disableSaveEvent);
            }

       }
       else if (error) {
           console.log('error in getOrderData callback', error);
       }

    }

    /**
    * @author Jason Flippen
    * @date 01/25/2021
    * @description Method to create a new Change Order.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @api
    handleSaveClick() {

        const showSpinner = new CustomEvent('ShowSpinner');
        const hideSpinner = new CustomEvent('HideSpinner');
        const closeQuickAction = new CustomEvent('CloseQuickAction');

        this.dispatchEvent(showSpinner);

        // Call Apex method to update the Order.
        createChangeOrder({ order: this.order })
        .then((result) => {

            var resultMap = result;

            var returnValue = '';
            var saveSuccess = false;
            for (var key in resultMap) {
                returnValue = key;
                if (key === 'New Change Order Success') {
                    returnValue = resultMap[key];
                    saveSuccess = true;
                }
                break;
            }
            console.log('Return Value', returnValue);

            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(closeQuickAction);    

            if (saveSuccess === true) {

                // Navigate to the new Cost Purchase Order.
                this[NavigationMixin.Navigate]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: returnValue,
                        objectApiName: 'Order', // objectApiName is optional
                        actionName: 'view'
                    }
                });

            }
            else  {

                const toastMessage = returnValue;
                const toastType = 'error';
                const showToastEvent = new CustomEvent('ShowToast', { detail: { message: toastMessage, type: toastType } });
                this.dispatchEvent(showToastEvent);

            }

        })
        .catch((error) => {
            console.log('Create Change Order Error', error);
            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(closeQuickAction);
        });

    }

    /**
    * @author Jason Flippen
    * @date 01/25/2021
    * @description Method to refresh the cache.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    refreshOrderData() {
        console.log('Refreshing newChangeOrderAction');
        return refreshApex(this.wiredOrderResult);
    }

}