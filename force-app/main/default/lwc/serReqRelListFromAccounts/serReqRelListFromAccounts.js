import { LightningElement, wire, track, api } from 'lwc';
import getServiceReqs from '@salesforce/apex/RMS_LWC_SerReqRelListFromAccount.getServiceReqs';
import assetsExist from '@salesforce/apex/RMS_LWC_SerReqRelListFromAccount.assetExits';
import allValidationsCheck from '@salesforce/apex/RMS_LWC_SerReqRelListFromAccount.allValidationsCheck';

const columns = [
    {label: 'Request Number', fieldName: 'RequestNumberURL', type: "url", typeAttributes: {label: { fieldName: 'RequestNumber' }, target: '_blank'}, sortable: true},
    {label: 'Status', fieldName: 'Status', type: 'Text', sortable: true},
    {label: 'Start Date', fieldName: 'EffectiveDate', type: 'date-local', sortable: true},
    {label: 'Service Type', fieldName: 'ServiceType', type: 'Text', sortable: true},
    {label: 'Amount Due', fieldName: 'AmountDue', type: 'currency', cellAttributes: { alignment: 'left' }, sortable: true},
    {label: 'Description', fieldName: 'Description', type: 'Text', sortable: true}        
    ];

export default class serReqRelListFromAccounts extends  LightningElement {
    
    @api recordId;
    @api storeId;
    @api accountBalance;
    @api recordPageUrl;

    @track title = '';
    @track showData = false;
    @track listServReqs = [];
    @track columns = columns;
    @track sortedBy;
    @track sortedDirection = 'asc';
    @track tableLoadingState = true;
    @track assetExist = false;
    @track errorMessage;

    connectedCallback(){
        this.getServiceRequests();
    }

    // @wire(getServiceReqs,{pAccId : '$recordId'})
    // wiredServiceReqsMethod({error, data}) {
    //     if (data) {
    //         this.listServReqs = data;
    //         this.title = 'Service Requests (' + this.listServReqs.length + ')';
    //         if (this.listServReqs.length > 0) {
    //             this.showData = true;
    //         }
    //     }
    //     else if (error) {
    //         this.error = error;
    //     }
    //     //this.tableLoadingState = false;
    //     console.log('listServReqs ' +JSON.stringify(data));
    // }
    getServiceRequests(){
        getServiceReqs({pAccId : this.recordId, communityURL: this.recordPageUrl})
        .then(data => {
            if (data) {
                this.listServReqs = data;
                this.title = 'Service Requests (' + this.listServReqs.length + ')';
                if (this.listServReqs.length > 0) {
                    this.showData = true;
                }
            }
            else if (error) {
                this.error = error;
            }
            //this.tableLoadingState = false;
            console.log('listServReqs ' +JSON.stringify(data));
        })
        .catch(error => {
            this.error = error;
        })
    }
   

    @wire(assetsExist, {pAccId : '$recordId'})
    handleassetsExist({error, data}) {
        if(data){
            this.assetExist = data;
        }  else if (error) {
            this.error = error;
        }
    }

    @wire(allValidationsCheck, {pAccId : '$recordId'})
    handleallValidationsCheck({error, data}) {
        if(data){
            this.errorMessage = data;
        }  else if (error) {
            this.error = error;
        }
    }

    sortData(fieldName, sortDirection){
        var data = JSON.parse(JSON.stringify(this.listServReqs));
        
        var key =(a) => a[fieldName];
        var reverse = sortDirection === 'asc' ? 1: -1;
        data.sort((a,b) => {
            let valueA = key(a) ? key(a).toLowerCase() : '';
            let valueB = key(b) ? key(b).toLowerCase() : '';
            return reverse * ((valueA > valueB) - (valueB > valueA));
        });

        this.listServReqs = data;
    }

    updateColumnSorting(event){
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.sortData(this.sortedBy,this.sortedDirection);
    }

}