import { LightningElement, api, track, wire } from "lwc";
import { CurrentPageReference, NavigationMixin } from "lightning/navigation";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { updateRecord } from "lightning/uiRecordApi";
import { refreshApex } from "@salesforce/apex";
import { registerListener } from "c/pubsub";
import { fireEvent } from "c/pubsub";
import getOrder from "@salesforce/apex/ServiceReqRecordPageHeaderController.getOrder";
import getServiceTypeOptionMap from "@salesforce/apex/ServiceReqRecordPageHeaderController.getServiceTypeOptionMap";
import getCongaPrintData from "@salesforce/apex/ServiceReqRecordPageHeaderController.getCongaPrintData";
import getShowRejectRequestButton from "@salesforce/apex/ServiceReqRecordPageHeaderController.getShowRejectRequestButton";
import updateStatusCancelled from "@salesforce/apex/ServiceReqRecordPageHeaderController.updateStatusCancelled";
import updateStatusRejected from "@salesforce/apex/ServiceReqRecordPageHeaderController.updateStatusRejected";
import updateServiceType from "@salesforce/apex/ServiceReqRecordPageHeaderController.updateServiceType";
import fetchCharges from "@salesforce/apex/ReimbursementController.getCharges";
import saveServiceSymptoms from "@salesforce/apex/ReimbursementController.saveServiceSymptoms";
import deleteServiceSymptoms from "@salesforce/apex/ReimbursementController.deleteServiceSymptoms";
import fetchAllSymptomNames from "@salesforce/apex/ReimbursementController.getAllSymptoms";
import fetchAllProcedures from "@salesforce/apex/ReimbursementController.getProcedureList";
import saveAll from "@salesforce/apex/ReimbursementController.saveAll";
import getReimbursableParts from "@salesforce/apex/ReimbursementController.getReimbursableParts";
import getReimbursablePartDetail from "@salesforce/apex/ReimbursementController.getReimbursablePartDetail";
import deleteReimbursablePart from "@salesforce/apex/ReimbursementController.deleteReimbursablePart";
import deleteSFRecords from "@salesforce/apex/ReimbursementController.deleteAllRecords";

const newReimbursablePartColumns = [
	{ label: "Product", fieldName: "productName", type: "string", initialWidth: 175, cellAttributes: { alignment: "left" }, wrapText: true },
	{ label: "Part", fieldName: "name", type: "string", initialWidth: 175, cellAttributes: { alignment: "left" }, wrapText: true },
	{ label: "Description", fieldName: "description", type: "string", cellAttributes: { alignment: "left" }, wrapText: true },
	{ label: "Quantity", fieldName: "quantity", type: "number", initialWidth: 75, cellAttributes: { alignment: "center" }, hideDefaultActions: true },
	{ label: "Reason", fieldName: "reason", type: "string", initialWidth: 150, cellAttributes: { alignment: "left" }, wrapText: true },
	{ label: "", type: "button", fixedWidth: 100, typeAttributes: { label: "Remove", name: "remove" }  }
];

export default class ServiceRequestRecordPageHeader extends NavigationMixin(LightningElement) {

	@api recordId;
	@api currentStep = "1";

	wiredOrderResult;

	@track order;
	@track eligibleForReimbursement = true;
	@track cancelEligible;
	@track orderAccountId = "";
	@track orderName = "";
	@track orderNumber = "";
	@track saveReason = "";
	@track secondarySaveReason = "";
	@track serviceType = "";
	@track soldOrder = "";
	@track soldOrderJIPEligible;
	@track status = "";
	@track serviceTypeOptionList = [];
	@track sourceIsCommunity = false;
    @track baseUrl = '';
	@track showRejectRequestButton = false;
	@track showCancelRequestButton = false;
	@track orderRequired = false;
	@track saveReasonRequired = false;
	@track disableModalSave = true;
	@track showModal = false;
	@track showSpinner = false;
	@track showModalSpinner = false;
	@track showReimburementModal = false;
	@track charges = [];
	@track showEligibleProducts = true;
	@track showAddProblemSection = false;
	@track showAddSymptomSection = false;
	@track showProcedureSection = false;
	@track showPartsReimbursementSection = false;
	@track showConfirmSection = false;
	@track nextButtonLabel = "Continue";
	@track backButtonLabel = "Back";
	@track serviceSymptoms = [];
	@track serviceSymptomsIds = [];
	@track serviceSymptomOptions = [];
	@track problems = [];
	@track procedures = [];
	@track disableButton = true;
	@track isFieldsReadOnly = false;
	@track hideReimburementFeatures = false;

/*
	@track disableReimbursablePartsSaveButton = true;
	@track disableReimbursablePartsCancelButton = false;
	@track showNewPartsReimbursementModal = false;
*/
	noPartsAdded = true;
	newReimbursablePartColumnList = newReimbursablePartColumns;
	newReimbursablePartList = [];
	newReimbursablePart = {
		id: "",
		orderProductId: "",
		productName: "",
		quantity: 1,
		reason: ""
	};
	defaultPartQuantity = 1;
	productList = [];
	defaultSelectedProduct = "";

	@wire(CurrentPageReference) pageRef;

	get options1() {
		return [
			{ label: "New", value: "new" },
			{ label: "In Progress", value: "inProgress" },
			{ label: "Finished", value: "finished" }
		];
	}

	steps = [
		{ label: "Select Service Product", value: "1" },
		{ label: "Add Service Symptoms", value: "2" },
		{ label: "Add Problem Components ", value: "3" },
		{ label: "Add Procedure Codes", value: "4" },
		{ label: "Add Reimbursable Parts", value: "5" },
		{ label: "Confirm Details", value: "6" }
	];

	/*
	* @author Jason Flippen
	* @date 02/18/2021
	* @description Event fired when the element is inserted into the document.
	*              It's purpose here is to subscribe the "updateOrderEvent".
	*/
	connectedCallback() {
		registerListener("updateOrderEvent", this.refreshOrderData, this);
	}

	@wire(getOrder, { orderId: "$recordId" })
	wiredGetOrder(result) {

		// Hold on to the provisioned value so it can be refreshed.
		this.wiredOrderResult = result;
		const { data, error } = result; // destructure the provisioned value
		if (data) {
			console.log("Order", JSON.parse(JSON.stringify(data)));
			this.order = JSON.parse(JSON.stringify(data));

			this.cancelEligible = this.order.cancelEligible;
			this.eligibleForReimbursement = this.order.eligibleForReimbursement;
			this.soldOrderJIPEligible = this.order.soldOrderJIPEligible;
			this.isFieldsReadOnly = false;

			if (this.order.name != null && this.order.name !== undefined) {
				this.orderName = this.order.name;
			}
			console.log("Order Name:", this.orderName);
			if (this.order.orderNumber != null && this.order.orderNumber !== undefined) {
				this.orderNumber = this.order.orderNumber;
			}
			console.log("Order Number:", this.orderNumber);
			if (this.order.orderAccountId != null && this.order.orderAccountId !== undefined) {
				this.orderAccountId = this.order.orderAccountId;
			}
			console.log("Order Account:", this.orderAccountId);
			if (this.order.saveReason != null && this.order.saveReason !== undefined) {
				this.saveReason = this.order.saveReason;
			}
			console.log("Save Reason:", this.saveReason);
			if (this.order.secondarySaveReason != null && this.order.secondarySaveReason !== undefined) {
				this.secondarySaveReason = this.order.secondarySaveReason;
			}
			console.log("Secondary Save Reason:", this.secondarySaveReason);
			if (this.order.serviceType != null && this.order.serviceType !== undefined) {
				this.serviceType = this.order.serviceType;
			}
			console.log("Service Type:", this.serviceType);
			if (this.order.soldOrderId != null && this.order.soldOrderId != undefined) {
				this.soldOrder = this.order.soldOrderId;
			}
			console.log("Sold Order:", this.soldOrder);
			if (this.order.status != null && this.order.status !== undefined) {
				this.status = this.order.status;
				let readOnlyStatus = ["Warranty Submitted","Warranty Accepted","Cancelled","Closed"	];
				if (readOnlyStatus.indexOf(this.status) > -1) {
					this.isFieldsReadOnly = true;
				}
			}
			console.log("Status:", this.status);

			this.fetchReimbursableParts();
			this.setServiceTypeOptionList();
			this.setCongaPrintData();
			this.setShowCancelRequestButton();
			this.setShowRejectRequestButton();

		}
		else if (error) {
			this.error = error;
			console.log("Error in GetOrder callback", error);
		}

	}

	fetchReimbursableParts() {

		this.newReimbursablePartList = [];
		getReimbursableParts({ orderId: this.recordId })
		.then((result) => {
			this.newReimbursablePartList = result;
			console.log("New Reimbursable Part List", this.newReimbursablePartList);
			this.updateNoPartsAddedFlag();
		})
		.catch((error) => {
			console.log("getReimbursableParts Error", error);
		});

	}

	fetchAllProceduresLookUp() {
		fetchAllProcedures({ symptomJSON: JSON.stringify(this.problems) })
		.then((result) => {
			result.forEach((el) => {
				let oldObj = this.procedures.find((chargeObj) => chargeObj.chargeId === el.chargeId);
				if (!(typeof oldObj != "undefined" && oldObj != null)) {
					this.procedures.push(Object.assign({}, el));
				}
				else {
					oldObj.procedures = el.procedures;
				}
			});
			console.log("this.procedures: ", this.procedures);
			this.showAddProblemSection = false;
			this.showEligibleProducts = false;
			this.showAddSymptomSection = false;
			this.backButtonLabel = "Back";
			this.showProcedureSection = true;
			this.showPartsReimbursementSection = false;
			this.showConfirmSection = false;
			this.showModalSpinner = false;
		})
		.catch((error) => {
			console.log("Error", error);
			this.showModalSpinner = false;
		});
	}

	/*
	* @author Jason Flippen
	* @date 08/24/2020
	* @description Method to handle the Reimbursable Part row action click event.
	*/
	handleReimbursablePartRowAction(event) {
		var actionName = event.detail.action.name;
		var row = event.detail.row;
		console.log("Stringify Row", JSON.parse(JSON.stringify(row)));

		switch (actionName) {
			case "remove":
				this.handleRemoveReimbursablePartClick(row);
				break;
			default:
		}

	}


/** Click Events **/


	handleEditClick() {

		// Opens the Order record modal to edit record.
		this[NavigationMixin.Navigate]({
			type: "standard__recordPage",
			attributes: {
				recordId: this.recordId,
				objectApiName: "Order",
				actionName: "edit"
			}
		});

	}

	handleUpdateServiceTypeClick() {
		this.setDisableModalSave();
		this.showModal = true;
	}

	handleCancelOrderClick() {
		this.refreshOrderData();

		if (this.showCancelRequestButton === true) {

			this.showSpinner = true;

			updateStatusCancelled({ orderId: this.recordId })
			.then((result) => {

				// Workaround way to refresh the record page.
				updateRecord({ fields: { Id: this.recordId } });

				if (result === "Success") {
					this.dispatchEvent(
						new ShowToastEvent({
							message: "Service Request " + this.orderNumber + " Updated",
							variant: "success"
						})
					);
				}
				else {
					console.log("Apex Error:", result);
					this.dispatchEvent(
						new ShowToastEvent({
							message: result,
							variant: "error",
							mode: "sticky"
						})
					);
				}
				this.showSpinner = false;

				// Fire event to refresh the cache.
				fireEvent(this.pageRef, "updateOrderEvent", "");

			})
			.catch((error) => {
				console.log("Error", error);
				this.showSpinner = false;
			});
		}
		else {

			const toastEvent = new ShowToastEvent({
				message: "Order cannot be Cancelled",
				variant: "info"
			});
			this.dispatchEvent(toastEvent);
		}

	}

	handleRejectOrderClick() {

		this.refreshOrderData();

		if (this.showRejectRequestButton === true) {

			this.showSpinner = true;

			updateStatusRejected({ orderId: this.recordId })
			.then((result) => {

				// Workaround way to refresh the record page.
				updateRecord({ fields: { Id: this.recordId } });

				if (result === "Success") {
					this.dispatchEvent(
						new ShowToastEvent({
							message: "Service Request " + this.orderNumber + " Updated",
							variant: "success"
						})
					);
				}
				else {
					console.log("Apex Error:", result);
					this.dispatchEvent(
						new ShowToastEvent({
							message: "Service Request update failed. Please try again.",
							variant: "error",
							mode: "sticky"
						})
					);
				}
				this.showSpinner = false;
				
				// Fire event to refresh the cache.
				fireEvent(this.pageRef, "updateOrderEvent", "");

			})
			.catch((error) => {
				console.log("Error", error);
				this.showSpinner = false;
			});
		}
		else {
			const toastEvent = new ShowToastEvent({
				message: "Order cannot be Rejected",
				variant: "info"
			});
			this.dispatchEvent(toastEvent);
		}

	}

	handleCancelModelClick() {
		this.showModal = false;
	}

	handleSaveServiceTypeClick() {

		let updateOrder;

		this.showModalSpinner = true;

		if (this.orderRequired === true) {
			updateOrder = {
				Id: this.recordId,
				Service_Type__c: this.serviceType,
				Sold_Order__c: this.soldOrder,
				Save_Reason__c: "",
				Secondary_Save_Reason__c: ""
			};
		}
		else if (this.saveReasonRequired === true) {
			updateOrder = {
				Id: this.recordId,
				Service_Type__c: this.serviceType,
				Save_Reason__c: this.saveReason,
				Secondary_Save_Reason__c: this.secondarySaveReason
			};
		}
		else {
			updateOrder = {
				Id: this.recordId,
				Service_Type__c: this.serviceType,
				Save_Reason__c: "",
				Secondary_Save_Reason__c: ""
			};
		}

		console.log("Update Order", updateOrder);

		updateServiceType({ updateOrder: updateOrder })
		.then((result) => {

			// Workaround way to refresh the record page.
			updateRecord({ fields: { Id: this.recordId } });

			if (result === "Success") {
				this.dispatchEvent(
					new ShowToastEvent({
						message: "Service Request Type Updated",
						variant: "success"
					})
				);
			}
			else {
				console.log("Apex Error:", result);
				this.dispatchEvent(
					new ShowToastEvent({
						message: "Service Request Type update failed. Please try again.",
						variant: "error"
					})
				);
			}
			this.showModalSpinner = false;
			this.showModal = false;

			// Fire event to refresh the cache.
			fireEvent(this.pageRef, "updateOrderEvent", "");

		})
		.catch((error) => {
			console.log("Error", error);
			this.dispatchEvent(
				new ShowToastEvent({
					message: "Service Request Type update failed. Please try again.",
					variant: "error"
				})
			);
			this.showModalSpinner = false;
			this.showModal = false;
		});

	}

	handleReimbursementClick(event) {

		this.refreshOrderData();

		// Display the Reimbursement modal window if this
		// Service Request is eligible for reimbursement.
		if (this.eligibleForReimbursement === true) {

			this.showReimburementModal = true;
			this.disableButton = true;
			let serSymKey;
			this.charges = [];

			this.productList = [
				{ label: "--None--", value: "" }
			];

			fetchCharges({ orderId: this.recordId })
			.then((result) => {
				result.forEach((el) => {
					this.charges.push(Object.assign({}, el));
				});
				console.log(result);
				for (let i in this.charges) {
					if (this.charges[i].iconPath === "utility:check") {
						this.disableButton = false;
						this.serviceSymptomsIds.push(this.charges[i].selectedSymptom);
						let productLabel = this.charges[i].productName + " | " + this.charges[i].variantNumber;
						this.productList.push({label: productLabel, value: this.charges[i].orderProductId});
					}
				}
			})
			.catch((error) => {
				this.showModalSpinner = false;
			});

			fetchAllSymptomNames({})
			.then((result) => {
				for (serSymKey in result) {
					if (result[serSymKey].Id !== null && result[serSymKey].Id !== "") {
						let opt = {label: result[serSymKey].Name,value: result[serSymKey].Id};
						this.serviceSymptomOptions.push(opt);
					}
				}
			})
			.catch((error) => {
				console.log("Error", error);
				this.showModalSpinner = false;
			});

			this.showModalSpinner = false;

		}
		else {

			const toastEvent = new ShowToastEvent({
				message: "Current date exceeds submission timeframe allowed to submit this Service Request for Warranty",
				variant: "info"
			});
			this.dispatchEvent(toastEvent);

		}

	}

	handleReimburementCancelModelClick(event) {
		console.log("closing");
		this.deleteNewServiceSymptoms();
		this.showReimburementModal = false;
		this.showEligibleProducts = true;
		this.showAddProblemSection = false;
		this.showAddSymptomSection = false;
		this.showProcedureSection = false;
		this.showPartsReimbursementSection = false;
		this.showConfirmSection = false;
		this.backButtonLabel = "Back";
		this.nextButtonLabel = "Continue";
		this.currentStep = "1";
		this.charges = [];
		this.serviceSymptoms = [];
		this.problems = [];
		this.procedures = [];
		this.fetchReimbursableParts();
	}

	/*
	* @auther Jason Flippen
	* @date 08/24/2020
	* @description Method to add Part to newReimbursablePartList.
	*/
	handleAddReimbursablePartClick(event) {

		if (this.newReimbursablePart.id) {

			getReimbursablePartDetail({ partId: this.newReimbursablePart.id })
			.then((result) => {
				console.log("Result", JSON.parse(JSON.stringify(result)));
				const newReimbursablePartDetail = JSON.parse(JSON.stringify(result));
				console.log("New Reimbursable Part Detail", newReimbursablePartDetail);

				// Update our New Part List variable.
				this.newReimbursablePartList = [...this.newReimbursablePartList, {id: this.newReimbursablePart.id,
																				  description: newReimbursablePartDetail.description,
																				  isNew: true,
																				  name: newReimbursablePartDetail.name,
																				  orderProductId: this.newReimbursablePart.orderProductId,
																				  productName: this.newReimbursablePart.productName,
																				  quantity: this.newReimbursablePart.quantity,
																				  reason: this.newReimbursablePart.reason,
																				  unitPrice: newReimbursablePartDetail.unitPrice}];
				
				// Reset the data-entry fields.
				this.newReimbursablePart.id = "";
				this.newReimbursablePart.orderProductId = "";
				this.newReimbursablePart.productName = "";
				this.newReimbursablePart.quantity = 1;
				this.newReimbursablePart.reason = "";
				this.template.querySelector('lightning-combobox[data-field="partProductComponent"]').value = "";
				this.template.querySelectorAll('lightning-input-field[data-id="reset"]').forEach((field) => {
					field.reset();
				});
				this.template.querySelector('lightning-input-field[data-field="partQuantityComponent"]').value = 1;

				this.updateNoPartsAddedFlag();
			})
			.catch((error) => {
				console.log("handleAddReimbursablePartClick Error", error);
			});
		}

	}

	/*
	* @author Jason Flippen
	* @date 08/24/2020
	* @description Method to handle the Reimbursable Part "Remove" click event.
	*/
	handleRemoveReimbursablePartClick(row) {

		const reimbursablePartId = row.id;
		console.log("Reimbursable Part Id", reimbursablePartId);

		// Grab a copy of the newReimbursablePartList array and clear it.
		let originalPartList = this.newReimbursablePartList;
		this.newReimbursablePartList = [];

		// Iterate through the "original" Reimbursable Part List
		// and build an "updated" List of Parts to be added back
		// to the newReimbursablePartList variable.
		let updatedPartList = [];
		originalPartList.forEach(function (item, index) {
			let part = JSON.parse(JSON.stringify(item));
			if (part.id === reimbursablePartId && part.isNew === false) {

				// Delete Reimbursable Part from the database.
				deleteReimbursablePart({ reimbursablePartId: reimbursablePartId })
				.then((result) => {
					console.log("Delete ReimbursablePart Success");
					
					// Workaround way to refresh the record page.
					updateRecord({ fields: { Id: this.recordId } });

				})
				.catch((error) => {
					console.log("Error in deleteReimbursablePart", error);
				});
			}
			else if (part.id !== reimbursablePartId) {
				updatedPartList.push(part);
			}
		});
		console.log("updatedPartList", updatedPartList);

		this.newReimbursablePartList = updatedPartList;

		this.updateNoPartsAddedFlag();

	}

	deleteRecordsClick(event) {
		let confirmRes = confirm("You are deleting all of the warranty records for this product (not the product itself). Are you sure?");
		if (confirmRes) {
			this.showModalSpinner = true;
			deleteSFRecords({orderId: this.recordId, orderProductId: event.target.value	})
			.then((result) => {
				this.handleReimbursementClick(event);
				this.showModalSpinner = false;

				// Fire event to refresh the cache.
				fireEvent(this.pageRef, "updateOrderEvent", "");
				
			})
			.catch((error) => {
				this.showModalSpinner = false;
			});
		}
	}

	selectedChargeClick(event) {
		console.log(event.target.value);
		let charge;
		this.disableButton = true;
		charge = this.charges.find((chargeObj) => chargeObj.chargeId === event.target.value);
		if (charge.iconPath === "utility:add") {
			charge.iconPath = "utility:check";
			charge.variant = "brand";
			let productLabel = charge.productName + " | " + charge.variantNumber;
			this.productList.push({label: productLabel,value: charge.orderProductId});
			charge.isNew = true;
		}
		else if (charge.iconPath === "utility:check") {
			this.charges.find((chargeObj) => chargeObj.chargeId === event.target.value).iconPath = "utility:add";
			this.charges.find((chargeObj) => chargeObj.chargeId === event.target.value).variant = "warning";

			// Grab a copy of the productList array and clear it.
			let originalProductList = this.productList;
			this.productList = [];

			// Iterate through the "original" Product List and
			// build an "updated" List of Products to be added
			// back to the productList variable.
			let updatedProductList = [];
			originalProductList.forEach(function (item, index) {
				if (item.value !== charge.orderProductId) {
					updatedProductList.push(item);
				}
			});
			console.log("updatedProductList", updatedProductList);

			this.productList = updatedProductList;
		}

		this.serviceSymptomsIds.push(charge.selectedSymptom);
		for (let i in this.charges) {
			if (this.charges[i].iconPath === "utility:check") {
				this.disableButton = false;
				break;
			}
		}

	}

	nextClickHandler(event) {

		this.showModalSpinner = true;
		if (this.isFieldsReadOnly != true) {
			this.disableButton = false;
		}
		const allValid = [...this.template.querySelectorAll(".requiredField")]
		.reduce((validSoFar, inputCmp) => {
			inputCmp.reportValidity();
			return validSoFar && inputCmp.checkValidity();
		}, true);

		if (!allValid) {
			this.showModalSpinner = false;
			return;
		}

		this.currentStep = parseInt(this.currentStep, 10) + 1;
		this.currentStep = this.currentStep.toString();
		console.log("Current Step", this.currentStep);

		if (this.currentStep === "1") {

			this.showEligibleProducts = true;

			this.showAddProblemSection = false;
			this.showAddSymptomSection = false;
			this.showProcedureSection = false;
			this.showPartsReimbursementSection = false;
			this.showConfirmSection = false;
			this.backButtonLabel = "Back";
			this.nextButtonLabel = "Continue";
			this.showModalSpinner = false;

		}
		else if (this.currentStep === "2") {

			this.showAddSymptomSection = true;

			this.showEligibleProducts = false;
			this.showAddProblemSection = false;
			this.showProcedureSection = false;
			this.showPartsReimbursementSection = false;
			this.showConfirmSection = false;
			this.backButtonLabel = "Back";
			this.nextButtonLabel = "Continue";
			this.showServiceSymptom();

		}
		else if (this.currentStep === "3") {

			console.log("this.serviceSymptomsIds");
			console.log(JSON.stringify(this.serviceSymptomsIds));
			console.log(JSON.stringify(this.problems));
			console.log(JSON.stringify(this.serviceSymptoms));
			saveServiceSymptoms({symptomJSON: JSON.stringify(this.serviceSymptoms), symptomIdList: this.serviceSymptomsIds})
			.then((result) => {
				console.log(result);
				result.forEach((el) => {
					let oldObj = this.problems.find((chargeObj) => chargeObj.chargeId === el.chargeId);
					if (!(typeof oldObj != "undefined" && oldObj != null)) {
						this.problems.push(Object.assign({}, el));
					}
					else {
						oldObj.problems = el.problems;
					}

					for (let index in this.charges) {
						if (this.charges[index].chargeId === el.chargeId) {
							this.charges[index].symptomId = el.symptomId;
						}
					}

				});
				console.log(this.problems);
				this.showModalSpinner = false;
			})
			.catch((error) => {
				console.log("Error", error);
			});

			this.showAddProblemSection = true;

			this.showEligibleProducts = false;
			this.showAddSymptomSection = false;
			this.showProcedureSection = false;
			this.showPartsReimbursementSection = false;
			this.showConfirmSection = false;
			this.backButtonLabel = "Back";
			this.nextButtonLabel = "Continue";

		}
		else if (this.currentStep === "4") {
			this.nextButtonLabel = "Continue";
			this.fetchAllProceduresLookUp();
		}
		else if (this.currentStep === "5") {

			this.showPartsReimbursementSection = true;

			this.showAddProblemSection = false;
			this.showEligibleProducts = false;
			this.showAddSymptomSection = false;
			this.showProcedureSection = false;
			this.showConfirmSection = false;
			this.backButtonLabel = "Back";
			this.nextButtonLabel = "Continue";
			this.showModalSpinner = false;
			if (this.order.status != "Warranty Submitted" && this.order.status != "Warranty Accepted" && this.order.status != "Closed") {
				this.hideReimburementFeatures = false;
			}
			else {
				this.hideReimburementFeatures = true;
				// this.disableButton = true;
			}
		}
		else if (this.currentStep === "6") {

			this.showConfirmSection = true;

			this.showAddProblemSection = false;
			this.showEligibleProducts = false;
			this.showAddSymptomSection = false;
			this.showProcedureSection = false;
			this.showPartsReimbursementSection = false;
			this.backButtonLabel = "Back";
			this.nextButtonLabel = "Finish";
			this.showModalSpinner = false;

			if (this.isFieldsReadOnly === true) {
				this.disableButton = true;
			}

		}
		else if (this.currentStep === "7") {

			this.currentStep = "6";
			console.log("@@@@ Problems" + JSON.stringify(this.procedures));
			saveAll({symptomJSON: JSON.stringify(this.procedures), newPartList: this.newReimbursablePartList,orderId: this.recordId	})
			.then((result) => {
				//this.problems = result;
				this.showModalSpinner = false;
//                window.location = '/' + this.recordId;

				// Navigate to the Service Request.
				this[NavigationMixin.Navigate]({
					type: "standard__recordPage",
					attributes: {
						recordId: this.recordId,
						objectApiName: "Order", // objectApiName is optional
						actionName: "view"
					}
				});

			})
			.catch((error) => {
				console.log("Error", error);
			});
		}

	}

	previousClickHandler(event) {

		this.showModalSpinner = true;
		this.disableButton = false;
		this.currentStep = parseInt(this.currentStep, 10) - 1;
		this.currentStep = this.currentStep.toString();
		console.log("Current Step", this.currentStep);

		if (this.currentStep === "0") {
			this.handleReimburementCancelModelClick(event);
			this.showModalSpinner = false;
		}
		else if (this.currentStep === "1") {

			this.showEligibleProducts = true;
			
			this.showAddProblemSection = false;
			this.showAddSymptomSection = false;
			this.showProcedureSection = false;
			this.showPartsReimbursementSection = false;
			this.showConfirmSection = false;
			this.backButtonLabel = "Back";
			this.nextButtonLabel = "Continue";
			this.showModalSpinner = false;

		}
		else if (this.currentStep === "2") {

			this.deleteNewServiceSymptoms();

			this.showAddSymptomSection = true;

			this.showEligibleProducts = false;
			this.showAddProblemSection = false;
			this.showProcedureSection = false;
			this.showPartsReimbursementSection = false;
			this.showConfirmSection = false;
			this.backButtonLabel = "Back";
			this.nextButtonLabel = "Continue";

		}
		else if (this.currentStep === "3") {

			this.showAddProblemSection = true;

			this.showEligibleProducts = false;
			this.showAddSymptomSection = false;
			this.showProcedureSection = false;
			this.showPartsReimbursementSection = false;
			this.showConfirmSection = false;
			this.backButtonLabel = "Back";
			this.nextButtonLabel = "Continue";
			this.showModalSpinner = false;

		}
		else if (this.currentStep === "4") {

			this.showProcedureSection = true;

			this.showAddProblemSection = false;
			this.showEligibleProducts = false;
			this.showAddSymptomSection = false;
			this.showPartsReimbursementSection = false;
			this.showConfirmSection = false;
			this.backButtonLabel = "Back";
			this.nextButtonLabel = "Continue";
			this.showModalSpinner = false;

		}
		else if (this.currentStep === "5") {

			this.showPartsReimbursementSection = true;

			this.showAddProblemSection = false;
			this.showEligibleProducts = false;
			this.showAddSymptomSection = false;
			this.showProcedureSection = false;
			this.showConfirmSection = false;
			this.backButtonLabel = "Back";
			this.nextButtonLabel = "Continue";
			this.showModalSpinner = false;

		}
		else if (this.currentStep === "6") {

			this.showConfirmSection = true;

			this.showAddProblemSection = false;
			this.showEligibleProducts = false;
			this.showAddSymptomSection = false;
			this.showProcedureSection = false;
			this.showPartsReimbursementSection = false;
			this.backButtonLabel = "Back";
			this.nextButtonLabel = "Finish";
			this.showModalSpinner = false;

		}

	}


/** Change Events **/


	handleServiceTypeChange(event) {
		console.log("Service Type Value", event.target.value);
		this.serviceType = event.target.value;
		this.setDisableModalSave();
	}

	handleSoldOrderChange(event) {
		console.log("Sold Order Value", event.target.value);
		this.soldOrder = event.target.value;
		this.setDisableModalSave();
	}

	handleSaveReasonChange(event) {
		console.log("Save Reason Value", event.target.value);
		this.saveReason = event.target.value;
		this.setDisableModalSave();
	}

	handleSecondarySaveReasonChange(event) {
		console.log("Secondary Save Reason Value", event.target.value);
		this.secondarySaveReason = event.target.value;
		this.setDisableModalSave();
	}

	/*
	* @auther Jason Flippen
	* @date 12/29/2020
	* @description Method to handle the Product "Change" event
	*/
	handleProductChange(event) {

		console.log("Selected Product Id", event.detail.value);
		console.log("Selected Product Name", event.target.options.find((opt) => opt.value === event.detail.value).label);

		if (event.detail.value) {
			this.newReimbursablePart.orderProductId = event.detail.value;
			this.newReimbursablePart.productName = event.target.options.find((opt) => opt.value === event.detail.value).label;
		}
			console.log("newReimbursablePart.orderProductId", this.newReimbursablePart.orderProductId);
			console.log("newReimbursablePart.productName", this.newReimbursablePart.productName);
	}

	/*
	* @auther Jason Flippen
	* @date 08/24/2020
	* @description Method to handle the Reimbursable Part "Change" event
	*/
	handleReimbursablePartChange(event) {

		console.log("Part Id", event.detail.value[0]);
		this.newReimbursablePart.id = "";
		if (event.detail.value[0]) {
			this.newReimbursablePart.id = event.detail.value[0];
		}
		console.log("newReimbursablePart Id", this.newReimbursablePart.id);

	}

	/*
	* @auther Jason Flippen
	* @date 10/22/2020
	* @description Method to handle the Reimbursable Part Reason "Change" event
	*/
	handleReimbursablePartReasonChange(event) {

		console.log("Reason", event.detail.value);
		this.newReimbursablePart.reason = "";
		if (event.detail.value) {
			this.newReimbursablePart.reason = event.detail.value;
		}
		console.log("newReimbursablePart reason", this.newReimbursablePart.reason);

	}

	/*
	* @auther Jason Flippen
	* @date 08/24/2020
	* @description Method to handle the Reimbursable Part Quantity "Change" event
	*/
	handleReimbursablePartQuantityChange(event) {

		const partQuantityValue = event.detail.value;
		console.log("Quantity", partQuantityValue);

		if (partQuantityValue) {

			if (partQuantityValue > 5) {
				this.newReimbursablePart.quantity = 5;
				this.template.querySelector('lightning-input-field[data-field="partQuantityComponent"]'	).value = 5;
			}
			else if (partQuantityValue == 0) {
				this.newReimbursablePart.quantity = 1;
			}
			else {
				this.newReimbursablePart.quantity = partQuantityValue;
			}

		}
		else {
			this.newReimbursablePart.quantity = 1;
		}
		console.log("newReimbursablePart quantity", this.newReimbursablePart.quantity);

	}


/** Conga Button Click Events **/


	handlePrintServiceInvoiceClick() {
		const targetURL = "/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&id=" + this.recordId + "&QueryId=[StoreConfig]a2261000001FMWZ,[OrderswithProductsV2]a2261000001FMWe,[Payments]a2261000001FMWo,[BusAdjs]a2261000001FMWt,[Refunds]a2261000001FMWy,[Discounts]a2261000001FMWj&TemplateId=a2A61000000iaT6,a2A61000001x03x&DS7=3&DefaultPDF=1&AC0=1&SC0=1&SC1=Attachments";
		this.navigateToWebPage(targetURL);
	}

	handlePrintServiceQuoteClick() {
		let currentDate = new Date();
		const day = String(currentDate.getDate()).padStart(2, "0");
		const month = String(currentDate.getMonth() + 1).padStart(2, "0"); //January is 0!
		const year = currentDate.getFullYear();
		currentDate = year + "-" + month + "-" + day;

		const targetURL = "/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&id=" + this.recordId + "&QueryId=[SRwithProducts]a2261000001FMWU,[StoreConfig]a2261000001FMWZ&TemplateId=a2A610000003C1p&OFN=" + this.orderName + "+-+ServiceQuote+-+" + currentDate + "&DS7=3&DefaultPDF=1&AC0=1&SC0=1&SC1=Attachments";
		this.navigateToWebPage(targetURL);
	}

	handlePrintServiceWOClick() {
		const targetURL = "/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&id=" + this.recordId + "&QueryId=[SRwithWOALL]a2261000001FMX3,[AcctwithSO]a2261000001FMVg%3Fpv0%3D" + this.orderAccountId + ",[SRwPO]a2261000001FMX8,[SRwMaterial]a2261000001FMXD,[SRwTasks]a2261000001FMXI,[SRwSoldOrder]a2261000001FMXN,[SRwTasksv2]a2261000001OIQQ,[SRwMaterialv2]a2261000001OIRn,[spHSR]a2261000001OJZT,[Resources]a2261000001OJB3&TemplateId=a2A61000000K87L&AC0=1&AC1=Service+Work+Order+Generated&OFN=Service+Request+" + this.orderNumber + "+HSR+Form&DefaultPDF=1&DS4=1&DS0=1&DS7=3";
		this.navigateToWebPage(targetURL);
	}

	handlePrintInstallWOClick() {
		const targetURL = "/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&id=" + this.recordId + "&QueryId=[SRwInstallRes]a2261000001OJO6,[SRwOrder]a2261000001FMXN,[SRwPO]a2261000001FMX8,[SRwInstallTask]a2261000001OJP9,[SRwInstallMat]a2261000001OJR5,[spHSR]a2261000001FMXS&TemplateId=a2A610000003CmE&AC0=1&AC1=Install+Work+Order+Generated&DefaultPDF=1&DS0=1&DS4=1&DS7=3&OFN=Service+Request+" + this.orderNumber + "+HSR+Form";
		this.navigateToWebPage(targetURL);
	}


// ARO Community Buttons


	handlePrintAROServiceInvoiceClick() {
//		const targetURL = "https://rforce.renewalbyandersen.com/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&id=" + this.recordId + "&QueryId=[StoreConfig]a2261000001FMWZ,[OrderswithProductsV2]a2261000001FMWe,[Payments]a2261000001FMWo,[BusAdjs]a2261000001FMWt,[Refunds]a2261000001FMWy,[Discounts]a2261000001FMWj&TemplateId=a2A61000000iaT6,a2A61000001x03x&DS7=3&DefaultPDF=1&AC0=1&SC0=1&SC1=Attachments";
		const targetURL = this.baseUrl + "/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&id=" + this.recordId + "&QueryId=[StoreConfig]a2261000001FMWZ,[OrderswithProductsV2]a2261000001FMWe,[Payments]a2261000001FMWo,[BusAdjs]a2261000001FMWt,[Refunds]a2261000001FMWy,[Discounts]a2261000001FMWj&TemplateId=a2A61000000iaT6,a2A61000001x03x&DS7=3&DefaultPDF=1&AC0=1&SC0=1&SC1=Attachments";
		this.navigateToWebPage(targetURL);
	}

	handlePrintAROServiceQuoteClick() {
		let currentDate = new Date();
		const day = String(currentDate.getDate()).padStart(2, "0");
		const month = String(currentDate.getMonth() + 1).padStart(2, "0"); //January is 0!
		const year = currentDate.getFullYear();
		currentDate = year + "-" + month + "-" + day;

//		const targetURL = "https://rforce.renewalbyandersen.com/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&id=" + this.recordId + "&QueryId=[SRwithProducts]a2261000001FMWU,[StoreConfig]a2261000001FMWZ&TemplateId=a2A610000003C1p&OFN=" + this.orderName + "+-+ServiceQuote+-+" + currentDate + "&DS7=3&DefaultPDF=1&AC0=1&SC0=1&SC1=Attachments";
		const targetURL = this.baseUrl + "/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&id=" + this.recordId + "&QueryId=[SRwithProducts]a2261000001FMWU,[StoreConfig]a2261000001FMWZ&TemplateId=a2A610000003C1p&OFN=" + this.orderName + "+-+ServiceQuote+-+" + currentDate + "&DS7=3&DefaultPDF=1&AC0=1&SC0=1&SC1=Attachments";
		this.navigateToWebPage(targetURL);
	}

	handlePrintAROServiceWOClick() {
//		const targetURL = "https://rforce.renewalbyandersen.com/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&id=" + this.recordId + "&QueryId=[SRwithWOALL]a2261000001FMX3,[AcctwithSO]a2261000001FMVg%3Fpv0%3D" + this.orderAccountId + ",[SRwPO]a2261000001FMX8,[SRwMaterial]a2261000001FMXD,[SRwTasks]a2261000001FMXI,[SRwSoldOrder]a2261000001FMXN,[SRwTasksv2]a2261000001OIQQ,[SRwMaterialv2]a2261000001OIRn,[spHSR]a2261000001OJZT,[Resources]a2261000001OJB3&TemplateId=a2A61000000K87L&AC0=1&AC1=Service+Work+Order+Generated&OFN=Service+Request+" + this.orderNumber + "+HSR+Form&DefaultPDF=1&DS4=1&DS0=1&DS7=3";
		const targetURL = this.baseUrl + "/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&id=" + this.recordId + "&QueryId=[SRwithWOALL]a2261000001FMX3,[AcctwithSO]a2261000001FMVg%3Fpv0%3D" + this.orderAccountId + ",[SRwPO]a2261000001FMX8,[SRwMaterial]a2261000001FMXD,[SRwTasks]a2261000001FMXI,[SRwSoldOrder]a2261000001FMXN,[SRwTasksv2]a2261000001OIQQ,[SRwMaterialv2]a2261000001OIRn,[spHSR]a2261000001OJZT,[Resources]a2261000001OJB3&TemplateId=a2A61000000K87L&AC0=1&AC1=Service+Work+Order+Generated&OFN=Service+Request+" + this.orderNumber + "+HSR+Form&DefaultPDF=1&DS4=1&DS0=1&DS7=3";
		this.navigateToWebPage(targetURL);
	}

	handlePrintAROInstallWOClick() {
//		const targetURL = "https://rforce.renewalbyandersen.com/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&id=" + this.recordId + "&QueryId=[SRwInstallRes]a2261000001OJO6,[SRwOrder]a2261000001FMXN,[SRwPO]a2261000001FMX8,[SRwInstallTask]a2261000001OJP9,[SRwInstallMat]a2261000001OJR5,[spHSR]a2261000001FMXS&TemplateId=a2A610000003CmE&AC0=1&AC1=Install+Work+Order+Generated&DefaultPDF=1&DS0=1&DS4=1&DS7=3&OFN=Service+Request+" + this.orderNumber + "+HSR+Form";
		const targetURL = this.baseUrl + "/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&id=" + this.recordId + "&QueryId=[SRwInstallRes]a2261000001OJO6,[SRwOrder]a2261000001FMXN,[SRwPO]a2261000001FMX8,[SRwInstallTask]a2261000001OJP9,[SRwInstallMat]a2261000001OJR5,[spHSR]a2261000001FMXS&TemplateId=a2A610000003CmE&AC0=1&AC1=Install+Work+Order+Generated&DefaultPDF=1&DS0=1&DS4=1&DS7=3&OFN=Service+Request+" + this.orderNumber + "+HSR+Form";
		this.navigateToWebPage(targetURL);
	}


/** Set Events **/


	setServiceTypeOptionList() {

		getServiceTypeOptionMap({ soldOrderJIPEligible: this.soldOrderJIPEligible })
		.then((result) => {
			for (let key in result) {
				if (result.hasOwnProperty(key)) {
					this.serviceTypeOptionList.push({ label: key, value: result[key] });
				}
			}
		})
		.catch((error) => {
			console.log("error in getServiceTypeOptionMap callback", error);
		});

		console.log("Service Type Options", this.serviceTypeOptions);

	}

	setCongaPrintData() {

		getCongaPrintData({})
		.then((result) => {
			this.sourceIsCommunity = result.sourceIsCommunity;
            this.baseUrl = result.baseUrl;
		})
		.catch((error) => {
			console.log("error in getCongaPrintData callback", error);
		});

	}

	setShowCancelRequestButton() {

		let showButtonResult = false;

		if (this.status !== "Cancelled" && this.cancelEligible === true) {
			showButtonResult = true;
		}

		this.showCancelRequestButton = showButtonResult;

	}

	setShowRejectRequestButton() {

		getShowRejectRequestButton({ orderStatus: this.status })
		.then((result) => {
			this.showRejectRequestButton = result;
		})
		.catch((error) => {
			console.log("error in getShowRejectRequestButtong callback", error);
		});

	}

	setDisableModalSave() {

		let disableSaveResult = false;

		this.orderRequired = false;
		this.saveReasonRequired = false;

		if (this.serviceType === "") {
			this.orderRequired = false;
			disableSaveResult = true;
		}
		else if (this.serviceType === "Save") {

			this.saveReasonRequired = true;

			if (this.saveReason === "") {
				disableSaveResult = true;
			}
			else {

				if ((this.saveReason === "Sent Hardware to Customer" ||
					 this.saveReason === "Phone Troubleshooting") &&
					this.secondarySaveReason === "") {
					disableSaveResult = true;
				}

			}

		}
		else if (this.serviceType === "Service") {
			this.orderRequired = true;
			if (this.soldOrder === "") {
				disableSaveResult = true;
			}
		}

		this.disableModalSave = disableSaveResult;

	}

	setServiceSymptom(event) {
		let oldSelectedSymptom = this.serviceSymptoms.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).selectedSymptom;
		this.serviceSymptoms.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).selectedSymptom = event.target.value;
		if (this.serviceSymptomsIds.indexOf(oldSelectedSymptom) > -1) {
			this.serviceSymptomsIds.splice(oldSelectedSymptom, 1);
		}
		this.serviceSymptomsIds.push(event.target.value);
	}

	setProcedure(event) {
		console.log("setProcedure", event.target.name);
		this.procedures.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).selectedProcedure = event.target.value;
		let procedureList = this.procedures.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).procedures;
		this.procedures.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).procedureName =
		procedureList.find((problem) => problem.value === event.target.value).label;
		console.log("procedureList", procedureList);
		console.log("setProcedure this.procedures", this.procedures);
	}

	setQty(event) {
		console.log("setQty", event.target.name);
		this.procedures.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).selectedQty = event.target.value;
		console.log("setQty this.procedures", this.procedures);
	}

	setProcedure2(event) {
		console.log("setProcedure", event.target.name);
		this.procedures.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).selectedProcedure2 = event.target.value;
		let procedureList = this.procedures.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).procedures;
		this.procedures.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).procedureName2 = procedureList.find(
		(problem) => problem.value === event.target.value).label;
		console.log("procedureList", procedureList);
		console.log("setProcedure this.procedures", this.procedures);
	}

	setQty2(event) {
		console.log("setQty", event.target.name);
		this.procedures.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).selectedQty2 = event.target.value;
		console.log("setQty this.procedures", this.procedures);
	}

	setProblemComponent(event) {
		this.problems.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).problemComponent = event.target.value;
		let problemList = this.problems.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).problems;
		this.problems.find((serviceSymptom) => serviceSymptom.orderProductId === event.target.name).problemComponentName =
		problemList.find((problem) => problem.value === event.target.value).label;
	}


/** Miscellaneous Methods **/


	/*
	* @author Jason Flippen
	* @date 03/18/2021
	* @description Method to delete the New Service Symptom records that are no longer needed.
	*/
	deleteNewServiceSymptoms() {

		deleteServiceSymptoms({ symptomJSON: JSON.stringify(this.serviceSymptoms) })
		.then((result) => {
			console.log(result);
			result.forEach((el) => {
				for (let index in this.charges) {
					if (this.charges[index].chargeId === el.chargeId) {
						this.charges[index].symptomId = el.symptomId;
						this.charges[index].isNew = false;
					}
				}
			});
			this.showModalSpinner = false;
		})
		.catch((error) => {
			console.log("deleteServiceSymptoms Error", error);
		});

	}

	/*
	* @author Jason Flippen
	* @date 12/28/2020
	* @description Method to set the "noPartsAdded" flag.
	*/
	updateNoPartsAddedFlag() {
		if (this.newReimbursablePartList.length > 0) {
			this.noPartsAdded = false;
		}
		else {
			this.noPartsAdded = true;
		}
	}

	showServiceSymptom() {
		let key;
		this.serviceSymptoms = [];
		for (key in this.charges) {
			if (this.charges[key].iconPath === "utility:check") {
				this.serviceSymptoms.push(this.charges[key]);
			}
		}
		this.showModalSpinner = false;
	}

	navigateToWebPage(targetURL) {

		// Navigate to a URL
		this[NavigationMixin.Navigate]({
			type: "standard__webPage",
			attributes: {
				url: targetURL
			}
		},
		false // If true it replaces the current page in your browser history with the URL
		);

	}

	/*
	* @author Jason Flippen
	* @date 02/18/2021
	* @description Method to refresh the cache.
	*/
	refreshOrderData() {
		console.log("Refreshing Order Data");
		return refreshApex(this.wiredOrderResult);
	}
	
}