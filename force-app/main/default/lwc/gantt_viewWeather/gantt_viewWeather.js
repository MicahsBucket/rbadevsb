/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
import { LightningElement, track, api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import WEATHER_JSON_FIELD from '@salesforce/schema/ServiceAppointment.Weather_Forecast_JSON__c';
import WEATHER_WARNINGS_FIELD from '@salesforce/schema/ServiceAppointment.Weather_Forecast_Warnings__c';
import WEATHER_TIMESTAMP_FIELD from '@salesforce/schema/ServiceAppointment.Weather_Timestamp__c';
import APPOINTMENT_NUMBER_FIELD from '@salesforce/schema/ServiceAppointment.AppointmentNumber';
import GANTT_LABEL_FIELD from '@salesforce/schema/ServiceAppointment.FSL__GanttLabel__c';
import HAS_FORECAST_ERROR from '@salesforce/schema/ServiceAppointment.Weather_Forecast_Error__c';
import SCHED_START_TIME from '@salesforce/schema/ServiceAppointment.SchedStartTime';
import { refreshApex } from '@salesforce/apex';

import updateForecast from '@salesforce/apex/GanttAction_ViewWeatherController.updateForecast';

const FIELDS = [SCHED_START_TIME,HAS_FORECAST_ERROR,GANTT_LABEL_FIELD,WEATHER_JSON_FIELD, WEATHER_WARNINGS_FIELD, WEATHER_TIMESTAMP_FIELD, APPOINTMENT_NUMBER_FIELD];

const COLUMNS = [
    {label: 'Interval', fieldName: 'timeIntervalString', initialWidth: 175},
    {label: 'Config Name', fieldName: 'thresholdConfigName'},
    {label: 'Details', fieldName: 'shortDescription', initialWidth: 475}
]

const FORECAST_WINDOW_DAYS = 7;

export default class Gantt_viewWeather extends LightningElement {
    @api saId;
    @track showWarnings = false;
    // @track serviceAppointment;
    @track weather;
    @track weatherWarnings;
    @track timeStamp;
    @track error;
    @track columns = COLUMNS;
    @track showSpinner = false;

    get warnings() {
        if (!this.serviceAppointment) {
            return null;
        }
        console.log(JSON.parse(this.serviceAppointment.data.fields.Weather_Forecast_Warnings__c.value));
        return JSON.parse(this.serviceAppointment.data.fields.Weather_Forecast_Warnings__c.value);
    }

    @wire(getRecord, {recordId: '$saId', fields: FIELDS})
    serviceAppointment;
    // wiredServiceAppointment({error, data}) {
    //     console.log('fields: ' + FIELDS);
    //     console.log('SA Id: ' + this.saId);
    //     if (error) {
    //         console.log('error here');
    //         //console.log(error);
    //         //console.log(JSON.parse(JSON.stringify(error)));
    //         this.error = error;
    //     } else if (data) {
    //         console.log('data here');
    //         //console.log(data);
    //         console.log(JSON.parse(JSON.stringify(data)));
    //         this.serviceAppointment = data;
    //         this.timeStamp = this.serviceAppointment.fields.Weather_Timestamp__c.value;
    //         this.weather = this.serviceAppointment.fields.Weather_Forecast_Warnings__c.value
    //         console.log('timestamp: ' +this.timeStamp);

    //     } else {
    //         console.log('error and data undefined');
    //     }
    // } 

    refreshForecast(event) {
        console.log('refreshing forecast');
        this.showSpinner = true;
        updateForecast({saId: this.saId})
        .then(result => {
            console.log(result);
            refreshApex(this.serviceAppointment);
            this.showSpinner = false;
        })
        .catch(error => {
            console.log('Error: ' + error);
            this.showSpinner = false;
        });
    }

    get isWithinForecastWindow() {
        if (!this.serviceAppointment) {
            return false;
        }
        if (this.serviceAppointment.data.fields.SchedStartTime.value > (new Date()).setDate((new Date()).getDate() + FORECAST_WINDOW_DAYS)) {
            return false;
        }
        return true;
    }
}