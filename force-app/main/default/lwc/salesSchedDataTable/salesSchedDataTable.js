import LightningDatatable from 'lightning/datatable';
import salesSchedAssignCol from './salesSchedAssignColumnTemplate.html';

export default class SalesSchedDataTable extends LightningDatatable {
   static customTypes = {
       salesSchedAssignColumn: {
           template: salesSchedAssignCol,
           typeAttributes: ['buttonClass', 'buttonLabel', 'appointmentId', 'appointmentSlot', 'repId', 'aptStatus', 'isRideAlong','salesCapCount','mouseHover']
       }
   };

    
}