import { LightningElement, api, track } from 'lwc';

export default class SalesSchedAssignColumn extends LightningElement{
    @api rowId;
    @api buttonLabel;
    @api buttonClass;
    @api appointmentSlot;
    @api appointmentId;
    @api repId;
    @api aptStatus;
    @api isRideAlong;
    @api salesCapCount;
    @api mouseHover;
    @track hshow = false;
    debugger;

    showAppointment() {
        const event = CustomEvent('showassignment', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                rowId: this.rowId,
                appointmentSlot : this.appointmentSlot,
                appointmentId : this.appointmentId,
                repId : this.repId
            }
        });
        console.log("___ratnastatement___event___",JSON.stringify(event));
        this.dispatchEvent(event);
    }

    get css(){
        if (this.salesCapCount !== undefined && this.salesCapCount > 1) {
            return 'slds-button slds-button_neutral doubleBooking';
        }
        if (this.isRideAlong !== undefined && this.isRideAlong === 'true') {
            return 'slds-button slds-button_neutral rideAlong';
        }
        return 'slds-button slds-button_neutral ' + (this.buttonClass ? this.buttonClass : '');
    }

    get status(){
        if(this.aptStatus === "Accepted" || this.aptStatus ==="Checked In" || this.aptStatus ==="Checked Out")
            return true;
            return false;
        
    }

    get stat(){

        if(this.aptStatus === "Resulted")
        return true;
        return false;
    }
    get doubleBooking() {
        return ((this.salesCapCount !== undefined && this.salesCapCount > 1) ? true : false);
    }


    get unConfirmendAppts() {
        return(( this.aptStatus === "Assigned" || this.aptStatus === "Manually Reassigned" || this.aptStatus === "Manually Assigned" ) ? true : false);
    }

    handleMouseHover(event){
        var ondis= event.target.value;
        if(ondis === this.appointmentId ){
                this.hshow = true;
        }else{
            this.hshow = false;
        }
        //console.log('----onmousehover ----',event.target.value);
    }

    handleMouseOut(event){
        this.hshow = false;
        console.log('----onmousehoverout ----' );
    }
    @api 
    get hoverdata(){
       /* var hObj = {};
        hObj = JSON.parse(this.mouseHover);
        console.log('----hObj------ ' + JSON.stringify(hObj));*/
        console.log('----this.mouseHover------ ',this.mouseHover);
        
        return JSON.parse(this.mouseHover);
    }
}