/* eslint-disable */
import { LightningElement, wire, track, api } from 'lwc';
import getAccountInfo from '@salesforce/apex/AddLegacyAssetController.getAccountInfo';
import getProducts from '@salesforce/apex/AddLegacyAssetController.getProducts';
import saveAssets from '@salesforce/apex/AddLegacyAssetController.saveAssets';

/**
 * @author Connor Davis
 * @description This class will handle most of the functionality of the Add Legacy Asset LWC
 */
export default class AddLegacyAsset extends LightningElement {

    @api accountId; // The id of the account that the assets will be a part of.
    @track account = {}; // The account that was used to get to this page
    @track contacts=[]; // The list of contacts on this account
    @track contactHistory=[]; // The objects that hold certain info about contacts
    @track legacyAssets = []; // An array of legacy assets
    @track search; // The string that was searched
    @track result = []; // The objects returned due to a search
    @track selectedProduct=''; // The product that the user selected

    // These variables will hold the data for the account fields that are lookups
    @track salesRepName = '';
    @track location = '';
    @track HOA = '';
    @track historical = '';
    @track buildPermit = '';

    // Get the products that the user is searching for
    @wire (getProducts, { name: '$search' })
    wireGetSearchResults ({ error, data }) {
            this.result = data;
    }

    // Get the account data from the account with the Id the user was looking at
    @wire (getAccountInfo, { accId: '$accountId' })
    wireGetAccountInfo({ error, data }) {
        if (error) {
            console.log('Error when getting account info');
        } else if (data) {
            // Set account equal to the data
            this.account = data;
            // Set all the lookups to the correct variable so they can be displayed correctly on the page.
            if (this.account.Store_Location__c !== undefined) {
                this.location = this.account.Store_Location__r.Name;
            }

            if (this.account.Sales_Rep_User__c !== undefined) {
                this.salesRepName = this.account.Sales_Rep_User__r.Name;
            }

            if (this.account.HOA__c !== undefined) {
                this.HOA = this.account.HOA__r.Name;
            }

            if (this.account.Historical__c !== undefined) {
                this.historical = this.account.Historical__r.Name;
            }

            if (this.account.Building_Permit__c !== undefined) {
                this.buildPermit = this.account.Building_Permit__r.Name;
            }
        }
    }

    /**
     * @author Connor Davis
     * @description This function calls the saveAssets apex function, which saves the legacy assets the user added
     */
    save() {
        let assetsToSave = [];
        for (let i = 0; i< this.legacyAssets.length; i++) {
            console.log('In for loop');
            if (this.legacyAssets[i].Editable === true) {
                console.log('pushing: ', this.legacyAssets[i]);
                assetsToSave.push(this.legacyAssets[i]);
            }
        }
        //    this.formatDates();
        saveAssets({ assets: assetsToSave })
        .then( data => {
            try {
                // Alert the user that the assets were saved
                alert('Legacy Assets Saved');

                for (let i = 0; i < this.legacyAssets.length; i++) {
                    this.legacyAssets[i].Editable = false;
                    console.log(this.legacyAssets[i].InstallDate);
                }
            } catch (error) {
                console.log('Error updating legacy assets');
            }
        })
        .catch (error => {
            // Alert the user that there were errors
            console.log('Error saving assets: ', error);
            alert('Couldn\'t save assets, make sure all required fields are filled out')
        })
    }

    /**
     * @author Connor Davis
     * @description This function returns the column objects for the contact datatable
     */
    get contactCols() {
        const cols = [
            { label: 'Contact', fieldName: 'FullName', type: 'text', cellAttributes: { alignment: 'left' } },
            { label: 'Home Phone', fieldName: 'HomePhone', type: 'phone', cellAttributes: { alignment: 'left' } },
            { label: 'Mobile Phone', fieldName: 'MobilePhone', type: 'phone', cellAttributes: { alignment: 'left' } },
            { label: 'Type', fieldName: 'Type', type: 'text', cellAttribuges: { alignment: 'left' } },
            { label: 'Primary Contact', fieldName: 'PrimaryContact', type: 'boolean' },
            { label: 'Primary Dwelling for Contact', fieldName: 'PrimaryDwellingforContact', type: 'boolean' } 
        ];
        return cols;
    }

    /**
     * @author Connor Davis
     * @description This function adds a new row the the Legacy Assets table
     * @param {Object} event The event that triggered this function
     */
    onClickAdd(event) {
        // Create an array that will store the data from legacyAssets
        let updatedAssets = [];
        updatedAssets = this.legacyAssets;
        // Clear legacyAssets so the table will update
        this.legacyAssets = [];
        // Push the new asset into the array
        updatedAssets.push({ RowNum: updatedAssets.length,
                             HasSearchResults: false,
                             Editable: true,
                             Name: "New Asset",
                             AccountId: this.accountId,
                             Description: "",
                             InstallDate: "",
                             Legacy_Asset__c: true,
                             Product2Id: "",
                             Product_Name__c: "",
                             Status: "Installed",
                             Quantity: 1,
                             Variant_Number__c: "" });

        // Set legacyAssets to the new list of assets
        this.legacyAssets = updatedAssets;
    }

    /**
     * @author Connor Davis
     * @description This function will remove the deleted row, then re-number the remaining rows
     * @param {Object} event The event that triggered the function to be called
     */
    onClickDelete(event) {
        // Remove the correct row from the list
        this.legacyAssets.splice(event.target.dataset.rownum, 1);
        // Re-number the rows in the list
        for (let i = 0; i < this.legacyAssets.length; i++) {
            this.legacyAssets[i].RowNum = i
        }
    }

    /**
     * @author Connor Davis
     * @description This function takes the user back to the account page
     * @param {Object} event 
     */
    onClickBack(event) {
        window.open('/' + this.accountId, '_top');
    }

    /**
     * @author Connor Davis
     * @description This function is called when the user clicks on a product. It then assigns the correct values to the correct legacy asset
     * @param {Object} event The event that triggered the function
     */
    productSelect(event) {
        // Get the row that the user was searching on
        let currentRow = event.target.dataset.rownum;
        try {
            // Set the product data in the correct legacy asset
            this.legacyAssets[currentRow].Product_Name__c = event.currentTarget.dataset.name;
            this.legacyAssets[currentRow].Name = event.currentTarget.dataset.name;
            this.legacyAssets[currentRow].Product2Id = event.currentTarget.dataset.id;
            // Clear the results so the search bar goes away
            this.result=[];
            this.legacyAssets[currentRow].HasSearchResults = false;
        } catch (error) {
            console.log('Error: ', error);
        }
    }

    /**
     * @author Connor Davis
     * @description This function formats the account balance to be a dollar amount
     */
    get accountBal() {
        let formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2
        });
        return formatter.format(Number(this.account.Account_Balance__c));
    }
    
    /**
     * @author Connor Davis
     * @description This function reformats the dates of the assets to match what they were when they were inputted.
     */
    formatDates() {
        for (let i = 0; i < this.legacyAssets.length; i++) {
            let pieces = this.legacyAssets[i].InstallDate.split('-');
			console.log('Pieces', pieces);
            this.legacyAssets[i].InstallDate = parseInt(pieces[1],10) + '/' + parseInt(pieces[2],10) + '/' + pieces[0];
			console.log('Dates: ', this.legacyAssets[i].InstallDate);
        }
    }

    /**
     * @author Connor Davis
     * @description This function will cause the search bar to open with the products that fit the value the user is inputting
     * @param {Object} event The event that triggered the function to be called
     */
    handleSearch(event) {
        // Get the row that the user was searching on
        let currentRow = event.target.dataset.rownum;
        // If the user has nothing inputted, don't show the search results
        if (event.target.value == '') {
            this.result = [];
            this.legacyAssets[currentRow].HasSearchResults = false;
        } else {
            // Get the value the user is inputting, show the search results in a dropdown
            this.search = event.target.value;
            this.legacyAssets[currentRow].HasSearchResults = true;
        }
    }

    /**
     * @author Connor Davis
     * @description This function sets the variant number on the correct row to what the user inputted
     * @param {Object} event The event that triggered the function
     */
    handleVariantChange(event) {
        this.legacyAssets[event.target.dataset.rownum].Variant_Number__c = event.target.value;
    }

    /**
     * @author Connor Davis
     * @description This function sets the date on the correct row to what the user inputted
     * @param {Object} event The event that triggered the function
     */
    handleInstallDateChange(event) {
        this.legacyAssets[event.target.dataset.rownum].InstallDate = event.target.value;
    }

    /**
     * @author Connor Davis
     * @description This function sets the notes on the correct row to what the user inputted
     * @param {Object} event The event that triggered the function
     */
    handleNotesChange(event) {
        this.legacyAssets[event.target.dataset.rownum].Description = event.target.value;
    }
}