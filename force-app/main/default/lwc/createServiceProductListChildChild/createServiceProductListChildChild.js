import { LightningElement, api, track, wire } from "lwc";
import getRespectiveDefectCode from "@salesforce/apex/CreateServiceRequestController.fetchDefectCodes";
import getChargeCategories from "@salesforce/apex/CreateServiceRequestController.getChargeCategories";
import getWhatWhere from "@salesforce/apex/CreateServiceRequestController.getWhatWhere";

export default class CreateServiceProductListChildChild extends LightningElement {
  @api serviceProduct;
  @api asset;
  @api assets;
  @api mtoDoorOptions;
  @api mtoWindowOptions;
  @api groups;
  @api groupMap = new Map();
  @api assetMap = new Map();

  @track isSelected;
  @track assetId;
  @track serviceProductId;
  @track mtoSourceCodeSelected = "";
  @track responsibilitySelected = "";
  @track defectCodeOption;
  @track chargeToSelected = "";
  @track categoryOptions;
  @track whatWhereOptions;
  @track hasResponsibility = false;
  @track hasMTOCode = false;
  @track showAddNotesModal = false;
  @track chargeToSelectedId;
  @track defectDetails;
  @track categorySelected;
  @track dropdownNOTActive = false;

  error;

  get options() {
    return [
      { label: "--None--", value: "NA" },
      { label: "Manufacturing", value: "Manufacturing" },
      { label: "Retailer", value: "Retailer" },
      { label: "Customer", value: "Customer" }
    ];
  }

  connectedCallback() {
    console.log("this.serviceProduct", this.serviceProduct);
    if (this.serviceProduct.isSelected == true) {
      this.dispatchEvent(new CustomEvent("productischecked"));
      this.isSelected = true;
      console.log(
        "this.serviceProduct:  ",
        JSON.stringify(this.serviceProduct)
      );
      if (this.serviceProduct.responsibilty != null) {
        this.responsibilitySelected = this.serviceProduct.responsibilty;
        this.hasResponsibility = true;
      }
      if (this.serviceProduct.mtoSourceCode != null) {
        this.mtoSourceCodeSelected = this.serviceProduct.mtoSourceCode;
        this.hasMTOCode = true;
      }
      if (this.hasResponsibility == true && this.hasMTOCode == true) {
        this.getCatgeoriesAndWhatWhere();
        if (this.serviceProduct.selectedDefectCode != null) {
          this.chargeToSelectedId = this.serviceProduct.selectedDefectCode;
        }
      }
      if (this.serviceProduct.selectedSourceOfDefect != null) {
        this.categorySelected = this.serviceProduct.selectedSourceOfDefect;
      }
      if (this.serviceProduct.defectDetails != null) {
        this.defectDetails = this.serviceProduct.selectedSourceOfDefect;
        this.getWhatWhereOptions();
      }
      if (this.serviceProduct.defectDetails != null) {
        this.defectDetails = this.serviceProduct.defectDetails;
      }
    }
  }

  setSelectedValue(event) {
    console.log("childchild setSelectedValue", event.target.name);
    console.log("childchild checked", event.target.checked);
    this.assetId = event.target.name.split("#")[1];
    this.serviceProductId = event.target.name.split("#")[0];
    this.isSelected = event.target.checked;
    if (event.target.checked == true) {
      this.dispatchEvent(new CustomEvent("productischecked"));
    } else {
      this.defectCodeOption = null;
      this.defectDetails = null;
      this.dispatchEvent(new CustomEvent("productisunchecked"));
    }
    this.dispatchEvent(
      new CustomEvent("setselectedvalue", { detail: event.target.name })
    );
  }

  handleResponsibilityChange(event) {
    console.log("handleResponsibilityChange: ", event.target.value);
    this.hasResponsibility = true;
    this.responsibilitySelected = event.target.value;
    this.dispatchEvent(
      new CustomEvent("handlechange", {
        detail: { name: event.target.name, responsibility: event.target.value }
      })
    );
    if (this.mtoSourceCodeSelected != "" && this.responsibilitySelected != "") {
      this.getCatgeoriesAndWhatWhere();
    }
  }

  handleMTOChange(event) {
    console.log("handleMTOChange: ", event.target.value);
    this.hasMTOCode = true;
    this.mtoSourceCodeSelected = event.target.value;
    this.dispatchEvent(
      new CustomEvent("handlemtochange", {
        detail: { name: event.target.name, mtocode: event.target.value }
      })
    );
    if (this.mtoSourceCodeSelected != "" && this.responsibilitySelected != "") {
      this.getCatgeoriesAndWhatWhere();
    }
  }

  handleChange(event) {
    this.dispatchEvent(
      new CustomEvent("handlechange", {
        detail: {
          name: event.target.name,
          responsibility: event.target.value
        }
      })
    );
  }

  setChargeCostTo(event) {
    console.log("setChargeCostTo Value: ", event.target.value);
    console.log("setChargeCostTo Name: ", event.target.name);
    this.chargeToSelectedId = event.target.value;
    this.dispatchEvent(
      new CustomEvent("setchargecostto", {
        detail: { name: event.target.name, value: event.target.value}
      })
    );
  }

  getCatgeoriesAndWhatWhere(event) {
    getRespectiveDefectCode({
      responsibility: this.responsibilitySelected,
      mtoSourceCode: this.mtoSourceCodeSelected
    })
      .then((result) => {
        console.log("@@@ results defectCodes in handleMTOChange: ", result);
        this.defectCodeOption = result;
      })
      .catch((error) => {
        console.log(error);
      });
    getChargeCategories({ responsibility: this.responsibilitySelected })
      .then((result) => {
        console.log("getChargeCategories: ", result);
        this.categoryOptions = result;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  setCategory(event) {
    this.categorySelected = event.target.value;
    this.getWhatWhereOptions();
    this.dispatchEvent(
      new CustomEvent("setcategory", {
        detail: { name: event.target.name, value: event.target.value }
      })
    );
  }
  getWhatWhereOptions() {
    getWhatWhere({ chargeTo: this.categorySelected })
      .then((result) => {
        console.log("getWhatWhere: ", result);
        this.whatWhereOptions = result;
        if(result == 0){
          this.dropdownNOTActive = true;
        }
        else{
          this.dropdownNOTActive = false;
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  setWhatWhere(event) {
    console.log("setWhatWhere ChildChild", event.target.value);
    this.defectDetails = event.target.value;
    this.dispatchEvent(
      new CustomEvent("setwhatwhere", {
        detail: { name: event.target.name, value: event.target.value }
      })
    );
  }

  handleAddNotesClick(event) {
    this.dispatchEvent(
      new CustomEvent("handleaddnotesclick", {
        detail: { name: event.target.name, value: event.target.value }
      })
    );
  }

  handleAddNotesCloseClick() {
    this.dispatchEvent(new CustomEvent("handleaddnotescloseclick"));
  }

  applyToAllProducts(event) {
    console.log("applyToAllProducts check CHILDCHILD", event.target.checked);
    console.log("applyToAllProducts name CHILDCHILD", event.target.name);
    this.dispatchEvent(
      new CustomEvent("applytoallproducts", {
        detail: { name: event.target.name, checked: event.target.checked }
      })
    );
  }
}