import {wire, track, api} from 'lwc';
import { Calendar } from 'c/calendar';
import getScheduleWrapper from '@salesforce/apex/SalesSchedRepCalendarCtrl.getScheduleWrapper';
import saveCapacities from '@salesforce/apex/SalesSchedRepCalendarCtrl.saveCapacities';
import getDefaultMonth from '@salesforce/apex/SalesSchedRepCalendarCtrl.getDefaultMonth';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import getUserInfo from '@salesforce/apex/SalesSchedRepCalendarCtrl.getDefaultStore';
import Id from '@salesforce/user/Id';

export default class SalesRepCalendar extends Calendar{
    @track scheduleWrapper;
    @track error;
    @track saveVariant;
    @track cardTitle = '';
    @track monthStart = '';
    @api recordId;
    @track updatedCapacityWrappers;
    @track results;
    @wire(getUserInfo, { userId: Id })
        userData;

    connectedCallback() {
        if(!this.recordId){
            var parsedUrl = new URL(window.location.href);
            this.recordId = parsedUrl.searchParams.get("id"); 
            this.saveVariant="neutral";
            this.updatedCapacityWrappers = new Map();
        }
    }

    @wire(getDefaultMonth,{ salesRepId : '$recordId'})
    handleGetDefaultMonth({error,data}){
        if(data && this.results){
            console.log('--0-- ', this.results);
            this.currentMoment = new moment(data,'YYYY-MM-DD');// add the days
            this.initializePeriods();
            this.monthStart = data;
        }

        if(error){
            this.getErrorMessage(error);
        }
    }

    @wire(getScheduleWrapper, {monthStart : '$monthStart', salesRepId : '$recordId'})
    handleGetScheduleWrapper(value){
        this.scheduleWrapperResult = value;
        if(value.data && this.results && this.currentMoment){
            try{
                console.log('--1-- ', this.results);
                //debugger;
                this.results.forEach(row => {
                    row.periods.forEach(day => {
                        value.data.days.forEach(dayWrapper =>{
                            // eslint-disable-next-line no-undef
                            let capacityMoment = new moment(dayWrapper.day,'YYYY-MM-DD');
                            if(capacityMoment.isSame(day.moment, 'day')){
                                day.dayWrapper = dayWrapper;
                                day.turnAllOn = false; // added to allow the switch to All Off
                                // Connor Davis: Check if the button should say "All On" or "All Off"
                                dayWrapper.capacityWrappers.forEach(capacityWrapper =>{
                                    if (capacityWrapper.store == 'Unavailable'){
                                        day.turnAllOn = true;
                                    }
                                
                                })
                            }
                        });
                    });
                });
            }catch(error){
                this.getErrorMessage(error);
            }
            this.scheduleWrapper = value.data;
            this.cardTitle = value.data.title;
            this.saveVariant = 'neutral';
            this.updatedCapacityWrappers = new Map();
        }

        if(value.error) {
            this.getErrorMessage(value.error);
        }
    }

    //Overrides calendar's change period method to also set start month.
    handleChangePeriod(event){
        super.handleChangePeriod(event);
        this.monthStart = this.currentMoment.clone().startOf('month').format('YYYY-MM-DD');
    }

    // change all the picklist to set to user default value
    handleCapacityChangeAll(event) {
        debugger;
        console.log("userData", this.userData);
        let data = event.currentTarget.dataset;
        let key = data.key;
        let list = this.results[data.resultIndex].periods[data.periodIndex]
          .dayWrapper.capacityWrappers;
        let tempList = JSON.parse(
          JSON.stringify(
            this.results[data.resultIndex].periods[data.periodIndex].dayWrapper
              .capacityWrappers
          )
        );
        for (let index = 0; index < list.length; index++) {
          if (list[index].key) {
              // Set the store to the user's default store location if they selected All On, and Unavailable if All Off
          tempList[index].store = this.results[data.resultIndex].periods[data.periodIndex].turnAllOn ? this.userData.data : 'Unavailable';
          this.updatedCapacityWrappers.set(list[index].key, {
            store: tempList[index].store, // set the value of the store to either the user's default or Unavailable
            capacity: list[index].capacity
          });          
        }
        }
        if (tempList) {
          let tempResults = JSON.parse(JSON.stringify(this.results));
          tempResults[data.resultIndex].periods[
            data.periodIndex
          ].dayWrapper.capacityWrappers = tempList;
          this.results = tempResults;
          this.results[data.resultIndex].periods[data.periodIndex].turnAllOn = !this.results[data.resultIndex].periods[data.periodIndex].turnAllOn;
        }
        this.saveVariant = "brand";
      }
    
    handleCapacityChange(event){
        console.log('handleCapacityChange', event);
        let data = event.currentTarget.dataset;
        let key = data.key;
        let store = event.detail.value;
        console.log('handleCapacityChange_Data', key,'11', event.detail.value, '22',this.results[data.resultIndex].periods[data.periodIndex].dayWrapper.capacityWrappers[data.wrapperIndex].capacity);
        if(key){
            this.updatedCapacityWrappers.set(
                key,
                {
                    store:event.detail.value,
                    capacity : this.results[data.resultIndex].periods[data.periodIndex].dayWrapper.capacityWrappers[data.wrapperIndex].capacity
                }
            );
        }
        this.saveVariant = 'brand';
    }

    handleCapacitiesSave() {
        debugger;
        if (this.updatedCapacityWrappers.size > 0) {
          //Get all the capacity wrappers in the updated map.
          let wrappers = [...this.updatedCapacityWrappers.values()];
          //debugger;
          // this.showSuccess('Success','Capacities successfully updated.');
          // refreshApex(this.scheduleWrapperResult);
          saveCapacities({ capacityWrappers: wrappers, salesRepId: this.recordId })
            .then(result => {
              // eslint-disable-next-line no-console
              this.showSuccess("Success", "Capacities successfully updated.");
              refreshApex(this.scheduleWrapperResult);
            })
            .catch(error => {
              // eslint-disable-next-line no-console
              console.log("error: " + error);
              this.getErrorMessage(error);
            });
        }
      }

    getErrorMessage(error){
        if(!error.message && error.details && error.details.body){
            error.message = error.details.body;
        }
        this.error = error;
    }

    showSuccess(title, message){
        this.showNotification(title, message, 'success');
    }

    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    get isSingularStore(){
        return this.scheduleWrapper && this.scheduleWrapper.options && this.scheduleWrapper.options.length === 1;
    }
}