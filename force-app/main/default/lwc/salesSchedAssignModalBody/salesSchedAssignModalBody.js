import { LightningElement, api, track, wire } from "lwc";
import getAppointment from "@salesforce/apex/SalesSchedAppointmentCtrl.getAppointment";
//import getPastAppointments from "@salesforce/apex/SalesSchedAppointmentCtrl.getPastAppointments";
import clearResultDetails from '@salesforce/apex/SalesSchedAppointmentCtrl.clearResultantDetails';
import deleteRidealong from "@salesforce/apex/SalesSchedAppointmentCtrl.deleteRidealong";
import deleteAssignment from "@salesforce/apex/SalesSchedAppointmentCtrl.deleteAssignment";
import getAvailableCapacities from "@salesforce/apex/SalesSchedAppointmentCtrl.getAvailableCapacities";
import { refreshApex } from "@salesforce/apex";

//The imports below are needed for adding a ride along rep.
import { createRecord } from "lightning/uiRecordApi";
import { updateRecord } from "lightning/uiRecordApi";
import SALES_APPOINTMENT_RESOURCE_OBJECT from "@salesforce/schema/Sales_Appointment_Resource__c";
import SALES_CAPACITY_FIELD from "@salesforce/schema/Sales_Appointment_Resource__c.Sales_Capacity__c";
import SALES_APPOINTMENT_FIELD from "@salesforce/schema/Sales_Appointment_Resource__c.Sales_Appointment__c";
import STATUS_FIELD from "@salesforce/schema/Sales_Appointment_Resource__c.Status__c";
import PRIMARY_FIELD from "@salesforce/schema/Sales_Appointment_Resource__c.Primary__c";
import ID_FIELD from "@salesforce/schema/Sales_Appointment_Resource__c.Id";
import REASON_FIELD from "@salesforce/schema/Sales_Appointment_Resource__c.Assignment_Reason__c";

const detailcols = [
 // { label: "Date", fieldName: "date", type: "date" },
 // { label: "Result", fieldName: "result" },
  //{ label: "Done By", fieldName: "doneby" },
  { label: 'Date', fieldName: 'lastSalesDate' },
  { label: 'Result', fieldName: 'salesOrderResult' },
 // { label: 'Result', fieldName: 'salUrl', type: 'url', typeAttributes: {label: { fieldName: 'salesOrderResult' }, target: '_blank'}},
  { label: 'Done By', fieldName: 'opptyOwner' },
  { type: 'button', initialWidth: 150, typeAttributes: { label: "Resulted Details",  title: "Resulted Details", variant: "828f69"}} 
];

const DELAY = 300;

export default class SalesSchedAssignModalBody extends LightningElement {
  @api appointmentId; //Passed in by a parent component to determine what appointment to display.
  @api displayRideAlongDetails; //Passed in by a parent component, determines if ride along info is shown (e.g. if the appointment is already assigned.)
  @api readOnly;
  @api prepopulatedRepId;
  @api prepopulatedRepName;
  @track isPhoneAvailable = false;
  @api capacityId;
  @api listLength;
  @api unassigned = false;
  @track error;
  @track repId; //Specified if this appointment is assigned to a specific rep.
  @track repName; //Specified if this appointment is assigned to a specific rep.
  @track appwrapper; //This variable will be populated once the appointmentId updates and apex is called to fetch the appointment info.
  @track showridealong; //Set to true IF displayRideAlongDetails is true, and if the user clicked on the add ride along rep button.
  @track detailcols = detailcols;
  @track ridealongs = false;
  @track toggleUpdateAssignment = false;
  @track assignable;
  appointmentReturn;
  primaryResourceId;
  
  @track assignmentSearchKey = "";
  @track selectedAssignment;
  @track selectedAssignmentObject;
  @track assignmentFocus;
  @track openResultant = false;
  @track capacities;//238		
  @track acprimaryContact;		
  @track acprimaryContactBol = false;		
  @track selSaleOrder;

 @api appointmentReturn;

  @wire(getAvailableCapacities, {
    appointmentId: "$appointmentId",
    searchText: "$assignmentSearchKey"
  })
  capacities;
  populateCapacity(result) {
    this.mainCapacity = result;
    this.capacities = result.data;
  }


  connectedCallback() {
    this.handleGetAppointment();
  }

  
  //Get the appointment information that is displayed in the modal.
  @wire(getAppointment, { appointmentId: '$appointmentId' })
  handleGetAppointment(appointmentReturn) {
    this.appointmentReturn = appointmentReturn;
    let ridealongs = [];
    let repName = undefined;
    let repId = undefined;
    let primaryResourceId = undefined;
    if (appointmentReturn && appointmentReturn.data) {
      if(appointmentReturn.data.primaryContact.HomePhone || appointmentReturn.data.primaryContact.MobilePhone){
        this.isPhoneAvailable = true;
      }
      if (appointmentReturn.data.appointment.Sales_Appointment_Resources__r) {

        appointmentReturn.data.appointment.Sales_Appointment_Resources__r.forEach(rep => {
          if (rep.Primary__c) {
            
            repId = rep.Sales_Capacity__r.User__r.Id;
            repName = rep.Sales_Capacity__r.User__r.Name;
            primaryResourceId = rep.Id;
          }
          ridealongs.push({
            type: 'icon',
            label: rep.Sales_Capacity__r.User__r.Name + (rep.Primary__c ? ' (Primary)' : ' (Ride Along)'),
            name: rep.Id,
            iconName: 'utility:resource_capacity',
            alternativeText: rep.Sales_Capacity__r.User__r.Name,
            showClose: !rep.Primary__c
          });
       });
      }
      this.repId = repId;
      this.primaryResourceId = primaryResourceId;
      this.repName = repName;
      this.ridealongs = ridealongs;
      this.appwrapper = appointmentReturn.data;
      this.displayRideAlongDetails = true;
    }
   /* if (appointmentReturn && appointmentReturn.error) {
      if (!appointmentReturn.error.message && appointmentReturn.error.body.message) {
        this.error = { message: appointmentReturn.error.body.message };
        return;
      }
      this.error = appointmentReturn.error;
    }*/
      
    this.paymentTypeOptions = [
      { label: "-- None --", value: "" },
      { label: "Cash", value: "Cash" },
      { label: "Finance", value: "Finance" },
      { label: "Cash & Finance", value: "Cash & Finance" },
      { label: "Finance Turn-Down", value: "Finance Turn-Down" }
    ];
    //console.log("____appointment_____" + JSON.stringify(appointmentReturn));
  }
  
  handleToggleUpdateAssignment() {
    this.toggleUpdateAssignment = true;
  }
  
  @api
  getAvailableCapacity() {
    if (this.mainCapacity.data || this.mainCapacity.error)
    refreshApex(this.mainCapacity);
  }
  //When the lightning data service record is submitted for the primary rep, then this is called.
  handleAssignSubmit(event) {
    if (!this.selectedAssignment) {
      return;
    }

    event.preventDefault();
    const newFields = {};

    newFields[SALES_CAPACITY_FIELD.fieldApiName] = this.selectedAssignment;
    newFields[SALES_APPOINTMENT_FIELD.fieldApiName] = this.appointmentId;
    newFields[PRIMARY_FIELD.fieldApiName] = true;
    newFields[STATUS_FIELD.fieldApiName] = this.primaryRepId
      ? "Manually Reassigned"
      : "Manually Assigned";
    newFields[REASON_FIELD.fieldApiName] = "Manual";

    if (this.primaryRepId) {
      newFields[ID_FIELD.fieldApiName] = this.primaryRepId;
      const recordInput = { fields: newFields };
      updateRecord(recordInput)
        .then(result => {
          //this.reset();
          //this.dispatchEvent(new CustomEvent('refresh'))
          //SS-257 Start
          this.RemoveUpdateAssignments();
          //SS-257 Stop
        })
        .catch(error => {
          this.appwrapper = false;
          this.dispatchEvent("bodyerror", { detail: { errorMessage: error } });
          //this.error = error;
        });
    } else {
      const recordInput = {
        apiName: SALES_APPOINTMENT_RESOURCE_OBJECT.objectApiName,
        fields: newFields
      };
      createRecord(recordInput)
        .then(result => {
          this.reset();
          this.dispatchEvent(
            new CustomEvent("reset", { bubbles: true, composed: true })	
          );
          this.dispatchEvent(new CustomEvent("refresh"));	
          
        })
        .catch(error => {
          this.appwrapper = false;
          this.dispatchEvent("bodyerror", { detail: { errorMessage: error } });
        });
    }
  }

  handlePrepopulatedSubmit() {
    const newFields = {};
    newFields[SALES_CAPACITY_FIELD.fieldApiName] = this.capacityId;
    newFields[SALES_APPOINTMENT_FIELD.fieldApiName] = this.appointmentId;
    newFields[PRIMARY_FIELD.fieldApiName] = true;
    newFields[STATUS_FIELD.fieldApiName] = this.primaryRepId
      ? "Manually Reassigned"
      : "Manually Assigned";
    newFields[REASON_FIELD.fieldApiName] = "Manual";

    const recordInput = {
      apiName: SALES_APPOINTMENT_RESOURCE_OBJECT.objectApiName,
      fields: newFields
    };
    createRecord(recordInput)
      .then(result => {
        this.reset();
        //SS-257 Start
        this.dispatchEvent(new CustomEvent("refresh"));
        //SS-257 Stop
      })
      .catch(error => {
        this.appwrapper = false;
        this.dispatchEvent("bodyerror", { detail: { errorMessage: error } });
      });
  }

  //Displays the lightning data service / record form for ride along rep input.
  handleAddRideAlong() {
    this.showridealong = true;
  }

  //When the lightning data service record is submitted for adding a ride along rep, then this is called.
  handleRidealongSubmit(event) {
    if (!this.selectedAssignment) {
      return; 
 }
    event.preventDefault();
    const newFields = {};
    newFields[SALES_CAPACITY_FIELD.fieldApiName] = this.selectedAssignment;
    newFields[SALES_APPOINTMENT_FIELD.fieldApiName] = this.appointmentId;
    newFields[PRIMARY_FIELD.fieldApiName] = false;
    newFields[STATUS_FIELD.fieldApiName] = "Manually Assigned";
    const recordInput = {
      apiName: SALES_APPOINTMENT_RESOURCE_OBJECT.objectApiName,
      fields: newFields
    };
    createRecord(recordInput)
      .then(result => {
        this.reset();
        this.dispatchEvent(new CustomEvent("refresh"));
               
      })
      .catch(error => {
        this.dispatchEvent("bodyerror", { detail: { errorMessage: error } });
        this.showridealong = false;
      });
  }

  handleRidealongRemove(event) {
    let ridealong = event.detail;
    deleteRidealong({ appointmentResourceId: ridealong })
      .then(result => {
        //refreshApex(this.appointmentReturn);
        this.reset();
        //SS-257 Start
        this.dispatchEvent(new CustomEvent("refresh"));
        //SS-257 Stop
      })
      .catch(error => {});
  }

  handleRemoveAssignment(event) {
    try {
      if (this.appointmentId) {
             deleteAssignment({ appointmentId: this.appointmentId })
          .then(result => {
            this.RemoveUpdateAssignments();
          })
          .catch(error => {});
      }
    } catch (ex) {
      console.log(ex);
    }
  }
  handleErrorModalClose() {
    this.error = false;
  }
  handleAssignmentModalClose() {
    //this.appwrapper = false;
    //this.appointmentId = undefined;
    this.reset();
    this.dispatchEvent(new CustomEvent('assignmodalclose'));
  }

  handleAssignmentChange(event) {
    // Debouncing this method: Do not update the reactive property as long as this function is
    // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
    window.clearTimeout(this.delayTimeout);
    const searchKey = event.target.value;

    // eslint-disable-next-line @lwc/lwc/no-async-operation
    this.delayTimeout = setTimeout(() => {
      this.assignmentSearchKey = searchKey;
    }, DELAY);
  }

  handleAssignmentClick(event) {
    this.selectedAssignment = event.detail.value;
    this.selectedAssignmentObject = event.detail;
  }

  handleAssignmentRemove() {
    this.selectedAssignment = undefined;
    this.selectedAssignmentObject = undefined;
  }

  handleRemoveAssignmentFocus() {
    this.assignmentFocus = false;
  }

  handleAssignmentFocus() {
    this.assignmentFocus = true;
  }

  populateHeader(evt) {
    this.template.querySelector(`[data-id=${evt.detail.id}]`).label =
      evt.detail.heading;
  }

     handleResultantData() {
       this.openResultant = true;
     }
     handleMultiResultData(event){
      const row = event.detail.row;
      this.selSaleOrder = row;
      this.acprimaryContactBol = true; 
      // eslint-disable-next-line no-console
      console.log("Event Console" , JSON.stringify(row));
        }
  
      handleCloseAction(event){
      this.acprimaryContactBol = false; 
          }
    
    clearResultantDetails() { 
      let salesOrderStatus=this.appwrapper.appointment.Sales_Order__r.Result__c;
      // eslint-disable-next-line eqeqeq
      if(salesOrderStatus=='Sale')
      {
        // eslint-disable-next-line no-alert
        alert("Sales Order is resulted as a Sale, you cannot clear Result Details. Managers can edit the Sales Order, or Contact rSupport for assistance.");
        return;
      }
      // eslint-disable-next-line no-alert
      let clrDetails = confirm("Are you sure you want to clear Result Details? This cannot be undone.");
      if (clrDetails) {
        clearResultDetails({ salesorderId: this.appwrapper.appointment.Sales_Order__c })
        .then(() => {
          //refreshApex(this.appointmentReturn);
          this.reset();
          this.dispatchEvent(new CustomEvent('refresh'));
        })
        .catch(error => { 
          console.log("____error______"+JSON.stringify(error));
        });
      }
    }
  closeResultant() {
    this.openResultant = false;
  }

  reset() {
    debugger;
    refreshApex(this.appointmentReturn);
    this.appwrapper = false;
    this.isPhoneAvailable = false;
    this.appointmentId = undefined;
    this.capacityId = undefined;
    this.error = false;
    this.repId = undefined;
    this.repName = undefined;
    this.prepopulatedRepId = undefined;
    this.prepopulatedRepName = undefined;
    this.unassigned = false;
    this.toggleUpdateAssignment = false;
    this.ridealongs = false;
    this.displayRideAlongDetails = false;
    this.openResultant = false;
    this.dispatchEvent(
      new CustomEvent("reset", { bubbles: true, composed: true })
    );
  }

  /** Getters **/
  /**Ratna written below getters for Resultant Details */
  get displayResultedButton() {
    return this.appwrapper.appointment.Sales_Order__r.Resulted__c;
  }
  get displaySaleResulted() {
    return this.appwrapper.appointment.Sales_Order__r.Result__c === "Sale";
  }
  get displaySaleDemoNoSaleOrNoDemoResulted() {
    return (
      this.displaySaleResulted ||
      this.displayDemoNoSaleResulted ||
      this.displayNoDemoResulted
    );
  }
  get displaySaleOrDemoNoSaleResulted() {
    let result = this.appwrapper.appointment.Sales_Order__r.Result__c;
    return "Sale" === result || "Demo No Sale" === result;
  }
  get displaySeriesTwo() {
    return this.appwrapper.appointment.Sales_Order__r.Opportunity__r
      .Store_Location__r.Active_Store_Configuration__r.Sells_Series_2__c;
  }
  get displaySeriesA() {
    return this.appwrapper.appointment.Sales_Order__r.Opportunity__r
      .Store_Location__r.Active_Store_Configuration__r.Sells_Series_A__c;
  }
  get displayEntryDoor() {
    return this.appwrapper.appointment.Sales_Order__r.Opportunity__r
      .Store_Location__r.Active_Store_Configuration__r.Entry_Doors__c;
  }
  get displayDemoNoSaleResulted() {
    return (
      "Demo No Sale" === this.appwrapper.appointment.Sales_Order__r.Result__c
    );
  }
  get displayFinanceResulted() {
    return (
      this.appwrapper.appointment.Sales_Order__r.Payment_Type__c === "Finance"
    );
  }
  get displayCashAndFinanceResulted() {
    return (
      this.appwrapper.appointment.Sales_Order__r.Payment_Type__c ===
      "Cash & Finance"
    );
  }
  get displayFinanceTurnDownResulted() {
    return (
      this.appwrapper.appointment.Sales_Order__r.Payment_Type__c ===
      "Finance Turn-Down"
    );
  }
  get displayFinanceDetailsResulted() {
    return (
      "No" === this.appwrapper.appointment.Sales_Order__r.Finance_Issues__c
    );
  }
  get displayNoDemoResulted() {
    return "No Demo" === this.appwrapper.appointment.Sales_Order__r.Result__c;
  }
  get displayFollowup() {
    return "Yes" === this.appwrapper.appointment.Sales_Order__r.Rescheduled__c;
  }
  get displayNotHomeResulted() {
    return "Not Home" === this.appwrapper.appointment.Sales_Order__r.Result__c;
  }
  get displayNotCoveredResulted() {
    return (
      "Not Covered" === this.appwrapper.appointment.Sales_Order__r.Result__c
    );
  }
  get displayRideAlongAdvice() {
    return this.appwrapper && this.appwrapper.hasRideAlongs;
  }
  /**till here--Resultant details getters */

  /** Getters **/
  get primaryRepId() {
    return this.appwrapper && this.appwrapper.primaryRep
      ? this.appwrapper.primaryRep.Id
      : false;
  }

  get additionalDetailsText() {
    return this.showdetails
      ? "Hide Additional Details"
      : "Show Additional Details";
  }

  get showAssignedTo() {
    return !this.unassigned && this.repName && !this.toggleUpdateAssignment;
  }

  get showPrepopulatedName() {
    return !this.unassigned && !this.repName && !this.toggleUpdateAssignment;
  }

  get showUpdateAssignment() {
    return this.unassigned || this.toggleUpdateAssignment;
  }

  get details() {
    this.acprimaryContact = this.appwrapper.actSalOrdHist.con;
    let details = [];
    return this.appwrapper.actSalOrdHist.asaccwrap;
    /*if (this.appwrapper && this.appwrapper.appointment) {
      let opp = this.appwrapper.appointment.Sales_Order__r.Opportunity__r;
      if (opp.Last_Sale_Date__c && opp.Last_Sale_Rep__c) {
        let d = moment(opp.Last_Sale_Date__c).format();
        details.push({
          doneby: opp.Last_Sale_Rep__c,
          date: d,
          result: "Sale"
        });
      }

      if (opp.Last_DNS_Date__c && opp.Last_DNS_Rep__c) {
        let d = moment(opp.Last_DNS_Date__c).format();
        details.push({
          doneby: opp.Last_DNS_Rep__c,
          date: d,
          result: "Demo No Sale"
        });
      }
    }

    if (details.length > 0) {
      return details.sort((d1, d2) => moment(d2.date) - moment(d1.date));
    }
    return false;*/
  }

  //SS-257 Start
  RemoveUpdateAssignments() {
    if (this.listLength === 1) {
      this.appwrapper = false;
      this.dispatchEvent(new CustomEvent("reset"));
      this.dispatchEvent(new CustomEvent("refresh"));
    } else {
      this.dispatchEvent(
        new CustomEvent("removeassignment", {
          detail: {
            appointmentId: this.appointmentId
          }
        })
      );
    }
  }
  //SS-257 Stop
}