import { LightningElement, api, wire } from 'lwc';
import fetchResourceAbsence from '@salesforce/apex/Resource_Absence_Controller.getResourceAbsence';
import fetchUserTimeZone from '@salesforce/apex/Resource_Absence_Controller.getCurrentLoggedInUserTimeZone';

export default class ResourceAbsenceDetails extends LightningElement {
    @api recordId;

    @wire(fetchResourceAbsence, { resourceAbsenceId: '$recordId'  })
    absence;

    @wire(fetchUserTimeZone, {})
    _timezone;
    get timezone() {
        return this._timezone;
    }

    get resourceurl() {
        return '/'+this.absence.data.ResourceId;
    }

}