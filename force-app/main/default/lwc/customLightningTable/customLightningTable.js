import LightningDatatable from 'lightning/datatable';
import customTemplate from './customTemplate.html';


export default class CustomLightningTable extends LightningDatatable {

    static customTypes = {
        customUrl: {
            template: customTemplate,
            // Provide template data here if needed
            typeAttributes: ['field1', 'field2','field3'],
        }
       //more custom types here
    };
 
 
 }