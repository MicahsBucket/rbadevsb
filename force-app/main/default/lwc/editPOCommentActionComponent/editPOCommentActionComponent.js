import { LightningElement, api,track, wire } from 'lwc';
import getPOComments from '@salesforce/apex/EditPOCommentsActionController.getPOComments';
import savePOComments from '@salesforce/apex/EditPOCommentsActionController.savePOComments';

export default class EditPOCommentActionComponent extends LightningElement {
    @api recordId;
    @track currentComments;
    @track requestedShipDate;
    @track disableRequestShipDateEdit = false;
    @track estimatedShipDate;
    @track confirmationNumber;
    @track invoiceNumber;
    @track tax;
    @track processType;
    @track isExpediteShipping = false;
    @track isEstShipDateRequired = false;

    connectedCallback(){
       this.getPOInfo();
    }

    getPOInfo(){
        getPOComments({recordId: this.recordId })
        .then((result) => {
            console.log('result', result);
            this.currentComments = result.Comments__c;
            this.requestedShipDate = result.Requested_Ship_Date__c;
            this.estimatedShipDate = result.Estimated_Ship_Date__c;
            this.confirmationNumber = result.Confirmation_Number__c;
            this.invoiceNumber = result.Invoice_Number__c;
            this.tax = result.Tax__c;
            if(result.Status__c == 'Released' || result.Status__c == 'Confirmed' || result.Status__c == 'Partially Received' || result.Status__c == 'Received'){
                this.disableRequestShipDateEdit = true;
                if(result.Status__c == 'Confirmed' || result.Status__c == 'Received'){
                    this.isEstShipDateRequired = true;
                }
                else{
                    this.isEstShipDateRequired = false; 
                }
            }else{
                this.disableRequestShipDateEdit = false;
            }
            if(result.Processing_Type__c == 'Expedited'){
                this.isExpediteShipping = true;
            }
            else{
                this.isExpediteShipping = false;
            }
        })
        .catch((error) => {
            console.log('Edit Purchase Order Comments Error', error);
        });
    }
    
    newComments(event){
        console.log('comments: ', event.target.value);
        this.currentComments = event.target.value;
    }

    newRequestedShipDate(event){
        console.log('requestedShipDate: ', event.target.value);
        this.requestedShipDate = event.target.value;
    }

    newEstimatedShipDate(event){
        console.log('newEstimatedShipDate: ', event.target.value);
        this.estimatedShipDate = event.target.value;
    }

    newConfirmationNumber(event){
        console.log('newConfirmationNumber: ', event.target.value);
        this.confirmationNumber = event.target.value;
    }

    newTax(event){
        console.log('newTax: ', event.target.value);
        this.tax = event.target.value;
    }

    newInvoiceNumber(event){
        console.log('newInvoiceNumber: ', event.target.value);
        this.invoiceNumber = event.target.value;
    }

    handleExpediteCheck(event){
        console.log('Expedited: ' +event.target.checked)
        if(event.target.checked == true){
            this.processType = 'Expedited';
        }
        else{
            this.processType = 'Standard';
        }
    }
    saveNewComments(){
        console.log('newCommentsToSave: ', this.currentComments );
        var showSpinner = new CustomEvent('ShowSpinner');
        this.dispatchEvent(showSpinner);
        if((this.estimatedShipDate != null && this.isEstShipDateRequired == true) || (this.estimatedShipDate == null && this.isEstShipDateRequired == false)){
                        savePOComments({recordId: this.recordId, 
                            comments: this.currentComments, 
                            requestedShipDate: this.requestedShipDate, 
                            estimatedShipDate: this.estimatedShipDate,
                            confirmationNumber: this.confirmationNumber,
                            invoiceNumber: this.invoiceNumber,
                            tax: this.tax,
                            processingType: this.processType
                        })
            .then((result) => {
                console.log('result', result);
                this.currentComments = result.Comments__c;
                var hideSpinner = new CustomEvent('HideSpinner');
                this.dispatchEvent(hideSpinner);
                var closeQuickAction = new CustomEvent('CloseQuickAction');
                this.dispatchEvent(closeQuickAction);
                const showToastEvent = new CustomEvent('ShowToast', { detail: { message: 'Purchase Order Updated', type: 'Success' } });
                this.dispatchEvent(showToastEvent);
                var refreshPage = new CustomEvent('Refresh');
                this.dispatchEvent(refreshPage);
            })
            .catch((error) => {
                console.log('Edit Purchase Order Comments Error', error);
            });
        }
        else{
                var hideSpinner = new CustomEvent('HideSpinner');
                this.dispatchEvent(hideSpinner);
                // var closeQuickAction = new CustomEvent('CloseQuickAction');
                // this.dispatchEvent(closeQuickAction);
                const showToastEvent = new CustomEvent('ShowToast', { detail: { message: 'Estimated Ship Date Required', type: 'Error' } });
                this.dispatchEvent(showToastEvent);
        }
        
        
    }
}