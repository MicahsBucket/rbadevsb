import { LightningElement, api } from 'lwc';

export default class MapLink extends LightningElement {
    @api latitude;
    @api longitude;

    get url(){
        if(this.latitude && this.longitude){
            if(navigator.platform.indexOf("Android") !== -1){
                return `${this.latitude},${this.longitude}`;
            }
            return `maps.google.com/maps?layer=t&saddr=Current+Location&daddr=${this.latitude},${this.longitude}`;
        }
        return '';
    }

    get protocol(){
        if(
            navigator.platform.indexOf("iPhone") !== -1 ||
            navigator.platform.indexOf("iPod") !== -1 ||
            navigator.platform.indexOf("iPad") !== -1){
                return 'maps://';
        }

        if(navigator.platform.indexOf("Android") !== -1){
            return 'geo:';
        }

        return 'https://';
    }
}