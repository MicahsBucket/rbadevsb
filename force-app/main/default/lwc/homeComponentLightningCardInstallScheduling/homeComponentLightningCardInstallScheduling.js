import { LightningElement, track, wire } from 'lwc';
//import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getInstallNeededOrderList from '@salesforce/apex/HomeCompLightningCardInstallScheduling.getInstallNeededOrderList';
import getInstallScheduledOrderList from '@salesforce/apex/HomeCompLightningCardInstallScheduling.getInstallScheduledOrderList';
import getInstallConflictOrderList from '@salesforce/apex/HomeCompLightningCardInstallScheduling.getInstallConflictOrderList';
import getUnconfirmedWorkOrderList from '@salesforce/apex/HomeCompLightningCardInstallScheduling.getUnconfirmedWorkOrderList';


const InstallNeededColumns = [
    {label: 'Order Number', fieldName: 'INOrderNumberURL', type: "url", typeAttributes: {'label': { fieldName: 'INOrderNumber'}, value:{fieldName: 'INOrderNumberURL'}, 'target': '_blank', 'tooltip': {fieldName: 'INOrderNumber'}}, sortable: true}, 
    {label: 'Bill To Contact', fieldName: 'INOrderRBillToContactURL', type: "url", typeAttributes: {'label': { fieldName: 'INOrderRBillToContactName'}, value:{fieldName: 'INOrderRBillToContactURL'}, target: '_blank', 'tooltip': {fieldName: 'INOrderRBillToContactName'}}, sortable: true}, 
    {label: 'Estimated Ship Date', fieldName: 'INOEstimatedShipDate', type: "date", sortable: true}, 
    {label: 'Total Units', fieldName: 'TotalUnits', type: 'decimal', sortable: true}
    ]; 


const InstallScheduledColumns = [
    {label: 'Order Number', fieldName: 'SOrderNumberURL', type: "url", typeAttributes: {'label': { fieldName: 'SOrderNumber'}, value:{fieldName: 'SOrderNumberURL'}, 'target': '_blank', 'tooltip': {fieldName: 'SOrderNumber'}},  sortable: true}, 
    {label: 'Bill To Contact', fieldName: 'SOrderRBillToContactURL', type: "url", typeAttributes: {'label': { fieldName: 'SOrderRBillToContactName'}, value:{fieldName: 'SOrderRBillToContactURL'}, target: '_blank', 'tooltip': {fieldName: 'SOrderRBillToContactName'}}, sortable: true}, 
    {label: 'Estimated Ship Date', fieldName: 'SOEstimatedShipDate', type: 'date', sortable: true},
    {label: 'Installation Date', fieldName: 'SOInstallationDate', type: 'date', sortable: true}
    ]; 

const InstallConflictColumns = [
    {label: 'Estimated Ship Date', fieldName: 'COEstimatedShipDate', type: 'date', sortable: true},
    {label: 'Installation Date', fieldName: 'COInstallationDate', type: 'date', sortable: true}, 
    {label: 'Order Number', fieldName: 'COrderNumberURL', type: "url", typeAttributes: {'label': { fieldName: 'COrderNumber'}, value:{fieldName: 'COrderNumberURL'}, 'target': '_blank', 'tooltip': {fieldName: 'COrderNumber'}}, sortable: true}, 
    {label: 'Bill To Contact', fieldName: 'COrderBillToContactURL', type: "url", typeAttributes: {'label': { fieldName: 'COrderBillToContactName'}, value:{fieldName: 'COrderBillToContactURL'}, 'target': '_blank', 'tooltip': {fieldName: 'COrderBillToContactName'}}, sortable: true}
    ]; 

const UnconfirmedWOColumns = [
    {label: 'Work Order Number', fieldName: 'NCWorkOrderNumberURL', type: "url", typeAttributes: {'label': { fieldName: 'NCWorkOrderNumber'}, value:{fieldName: 'NCWorkOrderNumberURL'}, 'target': '_blank', 'tooltip': {fieldName: 'NCWorkOrderNumber'}}, sortable: true}, 
    {label: 'Contact', fieldName: 'NCContactURL', type: "url", typeAttributes: {'label': { fieldName: 'NCContactName'}, value:{fieldName: 'NCContactURL'}, 'target': '_blank', 'tooltip': {fieldName: 'NCContactName'}}, sortable: true}, 
    {label: 'Primary Resource', fieldName: 'NCPrimaryResourse', type: "text", sortable: true}, 
    {label: 'Scheduled Start Time', fieldName: 'NCScheduledStartTime', type: 'date', sortable: true, typeAttributes:{
        year: "numeric",
        month: "long",
        day: "2-digit",
        hour: "2-digit",
        minute: "2-digit"
    }}
    ];

export default class HomeComponentLightningCardInstallScheduling extends LightningElement {

    @track InstallNeededColumns=InstallNeededColumns;
    @track listInstallNeeded;
    @track listInstallNeededDup;
    @track inDataLength;
    @track inDataLengthWord = "items";

    @track ScheduledOrderColumns=InstallScheduledColumns;
    @track listScheduledOrder;
    @track listScheduledOrderDup;
    @track soDataLength;
    @track soDataLengthWord = "items";
  
    @track ConflictInstallColumns=InstallConflictColumns;
    @track listConflictInstall;
    @track listConflictInstallDup;
    @track ciDataLength;
    @track ciDataLengthWord = "items";

    @track UnconfirmedWOColumns=UnconfirmedWOColumns;
    @track listUnconfirmedWO;
    @track listUnconfirmedWODup;
    @track uwoDataLength;
    @track uwoDataLengthWord = "items";

    sVal = '';


    @wire(getInstallNeededOrderList, {searchKey: ''})
    wiredGetInstallNeededOrderList({ error, data }) {             
        if (data) {           
            this.listInstallNeeded =  data;
            this.listInstallNeededDup =  data;   
            this.inDataLength = data.length;
            if(this.inDataLength === 1) {
                this.inDataLengthWord = "item";
            }
        }
        else if (error) {
            this.error = error;
        }
        console.log('List Install Needed ' +JSON.stringify(data));
        console.log('List Install Needed 2 ' +JSON.stringify( this.listInstallNeeded));
        console.log('IN Length: ' + this.inDataLength);
    }

    @wire(getInstallScheduledOrderList, {searchKey: ''})
    wiredGetInstallScheduledOrderList({ error, data }) {             
        if (data) {           
            this.listScheduledOrder =  data;
            this.listScheduledOrderDup =  data;    
            this.soDataLength = data.length;
            if(this.soDataLength === 1){
                this.soDataLengthWord = "item";
             }            
        } else if (error) {
            this.error = error;
           }     
           console.log('List Scheduled Order ' +JSON.stringify(data));
           console.log('List Scheduled Order 2 ' +JSON.stringify( this.listScheduledOrder));
           console.log('SO Length: ' + this.soDataLength);         
    }

    @wire(getInstallConflictOrderList, {searchKey: ''})
    wiredGetInstallConflictOrderList({ error, data }) {             
        if (data) {           
            this.listConflictInstall =  data; 
            this.listConflictInstallDup =  data; 
            this.ciDataLength = data.length;
            if(this.ciDataLength === 1){
                this.ciDataLengthWord = "item";
             }              
        } else if (error) {
            this.error = error;
           }     
           console.log('List Conflict Install ' +JSON.stringify(data));
           console.log('List Conflict Install 2 ' +JSON.stringify( this.listConflictInstall));
           console.log('CI Length: ' + this.ciDataLength);         
    }

    @wire(getUnconfirmedWorkOrderList, {searchKey: ''})
    wiredGetUnconfirmedWorkOrderList({ error, data }) {             
        if (data) {           
            this.listUnconfirmedWO =  data; 
            this.listUnconfirmedWODup =  data; 
            this.uwoDataLength = data.length;
            if(this.uwoDataLength === 1){
                this.uwoDataLengthWord = "item";
             }              
        } else if (error) {
            this.error = error;
           }     
           console.log('List Unconfirmed WO ' +JSON.stringify(data));
           console.log('List Unconfirmed WO 2 ' +JSON.stringify( this.listUnconfirmedWO));
           console.log('UWO Length: ' + this.uwoDataLength);         
    }

    updateSearchKeyInstallNeeded(event) {
        this.sVal = event.target.value;
        this.handleSearchInstallNeeded();
    }

    handleSearchInstallNeeded() {
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getInstallNeededOrderList({
                    searchKey: this.sVal
                })
                .then(result => {
                    // set @track contacts variable with return contact list from server  
                    this.listInstallNeeded = result;
                })
                .catch(error => {
                    // display server exception in toast msg 
                    const event = new ShowToastEvent({
                        title: 'Error',
                        variant: 'error',
                        message: error.body.message,
                    });
                    this.dispatchEvent(event);
                    // reset contacts var with null   
                    this.listInstallNeeded = null;
                });
        } else {
           
            this.listInstallNeeded = this.listInstallNeededDup;
        }
    }

    installNeededSortData(fieldName, sortDirection){
        var data = JSON.parse(JSON.stringify(this.listInstallNeeded));
        
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        
                data.sort((a,b) => {
                    let valueA = key(a) ? key(a).toLowerCase() : ''; 
                    let valueB = key(b) ? key(b).toLowerCase() : '';
                    return reverse * ((valueA > valueB) - (valueB > valueA));
                });

        this.listInstallNeeded = data;
    }    
    installNeededUpdateColumnSorting(event){
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.installNeededSortData(this.sortedBy,this.sortedDirection);
    }

    updateSearchKeyScheduledOrder(event) {
        this.sVal = event.target.value;
        this.handleSearchScheduledOrder();
    }

    handleSearchScheduledOrder() {
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getInstallScheduledOrderList({
                    searchKey: this.sVal
                })
                .then(result => {
                    // set @track contacts variable with return contact list from server  
                    this.listScheduledOrder = result;
                })
                .catch(error => {
                    // display server exception in toast msg 
                    const event = new ShowToastEvent({
                        title: 'Error',
                        variant: 'error',
                        message: error.body.message,
                    });
                    this.dispatchEvent(event);
                    // reset contacts var with null   
                    this.listScheduledOrder = null;
                });
        } else {
           
            this.listScheduledOrder = this.listScheduledOrderDup;
        }
    }

    scheduledOrderSortData(fieldName, sortDirection){
        var data = JSON.parse(JSON.stringify(this.listScheduledOrder));
        
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        
                data.sort((a,b) => {
                    let valueA = key(a) ? key(a).toLowerCase() : ''; 
                    let valueB = key(b) ? key(b).toLowerCase() : '';
                    return reverse * ((valueA > valueB) - (valueB > valueA));
                });

        this.listScheduledOrder = data;
    }

    scheduledOrderUpdateColumnSorting(event){
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.scheduledOrderSortData(this.sortedBy,this.sortedDirection);
    }

    updateSearchKeyConflictInstall(event) {
        this.sVal = event.target.value;
        this.handleSearchConflictInstall();
    }

    handleSearchConflictInstall() {
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getInstallConflictOrderList({
                    searchKey: this.sVal
                })
                .then(result => {
                    // set @track contacts variable with return contact list from server  
                    this.listConflictInstall = result;
                })
                .catch(error => {
                    // display server exception in toast msg 
                    const event = new ShowToastEvent({
                        title: 'Error',
                        variant: 'error',
                        message: error.body.message,
                    });
                    this.dispatchEvent(event);
                    // reset contacts var with null   
                    this.listConflictInstall = null;
                });
        } else {
           
            this.listConflictInstall = this.listConflictInstallDup;
        }
    }

    conflictInstallSortData(fieldName, sortDirection){
        var data = JSON.parse(JSON.stringify(this.listConflictInstall));
        
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        
                data.sort((a,b) => {
                    let valueA = key(a) ? key(a).toLowerCase() : ''; 
                    let valueB = key(b) ? key(b).toLowerCase() : '';
                    return reverse * ((valueA > valueB) - (valueB > valueA));
                });

        this.listConflictInstall = data;
    }

    conflictInstallUpdateColumnSorting(event){
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.conflictInstallSortData(this.sortedBy,this.sortedDirection);
    }

    updateSearchKeyUnconfirmedWO(event) {
        this.sVal = event.target.value;
        this.handleSearchUnconfirmedWO();
    }

    handleSearchUnconfirmedWO() {
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getUnconfirmedWorkOrderList({
                    searchKey: this.sVal
                })
                .then(result => {
                    // set @track contacts variable with return contact list from server  
                    this.listUnconfirmedWO = result;
                })
                .catch(error => {
                    // display server exception in toast msg 
                    const event = new ShowToastEvent({
                        title: 'Error',
                        variant: 'error',
                        message: error.body.message,
                    });
                    this.dispatchEvent(event);
                    // reset contacts var with null   
                    this.listUnconfirmedWO = null;
                });
        } else {
           
            this.listUnconfirmedWO = this.listUnconfirmedWODup;
        }
    }

    unconfirmedWOSortData(fieldName, sortDirection){
        var data = JSON.parse(JSON.stringify(this.listUnconfirmedWO));
        
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        
                data.sort((a,b) => {
                    let valueA = key(a) ? key(a).toLowerCase() : ''; 
                    let valueB = key(b) ? key(b).toLowerCase() : '';
                    return reverse * ((valueA > valueB) - (valueB > valueA));
                });

        this.listUnconfirmedWO = data;
    }    
    unconfirmedWOUpdateColumnSorting(event){
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.unconfirmedWOSortData(this.sortedBy,this.sortedDirection);
    }


}