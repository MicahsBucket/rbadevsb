/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable vars-on-top */
/* eslint-disable no-alert */

import { LightningElement, wire, track, api} from 'lwc';
import { getPicklistValues, getObjectInfo} from 'lightning/uiObjectInfoApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getTerritoryName from '@salesforce/apex/GanttAction_ResourceAbsencesController.getTerritoryName';
import getServiceTerritoryMemberTimezone from '@salesforce/apex/GanttAction_ResourceAbsencesController.getServiceTerritoryMemberTimezone';
import getServiceResourcesForTerritory from '@salesforce/apex/GanttAction_ResourceAbsencesController.getServiceResourcesForTerritory';
import createAbsenceRecords from '@salesforce/apex/GanttAction_ResourceAbsencesController.createAbsenceRecords';
import RESOURCEABSENCE_OBJECT from '@salesforce/schema/ResourceAbsence';
import START_FIELD from '@salesforce/schema/ResourceAbsence.Start';
import END_FIELD from '@salesforce/schema/ResourceAbsence.End';
import TYPE_FIELD from '@salesforce/schema/ResourceAbsence.Type';
import DESCRIPTION_FIELD from '@salesforce/schema/ResourceAbsence.Description';
import RESOURCEID_FIELD from '@salesforce/schema/ResourceAbsence.ResourceId';
import GANTTLABEL_FIELD from '@salesforce/schema/ResourceAbsence.FSL__GanttLabel__c';
import TIMEZONE from '@salesforce/i18n/timeZone';

const dataTableColumns = [
    {label: 'Resource Name', fieldName: 'ServiceResourceName'},
    {label: 'Territory Membership Type', fieldName: 'MembershipType', initialWidth: 300}
];

export default class ServiceResourceAbscenses extends LightningElement {
    @api srId;
    @api stmId;
    @api initialStart;
    @api initialEnd;

    @track columns = dataTableColumns;
    @track resourceData;
    selectedRowIds;
    @track error;
    @track timezoneName;

    @track startDateTime;
    @track endDateTime;
    selectedRows;
    description;
    absenceType;
    ganttLabel;

    @track inputInvalid;
    @track showSpinner = true;

    @wire(getServiceTerritoryMemberTimezone, {serviceTerritoryMemberId: '$stmId'})
    stmTimeZone;
    
    @wire(getTerritoryName,{serviceTerritoryMemberId: '$stmId'})
    territoryName;

    @wire(getServiceResourcesForTerritory,{
        serviceTerritoryMemberId: '$stmId', 
        startDateTime: '$startDateTime', 
        endDateTime: '$endDateTime'
    })
    function({error,data}) {
        if (error) {
            console.log('error: ' + error);
            this.showSpinner = false;
        } else if (data) {
            var tempData = []; // 'data' is a proxy and can't be directly edited
            for (var i = 0; i < data.length; i++) {
                tempData.push(Object.assign({},data[i]));
                tempData[i].ServiceResourceName = data[i].ServiceResource.Name;

                switch(tempData[i].TerritoryType) {
                    case 'P':
                        tempData[i].MembershipType = 'Primary';
                        break;
                    case 'S':
                        tempData[i].MembershipType = 'Secondary';
                        break;
                    case 'R':
                        tempData[i].MembershipType = 'Relocation';
                        break;
                    default:
                }
                if (!this.selectedRows && tempData[i].ServiceResourceId === this.srId) {
                    this.selectedRows = [tempData[i]];
                }
            }
            this.resourceData = tempData;
            if (!this.selectedRowIds) {
                this.selectedRowIds = [this.srId];
            } 
            this.showSpinner = false;
        }
        
    }

    resourceAbsenceObject = RESOURCEABSENCE_OBJECT;
    startField = START_FIELD;
    endField = END_FIELD;
    typeField = TYPE_FIELD;
    descriptionField = DESCRIPTION_FIELD;
    resourceId = RESOURCEID_FIELD;

    @wire(getObjectInfo, { objectApiName: RESOURCEABSENCE_OBJECT })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: TYPE_FIELD })
    typePickListValues;

    connectedCallback() {
        this.startDateTime = new Date(this.initialStart);
        this.endDateTime = new Date(this.initialEnd);
        this.checkInput();
        switch(TIMEZONE) {
            case 'America/Indiana/Indianapolis':
                this.timezoneName = 'Eastern (Indiana)';
                break;
            case 'America/New_York':
                this.timezoneName = 'Eastern';
                break;
            case 'America/Chicago':
                this.timezoneName = 'Central';
                break;
            case 'America/Denver':
                this.timezoneName = 'Mountain';
                break;
            case 'America/Los_Angeles':
                this.timezoneName = 'Pacific';
                break;
            case 'America/Phoenix':
                this.timezoneName = 'Mountain (Arizona)';
                break;
            default:
                this.timezoneName = TIMEZONE;
        }
    }

    get startDateISO8601() {
        return this.startDateTime.toISOString();
    }

    get endDateISO8601() {
        return this.endDateTime.toISOString();
    }

    get hasActiveResources() {
        if (typeof this.resourceData === 'undefined' || this.resourceData === null || this.resourceData.length === 0) {
            return false;
        } 
        return true;
    }

    handleStartChange(event) {
        this.startDateTime = new Date(event.target.value);
        this.endDateTime = this.endDateTime > this.startDateTime ? this.endDateTime : this.startDateTime;
        this.checkInput();
    }

    handleEndChange(event) {
        this.endDateTime = new Date(event.target.value);
        this.startDateTime = this.endDateTime > this.startDateTime ? this.startDateTime : this.endDateTime;
        this.checkInput();
    }

    handleDescriptionChange(event) {
        this.description = event.target.value;
        this.checkInput();
    }

    handleTypeChange(event) {
        this.absenceType = event.target.value;
        this.checkInput();
    }

    handleGanttLabelChange(event) {
        this.ganttLabel = event.target.value;
        this.checkInput();
    }

    trackSelectedRows(event) {
        this.selectedRows = event.detail.selectedRows;
        this.checkInput();
    }

    checkInput() {
        var isInvalid = false;

        // Validate that required fields are populated
        if (typeof this.startDateTime === 'undefined' || this.startDateTime === null) {isInvalid = true;}
        if (typeof this.endDateTime === 'undefined' || this.endDateTime === null) {isInvalid = true;}
        if (typeof this.description === 'undefined' || this.description === null || this.description.length === 0) {isInvalid = true;}
        if (typeof this.absenceType === 'undefined' || this.absenceType === null || this.absenceType.length === 0) {isInvalid = true;}
        if (typeof this.selectedRows === 'undefined' || this.selectedRows === null || this.selectedRows.length === 0) {isInvalid = true;} 
        if (typeof this.ganttLabel === 'undefined' || this.ganttLabel === null || this.ganttLabel.length === 0) {isInvalid = true;} 
        
        // Validate inputs
        if (this.endDateTime <= this.startDateTime) {isInvalid = true;}

        this.inputInvalid = isInvalid;
    }

    createResourceAbsence(){
        if (!this.selectedRows || this.selectedRows.length === 0) {return;}
        
        var confirmResult = confirm('Insert resource absences for ' + this.selectedRows.length + ' resources?\n\n' +
            'Absences can only be modified or deleted individually after they have been inserted.');
        if (!confirmResult) {
            return;
        } 
        this.showSpinner = true;

        let newObjects = [];
        if (this.selectedRows.length > 0) {            
            for(let i = 0; i < this.selectedRows.length; i++) {
                const newFields = {};
                newFields[START_FIELD.fieldApiName] = this.startDateTime;
                newFields[END_FIELD.fieldApiName] = this.endDateTime;
                newFields[TYPE_FIELD.fieldApiName] = this.absenceType;
                newFields[DESCRIPTION_FIELD.fieldApiName] = this.description;
                newFields[RESOURCEID_FIELD.fieldApiName] = this.selectedRows[i].ServiceResourceId;
                newFields[GANTTLABEL_FIELD.fieldApiName] = this.ganttLabel;
                newObjects.push(newFields);
            }
        }

        createAbsenceRecords({ resourceAbsences: newObjects,  serviceTerritoryMemberId: this.stmId})
        .then(result => {
            this.showSpinner = false;
            var message = '';
            if (result.successfulRecords.length > 0) {
                message += result.successfulRecords.length + ' resource absence(s) created successfully!';
            }
            if (result.errors.length > 0) {
                message += '\n\n' + result.errors.length + ' resource absence(s) could not be created.';
            }
            alert(message);
            
            parent.postMessage('closeLightbox','*'); // close the Gantt action VF page
        })
        .catch(error => {
            alert('Error occurred while attempting to insert resources.  ' +
                'Please refresh the Dispatcher Console and contact an administrator if the issue persists.');
            this.showSpinner = false;
        })
    }

    

}