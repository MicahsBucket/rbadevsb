import { LightningElement, api, track } from 'lwc';
import getConfigurationData2 from '@salesforce/apex/DaylConfigurationOutputsCallout.getConfigurationData2';

const columns = [
    { label: 'Description', fieldName: 'Description' },
    { label: 'Cost', fieldName: 'SimpleValue', type: 'currency' }
];
export default class ConfigSummary extends LightningElement {
    @track returnedConfigData;
    @track configData;
    @track ProductDescription;
    @track ConfiguredPrice;
    @track UnitID;
    @track PricingDetails;
    @api recordId;
    columns = columns;

    connectedCallback(){
        console.log('this.returnedConfigData ConfigSummary= ', this.returnedConfigData);
        console.log('this.recordId ConfigSummary= ', this.recordId);
        this.getReturnData();
    }

    getReturnData(){
        getConfigurationData2({OrderitemId: this.recordId})
        .then((result) => {
            console.log('getConfigurationData result', JSON.stringify(JSON.parse(result.jsonResponse)));
            this.returnedConfigData = JSON.stringify(JSON.parse(result.jsonResponse));
            if(this.returnedConfigData != null && this.returnedConfigData != 'undefined'){
                this.openConfigData = true;
                this.showSplitLines = false;
                this.currentPathStep = '3';
            }
            else{
                this.openConfigData = false;
            }
            this.configData = JSON.parse(this.returnedConfigData);
            for(let i=0; i< this.configData.CommitParameters.length; i++ ){
                if(this.configData.CommitParameters[i].Name == 'CfgDescription'){
                    this.ProductDescription = this.configData.CommitParameters[i].SimpleValue;
                }
                if(this.configData.CommitParameters[i].Name == 'ConfiguredPrice'){
                    this.ConfiguredPrice = this.configData.CommitParameters[i].SimpleValue;
                }
                if(this.configData.CommitParameters[i].Name == 'UnitID'){
                    this.UnitID = this.configData.CommitParameters[i].SimpleValue;
                }
            }
            this.PricingDetails = this.configData.PricingDetails;
        })
        .catch((error) => {
            console.log('getConfigurationData Error', error);
        });

    }
}