import { LightningElement, wire,api } from 'lwc';
import getChangeHistory from '@salesforce/apex/GetChangeHistory.getChangeHistory';

export default class ChangeHistory extends LightningElement {
    @api recordId;

    @wire(getChangeHistory,{ orderItemId: '$recordId'})
    changeHistories;
}