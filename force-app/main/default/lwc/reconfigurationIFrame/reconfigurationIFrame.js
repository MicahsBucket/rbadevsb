import { LightningElement, api, track } from 'lwc';
import getUrlforReconfiguration from '@salesforce/apex/DaylConfigurationCallout.getUrlforReconfiguration';
import setOrderItemEligibleforReconfiguration from '@salesforce/apex/DaylConfigurationCallout.setOrderItemEligibleforReconfiguration';

export default class ReconfigurationIFrame extends LightningElement {
    @api recordId;
    @api inCommunity;
    @track cpqNamespace = 'POC';
    @track cpqName = 'IED';
    @api redirectUrl;
    @track daylightResponse;
    connectedCallback(){
        console.log('ReconfigurationIFrame: ', this.recordId);
        console.log('ReconfigurationIFrame redirectUrl: ', this.redirectUrl);
        console.log('ReconfigurationIFrame inCommunity: ', this.inCommunity);
        
        if(this.recordId != null){
            setOrderItemEligibleforReconfiguration({orderItemId:this.recordId})
            .then((result) => {
                console.log('setOrderItemEligibleforReconfiguration: ', result);
                if(result == 'Success'){
                    getUrlforReconfiguration({namespace:this.cpqNamespace, name:this.cpqName, orderItemId:this.recordId, redirectUrl: this.redirectUrl })
                    .then((result) => {
                        console.log('data.jsonResponse getUrlforReconfiguration?!?', JSON.parse(JSON.stringify(result.jsonResponse)));
                        let response = {};
                        response = JSON.parse(JSON.stringify(result.jsonResponse));
                        console.log('start config data ', JSON.parse(JSON.stringify(response)));
                        this.daylightResponse = JSON.parse(JSON.stringify(response));
                        // this.showIframe = true;
    
                    })
                    .catch((error) => {
                        this.error = error;
                        console.log('error in start config callback ', error.jsonResponse);
                    });
                }
            })
            .catch((error) => {
                this.error = error;
                console.log('error in start config callback ', error.jsonResponse);
            });
        }
    }
    renderedCallback(){
        console.log('ReconfigurationIFrame renderedCallback ');
    }
}