import { LightningElement, api, wire, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {handleLiftPulls} from 'c/createOrderShareCode';
import {handleWideBar} from 'c/createOrderShareCode';
import {handleGlazingValues} from 'c/createOrderShareCode';
import {invokeSpecialOptions} from 'c/createOrderShareCode';
import {FIELDS} from 'c/createOrderShareCode';
import {sillAngleZeroThruFourteenList} from 'c/createOrderShareCode';
import {singleLegShapes} from 'c/createOrderShareCode';
import {sillAngleFsOnlyList} from 'c/createOrderShareCode';
import {pivotalIdsForMakability} from 'c/createOrderShareCode';
import {controlQuantityForMasterProduct} from 'c/createOrderShareCode';
import getProductConfig from '@salesforce/apex/EditOrderPageLwcController.getProductConfiguration';
import saveOrderItem from '@salesforce/apex/EditOrderPageLwcController.saveOrderItem';
import performMakabilityCheck from '@salesforce/apex/EditOrderPageLwcController.performMakabilityCheck';
import getProductFieldcontrols from '@salesforce/apex/EditOrderPageLwcController.getProductFieldcontrols';
import getProductFieldControlDependency from '@salesforce/apex/EditOrderPageLwcController.getProductFieldControlDependency';
import getOrderItemForEdit from '@salesforce/apex/EditOrderPageLwcController.getOrderItemForEdit';
import getShowOrderButton from '@salesforce/apex/OrderProductListController.showProductButton';
import getIsSandboxOrg from '@salesforce/apex/UtilityMethods.isSandboxOrg';	
import getIsrForceUser from '@salesforce/apex/UtilityMethods.isrForceUser';	
import { getRecord } from 'lightning/uiRecordApi'; 
import productInteriorColors from '@salesforce/label/c.ProductInteriorColor';
import productScreenType from '@salesforce/label/c.ProductScreenType';
import productCodes from '@salesforce/label/c.ProductCodes';
import productCodesForGrilleTypes from '@salesforce/label/c.ProductCodesForGrilleTypes';
import glazingTypes from '@salesforce/label/c.GlazingTypes';
import grilleStyle from '@salesforce/label/c.GrilleStyle';
import glazingNoPattern from '@salesforce/label/c.GlazingPattern';
import sashOperation from '@salesforce/label/c.Sash_Operation';
import screenSize from '@salesforce/label/c.Screen_Size';
import screenType from '@salesforce/label/c.Screen_Type';
import glidingProductCode from '@salesforce/label/c.Gliding_Product_Code';
import costalPerfrmCategory from '@salesforce/label/c.CoastalPerformanceCategory';
import glazingConfigurationsForCoastal from '@salesforce/label/c.GlazingConfigurationsForCoastal';
import productCodesForLiftPulls from '@salesforce/label/c.ProductCodesForLiftPulls';
import nonApplicableLiftPullsForCoastal from '@salesforce/label/c.NonApplicableLiftPullsForCoastal';

const sfClassicReturnBaseUrl = '/';
const sfClassicSaveAndNewBaseUrl = '/apex/EditOrderProduct';
const sfClassicSaveAndViewBaseUrl = '/apex/ReadOnlyOrderProductPage';
const sfAROReturnBaseUrl = '/rForceARO/';
const sfAROSaveAndNewBaseUrl = '/rForceARO/apex/EditOrderProduct';
const sfAROSaveAndViewBaseUrl = '/rForceARO/apex/ReadOnlyOrderProductPage';

let partnerResult;
let sandboxResult;
export default class CreateDayLightProductPage extends NavigationMixin(LightningElement) {
 
    DEFAULTPFC = {
        Product2Id : true,
        OrderId : true,
    };
 //   @track orderItemId; duplicate.
    @track editMode = false;
    @track orderItem;
    @track activeSections = ['Main', 'Window-Configuration','Order-Information','Sales-Information'];
    @track error; 
    @track setOrderId = true;
    @track populateFieldsFromOi = false;
    @track fieldsPopulated = false; 
    @track setInteriorColorValue = false;
    @track setGlazingPatternValue=false;
    @track disabledFields={}; // used to track fields disabled by pfcds. to better manage undisabling them.
    @track makabilityModel = false;
    @track passedMakability;
    @track performMakabilityChecks = true;
    @track makabilityErrors = [];
    @track showMakabilitySpinner = false;
    @track productRecordId; // get from product lookup field - ex speciality window id = '01t61000004AIg8AAG';
    @api orderId;
    @api configId; 
//    @api purchaseOrderId; // possibly not needed. currently not in use
    @api orderItemId; 
    @track productFieldControl = this.DEFAULTPFC; // determines what fields will appear on the page.
    @track productFieldControlDependencyMap;     // pfcd map used to contain results of getAllPfcds.
    @track pfcResponse;
    @track screenColorDefault=''; // interior or exterior color from prod config

// isMasterProduct && runMakabilityType used to determine if makabilty should run. isMasterProduct to be deprecated.
    @track isMasterProduct = false;
    @track runMakabilityType;


    @track productRecord = {VendorName:'placeholder'}; // added 8/27 used for validating the vendor when trying to assign this product to a retail purchase order. 
    @track setRequiredFieldsCompleted = false;
    @track productRecordSet = false; 
    @track productName;
    @track performSetRequiredFields = false;
    @track setExteriorTrimListValues = false; // used to set drop down values for exterior trim in edit mode.

    @api inRop;
    @api returnBaseUrl;
    @api saveAndNewBaseUrl;
    @api saveAndViewBaseUrl;
	@api showButton;
	@track isSandboxOrg;
	@track isrForceUser;


    /*
    *      Arrays for Combobox fields         
    */

    @track exteriorColorMap = {}; // used to determine interior colors. key = exterior color , value = interior color options
    @track interiorColorList= []; // populated based on exterior color selection
    @track glazingtypeMap={};
    @track glazingPatternListS1=[];
    @track glazingPatternListS2=[];
    @track glazingPatternListS3=[];    
    @track fractionList= []; 
    @track astragalColorList=[];
    @track brickmouldLocationList=[];
    @track casingLocationList=[];
    @track casingSpeciesList=[];
    @track casingProfileList=[];
    @track checkrailStyleList=[];
    @track dripCapList=[];
    @track exteriorColorList= []; 
    @track ejColorList=[];
    @track ejSpeciesList=[];
    @track ejThicknessList=[];
    @track ejWallDepthList=[];
    @track exteriorGrilleColorList=[];
    @track exteriorTrimList=[];
    @track originalExteriorTrimList=[];
    @track exteriorTrimColorList=[];
    @track frameTypeList= []; 
    @track glassPatternList=[];
    @track glazingList=[];
    @track grilleStyleList=[];
    @track grillePatternList=[];
    @track hardwareColorList=[];
    @track hardwareFinishList=[];
    @track hardwareStyleList=[];
    @track hardwareOptionList=[];
    @track hubsList=[];
    @track interiorGrilleColorList=[];
    @track interiorTrimList=[];
    @track installTrackLocationList=[];
    @track jambLinerColorList=[];
    @track litesHighList=[];
    @track litesWideList=[];
    @track liftsPullsList=[];
    @track locksSashList=[];
    @track mullSequenceList=[];
    @track sashOperationList=[];
    @track sashRatioList=[];
    @track screenSizeList=[];
    @track screenTypeList=[];
    @track sillAngleList=[];
    @track specialShapeList= []; 
    @track spokesList=[];
    @track stormWindowColorList=[]; // not used
    @track screenColorList=[];
    @track fingerLiftsList=[]; // not used
    @track positiveForceList=[];
    @track negativeForceList=[];
    @track frameNotchConfigList=[];
    @track performanceCategoryList=[];

    @track grillePatternConfigs=[]; // list of grille pattern configurartion records returned from product config callout. 
    
    // show product description logic.
    @track showProductDescription = false;
    @track productDescription = '';
    @track focusOnProduct = false;
    @track isDisableScreenType=false;
    @track productCode='';
    @track screenTypeListClone=[];
    @track grilleStyleListClone=[];
    @track counterForRenderedCallback=0;
    @track counterForRenderedGlazingCallback=0;
    @track counterForGlidingPrCode=0;
    @track counterForGlazingType=0;
    @track counterForLiftPulls=0;
    @track counterForWideBar=0;
    @track counterForQuantity=0;
    @track requiredFields =[];
    @track isDPUpgradeMandatory = undefined;
    @track isPerfCategoryMandatory = undefined;
    @track sizedetailConfigClone;
    @track glazingPatternConfigurationsClone;
    @track liftsPullsListClone=[];
    customlabels ={productInteriorColors,productScreenType,productCodes,productCodesForGrilleTypes,glazingTypes,grilleStyle,glazingNoPattern,sashOperation,screenSize,screenType,glidingProductCode,costalPerfrmCategory,glazingConfigurationsForCoastal,productCodesForLiftPulls,nonApplicableLiftPullsForCoastal};

    //*********************************************************************************** */
    //********************************CallBacks And Wires ********************************* 
    //*********************Section for all inital page load functions  ********************
    //******************** and anything that runs on page render/update********************
    //******* */check for orderId or orderItem id and set up the page appropriately********
    //*********************************************************************************** */
    connectedCallback(){
        console.log('this order id - connected callback', this.orderId);
        console.log('this order item id - connected callback', this.orderItemId);

		getIsSandboxOrg ().then(result => {
		console.log('Sandbox Result', result);
		sandboxResult = result;
			this.isSandboxOrg = sandboxResult;	
			

		getIsrForceUser ().then(res => {
		console.log('Partner Result', res);
		partnerResult = res;
			this.isrForceUser = partnerResult;
		
		}).catch(error => {
			this.error = error;
			console.log('error in callback for get Partner User', error);

		}).then(() => {
		console.log('Sandbox: ', this.isSandboxOrg);
		console.log('rForce: ', this.isrForceUser);
        console.log('new base url for edit navigation = ', this.saveAndNewBaseUrl);
        console.log('new base url for read only navigation = ', this.saveAndViewBaseUrl);
        console.log('new base url for cancel navigation = ', this.returnBaseUrl);
			
				if(this.returnBaseUrl === undefined){
					if(this.isSandboxOrg === true && this.isrForceUser === true) {
						this.returnBaseUrl = sfAROReturnBaseUrl;
					} else {
						this.returnBaseUrl = sfClassicReturnBaseUrl;
					console.log('New Return Base URL: ', this.returnBaseUrl);
						}
					}
				if(this.saveAndNewBaseUrl === undefined){
					if(this.isSandboxOrg === true && this.isrForceUser === true) {
					this.saveAndNewBaseUrl = sfAROSaveAndNewBaseUrl;
					} else { 
					this.saveAndNewBaseUrl = sfClassicSaveAndNewBaseUrl;
					console.log('New Save and New Base URL: ', this.saveAndNewBaseUrl);
					}
					}
				if(this.saveAndViewBaseUrl === undefined){
				if(this.isSandboxOrg === true && this.isrForceUser === true) {
				this.saveAndViewBaseUrl = sfAROSaveAndViewBaseUrl;
					} else {
				this.saveAndViewBaseUrl = sfClassicSaveAndViewBaseUrl;
				console.log('New Save and View Base URL: ', this.saveAndViewBaseUrl);
					}
					}
                    console.log('base url for edit navigation = ', this.saveAndNewBaseUrl);
                    console.log('base url for read only navigation = ', this.saveAndViewBaseUrl);
                    console.log('base url for cancel navigation = ', this.returnBaseUrl);
		
	
		});


		}).catch(error => {
			this.error = error;
			console.log('error in callback for get Sandbox Org', error);
		});


        console.log('hit connectedCallback in ROP', this.inRop);
        console.log('hit connectedCallback', this.recordId);

		getShowOrderButton ({ orderId: this.orderId }).then(result => {
				this.showButton = result;
								}).catch(error => {
									this.error = error;
									console.log('error in callback for get order revenue recognized date', error);
								});



        // if orderitem id, page is in edit mode. 
        if(this.orderItemId){
            this.showMakabilitySpinner = !this.showMakabilitySpinner;
            console.log('order Item id in connected callback ', this.orderItemId);
            this.editMode = true;
            this.setInteriorColorValue = true;
            this.setGlazingPatternValue=true;
            getOrderItemForEdit({orderItemId:this.orderItemId})
            .then(result => {
                let res = {};
                console.log('getProductFieldControlDependency data ', result.jsonResponse);
                res = JSON.parse(result.jsonResponse);
                this.orderItem = res;
                this.productRecordId = res.Product2Id; // should kick off wired methods below.
                this.productName = res.Product2.Name; // populates the product2 lookup custom component
                this.populateFieldsFromOi = true;
                this.setExteriorTrimListValues = true;
                if(res.Interior_Color__c && res.Product2 && res.Product2.ProductCode) {
                    this.productCode = res.Product2.ProductCode;
                    this.setScreenType(res.Interior_Color__c);
                }
                if(res.Sash_Operation__c && res.Screen_Type__c && res.Product2 && res.Product2.ProductCode) {
                    this.productCode = res.Product2.ProductCode;
                    this.checkGLMakeability(this.productCode,res.Sash_Operation__c,res.Screen_Type__c);
                }
        //    this.orderId = res.OrderId;
            console.log('order item in get order item', this.orderItem);
            })
            .catch(err => {
            this.error = err;
            console.log('error in getOrderItem callback ', err);
            });
        }
    }
    count = 0;
    renderedCallback(){
        // rendered callback runs everytime something is rendered on page. 
        this.count++;
        console.log('hit rerender', this.count);
        if (this.setOrderId) {
            if (this.orderId) {
                let orderField = this.template.querySelector('lightning-input-field.OrderId');
                console.log('order field in connected callback', orderField);
                if(orderField !== null){
                    orderField.value = this.orderId;
                    this.setOrderId = false;
                }
            }
            
        }
        //populateFieldsFromOi set in connected callback above.
        if (this.populateFieldsFromOi) {
            console.log('hit toggle change history');
            this.toggleSection('Change-History');
        }
        if(this.performSetRequiredFields){
            this.setRequiredFields(this.pfcResponse);
        }
        if(this.setRequiredFieldsCompleted === true && this.productRecordSet === true && this.inRop === true){
            this.checkVendorType(this.productRecord);
            this.setRequiredFieldsCompleted = false;
            this.productRecordSet = false;
        }
        console.log('hit rerender - length of interiorColorList', this.interiorColorList.length);
        if(this.exteriorColorMapCreated === true && this.interiorColorList.length < 2){ //&& Object.keys(this.exteriorColorMap).length > 2
            console.log('hit third times the charm on interior color');
            this.populateInteriorColor();
            this.exteriorColorMapCreated = false;
        }
        console.log('hit rerender - length of glazingPatternList', this.glazingPatternListS1.length);
        if(this.glazingtypeMapCreated === true && this.glazingPatternListS1.length < 2){
            console.log('inside glazing pattern');
            this.populateGlazingPattern();
            this.glazingtypeMapCreated= false; 
        }

        let interiorColor = this.template.querySelector('lightning-combobox.Interior_Color__c'); 
                if(interiorColor && interiorColor.value && interiorColor.value !='' && this.orderItemId && this.counterForRenderedCallback==0) {
                    this.counterForRenderedCallback++;
                   this.setScreenType(interiorColor.value);
                }
               
                if(this.productCode && this.template.querySelector('lightning-combobox.Sash_Operation__c') && this.template.querySelector('lightning-combobox.Sash_Operation__c').value && this.template.querySelector('lightning-combobox.Screen_Type__c') &&this.template.querySelector('lightning-combobox.Screen_Type__c').value && this.counterForGlidingPrCode==0)  {
                    this.counterForGlidingPrCode++;
                    this.checkGLMakeability(this.productCode,this.template.querySelector('lightning-combobox.Sash_Operation__c').value,this.template.querySelector('lightning-combobox.Screen_Type__c').value);
                }
        //RTTFRONT-717 : Restrict New Glazing Types for Coastal
        if(this.template.querySelector('lightning-combobox.Glazing_S1__c') && this.template.querySelector('lightning-combobox.Glazing_S1__c').value && this.counterForGlazingType==0) {
            this.counterForGlazingType++;
            this.glazingList = this.createGlazingTypeMap(this.glazingPatternConfigurationsClone);
        }
        if(this.template.querySelector('lightning-combobox.Performance_Category__c') && this.template.querySelector('lightning-combobox.Performance_Category__c').value &&this.productCode && this.liftsPullsListClone && this.liftsPullsListClone.length>0 && this.counterForLiftPulls==0) {
            this.counterForLiftPulls++;
             this.liftsPullsList = handleLiftPulls(this.template.querySelector('lightning-combobox.Performance_Category__c')
             ,this.productCode,[...this.liftsPullsListClone],this.template.querySelector('lightning-combobox.Lifts_Pulls__c'));
              
            
        }

        //RTTFRONT-667
        if(this.template.querySelector('lightning-combobox.Performance_Category__c') &&this.template.querySelector('lightning-combobox.Performance_Category__c').value && this.template.querySelector('lightning-combobox.Grille_Style__c') &&this.template.querySelector('lightning-combobox.Grille_Style__c').value && this.template.querySelector('lightning-input.Wide_Bar__c') && this.counterForWideBar==0) {
        this.counterForWideBar++;
        handleWideBar(this.template.querySelector('lightning-combobox.Performance_Category__c'),this.template.querySelector('lightning-combobox.Grille_Style__c'),this.template.querySelector('lightning-input.Wide_Bar__c'));
        }  

    }

    @wire(getProductFieldcontrols,{productId:'$productRecordId'})
    wiredProductFieldControls({error,data}){
        if (error){
            this.error = error;
            console.log('error in getProductFieldcontrols callback ', error.jsonResponse);
        } else if (data){
            let response = {};
            console.log('The product recId: '+this.productRecordId);
            // console.log('product field control data ', JSON.parse(data.jsonResponse));
            response = JSON.parse(data.jsonResponse);
            this.pfcResponse = response;
            console.log('pfc response before creating pfcs',JSON.parse(JSON.stringify(this.pfcResponse)));
            if(this.productRecordId !== null){
                this.createProductFieldControlsObject(response);
            }
            getProductFieldControlDependency({productId:this.productRecordId})
            .then(result => {
            let res = {};
            // console.log('getProductFieldControlDependency data ', JSON.parse(data.jsonResponse));
            res = JSON.parse(result.jsonResponse);
            this.buildProductFieldDependancyMap(res);
            // EDIT MODE FIELD POPULATION BELOW
            // 1. populate all fields
            // 2. apply pfcds
            // 3. set interior color
            // 4. disable spinner that was set in connected callback.
            // if we are in edit mode. after the pfcd map has been populated above, apply the pfcds below.
            
            if(this.populateFieldsFromOi){
            //    this.applyPfcds();
                this.populateFields();
                this.populateFieldsFromOi = false;
             //   this.toggleSection('Change-History'); 
            }
            if(this.fieldsPopulated){
                console.log('hit apply pfcds');
                this.applyPfcds();

            }
            if(this.editMode && this.setInteriorColorValue){
                this.populateInteriorColor();
            }
            if(this.editMode && this.setGlazingPatternValue){
                this.populateGlazingPattern();
            }
            if(this.showMakabilitySpinner){
                this.showMakabilitySpinner = !this.showMakabilitySpinner;
            }
            //RTTFRONT-703
             controlQuantityForMasterProduct(this.isMasterProduct,this.template.querySelector('lightning-input.Quantity'));
          
            // END EDIT MODE FIELD POPULATION
            })
            .catch(err => {
            this.error = err;
            console.log('error in getProductFieldControlDependency callback ', err);
            });
        } else {
            console.log(' getProductFieldcontrols something bad happened...');
        }
    }

    @wire(getRecord, { recordId: '$productRecordId', fields: FIELDS })
    wiredRecord({ error, data }) {
        if (error) {
            console.log('Error in get product2Id wire service ' , error);
        } else if (data) {
            let record = {};
            console.log('product in wire record ', data);
            record.Family = data.fields.Family.value;
            record.RecordTypeId = data.fields.RecordTypeId.value;
            record.IsActive = data.fields.IsActive.value;
            record.Name = data.fields.Name.value;
            record.Master_Product__c = data.fields.Master_Product__c.value;
            record.Pivotal_Id__c = data.fields.Pivotal_Id__c.value;
            record.Run_Makability__c = data.fields.Run_Makability__c.value;
            record.Description = data.fields.Description.value;
            record.Unit_Short_Description__c = data.fields.Unit_Short_Description__c.value;
            if(data.fields.Vendor__r.value !== null && data.fields.Vendor__r.value !== undefined){
                record.VendorName = data.fields.Vendor__r.value.fields.Name.value;
            } else {
                record.VendorName = 'Vendor Name is null';  
            }
            this.productRecord = record;
            this.productRecordSet = true;
            this.checkForProductDescription(record);
            this.checkForMasterProduct(record);
            if(record.Run_Makability__c !== null && record.Run_Makability__c !== undefined){
                this.runMakabilityType = record.Run_Makability__c; 
            }
             if(this.inRop === true){
                 this.checkVendorType(record);
             }
            console.log('run makability for ... ' + this.runMakabilityType);
            console.log('current product is ' , record);
    }
}

    
    @wire(getProductConfig,{productId:'$productRecordId'})
    wiredOrderItems({ error, data }) {
         if (error) {
             this.error = error;
             console.log('error in getProductConfig callback',error);
         } else if (data) {
             console.log('prod config data', JSON.parse(JSON.stringify(data)));
             this.astragalColorList = this.convertToArray(data.Astragal__c);
             this.brickmouldLocationList = this.convertToArray(data.Brickmould_Location__c);
             this.casingLocationList = this.convertToArray(data.Casing_Location__c);
             this.casingProfileList = this.convertToArray(data.Casing_Profile__c);
             this.casingSpeciesList = this.convertToArray(data.Casing_Species__c);
             this.checkrailStyleList = this.convertToArray(data.Checkrail_Style__c);
             this.dripCapList = this.convertToArray(data.Drip_Cap_Pieces__c);
             this.ejColorList = this.convertToArray(data.EJ_Color__c);
             this.ejSpeciesList = this.convertToArray(data.EJ_Species__c);
             this.ejThicknessList = this.convertToArray(data.EJ_Thickness__c);
             this.ejWallDepthList = this.convertToArray(data.EJ_Wall_Depth__c);
             this.exteriorColorList = this.createExteriorColorMap(data.Color_Configurations__r);
             this.exteriorGrilleColorList = this.convertToArray(data.Exterior_Grille_Color__c);
             this.exteriorTrimColorList = this.convertToArray(data.Exterior_Trim_Color__c);
             this.exteriorTrimList = this.convertToArray(data.Exterior_Trim__c);
             this.originalExteriorTrimList = this.convertToArray(data.Exterior_Trim__c);
             this.fingerLiftsList = this.convertToArray(data.Finger_Lifts__c);
             this.frameTypeList = this.convertToArray(data.Frame_Type__c);
             this.performanceCategoryList = this.convertToArray(data.Performance_Category__c);
             this.fractionList = this.convertToArray(data.Measurement_Fractions__c);
            // this.glassPatternList = this.convertToArray(data.Glass_Pattern__c);
            this.glazingPatternConfigurationsClone = data.Glazing_Type_Pattern_Configurations__r;
             this.glazingList = this.createGlazingTypeMap(data.Glazing_Type_Pattern_Configurations__r);
             console.log('The value of glazing list: '+JSON.stringify(data.Glazing_Type_Pattern_Configurations__r));
             this.grillePatternList = this.convertToArray(data.Grille_Patterns__c);
             
             this.grilleStyleList = this.convertToArray(data.Grille_Style__c);
             this.grilleStyleListClone = this.convertToArray(data.Grille_Style__c);
             console.log('The value of glazing list: '+JSON.stringify(this.grilleStyleList));
             this.hardwareColorList = this.convertToArray(data.Hardware_Color__c);
             this.hardwareFinishList = this.convertToArray(data.Hardware_Finish__c);
             this.hardwareStyleList = this.convertToArray(data.Hardware_Style__c);
             this.hardwareOptionList = this.convertToArray(data.Hardware_Option__c);
             this.hubsList = this.convertToArray(data.Hubs__c);
             this.installTrackLocationList = this.convertToArray(data.Install_Track_Location__c);
             this.interiorSashColorList = this.convertToArray(data.Interior_Sash_Color__c);
             this.interiorGrilleColorList = this.convertToArray(data.Interior_Grille_Color__c);
             this.interiorTrimList = this.convertToArray(data.Interior_Trim__c);
             this.jambLinerColorList = this.convertToArray(data.Jamb_Liner_Color__c);
             this.liftsPullsList = this.convertToArray(data.Lifts_Pulls__c);
             //RTTFRONT-722
             this.liftsPullsListClone=this.convertToArray(data.Lifts_Pulls__c);
             console.log('In wired method: '+JSON.stringify(this.liftsPullsListClone));
             this.litesHighList = this.convertToArray(data.Lites_High__c);
             this.litesWideList = this.convertToArray(data.Lites_Wide__c);
             this.locksSashList = this.convertToArray(data.Locks_Sash__c);
             this.mullSequenceList = this.convertToArray(data.Mull_Sequence_Mullion__c);
             this.sashOperationList = this.convertToArray(data.Sash_Operation__c);
             console.log('The this.sashOperationList value: '+JSON.stringify(this.sashOperationList));
             this.sashRatioList = this.convertToArray(data.Sash_Ratio__c);
             this.screenColorList = this.convertToArray(data.Screen_Color__c);
             this.screenColorDefault = data.Screen_Color_Default__c;
             this.screenSizeList = this.convertToArray(data.Screen_Size__c);
             console.log('The this.screenSizeList value: '+JSON.stringify(this.screenSizeList));
             console.log('The this.screenTypeList value: '+JSON.stringify(this.screenTypeList));
             this.screenTypeListClone=this.convertToArray(data.Screen_Type__c);
             if(!this.screenTypeList || this.screenTypeList.length<1) {
                this.screenTypeList = this.convertToArray(data.Screen_Type__c);
                //let interiorColor = this.template.querySelector('lightning-combobox.Interior_Color__c'); 
                //console.log('Value of interior color: '+interiorColor.value);               
             }
                
             console.log('ScreenTypeListClone: '+JSON.stringify(this.screenTypeListClone));
             this.sillAngleList = this.convertToArray(data.Sill_Angle__c);
             this.specialShapeList = this.convertToArray(data.Specialty_Shape__c);
             this.spokesList = this.convertToArray(data.Spokes__c);
             this.stormWindowColorList = this.convertToArray(data.Storm_Window_Color__c);
             //Clone copy of data.Size_Detail_Configurations__r
             this.sizedetailConfigClone = data.Size_Detail_Configurations__r;
             this.positiveForceList = this.buildPositiveForceList(data.Size_Detail_Configurations__r);
             this.negativeForceList = this.buildNegativeForceList(data.Size_Detail_Configurations__r);
			this.grillePatternConfigs = data.Grille_Pattern_Configurations__r;
             if(data.Frame_Notch_Configurations__r !== undefined && data.Frame_Notch_Configurations__r !== null){
                this.frameNotchConfigList = data.Frame_Notch_Configurations__r;
            }
         } else {
            console.log('getProductConfig something bad happened...');
        }
     }

     // PRODUCT ID EVENT
     handleProductIdChange(event){
         console.log('product 2 info from product selector comp ', event.detail.Product2Id);
         console.log('product 2 info from product selector comp ', event.detail.ProductName);
         console.log('product 2 info from product selector comp ', event.detail.ProductCodeValue);
         if(event.detail.Product2Id === null || event.detail.Product2Id === undefined){
            this.clearForm('product2IdUpdated');
        }
         this.productRecordId = event.detail.Product2Id;
         this.productName = event.detail.ProductName;
         this.productCode=event.detail.ProductCodeValue;
     }

     // show/hide fields based on product field control records found on server.
    
        createProductFieldControlsObject(pfcArray){
            this.productFieldControl = {};
            for(let i = 0; i<pfcArray.length; i++){
                // console.log(i, 'index - pfc array',  pfcArray[i].Field_Control_ID__r.Target_Api_Name__c);
                this.productFieldControl[pfcArray[i].Field_Control_ID__r.Target_Api_Name__c] = true;
            }
            console.log('Product Field Controls: '+JSON.stringify(this.productFieldControl));
            this.performSetRequiredFields = true;
        }

        // TODO - now that every field has a class try getting this down to one loop 
        setRequiredFields(pfcArray){
            let allFields = this.template.querySelectorAll('lightning-input, lightning-combobox' );
            console.log('all fields before adding required',allFields);
            for (let pfc of pfcArray) {
                let apiName = pfc.Field_Control_ID__r.Target_Api_Name__c;
                for(let field of allFields ){
                    // TODO - should be able to remove field.fieldname reference - that only applies to lightning input field lookups.
                    if(field.name === apiName || field.fieldName === apiName){
                        field.required = pfc.Required__c;
                        if(pfc.Required__c==true || pfc.Required__c=='true')
                            this.requiredFields.push(apiName);
                        // console.log('pfc ApiNAme', apiName);
                        // console.log('pfc array by index', pfc);
                        // console.log('field to add required value to ', field);
                    }
                }
            }
            this.performSetRequiredFields = false;
            this.setRequiredFieldsCompleted = true;
        }
        @track exteriorColorMapCreated = false;
        // builds a map of exterior colors to interior color values. used to populate the interior color field.
        createExteriorColorMap(colorArray){
            let placeholder = {};
            if(colorArray){
                for(let i=0; i<colorArray.length; i++){
                    placeholder[colorArray[i].Exterior_Color__c] = colorArray[i].Interior_Color__c;
                }
            }
            this.exteriorColorMap = placeholder;
            console.log("create exterior color map",this.exteriorColorMap);
            // in edit mode - cannot set interior color until exterior color map has been created.
            if(this.editMode){
                this.exteriorColorMapCreated = true;
            }
            return this.buildPicklist(Object.keys(this.exteriorColorMap));
        }

        @track glazingtypeMapCreated=false;
        createGlazingTypeMap(glazingArray){
            console.log("create glazingArray"+JSON.stringify(glazingArray));
            let placeholder = {};
            let glazingConfigurationsForCoastal = this.customlabels.glazingConfigurationsForCoastal.split(",");
            if(glazingArray){
                for(let i=0; i<glazingArray.length; i++){
                    //RTTFRONT-717 : Restrict New Glazing Types for Coastal
                    if(this.template.querySelector('lightning-combobox.Performance_Category__c') && this.template.querySelector('lightning-combobox.Performance_Category__c').value==this.customlabels.costalPerfrmCategory) {
                        if(glazingConfigurationsForCoastal.indexOf(glazingArray[i].Glazing_Type__c)!=-1) {
                            placeholder[glazingArray[i].Glazing_Type__c] = glazingArray[i].Glazing__c;
                        }

                    }else {
                        if(!(glazingConfigurationsForCoastal.indexOf(glazingArray[i].Glazing_Type__c)!=-1)) {
                            placeholder[glazingArray[i].Glazing_Type__c] = glazingArray[i].Glazing__c;
                        }
                    }
                   
                }
            }
            this.glazingtypeMap = placeholder;
            console.log("create glazingTypeMap"+JSON.stringify(this.glazingtypeMap));
            // in edit mode - cannot set glazing pattern until glazing type map has been created.
            if(this.editMode){
                this.glazingtypeMapCreated = true;
            }
            return this.buildPicklist(Object.keys(this.glazingtypeMap));
        }

        //TODO - need to update into a map to remove duplicates.
        buildPositiveForceList(sizeDetailConfigList){
            console.log('hit build positive force list', sizeDetailConfigList);
            let placeholder =[];
            let arrayToSort = [];
            let positiveForceMap = {};
            placeholder.push({label:'--Select--', value:''});
            if(sizeDetailConfigList) {
                for(let sdc of sizeDetailConfigList){
                    if(sdc.Positive_Force__c !== null && sdc.Positive_Force__c !== undefined ){
                        //To show positive force values which are applicable only for Coastal Performance Category.
                        if(this.template.querySelector('lightning-combobox.Performance_Category__c') && this.template.querySelector('lightning-combobox.Performance_Category__c').value==this.customlabels.costalPerfrmCategory) {
                            if(sdc.Performance_Category__c==this.customlabels.costalPerfrmCategory) {
                                positiveForceMap[sdc.Positive_Force__c] = sdc.Positive_Force__c.toString(); 
                            }
                        }else {
                            positiveForceMap[sdc.Positive_Force__c] = sdc.Positive_Force__c.toString();
                        }
                        
                    }
                }
            }
            Object.keys(positiveForceMap).forEach(function(posForce){
                arrayToSort.push({label: posForce, value:posForce});
            });

            arrayToSort.sort(function(a, b){return a.value - b.value});
            placeholder = [...placeholder,...arrayToSort];
            console.log('postive force list',placeholder);
            return placeholder;
        }
        buildNegativeForceList(sizeDetailConfigList){
            console.log('hit build negative force list', sizeDetailConfigList);
            let placeholder =[];
            let arrayToSort = [];
            let negativeForceMap = {};
            placeholder.push({label:'--Select--', value:''});
            if(sizeDetailConfigList) {
                for(let sdc of sizeDetailConfigList){
                    if(sdc.Negative_Force__c !== null && sdc.Negative_Force__c !== undefined ){
                        //To show negative force values which are applicable only for Coastal Performance Category.
                        if(this.template.querySelector('lightning-combobox.Performance_Category__c') && this.template.querySelector('lightning-combobox.Performance_Category__c').value==this.customlabels.costalPerfrmCategory) { 
                            if(sdc.Performance_Category__c==this.customlabels.costalPerfrmCategory) {
                                negativeForceMap[sdc.Negative_Force__c] = sdc.Negative_Force__c.toString();
                            }
                        }else {
                            negativeForceMap[sdc.Negative_Force__c] = sdc.Negative_Force__c.toString();
                        }
                        
                    }
                }
            }
            Object.keys(negativeForceMap).forEach(function(negForce){
                arrayToSort.push({label: negForce, value:negForce});
            });
            arrayToSort.sort(function(a, b){return a.value - b.value});
            placeholder = [...placeholder,...arrayToSort];
            console.log('postive force list',placeholder);
            return placeholder;
        }
        buildPicklist(array){
            let placeholder =[{label:'--Select--', value:''}];
            // placeholder.push({label:'--Select--', value:''});
            if(array){
                for(let i=0;i<array.length;i++){
                    placeholder.push({label:array[i],value:array[i]});
                 }
            }
             return placeholder;
        }

        convertToArray(string){
            let placeholder =[{label:'--Select--', value:''}];
            // placeholder.push({label:'--Select--', value:''});
            if(string){
                let splitArray = string.split(";");
                for(let i=0;i<splitArray.length;i++){
                    placeholder.push({label:splitArray[i],value:splitArray[i]});
                 }
            }
             return placeholder;
        }



        toggleSection(sectionName){
            const accordion = this.template.querySelector('.rba-accordian');
            console.log(accordion.activeSectionName);
            let placeholder = [];
            placeholder = placeholder.concat(accordion.activeSectionName);
            placeholder.push(sectionName);
            accordion.activeSectionName = placeholder;
        }

        handleExteriorColor(event){
            console.log('color changed', event.detail.value);
            let extColor = event.detail.value 
            this.interiorColorList = this.convertToArray(this.exteriorColorMap[extColor]); 
                   
        }

        handleGlazingType(event){
            let glazingType = event.detail.value 
            this.glazingPatternList = this.convertToArray(this.glazingtypeMap[glazingType]); 
        }

        //TODO - need to be able to go back to original list if user selects something other than a fnc option...
        // initial thoughts. track the change through this method, set a boolean store original values.
        // if a boolean is set, reset list to original values and remove boolean.
        @track resetToOriginalList = true;
        setExteriorTrimList(){
            if(this.resetToOriginalList) {
                console.log('hit reset to original list');
                this.exteriorTrimList = this.originalExteriorTrimList;
            //    this.resetToOriginalList = false;
            }
            if (this.frameNotchConfigList.length > 0 ) { //&& this.resetToOriginalList === false
                let currentFrameType;
                let currentPocketNotch = false;
                if (this.editMode && this.setExteriorTrimListValues) {
                    currentFrameType = this.orderItem.Frame_Type__c;
                    currentPocketNotch = this.orderItem.Pocket_Notch__c;
                } else {
                    if (this.productFieldControl.Frame_Type__c) {
                        let frameTypeField = this.template.querySelector('lightning-combobox.Frame_Type__c');
                        currentFrameType = frameTypeField.value;
                    }
                    if (this.productFieldControl.Pocket_Notch__c) {
                        let pocketNotchField = this.template.querySelector('lightning-input.Pocket_Notch__c');
                        currentPocketNotch = pocketNotchField.checked;
                    }
                }
                console.log('set exterior trim list frame type and pocket notch', currentFrameType, ' ', currentPocketNotch);
                for (let fnc of this.frameNotchConfigList) {
                    console.log('fnc in loop ',fnc);
                    if (fnc.Pocket_Notch__c === currentPocketNotch && fnc.Frame_Type__c === currentFrameType) {
                        this.exteriorTrimList = this.convertToArray(fnc.Exterior_Trim__c);
                        console.log('set exterior trim list in loop. the matching list', this.exteriorTrimList);
                    }
                }
                this.setExteriorTrimListValues = false;
            //    this.resetToOriginalList = true;
            } 

        }
        

        setEjFrame(){
            if(this.productFieldControl.EJ_Frame__c){
                let ejFrameField = this.template.querySelector('lightning-input.EJ_Frame__c');
                ejFrameField.checked = true;
            }
        }

        handleSectionToggle(event) {
            const openSections = event.detail.openSections;
            console.log('handleSectionToggle open section event ',openSections);
        }

		setGrilleLitesDefaults(value){

            if(this.grillePatternConfigs !== undefined){
                for(let gpc of this.grillePatternConfigs){
                    if(gpc.Grille_Pattern__c === value){
                        console.log(gpc.Grille_Pattern__c);
                        console.log('hit match in gpc', JSON.parse(JSON.stringify(gpc)));
                        let S1LitesWide;
                        let S1LitesHigh;
                        let S2LitesWide;
                        let S2LitesHigh;
                        if(this.productFieldControl.Lites_Wide_S1__c){
                            S1LitesWide = this.template.querySelector('lightning-combobox.Lites_Wide_S1__c');
                            if(gpc.hasOwnProperty('Default_Lites_Wide__c')){
                                console.log('default lites wide s1', JSON.parse(JSON.stringify(gpc.Default_Lites_Wide__c)));
                                S1LitesWide.value = gpc.Default_Lites_Wide__c.toString();
                            } else{
                                S1LitesWide.value = undefined;
                            }
                        }
                        if(this.productFieldControl.Lites_High_S1__c){
                            S1LitesHigh = this.template.querySelector('lightning-combobox.Lites_High_S1__c');
                            if(gpc.hasOwnProperty('Default_Lites_High__c')){
                                console.log('default lites high s1', JSON.parse(JSON.stringify(gpc.Default_Lites_High__c)));
                                S1LitesHigh.value = gpc.Default_Lites_High__c.toString();
                            }  else{
                                S1LitesHigh.value = undefined;
                            }
                        }
                        if(this.productFieldControl.Lites_Wide_S2__c){
                            S2LitesWide = this.template.querySelector('lightning-combobox.Lites_Wide_S2__c');
                            if(gpc.hasOwnProperty('S2_Default_Lites_Wide__c')){
                                console.log('default lites wide s2', JSON.parse(JSON.stringify(gpc.S2_Default_Lites_Wide__c)));
                                S2LitesWide.value = gpc.S2_Default_Lites_Wide__c.toString();
                            }  else {
                                S2LitesWide.value = undefined
                            }
                        }
                        if(this.productFieldControl.Lites_High_S2__c){
                            S2LitesHigh = this.template.querySelector('lightning-combobox.Lites_High_S2__c');
                            if(gpc.hasOwnProperty('S2_Default_Lites_High__c')){
                                console.log('default lites high s2', JSON.parse(JSON.stringify(gpc.S2_Default_Lites_High__c)));
                                S2LitesHigh.value = gpc.S2_Default_Lites_High__c.toString();
                            } else {
                                S2LitesHigh.value = undefined;
                            }
                        }
                    }
                }
            }
        }


        // if in edit mode. populate fields from orderitem
        //TODO - update for exterior trim.
        populateFields(){
            if(this.editMode){
                let allFields = this.template.querySelectorAll( 'lightning-input, lightning-combobox, lightning-input-field ');
                for(let field of allFields){
                    console.log('field in all fields populate -  ',field.name ,field.fieldName, field);
                    if(this.orderItem[field.name]){
                        field.value = this.orderItem[field.name];
                    }
                    if(this.orderItem[field.fieldName]){
                        field.value = this.orderItem[field.fieldName];
                    }
                    if(field.type === 'checkbox'){
                        field.checked = this.orderItem[field.name];
                    }
                    if(field.name  === 'Frame_Type__c' && this.orderItem[field.name] === 'Flat Sill' || this.orderItem[field.name] === 'Flat Sill Insert'){
                        this.sillAngleList = sillAngleFsOnlyList;
                        console.log('populate fields hit set sillangleList FS only');
                    }
                    if(field.name  === 'Frame_Type__c' && this.orderItem[field.name] === 'Full Frame'){
                        console.log('set ejframe on edit.. full frame only');
                        this.setEjFrame();
                    }
                    if(field.name  === 'Frame_Type__c' && this.orderItem[field.name] === 'Slope Sill' || this.orderItem[field.name] === 'Slope Sill Insert'){
                        this.sillAngleList = sillAngleZeroThruFourteenList;
                        console.log('populate fields hit set sillangleList 0-14');
                    }                    
                    if(field.name === 'Specialty_Shape__c' && this.orderItem[field.name] !== null && this.orderItem[field.name] !== undefined){
                        this.toggleSection('Specialty-Window-Options');
                    }
                    if(field.name === 'Lites_Wide_S1__c' && this.orderItem[field.name] !== null && this.orderItem[field.name] !== undefined){
                        field.value = this.orderItem[field.name].toString();
                    }
                    if(field.name === 'Lites_High_S1__c' && this.orderItem[field.name] !== null && this.orderItem[field.name] !== undefined){
                        field.value = this.orderItem[field.name].toString();
                    }
                    if(field.name === 'Lites_Wide_S2__c' && this.orderItem[field.name] !== null && this.orderItem[field.name] !== undefined){
                        field.value = this.orderItem[field.name].toString();
                    }
                    if(field.name === 'Lites_High_S2__c' && this.orderItem[field.name] !== null && this.orderItem[field.name] !== undefined){
                        field.value = this.orderItem[field.name].toString();
                    }
                    if(field.name === 'Spokes__c' && this.orderItem[field.name] !== null && this.orderItem[field.name] !== undefined){
                        field.value = this.orderItem[field.name].toString();
                    }
                    if(field.name === 'Hubs__c' && this.orderItem[field.name] !== null && this.orderItem[field.name] !== undefined){
                        field.value = this.orderItem[field.name].toString();
                    }
                    if(field.name === 'Drip_Cap_Pieces__c' && this.orderItem[field.name] !== null && this.orderItem[field.name] !== undefined){
                        field.value = this.orderItem[field.name].toString();
                    }
                    //TODO - update to only apply value if PG_50__c === true - filer uploader issue
                    if(field.name === 'Positive_Force__c' && this.orderItem[field.name] !== null && this.orderItem[field.name] !== undefined){
                        field.value = this.orderItem[field.name].toString();
                        console.log('Positivie Force: '+field.value);
                    }
                    //TODO - update to only apply value if PG_50__c === true - filer uploader issue
                    if(field.name === 'Negative_Force__c' && this.orderItem[field.name] !== null && this.orderItem[field.name] !== undefined){
                        field.value = this.orderItem[field.name].toString();
                        console.log('Negative Force: '+field.value);
                    }
                    if(field.name === 'Grille_Pattern__c' && field.value !== 'No Grille' && field.value !== null && field.value !== undefined ){
                        this.toggleSection('Grille-Options');
                    }                    
                    // TODO - verify this is working correctly
                    if(field.name === 'Special_Options__c' && field.checked  === true){
                        this.toggleSection('Special-Options');
                    }
                }
                this.setExteriorTrimList();
            }
            this.fieldsPopulated = true;
        }
        populateInteriorColor(){
        //    let interiorColorField = this.template.querySelector('lightning-combobox.Interior_Color__c');
        //    let exteriorColorField = this.template.querySelector('lightning-combobox.Exterior_Color__c');
            let color = this.orderItem.Exterior_Color__c; //exteriorColorField.value;
            let list = this.convertToArray(this.exteriorColorMap[color]);
            console.log('list before mapping to interior color', color,list);
            this.interiorColorList = list;
        //    interiorColorField.value = this.orderItem.Interior_Color__c; 
            console.log('hit populate interior color - exterior color map', this.exteriorColorMap);
            console.log('hit populate interior color', this.interiorColorList);
            this.setInteriorColorValue = false;
        }

        populateGlazingPattern(){

            let listS1 ;
            let typeS1 = this.orderItem.Glazing_S1__c; 
         //   console.log('s1 type in populate glazing pattern',typeS1);
            if(typeS1 !== '' || typeS1 !== null || typeS1 !== undefined){

              listS1 = this.convertToArray(this.glazingtypeMap[typeS1]);
              this.glazingPatternListS1 = listS1;

      
            }
            let listS2 ;
            let typeS2 = this.orderItem.Glazing_S2__c; 
        //    console.log('s2 type in populate glazing pattern',typeS2);
            if(typeS2 !== '' || typeS2 !== null || typeS2 !== undefined){

              listS2 = this.convertToArray(this.glazingtypeMap[typeS2]);
              this.glazingPatternListS2 = listS2;
      
            } 
            let listS3 ;
            let typeS3 = this.orderItem.Glazing_S3__c; 
        //    console.log('s3 type in populate glazing pattern',typeS3);
            if(typeS3 !== '' || typeS3 !== null || typeS3 !== undefined){

              listS3 = this.convertToArray(this.glazingtypeMap[typeS3]);
              this.glazingPatternListS3 = listS3;
      
            } 
            this.setGlazingPatternValue = false;
        }

        populatePositiveAndNegativeForce(){
            let postiveForceField = this.template.querySelector('lightning-combobox.Positive_Force__c');
            let negativeForceField = this.template.querySelector('lightning-combobox.Negative_Force__c');
            postiveForceField.value = this.orderItem.Positive_Force__c.toString(); 
            negativeForceField.value = this.orderItem.Negative_Force__c.toString(); 
            console.log('hit populate pos and neg force', postiveForceField.value ,  negativeForceField.value );
        //    this.setPositveAndNegativeForce = false;    
        }
        applyPfcds(){
            let allFields = this.template.querySelectorAll( 'lightning-input, lightning-combobox, lightning-input-field ');
            for(let field of allFields){
                let name = field.name ? field.name : field.fieldName;
                if(this.orderItem[name]){
                    let value = this.orderItem[name];
                    console.log('edit - pfcd - field name, field value', name, value);
                    this.handleProductFieldDependency(name, value);
                    if(name=='Performance_Category__c' && value==this.customlabels.costalPerfrmCategory) {
                        this.handlePerformanceCategoryFieldDependency(value);
                    }
                    if(name=='PG_50__c') {
                        console.log('The P50Value: '+value);
                        this.controlPerformanceCategory(value);
                    }  
                }
            }
        }


        //********************************************************************************************
        //****************************On Change Handler***********************************************
        //*********************Logic to handle field updates******************************************
        //******************************************************************************************** 
        handleChange(event){
            // <lightning-input>  uses name and value <lightning-input-field> uses  fieldName and value[0]. 
            let name = event.target.name ? event.target.name : event.target.fieldName;
            let value = Array.isArray(event.detail.value) ? event.detail.value[0] : event.detail.value;
            let sillAngle;
            // deprecated - product2Id now comes through event ***********************************
            console.log('hit handle change for ', name);

            if(name==='PG_50__c') {
                this.controlPerformanceCategory(event.target.checked);
                this.glazingList = this.createGlazingTypeMap(this.glazingPatternConfigurationsClone);
                this.liftsPullsList = handleLiftPulls(this.template.querySelector('lightning-combobox.Performance_Category__c'),this.productCode,[...this.liftsPullsListClone],this.template.querySelector('lightning-combobox.Lifts_Pulls__c'));
                handleGlazingValues(this.template.querySelector('lightning-combobox.Glazing_S1__c'),this.template.querySelector('lightning-combobox.Glazing_S3__c'),this.template.querySelector('lightning-combobox.Glazing_S2__c'),this.glazingList);

                //RTTFRONT-667
                handleWideBar(this.template.querySelector('lightning-combobox.Performance_Category__c'),this.template.querySelector('lightning-combobox.Grille_Style__c'),this.template.querySelector('lightning-input.Wide_Bar__c'));
            }
            if(name === 'Product2Id'){
                this.productRecordId = event.detail.value[0];
                this.clearForm('product2IdUpdated');
            }

            if(name === 'Performance_Category__c'){
                this.handlePerformanceCategoryFieldDependency(value);
                console.log('Performance Category Value: '+value);
                //RTTFRONT-717 : Restrict New Glazing Types for Coastal
                this.glazingList = this.createGlazingTypeMap(this.glazingPatternConfigurationsClone);
                console.log('glazingList value in handle change: ',this.glazingList);
                //RTTFRONT-717 : Restrict New Glazing Types for Coastal
                handleGlazingValues(this.template.querySelector('lightning-combobox.Glazing_S1__c'),this.template.querySelector('lightning-combobox.Glazing_S3__c'),this.template.querySelector('lightning-combobox.Glazing_S2__c'),this.glazingList);
                //RTTFRONT-722
                
                this.liftsPullsList = handleLiftPulls(this.template.querySelector('lightning-combobox.Performance_Category__c')
                ,this.productCode,[...this.liftsPullsListClone],this.template.querySelector('lightning-combobox.Lifts_Pulls__c'));

                //RTTFRONT-667
                  handleWideBar(this.template.querySelector('lightning-combobox.Performance_Category__c'),this.template.querySelector('lightning-combobox.Grille_Style__c'),this.template.querySelector('lightning-input.Wide_Bar__c'));
            }
            
            if(name === 'OrderId'){
                this.orderId = event.detail.value[0];
            }
            if(name === 'Specialty_Shape__c'){
                this.toggleSection('Specialty-Window-Options');
            }
            if(name === 'Exterior_Color__c'){
                this.interiorColorList = this.convertToArray(this.exteriorColorMap[value]);   
                
                console.log('The value of exteriorColorMap: '+JSON.stringify(this.exteriorColorMap));     

            }

            if(name==='Glazing_S1__c'){
                console.log('The glazing type map values: ',this.glazingtypeMap);
                this.glazingPatternListS1 = this.convertToArray(this.glazingtypeMap[value] );
                let glazingpatternS1 = this.template.querySelector('lightning-combobox.Glass_Pattern_S1__c');
                if(glazingpatternS1 && glazingpatternS1.value) {
                    let valueFound = false;
                    this.glazingPatternListS1.forEach(element => {
                        if(glazingpatternS1.value == element.value) {
                            valueFound = true;
                        }
                        console.log('Element value: '+element.value);
                    });

                    if(!valueFound) {
                        glazingpatternS1.value =''; 
                    }  
                }
                
               
            }
            if(name==='Glass_Pattern_S1__c'){
                let grilleStyle = this.template.querySelector('lightning-combobox.Grille_Style__c');
               
            }
            

            if(name==='Glazing_S2__c'){
                this.glazingPatternListS2 = this.convertToArray(this.glazingtypeMap[value] );
                let glazingpatternS2 = this.template.querySelector('lightning-combobox.Glass_Pattern_S2__c');
                if(glazingpatternS2 && glazingpatternS2.value) {
                    let valueFound = false;
                    this.glazingPatternListS2.forEach(element => {
                        if(glazingpatternS2.value == element.value) {
                            valueFound = true;
                        }
                        console.log('Element value: '+element.value);
                    });

                    if(!valueFound) {
                        glazingpatternS2.value =''; 
                    }  
                }
               

            }
            if(name==='Glass_Pattern_S2__c'){
                let grilleStyle = this.template.querySelector('lightning-combobox.Grille_Style__c');
                

            }
            

            if(name==='Glazing_S3__c'){
                console.log('hit glazing s3 in handle change');
                this.glazingPatternListS3 = this.convertToArray(this.glazingtypeMap[value] );
                console.log('GlazingPatternLists: '+JSON.stringify(this.glazingPatternListS3));
            

            }
            if(name === 'Grille_Pattern__c' && value !== 'No Grille' && value !== null ){
                this.toggleSection('Grille-Options');
				this.setGrilleLitesDefaults(value);
                console.log('finsihed default lites logic...');
            }
            if(name === 'Frame_Type__c' && value === 'Flat Sill' || value === 'Flat Sill Insert'){
                this.sillAngle = this.template.querySelector('lightning-combobox.Sill_Angle__c');
                this.sillAngle.value=undefined;
                this.sillAngleList = sillAngleFsOnlyList;
            }
            if(name === 'Frame_Type__c' && value === 'Slope Sill' || value === 'Slope Sill Insert'){
                this.sillAngle = this.template.querySelector('lightning-combobox.Sill_Angle__c');
                this.sillAngle.value=undefined;
                this.sillAngleList = sillAngleZeroThruFourteenList;
            }
            if(name === 'Frame_Type__c' && value === 'Full Frame'){
                this.setEjFrame();
            }
            if(name === 'Frame_Type__c'){
                console.log('hit set exterior trim for frame.');
                this.setExteriorTrimList();
            }
            if(name === 'Pocket_Notch__c'){
                console.log('hit set exterior trim for notch.');
                this.setExteriorTrimList();
            }
            if(event.target.type === 'checkbox'){
                 value = this.template.querySelector('lightning-input.'+name).checked;
                 console.log('name of checked field',name);
                 console.log('value of checked field',value);
            }
            // checking special options field needs to happen below the setting the checkbox boolean method.
            if(name === 'Special_Options__c' && this.template.querySelector('lightning-input.Special_Options__c').checked === true){
                this.toggleSection('Special-Options');
                this.setSpecialOptions();
            }
            if(name==='Interior_Color__c') {
                console.log('The interior name: '+this.productCode);
                console.log('The interior value: '+value);
                //let screenTypeVar = this.template.querySelector('lightning-combobox.Screen_Type__c');
                // screenTypeVar.value="";
                this.setScreenType(value);
            
                
            }if(name==='Screen_Type__c') {
                this.checkGLMakeability(this.productCode,this.template.querySelector('lightning-combobox.Sash_Operation__c').value,value);
            }
            if(name==='Sash_Operation__c') {
                this.checkGLMakeability(this.productCode,value,this.template.querySelector('lightning-combobox.Screen_Type__c').value);
            }
            this.handleProductFieldDependency(name,value);
            
            if(name==='Grille_Style__c') {

                //RTTFRONT-667
                handleWideBar(this.template.querySelector('lightning-combobox.Performance_Category__c'),this.template.querySelector('lightning-combobox.Grille_Style__c'),this.template.querySelector('lightning-input.Wide_Bar__c'));
            }  
        }



        //*****************************End On Change Handler********************************************** */

        // ROP requested feature to focus on the current field...
        //deprecated 6-9-20 mtr - added dropdown-alignment="auto"  to all html fields instead.
		handleFocus(event){
            console.log('In handle focus event ',JSON.parse(JSON.stringify(event.target)));
        //     let name = event.target.name ? event.target.name : event.target.fieldName;
        //     // eslint-disable-next-line @lwc/lwc/no-document-query
        //     let field = this.template.querySelector('lightning-combobox.'+name);
        //    // console.log('field in focus event',field);
        //     field.scrollIntoView({
        //         behavior: 'smooth',
        //         block: 'center',
        //         inline: 'center'
        //     });
        }


        //*******************************************************************************
        //*********************************Submit Button Handlers************************
        // Logic and methods to support the submission of the orderItem or the closing of 
        // the page
        //*******************************************************************************

        handleCancel(event){
            event.preventDefault();
            this.redirectToOrderPage();
        }
        handleSaveAndClose(event){
            event.preventDefault();
            let config = {};
            config.redirectToOrder = true;
            config.showSuccessToast = false;
            config.clearForm = false;
            this.handleSubmit(config);
        }
        handleSaveAndNew(event){
            event.preventDefault();
            let config = {};
            config.clearForm = true;
            config.redirectToCreatePage = true;
            config.redirectToOrder = false;
            config.showSuccessToast = true;
            config.clearEditMode = true;
            this.handleSubmit(config);

        }
        handleSaveAndView(event){
            event.preventDefault();
            let config = {};
            config.clearForm = false;
            config.redirectToCreatePage = false;
            config.redirectToOrder = false;
            config.showSuccessToast = true;
            config.clearEditMode = false;
            config.redirectToView = true;
            this.handleSubmit(config);

        }
        handleSaveAndClone(event){
            event.preventDefault();
            let config = {};
            config.clearForm = false;
            config.redirectToOrder = false;
            config.showSuccessToast = true;
            config.clearEditMode = true;
            this.handleSubmit(config);
        }
        //TODO need to break this up into save with makability and save without makability.
        handleSubmit(config){
            console.log('handle submit- config', config);
            // validation toasts fire from this.validateForm
            let validated = this.validateForm();
            let runWindowMakability = this.checkRunWindowMakability();
            if(validated){
                let formValues = this.getFormValues();
                this.passedMakability = true;
                this.showMakabilitySpinner = !this.showMakabilitySpinner;
                if(this.editMode){
                    formValues.Id = this.orderItem.Id;
                }
                if(runWindowMakability){
                    console.log('hit run window makability');
                    performMakabilityCheck({form:formValues})
                    .then(result => {
                        let parsedResponse = JSON.parse(result.jsonResponse);
                        console.log('response in save order item ',parsedResponse);
                        this.showMakabilitySpinner = !this.showMakabilitySpinner;
                        this.buildMakabilityMessage(parsedResponse);
                        if(this.passedMakability){
                            this.showMakabilitySpinner = !this.showMakabilitySpinner;
                            formValues.Verify_Item_Configuration__c = true;
                            saveOrderItem({form:formValues})
                            .then(res => {
                                let parsedRes = JSON.parse(res.jsonResponse);
                                console.log('response in save order item post makability',parsedRes);
                                this.showMakabilitySpinner = !this.showMakabilitySpinner;
                            //    this.buildMakabilityMessage(parsedResponse);
                                if(config.redirectToOrder){
                                    console.log('hit redirect to order call');
                                    this.redirectToOrderPage();
                                }
                                if(config.redirectToCreatePage){
                                    console.log('hit save and new redirect call');
                                    this.redirectForSaveAndNew();
                                }
                                if(config.clearForm){
                                    console.log('hit clearform on submit call');
                                    this.clearForm(null);
                                }
                                if(config.redirectToView){
                                    if(this.orderItemId === '' || this.orderItemId === null || this.orderItemId === undefined){
                                        this.orderItemId = parsedRes.recordId ;
                                    }
                                    console.log('hit save and view on submit call');
                                    this.redirectForSaveAndView();
                                }
                                if(config.showSuccessToast){
                                    console.log('create a toast event here.');
                                    if(parsedRes.message === 'Insert Successful'){
                                        this.validationToast('Record Saved Successfully!','Success','dismissable','Success');                                    
                                    }
                                    if(parsedRes.message === 'Upsert Successful'){
                                        this.validationToast('Record Updated Successfully!','Success','dismissable','Success');                                    
                                    }
                                }
                                if(config.clearEditMode){
                                    console.log('hit clear edit mode on submit call');
                                    this.editMode = false;
                                } 
                        })
                        .catch(error => {
                            //TODO need to display errors - throw a toast?
                            this.error = error;
                            this.showMakabilitySpinner = !this.showMakabilitySpinner;
                            console.log('error in callback for save item + makability',this.error);
                        });
                        }
                    })
                    .catch(error => {
                        //TODO need to display errors - throw a toast?
                        this.error = error;
                        this.showMakabilitySpinner = !this.showMakabilitySpinner;
                        console.log('error in callback for makability before save',this.error);
                        this.validationToast('Error in makability before saving record : ' + error.message, 'Error', 'sticky','Error');
                    });
                } else {
                    console.log('form values in save only ', formValues);
                    saveOrderItem({form:formValues})
                    .then(result => {
                        let parsedResponse = JSON.parse(result.jsonResponse);
                        console.log('response in save order item ',parsedResponse);
                        this.showMakabilitySpinner = !this.showMakabilitySpinner;
                    //    this.buildMakabilityMessage(parsedResponse);
                        if(config.redirectToOrder){
                            console.log('hit redirect to order call');
                            this.redirectToOrderPage();
                        }
                        if(config.redirectToCreatePage){
                            console.log('hit save and new redirect call');
                            this.redirectForSaveAndNew();
                        }
                        if(config.clearForm){ 
                            console.log('hit clearform on submit call');
                            this.clearForm(null);
                        }
                        if(config.redirectToView){
                            if(this.orderItemId === '' || this.orderItemId === null || this.orderItemId === undefined){
                                this.orderItemId = parsedResponse.recordId ;
                            }
                            console.log('hit save and view on submit call');
                            this.redirectForSaveAndView();
                        }
                        if(config.showSuccessToast){
                            console.log('create a toast event here.');
                            if(parsedResponse.message === 'Insert Successful'){
                                this.validationToast('Record Saved Successfully!','Success','dismissable','Success');                                    
                            }
                            if(parsedResponse.message === 'Upsert Successful'){
                                this.validationToast('Record Updated Successfully!','Success','dismissable','Success');                                    
                            }                        
                        }
                        if(config.clearEditMode){
                            console.log('hit clear edit mode on submit call');
                            this.editMode = false;
                        } 
                })
                .catch(error => {
                    //TODO need to display errors - throw a toast?
                    this.error = error;
                    this.showMakabilitySpinner = !this.showMakabilitySpinner;
                    console.log('error in callback save only no makability',this.error);
                });
            }
            
            }
            //  else{
            //     // this.showMakabilitySpinner = !this.showMakabilitySpinner;
            //     //TODO need to update this toast
            //     this.validationToast('you messed up boo');
            //     console.log('show failed validation toast');
            // }
        }


/******************************************************************************************************************
*       Validate form, performs a number of custom validation rules. 
        1. standard form validation. checks all text fields and comboboxes for correct or missing data.
        2. grille validation. verifies that if a grille pattern is selected that atleast 1 sash of lites high and wide is filled out
        3. single leg validation. verifies that if a product is one of a specific set of specialty shapes 
        that one and only one set of the left or right leg Inches/fractions fields is filled out
        4. OrderId and Product2Id validates that both of those fields have values.
        5. NSPR number validation - is nspr box is checked make sure nspr number is filled out.
*/
    validateForm() {
        console.log('hit validate form');
        let hasNSPR = this.checkIfNSPR();
        console.log('hasNSPR in validate form', hasNSPR);
        let allValid;
        let grillePattern; 
        let specialtyShape;
        let quantity;
        let unitPrice;
        let nsprNumber;
        let verifyItemConfig;
        let validationPassed = true;
        let grilleValidationPassed = true;
        let singleLegValidationPassed = true;
        let quantityValidationPassed = true;
        let unitPriceValidationPassed = true;
        let nsprNumberValidationPassed = true;
        let verifyItemConfigValidationPassed = true;
        let product2Validation = this.checkProduct2Validation();
        let orderIdValidation = this.checkFieldValidation('OrderId');
		let editValidationPassed = this.showButton;
        if(hasNSPR === false){
            console.log('hit allValid - should run all field validations');
            allValid = [...this.template.querySelectorAll('lightning-input, lightning-combobox')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
        }
        if(this.productFieldControl.Grille_Pattern__c){
            grillePattern = this.template.querySelector('lightning-combobox.Grille_Pattern__c');
            if (grillePattern.value !== 'No Grille' && grillePattern.value !== 'Special Instruction' && grillePattern.value !== 'Designer' && grillePattern.value !== undefined )  {
                console.log('hit grille check', grillePattern.value);
                grilleValidationPassed = this.checkGrilleValidation();
            }
        }
        if(this.productFieldControl.Specialty_Shape__c){
            specialtyShape = this.template.querySelector('lightning-combobox.Specialty_Shape__c');
            if(singleLegShapes.includes(specialtyShape.value)){
                console.log('hit single leg validation');
                singleLegValidationPassed = this.checkForSingleLeg();
            }
        }
        if(this.productFieldControl.Quantity){
            quantity = this.template.querySelector('lightning-input.Quantity');
            quantity.reportValidity();
            quantityValidationPassed = quantity.checkValidity();
        }
        if(this.productFieldControl.UnitPrice){
            unitPrice = this.template.querySelector('lightning-input.UnitPrice');
            unitPrice.reportValidity();
            unitPriceValidationPassed = unitPrice.checkValidity();
        }
        if(this.productFieldControl.NSPR_Number__c){
            nsprNumber = this.template.querySelector('lightning-input.NSPR_Number__c');
            nsprNumber.reportValidity();
            nsprNumberValidationPassed = nsprNumber.checkValidity();
        }
        if(this.productFieldControl.Verify_Item_Configuration__c){
            verifyItemConfig = this.template.querySelector('lightning-input.Verify_Item_Configuration__c');
            verifyItemConfig.reportValidity();
            verifyItemConfigValidationPassed = verifyItemConfig.checkValidity();
        }
        if(hasNSPR){
            allValid = true;
            grilleValidationPassed = true;
            singleLegValidationPassed = true;
            console.log('hit has NSPR - IGNORE MOST VALIDATION');
        }
        if (allValid === false) {
            validationPassed = false;
            this.validationToast('Please update the invalid form entries and try again.','Validation Error', 'sticky','Error');
            // console.log('Please update the invalid form entries and try again.');
        }
        if (product2Validation === false) {
            // console.log('Missing product value. please check Product field');
            this.validationToast('Missing product value. Please check Product field.','Validation Error', 'sticky','Error');
            validationPassed = false;
        }
        if (orderIdValidation === false) {
            // console.log('Missing order number. please recheck Order field');
            this.validationToast('Missing order number. Please check Order Id field.','Validation Error', 'sticky','Error');
            validationPassed = false;
        }
        if (grilleValidationPassed === false) {
            validationPassed = false;
            this.validationToast('Grille Pattern requires either S1/S2 Lites OR Hub and Spoke Values','Validation Error', 'sticky','Error');
            // console.log('You need to enter values for atleast one Lite high lites Wide sash.');
            // send validation toast here            
        }
        if(singleLegValidationPassed === false){
            this.validationToast('This Specialty Shape requires one leg. Please verify that either left OR right leg measurements are filled out.','Validation Error', 'sticky','Error');
            validationPassed = false;
        }
        if(quantityValidationPassed === false){
            this.validationToast('Missing Quantity value. Please check Quantity field.','Validation Error', 'sticky','Error');
            validationPassed = false;
        }
        if(unitPriceValidationPassed === false){
            this.validationToast('Missing Unit Price value. Please check field. zero is a valid unit price.','Validation Error', 'sticky','Error');
            validationPassed = false;
        }
        if(nsprNumberValidationPassed === false){
            this.validationToast('Missing NSPR Number value. Please check field.','Validation Error', 'sticky','Error');
            validationPassed = false;
        }
        if(verifyItemConfigValidationPassed === false){
            this.validationToast('Missing Verify Item Configuration value. Please check field.','Validation Error', 'sticky','Error');
            validationPassed = false;
        }
		if(editValidationPassed === false){
            this.validationToast('Products cannot be edited to an Order after revenue has been recognized. To edit a product, please create a Change Order.','Validation Error', 'sticky','Error');
            validationPassed = false;
        }

        return validationPassed;
    }

        checkForSingleLeg(){
            let validated = false;
            let hasRightLegValues = false;
            let hasLeftLegValues = false;
            let hasRightInches = false;
            let hasRightFraction = false;
            let hasLeftInches = false;
            let hasLeftFraction = false;
            let rightLegInches;
            let rightLegFraction;
            let leftLegInches;
            let leftLegFraction;
            let totalLegMeasurements = 0;
            if(this.productFieldControl.Right_Leg_Inches__c){
                rightLegInches = this.template.querySelector('lightning-input.Right_Leg_Inches__c');
            }
            if(this.productFieldControl.Right_Leg_Fraction__c){
                rightLegFraction = this.template.querySelector('lightning-combobox.Right_Leg_Fraction__c');
            }
            if(this.productFieldControl.Left_Leg_Inches__c){
                leftLegInches = this.template.querySelector('lightning-input.Left_Leg_Inches__c');
            }
            if(this.productFieldControl.Left_Leg_Fraction__c){
                leftLegFraction = this.template.querySelector('lightning-combobox.Left_Leg_Fraction__c');
            }
            
            if(rightLegInches.value !== null && rightLegInches.value !== undefined  && rightLegInches.value !== ''){
                totalLegMeasurements++;
                hasRightInches = true;
            }
            if(rightLegFraction.value !== undefined && rightLegFraction.value !== null){
                totalLegMeasurements++;
                hasRightFraction = true;
            }
            if(leftLegInches.value !== undefined && leftLegInches.value !== null && leftLegInches.value !== ''){
                totalLegMeasurements++;
                hasLeftInches = true;
            }
            if(leftLegFraction.value !== undefined && leftLegFraction.value !== null){
                totalLegMeasurements++;
                hasLeftFraction = true;
            }
            if(hasLeftInches || hasLeftFraction){
                hasLeftLegValues = true;
            }
            if(hasRightInches || hasRightFraction){
                hasRightLegValues = true;
            }
            if(totalLegMeasurements === 2){   
                if(hasRightLegValues && !hasLeftLegValues || hasLeftLegValues && !hasRightLegValues){
                    console.log('single leg validated ', totalLegMeasurements);
                    validated = true;
                }   
            } 

            return validated;

        }

        checkGrilleValidation(){
            let validated  = false;
            let hasSashOneValues = false;
            let hasSashTwoValues = false;
            let hasHubs = false;
            let hasSpokes = false;
            let S1LitesWide;
            let S1LitesHigh;
            let S2LitesWide;
            let S2LitesHigh;
            let hubs;
            let spokes;
            if(this.productFieldControl.Lites_Wide_S1__c){
                S1LitesWide = this.template.querySelector('lightning-combobox.Lites_Wide_S1__c');
            }
            if(this.productFieldControl.Lites_High_S1__c){
                S1LitesHigh = this.template.querySelector('lightning-combobox.Lites_High_S1__c');
            }
            if(this.productFieldControl.Lites_Wide_S2__c){
                S2LitesWide = this.template.querySelector('lightning-combobox.Lites_Wide_S2__c');
            }
            if(this.productFieldControl.Lites_High_S2__c){
                S2LitesHigh = this.template.querySelector('lightning-combobox.Lites_High_S2__c');
            }
            if(this.productFieldControl.Hubs__c){
                hubs = this.template.querySelector('lightning-combobox.Hubs__c');
            }
            if(this.productFieldControl.Spokes__c){
                spokes = this.template.querySelector('lightning-combobox.Spokes__c');
            }
            //check sash 1 fields
            if(S1LitesWide !== undefined &&  S1LitesHigh !== undefined){
                if(S1LitesWide.value !== null &&  S1LitesHigh.value !== null && S1LitesWide.value !== undefined && S1LitesHigh.value !== undefined  ){
                    hasSashOneValues = true;
                    console.log('has sash one', hasSashOneValues);
                }
            }
            //grab sash 2 fields
            if(S2LitesWide !== undefined && S2LitesHigh !== undefined){
                if(S2LitesWide.value !== null &&  S2LitesHigh.value !== null && S2LitesWide.value !== undefined && S2LitesHigh.value !== undefined ){
                    hasSashTwoValues = true;
                    console.log('has sash two', hasSashTwoValues);
                }
            }
            //grab hub field
            if(hubs !== undefined && hubs !== undefined){
                if(hubs.value !== null &&  hubs.value !== undefined){
                    hasHubs = true;
                    console.log('has hubs', hasHubs);
                }
            }
            //grab spoke field
            if(spokes !== undefined && spokes !== undefined){
                if(spokes.value !== null &&  spokes.value !== undefined){
                    hasSpokes = true;
                    console.log('has spokes', hasSpokes);
                }
            }
            if(hasSashOneValues || hasSashTwoValues){
                validated = true;
            }
            if(hasHubs && hasSpokes){
                validated = true;
            }
            console.log('check grille validation???',validated );
            return validated;
        }

        checkProduct2Validation(){
            let validated = false;
            console.log('product2 field validation',this.productRecordId, this.productName);
            if(this.productRecordId && this.productName){
                validated = true;
            }
            return validated;
        }

        checkFieldValidation(fieldName){
            let validated = true;
            let field = this.template.querySelector('lightning-input-field.' + fieldName);
            if(field.value === null || field.value === '' || field.value === undefined){
                validated = false;
            }
            console.log('in check order id ',validated);
            return validated;
        }

        // determines if makability should be checked before saving a record.
        // currently designed to skip makailibty if record is NSBP or if not a master record type
        // need to update --- must create one check to handle both options. 
        checkRunWindowMakability(){
            let runMakability = true;
            let isNSPR = false;
            if(this.productFieldControl.NSPR__c){
                let nsprField = this.template.querySelector('lightning-input.NSPR__c');
                isNSPR = nsprField.checked;
                console.log('NSPR value?',isNSPR);
            }
            if(isNSPR === true){
                console.log('hit NSPR true');
                runMakability = false;
            }
            // if(this.isMasterProduct === false){
            //     console.log('hit masterProduct false');
            //     runMakability = false;
            // }
            if(this.runMakabilityType !== 'Window'){
                console.log('hit run makability type not windows');
                runMakability = false;
            }
            return runMakability;
        }
        checkIfNSPR(){
            let isNSPR = false;
            if(this.productFieldControl.NSPR__c){
                let nsprField = this.template.querySelector('lightning-input.NSPR__c');
                isNSPR = nsprField.checked;
                console.log('NSPR value?',isNSPR);
            }
            return isNSPR;
        }


        checkForMasterProduct(record){
            console.log('record in check for master product', record);
            if(pivotalIdsForMakability.includes(record.Pivotal_Id__c)){
                console.log('hit pivotal id match');
                this.isMasterProduct = true;
                //RTTFRONT-703
                controlQuantityForMasterProduct(this.isMasterProduct,this.template.querySelector('lightning-input.Quantity'));
            } else{
                console.log('hit pivotol id no match');
                this.isMasterProduct = false;
                //RTTFRONT-703
                controlQuantityForMasterProduct(this.isMasterProduct,this.template.querySelector('lightning-input.Quantity'));
            }
        }
        // record.Description = data.fields.Description.value;
        // record.Unit_Short_Description__c = data.fields.Unit_Short_Description__c.value;
        // this.checkForProductDescription(record);
        checkForProductDescription(record){
            console.log('prod description', record.Description);
            console.log('prod unit short description', record.Unit_Short_Description__c);
            if (record.Description !== null && record.Description !== undefined ){
                this.showProductDescription = true;
                this.productDescription = record.Description;
            } else {
                this.showProductDescription = false;
            }
        }
        // disable retail purchase order field if order is ROP record type and Vendor is not Renewal by andersen.
        checkVendorType(record){
            console.log('Vendor in Check Vendor', record.VendorName);
            if(record.VendorName !== 'Renewal by Andersen'){
                console.log('hit disable rpo field');
                let rpoField = this.template.querySelector('lightning-input-field.Retail_Purchase_Order__c');
                console.log('retail purcahse order field in check vendor ',rpoField); 
                if(rpoField !== null && rpoField !== undefined){
                    rpoField.value = null;
                    console.log('retail purcahse order field value?!?!? should be null ', rpoField.value); 
                    rpoField.disabled = true;
                }   
            } else {
                console.log('hit enable rpo field');
                let rpoField = this.template.querySelector('lightning-input-field.Retail_Purchase_Order__c');
                if(rpoField !== null && rpoField !== undefined){
                    rpoField.disabled = false;
                }                   
            }
        }

        buildMakabilityMessage(resultsArray){
            let message = '';
            for(let i = 0; i<resultsArray.length; i++){
                if(resultsArray[i].isMakable === false){
                    let resultsList = resultsArray[i].errorMessages;
                    for(let j = 0; j<resultsList.length;j++){
                        message = resultsList[j];
                        console.log(message);
                        this.makabilityErrors.push({Id:i+' '+j,errorMessage: message});
                    }
                }
            }
            if(this.makabilityErrors.length === 0){
             //  this.makabilityErrors.push({Id:0,errorMessage:'All Passed'});
             console.log('makability passed');
            }
            if(this.makabilityErrors.length > 0){
                this.makabilityModel = true;
                this.passedMakability = false;
            }
            
        }

        displayMakabilityResults(message) {
            const evt = new ShowToastEvent({
                title: 'Makability Error',
                message: message,
                mode:'pester',
                variant: 'error',
            });
            this.dispatchEvent(evt);
        }

        validationToast(message,title,mode,variant){
            console.log('toast in validation Toast lwc',message);
            if(this.inRop === true){
                console.log('hit in rop === "true", validation toast ');
                this.communityValidationToast(title,message,mode, variant);
            } else {
                console.log('validation toast - not in ROP. must be visualforce');
                let evt = new CustomEvent('validationtoast',
                {
                    detail: {message}
                });
                this.dispatchEvent(evt); 
            }

        }

        communityValidationToast(title,message,mode, variant){
            const evt = new ShowToastEvent({
                title: title,
                message: message,
                mode:mode,
                variant:variant,
            });
            this.dispatchEvent(evt);
        }

        getFormValues(){
            let currentFields = this.template.querySelectorAll( 'lightning-input, lightning-combobox, lightning-input-field ');
            let form = {};
            for(let field of currentFields){
                let fieldName = field.name ? field.name : field.fieldName;
                let fieldValue = field.value;
                if(field.type === 'checkbox'){
                    fieldValue = field.checked;
                }
                if(fieldName === "Product_Description"){
                    continue;
                }
                if(fieldValue === ''){
                    fieldValue = null;
                }                
                form[fieldName] = fieldValue;
            }
            if(this.productRecordId){
                form.Product2Id = this.productRecordId;
            }
            console.log('form in get all form values',form);
            return form;
        }        

        clearForm(){

            // TODO - possibly not needed
            // if(msgString !== 'product2IdUpdated'){
            // //    let product2Field = this.template.querySelector('lightning-input-field.Product2Id');
            // //    product2Field.value = null;
            //     this.productName = null;
            //     this.productRecordId = null;
            // } 
            if(this.editMode){
                this.orderItemId = null;
                this.orderItem = {};
                this.editMode = false;
            }
            this.productFieldControl = this.DEFAULTPFC;
            this.showProductDescription = false;
            this.setOrderId = true;


        }

        redirectToOrderPage(){
            if(this.inRop != true){
                console.log('NOT ROP');
                let orderId = this.template.querySelector('lightning-input-field.OrderId').value
                console.log('order id in cancel ', this.orderId);
                window.open(this.returnBaseUrl + orderId ,'_top');    
            }
            else{
                console.log('YES ROP');
                this[NavigationMixin.Navigate]({
					type: 'standard__commPage',
					attributes: {
						name: 'Order',
					},
					state: {
						orderId: this.configid,
					}
				});
            }
                      
        }
        redirectForSaveAndNew(){
            let orderId = this.template.querySelector('lightning-input-field.OrderId').value
            console.log('order id in redirect save and new ', this.orderId);
            window.open(this.saveAndNewBaseUrl+'?orderId=' + orderId ,'_top');              
        }
        redirectForSaveAndView () {
            console.log('order id in redirect save and new ', this.orderItemId);
            window.open(this.saveAndViewBaseUrl + '?orderItemId=' + this.orderItemId +'&orderId=' + this.orderId,'_top'); 
        }
        closeMakabilityModel(){
            this.makabilityErrors = [];
            this.makabilityModel = false;
        }

        //**************************End Handle Submit **************************************** */


        //************************************************************************************
        //************************Product Field Control Dependency Logic**********************
        // This section handles logic to disable and enable fields based on inputs ***********
        //************************************************************************************


        // disable fields based on product field control dependency records
         // pfcd map should be the contrlolling field apiname as the key, and a possible array of pfcds as the value
        buildProductFieldDependancyMap(pfcds){
            console.log('hit build PFCD map ', pfcds);
            let placeholder = {};
            let tempArray = [];
            for(let i = 0; i<pfcds.length;i++){
                if(placeholder[pfcds[i].Controlling_Field__r.Field_Control_ID__r.Target_Api_Name__c]){
                    tempArray = placeholder[pfcds[i].Controlling_Field__r.Field_Control_ID__r.Target_Api_Name__c];
                }
                tempArray.push(pfcds[i]);
                placeholder[pfcds[i].Controlling_Field__r.Field_Control_ID__r.Target_Api_Name__c] = tempArray;
            }
            this.productFieldControlDependencyMap = placeholder;
            console.log(' this.pfcdMap',this.productFieldControlDependencyMap );
        //    this.setRequiredFields(this.pfcResponse);

        }


        handleProductFieldDependency(fieldName, fieldValue) {
             console.log('fired handle pfcd ',fieldName, fieldValue );
             console.log('this.pfcdArray before loop',this.productFieldControlDependencyMap[fieldName]);
             //console.log('this.disabled fields before undue '+JSON.stthis.disabledFields);
            let inputFields = this.template.querySelectorAll('lightning-input, lightning-combobox, lightning-input-field ');
            let placeholder = [];

            // check and see if we need to undue a previously applied pfcd.
            if (this.disabledFields[fieldName]) {
                let unduePfcd = this.disabledFields[fieldName];
                console.log('pfcd to undue '+ JSON.stringify(unduePfcd));
                for (let pfcd of unduePfcd) {
                    if (pfcd.Controlling_Value__c !== fieldValue) {
                        let dependentField = pfcd.Dependent_Field__r.Field_Control_ID__r.Target_Api_Name__c;
                        console.log('undue ' + pfcd.Action_Taken__c + ' to ' + dependentField);
                        for (let input of inputFields) {
                            if (input.name === dependentField && pfcd.Action_Taken__c === 'Disable') {
                                console.log('undue disable');
                                input.disabled = false;
                                if (pfcd.removedRequired) {
                                    input.required = true;
                                }
                            }
                            if (input.name === dependentField && pfcd.Action_Taken__c === 'Enable') {
                                console.log('hit undo enabled');
                                input.disabled = true;
                                if (input.checked === true) {
                                    input.checked = false;
                                }
                                if (input.value !== null) {
                                    input.value = null;
                                }
                            }
                            if (input.name === dependentField && pfcd.Make_Required__c === true) {
                                console.log('hit undo required');
                                input.required = false;
                            }
                        }
                    }
                }
                // once we have undone a pfcd, remove it from the list.
                this.disabledFields[fieldName] = null;
            }
            if (this.productFieldControlDependencyMap[fieldName]) {
                for (let pfcd of this.productFieldControlDependencyMap[fieldName]) {
                    console.log('pfcd Value: '+JSON.stringify(pfcd));
                    if(pfcd.Controlling_Value__c === 'true'){
                         pfcd.Controlling_Value__c = true;
                        }
                    if (pfcd.Controlling_Value__c === fieldValue && pfcd.Controlling_Field__r.Field_Control_ID__r.Target_Api_Name__c === fieldName) {
                        let dependentField = pfcd.Dependent_Field__r.Field_Control_ID__r.Target_Api_Name__c;
                        console.log('apply ' + pfcd.Action_Taken__c + ' to ', dependentField);
                        for (let input of inputFields) {
                            if (input.name === dependentField && pfcd.Action_Taken__c === 'Disable') {
                                console.log('hit disabled');
                                // clear the checkbox,
                                if (input.checked === true) {
                                    input.checked = false;
                                }
                                // clear the field on the form.
                                if (input.value !== null) {
                                    input.value = null;
                                }
                                if (input.required === true) {
                                    input.required = false;
                                    pfcd.removedRequired = true;
                                }
                                input.reportValidity();
                                input.disabled = true;


                            }
                            if (input.name === dependentField && pfcd.Action_Taken__c === 'Enable') {
                                console.log('hit enabled');
                                //We want to enable the field if it is disabled, and make sure to 
                                //show the field if it has been removed by product field controls.
                                input.disabled = false;
                                this.productFieldControl[dependentField] = true;
                            }
                            if (input.name === dependentField && pfcd.Make_Required__c === true) {
                                console.log('hit required');
                                input.required = true;
                            }
                        }
                        placeholder.push(pfcd);
                    }
                }
                this.updateDisabledPfcdMap(fieldName, placeholder);
            }
        }

        updateDisabledPfcdMap(fieldName, pfcdArray) {
            this.disabledFields[fieldName] = pfcdArray;
            console.log(' disabled field list', JSON.parse(JSON.stringify(this.disabledFields)));
        }

        //*****************************End Product Field Control Dependencies ************* */

        
        //*******************************************************************************
        //*********************************Set Special Options***************************
        // Logic to handle setting values to special option section fields 
        // when the special options checkbox is checked *********************************
        //*******************************************************************************

        // used to default special options values based on exterior and interior color/grille style/pattern choices above
        setSpecialOptions(){
            console.log('hit set special options');
            
            let currentFields = this.template.querySelectorAll( 'lightning-input, lightning-combobox, lightning-input-field ');
            let interiorSash = this.template.querySelector('lightning-combobox.Interior_Sash_Color__c');
            let hardwareColor = this.template.querySelector('lightning-combobox.Hardware_Color__c');
            let screenColor = this.template.querySelector('lightning-combobox.Screen_Color__c');
            let interiorGrilleColor = this.template.querySelector('lightning-combobox.Interior_Grille_Color__c');
            let exteriorGrilleColor = this.template.querySelector('lightning-combobox.Exterior_Grille_Color__c');
            let exteriorTrimColor = this.template.querySelector('lightning-combobox.Exterior_Trim_Color__c');
            invokeSpecialOptions(currentFields,interiorSash,hardwareColor,screenColor,interiorGrilleColor,exteriorGrilleColor,exteriorTrimColor,this.screenColorDefault);
            
        }

       
        handleFormLoad( event)
        {
        
       event.preventDefault();   
       
        const serachfocus=  this.template.querySelector('c-order-item-selector ');
        console.log('the focus is'+serachfocus);
        if(this.focusOnProduct===false)
        {
        serachfocus.searchFocus();
        this.focusOnProduct=true;
        }
        }

        setScreenType(value) {
            let applicableInteriorColors = [];
                applicableInteriorColors = this.customlabels.productInteriorColors.split(",");
                let applicableProductCodes=[];
                applicableProductCodes= this.customlabels.productCodes.split(",");
                console.log('Applicable Interior Colours: '+applicableInteriorColors);
                console.log('Applicable this.productCode value: '+this.productCode);
                if(value && applicableInteriorColors.indexOf(value)!=-1 && this.productCode && applicableProductCodes.indexOf(this.productCode) !=-1) {
                    console.log('Screen type clone: '+JSON.stringify(this.screenTypeListClone));
                    this.screenTypeList= [...this.screenTypeListClone];
                }else if(this.productCode && applicableProductCodes.indexOf(this.productCode) !=-1){
                    console.log('Console log in else if: '+JSON.stringify(this.screenTypeListClone));
                    this.screenTypeList= [...this.screenTypeListClone];
                    for( var i = 0; i < this.screenTypeList.length; i++){
                         if ( this.screenTypeList[i].value === this.customlabels.productScreenType) { 
                            this.screenTypeList.splice(i, 1); i--; 
                            }
                        }
                }
                else {
                    this.isDisableScreenType=false;

                    /*Variant2*/
                    this.screenTypeList= [...this.screenTypeListClone];
                    /*Variant2*/
                }
        }
       
        checkGLMakeability(prCode,sashOp,scType) {
            let sashOpslabel = [];
                sashOpslabel = this.customlabels.sashOperation.split(";");
                let scTypeLabel=[];
                scTypeLabel= this.customlabels.screenType.split(";");
            let screenSize= this.template.querySelector('lightning-combobox.Screen_Size__c');
            if(prCode && prCode===this.customlabels.glidingProductCode && sashOp && sashOpslabel.indexOf(sashOp)!=-1 && scType && scTypeLabel.indexOf(scType)!=-1 && screenSize) {
                screenSize.required = true; 
            }else if(this.requiredFields && screenSize && !(this.requiredFields.indexOf(screenSize.name) !=-1)){
                 console.log('requiredFields values: '+JSON.stringify(this.requiredFields));
                 screenSize.required = false; 
                 screenSize.value =''; 
            }

        }

        handlePerformanceCategoryFieldDependency(value) {
            if(value == this.customlabels.costalPerfrmCategory) {
                if(this.sizedetailConfigClone) {
                    this.positiveForceList = this.buildPositiveForceList(this.sizedetailConfigClone);
                    this.negativeForceList = this.buildNegativeForceList(this.sizedetailConfigClone);
                }
            }else if(value !== this.customlabels.costalPerfrmCategory) {
                if(this.sizedetailConfigClone) {
                    this.positiveForceList = this.buildPositiveForceList(this.sizedetailConfigClone);
                    this.negativeForceList = this.buildNegativeForceList(this.sizedetailConfigClone);
                   }
            } 
            
            
             
        }
       

        controlPerformanceCategory(dpvalue) {
            console.log('Console at 2567: '+dpvalue);
                if(dpvalue ==true || dpvalue =='true') {
                    let performanceCategory = this.template.querySelector('lightning-combobox.Performance_Category__c');
                    if(performanceCategory) {
                        console.log('Console at 2572: '+performanceCategory.value);
                        if(this.isPerfCategoryMandatory==undefined) {
                            this.isPerfCategoryMandatory = performanceCategory.required;
                        }
                            performanceCategory.disabled=true;
                            performanceCategory.required=false;
                            performanceCategory.value = null;
                            // To reset positive and negative force valeus
                            if(this.sizedetailConfigClone) {
                                this.positiveForceList = this.buildPositiveForceList(this.sizedetailConfigClone);
                                this.negativeForceList = this.buildNegativeForceList(this.sizedetailConfigClone);
                            }
                    }

                }
                else if(dpvalue ==false || dpvalue =='false') {
                    let performanceCategory = this.template.querySelector('lightning-combobox.Performance_Category__c');
                    if(performanceCategory) {
                        console.log('Console at 2587:  '+performanceCategory.value);
                        if(this.isPerfCategoryMandatory==undefined) {
                            this.isPerfCategoryMandatory = performanceCategory.required;
                        }
                        performanceCategory.disabled=false;
                        performanceCategory.required=this.isPerfCategoryMandatory;
                        performanceCategory.value = null;
                    }

                }

            
        }


        //***********************End Set Special Options ****************************************

        ///////////////////////////////////////End File/////////////////////////////////////////////////
}