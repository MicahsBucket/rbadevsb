import { LightningElement, api } from 'lwc';

export default class cancelProductCard extends LightningElement{
	@api rowId;
	@api miscItems = [];
	@api cancellationReason;
	@api hasError;
	@api errorMessage;

    handleSelect(event) {

		this.cancellationReason = event.detail.value;

        // Custom event that bubbles to Parent component.
        const selectEvent = new CustomEvent('cancelreasonselect', {
            bubbles: true
		});
		
        // Fire the custom event
		this.dispatchEvent(selectEvent);
		
	}
	
}