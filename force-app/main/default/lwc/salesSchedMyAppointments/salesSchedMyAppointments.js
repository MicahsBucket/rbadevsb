import { LightningElement, track, wire } from 'lwc';
import getMyAppointments from '@salesforce/apex/SalesSchedAppointmentManagerCtrl.getMyAppointments';
import { refreshApex } from '@salesforce/apex';

export default class SalesSchedMyAppointments extends LightningElement {
    @track error;
    @track day = "";
    @track appointmentsWrapper;
    @track resultOptions;
    @track dayFormatted;
    appointmentWrapperResult;

    @wire(getMyAppointments,{day : '$day'})
    handleGetAppointmentsWrapper(result){
        this.appointmentWrapperResult = result;
        if(result && result.data){
            this.resultOptions = [
                { label: '-- None --', value: ''},
                { label: 'Sale', value: 'Sale' },
                { label: 'Demo No Sale', value: 'Demo No Sale' },
                { label: 'No Demo', value: 'No Demo' },
                { label: 'Not Home', value: 'Not Home' },
                { label: 'Not Covered', value: 'Not Covered' }
            ];

            this.paymentTypeOptions = [
                { label: '-- None --', value: ''},
                { label: 'Cash', value: 'Cash' },
                { label: 'Finance', value: 'Finance' },
                { label: 'Cash & Finance', value: 'Cash & Finance' },
                { label: 'Finance Turn-Down', value: 'Finance Turn-Down' }
            ];

            this.yesNoOptions = [
                { label: '-- None --', value: ''},
                { label: 'Yes', value: 'Yes' },
                { label: 'No', value: 'No' }
            ];

            let day = result.data.day;

            //this.day = result.data.day;
            this.appointmentsWrapper = result.data;
            console.log("___Ratna____this.appointmentsWrapper________" + JSON.stringify(this.appointmentsWrapper));
            this.dayFormatted = day;
        }

        if(result && result.error){
            this.error = result.error;
        }
    }

    handleDayChange(event){
        if(event.detail){
            this.day = event.detail.format('YYYY-MM-DD');
        }
    }

    handleRefreshAppointments(){
        refreshApex(this.appointmentWrapperResult);
    }
}