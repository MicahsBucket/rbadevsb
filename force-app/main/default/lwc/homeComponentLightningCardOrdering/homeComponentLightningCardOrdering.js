import { LightningElement, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getPurchaseOrderList from '@salesforce/apex/HomeCompLightningCardOrdering.getPurchaseOrderList';
import getReadyToOrderList from '@salesforce/apex/HomeCompLightningCardOrdering.getReadyToOrderList';
import getOrderReleasedList from '@salesforce/apex/HomeCompLightningCardOrdering.getOrderReleasedList';

const ReadyToOrderColumns = [
    {label: 'Order Number', fieldName: 'ReadyToONumberURL', type: 'url', typeAttributes: {'label': { fieldName: 'ReadyToONumber'}, 'target': '_blank', 'tooltip': {fieldName: 'ReadyToONumber'}}, sortable: true}, 
    {label: 'Account Name', fieldName: 'ReadyToOAccountURL', type: "url", typeAttributes: {'label': { fieldName: 'ReadyToOAccountName'}, value:{fieldName: 'ReadyToOAccountURL'}, target: '_blank', 'tooltip': {fieldName: 'ReadyToOAccountName'}}, sortable: true}, 
    {label: 'Bill To Contact', fieldName: 'ReadyToOBillToContactURL', type: "url", typeAttributes: {'label': { fieldName: 'ReadyToOBillToContactName'}, value:{fieldName: 'ReadyToOBillToContactURL'}, target: '_blank', 'tooltip': {fieldname: 'ReadyToOBillToContactName'}}, sortable: true}, 
    {label: 'Tech Measure Complete Date', fieldName: 'ReadyToOCompleteDate', type: 'date', sortable: true}
    ]; 


const PurchaseOrderColumns = [
    {label: 'Purchase Order Number', fieldName: 'PurchaseOrderNumberURL', type: "url", typeAttributes: {'label': { fieldName: 'PurchaseOrderNumber'}, value:{fieldName: 'PurchaseOrderNumberURL'}, 'target': '_blank', 'tooltip': {fieldName: 'PurchaseOrderNumber'}},  sortable: true}, 
    {label: 'Vendor', fieldName: 'Vendor', type: 'text', sortable: true},
    {label: 'Status', fieldName: 'POStatus', type: 'text', sortable: true},
    {label: 'Reference', fieldName: 'Reference', type: 'text', sortable: true}
    ]; 

const OrderReleasedColumns = [
    {label: 'Order Number', fieldName: 'OrderRNumberURL', type: "url", typeAttributes: {'label': { fieldName: 'OrderRNumber'}, value:{fieldName: 'OrderRNumberURL'}, 'target': '_blank', 'tooltip': {fieldName: 'OrderRNumber'}}, sortable: true}, 
    {label: 'Account Name', fieldName: 'OrderRAccountURL', type: "url", typeAttributes: {'label': { fieldName: 'OrderRAccountName'}, value:{fieldName: 'OrderRAccountURL'}, 'target': '_blank', 'tooltip': {fieldName: 'OrderRAccountName'}}, sortable: true}, 
    {label: 'Bill To Contact', fieldName: 'OrderRBillToContactURL', type: "url", typeAttributes: {'label': { fieldName: 'OrderRBillToContactName'}, value:{fieldName: 'OrderRBillToContactURL'}, 'target': '_blank', 'tooltip': {fieldName: 'OrderRBillToContactName'}}, sortable: true}, 
    {label: 'Time - Order Released', fieldName: 'OrderRTime', type: 'date', sortable: true}
    ]; 

export default class HomeComponentLightningCardOrdering extends LightningElement {

    @track ReadyToOrderColumns=ReadyToOrderColumns;
    @track listReadyToOrder;
    @track listReadyToOrderDup;
    @track rtoDataLength;
    @track rtoDataLengthWord = "items";

    @track PurchaseOrderColumns=PurchaseOrderColumns;
    @track listPurchaseOrder;
    @track listPurchaseOrderDup;
    @track poDataLength;
    @track poDataLengthWord = "items";
  
    @track OrderReleasedColumns=OrderReleasedColumns;
    @track listOrderReleased;
    @track listOrderReleasedDup;
    @track orDataLength;
    @track orDataLengthWord = "items";

    sVal = '';

    @wire(getPurchaseOrderList, {searchKey: ''})
    wiredPurchaseOrderListMethod({ error, data }) {             
        if (data) {           
            this.listPurchaseOrder =  data;
            this.listPurchaseOrderDup =  data;   
            this.poDataLength = data.length;
            if(this.poDataLength === 1){
                this.poDataLengthWord = "item";
             }         
        } else if (error) {
            this.error = error;
           }     
           console.log('List Purchase Order ' +JSON.stringify(data));
           console.log('List Purchase Order 2 ' +JSON.stringify( this.listPurchaseOrder));
           console.log('PO Length: ' + this.poDataLength);       
    }

    @wire(getReadyToOrderList, {searchKey: ''})
    wiredReadyToOrderListMethod({ error, data }) {             
        if (data) {           
            this.listReadyToOrder =  data;
            this.listReadyToOrderDup =  data;    
            this.rtoDataLength = data.length;
            if(this.rtoDataLength === 1){
                this.rtoDataLengthWord = "item";
             }            
        } else if (error) {
            this.error = error;
           }     
           console.log('List Ready To Order ' +JSON.stringify(data));
           console.log('List Ready To Order 2 ' +JSON.stringify( this.listReadyToOrder));
           console.log('RTO Length: ' + this.rtoDataLength);         
    }

    @wire(getOrderReleasedList, {searchKey: ''})
    wiredOrderReleasedListMethod({ error, data }) {             
        if (data) {           
            this.listOrderReleased =  data; 
            this.listOrderReleasedDup =  data; 
            this.orDataLength = data.length;
            if(this.orDataLength === 1){
                this.orDataLengthWord = "item";
             }              
        } else if (error) {
            this.error = error;
           }     
           console.log('List Order Released ' +JSON.stringify(data));
           console.log('List Order Released 2 ' +JSON.stringify( this.listOrderReleased));
           console.log('OR Length: ' + this.orDataLength);         
    }

    updateSearchKeyPurchaseOrder(event) {
        this.sVal = event.target.value;
        this.handleSearchPurchaseOrder();
    }

    handleSearchPurchaseOrder() {
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getPurchaseOrderList({
                    searchKey: this.sVal
                })
                .then(result => {
                    // set @track contacts variable with return contact list from server  
                    this.listPurchaseOrder = result;
                })
                .catch(error => {
                    // display server exception in toast msg 
                    const event = new ShowToastEvent({
                        title: 'Error',
                        variant: 'error',
                        message: error.body.message,
                    });
                    this.dispatchEvent(event);
                    // reset contacts var with null   
                    this.listPurchaseOrder = null;
                });
        } else {
           
            this.listPurchaseOrder = this.listPurchaseOrderDup;
        }
    }

    purchaseOrderSortData(fieldName, sortDirection){
        var data = JSON.parse(JSON.stringify(this.listPurchaseOrder));
        
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        
                data.sort((a,b) => {
                    let valueA = key(a) ? key(a).toLowerCase() : ''; 
                    let valueB = key(b) ? key(b).toLowerCase() : '';
                    return reverse * ((valueA > valueB) - (valueB > valueA));
                });

        this.listPurchaseOrder = data;
    }    
    purchaseOrderUpdateColumnSorting(event){
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.purchaseOrderSortData(this.sortedBy,this.sortedDirection);
    }

    updateSearchKeyReadyToOrder(event) {
        this.sVal = event.target.value;
        this.handleSearchReadyToOrder();
    }

    handleSearchReadyToOrder() {
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getReadyToOrderList({
                    searchKey: this.sVal
                })
                .then(result => {
                    // set @track contacts variable with return contact list from server  
                    this.listReadyToOrder = result;
                })
                .catch(error => {
                    // display server exception in toast msg 
                    const event = new ShowToastEvent({
                        title: 'Error',
                        variant: 'error',
                        message: error.body.message,
                    });
                    this.dispatchEvent(event);
                    // reset contacts var with null   
                    this.listReadyToOrder = null;
                });
        } else {
           
            this.listReadyToOrder = this.listReadyToOrderDup;
        }
    }

    readyToOrderSortData(fieldName, sortDirection){
        var data = JSON.parse(JSON.stringify(this.listReadyToOrder));
        
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        
                data.sort((a,b) => {
                    let valueA = key(a) ? key(a).toLowerCase() : ''; 
                    let valueB = key(b) ? key(b).toLowerCase() : '';
                    return reverse * ((valueA > valueB) - (valueB > valueA));
                });

        this.listReadyToOrder = data;
    }    
    readyToOrderUpdateColumnSorting(event){
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.readyToOrderSortData(this.sortedBy,this.sortedDirection);
    }

    updateSearchKeyOrderReleased(event) {
        this.sVal = event.target.value;
        this.handleSearchOrderReleased();
    }

    handleSearchOrderReleased() {
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getOrderReleasedList({
                    searchKey: this.sVal
                })
                .then(result => {
                    // set @track contacts variable with return contact list from server  
                    this.listOrderReleased = result;
                })
                .catch(error => {
                    // display server exception in toast msg 
                    const event = new ShowToastEvent({
                        title: 'Error',
                        variant: 'error',
                        message: error.body.message,
                    });
                    this.dispatchEvent(event);
                    // reset contacts var with null   
                    this.listOrderReleased = null;
                });
        } else {
           
            this.listOrderReleased = this.listOrderReleasedDup;
        }
    }

    orderReleasedSortData(fieldName, sortDirection){
        var data = JSON.parse(JSON.stringify(this.listOrderReleased));
        
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        
                data.sort((a,b) => {
                    let valueA = key(a) ? key(a).toLowerCase() : ''; 
                    let valueB = key(b) ? key(b).toLowerCase() : '';
                    return reverse * ((valueA > valueB) - (valueB > valueA));
                });

        this.listOrderReleased = data;
    }    
    orderReleasedUpdateColumnSorting(event){
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.orderReleasedSortData(this.sortedBy,this.sortedDirection);
    }
}