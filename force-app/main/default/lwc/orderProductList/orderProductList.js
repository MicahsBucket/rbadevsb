/**
 * @File Name          : orderProductList.js
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : jason.flippen@andersencorp.com
 * @Last Modified On   : 2/9/2021, 8:15 AM
 * @Modification Log   : 
 *====================================================================================================
 * Ver	Date					Author      		      			Modification
 *====================================================================================================
 * 1.0	6/6/2019, 3:02 PM		mark.rothermal@andersencorp.com		Initial Version
 * 2.0	2/9/2021, 8:15 AM		jason.flippen@andersencorp.com		Added title & showData fields to suppor front-end styling.
**/
import { LightningElement, track, api, wire } from 'lwc';
import getOrderItems from '@salesforce/apex/OrderProductListController.getOrderItems';
import getShowOrderButton from '@salesforce/apex/OrderProductListController.showProductButton';
import getShowComponent from '@salesforce/apex/OrderProductListController.showOrderList';
import cancelOrderItems from '@salesforce/apex/OrderProductListController.cancelOrderItems';
import getIsSandboxOrg from '@salesforce/apex/UtilityMethods.isSandboxOrg';	
import getIsrForceUser from '@salesforce/apex/UtilityMethods.isrForceUser';	
import getUrlforConfiguration from '@salesforce/apex/DaylConfigurationCallout.getUrlforConfiguration';
import checkForNewConfigPricing from '@salesforce/apex/DaylConfigurationOutputsCallout.checkForNewConfigPricing';
import { NavigationMixin } from 'lightning/navigation';
import getUrlforReconfiguration from '@salesforce/apex/DaylConfigurationCallout.getUrlforReconfiguration';
import setOrderItemEligibleforReconfiguration from '@salesforce/apex/DaylConfigurationCallout.setOrderItemEligibleforReconfiguration';
import { refreshApex } from '@salesforce/apex';
//import userId from '@salesforce/user/Id';

const actions = [
	{ label: 'View', name: 'view' },
	{ label: 'View Daylight Configuration', name: 'viewConfig' },
	{ label: 'View Product Splitlines', name: 'viewSplitLines' },
	{ label: 'Edit Opening Configuration', name: 'editConfig' },
	{ label: 'Edit', name: 'edit' },
	{ label: 'Cancel', name: 'cancel' },
];

const columns = [
	{ label: 'Unit Id', fieldName: 'unitId' },
	{ label: 'Product', fieldName: 'productName' },
	{ label: 'Purchase Order', fieldName: 'purchaseOrderName', type: 'string' },
	{ label: 'Quantity', fieldName: 'quantity', type: 'number' },
	{ label: 'Unit Price', fieldName: 'unitPrice', type: 'currency' },
	{ label: 'Total Retail Price', fieldName: 'totalRetailPrice', type: 'currency' },
	{ label: 'Wholesale Cost', fieldName: 'unitWholesaleCost', type: 'currency' },
	{ label: 'Total Wholesale Cost', fieldName: 'totalWholesaleCost', type: 'currency' },
	{ label: 'Variant Number', fieldName: 'variantNumber', type: 'string' },
	{
		type: 'action',
		typeAttributes: { rowActions: actions, menuAlignment: 'Auto' },
	}
];

const ropColumns = [
	{ label: 'Unit Id', fieldName: 'unitId' },
	{ label: 'Product', fieldName: 'productName' },
	{ label: 'Purchase Order (Retail)', fieldName: 'retailPurchaseOrderNameOPL', type: 'string' },
	{ label: 'Quantity', fieldName: 'quantity', type: 'number' },
	{ label: 'Unit Price', fieldName: 'unitPrice', type: 'currency' },
	{ label: 'Total Retail Price', fieldName: 'totalRetailPrice', type: 'currency' },
	{ label: 'Wholesale Cost', fieldName: 'unitWholesaleCost', type: 'currency' },
	{ label: 'Total Wholesale Cost', fieldName: 'totalWholesaleCost', type: 'currency' },
	{ label: 'Variant Number', fieldName: 'variantNumber', type: 'string' },
	{
		type: 'action',
		typeAttributes: { rowActions: actions, menuAlignment: 'Auto' },
	}
];

// set base url for navigation based on environment  component deployed in. rop is retailer order portal community.
// moved to configuration xml. setting sfclassic base urls for use in visualforce.
 const sfClassicEditBaseUrl = '/apex/EditOrderProduct';
 const sfClassicDaylightEditBaseUrl= '/apex/EditDayLightProduct';
 const sfClassicDaylightReadBaseUrl= '/apex/ReadOnlyDaylightProductPage';
 const sfClassicReadBaseUrl = '/apex/ReadOnlyOrderProductPage';
 const sfClassicCancelBaseUrl = '/';
 const sfAROEditBaseUrl = '/rForceARO/apex/EditOrderProduct';
 const sfAROReadBaseUrl = '/rForceARO/apex/ReadOnlyOrderProductPage';
 const sfARODaylightEditBaseUrl = '/rForceARO/apex/EditDayLightProduct';
 const sfARODaylightReadBaseUrl = '/rForceARO/apex/ReadOnlyDaylightProductPage';
 const sfAROCancelBaseUrl = '/rForceARO/';
 const sfRMS = '/apex/RMS_prodConfigEdit?id=';
 const sfARO = '/rForceARO/apex/RMS_prodConfigEdit?id=';

let productsToCancel;
let excludePOStatusList = ["Released","Confirmed","Partially Received","Received"];
let partnerResult;
let sandboxResult;
let sfRMSPage = '';



export default class orderProductList extends NavigationMixin(LightningElement) {
	//TODO update to get userid see below, and use record api to get current order or current order item and 
	// update record as part of naviation. step two. set up create edit page to also get user id, and get the user record to grab the order id and order item id.
	//currentUser = userId;
	@api childData = [];
	@api recordId;
	@api rowId;
	@track columns = columns;
	@track ropColumns = ropColumns;
	@track title = 'Products (0)';
    @track showData = false;
	@track data = [];
	@track error;
	@track selectedCancelReason;
	@track disableSave = true;
	@track openCancelModel = false;
	@track denyCancel = false;
	@track hasCancelError = false;
	@track cancelErrorMessage;
	@track initialPageLoad = true;
	@track isSandboxOrg;
	@track isrForceUser;
	@api inRop;
	@api editBaseUrl;
	@api editDaylightBaseUrl;
	@api readBaseUrl;
	@api readDaylightBaseUrl;
	@api cancelBaseUrl;
	@api showButton;
	@track showComponent;
	@track isLightning;
	@track urlString;
	@track baseURL;

	@track detailId;
    @track headerId;
    @track openConfigData = false;
    @track returnedConfigData;
    @api redirectUrl;
	@track cpqNamespace = 'POC';
    @track cpqName = 'IED';
	@track openIframe = false;

    connectedCallback(){
		this.openIframe = false;
		this.openReconfiguration = false;
		// console.log('THEME === ', UserInfo);
		console.log('recordId DaylTestDisplay', this.recordId);
        console.log('isLightning DaylTestDisplay', this.isLightning);
        // // console.log('BASE URL === ', System.URL.getSalesforceBaseURL().toExternalForm());
        // // String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        // this.showLightningButton = this.isLightning;
        // if(this.showLightningButton == false){
        //     this.getURL();
        //     this.showLightningButton = false;
        // }
        // else{
        //     this.showLightningButton = true;
        // }

        this.configid = this.recordId
        if(this.recordId != null && this.recordId != 'undefined'){
            this.detailId = `Canned_UI_${this.recordId}`;
            this.headerId = `headID_${this.recordId}`;
            // this.getURL();
        }
        else{
            this.headerId = 'Simulator';
            this.detailId = `Canned_UI_TESTING`;
        }

       
		this.urlString = window.location.href;
		this.baseURL = this.urlString.substring(0, this.urlString.indexOf('/s'));
		console.log('baseURL', this.baseURL);
		if(this.baseURL != null && this.baseURL != ''){
			if(this.baseURL.includes('my.salesforce.com') || this.baseURL.includes('visualforce.com')){
				console.log('THIS IS CLASSIC');
				this.isLightning = false;
			}
			else{
				console.log('THIS IS LIGHTNING');
				this.isLightning = true;
				this.redirectUrl = `${this.baseURL}/${this.recordId}`;
			}
		}
		else{
			console.log('THIS IS LIGHTNING TOO');
			this.isLightning = true;
			this.redirectUrl = `https://renewalbyandersen--micahdcdev.lightning.force.com/lightning/r/Order/${this.recordId}/view`;
		}

		if(this.inRop == true){
			console.log('THIS IS LIGHTNING COMMUNITY');
            this.redirectUrl = `${this.baseURL}/${this.recordId}`;
        }
		getIsSandboxOrg ().then(result => {
		console.log('Sandbox Result', result);
		sandboxResult = result;
			this.isSandboxOrg = sandboxResult;	
			

		getIsrForceUser ().then(res => {
		console.log('Partner Result', res);
		partnerResult = res;
			this.isrForceUser = partnerResult;
		
		}).catch(error => {
			this.error = error;
			console.log('error in callback for get Partner User', error);

		}).then(() => {
		if(this.editBaseUrl === undefined && this.readBaseUrl === undefined && 
					this.editDaylightBaseUrl === undefined && this.readDaylightBaseUrl === undefined){
			console.log('Sandbox: ', this.isSandboxOrg);
			console.log('rForce: ', this.isrForceUser);
			
			
			
				if(this.isSandboxOrg === true && this.isrForceUser === true) {
					this.editBaseUrl = sfAROEditBaseUrl;
					this.readBaseUrl = sfAROReadBaseUrl;
					this.editDaylightBaseUrl = sfARODaylightEditBaseUrl;
					this.readDaylightBaseUrl = sfARODaylightReadBaseUrl;
					this.cancelBaseUrl = sfAROCancelBaseUrl;
					sfRMSPage  = sfARO;
				}else {
					this.editBaseUrl = sfClassicEditBaseUrl;
					this.readBaseUrl = sfClassicReadBaseUrl;
					this.editDaylightBaseUrl = sfClassicDaylightEditBaseUrl;
					this.readDaylightBaseUrl = sfClassicDaylightReadBaseUrl;
					this.cancelBaseUrl = sfClassicCancelBaseUrl;
					sfRMSPage  = sfRMS;
				}
			console.log('base url for edit navigation = ', this.editBaseUrl);
			console.log('base url for read only navigation = ', this.readBaseUrl);
			console.log('base url for cancel navigation = ', this.cancelBaseUrl);
			console.log('editDaylightBaseUrl: ', this.editDaylightBaseUrl);
		}
		
	    });


		}).catch(error => {
			this.error = error;
			console.log('error in callback for get Sandbox Org', error);
		});


        console.log('hit connectedCallback inRop', this.inRop);
        console.log('hit connectedCallback recordId', this.recordId);

		checkForNewConfigPricing({OrderId: this.recordId})
        .then((result) => {
            console.log('checkForNewConfigPricing result', result);

			getOrderItems ({ orderId: this.recordId }).then(result => {
				let tempJSON = JSON.parse(JSON.stringify(result).split('miscCharges').join('_children'));
				console.log('tempJSON data', JSON.parse(JSON.stringify(tempJSON)));
				this.data = tempJSON;			
				this.title = 'Products (' + this.data.length + ')';
				if (this.data.length > 0) {
					this.showData = true;
				}
			}).catch(error => {
				this.error = error;
				console.log('error in callback for get order items', error);
			});

			getShowOrderButton ({ orderId: this.recordId }).then(result => {
				this.showButton = result;
			}).catch(error => {
				this.error = error;
				console.log('error in callback for get Show Order Button', error);
			});
			getShowComponent ({ orderId: this.recordId }).then(result => {
				this.showComponent = result;
			}).catch(error => {
				this.error = error;
				console.log('error in callback for get show component method', error);
			});
		})
		.catch((error) => {
			console.log('getConfigurationData Error', error);
		});

	}	
	renderedCallback(){
		console.log('hit rendered callback');
		
		// if(this.initialPageLoad === true ){
		// 	console.log('hit refresh apex');
		// 	refreshApex(this.data);
		// 	this.initialPageLoad = false;
		// }
	}
	// *********************commented out 8-2-19 Mark Rothermal - unable to get refreshApex to work properly with Wire method, moved getOrderItems to connected callback
	// @wire(getOrderItems, { orderId: '$recordId' })
	// wiredOrderItems({error, data}) {
	// 	if (error) {
	// 		this.error = error;
	// 		console.log('error in callback', error);
	// 	} else if (data) {
	// 		let tempJSON = JSON.parse(JSON.stringify(data).split('miscCharges').join('_children'));
	// 		console.log('tempJSON data', JSON.parse(JSON.stringify(tempJSON)));
	// 		this.data = tempJSON;
	// 	}
	// }

	navigateToVF(){
		console.log('In navigateToVF');
		this.redirectUrl = `https://renewalbyandersen--micahdcdev.my.salesforce.com/${this.recordId}`;
		getUrlforConfiguration({namespace:this.cpqNamespace, name:this.cpqName,headerId:this.headerId,detailId:this.detailId, pdateExistingProduct:this.updateExistingProduct, redirectUrl:this.redirectUrl})
        .then((result) => {
            console.log('data.jsonResponse?!?', JSON.parse(JSON.stringify(result.jsonResponse)));
            let response = {};
            response = JSON.parse(JSON.stringify(result.jsonResponse));
            console.log('start config data ', JSON.parse(JSON.stringify(response)));
            //console.log('start config data test ', response);
            this.daylightResponse = JSON.parse(JSON.stringify(response));
			window.open(this.daylightResponse, '_top');
            // this.showIframe = true;
            // let urlToNavigate = this.daylightResponse + this.recordId;
            /////Navigate to INFOR CPQ page/////
            // this[NavigationMixin.Navigate]({
            //     type: 'standard__webPage',
            //     attributes: {
            //         url: urlToNavigate,
            //     }
            // });
            /////Get initial INFOR CPQ configs/////
            // this.getUiStartConfig();
            // this.getUiStartConfig();
        })
        .catch((error) => {
            this.error = error;
            console.log('error in start config callback ', error.jsonResponse);
            
        });
		
	}

	navigateToReconfigVF(){
		console.log('In navigateToReconfigVF');
		this.redirectUrl = `https://renewalbyandersen--micahdcdev.my.salesforce.com/${this.recordId}`;
		setOrderItemEligibleforReconfiguration({orderItemId:this.rowId})
		.then((result) => {
			console.log('setOrderItemEligibleforReconfiguration: ', result);
			if(result == 'Success'){
				getUrlforReconfiguration({namespace:this.cpqNamespace, name:this.cpqName, orderItemId:this.rowId, redirectUrl: this.redirectUrl })
				.then((result) => {
					console.log('data.jsonResponse getUrlforReconfiguration?!?', JSON.parse(JSON.stringify(result.jsonResponse)));
					let response = {};
					response = JSON.parse(JSON.stringify(result.jsonResponse));
					console.log('start config data ', JSON.parse(JSON.stringify(response)));
					this.daylightResponse = JSON.parse(JSON.stringify(response));
					window.open(this.daylightResponse, '_top');

				})
				.catch((error) => {
					this.error = error;
					console.log('error in start config callback ', error.jsonResponse);
				});
			}
		})
		.catch((error) => {
			this.error = error;
			console.log('error in start config callback ', error.jsonResponse);
		});
		
	}

	handleRowAction(event) {

		console.log('In Row Action');
		const actionName = event.detail.action.name;
		const row = event.detail.row;
		console.log('stringify row: ' + JSON.parse(JSON.stringify(row)));
		this.rowId = row.id;
		this.denyCancel = false;
		switch (actionName) {
			case 'cancel':
				// If the selected Product has a related Purchase Order,
				// make sure its Status is not one that is excluded. If
				// it is we need to deny the ability to cancel it.
				if (row.purchaseOrderStatus !== '' && row.purchaseOrderStatus !== undefined) {
					this.denyCancel = excludePOStatusList.includes(row.purchaseOrderStatus);
				}				
				if (row.retailPurchaseOrderStatus !== '' && row.retailPurchaseOrderStatus !== undefined) {
					this.denyCancel = excludePOStatusList.includes(row.retailPurchaseOrderStatus);
				}
				// Can this Product be cancelled?
				if (this.denyCancel === true) {
					// eslint-disable-next-line
					alert('Product cannot be cancelled due to related Purchase Order status');
				}
				else {
					this.openCancelModal(row);
				}
				break;

			case 'edit':
				this.handleEditClicked(row);
				break;

			case 'view':
				this.handleViewClicked(row);
				break;
			
			case 'editConfig':
				this.handleViewDaylightReconfigClicked(row);
				if(this.isLightning == false){
					this.openIframe = false;
				}
				else{
					this.openIframe = true;
				}
				break;
			case 'viewConfig':
				this.handleViewDaylightConfigClicked(row);
				this.openIframe = true;
				break;

			case 'viewSplitLines':
				this.handleViewSplitLinesClicked(row);
				this.openIframe = true;
				break;

			default:
		}

	}

	@track openConfigSummary = false;
	handleViewDaylightConfigClicked(row){
		console.log('handleViewDaylightConfigClicked: ', row.id); //the line item
		this.openConfigSummary = true;
	}

	@track openReconfiguration = false;
	handleViewDaylightReconfigClicked(row){
		console.log('handleViewDaylightReconfigClicked: ', row.id);
		if(this.isLightning == false){
			this.navigateToReconfigVF();
		}
		else{
			this.openReconfiguration = true;
		}
	}

	@track openSplitLineSummary = false;
	handleViewSplitLinesClicked(row){
		console.log('handleViewSplitLinesClicked: ', row.id); //the line item
		this.openSplitLineSummary = true;
	}

	closeIframe(){
		this.openIframe = false;
		this.openConfigSummary = false;
		this.openSplitLineSummary = false;
		this.openReconfiguration = false;
		if(this.inRop == true){
			window.location.reload();
		}
		
	}

	handleEditClicked(row) {

		console.log(row.id); //the line item
		console.log(this.recordId); //the order
		console.log('Handle edit clicked: ',JSON.parse(JSON.stringify(row)));
		console.log('Handle edit sfRMSPage: ',sfRMSPage);

		// Navigate to the edit page if the Product
		// is active and has NOT been released.
		// updated to also check for retail purchased orders - mark rothermal 7/18/19
		if (row.productIsActive &&
			(row.purchaseOrderReleasedTimestamp === undefined || row.purchaseOrderReleasedTimestamp === null) &&
			(row.retailPurchaseOrderReleasedTimestamp === undefined || row.retailPurchaseOrderReleasedTimestamp === null)) {

			// Do we open the old edit page or the new edit page?
			if (row.productOpenInVisualforce === true) {
				window.open(sfRMSPage + row.id + '&mode=e', '_top');
			}
			else {
				if(row.configId) {
					window.open(this.editDaylightBaseUrl+'?orderItemId=' + row.id + '&orderId=' + this.recordId + '&configId=' + row.configId, '_top');
				}else {
					window.open(this.editBaseUrl+'?orderItemId=' + row.id + '&orderId=' + this.recordId, '_top');
				}
			}

		}
		else {

			// Do we open the old read-only page or the new read-only page?
			if (row.productOpenInVisualforce === true) {
				window.open(sfRMSPage + row.id, '_top');
			}
			else {
				if(row.configId) {
					window.open(this.readDaylightBaseUrl+'?orderItemId=' + row.id + '&orderId=' + this.recordId + '&configId=' + row.configId, '_top');
				}else {
					window.open(this.readBaseUrl+'?orderItemId=' + row.id + '&orderId=' + this.recordId, '_top');
				}
				
			}

		}

	}

	handleViewClicked(row) {

		// Navigate to the appropriate read-only Product page.
		if (row.productOpenInVisualforce === true) {
			window.open(sfRMSPage + row.id, '_top');
		}
		else {
			if(row.configId) {
				window.open(this.readDaylightBaseUrl+'?orderItemId=' + row.id + '&orderId=' + this.recordId + '&configId=' + row.configId, '_top');
			}else {
				window.open(this.readBaseUrl+'?orderItemId=' + row.id + '&orderId=' + this.recordId, '_top');
			}
			
		}

	}

	handleCancelReasonSelect(event) {

		// Update the local property with the value from child component.
		this.selectedCancelReason = event.target.cancellationReason;

		// We need to disable the Save button until
		// a Cancellation Reason is selected.
		if (this.selectedCancelReason === '') {
			this.disableSave = true;
		}
		else {
			this.disableSave = false;
		}

	}

	openCancelModal(row) {

		console.log('cancel button clicked');
		console.log('stringify row: '  + JSON.parse(JSON.stringify(row)));
		console.log('stringify row.productId: ' + JSON.parse(JSON.stringify(row.productId)));
		console.log('this.recordId: ' + this.recordId);

		this.hasCancelError = false;
		this.cancelErrorMessage = '';
		
		// Find the matching record in the data variable using
		// the row and grab its child data (if applicable).
		this.childData = [];
		for (let index = 0; index<this.data.length; ++index) {
			if (this.data[index].id === row.id &&
				this.data[index]._childrenCount > 0) {
				this.childData = this.data[index]._children;
				break;
			}
		}

		// Grab the Id of the OrderItem record, and
		// the Ids of its children (if applicable).
		productsToCancel = [];
		productsToCancel.push(row.id);
		if (this.childData != null) {
			for (let index = 0; index<this.childData.length; ++index) {
				productsToCancel.push(this.childData[index].id);
			}
		}
		console.log('products to cancel: ' + productsToCancel);
		this.openCancelModel = true;

	} // End Function: openCancelModal()

	closeCancelModal() {
		this.openCancelModel = false;
	}

	saveMethod() {

		// Update the OrderItem(s) to "Cancelled" and refresh the page.

		if (this.selectedCancelReason === '') {
			alert('Cancellation Reason Required');
		}
		else {
			cancelOrderItems({orderItemIdList: productsToCancel, cancellationReason: this.selectedCancelReason})
				.then(result => {
					this.disableSave = true;
					console.log('result:', result);
					if (result === '') {
						this.openCancelModel = false;
						window.open(this.cancelBaseUrl + this.recordId, '_top');
						}
					else {
						this.hasCancelError = true;
						this.cancelErrorMessage = result;
					}
				})
				.catch(error => {
					this.error = error;
				});
		}

	}

	checkForNewConfigOrderItemPricingDetails(){
		checkForNewConfigPricing({OrderId: this.recordId})
        .then((result) => {
            console.log('checkForNewConfigPricing result', result);

        })
        .catch((error) => {
            console.log('getConfigurationData Error', error);
        });
    }

}