import getUrlforConfiguration from '@salesforce/apex/DaylConfigurationCallout.getUrlforConfiguration';
import splitLines from '@salesforce/apex/DaylConfigurationOutputsCallout.splitLines';
import getConfigurationData from '@salesforce/apex/DaylConfigurationOutputsCallout.getConfigurationData';
import getSplitLineMockData from '@salesforce/apex/SplitLineMockData.MockData';
import { NavigationMixin } from 'lightning/navigation';
import { LightningElement, api, wire, track } from 'lwc';

/**
 * @description       : created for daylight callout test/response
 * @author            : mark.rothermal@andersencorp.com
 * @group             : 
 * @last modified on  : 04-29-2021
 * @last modified by  : mark.rothermal@andersencorp.com
 * Modifications Log 
 * Ver   Date         Author                            Modification
 * 1.0   03-23-2021   mark.rothermal@andersencorp.com   Initial Version
**/



export default class DaylTestDisplay extends NavigationMixin(LightningElement) {
    @api daylightResponse;
    @api recordId;
    @api inCommunity;
    @api isLightning;

    @track showLightningButton = true;
    @track configid;
    @api showIframe;
    @track openPricingModal = false;
    @track detailId;
    @track headerId;
    @api openConfigData;
    @track returnedConfigData;
    @track redirectUrl = `https://renewalbyandersen--micahdcdev.lightning.force.com/${this.recordId}` ;
    @track disableConfigViewButton = false;

    connectedCallback(){
        console.log('recordId DaylTestDisplay', this.recordId);
        console.log('daylightResponse DaylTestDisplay', this.daylightResponse);
        console.log('daylightResponse inCommunity', this.inCommunity);
        this.configid = this.recordId
        let random1 = Math.random().toString(36).slice(2);
        let random2 = Math.random().toString(36).slice(2);
        console.log('random1', random1);
        console.log('random2', random2);
        this.redirectUrl = `https://renewalbyandersen--micahdcdev.lightning.force.com/lightning/r/Order/${this.recordId}/view` 
        // if(this.recordId != null && this.recordId != 'undefined'){
        this.detailId = `${random1}`;
        this.headerId = `${random2}`;
            // this.getURL();
        // }
        // else{
        //     this.headerId = 'Simulator';
        //     this.detailId = `Canned_UI_TESTING`;
        // }

        if(this.inCommunity == true){
            this.redirectUrl = `https://micahdcdev--micahdcdev.cs35.force.com/rForceLEX/s/order/${this.recordId}`;
        }

        // this.checkForNewConfigOrderItemPricingDetails();
        
    }
    //cpqNamespace = 'TEST';
    //cpqName = 'SimpleRuleset';
    // headerId = '8AE64EDC770924FA70D93A1C0E2143';
    
    @track updateExistingProduct = false;
    
    @track cpqNamespace = 'POC';
    @track cpqName = 'IED';

    @track seshId; 
    @track screenOptionId;
    @track urlToNavigate;
    @track splitLines;
    @api showSplitLines;
    @track currentPathStep = '1';
    @track stepValue;

    // @wire(getUrlforConfiguration,{namespace:'$cpqNamespace', name:'$cpqName',headerId:'$headerId',detailId:'$detailId', pdateExistingProduct:'$updateExistingProduct', redirectUrl:'$redirectUrl'})
    //     wiredGetUrlforConfiguration({error,data}){
    //         console.log('made it inside wire method');
    //         console.log('error??', error);
    //         console.log('data?!?', data);
        
    //     if (error){
    //         this.error = error;
    //         console.log('error in start config callback ', error.jsonResponse);
    //     } else if (data){
    //         console.log('data.jsonResponse?!?', JSON.parse(JSON.stringify(data.jsonResponse)));
    //         let response = {};
    //         response = JSON.parse(JSON.stringify(data.jsonResponse));
    //         console.log('start config data ', JSON.parse(JSON.stringify(response)));
    //         //console.log('start config data test ', response);
    //         this.daylightResponse = JSON.parse(JSON.stringify(response));
    //         this.getUiStartConfig();
    //     } else {
    //         this.daylightResponse = 'It didnt work out... ';
    //         console.log(' start config wire.. no data or error.');
    //     }
    // }

    stepNavigate(event){
        this.stepValue = event.target.value;
        console.log('navigate to step', stepValue);
        switch(this.stepValue) {
            case '1':
                this.currentPathStep = '1';
                break;
            case '2':
                this.currentPathStep = '2';
                break;
            case '3':
                this.currentPathStep = '3';
                break;
        }
    }

    getURL(){
        getUrlforConfiguration({namespace:this.cpqNamespace, name:this.cpqName,headerId:this.headerId,detailId:this.detailId, pdateExistingProduct:this.updateExistingProduct, redirectUrl:this.redirectUrl, orderId:this.recordId})
        .then((result) => {
            console.log('data.jsonResponse?!?', JSON.parse(JSON.stringify(result.jsonResponse)));
            let response = {};
            response = JSON.parse(JSON.stringify(result.jsonResponse));
            console.log('start config data ', JSON.parse(JSON.stringify(response)));
            this.daylightResponse = JSON.parse(JSON.stringify(response));
            this.showIframe = true;

        })
        .catch((error) => {
            this.error = error;
            console.log('error in start config callback ', error.jsonResponse);
        });
    }
    

    handleCopyToClickboardEvent(event) {
        console.log('handleCopyToClickboardEvent');
        this.getURL();
    }


    getConfigData(){
        this.getReturnData();
    }

    closeModal(){
        this.openConfigData = false;
    }

    closeIframe(){
        this.showIframe = false;
        this.openConfigData = false;
        this.showSplitLines = false;
        this.disableConfigViewButton = false;
        if(this.inCommunity == true){
            window.location.reload();
        }
    }

    viewSummary(){
        this.openConfigData = true;
        this.disableConfigViewButton = true;
        this.getReturnData();
    }

    handleCopyToClickboardEventToClassicVFPage(){
        window.open('/apex/DaylightVFWrapperPage?id=?orderId='+this.recordId , '_target')
    }

    @track ProuctDescription;
    getReturnData(){
        getConfigurationData({namespace:this.cpqNamespace, name:this.cpqName,headerId:this.headerId ,detailId:this.detailId})
        .then((result) => {
            console.log('getConfigurationData result', JSON.stringify(JSON.parse(result.jsonResponse)));
            this.returnedConfigData = JSON.stringify(JSON.parse(result.jsonResponse));
            if(this.returnedConfigData != null && this.returnedConfigData != 'undefined'){
                this.openConfigData = true;
                this.showSplitLines = false;
                this.currentPathStep = '3';
            }
            else{
                this.openConfigData = false;
            }
        })
        .catch((error) => {
            console.log('getConfigurationData Error', error);
        });

    }

    getSplitLines(){
        splitLines({namespace: this.cpqNamespace, name:this.cpqName ,headerId:this.headerId,detailId:this.detailId})
        .then((result) => {
            console.log('getSplitLines result', result.state);
            console.log('getSplitLines ', result);
            if(result.state == 'SUCCESS'){
                alert('We have hit the Endpoint!');
                getSplitLineMockData({})
                .then((result) => {
                    console.log('Split Line Mock Data Response', result);
                    this.openConfigData = false;
                    this.showSplitLines = true;
                    this.splitLines = result;
                    this.currentPathStep = '2';
                })
                .catch((error) => {
                    console.log('error in start config callback ', error);
                    
                });
            }
            // let parsedResponse =  JSON.parse(result.jsonResponse);
            // this.seshId = parsedResponse.SessionID;
            // console.log('seshId', this.seshId);
            // this.screenOptionId = parsedResponse.OptionToFocus;
            // console.log('screenOptionId', this.screenOptionId);
            
            // let tag = document.createElement('textarea');
            // tag.setAttribute('id', 'input_test_id');
            // // tag.value = 'test copy value\nthis is another line content';
            // tag.value = this.seshId;
            // document.getElementsByTagName('body')[0].appendChild(tag);
            // document.getElementById('input_test_id').select();
            // document.execCommand('copy');
            // const text = document.getElementById("input_test_id")
            // console.log('TEXT====', text);
        })
        .catch((error) => {
            console.log('getConfigurationData Error', error);
        });
    }

    
}