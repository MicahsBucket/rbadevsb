import { LightningElement,wire ,track,api} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getRelatedCostItems from '@salesforce/apex/RetailPurchaseOrderItemsController.getRelatedLineItems';
import cancelRelatedOrderItem from  '@salesforce/apex/RetailPurchaseOrderItemsController.cancelRelatedCostItem';
import getStatusCancelCPO from '@salesforce/apex/RetailPurchaseOrderItemsController.getStatus';
import { encodeDefaultFieldValues } from 'lightning/pageReferenceUtils';

const actions = [
    { label: 'Edit', name: 'Edit' },
    { label: 'Cancel', name: 'Cancel' },
];

const columns = [
    { label: 'Product', fieldName: 'Product__c',cellAttributes: { alignment: 'left' } },
    { label: 'Product Description', fieldName: 'Product_Description__c',cellAttributes: { alignment: 'left' } },
    { label: 'Quantity', fieldName: 'Quantity__c',type:'number',cellAttributes: { alignment: 'left' }},
    { label: 'Total Price', fieldName: 'Total_Price__c', type: 'currency',cellAttributes: { alignment: 'left' } },
    { label: 'Unit Price', fieldName: ' Unit_Price__c', type: 'currency' ,cellAttributes: { alignment: 'left' }},
    { label: 'Unit Of Measure', fieldName: 'Unit_of_Measure__c',cellAttributes: { alignment: 'left' } },
    {
        type: 'action',
        typeAttributes: { rowActions: actions },
    },
];
export default class RelatedPurchaseOrderItemList extends NavigationMixin(LightningElement) {

@api recordId;
@track columns=columns;
@track data=[];
@track openmodel = false;
@track visibleonrelease=false;
@track visibleStatus=false;
@track visibleRecordtype=false;
@track opennewModal=false;
rowid;
@track loaded;
@track purchaseorderid;
@track objectApiName='Retail_Purchase_Order_Items__c';
@track openEditModal=false;
@track orderItemrecordId;
  
wiredresult;
    @wire(getRelatedCostItems,{relatedPID:'$recordId'})
    wiredRelatedOrderItems(result) {
       /* eslint-disable no-console */
       this.wiredresult=result;
      console.log('the '+JSON.stringify(result));
      if (result.data) {
        this.data = result.data.map(row=>{
            return{...row, Product__c: row.Product__r.Name}

        
      })
    
    }
    }
    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const row = event.detail.row;
        console.log(JSON.stringify(row));
        switch (actionName) {
            case 'Cancel':
                this.rowid=row.Id;
                this.openmodal();
                break;
            case 'Edit':
                console.log('the row ghj'+row.Id);
                //alert('IN row recordid'+row.Id);
                this.rowid=row.Id;
               // alert('order record if:'+this.rowid);
                this.openEditModal=true;
                //this.showRowDetails(row);
                break;
            default:
        }
    }

    showRowDetails(row)
    {
      
       console.log('the row'+row.id);
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: row.Id,
                objectApiName: 'Retail_Purchase_Order_Items__c',
                actionName: 'edit'
            }
        });
    }

  CancelRow()
  {
    this.closeModal();
    this.loaded=true; 
    
cancelRelatedOrderItem({
    relatedPID: this.rowid
       
    })
    .then(() => {
        this.loaded=false;
        const evt = new ShowToastEvent({
            title: 'Success',
            message: 'The Related Purchase Order Item Has Been Cancelled',
            variant: 'success',
        });
       
        this.dispatchEvent(evt);
         refreshApex(this.wiredresult);
         
        
    })
    .catch((error) => {
        this.message = 'Error received: code' + error.errorCode + ', ' +
            'message ' + error.body.message;
    });
}

openmodal() {
    if(this.visibleonrelease===false)
    {
    this.openmodel = true;
    }
    else{
this.visibleStatus=true;

    }
}
closeModal() {
    this.openmodel = false;
    this.visibleStatus=false;
} 

openNew()
    {
       //this.openEditModal=true; 
       this.opennewModal=true;
    }

   handleSubmit(event){
        event.preventDefault();       // stop the form from submitting
        let fields = event.detail.fields;
        console.log('event details in submit ', JSON.parse(JSON.stringify(event.detail)));
        this.template.querySelector('lightning-record-edit-form').submit(fields);
        this.opennewModal=false;
     }

     handleSuccess(event) {
        const evt = new ShowToastEvent({
            title: 'Success',
            message: 'The Related Purchase Order Item Has Been Created',
            variant: 'success',
        });
       
        this.dispatchEvent(evt);
         refreshApex(this.wiredresult);
         
    }

    handleCancel() {
        //alert('IN CANCEL METHOD');
        //this.openEditModal=false;
      this.opennewModal=false; 
    }
    handleEditCancel() {
        //alert('IN CANCEL METHOD');
        this.openEditModal=false;
      //this.opennewModal=false; 
    }
    @wire(getStatusCancelCPO,{purchaseId:'$recordId'})
wiredStatus(result) {
  
  console.log('the '+JSON.stringify(result));
  if (result.data) {
    
    const rpo= result.data;
    this.purchaseorderid=rpo.Id;
    let recordtypename =rpo.RecordType;
    this.orderid=rpo.Order__c;
  console.log('the '+result.data);
   if(rpo.hasOwnProperty('Confirmed_Timestamp__c') ||rpo.hasOwnProperty('Released_Timestamp__c')) 
   {
this.visibleonrelease=true;
   }
   if( recordtypename.DeveloperName==='Cost_Purchase_Order')
   {
    this.visibleRecordtype=true;
   }

  }

}  

handleEditSubmit(event){
   //alert('handle edit submit');
      event.preventDefault();       // stop the form from submitting
    let fields = event.detail.fields;
    fields['Id'] = this.recordId;
    console.log('event details in submit ', JSON.parse(JSON.stringify(event.detail)));
    this.template.querySelector('lightning-record-edit-form').submit(fields);
    this.openEditModal=false;
 }

 handleEditSuccess(event) {
    //alert('handle edit Success'); 
    const evt = new ShowToastEvent({
        title: 'Success',
        message: 'The Related Purchase Order Item Has Been Updated',
        variant: 'success',
    });
   
    this.dispatchEvent(evt);
     refreshApex(this.wiredresult);
     
}
  
}