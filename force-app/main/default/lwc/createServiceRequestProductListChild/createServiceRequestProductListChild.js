import { LightningElement, api, track, wire } from "lwc";

export default class CreateServiceRequestProductListChild extends LightningElement {
  @api asset;
  @api assets;
  @api serviceProductMap;
  @api mtoDoorOptions;
  @api mtoWindowOptions;
  @track sectionOpen = false;
  @api newAsset;
  @track assetId;
  @track serviceProductId;
  @track productSelectedHere = false;
  @track numberOfProductsSelected = 0;

  get options() {
    return [
      { label: "--None--", value: "NA" },
      { label: "Manufacturing", value: "Manufacturing" },
      { label: "Retailer", value: "Retailer" },
      { label: "Customer", value: "Customer" }
    ];
  }

  openSelection() {
    this.sectionOpen = true;
  }

  closeSelection() {
    this.sectionOpen = false;
  }

  connectedCallback() {
    console.log("child asset", this.asset.isChecked);
    if (this.asset.isChecked == true) {
      this.productSelectedHere = true;
    } else {
      this.productSelectedHere = false;
    }
  }

  steps = [
    { label: "Select Order", value: "1" },
    { label: "Select Service Type", value: "2" },
    { label: "Select Products", value: "3" }
  ];

  get options() {
    return [
      { label: "--None--", value: "NA" },
      { label: "Manufacturing", value: "Manufacturing" },
      { label: "Retailer", value: "Retailer" },
      { label: "Customer", value: "Customer" }
    ];
  }

  get makerequired() {
    return true;
  }

  addCheckedProduct(event) {
    this.numberOfProductsSelected += 1;
    if (this.numberOfProductsSelected > 0) {
      this.productSelectedHere = true;
    } else {
      this.productSelectedHere = false;
      this.numberOfProductsSelected = 0;
    }
  }

  removeCheckedProduct(event) {
    this.numberOfProductsSelected -= 1;
    if (this.numberOfProductsSelected > 0) {
      this.productSelectedHere = true;
    } else {
      this.productSelectedHere = false;
      this.numberOfProductsSelected = 0;
    }
  }

  ///KEEP////
  handleChange(event) {
    this.dispatchEvent(
      new CustomEvent("handlechange", {
        detail: {
          name: event.detail.name,
          responsibility: event.detail.responsibility
        }
      })
    );
  }

  handleMTOChange(event) {
    console.log("Child handleMTOChange", event.detail);
    this.dispatchEvent(
      new CustomEvent("handlemtochange", {
        detail: { name: event.detail.name, mtocode: event.detail.mtocode }
      })
    );
  }

  setSelectedValue(event) {
    if (this.asset.isChecked != false) {
      this.productSelected = true;
    } else {
      this.productSelected = false;
    }
    this.dispatchEvent(
      new CustomEvent("setselectedvalue", { detail: event.detail })
    );
  }

  handleAddNotesClick(event) {
    console.log(event.detail.name);
    this.dispatchEvent(
      new CustomEvent("handleaddnotesclick", { detail: event.detail.name })
    );
  }

  handleAddNotesCloseClick(event) {
    this.showAddNotesModal = false;
  }

  handleNotesChange(event) {
    this.notesValue = event.detail.value;
    this.assets
      .find((ele) => ele.assetId == this.notesAssetId)
      .serviceProducts.find(
        (ele1) => ele1.product.Id == this.notesServiceProductId
      ).notes = this.notesValue;
  }

  applyToAllProducts(event) {
    console.log("applyToAllProducts check CHILD", event.detail.checked);
    console.log("applyToAllProducts name CHILD", event.detail.name);
    this.dispatchEvent(
      new CustomEvent("applytoallproducts", {
        detail: { name: event.detail.name, checked: event.detail.checked }
      })
    );
  }

  setWhatWhere(event){
    this.dispatchEvent(new CustomEvent("setwhatwhere", {detail: { name: event.detail.name, value: event.detail.value }}));
  }

  setChargeCostTo(event) {
    this.dispatchEvent(
      new CustomEvent("setchargecostto", {
        detail: { name: event.detail.name, value: event.detail.value }
      })
    );
  }

  setCategory(event) {
    this.dispatchEvent(
      new CustomEvent("setcategory", {
        detail: { name: event.detail.name, value: event.detail.value }
      })
    );
  }

  setWhatWhere(event) {
    this.dispatchEvent(
      new CustomEvent("setwhatwhere", {
        detail: { name: event.detail.name, value: event.detail.value }
      })
    );
  }

  handleAddNotesClick(event) {
    this.dispatchEvent(
      new CustomEvent("handleaddnotesclick", {
        detail: { name: event.detail.name, value: event.detail.value }
      })
    );
  }

  handleAddNotesCloseClick() {
    this.dispatchEvent(new CustomEvent("handleaddnotescloseclick"));
  }
}