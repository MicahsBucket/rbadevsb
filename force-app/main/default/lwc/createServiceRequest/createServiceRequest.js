import { LightningElement, api, track, wire } from "lwc";
import getAssetsWithProducts1 from "@salesforce/apex/CreateServiceRequestController.getAssetsWithProducts1";
import getOrder from "@salesforce/apex/CreateServiceRequestController.getOrder";
import getServiceTypes from "@salesforce/apex/CreateServiceRequestController.getServiceTypes";
import getGroupDetails from "@salesforce/apex/CreateServiceRequestController.fetchGroupDetails";
//import deleteLineItems from '@salesforce/apex/CreateServiceRequestController.deleteLineItems';
import saveCharges from "@salesforce/apex/CreateServiceRequestController.saveCharges";
import saveOrderRec from "@salesforce/apex/CreateServiceRequestController.saveOrderRecord";
import getContactId from "@salesforce/apex/CreateServiceRequestController.getPrimaryContact";
import fetchMTOSourceCode from "@salesforce/apex/CreateServiceRequestController.getMTOSourceCode";
import saveAllRec from "@salesforce/apex/CreateServiceRequestController.saveAll";
import getRespectiveDefectCode from "@salesforce/apex/CreateServiceRequestController.fetchDefectCodes";
import { NavigationMixin } from 'lightning/navigation';

export default class CreateServiceRequest extends NavigationMixin(LightningElement) {
  @api currentStep = "2"; // The current step for the process. We start at 2 because 1 is done on the account page
  @track assets = []; // !!! We may want to use the Master Product instead of the asset, but I'm not sure it matters
  @api accountId; // The account Id taken from the Account page
  @api storeId; // The store Id taken from the Account page
  @api orderId; // The order Id taken from the popup on the Account page
  @api hideAddressSection = false;
  @api showGroupingSection = false;
  @api showProductListing = false;
  @api serviceProductMap = new Map();
  @api groupMap = new Map();
  @api assetMap = new Map();
  @api assetMapWithProducts = new Map();
  @api groups = [];
  @api isLoaded = false;
  @api isError = false;
  @api backButtonLabel = "Back To Account";
  @api continueButtonLabel = "Continue";
  @api assetId;
  @api isServiceRecordCreated;
  @api myServiceRequestCommunityPage;
  @api myServiceRequestAccountId;
  @api myCommunityPage;
  @api recordPageUrl;
  @api recordId;
  //    @api serviceType
  //    @api serviceIntBy;
  @api installDate;
  @api showServiceFields;

  @track disableContinueButton = true;
  @track serviceTypeList = [];
  @track selectedServiceType = "";
  @track selectedServiceInitiatedBy = "";
  @track selectProducts = true; // The flag to show the product selection part of the page
  @track identifySourceOfDefect = false; // The flag to show the part of the page where the user identifies the source of the defect
  @track order = {}; // The order the user wants to create a service request based on
  @track orderStoreName = ""; // The name of the store associated with the order
  @track createServiceRecord = true;
  @track isOrderDetailsNeeded = true;
  @track isErrorOccured = true;
  @track isErrorOnOrder = false;
  @track showReasonFields = false;
  @track errorMsg;
  @track mtoDoorOptions;
  @track mtoWindowOptions;
  @track orderItemMap = new Map();
  @track showAddNotesModal = false;
  @track notesValue = "";
  notesAssetId = "";
  notesserviceProductId = "";
  steps = [
    { label: "Select Order", value: "1" },
    { label: "Select Service Type", value: "2" },
    { label: "Select Products", value: "3" }
  ];
  get options() {
    return [
      { label: "--None--", value: "NA" },
      { label: "Manufacturing", value: "Manufacturing" },
      { label: "Retailer", value: "Retailer" },
      { label: "Customer", value: "Customer" }
    ];
  }
  get makerequired() {
    return true;
  }

  checkApplyAllVisibility(assetId, serviceProductId) {
    let serviceProduct = this.assets
      .find((ele) => ele.assetId == assetId)
      .serviceProducts.find((ele1) => ele1.product.Id == serviceProductId);
    console.log("@@@@ checkApplyAllVisibility", JSON.stringify(serviceProduct));
    let isRecordChecked = false;
    if (
      serviceProduct.responsibilty != null &&
      serviceProduct.responsibilty != "" &&
      serviceProduct.mtoSourceCode != null &&
      serviceProduct.mtoSourceCode != "" &&
      serviceProduct.selectedDefectCode != "" &&
      serviceProduct.selectedDefectCode != null &&
      serviceProduct.isSelected == true &&
      serviceProduct.selectedSourceOfDefect != "" &&
      serviceProduct.selectedSourceOfDefect != null &&
      (serviceProduct.defectDetails != "" ||
        (serviceProduct.defectDetails == "" &&
          serviceProduct.responsibilty == "Manufacturing"))
    ) {
      this.assets
        .find((ele) => ele.assetId == assetId)
        .serviceProducts.find(
          (ele1) => ele1.product.Id == serviceProductId
        ).showApplyall = true;
      isRecordChecked = true;
    } else {
      this.assets
        .find((ele) => ele.assetId == assetId)
        .serviceProducts.find(
          (ele1) => ele1.product.Id == serviceProductId
        ).showApplyall = false;
      isRecordChecked = false;
    }
    if (isRecordChecked) {
      let localAssets = [];
      for (let i of this.assets) {
        localAssets.push(i);
      }
      this.assets = localAssets;
    }
    console.log(this.assets);
  }
  handleChange(event) {
    this.isLoaded = true;
    // let assetId = event.target.name.split("#")[1];
    // let serviceProductId = event.target.name.split("#")[0];
    console.log("handleChange PARRENT NAME", event.detail.name);
    console.log(
      "handleChange PARRENT responsibility",
      event.detail.responsibility
    );
    let assetId = event.detail.name.split("#")[1];
    let serviceProductId = event.detail.name.split("#")[0];
    if (event.detail.value == "NA") {
      this.serviceProductMap.get(event.detail.name).responsibilty = "";
      this.assets
        .find((ele) => ele.assetId == assetId)
        .serviceProducts.find(
          (ele1) => ele1.product.Id == serviceProductId
        ).responsibilty = "";
    } else {
      this.serviceProductMap.get(event.detail.name).responsibilty =
        event.detail.responsibility;
      this.assets
        .find((ele) => ele.assetId == assetId)
        .serviceProducts.find(
          (ele1) => ele1.product.Id == serviceProductId
        ).responsibilty = event.detail.responsibility;
    }
    let assetWrap = this.assets
      .find((ele) => ele.assetId == assetId)
      .serviceProducts.find((ele1) => ele1.product.Id == serviceProductId);
    this.checkApplyAllVisibility(assetId, serviceProductId);
    if (
      typeof assetWrap != "undefined" &&
      assetWrap.responsibility != "" &&
      assetWrap.mtoSourceCode != ""
    ) {
      getRespectiveDefectCode({
        responsibility: assetWrap.responsibilty,
        mtoSourceCode: assetWrap.mtoSourceCode
      })
        .then((result) => {
          console.log("@@@ results handle");
          console.log(result);
          this.assets
            .find((ele) => ele.assetId == assetId)
            .serviceProducts.find(
              (ele1) => ele1.product.Id == serviceProductId
            ).defectCodes = result;
          this.isLoaded = false;
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }
  handleMTOChange(event) {
    this.isLoaded = true;
    `  `;
    // let assetId = event.target.name.split("#")[1];
    // let serviceProductId = event.target.name.split("#")[0];
    let assetId = event.detail.name.split("#")[1];
    let serviceProductId = event.detail.name.split("#")[0];
    if (event.detail.value == "NA") {
      this.serviceProductMap.get(event.detail.name).mtoSourceCode = "";
      this.assets
        .find((ele) => ele.assetId == assetId)
        .serviceProducts.find(
          (ele1) => ele1.product.Id == serviceProductId
        ).mtoSourceCode = "";
    } else {
      this.serviceProductMap.get(event.detail.name).mtoSourceCode =
        event.detail.mtocode;
      this.assets
        .find((ele) => ele.assetId == assetId)
        .serviceProducts.find(
          (ele1) => ele1.product.Id == serviceProductId
        ).mtoSourceCode = event.detail.mtocode;
    }
    let assetWrap = this.assets
      .find((ele) => ele.assetId == assetId)
      .serviceProducts.find((ele1) => ele1.product.Id == serviceProductId);
    this.checkApplyAllVisibility(assetId, serviceProductId);
    if (
      typeof assetWrap != "undefined" &&
      assetWrap.responsibility != "" &&
      assetWrap.mtoSourceCode != ""
    ) {
      getRespectiveDefectCode({
        responsibility: assetWrap.responsibilty,
        mtoSourceCode: assetWrap.mtoSourceCode
      })
        .then((result) => {
          console.log("@@@ results mto");
          console.log(result);
          this.assets
            .find((ele) => ele.assetId == assetId)
            .serviceProducts.find(
              (ele1) => ele1.product.Id == serviceProductId
            ).defectCodes = result;
          this.isLoaded = false;
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  setSelectedValue(event) {
    console.log("Parent setSelectedValue: ", event.detail);
    let assetId = event.detail.split("#")[1];
    let serviceProductId = event.detail.split("#")[0];
    // this.assetId = event.target.name.split("#")[1];
    // let serviceProductId = event.target.name.split("#")[0];
    let isDataSelected = this.assets
      .find((ele) => ele.assetId == assetId)
      .serviceProducts.find((ele1) => ele1.product.Id == serviceProductId)
      .isSelected;
    if (isDataSelected) {
      this.assets
        .find((ele) => ele.assetId == assetId)
        .serviceProducts.find(
          (ele1) => ele1.product.Id == serviceProductId
        ).isSelected = false;
    } else {
      this.assets
        .find((ele) => ele.assetId == assetId)
        .serviceProducts.find(
          (ele1) => ele1.product.Id == serviceProductId
        ).isSelected = true;
    }
    this.checkApplyAllVisibility(assetId, serviceProductId);
    this.assets.find((ele) => ele.assetId == assetId).isChecked = false;
    for (let k of this.assets.find((ele) => ele.assetId == assetId)
      .serviceProducts) {
      if (k.isSelected) {
        this.assets.find((ele) => ele.assetId == assetId).isChecked =
          // "utility:check";
          true;
        break;
      }
    }
    console.log(this.assets);
    let localAssets = [];
    for (let i of this.assets) {
      localAssets.push(i);
    }
    this.assets = localAssets;
  }
  setSelectedDescription(event) {
    let assetId = event.target.name.split("#")[1];
    let serviceProductId = event.target.name.split("#")[0];
    if (event.detail.value == "NA") {
      this.assets
        .find((ele) => ele.assetId == assetId)
        .serviceProducts.find(
          (ele1) => ele1.product.Id == serviceProductId
        ).description = "";
    } else {
      this.assets
        .find((ele) => ele.assetId == assetId)
        .serviceProducts.find(
          (ele1) => ele1.product.Id == serviceProductId
        ).description = event.detail.value;
    }
  }
  handleAddNotesClick(event) {
    console.log("PARENT handleAddNotesClick", event.detail.name);
    this.notesAssetId = event.detail.name.split("#")[1];
    this.notesServiceProductId = event.detail.name.split("#")[0];
    this.notesValue = this.assets
      .find((ele) => ele.assetId == this.notesAssetId)
      .serviceProducts.find(
        (ele1) => ele1.product.Id == this.notesServiceProductId
      ).description;
    this.showAddNotesModal = true;
  }
  handleAddNotesCloseClick(event) {
    console.log("PARENT handleAddNotesCloseClick");
    this.showAddNotesModal = false;
  }
  handleDescriptionChange(event) {
    console.log("PARENT handleDescriptionChange", event.target.value);
    console.log("PARENT this.notesAssetId", this.notesAssetId);
    this.notesValue = event.target.value;
    this.assets
      .find((ele) => ele.assetId == this.notesAssetId)
      .serviceProducts.find(
        (ele1) => ele1.product.Id == this.notesServiceProductId
      ).description = this.notesValue;
    this.assets
      .find((ele) => ele.assetId == this.notesAssetId)
      .serviceProducts.find(
        (ele1) => ele1.product.Id == this.notesServiceProductId
      ).notes = this.notesValue;
  }
  onServiceTypeChange(event) {
    this.showServiceFields = false;
    this.showReasonFields = false;
    //        this.serviceType = event.detail.value;
    this.selectedServiceType = event.detail.value;
    if (event.detail.value == "Save") {
      this.showReasonFields = true;
    } else if (event.detail.value == "Legacy") {
      this.showServiceFields = true;
    }
    this.enableContinueButton();
  }
  onServiceInitiatedByChange(event) {
    this.selectedServiceInitiatedBy = event.detail.value;
    this.enableContinueButton();
  }

  // renderedCallback() {
  //   if (this.template.querySelector("lightning-tab") != null) {
  //     const list = document.querySelectorAll("div");
  //     for (let checkbox of list) {
  //       console.log(checkbox);
  //     }
  //     // this.template.querySelector('lightning-tab').style.overflow = 'none';
  //   }
  // }
  connectedCallback() {
    if(this.myServiceRequestCommunityPage != null){
      this.orderId = this.myServiceRequestCommunityPage;
    }
    let localResults = [];
    this.isLoaded = true;
    this.createServiceRecord = true;
    this.isErrorOnOrder = false;
    this.backButtonLabel = "Back To Account";
    this.continueButtonLabel = "Continue";
    this.isOrderDetailsNeeded = true;
    this.isErrorOccured = false;
    this.mainTab = "mainTab";
    if (this.asset) {
      console.log("asset.isChecked", this.asset.isChecked);
    }
    if (this.isServiceRecordCreated == "true") {
      this.showServiceFields = true;
    } else {
      this.showServiceFields = false;
    }
    this.getOrderInfo();
    getContactId({ accountId: this.accountId })
      .then((result) => {
        this.template.querySelector('[data-id="billToContactId"]').value = result.Contact__c;
        this.isLoaded = false;
      })
      .catch((error) => {
        //this.assets = error;
        console.log("getContactId Error", error);
      });

    fetchMTOSourceCode({})
      .then((result) => {
        console.log("fetchMTOSourceCode result.Door", result.Door);
        console.log("fetchMTOSourceCode result.Window", result.Window);
        this.mtoDoorOptions = result.Door;
        this.mtoWindowOptions = result.Window;
        this.isLoaded = false;
      })
      .catch((error) => {
        console.log("fetchMTOSourceCode", error);
      });
    getAssetsWithProducts1({ orderId: this.orderId })
      .then((result) => {
        result.forEach((el) => {
          localResults.push(Object.assign({}, el));
        });
        localResults.forEach((el) => {
          this.assets.push(Object.assign({}, el));
        });
        this.assets = localResults;
        let localServiceList = [];
        for (var key in this.assets) {
          this.assetMap.set(this.assets[key].assetId, this.assets[key]);
          let servicesList = this.assets[key].serviceProducts;
          for (var key1 in servicesList) {
            localServiceList.push(Object.assign({}, servicesList[key1]));
          }
        }
        for (let serviceKey in localServiceList) {
          this.serviceProductMap.set(
            localServiceList[serviceKey].rowIdentifier,
            localServiceList[serviceKey]
          );
        }

        this.isLoaded = false;
      })
      .catch((error) => {
        //this.assets = error;
      });

      // if(this.myServiceRequestCommunityPage != null){
      //   this.saveOrder();
      // }
  }

  getOrderInfo() {
    console.log('this.orderId', this.orderId);
    console.log('this.myServiceRequestCommunityPage', this.myServiceRequestCommunityPage);
    getOrder({ orderId: this.orderId })
      .then((result) => {
        if (this.isOrderDetailsNeeded) {
          this.order = Object.assign({}, result);
          this.isOrderDetailsNeeded = false;
        }
        if (
          null != this.order &&
          typeof this.order.Store_Location__c != "undefined"
        ) {
          this.orderStoreName = this.order.Store_Location__r.Name;
        }
        if (this.currentStep === "3") {
          this.hideAddressSection = true;
        }

        this.getServiceTypes();
      })
      .catch((error) => {
        console.log("Error in getOrderInfo callback", error);
        this.error = error;
      });
  }

  // wireGetOrder({ error, data }) {
  //   if (data) {
  //     if (this.isOrderDetailsNeeded) {
  //       this.order = Object.assign({}, data);
  //       this.isOrderDetailsNeeded = false;
  //     }
  //     console.log("@@@order" + JSON.stringify(this.order));
  //     // Set the store name for the order
  //     if (
  //       null != this.order &&
  //       typeof this.order.Store_Location__c != "undefined"
  //     ) {
  //       this.orderStoreName = this.order.Store_Location__r.Name;
  //     }

  /*
            if(this.isServiceRecordCreated == 'true'){
                this.serviceType = 'Legacy';
            }
            else if(this.order.Status === 'Job in Progress'){   
                this.serviceType = 'Job in Progress';
            }
            else{
                this.serviceType = 'Service';
            }
*/

  //     if (this.currentStep === "3") {
  //       this.hideAddressSection = true;
  //     }

  //     this.getServiceTypes();
  //   } else if (error) {
  //     console.log("Error getting order: ", error);
  //   }
  // }

  getServiceTypes() {
    getServiceTypes({ currentOrder: this.order })
      .then((result) => {
        console.log("Service Type List", JSON.parse(JSON.stringify(result)));
        this.serviceTypeList = JSON.parse(JSON.stringify(result));
        console.log("Service Type Count", this.serviceTypeList.length);
        if (this.serviceTypeList.length === 1) {
          console.log("Selected Service Type", this.serviceTypeList[0].value);
          this.selectedServiceType = this.vendorList[0].value;
        } else if (this.serviceTypeList.length > 1) {
          if (this.isServiceRecordCreated == "true") {
            this.selectedServiceType = "Legacy";
          } else {
            this.selectedServiceType = "Service";
          }
        }
        this.enableContinueButton();
      })
      .catch((error) => {
        console.log("Error in getVendors callback", error);
        this.error = error;
      });
  }

  // Shift the progress to the next step
  nextStep(event) {
    console.log("@@@this.currentStep" + this.currentStep);
    this.currentStep = parseInt(this.currentStep) + 1;
    this.currentStep = this.currentStep.toString();
    console.log("@@@this.currentStep" + this.currentStep);
    console.log("@@@this.this.myServiceRequestCommunityPage " + this.myServiceRequestCommunityPage);
    if(this.myServiceRequestCommunityPage != null && this.currentStep != "4"){
        this.currentStep = "3";
        this.accountId = this.myServiceRequestAccountId;
    }
    else if(this.myServiceRequestCommunityPage != null && this.currentStep === "4"){
        console.log("@@@this.this.myServiceRequestCommunityPage SAVE ALL RECORDS" );
        this.currentStep = "4";
        // this.saveAllRecords();
    }
    this.isLoaded = true;
    if (this.currentStep === "2") {
      this.hideAddressSection = false;
      this.showProductListing = false;
      this.showGroupingSection = false;
      this.backButtonLabel = "Back To Account";
      this.continueButtonLabel = "Continue";
      this.isLoaded = false;
    } else if (this.currentStep === "3") {
      //            this.deleteOrderLines();
      this.saveOrder();
    } else if (this.currentStep === "4") {
      console.log("in 4 step");
      this.backButtonLabel = "Back";
      this.continueButtonLabel = "Complete";
      this.isError = false;
      let checkForError = false;
      for (let i in this.assets) {
        console.log("@this.assets[i].isChecked ", this.assets[i].isChecked);
        // debugger;
        if (
          this.assets[i].isChecked === true ||
          "utility:check" == this.assets[i].isChecked
        ) {
          for (let j in this.assets[i].serviceProducts) {
            console.log(
              "this.assets[i].serviceProducts[j].responsibilty",
              this.assets[i].serviceProducts[j].responsibilty
            );
            console.log(
              "this.assets[i].serviceProducts[j].mtoSourceCode",
              this.assets[i].serviceProducts[j].mtoSourceCode
            );
            console.log(
              "this.assets[i].serviceProducts[j].selectedDefectCode",
              this.assets[i].serviceProducts[j].selectedDefectCode
            );
            console.log(
              "this.assets[i].serviceProducts[j].isSelected",
              this.assets[i].serviceProducts[j].isSelected
            );
            if (
              (this.assets[i].serviceProducts[j].responsibilty == null ||
                this.assets[i].serviceProducts[j].responsibilty == "" ||
                this.assets[i].serviceProducts[j].mtoSourceCode == null ||
                this.assets[i].serviceProducts[j].mtoSourceCode == "" ||
                (this.assets[i].serviceProducts[j].responsibilty !=
                  "Manufacturing" &&
                  this.assets[i].serviceProducts[j].defectDetails == "") ||
                this.assets[i].serviceProducts[j].selectedDefectCode == "" ||
                this.assets[i].serviceProducts[j].selectedDefectCode == null) &&
              this.assets[i].serviceProducts[j].isSelected == true
            ) {
              checkForError = true;
              this.currentStep = parseInt(this.currentStep) - 1;
              this.currentStep = this.currentStep.toString();
              break;
            }
          }
          if (checkForError) {
            break;
          }
        }
      }
      if (checkForError) {
        console.log('IN checkForError');
        this.isError = true;
      }
      if (!this.isError) {
        console.log('No Errors');
        this.showProductListing = false;
        this.hideAddressSection = true;
        this.showGroupingSection = true;
        this.saveAllRecords();
      } else {
        this.isLoaded = false;
      }
    } else if (this.currentStep === "5") {
      this.isLoaded = false;
      this.backButtonLabel = "Back";
      this.continueButtonLabel = "Complete";
      this.saveChargeRecords();
    } else {
      this.backButtonLabel = "Back To Account";
      this.continueButtonLabel = "Continue";
      this.currentStep = "2";
      this.hideAddressSection = true;
      this.showProductListing = false;
      this.showGroupingSection = false;
      this.isLoaded = false;
    }
  }

  /*
    deleteOrderLines(event){
        deleteLineItems({orderId : this.orderId }).
        then(result => {
           // this.isLoaded = false;
        })
        .catch(error => {
            console.log(error);
        });
       
    }
*/

  // Shift the progress to the previous step
  previousStep(event) {
    console.log("@@@ this assets");
    console.log(this.assets);
    this.currentStep = parseInt(this.currentStep) - 1;
    this.currentStep = this.currentStep.toString();
    this.isLoaded = true;
    //        this.deleteOrderLines();
    if (this.currentStep === "3") {
      this.backButtonLabel = "Back";
      this.continueButtonLabel = "Continue";
      this.isLoaded = false;
      this.showProductListing = true;
      this.hideAddressSection = true;
      this.showGroupingSection = false;
    } else if (this.currentStep === "1") {
      this[NavigationMixin.Navigate]({
        type: 'standard__recordPage',
        attributes: {
            recordId: this.accountId,
            actionName: 'view',
        },
        })
      // window.open("/" + this.accountId, "_self");
    } else if (this.currentStep === "2") {
      this.backButtonLabel = "Back To Account";
      this.continueButtonLabel = "Continue";
      this.hideAddressSection = false;
      this.showProductListing = false;
      this.showGroupingSection = false;
      this.isLoaded = false;
    } else if (this.currentStep === "4") {
      this.backButtonLabel = "Back";
      this.continueButtonLabel = "Continue";
      this.showProductListing = false;
      this.hideAddressSection = true;
      this.showGroupingSection = true;
      this.fetchGroupData();
    } else if (this.currentStep === "5") {
      this.isLoaded = false;
      this.backButtonLabel = "Back";
      this.continueButtonLabel = "Complete";
      this.showProductListing = false;
      this.hideAddressSection = true;
      this.showGroupingSection = false;
      this[NavigationMixin.Navigate]({
        type: 'standard__recordPage',
        attributes: {
            recordId: this.orderId,
            actionName: 'view',
        },
      })
      // window.open("/" + this.orderId, "_self");
    } else {
      this.currentStep = "2";
      this.backButtonLabel = "Back";
      this.continueButtonLabel = "Continue";
      this.hideAddressSection = true;
      this.showProductListing = false;
      this.isLoaded = false;
    }
  }
  saveOrder() {
    var order = {};
    this.isErrorOnOrder = false;
    /*
        console.log(this.template.querySelector('[data-id="serviceType"]'.value));
        if(this.template.querySelector('[data-id="serviceType"]').value != null){
            order.Service_Type__c = this.template.querySelector('[data-id="serviceType"]').value;
        }
*/
    console.log(this.selectedServiceType);
    if (this.selectedServiceType != "NA") {
      order.Service_Type__c = this.selectedServiceType;
    }

    if (
      this.template.querySelector('[data-id="billToContactId"]').value != null
    ) {
      order.BillToContactId = this.template.querySelector(
        '[data-id="billToContactId"]'
      ).value;
    }
    /*
        if(this.template.querySelector('[data-id="serviceInitiatedBy"]').value != null){
            order.Service_initiated_By__c = this.template.querySelector('[data-id="serviceInitiatedBy"]').value;      
        }
*/
    console.log(this.selectedServiceInitiatedBy);
    if (this.selectedServiceInitiatedBy != "") {
      order.Service_initiated_By__c = this.selectedServiceInitiatedBy;
    }

    if (this.template.querySelector('[data-id="description"]').value != null) {
      order.Description = this.template.querySelector(
        '[data-id="description"]'
      ).value;
    }

    console.log(this.template.querySelector('[data-id="installDate"]'));
    if (
      typeof this.template.querySelector('[data-id="installDate"]') !=
        "undefined" &&
      this.template.querySelector('[data-id="installDate"]') != null &&
      this.template.querySelector('[data-id="installDate"]').value != null
    ) {
      order.Install_Date__c = this.template.querySelector(
        '[data-id="installDate"]'
      ).value;
    }
    if (
      typeof this.template.querySelector('[data-id="primarySaveReason"]') !=
        "undefined" &&
      this.template.querySelector('[data-id="primarySaveReason"]') != null &&
      this.template.querySelector('[data-id="primarySaveReason"]').value != null
    ) {
      order.Save_Reason__c = this.template.querySelector(
        '[data-id="primarySaveReason"]'
      ).value;
    }
    if (
      typeof this.template.querySelector('[data-id="secondarySaveReason"]') !=
        "undefined" &&
      this.template.querySelector('[data-id="secondarySaveReason"]') != null &&
      this.template.querySelector('[data-id="secondarySaveReason"]').value !=
        null
    ) {
      order.Secondary_Save_Reason__c = this.template.querySelector(
        '[data-id="secondarySaveReason"]'
      ).value;
    }
    if (order.Service_Type__c != "Save") {
      order.Secondary_Save_Reason__c = null;
      order.Save_Reason__c = null;
    }

    console.error(this.createServiceRecord);
    if (
      this.isServiceRecordCreated === "true" ||
      this.createServiceRecord == false
    ) {
      order.Id = this.orderId;
    } else {
      order.Sold_Order__c = this.orderId;
    }

    if (
      order.Service_Type__c == "Service" &&
      (order.BillToContactId == null || order.BillToContactId == "")
    ) {
      this.isErrorOnOrder = true;
      this.errorMsg = "Please populate Bill To Contact.";
    } else if (
      order.Service_Type__c == "Legacy" &&
      (order.Install_Date__c == null || order.Install_Date__c == "")
    ) {
      this.isErrorOnOrder = true;
      this.errorMsg = "Please populate Install Date.";
    } else if (
      order.Service_Type__c == "Save" &&
      (order.Save_Reason__c == null ||
        order.Save_Reason__c == "" ||
        order.Secondary_Save_Reason__c == null ||
        order.Secondary_Save_Reason__c == "")
    ) {
      this.isErrorOnOrder = true;
      this.errorMsg = "Please populate Primary and Secondary Save Reason.";
    }
    if (this.isErrorOnOrder) {
      this.currentStep = parseInt(this.currentStep) - 1;
      this.currentStep = this.currentStep.toString();
      this.isLoaded = false;
    } else {
      console.log('order', order);
      console.log('accountId', this.accountId);
      saveOrderRec({
        currentOrder: JSON.stringify(order),
        accountId: this.accountId
      })
        .then((result) => {
          console.log('result of new Order ', JSON.stringify(result));
          this.orderId = result;
          this.createServiceRecord = false;
          this.backButtonLabel = "Back";
          this.continueButtonLabel = "Continue";
          this.isLoaded = false;
          this.showProductListing = true;
          this.hideAddressSection = true;
          this.showGroupingSection = false;
          this.groups = [];
          this.installDate = order.Install_Date__c;
          //                    this.serviceIntBy = order.Service_initiated_By__c;
          this.selectedServiceInitiatedBy = order.Service_initiated_By__c;
          console.log("@@@@ installDate" + this.installDate);
          console.log(
            "@@@@ selectedServiceInitiatedBy" + this.selectedServiceInitiatedBy
          );
        })
        .catch((error) => {
          console.log("@@@@ error" + error);
        });
    }
  }

  fetchGroupData() {
    var orderItems = [];
    var listOfRecords = [];
    this.groups = [];
    for (let i of this.serviceProductMap.keys()) {
      console.log(i);
      console.log(this.serviceProductMap.get(i));
      if (this.serviceProductMap.get(i).isSelected) {
        this.serviceProductMap.get(i).sobjectType = "ServiceProductWrapper";
        listOfRecords.push(this.serviceProductMap.get(i));
      }
    }
    getGroupDetails({
      orderId: this.orderId,
      rowIdentifier: JSON.stringify(listOfRecords)
    })
      .then((result) => {
        result.forEach((el) => {
          this.groups.push(Object.assign({}, el));
        });
        console.log(result);
        let orderItems = [];
        this.groupMap = new Map();
        let chargeItems = [];
        for (var key in this.groups) {
          let orderItems = this.groups[key].accs;
          console.log("@@@@ orderItems" + JSON.stringify(orderItems));
          for (var key1 in orderItems) {
            this.orderItemMap.set(
              orderItems[key1].orderItem.Id,
              orderItems[key1].orderItem
            );
            if (typeof orderItems[key1].orderItem.Charges__r != "undefined") {
              chargeItems.push(...orderItems[key1].orderItem.Charges__r);
            }
          }
        }
        console.log("@@@@@orderItemMap");
        console.log(chargeItems);
        for (var chargeId in chargeItems) {
          this.groupMap.set(chargeItems[chargeId].Id, chargeItems[chargeId]);
        }
        this.isLoaded = false;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  applyToAllProducts(event) {
    this.isLoaded = true;
    let userSelectedId = event.detail.name.split("#")[1];
    let userSelectedServiceProductId = event.detail.name.split("#")[0];
    let selectedAsset = this.assets.find(
      (ele) => ele.assetId == userSelectedId
    );
    let selectedServiceProds = selectedAsset.serviceProducts;
    let isError = false;
    let serviceProdMap = new Map();
    console.log(event.detail);
    let isSelected = event.detail.checked;
    for (let j of selectedServiceProds) {
      let serviceProductId = j.rowIdentifier.split("#")[0];
      if (userSelectedServiceProductId === serviceProductId) {
        serviceProdMap.set(serviceProductId, j);
      }
    }

    console.log("@@@service erro" + isSelected);
    console.log(serviceProdMap);
    if (isError) {
      this.isError = true;
    } else {
      console.log(serviceProdMap);
      for (let i of this.assets) {
        let chargeItems = [];
        if (
          i.assetName == selectedAsset.assetName &&
          i.assetId != selectedAsset.assetId
        ) {
          for (let k of i.serviceProducts) {
            let key = k.rowIdentifier.split("#")[0];
            if (key === userSelectedServiceProductId) {
              if (isSelected == false) {
                k.responsibilty = "";
                k.description = "";
                k.mtoSourceCode = "";
                k.selectedDefectCode = "";
                k.selectedSourceOfDefect = "";
                k.defectDetails = "";
                k.isSelected = false;
                k.defectCode = "";
              } else if (serviceProdMap.has(key)) {
                k.responsibilty = serviceProdMap.get(key).responsibilty;
                k.description = serviceProdMap.get(key).description;
                k.mtoSourceCode = serviceProdMap.get(key).mtoSourceCode;
                k.selectedDefectCode = serviceProdMap.get(
                  key
                ).selectedDefectCode;
                k.selectedSourceOfDefect = serviceProdMap.get(
                  key
                ).selectedSourceOfDefect;
                k.defectDetails = serviceProdMap.get(key).defectDetails;
                k.defectCodes = serviceProdMap.get(key).defectCodes;
                k.isSelected = serviceProdMap.get(key).isSelected;
                k.defectCode = serviceProdMap.get(key).defectCode;

                // chargeItems.push(k);
              }
            }
          }
          if (isSelected) {
            this.assets.find((ele) => ele.assetId == i.assetId).isChecked =
              // "utility:check";
              true;
          } else {
            this.assets.find((ele) => ele.assetId == i.assetId).isChecked = "";
            for (let k of this.assets.find((ele) => ele.assetId == i.assetId)
              .serviceProducts) {
              if (k.isSelected) {
                this.assets.find((ele) => ele.assetId == i.assetId).isChecked =
                  // "utility:check";
                  true;
                break;
              }
            }
          }
        }
      }
    }
    console.log("@@@ 743");
    console.log("UPDATED ASSETS", this.assets);
    this.isLoaded = false;
    this.assets = this.assets;
  }

  saveAllRecords() {
    this.isLoaded = true;
    var listOfRecords = [];
    for (let i of this.assets) {
      // if (i.isChecked === "utility:check") {
      if (i.isChecked === true) {
        i.sobjectType = "AssetProductWrapper";
        i.isChecked = true;
        listOfRecords.push(i);
      }
    }
    saveAllRec({
      orderId: this.orderId,
      rowIdentifier: JSON.stringify(listOfRecords)
    })
      .then((result) => {
        ///MRJ 4.22.2021 - check to see if Community for navigation///
        if(this.myServiceRequestCommunityPage != null){
          this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
              recordId: this.orderId,
              objectApiName: 'Order',
              actionName: 'view'
            },
          })
        }else{
            this[NavigationMixin.Navigate]({
              type: 'standard__recordPage',
              attributes: {
                  recordId: this.orderId,
                  actionName: 'view',
              },
            })
        }
        this.isLoaded = false;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  setChargeCostTo(event) {
    let assetId = event.detail.name.split("#")[1];
    let serviceProductId = event.detail.name.split("#")[0];
    if (event.detail.value == "NA") {
      this.assets
        .find((ele) => ele.assetId == assetId)
        .serviceProducts.find(
          (ele1) => ele1.product.Id == serviceProductId
        ).selectedDefectCode = "";
    } else {
      this.assets
        .find((ele) => ele.assetId == assetId)
        .serviceProducts.find(
          (ele1) => ele1.product.Id == serviceProductId
        ).selectedDefectCode = event.detail.value;
    }
    this.checkApplyAllVisibility(assetId, serviceProductId);
    console.log(this.assets);
  }

  setCategory(event) {
    console.log('Name' + event.detail.name);
    console.log('Value' + event.detail.value);
    if (event.detail.name != null) {
      let assetId = event.detail.name.split("#")[1];
      let serviceProductId = event.detail.name.split("#")[0];
      if (event.detail.value == "NA") {
        this.assets
          .find((ele) => ele.assetId == assetId)
          .serviceProducts.find(
            (ele1) => ele1.product.Id == serviceProductId
          ).selectedSourceOfDefect = "";
      } else {
        this.assets
          .find((ele) => ele.assetId == assetId)
          .serviceProducts.find(
            (ele1) => ele1.product.Id == serviceProductId
          ).selectedSourceOfDefect = event.detail.value;
      }
      this.checkApplyAllVisibility(assetId, serviceProductId);
      console.log("@@@ event.detail.value", event.detail.value);
      console.log(this.assets);
    }
  }

  setWhatWhere(event) {
    console.log("PARENT setWhatWhere", JSON.stringify(event.detail));
    if (event.detail.name != null) {
      let assetId = event.detail.name.split("#")[1];
      let serviceProductId = event.detail.name.split("#")[0];
      if (event.detail.value == "NA") {
        this.assets
          .find((ele) => ele.assetId == assetId)
          .serviceProducts.find(
            (ele1) => ele1.product.Id == serviceProductId
          ).defectDetails = "";
      } else {
        this.serviceProductMap.get(event.detail.name).selectedDefectCode =
          event.detail.value;
        this.assets
          .find((ele) => ele.assetId == assetId)
          .serviceProducts.find(
            (ele1) => ele1.product.Id == serviceProductId
          ).defectDetails = event.detail.value;
      }
      this.checkApplyAllVisibility(assetId, serviceProductId);
    }
  }

  saveChargeRecords() {
    var listOfRecords = [];
    this.isErrorOccured = false;
    for (let i of this.groupMap.keys()) {
      console.log(i);
      console.log(this.groupMap.get(i).Category__c);
      listOfRecords.push(this.groupMap.get(i));
      if (
        this.groupMap.get(i).Charge_Cost_To__c == null ||
        this.groupMap.get(i).Charge_Cost_To__c == "" ||
        this.groupMap.get(i).Category__c == null ||
        this.groupMap.get(i).Category__c == "" ||
        (this.groupMap.get(i).Charge_Cost_To__c != "Manufacturing" &&
          this.groupMap.get(i).What_Where__c == "")
      ) {
        this.isErrorOccured = true;
        this.currentStep = "4";
        break;
      }
    }
    var listOfOrderItemRecords = [];
    for (let i of this.orderItemMap.keys()) {
      let orderItem = {
        Id: this.orderItemMap.get(i).Id,
        Defect_Code__c: this.orderItemMap.get(i).Defect_Code__c
      };
      listOfOrderItemRecords.push(orderItem);
    }
    if (!this.isErrorOccured) {
      this.showProductListing = false;
      this.hideAddressSection = true;
      this.showGroupingSection = false;
      this[NavigationMixin.Navigate]({
        type: 'standard__recordPage',
        attributes: {
            recordId: this.orderId,
            actionName: 'view',
        },
      })
      // window.open("/" + this.orderId, "_self");

      saveCharges({
        chargeJSON: JSON.stringify(listOfRecords),
        orderItemToUpdate: listOfOrderItemRecords
      })
        .then((result) => {
          this.isLoaded = false;
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  formatDate(date) {
    let newDate;
    // Split the date at '-'
    if (date != null && date != "") {
      let pieces = date.split("-");
      // Put together the pieces of the date in the correct order
      let newDate =
        parseInt(pieces[1], 10) +
        "/" +
        parseInt(pieces[2], 10) +
        "/" +
        pieces[0];
    }
    // return the new date
    return newDate;
  }

  focusTab(event) {
    this.template.querySelector('[id^="focusedTab"]').focus();
  }

  enableContinueButton() {
    console.log("Current Step", this.currentStep);
    console.log("Service Initiated By", this.selectedServiceInitiatedBy);
    console.log("Service Type", this.selectedServiceType);
    if (
      this.currentStep === "2" &&
      (this.selectedServiceInitiatedBy === "" ||
        this.selectedServiceType === "NA")
    ) {
      this.disableContinueButton = true;
    } else {
      this.disableContinueButton = false;
    }
  }
}