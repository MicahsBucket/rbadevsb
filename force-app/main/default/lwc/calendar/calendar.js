import { LightningElement, api, track} from 'lwc';
import momentJS from '@salesforce/resourceUrl/moment';
import { loadScript } from 'lightning/platformResourceLoader';
import {PERIOD_MAP} from 'c/calendarViews';

export class Calendar extends LightningElement{
    currentMoment;
    @track startDay;
    @api period;
    @api view;
    @api showMonthView;
    @api showYearView;
    @api showDayView;
    @track results;
    @track error; //Standard error handler for errorPanel component
    @track title;

    constructor(){
        super();
        this.view = 'month';
    }

    renderedCallback(){
        if(this.currentMoment){
            return;
        }

        Promise.all([
            loadScript(this, momentJS + '/moment.js')
        ]).then(() => {
            // eslint-disable-next-line no-undef
            this.currentMoment = moment();
            this.initializePeriods();
        })
        .catch(error => {
            this.error = error;
        });
    }

    initializePeriods(){
        let periodObject = PERIOD_MAP[this.view];
        let titleDay = this.currentMoment;
        this.startDay = this.currentMoment.clone().startOf(periodObject.name);
        this.title = this.startDay.format(periodObject.headerLabelFormat);
        this.results = this.getPeriods(periodObject, true, titleDay);
    }

    getPeriods(periodObject, showPeriodChange, currentMoment){
        if(!periodObject){
            return [];
        }

        let periodGroups = [];
        let currentPeriod = currentMoment.clone().startOf(periodObject.name).startOf(periodObject.group);
        let lastPeriod = currentMoment.clone().endOf(periodObject.name).endOf(periodObject.group);
        this.title = this.startDay.format(periodObject.headerLabelFormat);

        while(currentPeriod <= lastPeriod){
            periodGroups.push({key: currentPeriod.format('YYYYDDMMHHMM'), periods:[]});
            let lastPeriodInGroup = currentPeriod.clone().endOf(periodObject.group);
            while(currentPeriod <= lastPeriodInGroup){
                let label = currentPeriod.format(periodObject.periodLabelFormat);
                let isActive = !currentPeriod.isSame(currentMoment, periodObject.group);

                if(showPeriodChange && periodObject.startOfCurrentGroupLabelFormat && currentPeriod.isSame(currentPeriod.clone().startOf(periodObject.name),periodObject.periodSingular)){
                    label= currentPeriod.format(periodObject.startOfCurrentGroupLabelFormat);
                }

                let labelCSS = periodObject.labelCSS;
                let containerCSS = periodObject.containerCSS;
                if(this.view === 'day' && currentPeriod.minute() === 0){
                    labelCSS += ' hourLabel';
                    containerCSS += ' hourContainer';
                }

                periodGroups[(periodGroups.length-1)].periods.push({
                    label : label
                    ,moment : currentPeriod
                    ,day : currentPeriod.dayOfYear()
                    ,date : currentPeriod.date()
                    ,year : currentPeriod.year()
                    ,month : currentPeriod.month()
                    ,hour : currentPeriod.hour()
                    ,key: currentPeriod.minute()
                    ,isActive : isActive
                    ,subPeriodGroups : this.getPeriods(PERIOD_MAP[periodObject.subPeriodGroup],false,currentMoment.clone())
                    ,events : []
                    ,labelCSS : labelCSS
                    ,containerCSS : containerCSS
                });

                ////
                currentPeriod = currentPeriod.clone().add(periodObject.periodIncrement,periodObject.periodPlural);
            }
        }
        return periodGroups;
    }

    get isYearView(){
        return this.isCurrentView('year');
    }

    get isMonthView(){
        return this.isCurrentView('month');
    }

    get isDayView(){
        return this.isCurrentView('day');
    }

    get isWeekView(){
        return this.isCurrentView('week');
    }

    handleChangePeriod(event){
        this.currentMoment.add(event.target.dataset.increment,PERIOD_MAP[this.view].pluralName);

        this.initializePeriods();
    }

    handleChangeView(event){
        let requestedView = event.target.dataset.view;
        if(this.view !== requestedView){
            this.view = requestedView;
            this.initializePeriods();
        }
    }

    isCurrentView(viewToCheck){
        return this.view === viewToCheck ? 'brand' : '';
    }
}