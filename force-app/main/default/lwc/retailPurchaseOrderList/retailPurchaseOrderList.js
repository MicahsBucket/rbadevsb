import { LightningElement,wire ,track,api} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getRelatedOrderProducts from '@salesforce/apex/RetailPurchaseOrderItemsController.getRelatedOrderProducts';
import cancelRelatedOrderItem from  '@salesforce/apex/RetailPurchaseOrderItemsController.cancelRelatedOrderItem';
import getStatusCancel from '@salesforce/apex/RetailPurchaseOrderItemsController.getStatus';


const actions = [
    { label: 'View', name: 'View' },
    { label: 'Cancel', name: 'Cancel' }
    
];

const columns = [
    { label: 'Product', fieldName: 'Product2' , type:'text', cellAttributes: { alignment: 'left' }},
    { label: 'Quantity', fieldName: 'Quantity',type:'number',cellAttributes: { alignment: 'left' }},
    { label: 'Status', fieldName: 'Status__c',type:'text',cellAttributes: { alignment: 'left' }},
    {label: 'Unit Id', fieldName: 'Unit_Id__c',type:'text',cellAttributes: { alignment: 'left' }},
     
    
    {
        type: 'action',
        typeAttributes: { rowActions: actions },
    },
];
const sfAROReadBaseUrl = '/ROP/s/Read-Only-Order-Product';
export default class RetailPurchaseOrderList extends NavigationMixin(LightningElement) {


    @api recordId;
    @track columns=columns;
    @track data=[];
    @track openmodel = false;
    @track visibleonrelease=false;
    @track visibleStatus=false;
    @track visibleRecordtype=false;
    @track loaded;
   readurl=sfAROReadBaseUrl;
    rowid;
    orderid;
    toastMessage;
    toastType;
      
    wiredresult;
        @wire(getRelatedOrderProducts,{relatedPID:'$recordId'})
        wiredRelatedOrderItems(result) {
           /* eslint-disable no-console */
           this.wiredresult=result;
          console.log('the '+JSON.stringify(result));
          if (result.data) {
             
              this.data = result.data.map(row=>{
                return{...row, Product2: row.Product2.Name}
            })
    
            
          }
        
        }
    
        handleRowAction(event) {
            const actionName = event.detail.action.name;
            const row = event.detail.row;
            console.log(JSON.stringify(row));
            switch (actionName) {
                case 'Cancel':
                    this.rowid=row.Id;
                    this.openmodal();
                    break;
                case 'Edit':
                    console.log('the row ghj'+row.Id);
                    this.showRowDetails(row);
                    break;
                case 'View':
                    this.rowid=row.Id;
                    this.viewReadOnly();

                default:
            }
        }
    
        showRowDetails(row)
        {
          
           console.log('the row'+row.id);
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: row.Id,
                    objectApiName: 'Retail_Purchase_Order_Items__c',
                    actionName: 'edit'
                }
            });
        }
    
    
      CancelRow()
      {
        console.log('the url is'+this.sfAROReadBaseUrl);
        this.closeModal();
       this.loaded=true; 
       console.log('ebtered loaded'+this.loaded);
    cancelRelatedOrderItem({
        relatedPID: this.rowid
           
        })
        .then(() => {
            this.loaded=false;
            const evt = new ShowToastEvent({
                title: 'Success',
                message: 'The Order Product Has Been Cancelled',
                variant: 'success',
            });
           
            this.dispatchEvent(evt);
            
             refreshApex(this.wiredresult);
             
            
        })
        .catch((error) => {
            this.message = 'Error received: code' + error.errorCode + ', ' +
                'message ' + error.body.message;
        });
    }
    
    
    
    openmodal() {
        if(this.visibleonrelease===false)
        {
        this.openmodel = true;
        }
        else{
this.visibleStatus=true;

        }
    }
    closeModal() {
        this.openmodel = false;
        this.visibleStatus=false;
    } 

    openNew()
    {
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
        
                objectApiName: 'OrderItem',
                actionName: 'new'
            }
        });   
    }
      

    @wire(getStatusCancel,{purchaseId:'$recordId'})
wiredStatus(result) {
  
  console.log('the '+JSON.stringify(result));
  if (result.data) {
    
    const rpo= result.data;
    let recordtypename =rpo.RecordType;
    this.orderid=rpo.Order__c;
  console.log('the '+result.data);
   if(rpo.hasOwnProperty('Confirmed_Timestamp__c') ||rpo.hasOwnProperty('Released_Timestamp__c')) 
   {
this.visibleonrelease=true;
   }
   if( recordtypename.DeveloperName==='RbA_Purchase_Order')
   {
    this.visibleRecordtype=true;
   }

  }

}

viewReadOnly()
{ 
    console.log('the url is'+this.sfAROReadBaseUrl);

    window.open(sfAROReadBaseUrl+'?orderItemId=' + this.rowid + '&orderId=' + this.orderid, '_top');
}
}