import { LightningElement, track,  wire, api  } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getServiceRequests from '@salesforce/apex/HomeCompLightningCardServiceOrdering.getServiceRequests';
import getPurchaseOrders from '@salesforce/apex/HomeCompLightningCardServiceOrdering.getPurchaseOrders';

    const ServiceReqColumns = [
        {label: 'Service Request Number', fieldName: 'ServiceReqNumberURL', type: "url", typeAttributes: {label: { fieldName: 'ServiceReqNumber'}, value:{fieldName: 'ServiceReqNumberURL'}, target: '_blank'}, sortable: true}, 
        {label: 'Bill To Contact', fieldName: 'CustomernameURL', type: "url", typeAttributes: {label: { fieldName: 'Customername'}, value:{fieldName: 'CustomernameURL'}, target: '_blank'}, sortable: true},
        {label: 'Account', fieldName: 'AccountnameURL', type: "url", typeAttributes: {label: { fieldName: 'Accountname'}, value:{fieldName: 'AccountnameURL'}, target: '_blank'}, sortable: true},
        {label: 'Amount Due', fieldName: 'AmountDue', type: 'currency',  sortable: true},
        {label: 'Estimated Ship Date', fieldName: 'EstShipDate', type: 'date', sortable: true} 
        ];

    const PurchaseOrderColumns = [
        {label: 'Purchase Order Number', fieldName: 'PurchaseOrderNumberURL', type: "url", typeAttributes: {label: { fieldName: 'PurchaseOrderNumber'}, value:{fieldName: 'PurchaseOrderNumberURL'}, target: '_blank'},  sortable: true}, 
        {label: 'Reference', fieldName: 'Reference', type: 'text', sortable: true},
        {label: 'Comments', fieldName: 'Comment', type: 'text', sortable: true},
        {label: 'Rejection Date', fieldName: 'RejectedDate', type: 'date',  sortable: true, typeAttributes: { day: 'numeric',
                                                                                                              month: 'short',
                                                                                                              year: 'numeric',
                                                                                                              hour: '2-digit',
                                                                                                              minute: '2-digit',
                                                                                                              second: '2-digit',
                                                                                                              hour12: true } }
        ]; 

export default class HomeComponentLightningCardServiceOrdering extends  NavigationMixin (LightningElement) {

    @api recordId;
    @track ServiceReqColumns=ServiceReqColumns;
    @track listServiceRequest;
    @track listServiceRequestDup;

    @track PurchaseOrderColumns=PurchaseOrderColumns;
    @track listPurchaseOrder;
    @track listPurchaseOrderDup;

    @wire(getServiceRequests, {searchKey: ''})
        wiredServiceRequests({ error, data }) {             
            if (data) {
                this.listServiceRequest = data;
                this.listServiceRequestDup = data;
            }
            else if (error) {
                this.error = error;
            }
            
            console.log('getServiceRequests ' +JSON.stringify( this.listServiceRequest));

        }

    @wire(getPurchaseOrders, {searchKey: ''})
    wiredPurchaseOrders({ error, data }) {             
        if (data) {           
            this.listPurchaseOrder =  data;
            this.listPurchaseOrderDup =  data;               
        } else if (error) {
            this.error = error;
           }     
           console.log('listPurchaseOrder ' +JSON.stringify(data));
           console.log('listPurchaseOrder 2 ' +JSON.stringify( this.listPurchaseOrder));       
    }

    updateSeachKeySerComp(event) {
        this.sVal = event.target.value;
        this.handleSearchSerComp();
    }

    handleSearchSerComp() {
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getServiceRequest({
                    searchKey: this.sVal
                })
                .then(result => {
                    // set @track contacts variable with return contact list from server  
                    this.listServiceRequest = result;
                })
                .catch(error => {
                    // display server exception in toast msg 
                    const event = new ShowToastEvent({
                        title: 'Error',
                        variant: 'error',
                        message: error.body.message,
                    });
                    this.dispatchEvent(event);
                    // reset contacts var with null   
                    this.listServiceRequest = null;
                });
        }
        else {   
            this.listServiceRequest = this.listServiceRequestDup;
        }
    }

    updateSeachKeyPOComp(event) {
        this.sVal = event.target.value;
        this.handleSearchPOComp();
    }

    handleSearchPOComp() {
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getPurchaseOrderRec({
                    searchKey: this.sVal
                })
                .then(result => {
                    // set @track contacts variable with return contact list from server  
                    this.listPurchaseOrder = result;
                })
                .catch(error => {
                    // display server exception in toast msg 
                    const event = new ShowToastEvent({
                        title: 'Error',
                        variant: 'error',
                        message: error.body.message,
                    });
                    this.dispatchEvent(event);
                    // reset contacts var with null   
                    this.listPurchaseOrder = null;
                });
        }
        else {
            this.listPurchaseOrder = this.listPurchaseOrderDup;
        }

    }

    serviceSortData(fieldName, sortDirection) {
        var data = JSON.parse(JSON.stringify(this.listServiceRequest));
        
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        if (fieldName == 'AmountDue') {
            data.sort((a,b) => {
                let valueA = key(a) ? key(a) : ''; 
                let valueB = key(b) ? key(b) : '';
                return reverse * ((valueA > valueB) - (valueB > valueA));
            });
        }
        else {
                data.sort((a,b) => {
                    let valueA = key(a) ? key(a).toLowerCase() : ''; 
                    let valueB = key(b) ? key(b).toLowerCase() : '';
                    return reverse * ((valueA > valueB) - (valueB > valueA));
                });
        }

        this.listServiceRequest = data;

    }

    serviceUpdateColumnSorting(event) {
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.serviceSortData(this.sortedBy,this.sortedDirection);
    }

    purchaseOrderSortData(fieldName, sortDirection) {

        var data = JSON.parse(JSON.stringify(this.listPurchaseOrder));

        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        
        data.sort((a,b) => {
            let valueA = key(a) ? key(a).toLowerCase() : ''; 
            let valueB = key(b) ? key(b).toLowerCase() : '';
            return reverse * ((valueA > valueB) - (valueB > valueA));
        });

        this.listPurchaseOrder = data;

    }

    purchaseOrderUpdateColumnSorting(event) {
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.purchaseOrderSortData(this.sortedBy,this.sortedDirection);
    }
    
}