const PERIOD_MAP = {
    "month" : {
        "name":"month",
        "pluralName" : "months",
        "periodSingular":"day",
        "periodPlural":"days",
        "headerLabelFormat":"MMMM YYYY",
        "group" : "week",
        "periodLabelFormat" : "D",
        "startOfCurrentGroupLabelFormat" : "MMM D",
        "periodIncrement" : 1
    },
    "year" : {
        "name":"year",
        "pluralName" : "years",
        "periodSingular":"month",
        "periodPlural":"months",
        "headerLabelFormat":"YYYY",
        "group" : "quarter",
        "periodLabelFormat" : "MMM",
        "startOfCurrentGroupLabelFormat" : "MMM YYYY",
        "subPeriodGroup" : "month",
        "periodIncrement" : 1
    },
    "day" : {
        "name":"day",
        "pluralName" : "days",
        "periodSingular":"minute",
        "periodPlural":"minutes",
        "headerLabelFormat":"dddd, MMMM Do, YYYY",
        "group" : "hour",
        "periodLabelFormat" : "h:mm a",
        "periodIncrement" : 15,
        "containerCSS" : "periodContainer",
        "labelCSS" : "periodLabel",
    },
    "week" : {
        "name":"week",
        "pluralName" : "weeks",
        "periodSingular":"day",
        "periodPlural":"days",
        "headerLabelFormat":"[Week of ]dddd, MMMM Do YYYY",
        "group" : "hour",
        "periodLabelFormat" : "dddd",
        "periodIncrement" : 15,
        "containerCSS" : "periodContainer",
        "labelCSS" : "periodLabel",
    }
}

export { PERIOD_MAP };