import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getIsSandboxOrg from '@salesforce/apex/UtilityMethods.isSandboxOrg';	
import getIsrForceUser from '@salesforce/apex/UtilityMethods.isrForceUser';

const sfClassicEditBaseUrl = '/apex/EditOrderProduct';
const sfAROEditBaseUrl = '/rForceARO/apex/EditOrderProduct';
export default class AddDayLightProductButton extends NavigationMixin(LightningElement) {
    @api configid;
    @api inRop;
    @api editbaseurl;
	@api recordId;
	@track isSandboxOrg;
	@track isrForceUser;
	@api isLightning;
	@track openModal = false;

	connectedCallback(){
		console.log('AddDayLightProductButton isLightning: ', this.isLightning);
	}
	
    addProduct(){
		this.openModal = true;
		// window.open("https://mingle-cloudidentities.inforcloudsuite.com/idp/SSO.saml2" , '_top');
	}

	closeModal(){
		this.openModal = false;
	}
}