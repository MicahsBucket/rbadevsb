import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import getChangeOrderList from '@salesforce/apex/ChangeOrderListCtlr.getChangeOrderList';
import getOrderRevRev from '@salesforce/apex/ChangeOrderListCtlr.getOrderRevRec';
import createChangeOrder from '@salesforce/apex/ChangeOrderListCtlr.createChangeOrder';

const changeOrderColumns = [
    { label: 'Order Number', fieldName: 'orderNumberUrl', type: "url", typeAttributes: {'label': { fieldName: 'orderNumber'}, value:{fieldName: 'orderNumberUrl'}, 'target': '_blank', 'tooltip': {fieldName: 'orderNumber'}}}, 
    //{ label: 'Order Number', fieldName: 'orderNumber', cellAttributes: {'alignment': 'left'}, type: 'string'},
    //{ label: 'Order Name', fieldName: 'orderNameUrl', cellAttributes: {'alignment': 'left'}, type: 'url', typeAttributes: {label: {fieldName: 'name'}, value: {fieldName: 'orderNameUrl'}, target: '_top', 'tooltip':{fieldName: 'name'}}},
    { label: 'Retail Total', fieldName: 'retailTotal', cellAttributes: {'alignment': 'left'}, type: 'currency', typeAttributes: {'label': {fieldName: 'installedProductName'}, 'target': '_top', 'tooltip':  {fieldName: 'installedProductName'}} },
    { label: 'Status', fieldName: 'status', cellAttributes: {'alignment': 'left'}, type: 'string'},
    { label: 'Created Date', fieldName: 'createdDate', cellAttributes: {'alignment': 'left'}, type: 'date', typeAttributes: {
        year: "numeric",
        month: "long",
        day: "2-digit",
        hour: "2-digit",
        minute: "2-digit"
    } },
];

export default class ChangeOrderList extends NavigationMixin(LightningElement) {

    @api soldOrderId;
    @api recordId;

    @track revRecDate;

    @track changeOrderColumns=changeOrderColumns;
    @track listChangeOrder;
    @track coDataLength;

    @track confirmationMessage = 'Are you sure you want to create a new Change Order?';

    @track showNewCOModal = false;
    @track disableCancelButton = false;
    @track disableSaveButton = false;
    @track showSpinner = false;

    @track showData = false;
    @track data = [];


    @wire(getChangeOrderList, {orderId: '$recordId'})
    wiredChangeOrderListMethod({ error, data }) {             
        if (data) {
            console.log('List Change Order ' +JSON.stringify(data));
            this.data = JSON.parse(JSON.stringify(data));         
            this.showData = true;   
            //this.revRecDate = data[0].revRecDate;
            //this.soldOrderId = data[0].soldOrder;
            this.listChangeOrder =  data;  
            this.coDataLength = data.length;
            this.recordCount = 'Change Orders (' + data.length + ')';        
        } else if (error) {
            this.error = error;
           }     
           console.log('List Change Order ' +JSON.stringify(data));
           console.log('List Change Order 2 ' +JSON.stringify( this.listChangeOrder));
           console.log('CO Length: ' + this.coDataLength);
           console.log('Rev Rec Date: ' + this.revRecDate);          
    }

    @wire(getOrderRevRev, {orderId: '$recordId'})
    wiredOrderRevRecMethod({ error, data }) {             
        if (data) {
            console.log('List Change Order ' +JSON.stringify(data));
            this.data = JSON.parse(JSON.stringify(data));          
            this.revRecDate = data[0].revRecDate;
            this.soldOrderId = data[0].id;      
        } else if (error) {
            this.error = error;
           }     
           console.log('List Rev Rec 2' +JSON.stringify(data));
           console.log('Rev Rec Date: ' + this.revRecDate);          
    }


    handleAddChangeOrderClicked(event) {

        //if (confirm("Are you sure you want to create a new Change Order?"));

        /*if (this.revRecDate == null || this.revRecDate.length == 0) {
            alert("You cannot create a Change Order from an order that has not been revenue recognized.");
        } else {
            this.handleClick()
        }*/
        if (this.revRecDate == null || this.revRecDate.length == 0) {
            this.confirmationMessage = 'You cannot create a Change Order from an Order that has not been revenue recognized.';
            this.disableSaveButton = true;
        } else {
            this.confirmationMessage = 'Are you sure you want to create a new Change Order?';
            this.disableSaveButton = false;
        }
        this.showNewCOModal = true;

    }

    handleCancelCOClick(event) {
        this.showNewCOModal = false;
    }

    handleSaveCOClick() {
        this.showSpinner = true;
        this.disableCancelButton = true;
        this.disableSaveButton =true;
            createChangeOrder({ orderId: this.soldOrderId })
                .then(result => {
                    var resultMap = result;

                    var returnValue = '';
                    var saveSuccess = false;
                    for (var key in resultMap) {
                        returnValue = key;
                        if (key === 'New Change Order Success') {
                            returnValue = resultMap[key];
                            saveSuccess = true;
                        }
                        break;
                    }
                    console.log('Return Value', returnValue);
        
                    if (saveSuccess === true) {

                        this.handleSaveCOSuccess(returnValue);
                    
                    }
                    else if (saveSuccess === false) {
        
                        this.handleSaveCOFailure(returnValue);
        
                    }
                    
                    console.log(JSON.stringify(result));
                    console.log("result", this.message);
                })
                .catch(error => {
                    this.message = undefined;
                    this.error = error;
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error creating record',
                            message: error.body.message,
                            variant: 'error',
                        }),
                    );
                    console.log("error", JSON.stringify(this.error));
                });
            
    }

    handleSaveCOSuccess(newChangeOrderId) {

        
                this.disableCancelButton = false;
                this.disableSaveButton = false;
                this.showSpinner = false;
        
                // Close the modal window and notify the user that
                // the Change Order was created successfully.
                this.showNewCOModal = false;
                const toastEvent = new ShowToastEvent({
                    message: 'New Change Order created',
                    variant: 'success',
                    mode: 'pester'
                });
                this.dispatchEvent(toastEvent);
        
                // Navigate to the new Change Order.
                this[NavigationMixin.Navigate]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: newChangeOrderId,
                        actionName: 'view'
                    }
                });
        
            }
        
            /*
            * @author Wallace Wylie
            * @date 03/19/2020
            * @description Method fired from child component to handle the
            *              new Change Order creation failure.
            */
            handleSaveCOFailure(errorMessage) {
        
        //        var errorMessage = event.detail;
        
                this.disableCancelButton = false;
                this.disableSaveButton = false;
                this.showSpinner = false;
        
                // Close the modal window and notify the user
                // that the Change Order creation failed.
                this.showNewCOModal = false;
                const toastEvent = new ShowToastEvent({
                    title: 'Unable to create New Change Order',
                    message: errorMessage,
                    variant: 'error',
                    mode: 'sticky'
                });
                this.dispatchEvent(toastEvent);
        
            }


}