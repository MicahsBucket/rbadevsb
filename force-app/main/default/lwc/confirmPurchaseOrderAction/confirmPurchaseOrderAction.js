/*
* @author Jason Flippen
* @date 02/25/2020
* @description Provides functionality for the confirmPurchaseOrderAction LWC.
*
*			   Supporting database functionality provided by the following Apex Class:
*			   - ConfirmPOActionController
*/

import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import { registerListener } from 'c/pubsub';
import { fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
import getPurchaseOrderData from '@salesforce/apex/ConfirmPOActionController.getPurchaseOrderData';
import confirmPurchaseOrder from '@salesforce/apex/ConfirmPOActionController.confirmPurchaseOrder';

const productColumns = [
    { label: 'Product', fieldName: 'productName', type: 'string', cellAttributes: { 'alignment': 'left' } },
    { label: 'Cost', fieldName: 'unitWholesaleCost', type: 'currency', editable: true, cellAttributes: {'alignment': 'left'} },
    { label: 'Old Variant', fieldName: 'variantNumberOld', type: 'string', editable: false, cellAttributes: {'alignment': 'left'} },
    { label: 'New Variant', fieldName: 'variantNumber', type: 'string', editable: true, cellAttributes: {'alignment': 'left'} }
];

export default class confirmPurchaseOrderAction extends LightningElement {

    @api recordId;

    wiredPOResult;

    purchaseOrder;
    recordError;
    productColumnList = productColumns;
    isEligible = false;
    displayText = '';
    disableButtons = false;
    showSpinner = false;
    confirmationNumber='';

    @wire(CurrentPageReference) pageRef;

    /*
    * @author Jason Flippen
    * @date 03/16/2020
    * @description Event fired when the element is inserted into the document.
    *              It's purpose here is to subscribe the "updatePOEvent". 
    */
    connectedCallback() {
        registerListener('updatePOEvent', this.refreshPOData, this);
    }
    
    /*
    * @author Jason Flippen
    * @date 02/25/2020
    * @description Method to retrieve data from Purchase Order. 
    */
    @wire(getPurchaseOrderData, { purchaseOrderId: '$recordId' })
    wiredGetPurchaseOrderData(result) {

        // Hold on to the provisioned value so it can be refreshed.
        this.wiredPOResult = result;
        const { data, error } = result; // destructure the provisioned value
        if (data) {
            console.log('Record Data', JSON.parse(JSON.stringify(data)));
            this.purchaseOrder = JSON.parse(JSON.stringify(data));

            // Is the Purchase Order eligible to be Confirmed?
/*
            if (this.purchaseOrder.relatedOrderService === true &&
                this.purchaserOrder.vendorName === 'Andersen Logistics - Minneapolis' &&
                this.purchaseOrder.storeLocation.Name === '0077 - Twin Cities' &&
                this.purchaseOrder.chargeCostTo === 'Manufacturing') {
                this.isEligible = false;
            }
*/
            if (this.purchaseOrder.vendorName !== '' && (this.purchaseOrder.status == 'Released' || this.purchaseOrder.status == 'Confirmed')
                &&
                (this.purchaseOrder.costPurchaseOrder === true || this.purchaseOrder.relatedOrderService === true ||
                 (this.purchaseOrder.relatedOrderIsClosed === false &&
                  (this.purchaseOrder.relatedOrderCORO === false || (this.purchaseOrder.relatedOrderCORO === true && this.purchaseOrder.revRecNullAndValidStatus === true))))) {
                this.isEligible = true;
                this.displayText = '';
            }
            else {
                this.isEligible = false;
                this.displayText = 'Purchase Order cannot be Confirmed';
            }

            this.enableSave();

        }
        else if (error) {
            this.recordError = error;
            console.log('error in PurchaseOrderData callback', error);
        }

    }

    /*
    * @author Jason Flippen
    * @date 02/25/2020
    * @description Method to handle the "Change" event from the Estimated Ship Date field.
    */
    handleEstimatedShipDateChange(event) {
        this.purchaseOrder.estimatedShipDate = event.target.value;
        this.enableSave();
    }

    /*
    * @author Jason Flippen
    * @date 02/26/2020
    * @description Method to handle the "Change" event from the Product List datatable component.
    */
    handleProductValueChange(event) {

        let draftValues = JSON.parse(JSON.stringify(event.detail.draftValues));
        let productList = JSON.parse(JSON.stringify(this.purchaseOrder.productList));
        for(let draftIndex in draftValues){

            let draftId = draftValues[draftIndex].id;
            let draftUnitWholesaleCost = draftValues[draftIndex].unitWholesaleCost;
            let draftVariantNumber = draftValues[draftIndex].variantNumber;

            console.log('Draft Id', draftId);
            console.log('Draft unitWholesaleCost', draftUnitWholesaleCost);
            console.log('Draft variantNumber', draftVariantNumber);

            for (let productIndex in productList) {

                let productId = productList[productIndex].id;
                if (productId === draftId) {
                    if (draftUnitWholesaleCost !== undefined) {
                        productList[productIndex].unitWholesaleCost = draftUnitWholesaleCost;
                        this.purchaseOrder.productList[productIndex] = productList[productIndex];
                    }
                    if (draftVariantNumber !== undefined) {
                        productList[productIndex].variantNumber = draftVariantNumber;
                        this.purchaseOrder.productList[productIndex] = productList[productIndex];
                    }
                    break;
                }
            }
        }
        console.log('Product List (Changed)', JSON.parse(JSON.stringify(this.purchaseOrder.productList)));

        this.enableSave();

    }

    /*
    * @author Jason Flippen
    * @date 02/26/2020
    * @description Method to enable/disable the Save button.
    */
    enableSave() {

        let enableSave = true;
        
        // Is the PO eligible to be confirmed and does it have an Estimated Ship Date?
        if (this.isEligible === false || this.purchaseOrder.estimatedShipDate == null || this.purchaseOrder.confirmationNumber == null) {
            enableSave = false;
        }

        if (enableSave === true) {

            if (this.purchaseOrder.zeroDollarVendor === false) {

                for (let index in this.purchaseOrder.productList) {

                    // Determine whether or not the Unit Wholesale Cost is required and if so make sure it has a value.
                    let isNotServiceManufacturing = (this.purchaseOrder.relatedOrderService === false || this.purchaseOrder.productList[index].chargeCostTo !== 'Manufacturing');
                    if (this.purchaseOrder.productList[index].unitWholesaleCost === undefined ||
                        this.purchaseOrder.productList[index].unitWholesaleCost === '' ||
                        this.purchaseOrder.productList[index].unitWholesaleCost < 0 ||
                        (isNotServiceManufacturing === true && this.purchaseOrder.productList[index].unitWholesaleCost == 0)) {
                        enableSave = false;
                    }
                    
                    // Determine whether or not the Variant Number is required and if so make sure it has a value.
                    if (this.purchaseOrder.relatedOrderService === true &&
                        this.purchaseOrder.vendorName === 'Renewal by Andersen' &&
                        this.purchaseOrder.productList[index].productName === 'Complete Unit' && 
                        (this.purchaseOrder.productList[index].variantNumber === undefined || this.purchaseOrder.productList[index].variantNumber === '')) {
                        enableSave = false;
                    }

                    if (enableSave === false) {
                        break;
                    }
                    
                }

            }

        }

        // Raise the event to Enable or Disable the Save button.
        if (enableSave === false) {
            const disableSaveEvent = new CustomEvent('DisableSave');
            this.dispatchEvent(disableSaveEvent);
        }
        else if (enableSave === true) {
            const enableSaveEvent = new CustomEvent('EnableSave');
            this.dispatchEvent(enableSaveEvent);
        }

    }

    /*
    * @author Jason Flippen
    * @date 02/25/2020
    * @description Method to update the Status of the Purchase Order to "Confirmed".
    */
    @api
    handleSaveClick() {

        const showSpinner = new CustomEvent('ShowSpinner');
        const hideSpinner = new CustomEvent('HideSpinner');
        const closeQuickAction = new CustomEvent('CloseQuickAction');

        this.dispatchEvent(showSpinner);
        // Call Apex method to confirm the Purchase Order.
        confirmPurchaseOrder({ purchaseOrder: this.purchaseOrder })
        .then((result) => {
            let toastMessage;
            let toastType;
            let saveSuccess = false;
            if (result === 'Confirm PO Success') {
                saveSuccess = true;
                toastMessage = 'Purchase Order has been Confirmed';
                toastType = 'success';
            }
            else  {
                toastMessage = result;
                toastType = 'error';
            }   
            
            this.dispatchEvent(hideSpinner);
            
            const showToastEvent = new CustomEvent('ShowToast', { detail: { message: toastMessage, type: toastType } });
            this.dispatchEvent(showToastEvent);
            this.dispatchEvent(closeQuickAction);

            if (saveSuccess === true) {

                // Workaround way to refresh the record page.
                updateRecord({ fields: { Id: this.recordId } });

                // Fire event to refresh the cache.
                fireEvent(this.pageRef, 'updatePOEvent', '');

//                // Use the provisioned value to refresh wiredGetPurchaseOrderData.
//                return refreshApex(this.wiredPOResult);
            }
            
        })
        .catch((error) => {
            console.log('Confirm Purchase Order Error', error);
            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(this.closeQuickAction);
        });

    }

    /*
    * @author Jason Flippen
    * @date 03/16/2020
    * @description Method to refresh the cache. 
    */
    refreshPOData() {
        console.log('Refreshing confirmPO');
        return refreshApex(this.wiredPOResult);
    }

    handleConfirmationNumber(event){
        console.log('handleConfirmationNumber: ',event.target.value);
        this.purchaseOrder.confirmationNumber = event.target.value;
        this.enableSave();
    }

}