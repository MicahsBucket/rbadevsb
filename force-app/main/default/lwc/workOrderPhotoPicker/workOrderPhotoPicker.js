/* eslint-disable no-useless-concat */
/* eslint-disable vars-on-top */
/* eslint-disable no-undef */
/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import JSZipJs from '@salesforce/resourceUrl/jszip';
// import FileSaverjs from '@salesforce/resourceUrl/filesaverjs';
import getListOfFileIds from '@salesforce/apex/WorkOrderPhotoPickerController.getListOfFileIds';
import getFile from '@salesforce/apex/WorkOrderPhotoPickerController.getFile';
import getListOfAttachmentIds from '@salesforce/apex/WorkOrderPhotoPickerController.getListOfAttachmentIds';
import getAttachment from '@salesforce/apex/WorkOrderPhotoPickerController.getAttachment';
import getRelatedWorkOrders from '@salesforce/apex/WorkOrderPhotoPickerController.getRelatedWorkOrders';
import transferSelectedFiles from '@salesforce/apex/WorkOrderPhotoPickerController.transferSelectedFiles';

// import JSZipMin from '@salesforce/resourceUrl/jszipmin';

export default class WorkOrderPhotoPicker extends LightningElement {
    @track imageURL;
    @track photos = [];
    @track isLoading = true;
    @track modalIsOpen = false;
    @track dataTableColumns;
    @track relatedOrders;
    @track isFileTransferLoading;
    @api recordId;

    dtColumns = [
        {label: 'WorkOrder/Order', fieldName: 'Name', type: 'text'},
        {label: 'Id', fieldName: 'Id', type: 'text'},
        {label: 'Work Type', fieldName: 'WorkType', type: 'text'},
    ];
   
    selectedPhotos = [];

    connectedCallback(){
        //INITIALIZE COLUMNS
        this.dataTableColumns = this.dtColumns;

        console.log("connectedCallback");
        console.log("Something Changed");
        this.isLoading = true;

        // GET PHOTOS
        Promise.all([
            // this.executeAction(getWorkOrderPhotos, { recordId: this.recordId }),
            this.executeAction(getListOfFileIds, { recordId: this.recordId }),
            this.executeAction(getListOfAttachmentIds, { recordId: this.recordId }),
            this.executeAction(getRelatedWorkOrders, { currentWorkOrderId: this.recordId }),
            loadScript(this, JSZipJs),
            
            // loadScript(this, FileSaverjs),
        ])
        .then((results) => {
            console.log(results);
            let relatedRecordRows = [];

            relatedRecordRows.push({
                Name : 'Order',
                Id : results[2].orderRecord.Id,
                WorkType : "",
            });
            
            results[2].workOrders.forEach(wo => {
                relatedRecordRows.push({
                    Name : 'Work Order',
                    Id : wo.Id,
                    WorkType : wo.Work_Order_Type__c,
                });
            });
            console.log("relatedRecordRows");
            console.log(relatedRecordRows);
            this.relatedOrders = relatedRecordRows;


            let promiseArray = [];
            //Iterate over Files List
            results[0].forEach(item => {
                promiseArray.push( this.executeAction(getFile, { fileRecordId: item }) );
            });
            //Iterate over Attachments List
            results[1].forEach(item => {
                promiseArray.push( this.executeAction(getAttachment, { attachmentRecordId: item }) );
            });

            Promise.all(promiseArray).then((res) =>{
                console.log(res);
                res.forEach(el => {
                    let dataString = "data:image/" + el.fileType.toLowerCase() + ";base64, " + el.photoDataString;
                    let photoCardData = {
                        contentVersionId : el.contentVersionId,
                        title : el.title,
                        photoDataString : dataString,
                        rawDataString : el.photoDataString,
                        fileType : el.fileType,
                        isSelected : false,
                    }
                    this.photos.push(JSON.parse(JSON.stringify(photoCardData)));
                });
                this.isLoading = false;
            });
        })
        .catch(error => {
            console.log(error.message);
        });
    }

    openModal(){
        if(this.selectedPhotos.length === 0){
            return;
        }
        console.log("Open Modal");
        this.modalIsOpen = true;
    }

    handleSelect(event){
        let id = event.currentTarget.dataset.id;
        let isSelected = JSON.parse(event.currentTarget.dataset.selected);
        // console.log(id);

        // let thisDataSet = this.template.querySelector(`[data-id="${id}"]`).dataset;
        // let datadata = JSON.parse(JSON.stringify(thisDataSet));
        // console.log(thisDataSet);
        // console.log(datadata);
        console.log(isSelected);

        if(isSelected === true){
            // this.template.querySelector(`[data-id="${id}"]`).value = amountDue;
            let index = this.selectedPhotos.indexOf(id);
            this.selectedPhotos.splice(index, 1);
            this.template.querySelector(`[data-id="${id}"]`).dataset.selected = false;
            // this.template.querySelector(`[data-outercard="${id}"]`).classList.add('selected-background-color');
            // this.template.querySelector(`[data-innercard="${id}"]`).classList.add('selected-background-color');
            this.photos.forEach(el => {
                if(el.contentVersionId == id){
                    el.isSelected = false;
                }
            });
            
        } else if(isSelected === false) {
            this.selectedPhotos.push(id);
            this.template.querySelector(`[data-id="${id}"]`).dataset.selected = true;

            this.photos.forEach(el => {
                if(el.contentVersionId == id){
                    el.isSelected = true;
                }
            });
        }

        console.log(JSON.parse(JSON.stringify(this.photos)));
        console.log(this.selectedPhotos);
        // this.template.querySelector(`[data-payment-id="${dataId}"]`).value = amountDue;
        // this.template.querySelector(`[data-sublinerow="${dataSet.paymentId}"]`).classList.add('slds-is-selected');

    }

    cancelTransfer(){
        console.log("Cancel");
        this.modalIsOpen = false;
    }

    transferFiles(){
        this.isFileTransferLoading = true;
        console.log("Transfer Files");
        //TODO:
        //1. get all selected related record IDs in a list
        let selectedRows = this.template.querySelector('lightning-datatable').getSelectedRows();
        let selectedRowIds = [];
        selectedRows.forEach(row => {
            selectedRowIds.push(row.Id);
        });
        console.log('selectedRowIds');
        console.log(selectedRowIds);

        //2. Get all selected Files
        let selectedFiles = this.selectedPhotos;
        
        //3. send list to apex method.
        this.executeAction(transferSelectedFiles, { relatedRecordIds: selectedRowIds, selectedFileIds: selectedFiles }).then((res) => {
            console.log(res);
            console.log("TRANSFERED");

            //3. on complete close out of modal on success.
            if(res){
                this.modalIsOpen = false;
            }
            this.isFileTransferLoading = false;
        });

    }

    handleSelectAll(){
        console.log("handleSelectAll");
        console.log(this.selectedPhotos);

        this.photos.forEach(element => {
            this.template.querySelector(`[data-id="${element.contentVersionId}"]`).dataset.selected = true;
            if(!this.selectedPhotos.includes(element.contentVersionId)){
                this.selectedPhotos.push(element.contentVersionId);
                element.isSelected = true;
            }
        });
    }

    handleClearSelected(){
        this.selectedPhotos.forEach(element => {
            this.template.querySelector(`[data-id="${element}"]`).dataset.selected = false;
        });
        this.selectedPhotos = [];
        this.photos.forEach(element => {
            element.isSelected = false;
        });
    }

    handleDownloadSelected(){
        console.log("handleDownloadSelected");
        console.log(this.selectedPhotos);
    }


    handleDownload(){
        if(this.selectedPhotos == null || this.selectedPhotos.length === 0){
            return;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        /*! @source http://purl.eligrey.com/github/FileSaver.js/blob/master/FileSaver.js */
        /*
        * FileSaver.js
        * A saveAs() FileSaver implementation.
        *
        * By Eli Grey, http://eligrey.com
        *
        * License : https://github.com/eligrey/FileSaver.js/blob/master/LICENSE.md (MIT)
        * source  : http://purl.eligrey.com/github/FileSaver.js
        */

        // The one and only way of getting global scope in all environments
        // https://stackoverflow.com/q/3277182/1008999


        var _global = typeof window === 'object' && window.window === window
        ? window : typeof self === 'object' && self.self === self
        ? self : typeof global === 'object' && global.global === global
        ? global
        : this

        function bom (blob, opts) {
        if (typeof opts === 'undefined') opts = { autoBom: false }
        else if (typeof opts !== 'object') {
        console.warn('Deprecated: Expected third argument to be a object')
        opts = { autoBom: !opts }
        }

        // prepend BOM for UTF-8 XML and text/* types (including HTML)
        // note: your browser will automatically convert UTF-16 U+FEFF to EF BB BF
        if (opts.autoBom && /^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(blob.type)) {
        return new Blob([String.fromCharCode(0xFEFF), blob], { type: blob.type })
        }
        return blob
        }

        function download (url, name, opts) {
        var xhr = new XMLHttpRequest()
        xhr.open('GET', url)
        xhr.responseType = 'blob'
        xhr.onload = function () {
        saveAs(xhr.response, name, opts)
        }
        xhr.onerror = function () {
        console.error('could not download file')
        }
        xhr.send()
        }

        function corsEnabled (url) {
        var xhr = new XMLHttpRequest()
        // use sync to avoid popup blocker
        xhr.open('HEAD', url, false)
        try {
        xhr.send()
        } catch (e) {}
        return xhr.status >= 200 && xhr.status <= 299
        }

        // `a.click()` doesn't work for all browsers (#465)
        function click (node) {
        try {
        node.dispatchEvent(new MouseEvent('click'))
        } catch (e) {
        var evt = document.createEvent('MouseEvents')
        evt.initMouseEvent('click', true, true, window, 0, 0, 0, 80,
                                20, false, false, false, false, 0, null)
        node.dispatchEvent(evt)
        }
        }

        var saveAs = _global.saveAs || (
        // probably in some web worker
        (typeof window !== 'object' || window !== _global)
        ? function saveAs () { /* noop */ }

        // Use download attribute first if possible (#193 Lumia mobile)
        : 'download' in HTMLAnchorElement.prototype
        ? function saveAs (blob, name, opts) {
        var URL = _global.URL || _global.webkitURL
        var a = document.createElement('a')
        name = name || blob.name || 'download'

        a.download = name
        a.rel = 'noopener' // tabnabbing

        // TODO: detect chrome extensions & packaged apps
        // a.target = '_blank'

        if (typeof blob === 'string') {
            // Support regular links
            a.href = blob
            if (a.origin !== location.origin) {
            corsEnabled(a.href)
                ? download(blob, name, opts)
                : click(a, a.target = '_blank')
            } else {
            click(a)
            }
        } else {
            // Support blobs
            a.href = URL.createObjectURL(blob)
            setTimeout(function () { URL.revokeObjectURL(a.href) }, 4E4) // 40s
            setTimeout(function () { click(a) }, 0)
        }
        }

        // Use msSaveOrOpenBlob as a second approach
        : 'msSaveOrOpenBlob' in navigator
        ? function saveAs (blob, name, opts) {
        name = name || blob.name || 'download'

        if (typeof blob === 'string') {
            if (corsEnabled(blob)) {
            download(blob, name, opts)
            } else {
            var a = document.createElement('a')
            a.href = blob
            a.target = '_blank'
            setTimeout(function () { click(a) })
            }
        } else {
            navigator.msSaveOrOpenBlob(bom(blob, opts), name)
        }
        }

        // Fallback to using FileReader and a popup
        : function saveAs (blob, name, opts, popup) {
        // Open a popup immediately do go around popup blocker
        // Mostly only available on user interaction and the fileReader is async so...
        popup = popup || open('', '_blank')
        if (popup) {
            popup.document.title =
            popup.document.body.innerText = 'downloading...'
        }

        if (typeof blob === 'string') return download(blob, name, opts)

        var force = blob.type === 'application/octet-stream'
        var isSafari = /constructor/i.test(_global.HTMLElement) || _global.safari
        var isChromeIOS = /CriOS\/[\d]+/.test(navigator.userAgent)

        if ((isChromeIOS || (force && isSafari)) && typeof FileReader !== 'undefined') {
            // Safari doesn't allow downloading of blob URLs
            var reader = new FileReader()
            reader.onloadend = function () {
            var url = reader.result
            url = isChromeIOS ? url : url.replace(/^data:[^;]*;/, 'data:attachment/file;')
            if (popup) popup.location.href = url
            else location = url
            popup = null // reverse-tabnabbing #460
            }
            reader.readAsDataURL(blob)
        } else {
            var URL = _global.URL || _global.webkitURL
            var url = URL.createObjectURL(blob)
            if (popup) popup.location = url
            else location.href = url
            popup = null // reverse-tabnabbing #460
            setTimeout(function () { URL.revokeObjectURL(url) }, 4E4) // 40s
        }
        }
        )

        _global.saveAs = saveAs.saveAs = saveAs

        if (typeof module !== 'undefined') {
        module.exports = saveAs;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        
        // console.log("SOMETHING IS HAPPENING!!!!");
        // let zip = new JSZip();
        // //  let fileSave = new saveAs();

        // // var fileSave = new FileSaverjs();
        // zip.file("Hello.txt", "Hello World\n");
        // // zip.file("nested/hellothere.txt", "Hello 2.0 World\n");

        // console.log(zip);
        
        // zip.generateAsync({type:"blob"})
        // .then(function(content) {
        //     // see FileSaver.js
        //     console.log(content);
        //     saveAs(content, "example.zip");
        // }).catch(error => {
        //     console.log("Error");
        //     console.log(error);
        // });


        console.log("Button");
        let zip = new JSZip();
        // let fileSave = new saveAs();

        // var fileSave = new FileSaverjs();
        // zip.file("newfolder/Hello.txt", "Hello World\n");
        // zip.file("newfolder/newest.txt", "Hello to the new World\n");
        console.log(this.photos[0].fileType.toLowerCase());
        console.log(this.photos[0].rawDataString);
        
        // GETS ALL SELECTED FILES AND PUTS THEM INTO THE ZIP FOLDER
        this.selectedPhotos.forEach(element => {
            this.photos.forEach(el => {
                if(element === el.contentVersionId){
                    zip.file("workorder_pictures/" + el.title + "." + el.fileType.toLowerCase(), el.rawDataString, {base64: true});
                }
            });
        });
        
        zip.generateAsync({type:"blob"})
        .then(function(content) {
            // see FileSaver.js
            saveAs(content, "workorder_pictures.zip");
        });
    }
    
    executeAction(apexMethod, params) {
        return new Promise(function(resolve, reject) {
            apexMethod(params)
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }
}