import { LightningElement, wire, api ,track} from 'lwc';
import getRecord from '@salesforce/apex/WindowLeadTimeController.getRecordId';

export default class WindowLeadTimes extends LightningElement {
    @track recordId;

    @wire(getRecord)
    wiredContacts(result ) {
        if (result.data) {
            console.log('the res'+JSON.stringify(result.data));
           this.recordId=result.data;
           
        } else if (result.error) {
            this.error = result.error;
          
        }
    }
}