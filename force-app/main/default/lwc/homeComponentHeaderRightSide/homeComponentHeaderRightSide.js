import { LightningElement } from 'lwc';
import RFORCE_Images from '@salesforce/resourceUrl/rSupport';

export default class HomeComponentHeaderRightSide extends LightningElement {
    Rforceimage = RFORCE_Images ;
}