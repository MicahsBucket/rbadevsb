import { LightningElement, api, wire } from 'lwc';
import fetchWorkOrder from '@salesforce/apex/Work_Order_Details_Controller.getServiceAppointment';
import fetchUserTimeZone from '@salesforce/apex/Work_Order_Details_Controller.getCurrentLoggedInUserTimeZone';

export default class ServiceAppointmentDetails extends LightningElement {
    @api recordId;

    @wire(fetchWorkOrder, { serviceAppointmentId: '$recordId' })
    contact;

    @wire(fetchUserTimeZone, {})
    _timezone;
    get timezone() {
        return this._timezone;
    }
    
    get worktypeurl() {
        return '/'+this.contact.data.WorkTypeId;
    }
    
    get serviceTerritoryurl() {
        return '/'+this.contact.data.ServiceTerritoryId;
    }

    get accounturl() {
        return '/'+this.contact.data.AccountId;
    }

    get contacturl() {
        return '/'+this.contact.data.ContactId;
    }

    get soldOrderurl() {
        return '/'+this.contact.data.Sold_Order__c;
    }
    
    get opportunityurl() {
        return '/'+this.contact.data.Opportunity__c;
    }
}