import { LightningElement, api, wire } from 'lwc';
import getOrderDetails from '@salesforce/apex/Account_Details_Controller.getOrders';

const columns = [
    // {label: 'Order Number', fieldName: 'orderNumberUrl', type: 'url', 
    // typeAttributes: {label: { fieldName: 'OrderNumber' }, target: '_blank'}},
/*
    { label: 'Order Number', fieldName: 'Id', type: 'customUrl',
    typeAttributes: {
        field1: { fieldName: 'OrderNumber' },
        field2: { fieldName: 'orderId' },
        field3: { fieldName: 'orderNumberUrl' },
      }},
*/
    { label: 'Order Number', fieldName: 'orderNumberUrl', type: "url", typeAttributes: {label: { fieldName: 'OrderNumber' }, target: '_blank'}, sortable: true},
    { label: 'Status', fieldName: 'status', type: 'text' },
    { label: 'Sales Rep', fieldName: 'salesRep', type: 'text' },
    { label: 'Booking Date', fieldName: 'effectiveDate', type:'date-local', typeAttributes: {day: 'numeric', month: 'numeric' , year: 'numeric'}},
    { label: 'Estimated Ship Date', fieldName: 'estimatedShipDate', type:'date-local', typeAttributes: {day: 'numeric', month: 'numeric' , year: 'numeric'}},
    { label: 'Job Close Date', fieldName: 'jobCloseDate', type:'date-local', typeAttributes: {day: 'numeric', month: 'numeric' , year: 'numeric'}},
    { label: 'Retail Total', fieldName: 'retailTotal', type: 'currency', cellAttributes: { alignment: 'left' }},    
    { label: 'Amount Due', fieldName: 'amountDue', type: 'currency', cellAttributes: { alignment: 'left' }},
    
];

export default class AccountOrderRelatedList extends LightningElement {
    
    @api recordId;
    @api strurl = '';
    @api communityBoolean;
    @api communityString;
    
    orderList;
    columnList = columns;
	title = 'Orders (0)';
    showData = false;

    connectedCallback(){
        this.getOrderDetails();
    }

    // @wire(getOrderDetails, { accountId: '$recordId' , strurl: '$strurl' }) 
	// wiredOrderDetails({error, data}) {
	// 	if (error) {
	// 		console.log('error in callback', error);
    //     }
    //     else if (data) {
    //         console.log('Order', JSON.parse(JSON.stringify(data)));
	// 		this.orderList = JSON.parse(JSON.stringify(data));
    //         this.title = 'Orders (' + this.orderList.length + ')';
    //         if (this.orderList.length > 0) {
    //             this.showData = true;
    //         }
	// 	}
    // }

    getOrderDetails(){
        getOrderDetails({accountId : this.recordId, strurl: this.strurl, communityBoolean: this.communityString})
        .then(data => {
            if (data) {
                this.orderList = JSON.parse(JSON.stringify(data));
                this.title = 'Orders (' + this.orderList.length + ')';
                if (this.orderList.length > 0) {
                    this.showData = true;
                }
            }
            else if (error) {
                this.error = error;
            }
            //this.tableLoadingState = false;
            console.log('listServReqs ' +JSON.stringify(data));
        })
        .catch(error => {
            this.error = error;
        })
    }

}