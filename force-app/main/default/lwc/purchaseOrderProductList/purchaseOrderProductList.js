/*
* @author Jason Flippen
* @date 11/21/2019 
* @description Related List designed to be used in place of the standard Related List for Purchase Order Products.
*
*              Server-Side support provided by the following Class:
*			   - PurchaseOrderProductListController
*/
import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import { registerListener } from 'c/pubsub';
import { fireEvent } from 'c/pubsub';
import getPurchaseOrderData from '@salesforce/apex/PurchaseOrderProductListController.getPurchaseOrderData';
//import getOrderItemList from '@salesforce/apex/PurchaseOrderProductListController.getOrderItemList';
import getCharges from '@salesforce/apex/PurchaseOrderProductListController.getCharges';
import deleteProduct from '@salesforce/apex/PurchaseOrderProductListController.deleteProduct';
import fetchPartDetail from '@salesforce/apex/PurchaseOrderProductListController.fetchPartDetail';
import saveParts from '@salesforce/apex/PurchaseOrderProductListController.saveParts';
import saveProducts from '@salesforce/apex/PurchaseOrderProductListController.saveProducts';
import updateRemakeProducts from '@salesforce/apex/PurchaseOrderProductListController.updateRemakeProducts';

const availableProductColumns = [
    { label: 'Product', fieldName: 'serviceProductName', type: 'string', cellAttributes: { 'alignment': 'left' } },
    { label: 'Installed Product', fieldName: 'installedProductName', type: 'string', cellAttributes: {'alignment': 'left'} },
    { label: 'Variant Number', fieldName: 'variantNumber', type: 'string', cellAttributes: {'alignment': 'left'} },
    { label: 'Responsibility', fieldName: 'chargeCostTo', type: 'string', cellAttributes: {'alignment': 'left'} },
    { label: 'Source of Defect', fieldName: 'category', type: 'string', cellAttributes: {'alignment': 'left'} },
    { label: 'Defect Details', fieldName: 'whatWhere', type: 'string', cellAttributes: {'alignment': 'left'} },
];

const newPartColumns = [
    { label: 'Part', fieldName: 'name', type: 'string', initialWidth: 175, cellAttributes: { 'alignment': 'left' } },
    { label: 'Description', fieldName: 'description', type: 'string', cellAttributes: { 'alignment': 'left' } },
    { label: 'Quantity', fieldName: 'quantity', type: 'number', initialWidth: 100, cellAttributes: { 'alignment': 'center' } },
    { label: '', type: 'button', fixedWidth: 100, typeAttributes: { label: 'Remove', name: 'remove'} }
];

export default class purchaseOrderProductList extends NavigationMixin(LightningElement) {

    @api recordId;

    wiredPOResult;

    @track purchaseOrder;
    @track purchaseOrderError;
    @track title = '';
    @track showAddProductsButton = false;
    @track addProductsTitle = '';
    @track columnList = [];
//    @track orderProductList = [];
//    @track orderProductError;
    @track rowId;
    @track showData = false;
    @track showAddPartsModal = false;
    @track showAddProductsModal = false;
    @track disableDeleteAction = true;
    @track disableAddPartsSaveButton = true;
    @track disableAddPartsCancelButton = false;
    @track disableAddProductsSaveButton = true;
    @track disableAddProductsCancelButton = false;
    @track showSpinner = false;

    @track receiving = false;
    @track vendorCredit = false;
    @track writeOff = false;

    @track vendorList = [];
    @track selectedVendorId = '';
    @track responsibilityList = [];
    @track selectedResponsibility = '';
    @track availableProductColumnList = availableProductColumns;
    @track availableProductList = [];
    @track defaultSelectedProductList = [];
    @track noProductsAvailable = true;

    @track newPartColumnList = newPartColumns;
    @track newPartList = [];
    @track noPartsAdded = true;

    @track openEditModal = false;

    newPart = {
        id : '',
        quantity : '0'
    };

    selectedProductList = [];

    @wire(CurrentPageReference) pageRef;

    connectedCallback() {
        // subscribe to updatePOEvent
        registerListener('updatePOEvent', this.refreshPOData, this);
    }

    refreshPOData() {
        console.log('Refreshing purchaseOrderPOList');
        return refreshApex(this.wiredPOResult);
    }

    /*
    * @author Jason Flippen
    * @date 12/02/2019
    * @description Method to retrieve data from Purchase Order and the List of columns to be displayed. 
    */
    @wire(getPurchaseOrderData, { purchaseOrderId: '$recordId' })
    wiredGetPurchaseOrderData(result) {

        // Hold on to the provisioned value so it can be refreshed.
        this.wiredPOResult = result;
        const { data, error } = result; // destructure the provisioned value
        if (data) {
			console.log('Purchase Order Data', JSON.parse(JSON.stringify(data)));
            this.purchaseOrder = JSON.parse(JSON.stringify(data));

            this.disableDeleteAction = true;
            if (this.purchaseOrder.submittedTimestamp == null &&
                this.purchaseOrder.status !== 'Cancelled' &&
                this.purchaseOrder.status !== 'Confirmed' &&
                this.purchaseOrder.status !== 'Partially Received' &&
                this.purchaseOrder.status !== 'Received' &&
                this.purchaseOrder.status !== 'Released') {
                this.disableDeleteAction = false;
            }

            if (this.purchaseOrder.costPurchaseOrder == true) {
                this.title = 'Parts (' + this.purchaseOrder.productList.length + ')';
                this.addProductsTitle = 'Add Parts';
            }
            else {
                this.title = 'Products (' + this.purchaseOrder.productList.length + ')';
                this.addProductsTitle = 'Add Products';
            }

            if (this.purchaseOrder.costPurchaseOrder == true || this.purchaseOrder.servicePurchaseOrder == true) {
                this.showAddProductsButton = true;
            }

            if (this.purchaseOrder.productList.length > 0) {
                this.showData = true;
            }

            this.vendorList = [{label: this.purchaseOrder.vendorName, value: this.purchaseOrder.vendorId }];
            this.selectedVendorId = this.purchaseOrder.vendorId;
            this.responsibilityList = [{label: this.purchaseOrder.chargeCostTo, value: this.purchaseOrder.chargeCostTo }];
            this.selectedResponsibility = this.purchaseOrder.chargeCostTo;

//            this.getOrderProductList();
            this.getColumnList();
            this.getAvailableProducts();

        }
        else if (error) {
			console.log('error in PurchaseOrderData callback', error);
			this.purchaseOrderError = error;
        }

    }

    /*
    * @author Jason Flippen
    * @date 12/02/2019
    * @description Method to retrieve the List of Columns to be displayed.
    */
    getColumnList() {

        var columnList = [];

        var productLabel = 'Product';

        if (this.purchaseOrder.costPurchaseOrder === true) {
            productLabel = 'Part';
        }

        columnList.push({ label: productLabel, fieldName: "productNamePartNumber", type: "text", initialWidth: 200, wrapText: true });

/*
        if (this.purchaseOrder.costPurchaseOrder === true) {
            columnList.push({ label: "GL Account", fieldName: "glAccount", type: "text", initialWidth: 200 });
        }
*/

        columnList.push({ label: "Description", fieldName: "productDescription", type: "text", initialWidth: 250, wrapText: true });
        columnList.push({ label: "Quantity", fieldName: "quantity", type: "number", initialWidth: 100, cellAttributes: { alignment: "left" } });

/*
        if (this.purchaseOrder.costPurchaseOrder === false) {
            columnList.push({ label: "Unit of Measure", fieldName: "unitOfMeasure", type: "text", initialWidth: 100 });
        }
*/

        if (this.purchaseOrder.costPurchaseOrder === false) {
            columnList.push({ label: "Retail Price", fieldName: "unitPrice", type: "currency", initialWidth: 115, cellAttributes: { alignment: "left" } });
        }

        if (this.purchaseOrder.servicePurchaseOrder === true) {
            columnList.push({ label: "Current Variant", fieldName: "soldOrderProdAssetVariantNumber", type: "text", initialWidth: 148, wrapText: true });    
        }

        if (this.purchaseOrder.productPurchaseOrder === true) {
            columnList.push({ label: "Variant Number", fieldName: "variantNumber", type: "text", initialWidth: 150, wrapText: true });
        }
        else if (this.purchaseOrder.servicePurchaseOrder === true) {
            columnList.push({ label: "New Variant", fieldName: "variantNumber", type: "text", initialWidth: 148, wrapText: true });
        }
        
        columnList.push({ label: "Cost", fieldName: "unitWholesaleCost", type: "currency", initialWidth: 100, cellAttributes: { alignment: "left" } });

        if (this.purchaseOrder.costPurchaseOrder === true) {
            columnList.push({ label: "Total Cost", fieldName: "totalWholesaleCost", type: "currency", initialWidth: 100, cellAttributes: { alignment: "left" } });
        }

        if (this.purchaseOrder.servicePurchaseOrder === true) {
            columnList.push({ label: "MTO Source Code", fieldName: "mtoSourceCode", type: "text", initialWidth: 148, wrapText: true });
            columnList.push({ label: "Defect Code", fieldName: "defectCode", type: "text", initialWidth: 148, wrapText: true });
            columnList.push({ label: "Notes", fieldName: "notes", type: "text", initialWidth: 225, wrapText: true });
            columnList.push({ label: "Multiple Remakes", fieldName: "multipleRemakes", type: "boolean", editable: false, initialWidth: 131, cellAttributes: { alignment: "left" } });
        }

        if (this.purchaseOrder.costPurchaseOrder === true) {
            columnList.push({ label: "Discount Amount", fieldName: "discountAmount", type: "currency", initialWidth: 100, cellAttributes: { alignment: "left" } });
        }

/*
        if (this.purchaseOrder.vendorName.includes('Andersen') || this.purchaseOrder.vendorName.includes('RbA')) {
            columnList.push({ label: "NSPR/SPR", fieldName: "nspr", type: "boolean", initialWidth: 100, cellAttributes: { alignment: "left" } });            
        }
*/

        columnList.push({ label: "Qty Received", fieldName: "installedProdAssetQty", type: "number", initialWidth: 135, cellAttributes: { alignment: "left" } });
        columnList.push({ label: "Date Received", fieldName: "installedProdAssetDateReceived", type: "date-local", initialWidth: 108, cellAttributes: { alignment: "left" }, typeAttributes: { month: "2-digit", day: "2-digit" } });

        if (this.purchaseOrder.costPurchaseOrder === true || this.purchaseOrder.servicePurchaseOrder === true) {
            
            columnList.push({ label: "Qty Written Off", fieldName: "installedProdAssetQtyWrittenOff", type: "number", initialWidth: 141, cellAttributes: { alignment: "left" } });
            columnList.push({ label: "Vendor Credit", fieldName: "installedProdAssetVendorCredit", type: "currency", initialWidth: 121, cellAttributes: { alignment: "left" } });

            var actions = [{ label: 'Delete', name: 'delete', disabled: this.disableDeleteAction },
                            { label: 'Edit Remakes', name: 'edit' }];
            columnList.push({ type: 'action', typeAttributes: { rowActions: actions, menuAlignment: 'Auto' }});
    
        }

        this.columnList = columnList;
    
    }
    
    /*
    * @author Jason Flippen
    * @date 11/21/2019
    * @description Method to retrieve OrderItems (Order Products) related to the Purchase Order.
    */
/*
    getOrderProductList() {

        getOrderItemList({ purchaseOrderId: this.recordId })
        .then((result) => {
            console.log('OrderProduct Data', JSON.parse(JSON.stringify(result)));
            this.orderProductList = JSON.parse(JSON.stringify(result));
            this.title = 'Order Products (' + this.orderProductList.length + ')';
            if (this.orderProductList.length > 0) {
                this.showData = true;
            }
        })
        .catch((error) => {
            console.log('error in OrderItemList callback', error);
            this.orderProductError = error;
        });

    }
*/

    /*
    * @author Jason Flippen
    * @date 06/28/2020
    * @description Method to retrieve List of Products eligible
    *              to be added to the Purchase Order. 
    */
    getAvailableProducts() {

        getCharges({ orderId: this.purchaseOrder.orderId,
                     vendorId: this.purchaseOrder.vendorId,
                     responsibility: this.purchaseOrder.chargeCostTo })
        .then(result => {
            console.log('Available Product Data', JSON.parse(JSON.stringify(result)));
            this.availableProductList = JSON.parse(JSON.stringify(result));
            if (this.availableProductList.length === 0) {
                this.noProductsAvailable = true;
            }
            else {
                this.noProductsAvailable = false;
/*
                this.defaultSelectedProductList = JSON.parse(JSON.stringify(result));
                console.log('Default Selected Product List', this.defaultSelectedProductList);
                this.selectedProductList = JSON.parse(JSON.stringify(result));
                console.log('Selected Product List', this.selectedProductList);
*/
            }
        })
        .catch(error => {
            console.log('Error in getCharges callback', error);
            this.error = error;
         })

    }

    /*
    * @author Jason Flippen
    * @date 02/10/2020
    * @description Method to handle the "View All" click event.
    */
/*
    handleViewAllClick(event) {

        // Navigate to the Order Products related list page
        // for a specific Purchase Order record.
        this[NavigationMixin.Navigate]({
            type: 'standard__recordRelationshipPage',
            attributes: {
                recordId: this.recordId,
                objectApiName: 'Purchase_Order__c',
                relationshipApiName: 'Order_Products__r',
                actionName: 'view'
            }
        });

    }
*/

    /*
    * @author Jason Flippen
    * @date 03/06/2020
    * @description Method to handle the row action click event.
    */
    handleRowAction(event) {

        var actionName = event.detail.action.name;
        var row = event.detail.row;
        console.log('Stringify Row', JSON.parse(JSON.stringify(row)));
        console.log('actionName', JSON.parse(JSON.stringify(actionName)));
        switch (actionName) {
			case 'delete':
				this.handleDeleteClick(row);
                break;
            case 'remove':
                this.handleRemovePartClick(row);
                break;
            case 'edit':
                this.openEditModal = true;
                this.recordToEdit = row;
                break;
			default:
		}

    }


/** Click Event Handler Methods **/


    /*
    * @author Jason Flippen
    * @date 03/06/2020
    * @description Method to handle the "Delete" click event.
    */
    handleDeleteClick(row) {

        console.log(row.id);

        this.showSpinner = true;

        // Call Apex method to delete the Product from the Purchase Order.
        deleteProduct({ orderItemId: row.id })
        .then((result) => {

            var toastMessage;
            var toastVariant;
            var saveSuccess = false;
            if (result === 'Delete Product Success') {
                saveSuccess = true;
                if (this.purchaseOrder.costPurchaseOrder === true) {
                    toastMessage = 'Part has been Deleted';
                }
                else {
                    toastMessage = 'Product has been Deleted';
                }
                toastVariant = 'success';
            }
            else  {
                toastMessage = result;
                toastVariant = 'error';
            }

            this.showSpinner = false;

            const toastEvent = new ShowToastEvent({
                message: toastMessage,
                variant: toastVariant,
            });
            this.dispatchEvent(toastEvent);
            
            if (saveSuccess === true) {
                
//                // Use the provisioned value to refresh wiredGetOrderItemList.
//                return refreshApex(this.wiredOrderItemResult);

                // Workaround way to refresh the record page.
                updateRecord({ fields: { Id: this.recordId } });

                // Fire event to refresh the cache.
                fireEvent(this.pageRef, 'updatePOEvent', '');

            }
            
        })
        .catch((error) => {
            console.log('deleteProduct Error', error);
            this.showSpinner = false;
        });
        
    }

    /*
    * @author Jason Flippen
    * @date 06/28/2020
    * @description Method to display the Add Parts (Cost PO) or Add Products modal window.
    */
    handleAddProductsClick(event) {

        if (this.purchaseOrder.costPurchaseOrder === true) {
            this.showAddPartsModal = true;
        }
        else {
            this.showAddProductsModal = true;
        }
        
    }

    /*
    * @author Jason Flippen
    * @date 06/28/2020
    * @description Method to close the New Parts modal window.
    */
    handleAddPartsCancelClick(event) {
        
        // Close the modal window and clear the New Part List variable.
        this.showAddPartsModal = false;
        this.disableAddPartsSaveButton = true;
        this.newPartList = [];
    }

    /*
    * @auther Jason Flippen
    * @date 08/10/2020
    * @description Method to add Part to newPartList
    */
    handleAddPartClick(event) {

        if (this.newPart.id) {
            fetchPartDetail( { partId: this.newPart.id, pricebookId: this.purchaseOrder.orderPricebookId })
            .then(result => {
                console.log('Result', JSON.parse(JSON.stringify(result)));
                const newPartDetail = JSON.parse(JSON.stringify(result));
                console.log('New Part Detail', newPartDetail);

                // Update our New Part List variable.
                this.newPartList = [...this.newPartList, {id: this.newPart.id,
                                                          description: newPartDetail.description,
                                                          name: newPartDetail.name,
                                                          pricebookEntryId: newPartDetail.pricebookEntryId,
                                                          quantity: this.newPart.quantity}];
                
                // Reset the data-entry fields.
                this.newPart.id = '';
                this.newPart.quantity = '0';
                const inputFields = this.template.querySelectorAll('lightning-input-field');
                if (inputFields) {
                    inputFields.forEach(field => {
                        field.reset();
                    });
                }

                this.enableAddPartsSaveButton()

            })
            .catch(error => {
                console.log('handleAddPartClick Error', error);
            })

        }

    }

    /*
    * @author Jason Flippen
    * @date 08/11/2020
    * @description Method to handle the Part "Remove" click event.
    */
    handleRemovePartClick(row) {

        const partId = row.id;
        console.log('Part Id', partId);

        // Grab a copy of the newPartList array and clear it.
        var originalPartList = this.newPartList;
        this.newPartList = [];

        // Iterate through the "original" Part List and
        // build an "updated" List of Parts to be added
        // back to the newPartList variable.
        var updatedPartList = [];
        originalPartList.forEach(function(item, index) {
            var part = JSON.parse(JSON.stringify(item));
            if (part.id !== partId) {
                updatedPartList.push(part);
            }
        })
        console.log('updatedPartList', updatedPartList);

        this.newPartList = updatedPartList;

        this.enableAddPartsSaveButton();

    }

    /*
    * @author Jason Flippen
    * @date 08/06/2020
    * @description Method to handle the click event of the Add Parts "Save" button.
    */
    handleAddPartsSaveClick(event) {

        this.disableAddPartsSaveButton = true;
        this.disableAddPartsCancelButton = true;
        this.showSpinner = true;

        saveParts({ newPartList: this.newPartList,
                    purchaseOrderId: this.purchaseOrder.id,
                    orderId: this.purchaseOrder.orderId })
        .then(result => {

            var saveSuccess = false;
            if (result === 'Add Parts Success') {
                saveSuccess = true;
                this.handleAddPartsSaveSuccess();
            }
            else  {
                this.handleAddPartsSaveFailure(result);
            }

            this.showAddPartsModal = false;
            this.newPartList = [];

            if (saveSuccess === true) {

                // Workaround way to refresh the record page.
                updateRecord({ fields: { Id: this.recordId } });

                // Fire event to refresh the cache.
                fireEvent(this.pageRef, 'updatePOEvent', '');

            }

        })
        .catch(error => {
            console.log('Error in saveParts callback', error);
            this.error = error;
        })

    }

    /*
    * @author Jason Flippen
    * @date 08/31/2020
    * @description Method to close the Add Products modal window.
    */
    handleAddProductsCancelClick(event) {
        
        // Close the modal window and clear the Selected Product List variable.
        this.showAddProductsModal = false;
        this.disableAddProductsSaveButton = true;
        this.selectedProductList = [];
    }

    /*
    * @author Jason Flippen
    * @date 06/29/2020
    * @description Method refresh the List of Rows that have been selected. 
    */
    handleRowSelectionClick(event) {
        console.log('Event Detail Selected Rows', event.detail.selectedRows);
        this.selectedProductList = event.detail.selectedRows;
        this.enableAddProductsSaveButton();
    }

    /*
    * @author Jason Flippen
    * @date 08/31/2020
    * @description Method to handle the click event of the Add Products "Save" button.
    */
    handleAddProductsSaveClick(event) {

        this.disableAddProductsSaveButton = true;
        this.disableAddProductsCancelButton = true;
        this.showSpinner = true;

        saveProducts({ purchaseOrder: this.purchaseOrder, chargeWrapperList: this.selectedProductList })
        .then(result => {

            var saveSuccess = false;
            if (result === 'Add Products Success') {
                saveSuccess = true;
                this.handleAddProductsSaveSuccess();
            }
            else  {
                this.handleAddProductsSaveFailure(result);
            }

            this.showAddProductsModal = false;
            this.selectedProductList = [];

            if (saveSuccess === true) {

                // Workaround way to refresh the record page.
                updateRecord({ fields: { Id: this.recordId } });

                // Fire event to refresh the cache.
                fireEvent(this.pageRef, 'updatePOEvent', '');

            }

        })
        .catch(error => {
            console.log('Error in saveProducts callback', error);
            this.error = error;
        })

    }


/** Change Event Handler Methods **/


    /*
    * @auther Jason Flippen
    * @date 08/10/2020
    * @description Method to handle the Part "Change" event
    */
    handlePartChange(event) {

        console.log('Part Id', event.detail.value[0]);
        this.newPart.id = '';
        if (event.detail.value[0]) {
            this.newPart.id = event.detail.value[0];
        }
        console.log('newPart Id', this.newPart.id);

    }

    /*
    * @auther Jason Flippen
    * @date 08/10/2020
    * @description Method to handle the Part Quantity "Change" event
    */
    handlePartQuantityChange(event) {

        console.log('Quantity', event.detail.value);
        this.newPart.quantity = 0;
        if (event.detail.value) {
            this.newPart.quantity = event.detail.value;
        }
        console.log('newPart quantity', this.newPart.quantity);

    }


/** Miscellaneous Methods **/


    /*
    * @author Jason Flippen
    * @date 08/10/2020
    * @description Method to set the "disableAddPartsSaveButton" flag.
    */
    enableAddPartsSaveButton() {

        if (this.purchaseOrder.costPurchaseOrder === true && this.newPartList.length > 0) {
            this.disableAddPartsSaveButton = false
            this.noPartsAdded = false;
        }
        else {
            this.disableAddPartsSaveButton = true;
            this.noPartsAdded = true;
        }

    }

    /*
    * @author Jason Flippen
    * @date 08/06/2020
    * @description Method to handle the successful addition of new Parts.
    */
    handleAddPartsSaveSuccess() {

        this.disableAddPartsSaveButton = false;
        this.disableAddPartsCancelButton = false;
        this.showSpinner = false;

        // Notify the User that the new Parts were saved successfully.
        const toastEvent = new ShowToastEvent({
            message: 'New Parts Added',
            variant: 'success',
            mode: 'pester'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 06/28/2020
    * @description Method to handle the new Parts save failure.
    */
    handleAddPartsSaveFailure(errorMessage) {

        this.disableAddPartsSaveButton = false;
        this.disableAddPartsCancelButton = false;
        this.showSpinner = false;

        // Notify the user that the new Parts save failed.
        const toastEvent = new ShowToastEvent({
            title: 'Unable to add New Parts',
            message: errorMessage,
            variant: 'error',
            mode: 'sticky'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 08/31/2020
    * @description Method to set the "disableAddProductsSaveButton" flag.
    */
    enableAddProductsSaveButton() {

        if (this.selectedProductList.length > 0) {
            this.disableAddProductsSaveButton = false
        }
        else {
            this.disableAddProductsSaveButton = true;
        }

    }

    /*
    * @author Jason Flippen
    * @date 08/31/2020
    * @description Method to handle the successful addition of new Products.
    */
    handleAddProductsSaveSuccess() {

        this.disableAddProductsSaveButton = false;
        this.disableAddProductsCancelButton = false;
        this.showSpinner = false;

        // Notify the User that the new Products were saved successfully.
        const toastEvent = new ShowToastEvent({
            message: 'New Products Added',
            variant: 'success',
            mode: 'pester'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 08/31/2020
    * @description Method to handle the new Products save failure.
    */
    handleAddProductsSaveFailure(errorMessage) {

        this.disableAddProductsSaveButton = false;
        this.disableAddProductsCancelButton = false;
        this.showSpinner = false;

        // Notify the user that the new Products save failed.
        const toastEvent = new ShowToastEvent({
            title: 'Unable to add New Products',
            message: errorMessage,
            variant: 'error',
            mode: 'sticky'
        });
        this.dispatchEvent(toastEvent);

    }

    closeModal(){
        this.openEditModal = false;
    }
    
    handleEditRemakes(event){
        console.log('checked? '+ event.target.checked);
        this.recordToEdit.multipleRemakes = event.target.checked;
    }

    submitDetails(){
        console.log('recordToEdit: '+ this.recordToEdit.id)
        console.log('multipleRemakes: '+ this.recordToEdit.multipleRemakes)
        this.openEditModal = false;
        this.showSpinner = true;
        updateRemakeProducts({ ItemId: this.recordToEdit.id, multipleRemakesCheck: this.recordToEdit.multipleRemakes })
        .then(result => {
            console.log(result);
            this.showSpinner = false;
            const toastEvent = new ShowToastEvent({
                message: 'Multiple Remakes Updated',
                variant: 'success'
            });
            this.dispatchEvent(toastEvent);
            this.refreshPOData();
        })
        .catch(error => {
            console.log('Error in saveProducts callback', error);
            this.error = error;
        })
    }
}