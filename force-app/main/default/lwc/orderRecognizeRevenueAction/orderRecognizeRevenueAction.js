/**
* @author Jason Flippen
* @date 01/23/2021 
* @description Provides functionality for the orderRecognizeRevenueAction LWC.
*
*			   Supporting database functionality provided by the following Apex Class:
*			   - OrderRecRevActionController
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/
import { LightningElement, api, wire } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import { updateRecord } from 'lightning/uiRecordApi';
import { registerListener } from 'c/pubsub';
import { fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
import getOrderData from '@salesforce/apex/OrderRecRevActionController.getOrderData';
import updateOrder from '@salesforce/apex/OrderRecRevActionController.updateOrder';

export default class OrderRecognizeRevenueAction extends LightningElement {

    @api recordId;

    wiredOrderResult;
    order;
    displayText = '';

    @wire(CurrentPageReference) pageRef;

    /**
    * @author Jason Flippen
    * @date 01/23/2021
    * @description Event fired when the element is inserted into the document.
    *              It's purpose here is to subscribe the "updateOrderEvent".
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    connectedCallback() {
        registerListener('updateOrderEvent', this.refreshOrderData, this);
    }
    
    /**
    * @author Jason Flippen
    * @date 01/23/2021
    * @description Method to retrieve Order and related Purchase Order data
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @wire(getOrderData, { orderId: '$recordId' })
    wiredGetOrderData(result) {

        // Hold on to the provisioned value so it can be refreshed.
        this.wiredOrderResult = result;
        const { data, error } = result; // destructure the provisioned value
        if (data) {
            console.log('Order Data', JSON.parse(JSON.stringify(data)));
            this.order = JSON.parse(JSON.stringify(data));

            let disableSave = false;

            // Is the Order for revenue recognition?
            if (this.order.recordTypeDeveloperName === 'CORO_Record_Type' &&
                (this.order.status !== 'Job in Progress' && this.order.status !== 'Install Scheduled')) {
                
                // Not eligible due to it being a CORO Order with its Status
                // current set to "Job In Progress" or "Install Scheduled".
                disableSave = true;
                this.displayText = 'Revenue cannot be recognized unless the Order has a status of "Job in Progress" or "Install Scheduled"';

            }
            else if (this.order.recordTypeDeveloperName !== 'Change_Order' &&
                     this.order.hasIneligiblePurchaseOrders === true) {
                
                // Not eligible due to it being a non-Change Order Order with at least one
                // related Purchase Order in a Status other than "Cancelled" or "Received".
                disableSave = true;
                this.displayText = 'Revenue cannot be recognized unless the POs are in a status of "Cancelled" or "Received"';

            }
            else {

                // Order is eligible.

                disableSave = false;
                this.displayText = 'Are you sure you want to recognize revenue on this order?'

            }

            if (disableSave === false) {
                const enableSaveEvent = new CustomEvent('EnableSave');
                this.dispatchEvent(enableSaveEvent);
            }
            else {
                const disableSaveEvent = new CustomEvent('DisableSave');
                this.dispatchEvent(disableSaveEvent);
            }

       }
       else if (error) {
           console.log('error in getOrderData callback', error);
       }

    }

    /**
    * @author Jason Flippen
    * @date 01/23/2021
    * @description Method to update the Status and Revenue Recognized Date of the Order.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @api
    handleSaveClick() {

        const showSpinner = new CustomEvent('ShowSpinner');
        const hideSpinner = new CustomEvent('HideSpinner');
        const closeQuickAction = new CustomEvent('CloseQuickAction');

        this.dispatchEvent(showSpinner);

        // Call Apex method to update the Order.
        updateOrder({ order: this.order })
        .then((result) => {

            let toastMessage;
            let toastType;
            let saveSuccess = false;
            if (result === 'Update Order Success') {
                saveSuccess = true;
                toastMessage = 'Revenue has been recognized';
                toastType = 'success';
            }
            else  {
                toastMessage = result;
                toastType = 'error';
            }

            this.dispatchEvent(hideSpinner);

            const showToastEvent = new CustomEvent('ShowToast', { detail: { message: toastMessage, type: toastType } });
            this.dispatchEvent(showToastEvent);
            this.dispatchEvent(closeQuickAction);

            if (saveSuccess === true) {

                // Workaround way to refresh the record page.
                updateRecord({ fields: { Id: this.recordId } });

                // Fire event to refresh the cache.
                fireEvent(this.pageRef, 'updateOrderEvent', '');

            }

        })
        .catch((error) => {
            console.log('Update Order Error', error);
            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(this.closeQuickAction);
        });

    }

    /**
    * @author Jason Flippen
    * @date 01/23/2021
    * @description Method to refresh the cache. 
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    refreshOrderData() {
        console.log('Refreshing orderRecognizeRevenueAction');
        return refreshApex(this.wiredOrderResult);
    }

}