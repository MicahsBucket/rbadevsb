import { LightningElement,track,api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import ACCOUNT_OBJECT from '@salesforce/schema/Lead_Time__c';
import NAME_FIELD from '@salesforce/schema/Lead_Time__c.CS_Closed_Through__c';
import WEBSITE_FIELD from '@salesforce/schema/Lead_Time__c.CS_LeadTime_Days__c';
import COMMENTS_FIELD from '@salesforce/schema/Lead_Time__c.Lead_Time_Comments__c';
export default class LeadTimeNewRecord extends NavigationMixin(LightningElement) {
    @api
    url;

    accountObject = ACCOUNT_OBJECT;
    nameField = NAME_FIELD;
    websiteField = WEBSITE_FIELD;
                 commentsField   =COMMENTS_FIELD;

    handleLeadTimeCreated(){
        this.dispatchEvent(
            // Default values for bubbles and composed are false.
            new CustomEvent('leadtimecreation')
        );
    }

    connectedCallback() {
        // Store the PageReference in a variable to use in handleClick.
        // This is a plain Javascript object that conforms to the
        // PageReference type by including "type" and "attributes" properties.
        // The "state" property is optional.
       
    }

    gotoListView(evt) {
        // eslint-disable-next-line no-console
        console.log('the url'+this.url);
        window.open(this.url,"_self");
    }

}