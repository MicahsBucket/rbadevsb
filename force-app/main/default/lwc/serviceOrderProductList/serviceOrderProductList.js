import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import { registerListener } from 'c/pubsub';
import { fireEvent } from 'c/pubsub';
import getServiceOrderProductList from '@salesforce/apex/ServiceOrderProductListCtlr.getServiceOrderProductList';
import getOrder from '@salesforce/apex/ServiceOrderProductListCtlr.getOrder';
import getAssetsWithProducts from '@salesforce/apex/ServiceOrderProductListCtlr.getAssetsWithProducts';
import updateNotes from '@salesforce/apex/ServiceOrderProductListCtlr.updateNotes';
//import updateVariantNumber from '@salesforce/apex/ServiceOrderProductListCtlr.updateVariantNumber';
import updateOrderProduct from '@salesforce/apex/ServiceOrderProductListCtlr.updateOrderProduct';
import cancelProduct from '@salesforce/apex/ServiceOrderProductListCtlr.cancelProduct';
import getMTOCodes from '@salesforce/apex/ServiceOrderProductListCtlr.getMTOCodes';
import getDefectCodes from '@salesforce/apex/ServiceOrderProductListCtlr.getDefectCodes';
import saveEditProduct from '@salesforce/apex/ServiceOrderProductListCtlr.saveEditProduct';
import refreshData from '@salesforce/apex/ServiceOrderProductListCtlr.refreshData';

const actions = [
//    { label: 'Edit Variant', name: 'editVariant' },
    { label: 'View/Edit Notes', name: 'editNotes' },
    { label: 'Update Quote', name: 'updateQuote' },
    { label: 'Edit Product', name: 'editProduct' },
    { label: 'Cancel Product', name: 'cancelProduct' },
];

const columns = [
//    { label: 'Service Product', fieldName: 'serviceProductUrl', type: 'url', initialWidth: 135, typeAttributes: { 'label': { fieldName: 'serviceProductNumber' }, 'target': '_top', 'tooltip':  { fieldName: 'serviceProductNumber' } } },
    { label: 'Service Product', fieldName: 'serviceProductNumber', type: 'string', initialWidth: 140, wrapText: true, cellAttributes: { alignment: 'left' } },
//    { label: 'Component', fieldName: 'componentUrl', cellAttributes: { alignment: 'left' }, type: 'url', initialWidth: 112, typeAttributes: { label: { fieldName: 'productName' }, target: '_top', tooltip: { fieldName: 'productName' } } },
    { label: 'Component', fieldName: 'productName', type: 'string', initialWidth: 120, wrapText: true, cellAttributes: { alignment: 'left' } },
//    { label: 'Installed Product', fieldName: 'assetUrl', cellAttributes: { alignment: 'left' }, type: 'url', initialWidth: 145, typeAttributes: { label: { fieldName: 'soldOrderProductAssetName' }, target: '_top', tooltip: { fieldName: 'soldOrderProductAssetName' } } },
    { label: 'Installed Product', fieldName: 'soldOrderProductAssetName', type: 'string', initialWidth: 150, wrapText: true, cellAttributes: { alignment: 'left'} },
    { label: 'Variant Number', fieldName: 'variantNumber', type: 'string', initialWidth: 145, wrapText: true, cellAttributes: { alignment: 'left' } },
    { label: 'Charge To', fieldName: 'chargeCostTo', cellAttributes: { alignment: 'left'}, type: 'string', initialWidth: 116 },
//    { label: 'Purchase Order', fieldName: 'purchaseOrderUrl', cellAttributes: { alignment: 'left' }, type: 'url', initialWidth: 132 , typeAttributes: { label: { fieldName: 'purchaseOrderName' }, target: '_top', tooltip: { fieldName: 'purchaseOrderName' } } },
    { label: 'Purchase Order', fieldName: 'purchaseOrderName', type: 'string', initialWidth: 140, cellAttributes: { alignment: 'left' } },
    { label: 'Reimbursement', fieldName: 'reimbursementCompleted', cellAttributes: { alignment: 'left' }, type: 'boolean' },
    {
        type: 'action',
        typeAttributes: { rowActions: actions, menuAlignment: 'Auto' },
    }
];

const ResponsibilityOptions = [
    {label: 'Customer', value: 'Customer'},
    {label: 'Retailer', value: 'Retailer'},
    {label: 'Manufacturing', value: 'Manufacturing'}
]

export default class serviceOrderProductList extends NavigationMixin(LightningElement) {

    @api recordId;
    @api createServiceRequest;
    @api createServiceRequestLegacyAssest;


    wiredOrderResult;
    wiredServiceOrderProductListResult;
    @track showSpinner = false;
    @track accountId;
    @track orderId;
//    @track newData;
    @track num_assets;
    @track mainAccountId;
    @track mainOrderId;
    @track mainOrderStatus;
    @track mainStoreId;
    @track rowId;
    @track recordCount;
    @track storeId;

	@track columns = columns;
    @track data = [];
    @track productData = [];
    @track error;
    @track showData = false;
/*
    @track showEditVariantModal = false;
    @track disableEditVariantCancelButton = false;
    @track disableEditVariantSaveButton = false;
    @track soldOrderProductAssetId;
    @track variantNumber;
*/
    @track showEditNotesModal = false;
    @track disableEditNotesCancelButton = false;
    @track disableEditNotesSaveButton = false;
    @track notes;
    @track showEditProductModal = false;
    @track disableEditProductCancelButton = false;
    @track disableEditProductSaveButton = false;
    @track updatedOrderProduct = {
        Id: '',
        Billable_Amount__c: 0,
        Customer_Pickup__c: '',
        Do_Not_Print_Service_Product_on_Invoice__c: false,
        Do_Not_Print_Service_Product_on_Quote__c: false,
        Quote_Accepted__c: false,
        Quote_Amount__c: 0
    };
    @track showCancelProductModal = false;
    @track disableCancelProductCancelButton = false;
    @track disableCancelProductSaveButton = false;
    @track selectedOrderProduct;
    @track showUpdateProductModal = false;
    @track MTOcodeOptions;
    @track ResponsibilityOptions = [
        {label: 'Customer', value: 'Customer'},
        {label: 'Retailer', value: 'Retailer'},
        {label: 'Manufacturing', value: 'Manufacturing'}
    ]
    @track newMTOCode;
    @track newResponsibility ;
    @track showMTOOptions = false;
    @track showDefectOptions = false;
    @track DefectCodeOptions;
    @track hideSaveButton = true;
    @track rowToUpdate;
    @track dynamicHeight;
    @track DefectCodeLabel;
    @track MTOCodeLabel;
    
    assetIdList = '';

    @wire(CurrentPageReference) pageRef;

    connectedCallback() {
        registerListener('updateOrderEvent', this.refreshOrderData, this);
    }
   
    @wire(getOrder, { orderId: '$recordId' })
    wiredGetOrder(result) {

        // Hold on to the provisioned value so it can be refreshed.
        this.wiredOrderResult = result;
        const { data, error } = result; // destructure the provisioned value
        if (data) {
            console.log('first data: ', data);
			console.log('data', JSON.parse(JSON.stringify(data)));
            this.data = JSON.parse(JSON.stringify(data));
            this.mainAccountId = data[0].mainAccountId;
            this.mainOrderId = data[0].mainOrderId;
            this.mainOrderStatus = data[0].mainOrderStatus;
            this.mainStoreId = data[0].mainStoreId;
            console.log('Main Store Id: ', this.mainStoreId);
        }
        else if (error) {
            this.error = error;
            console.log('Error in GetOrder callback', error);
        }

    }
    
    /*
    * @author Jason Flippen
    * @date 09/30/2020
    * @description Method to retrieve a List of (Service) Order Products. 
    */
    @wire(getServiceOrderProductList, { orderId: '$recordId' })
    wiredGetServiceOrderProductList(result) {

        // Hold on to the provisioned value so it can be refreshed.
        this.wiredServiceOrderProductListResult = result;
        const { data, error } = result; // destructure the provisioned value
        if (data) {
            console.log('Service Products first data: ', data);
            console.log('Service Products data', JSON.parse(JSON.stringify(data)));
            this.productData = JSON.parse(JSON.stringify(data));
            this.recordCount = 'Service Products (' + data.length + ')';
            if(data.length>0)
            {
                this.showData = true;
                this.num_assets = data[0].num_assets;
                this.orderId = data[0].orderId;
                this.accountId = data[0].accountId;
                this.storeId = data[0].storeId;
            }
            console.log('First Num-Assets: ', this.num_assets);
            console.log('First Store ID: ', this.storeId);
            this.getAssets();
        }
        else if (error) {
            this.error = error;
            console.log('error in wiredGetServiceOrderProductList callback', error);
        }

   }

    getAssets() {

        getAssetsWithProducts({ orderId: this.recordId })
        .then(result => {
            console.log('assets first data: ', result);
            console.log('assets data', JSON.parse(JSON.stringify(result)));
            this.result = JSON.parse(JSON.stringify(result));
            for(var key in this.result) {
                console.log('In for loop: ', key);
                console.log('Data: ', JSON.parse(JSON.stringify(this.result[key])));
                const record = JSON.parse(JSON.stringify(this.result[key]));
                console.log('Record: ', record.assetId);
                if (this.assetIdList !== "") {
                    this.assetIdList += '#';
                }
                this.assetIdList += record.assetId;
                console.log('Asset Id List: ', this.assetIdList);
                //this.wiredServiceOrderProductList({error, data});
            }
        })
        .catch(error => {
            console.log('Assets');
            if (error) {
                this.error = error;
                console.log('error in callback', error);
            }
            //this.productData = this.data.split('#')[0];
            //console.log('Product Data', this.productData);
        })

    }

    handleRowAction(event) {

        console.log('In handleRowAction');
        
		const actionName = event.detail.action.name;
		const row = event.detail.row;
        console.log('stringify row', JSON.parse(JSON.stringify(row)));
        this.selectedOrderProduct = row;
/*
        this.soldOrderProductAssetId = row.soldOrderProductAssetId
        this.variantNumber = row.variantNumber;
*/
        this.notes = row.notes;
        this.updatedOrderProduct.Id = row.id;
        this.updatedOrderProduct.Billable_Amount__c = row.billableAmount;
        this.updatedOrderProduct.Customer_Pickup__c = row.customerPickup;
        this.updatedOrderProduct.Do_Not_Print_Service_Product_on_Invoice__c = row.doNotPrintServiceProductOnInvoice;
        this.updatedOrderProduct.Do_Not_Print_Service_Product_on_Quote__c = row.doNotPrintServiceProductOnQuote;
        this.updatedOrderProduct.Quote_Accepted__c = row.quoteAccepted;
        this.updatedOrderProduct.Quote_Amount__c = row.quoteAmount;
        this.rowId = row.id;
        
        switch (actionName) {
            case 'editNotes':
                this.handleEditNotesClick(row);
                break;
			case 'editProduct':
				this.handleUpdateProductClick(row);
                break;
            case 'updateQuote':
                this.handleEditProductClick(row);
                break;

            case 'cancelProduct':
                this.handleCancelProductClick(row);
                break;    
			default:
        }   

    }


/** Click Events **/


    /*
    * @author Jason Flippen
    * @date 10/01/2020
    * @description Method to show the Edit Notes modal window.
    */
	handleEditNotesClick(row) {

        console.log('In handleEditNotesClick');
        console.log('Locked Service Request: ', row.lockedServiceRequest);
        
        this.refreshServiceOrderProductData();

        if (row.lockedServiceRequest === false) {
            this.showEditNotesModal = true;
        }
        else {
            
            const toastEvent = new ShowToastEvent({
                title: 'Notes cannot be edited',
                variant: 'info',
                mode: 'pester'
            });
            this.dispatchEvent(toastEvent);  
            
        }

    }

    /*
    * @author Jason Flippen
    * @date 09/28/2020
    * @description Method to show the Edit Variant modal window.
    */
/*
	handleEditVariantClick(row) {

        console.log('In handleEditVariantClick');
        console.log('Locked Service Request: ', row.lockedServiceRequest);
        
//        if (row.lockedServiceRequest === false) {
//		    // Navigate to the appropriate read-only Product page.
//            window.open('/apex/RMS_editServiceProduct?id=' + row.id + '&mode=e');
//        } else {
//            alert('Record cannot be edited.');
//        }

        if (row.lockedServiceRequest === false) {
            this.showEditVariantModal = true;
        }
        else {
            
            const toastEvent = new ShowToastEvent({
                title: 'Variant Number cannot be edited',
                variant: 'info',
                mode: 'pester'
            });
            this.dispatchEvent(toastEvent);  
            
        }

    }
*/

    /*
    * @author Jason Flippen
    * @date 02/24/2021
    * @description Method to show the Edit Product modal window.
    */
	handleEditProductClick(row) {

        console.log('In handleEditProductClick');
        console.log('Locked Service Request: ', row.lockedServiceRequest);

        this.refreshServiceOrderProductData();

        this.updatedOrderProduct.Id = row.id;
        
        if (row.lockedServiceRequest === false) {
            this.showEditProductModal = true;
        }
        else {
            
            const toastEvent = new ShowToastEvent({
                title: 'Product cannot be edited',
                variant: 'info',
                mode: 'pester'
            });
            this.dispatchEvent(toastEvent);  
            
        }

    }

	handleCancelProductClick(row) {

        console.log('In handleCancelProductClick');

/*
		// Navigate to the appropriate read-only Product page.
		if (row.status !== 'Cancelled') {
			window.open('/apex/RMS_CancelOrderLine?id=' + row.id);
		}
		else {
			alert('Order Item has been cancelled.');
        }
*/

        if (row.status !== 'Cancelled') {
            this.showCancelProductModal = true;
        }
        else {
            
            const toastEvent = new ShowToastEvent({
                title: 'Product has already been cancelled',
                variant: 'info',
                mode: 'pester'
            });
            this.dispatchEvent(toastEvent);  
            
        }

        
    }

    handleAddProductClick() {
        // You cannot add Products if the Order is in an "end of life" status.
        if (this.mainOrderStatus === 'Closed' ||
            this.mainOrderStatus === 'Job Closed' ||
            this.mainOrderStatus === 'Warranty Accepted' ||
            this.mainOrderStatus === 'Warranty Rejected' ||
            this.mainOrderStatus === 'Warranty Submitted') {
            // Notify the User that a Product cannot be added to this Service Request (aka Order).
            const toastEvent = new ShowToastEvent({
                message: 'Products cannot be added to Orders in end of life statuses',
                variant: 'info',
                mode: 'pester'
            });
            this.dispatchEvent(toastEvent);
        }
        else {
            console.log('Number of assets:', this.num_assets);
            console.log('Order Id:', this.orderId);
            if (this.assetIdList.length > 0) {
                if (this.createServiceRequest == null){
                    this[NavigationMixin.Navigate]({
                        type: 'standard__component',
                        attributes: {
                            componentName: 'c__CreateServiceRequestWrapper'
                        },
                        state: {
                            c__aid: this.mainAccountId,
                            c__slid: this.mainStoreId,
                            c__oid: this.mainOrderId,
                            c__isServiceRecordCreated: false,
                            c__currentStep: "3",
                            c__showProductListing: true,
                            c__assetId: this.assetIdList,
                            c__isChecked: "utility:check",
                            c__isDataSelected: true
                        }
                    });
                }
                else{
                    sessionStorage.setItem("soplc--accountId", this.mainAccountId);
                    sessionStorage.setItem('soplc--storeId', this.mainStoreId);
                    sessionStorage.setItem('soplc--orderId', this.mainOrderId);
                    sessionStorage.setItem('soplc--isServiceRecordCreated', false);
                    sessionStorage.setItem('soplc--currentStep', "3");
                    sessionStorage.setItem('soplc--showProductListing', true);
                    this[NavigationMixin.Navigate]({
                        type: 'standard__recordPage',
                        attributes: {
                            recordId: this.mainOrderId,
                            objectApiName: 'Order',
                            actionName: 'view'
                        },
                    }); 
                }
            }
            else {
                if(this.createServiceRequestLegacyAssest == null){
                    this[NavigationMixin.Navigate]({
                        type: 'standard__component',
                        attributes: {
                             componentName: 'c__AddLegacyAssetWrapper'
                        },
                        state: {
                            c__aid: this.mainAccountId
                        }
                    });
                }
                else{
                    sessionStorage.setItem("soplc--accountId", this.mainAccountId);
                    this[NavigationMixin.Navigate]({
                        type: 'comm__namedPage',
                        attributes: {
                            name: this.createServiceRequestLegacyAssest
                        }
                    });
                }              
            }
        }
    }

    /*
    * @author Jason Flippen
    * @date 10/01/2020
    * @description Method to close the Edit Notes modal window.
    */
    handleCancelEditNotesClick(event) {
        this.showEditNotesModal = false;
    }

    /*
    * @author Jason Flippen
    * @date 10/01/2020
    * @description Method to save the Notes changes.
    */
    handleSaveNotesClick(event) {
        console.log(event.detail);
        console.log('this.rowId= ', this.rowId);
        console.log('this.notes= ', this.notes);
        this.disableEditNotesCancelButton = true;
        this.disableEditNotesSaveButton = true;
        this.showSpinner = true;

        updateNotes({ orderItemId: this.rowId, notes: this.notes })
        .then(result => {

            var returnValue = result;

            var saveSuccess = false;
            if (returnValue === 'Update Notes Success') {
                saveSuccess = true;
            }

            this.showEditNotesModal = false;

            if (saveSuccess === true) {

                this.handleSaveNotesSuccess();

                // Fire event to refresh the cache.
                fireEvent(this.pageRef, 'updateOrderEvent', '');

                this.refreshServiceOrderProductData();
//                return refreshApex(this.wiredServiceOrderProductListResult);

            }
            else if (saveSuccess === false) {

                this.handleSaveNotesFailure(returnValue);

            }

        })
        .catch(error => {
            console.log('Error in updateNotes callback', error);
            this.error = error;
        })

    }

    /*
    * @author Jason Flippen
    * @date 09/28/2020
    * @description Method to close the Edit Variant modal window.
    */
/*
    handleCancelEditVariantClick(event) {
        this.showEditVariantModal = false;
    }
*/

    /*
    * @author Jason Flippen
    * @date 09/28/2020
    * @description Method to save the Variant Number update.
    */
/*
    handleSaveVariantClick(event) {

        this.disableEditVariantCancelButton = true;
        this.disableEditVariantSaveButton = true;
        this.showSpinner = true;

        updateVariantNumber({ assetId: this.soldOrderProductAssetId, variantNumber: this.variantNumber })
        .then(result => {

            var returnValue = result;

            var saveSuccess = false;
            if (returnValue === 'Update Variant Number Success') {
                saveSuccess = true;
            }

            this.showEditVariantModal = false;

            if (saveSuccess === true) {

                this.handleSaveVariantSuccess();

                return refreshApex(this.wiredServiceOrderProductListResult);

            }
            else if (saveSuccess === false) {

                this.handleSaveVariantFailure(returnValue);

            }

        })
        .catch(error => {
            console.log('Error in updateVariantNumber callback', error);
            this.error = error;
        })

    }
*/

    /*
    * @author Jason Flippen
    * @date 02/24/2021
    * @description Method to close the Edit Product modal window.
    */
    handleCancelEditProductClick(event) {
        this.showEditProductModal = false;
    }

    /*
    * @author Jason Flippen
    * @date 02/24/2021
    * @description Method to Save the Product updates.
    */
    handleSaveEditProductClick(event) {

        this.disableEditProductCancelButton = true;
        this.disableEditProductSaveButton = true;
        this.showSpinner = true;

        updateOrderProduct({ orderProduct: this.updatedOrderProduct })
        .then(result => {

            var returnValue = result;

            var saveSuccess = false;
            if (returnValue === 'Update Product Success') {
                saveSuccess = true;
            }

            this.showEditProductModal = false;

            if (saveSuccess === true) {

                this.handleSaveProductSuccess();

                // Fire event to refresh the cache.
                fireEvent(this.pageRef, 'updateOrderEvent', '');

                this.refreshServiceOrderProductData();
//                return refreshApex(this.wiredServiceOrderProductListResult);

            }
            else if (saveSuccess === false) {

                this.handleSaveProductFailure(returnValue);

            }

        })
        .catch(error => {
            console.log('Error in updateOrderProduct callback', error);
            this.error = error;
        })

    }

    /*
    * @author Jason Flippen
    * @date 10/05/2020
    * @description Method to close the Cancel Product modal window.
    */
    handleCancelCancelProductClick(event) {
        this.showCancelProductModal = false;
    }

    /*
    * @author Jason Flippen
    * @date 10/05/2020
    * @description Method to execute the Product cancellation.
    */
    handleSaveCancelProductClick(event) {

        this.disableCancelProductCancelButton = true;
        this.disableCancelProductSaveButton = true;
        this.showSpinner = true;

        cancelProduct({ orderItem: this.selectedOrderProduct })
        .then(result => {

            var returnValue = result;

            var saveSuccess = false;
            if (returnValue === 'Cancel Product Success') {
                saveSuccess = true;
            }

            this.showCancelProductModal = false;

            if (saveSuccess === true) {

                this.handleCancelProductSuccess();

                // Fire event to refresh the cache.
                fireEvent(this.pageRef, 'updateOrderEvent', '');

                this.refreshServiceOrderProductData();
//                return refreshApex(this.wiredServiceOrderProductListResult);

            }
            else if (saveSuccess === false) {

                this.handleCancelProductFailure(returnValue);

            }

        })
        .catch(error => {
            console.log('Error in cancelProduct callback', error);
            this.error = error;
        })

    }


/** Change Events **/


    /*
    * @author Jason Flippen
    * @date 10/01/2020
    * @description Method to handle the changed Notes from the Edit Service Product screen.
    */
    handleNotesChange(event) {
        var newNotes = event.target.value;
        this.notes = newNotes;
    }

    /*
    * @author Jason Flippen
    * @date 09/28/2020
    * @description Method to handle the changed Variant Number
    *              from the Edit Service Product screen.
    */
/*
    handleVariantNumberChange(event) {
        var newVariantNumber = event.target.value;
        this.variantNumber = newVariantNumber;
    }
*/

    /*
    * @author Jason Flippen
    * @date 02/24/2021
    * @description Method to handle the changed Billable Amount value from the Edit Product window.
    */
    handleBillableAmountChange(event) {
        const newBillableAmount = event.target.value;
        this.updatedOrderProduct.Billable_Amount__c = newBillableAmount;
    }

    /*
    * @author Jason Flippen
    * @date 02/24/2021
    * @description Method to handle the changed Customer Pickup value from the Edit Product window.
    */
    handleCustomerPickupChange(event) {
        const newCustomerPickup = event.target.value;
        this.updatedOrderProduct.Customer_Pickup__c = newCustomerPickup;
    }

    /*
    * @author Jason Flippen
    * @date 02/24/2021
    * @description Method to handle the changed Do Not Print Service Product on Invoice value from the Edit Product window.
    */
    handleDoNotPrintServiceProductOnInvoiceChange(event) {
        const newDoNotPrintServiceProductOnInvoice = event.target.value;
        this.updatedOrderProduct.Do_Not_Print_Service_Product_on_Invoice__c = newDoNotPrintServiceProductOnInvoice;
    }

    /*
    * @author Jason Flippen
    * @date 02/24/2021
    * @description Method to handle the changed Do Not Print Service Product on Quote value from the Edit Product window.
    */
    handleDoNotPrintServiceProductOnQuoteChange(event) {
        const newDoNotPrintServiceProductOnQuote = event.target.value;
        this.updatedOrderProduct.Do_Not_Print_Service_Product_on_Quote__c = newDoNotPrintServiceProductOnQuote;
    }

    /*
    * @author Jason Flippen
    * @date 02/24/2021
    * @description Method to handle the changed Quote Accepted value from the Edit Product window.
    */
    handleQuoteAcceptedChange(event) {
        const newQuoteAccepted = event.target.value;
        this.updatedOrderProduct.Quote_Accepted__c = newQuoteAccepted;
    }

    /*
    * @author Jason Flippen
    * @date 02/24/2021
    * @description Method to handle the changed Quote Amount value from the Edit Product window.
    */
    handleQuoteAmountChange(event) {
        const newQuoteAmount = event.target.value;
        this.updatedOrderProduct.Quote_Amount__c = newQuoteAmount;
    }


/** Miscellaneous Methods **/


    /*
    * @author Jason Flippen
    * @date 10/01/2020
    * @description Method to handle the successful Notes update.
    */
    handleSaveNotesSuccess() {

        this.disableEditNotesCancelButton = false;
        this.disableEditNotesSaveButton = false;
        this.showSpinner = false;

        // Notify the User that the Notes were updated successfully.
        const toastEvent = new ShowToastEvent({
            message: 'Notes updated sucessfully',
            variant: 'success',
            mode: 'pester'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 10/01/2020
    * @description Method to handle the Notes update failure.
    */
    handleSaveNotesFailure(errorMessage) {

        this.disableEditNotesCancelButton = false;
        this.disableEditNotesSaveButton = false;
        this.showSpinner = false;

        // Notify the user that the Notes update failed.
        const toastEvent = new ShowToastEvent({
            title: 'Unable to update Notes',
            message: errorMessage,
            variant: 'error',
            mode: 'sticky'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 09/28/2020
    * @description Method to handle the successful Variant Number update.
    */
/*
    handleSaveVariantSuccess() {

        this.disableEditVariantCancelButton = false;
        this.disableEditVariantSaveButton = false;
        this.showSpinner = false;

        // Notify the User that the Variant Number was updated successfully.
        const toastEvent = new ShowToastEvent({
            message: 'Variant Number updated sucessfully',
            variant: 'success',
            mode: 'pester'
        });
        this.dispatchEvent(toastEvent);

    }
*/

    /*
    * @author Jason Flippen
    * @date 09/28/2020
    * @description Method to handle the Variant Number update failure.
    */
/*
    handleSaveVariantFailure(errorMessage) {

        this.disableEditVariantCancelButton = false;
        this.disableEditVariantSaveButton = false;
        this.showSpinner = false;

        // Notify the user that the Variant Number update failed.
        const toastEvent = new ShowToastEvent({
            title: 'Unable to update Variant Number',
            message: errorMessage,
            variant: 'error',
            mode: 'sticky'
        });
        this.dispatchEvent(toastEvent);

    }
*/

    /*
    * @author Jason Flippen
    * @date 02/24/2021
    * @description Method to handle the successful Product update.
    */
    handleSaveProductSuccess() {

        this.disableEditProductCancelButton = false;
        this.disableEditProductSaveButton = false;
        this.showSpinner = false;

        // Notify the User that the Product was updated successfully.
        const toastEvent = new ShowToastEvent({
            message: 'Product updated sucessfully',
            variant: 'success',
            mode: 'pester'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 02/24/2021
    * @description Method to handle the Product update failure.
    */
    handleSaveProductFailure(errorMessage) {

        this.disableEditProductCancelButton = false;
        this.disableEditProductSaveButton = false;
        this.showSpinner = false;

        // Notify the user that the Product update failed.
        const toastEvent = new ShowToastEvent({
            title: 'Unable to update Product',
            message: errorMessage,
            variant: 'error',
            mode: 'sticky'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 10/05/2020
    * @description Method to handle the successful Product cancellation.
    */
    handleCancelProductSuccess() {

        this.disableCancelProductCancelButton = false;
        this.disableCancelProductSaveButton = false;
        this.showSpinner = false;

        // Notify the User that Product was cancelled successfully.
        const toastEvent = new ShowToastEvent({
            message: 'Product has been cancelled',
            variant: 'success',
            mode: 'pester'
        });
        this.dispatchEvent(toastEvent);

    }

    /*
    * @author Jason Flippen
    * @date 10/05/2020
    * @description Method to handle the Product cancellation failure.
    */
    handleSaveCancelProductFailure(errorMessage) {

        this.disableCancelCancelProductButton = false;
        this.disableSaveCancelProductButton = false;
        this.showSpinner = false;

        // Notify the user that the Product cancellation failed.
        const toastEvent = new ShowToastEvent({
            title: 'Unable to cancel the Product',
            message: errorMessage,
            variant: 'error',
            mode: 'sticky'
        });
        this.dispatchEvent(toastEvent);

    }

    handleUpdateProductClick(row){
        console.log('handleUpdateProductClick: ', row.id);
        console.log('handleUpdateProductClick: ', JSON.stringify(row));
        this.rowToUpdate = row;
        this.showUpdateProductModal = true;
        getMTOCodes({ProductId: row.id})
        .then(result => {
            console.log('handleUpdateProductClick result', result);
            this.MTOcodeOptions = result;
            this.dynamicHeight = 'height: 200px';
        })
        .catch(error => {
            console.log('Error in handleUpdateProductClick callback', error);
            // this.error = error;
        })
    }

    handleCancelUpdateProductClick(){
        this.showUpdateProductModal = false;
        this.newMTOCode = null;
        this.newResponsibility = null;
        this.showMTOOptions = false;
        this.showDefectOptions = false;
        this.DefectCodeOptions = null;
        this.hideSaveButton = true;
        this.rowToUpdate = null;
    }

    handleMTOChange(event){
        console.log('handleMTOChange value: ', event.target);
        this.newMTOCode = event.target.value;
        this.newMTOCodeName = event.target.label;
        if(this.newMTOCode != null && this.newResponsibility != null){
            getDefectCodes({newMTOCode: this.newMTOCode, newResponsibility: this.newResponsibility})
            .then(result => {
                console.log('handleUpdateProductClick result', result);
                this.showDefectOptions = true;
                this.DefectCodeOptions = result;
                this.dynamicHeight = 'height: 350px';
            })
            .catch(error => {
                console.log('Error in handleUpdateProductClick callback', error);
                // this.error = error;
            })
        }
    }

    handleResponsibilityChange(event){
        console.log('handleResponsibilityChange value: ', event.target);
        this.newResponsibility = event.target.value;
        this.showMTOOptions = true;
        this.dynamicHeight = 'height: 400px';
    }

    handleDefectChange(event){
        console.log('handleDefectChange value: ', event.target.value);
        this.newDefectCode = event.target.value;
        this.newDefectCodeLabel = event.target.label;
        this.hideSaveButton = false;
        this.dynamicHeight = 'height: 200px';
    }

    handleSaveEditProductResponsibility(){
        console.log('hhandleSaveEditProductResponsibility');
        console.log('hhandleSaveEditProductResponsibility row', JSON.parse(JSON.stringify(this.rowToUpdate) ));
        // this.showUpdateProductModal = false;
        this.showSpinner = true;
        this.hideSaveButton = true;
        saveEditProduct({RowId: JSON.stringify(this.rowToUpdate.id) , Responsibility: this.newResponsibility, DefectCode: this.newDefectCode, MTOCode: this.newMTOCode})
        .then(result => {
            console.log('handleSaveEditProductResponsibility result', result);
            this.showSpinner = false;
            this.refreshOrderData();
            this.refreshServiceOrderProductData();
            this.showUpdateProductModal = false;
            this.newMTOCode = null;
            this.newResponsibility = null;
            this.showMTOOptions = false;
            this.showDefectOptions = false;
            this.DefectCodeOptions = null;
            this.hideSaveButton = true;
            this.rowToUpdate = null;

        // Notify the user that the Product Update.
            const toastEvent = new ShowToastEvent({
                title: 'Service Products Updated!',
                variant: 'Success',
            });
            this.dispatchEvent(toastEvent);
        })
        .catch(error => {
            console.log('Error in handleSaveEditProductResponsibility callback', error);
            // this.error = error;
        })
    }

    /*
    * @author Jason Flippen
    * @date 02/18/2021
    * @description Method to refresh the cache. 
    */
    refreshOrderData() {
        console.log('Refreshing Order Data');
        return refreshApex(this.wiredOrderResult);
    }

    /*
    * @author Jason Flippen
    * @date 03/03/2021
    * @description Method to refresh the cache. 
    */
    refreshServiceOrderProductData() {
        console.log('Refreshing Service Order Product Data');
        return refreshApex(this.wiredServiceOrderProductListResult);
    }

}