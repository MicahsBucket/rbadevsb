/*
* @author Jason Flippen
* @date 01/10/2020 
* @description Provides functionality for the uncancelPurchaseOrderAction LWC.
*
*			   Supporting database functionality provided by the following Apex Class:
*			   - UncancelPOActionController
*/
import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import { updateRecord } from 'lightning/uiRecordApi';
import getPurchaseOrderData from '@salesforce/apex/UncancelPOActionController.getPurchaseOrderData';
import uncancelPurchaseOrder from '@salesforce/apex/UncancelPOActionController.uncancelPurchaseOrder';

export default class uncancelPurchaseOrderAction extends LightningElement {

    @api recordId;

    wiredPOResult;
    purchaseOrder;
    recordError;

    @track isEligible = false;
    @track disableButtons = false;
    @track showSpinner = false;

    /*
    * @author Jason Flippen
    * @date 01/10/2020
    * @description Method to retrieve data from Purchase Order. 
    */
    @wire(getPurchaseOrderData, { purchaseOrderId: '$recordId' })
/*
    wiredGetPurchaseOrder({error, data}) {
        if (error) {
            this.recordError = error;
            console.log('error in PurchaseOrder callback', error);
        }
        else if (data) {
            console.log('Record Data', JSON.parse(JSON.stringify(data)));
            var purchaseOrder = JSON.parse(JSON.stringify(data));

            var disableSave = true;

            // Is the Purchase Order eligible to be Uncancelled? -- Apex Class returns Profile Name in lowercase
            if ((purchaseOrder.currentUserProfileName === 'system administrator' ||
                 purchaseOrder.currentUserProfileName === 'super administrator' ||
                 purchaseOrder.currentUserProfileName === 'homeowner resolution') &&
                 purchaseOrder.status === 'Cancelled') {
                this.isEligible = true;
                disableSave = false;
            }

            if (disableSave === true) {
                var disableSaveEvent = new CustomEvent('DisableSave');
                this.dispatchEvent(disableSaveEvent);
            }

        }
*/
    wiredGetPurchaseOrderData(result) {

        // Hold on to the provisioned value so it can be refreshed.
        this.wiredPOResult = result;
        const { data, error } = result; // destructure the provisioned value
        if (data) {
            console.log('Record Data', JSON.parse(JSON.stringify(data)));
            this.purchaseOrder = JSON.parse(JSON.stringify(data));

            var disableSave = true;

            // Is the Purchase Order eligible to be Uncancelled? -- Apex Class returns Profile Name in lowercase
            if ((this.purchaseOrder.currentUserProfileName === 'system administrator' ||
                 this.purchaseOrder.currentUserProfileName === 'super administrator' ||
                 this.purchaseOrder.currentUserProfileName === 'homeowner resolution') &&
                 this.purchaseOrder.status === 'Cancelled') {
                this.isEligible = true;
                disableSave = false;
            }

            if (disableSave === true) {
                var disableSaveEvent = new CustomEvent('DisableSave');
                this.dispatchEvent(disableSaveEvent);
            }

        }
        else if (error) {
            this.recordError = error;
            console.log('error in PurchaseOrderData callback', error);
        }

    }

    /*
    * @author Jason Flippen
    * @date 12/30/2019
    * @description Method to update the Status of the Purchase Order to "In Process".
    */
    @api
    handleSaveClick() {

        var showSpinner = new CustomEvent('ShowSpinner');
        var hideSpinner = new CustomEvent('HideSpinner');
        var closeQuickAction = new CustomEvent('CloseQuickAction');

        this.dispatchEvent(showSpinner);

        // Call Apex method to uncancel the Purchase Order.
        uncancelPurchaseOrder({ purchaseOrderId: this.recordId })
        .then((result) => {

            var toastMessage;
            var toastType;
            var saveSuccess = false;
            if (result === 'Uncancel PO Success') {
                saveSuccess = true;
                toastMessage = 'Purchase Order has been Uncancelled';
                toastType = 'success';
            }
            else  {
                toastMessage = result;
                toastType = 'error';
            }

            this.dispatchEvent(hideSpinner);
            
            const showToastEvent = new CustomEvent('ShowToast', { detail: { message: toastMessage, type: toastType } });
            this.dispatchEvent(showToastEvent);
            this.dispatchEvent(closeQuickAction);

            if (saveSuccess === true) {
                // Workaround way to refresh the record page.
                updateRecord({ fields: { Id: this.recordId } });
                // Use the provisioned value to refresh wiredGetPurchaseOrderData.
                return refreshApex(this.wiredPOResult);
            }
            
        })
        .catch((error) => {
            console.log('Uncancel Purchase Order Error', error);
            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(this.closeQuickAction);
        });

    }

}