<apex:page standardController="Order" showHeader="false" sidebar="false" extensions="RMS_ServiceOrderExtensionMgr">   
    <apex:pageMessages />
    <apex:includeScript value="/soap/ajax/26.0/connection.js"/>
    <apex:includeScript value="/support/console/26.0/integration.js"/>
    <apex:includeScript value="{!$Resource.jquery}" />

    <style type="text/css">
        .asset-table {
            border-collapse: collapse;
        }
        .asset-table > tbody > .dataRow {
            border-color: #999;
        }
        .asset-table > tbody > .dataRow.even {
            border-style: solid solid none solid;
        }
        .asset-table > tbody > .dataRow.odd {
            border-style: none solid solid solid;
            border-bottom-width: 10px;
        }
        .asset-table > tbody > .dataRow.even > td {
            padding-top: 10px;
        }
        .asset-table > tbody > .dataRow.odd > td {
            padding-bottom: 15px;
        }
        .product-wrapper {
            position: relative;
        }
        .asset-table .product-container {
            max-height: 300px;
            overflow: auto;
            border-top: 22px solid #e3deb8;
        }
        .product-table .headerRow th {
            height: 0;
            line-height: 0;
            padding-top: 0;
            padding-bottom: 0;
            color: transparent;
            border: none;
            white-space: nowrap;
        }
        .product-table .headerRow th div {
            position: absolute;
            top: 0;
            margin-left: -5px;
            padding: 5px;
            line-height: normal;
        }
        tr.dataRow.highlight td, tr.dataRow.highlight th {
            background: none !important;
        }

    </style>
    
    <script type="text/javascript">
    window.onload = function() {
        testOpenSubtab();
        collapseSections();
        
    };
    
    function collapseSections(){
        if({!hasValidationErrors == false}){
            $('img[id*="additionalProducts"]').each(function() {
                twistSection(this);
            });
        }
    }  
    
    function testOpenSubtab() {
        //First find the ID of the primary tab to put the new subtab in
        sforce.console.getEnclosingPrimaryTabId(openSubtab);
    };
    
    var openSubtab = function openSubtab(result) {
        var primaryTabId = result.id;
        sforce.console.setTabTitle('Service Request {!Order.OrderNumber}');
    }; 
    
     function testCloseTab() {
        
        
        
        if (sforce.console.isInConsole()) { 
            sforce.console.getEnclosingTabId(closeSubtab);
        } else { 
            location.href = '{!baseUrl}/{!Order.Id}';
        }
        
    }
    
    var closeSubtab = function closeSubtab(result) {
        //Now that we have the tab ID, we can close it
        var tabId = result.id;
        sforce.console.closeTab(tabId);
    };       
    
    </script>
    <apex:form id="theForm">
        <apex:pageBlock title="Service Request">
            <apex:pageBlockButtons location="both">
                <apex:commandButton value="Cancel" onClick="testCloseTab();return false" immediate="true"/>
                <apex:commandButton action="{!saveNewProductsContinue}" value="Continue"/>                
            </apex:pageBlockButtons> 
            
            <apex:pageBlockSection columns="1" rendered="false">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Service Type</apex:outputLabel>
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel layout="block" styleClass="requiredBlock" />
                        <apex:selectList required="true" title="PickList1" size="1" value="{!propPickValSelected}" style="width: 150px;">
                            <apex:selectOptions value="{!PickLstValue}" />
                            <apex:actionSupport event="onchange" rerender="out1" />
                        </apex:selectList> 
                    </apex:outputPanel> 
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection id="out1" rendered="false">
                <apex:pageBlockSectionItem rendered="{!IF(propPickValSelected != 'Save', false , true)}">
                    <apex:outputLabel value="Save Reason" for="saveReason"></apex:outputLabel>
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel layout="block" styleClass="requiredBlock" />
                        <apex:inputField value="{!Order.Save_Reason__c}" id="saveReason" style="width: 150px;"/> 
                    </apex:outputPanel>                   
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection rendered="false">
                <apex:inputField value="{!Order.BillToContactId}"/>
                <apex:outputField value="{!Order.OwnerId}"/>      
                <apex:inputField value="{!Order.Service_initiated_By__c}" style="width: 150px;"/>                
                <apex:outputField value="{!Order.Account.Name}"/>
                <apex:inputField value="{!Order.Description}" label="Problem Description" style="width: 288px; height: 118px;"/>
                <apex:outputField value="{!Order.Sold_Order__c}"/>                                                
            </apex:pageBlockSection>
            
            <apex:pageBlockSection title="Select Installed Products" columns="1" collapsible="false">
                <apex:pageBlockTable value="{!selectedAssets}" styleClass="asset-table" var="a" id="table">
                    <apex:column value="{!a.Product_Name__c}" headerValue="Asset Name"/>
                    <apex:column headerValue="Variant Number">
                    <a target="_blank" href="apex/RMS_prodConfigEdit?id={!a.Original_Order_Product__c}&mode=v" id="{!a.Id}" 
                        onblur="LookupHoverDetail.getHover('{!a.Id}').hide();"
                        onclick="LookupHoverDetail.getHover('{!a.Id}').hide();"
                        onmouseout="LookupHoverDetail.getHover('{!a.Id}').hide();" 
                        onmouseover="LookupHoverDetail.getHover('{!a.Id}', '/{!a.Id}/m?retURL=%2F{!a.Id}&isAjaxRequest=1').show();">
                        {!a.Variant_Number__c}
                    </a>
                    </apex:column>
                    <apex:column value="{!a.Install_Date__c}"/>
                    <apex:column value="{!a.Location_Details__c}"/>
                    <apex:column breakBefore="true" colspan="4">

                        <apex:pageBlockSection title="Select Service Products" id="additionalProducts" showheader="true" collapsible="true" columns="1">

                            <apex:outputText ><b>Instructions:</b> Select each product by checking the box to the left of the product, then select the responsible party/parties for each.</apex:outputText>

                            <apex:outputPanel styleClass="product-wrapper">
                            <div class="product-container">
                            <apex:pageBlockTable value="{!AssetToProductWrapperMap[a.Id]}" var="wrapper" border="1" styleClass="product-table">
                                <apex:column headerValue="Select" width="8%" style="vertical-align: initial; border-bottom: 2px solid #ccc;" colspan="1">
                                    <apex:inputCheckBox value="{!wrapper.isSelected}" />
                                </apex:column>
                                <apex:column headerValue="Product" width="28%" style="vertical-align: initial;border-bottom: 2px solid #ccc;">
                                    <apex:outputText value="{!wrapper.product.Service_Product__r.Name}"/>
                                </apex:column>
                                <apex:column headerValue="Manufacturing" width="12%" style="border-bottom: 2px solid #ccc;">
                                    <apex:inputCheckbox value="{!wrapper.manufacturer}"/>
                                </apex:column>
                                <apex:column headerValue="Retailer" width="12%" style="border-bottom: 2px solid #ccc;">                     
                                    <apex:inputCheckbox value="{!wrapper.local}"/>
                                </apex:column>
                                <apex:column headerValue="Customer" width="12%" style="border-bottom: 2px solid #ccc;">
                                    <apex:inputCheckbox value="{!wrapper.customer}"/>
                                </apex:column>                                
                                <apex:column headerValue="Notes" width="28%" style="border-bottom: 2px solid #ccc;">
                                    <apex:inputTextarea value="{!wrapper.description}" />
                                </apex:column>
                            </apex:pageBlockTable>
                            </div>
                            </apex:outputPanel>

                        </apex:pageBlockSection>
                    </apex:column>
                </apex:pageBlockTable>
            </apex:pageBlockSection>
            
        </apex:pageBlock>
    </apex:form>   
</apex:page>