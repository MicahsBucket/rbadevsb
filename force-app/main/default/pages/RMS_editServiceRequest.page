<apex:page standardController="Order" tabStyle="Order" sidebar="true" extensions="RMS_ServiceOrderExtensionMgr">
    <apex:pageMessages escape="false"/>
    <apex:includeScript value="/soap/ajax/26.0/connection.js"/>
    <apex:includeScript value="/support/console/26.0/integration.js"/>
    <apex:includeScript value="{!$Resource.jquery}" />

    <script type="text/javascript">

    Element.prototype.remove = function() {
        this.parentElement.removeChild(this);
    }
    NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
        for(var i = this.length - 1; i >= 0; i--) {
            if(this[i] && this[i].parentElement) {
                this[i].parentElement.removeChild(this[i]);
            }
        }
    }

    var warrantyRejected = 'Warranty Rejected';
    var warrantySubmitted = 'Warranty Submitted';
    var warrantyAccepted = 'Warranty Accepted';
    var closedStatus = 'Closed';

    if(window.onload != null){var oldOnLoad = window.onload;}
    window.onload = function() {
        if(oldOnLoad!=null){oldOnLoad();}
        document.querySelector('option[value=Activated]').remove();
        testSetTabTitle();
        changeIconforConsole();
        limitStatusOptions();
    };

    function testSetTabTitle() {
        //Set the current tab's title
        sforce.console.setTabTitle('Edit Service Request: {!Order.OrderNumber}')
    }


    function callGenerateConsoleURL(urlPart, theTabName) {
        if(sforce.console.isInConsole()) {
            tabName = theTabName;
            var orderURL = '/'+'{!Order.Id}';
            sforce.console.generateConsoleUrl([orderURL, urlPart], callOpenConsoleUrl);
        }else{
            top.location.href = '{!baseUrl}'+urlPart;

        }
    }

    function changeIconforConsole() {
        if(sforce.console.isInConsole()) {
            var icon = document.getElementById("j_id0:j_id31:nonConsole");
            if(icon != null)
                icon.style.display = 'none';
        }
    }

    function limitStatusOptions(){
        var curStatus = '{!Order.Status}';
        var statusSelector = $("select[data-jsid='orderStatus']");
        if(curStatus == warrantyRejected){
            hideOptions(statusSelector, [curStatus, warrantySubmitted, closedStatus]);
        } else if(curStatus == warrantySubmitted){
            hideOptions(statusSelector, [curStatus, warrantyRejected, warrantyAccepted]);
        } else if(curStatus == warrantyAccepted){
            hideOptions(statusSelector, [curStatus, closedStatus]);
        } else if(curStatus == closedStatus){
            hideOptions(statusSelector, [curStatus]);
        }
    }

    function hideOptions(statusSelector, optionArray){
        statusSelector.find('option').each(function(i, val){
            var selectValue = $(val).val();
            if(optionArray.indexOf(selectValue) < 0){
                statusSelector.find(val).remove();
            }
        });
    }

    </script>

    <apex:form rendered="{!IF(Order.RecordType.Name == 'CORO Service',true,false)}">
        <div class="row" style="padding-top: 9px;">
            <div width="25%" style="float: left;">
                <apex:image id="nonConsole" value="{!$Resource.ServiceRequestIcon}" width="33" height="33" rendered="{!IF(Order.RecordType.Name == 'CORO Service',true,false)}"/>
            </div>
            <div width="75%" style="padding-top: 4px; margin-left: 37px;">
                <apex:outputText style="font-size: 2em; font-weight: normal;" value="Service Request {!Order.OrderNumber}" rendered="{!IF(Order.RecordType.Name == 'CORO Service',true,false)}">
                </apex:outputText>
            </div>
        </div>

        <apex:pageBlock tabStyle="Order" rendered="{!IF(Order.Lock_Service_Request__c == FALSE,true,false)}" id="block1">

            <apex:pageBlockButtons >
                <apex:commandButton action="{!verifyWarrantiesAndSave}" value="Save" rendered="{!IF(Order.RecordType.Name == 'CORO Service',true,false)}" />
                <apex:commandButton action="{!cancel}" value="Cancel" rendered="{!IF(Order.RecordType.Name == 'CORO Service',true,false)}"/>
            </apex:pageBlockButtons>
            <apex:pageBlockSection title="Service Request Edit" collapsible="false" columns="2" >
                <apex:inputField value="{!Order.AccountId}"/>
                <apex:inputField value="{!Order.OwnerId}" label="Service Request Owner"/>
                <apex:inputField value="{!Order.BilltoContactId}"/>
                <apex:inputField value="{!Order.EffectiveDate}"/>
                <apex:outputField value="{!Order.OrderNumber}" label="Service Request Number"/>
                <apex:inputField value="{!Order.Store_Location__c}"/>
                <apex:inputField value="{!Order.Incident_Number__c}" rendered="{!IF(Order.Incident_Number__c == null, true, false)}"/>
                <apex:outputField value="{!Order.Incident_Number__c}" rendered="{!IF(Order.Incident_Number__c != null, true, false)}"/>
                <apex:inputField value="{!Order.Service_Type__c}"/>
                <apex:inputField value="{!Order.NSA_Service_Type__c}"/>
                <apex:inputField value="{!Order.Service_initiated_By__c}"/>
                <apex:inputField value="{!Order.NSA_Service_Sub_Type__c}"/>
                <apex:inputField html-data-jsid="orderStatus" value="{!Order.Status}"/>
                <apex:outputField value="{!Order.LSWP_Test_Results__c}"/>
                <apex:inputField value="{!Order.Service_Request_Hold_Reason__c}"/> <!-- Added field for Story-925 -->
                <apex:inputField value="{!Order.Revenue_Recognized_Date__c}" label="Service Revenue Recognized Date" rendered="{!$Profile.Name='Super Administrator'}"/>
                <apex:outputField value="{!Order.Revenue_Recognized_Date__c}" label="Service Revenue Recognized Date" rendered="{!$Profile.Name!='Super Administrator'}"/>
                <apex:outputField value="{!Order.Pivotal_Id__c}"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection title="Description" collapsible="false" columns="1">
                <apex:inputField value="{!Order.Description}" style="width: 1080px; height: 240px"/>
                <apex:inputField value="{!Order.Save_Reason__c}" rendered="{!IF(Order.Service_Type__c == 'Save',true,false)}"/>
                <apex:inputField value="{!Order.Secondary_Save_Reason__c}" rendered="{!IF(Order.Service_Type__c == 'Save',true,false)}"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection title="Reimbursement" columns="2">
                <apex:inputField value="{!Order.of_Visits__c}"/>
                <apex:inputField value="{!Order.FSL_Mileage__c}"/>
                <apex:inputField value="{!Order.Total_Jobsite_Hours__c}"/>
                <apex:inputField value="{!Order.Reimbursement_Mileage__c}"/>
                <apex:inputField value="{!Order.Warranty_Date_Submitted__c}"/>
                <apex:inputField value="{!Order.Total_Procedure_Time__c}"/>
                <apex:outputField value="{!Order.Warranty_Processed_Date__c}"/>
                <apex:inputField value="{!Order.Reimbursement_Labor__c}"/>
                <apex:inputField value="{!Order.Rejection_Reason__c}"/><br/>
                <apex:inputField value="{!Order.Warranty_Date_Accepted__c}"/><br/>
                <apex:inputField value="{!Order.Warranty_Reimbursement_Amount__c}"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection title="Sold Order Details" collapsible="false" columns="2">
                <apex:inputField value="{!Order.Sold_Order__c}"/>
                <apex:outputField value="{!Order.Install_Complete_Date__c}"/><br/>
                <apex:outputField value="{!Order.Original_Tech_Measure__c}"/><br/>
                <apex:outputField value="{!Order.Original_Crew__c}"/><br/>
                <apex:outputField value="{!Order.Original_Stainer__c}"/><br/>
                <apex:outputField value="{!Order.Lead_Service_Tech__c}"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection title="Quote and Billing Details" columns="2" collapsible="true" id="qbd">
                <apex:inputField value="{!Order.OpportunityId}"/>
                <apex:inputField value="{!Order.Quote_Subtotal__c}"/>
                <apex:inputField value="{!Order.QuoteId}"/>
                <apex:inputField value="{!Order.Invoice_Subtotal__c}"/>
                <apex:inputField value="{!Order.Quote_Notes__c}"/>
                <apex:inputField value="{!Order.Billable_Tax_Amount__c}"/>
                <apex:inputField value="{!Order.Customer_Pickup_All__c}"/>
                <apex:inputField value="{!Order.Sales_Tax__c}"/>
                <apex:inputField value="{!Order.AR_Reason__c}"/>
                <apex:outputField value="{!Order.Payments_Received__c}"/>
                <apex:inputField value="{!Order.AR_Age__c}"/>
                <apex:inputField value="{!Order.Amount_Due__c}"/>
                <apex:inputField value="{!Order.Invoice_Notes__c}" style="width: 192px; height:100px;"/>
                <apex:outputField value="{!Order.Amount_Refunded__c}"/><br/>
                <apex:outputField value="{!Order.Business_Adjustments__c}"/><br/>
                <apex:outputField value="{!Order.Non_Sufficient_Funds__c}"/>
            </apex:pageBlockSection>
            <script> //twistSection(document.getElementById('{!$Component.block1.qbd}').getElementsByTagName('img')[0]) </script>
            <apex:pageBlockSection title="Address Details" collapsible="false" columns="2">
                <apex:inputField value="{!Order.BillingCountryCode}" label="Billing Country"/>
                <apex:inputField value="{!Order.ShippingCountryCode}" label="Shipping Country"/>
                <apex:inputField value="{!Order.BillingStreet}"/>
                <apex:inputField value="{!Order.ShippingStreet}"/>
                <apex:inputField value="{!Order.BillingCity}"/>
                <apex:inputField value="{!Order.ShippingCity}"/>
                <apex:inputField value="{!Order.BillingStateCode}" label="Billing State"/>
                <apex:inputField value="{!Order.ShippingStateCode}" label="Shipping State"/>
                <apex:inputField value="{!Order.BillingPostalCode}"/>
                <apex:inputField value="{!Order.ShippingPostalCode}"/>
            </apex:pageBlockSection>
            <apex:pageBlockSection title="System Information: Click Arrow to Expand" collapsible="true" columns="2" id="si">
                <apex:outputField value="{!Order.CreatedById}"/>
                <apex:outputField value="{!Order.LastModifiedById}"/>
                <apex:outputField value="{!Order.Sales_Tax_Percent__c}"/>
                <apex:outputField value="{!Order.RecordType.Name}"/><br/>
                <apex:outputField value="{!Order.hasPO__c}"/>
            </apex:pageBlockSection>
            <script> twistSection(document.getElementById('{!$Component.block1.si}').getElementsByTagName('img')[0]) </script>
            <apex:pageBlockSection title="Additional Information: Click Arrow to Expand" collapsible="true" columns="2" id="ai">
                <apex:outputField value="{!Order.Primary_Tech_Measure_FSL__c}"/>
                <apex:outputField value="{!Order.Tech_Measure_Date__c}"/><br/>
                <apex:outputField value="{!Order.Tech_Measure_Status__c}"/>
                <apex:outputField value="{!Order.Primary_Installer_FSL__c}"/>
                <apex:outputField value="{!Order.Installation_Date__c}"/><br/>
                <apex:outputField value="{!Order.Install_Order_Status__c}"/><br/>
                <apex:outputField value="{!Order.Service_Installation_Complete_Date__c}"/>
                <apex:outputField value="{!Order.Stainer__c}"/>
                <apex:outputField value="{!Order.Paint_Stain_Status__c}"/>
                <apex:outputField value="{!Order.Primary_Service_FSL__c}"/>
                <apex:outputField value="{!Order.Service_Status__c}"/><br/>
                <apex:outputField value="{!Order.HOA_Status__c}"/><br/>
                <apex:outputField value="{!Order.Historical_Status__c}"/><br/>
                <apex:outputField value="{!Order.Permit_Status__c}"/><br/>
                <apex:outputField value="{!Order.LSWP_Status__c}"/>
            </apex:pageBlockSection>
            <script> twistSection(document.getElementById('{!$Component.block1.ai}').getElementsByTagName('img')[0]) </script>
        </apex:pageBlock>
        <apex:pageBlock mode="edit" tabStyle="Order" rendered="{!IF(Order.Lock_Service_Request__c == TRUE,true,false)}">
            <apex:pageBlockButtons >
                <apex:commandButton action="{!verifyWarrantiesAndSave}" value="Save" rendered="{!IF(Order.RecordType.Name == 'CORO Service',true,false)}" />
                <apex:commandButton action="{!cancel}" value="Cancel" rendered="{!IF(Order.RecordType.Name == 'CORO Service',true,false)}"/>
            </apex:pageBlockButtons>
            <apex:pageBlockSection title="Update Request Status" collapsible="false" columns="1">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Service Request Status</apex:outputLabel>
                        <apex:outputPanel layout="block" styleClass="requiredInput">
                            <apex:outputPanel layout="block" styleClass="requiredBlock" />
                            <apex:inputField html-data-jsid="orderStatus" value="{!Order.Status}"/>
                            </apex:outputPanel>
                     </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>

</apex:page>